/*
               File: ModuloTabelaWC
        Description: Modulo Tabela WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:49.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulotabelawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public modulotabelawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public modulotabelawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_ModuloCod )
      {
         this.AV7Tabela_ModuloCod = aP0_Tabela_ModuloCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         chkTabela_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Tabela_ModuloCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_46 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_46_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_46_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Tabela_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
                  AV62TFTabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
                  AV63TFTabela_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFTabela_Nome_Sel", AV63TFTabela_Nome_Sel);
                  AV66TFTabela_SistemaDes = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
                  AV67TFTabela_SistemaDes_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFTabela_SistemaDes_Sel", AV67TFTabela_SistemaDes_Sel);
                  AV70TFTabela_PaiNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
                  AV71TFTabela_PaiNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFTabela_PaiNom_Sel", AV71TFTabela_PaiNom_Sel);
                  AV74TFTabela_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
                  AV7Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0)));
                  AV64ddo_Tabela_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Tabela_NomeTitleControlIdToReplace", AV64ddo_Tabela_NomeTitleControlIdToReplace);
                  AV68ddo_Tabela_SistemaDesTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Tabela_SistemaDesTitleControlIdToReplace", AV68ddo_Tabela_SistemaDesTitleControlIdToReplace);
                  AV72ddo_Tabela_PaiNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_Tabela_PaiNomTitleControlIdToReplace", AV72ddo_Tabela_PaiNomTitleControlIdToReplace);
                  AV75ddo_Tabela_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_Tabela_AtivoTitleControlIdToReplace", AV75ddo_Tabela_AtivoTitleControlIdToReplace);
                  AV85Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A181Tabela_PaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n181Tabela_PaiCod = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA3R2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV85Pgmname = "ModuloTabelaWC";
               context.Gx_err = 0;
               WS3R2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Modulo Tabela WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117185032");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("modulotabelawc.aspx") + "?" + UrlEncode("" +AV7Tabela_ModuloCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME1", StringUtil.RTrim( AV18Tabela_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_NOME", StringUtil.RTrim( AV62TFTabela_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_NOME_SEL", StringUtil.RTrim( AV63TFTabela_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_SISTEMADES", AV66TFTabela_SistemaDes);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_SISTEMADES_SEL", AV67TFTabela_SistemaDes_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_PAINOM", StringUtil.RTrim( AV70TFTabela_PaiNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_PAINOM_SEL", StringUtil.RTrim( AV71TFTabela_PaiNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFTABELA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_46", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_46), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV76DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTABELA_NOMETITLEFILTERDATA", AV61Tabela_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTABELA_NOMETITLEFILTERDATA", AV61Tabela_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTABELA_SISTEMADESTITLEFILTERDATA", AV65Tabela_SistemaDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTABELA_SISTEMADESTITLEFILTERDATA", AV65Tabela_SistemaDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTABELA_PAINOMTITLEFILTERDATA", AV69Tabela_PaiNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTABELA_PAINOMTITLEFILTERDATA", AV69Tabela_PaiNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTABELA_ATIVOTITLEFILTERDATA", AV73Tabela_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTABELA_ATIVOTITLEFILTERDATA", AV73Tabela_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV85Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Caption", StringUtil.RTrim( Ddo_tabela_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Tooltip", StringUtil.RTrim( Ddo_tabela_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Cls", StringUtil.RTrim( Ddo_tabela_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tabela_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tabela_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Filtertype", StringUtil.RTrim( Ddo_tabela_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Datalisttype", StringUtil.RTrim( Ddo_tabela_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Datalistproc", StringUtil.RTrim( Ddo_tabela_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Sortasc", StringUtil.RTrim( Ddo_tabela_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Sortdsc", StringUtil.RTrim( Ddo_tabela_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Loadingdata", StringUtil.RTrim( Ddo_tabela_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tabela_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tabela_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Caption", StringUtil.RTrim( Ddo_tabela_sistemades_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Tooltip", StringUtil.RTrim( Ddo_tabela_sistemades_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Cls", StringUtil.RTrim( Ddo_tabela_sistemades_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_sistemades_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_sistemades_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_sistemades_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_sistemades_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Sortedstatus", StringUtil.RTrim( Ddo_tabela_sistemades_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Includefilter", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Filtertype", StringUtil.RTrim( Ddo_tabela_sistemades_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_sistemades_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_sistemades_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Datalisttype", StringUtil.RTrim( Ddo_tabela_sistemades_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Datalistproc", StringUtil.RTrim( Ddo_tabela_sistemades_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_sistemades_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Sortasc", StringUtil.RTrim( Ddo_tabela_sistemades_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Sortdsc", StringUtil.RTrim( Ddo_tabela_sistemades_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Loadingdata", StringUtil.RTrim( Ddo_tabela_sistemades_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Cleanfilter", StringUtil.RTrim( Ddo_tabela_sistemades_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Noresultsfound", StringUtil.RTrim( Ddo_tabela_sistemades_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_sistemades_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Caption", StringUtil.RTrim( Ddo_tabela_painom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Tooltip", StringUtil.RTrim( Ddo_tabela_painom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Cls", StringUtil.RTrim( Ddo_tabela_painom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Filteredtext_set", StringUtil.RTrim( Ddo_tabela_painom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_painom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_painom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_painom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_painom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_painom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Sortedstatus", StringUtil.RTrim( Ddo_tabela_painom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Includefilter", StringUtil.BoolToStr( Ddo_tabela_painom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Filtertype", StringUtil.RTrim( Ddo_tabela_painom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Filterisrange", StringUtil.BoolToStr( Ddo_tabela_painom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_painom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Datalisttype", StringUtil.RTrim( Ddo_tabela_painom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Datalistproc", StringUtil.RTrim( Ddo_tabela_painom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tabela_painom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Sortasc", StringUtil.RTrim( Ddo_tabela_painom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Sortdsc", StringUtil.RTrim( Ddo_tabela_painom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Loadingdata", StringUtil.RTrim( Ddo_tabela_painom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Cleanfilter", StringUtil.RTrim( Ddo_tabela_painom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Noresultsfound", StringUtil.RTrim( Ddo_tabela_painom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_painom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Caption", StringUtil.RTrim( Ddo_tabela_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_tabela_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Cls", StringUtil.RTrim( Ddo_tabela_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_tabela_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tabela_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tabela_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_tabela_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_tabela_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_tabela_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_tabela_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_tabela_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_tabela_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_tabela_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_tabela_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_tabela_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_tabela_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_tabela_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tabela_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Activeeventkey", StringUtil.RTrim( Ddo_tabela_sistemades_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_sistemades_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_SISTEMADES_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_sistemades_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Activeeventkey", StringUtil.RTrim( Ddo_tabela_painom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Filteredtext_get", StringUtil.RTrim( Ddo_tabela_painom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_PAINOM_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_painom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_tabela_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_TABELA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_tabela_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm3R2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("modulotabelawc.js", "?20203117185231");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ModuloTabelaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Modulo Tabela WC" ;
      }

      protected void WB3R0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "modulotabelawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_3R2( true) ;
         }
         else
         {
            wb_table1_2_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_ModuloCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A188Tabela_ModuloCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_ModuloCod_Jsonclick, 0, "Attribute", "", "", "", edtTabela_ModuloCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ModuloTabelaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_nome_Internalname, StringUtil.RTrim( AV62TFTabela_Nome), StringUtil.RTrim( context.localUtil.Format( AV62TFTabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_nome_sel_Internalname, StringUtil.RTrim( AV63TFTabela_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV63TFTabela_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_sistemades_Internalname, AV66TFTabela_SistemaDes, StringUtil.RTrim( context.localUtil.Format( AV66TFTabela_SistemaDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_sistemades_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_sistemades_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_sistemades_sel_Internalname, AV67TFTabela_SistemaDes_Sel, StringUtil.RTrim( context.localUtil.Format( AV67TFTabela_SistemaDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_sistemades_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_sistemades_sel_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_painom_Internalname, StringUtil.RTrim( AV70TFTabela_PaiNom), StringUtil.RTrim( context.localUtil.Format( AV70TFTabela_PaiNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_painom_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_painom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_painom_sel_Internalname, StringUtil.RTrim( AV71TFTabela_PaiNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV71TFTabela_PaiNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_painom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_painom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftabela_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV74TFTabela_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftabela_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftabela_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ModuloTabelaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_TABELA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname, AV64ddo_Tabela_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavDdo_tabela_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ModuloTabelaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_TABELA_SISTEMADESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ModuloTabelaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_TABELA_PAINOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ModuloTabelaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_TABELA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname, AV75ddo_Tabela_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ModuloTabelaWC.htm");
         }
         wbLoad = true;
      }

      protected void START3R2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Modulo Tabela WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP3R0( ) ;
            }
         }
      }

      protected void WS3R2( )
      {
         START3R2( ) ;
         EVT3R2( ) ;
      }

      protected void EVT3R2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E113R2 */
                                    E113R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E123R2 */
                                    E123R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_SISTEMADES.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E133R2 */
                                    E133R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_PAINOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E143R2 */
                                    E143R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TABELA_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E153R2 */
                                    E153R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E163R2 */
                                    E163R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E173R2 */
                                    E173R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E183R2 */
                                    E183R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3R0( ) ;
                              }
                              nGXsfl_46_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_46_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_46_idx), 4, 0)), 4, "0");
                              SubsflControlProps_462( ) ;
                              AV36Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV83Update_GXI : context.convertURL( context.PathToRelativeUrl( AV36Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV84Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_SistemaCod_Internalname), ",", "."));
                              A191Tabela_SistemaDes = StringUtil.Upper( cgiGet( edtTabela_SistemaDes_Internalname));
                              n191Tabela_SistemaDes = false;
                              A181Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_PaiCod_Internalname), ",", "."));
                              n181Tabela_PaiCod = false;
                              A182Tabela_PaiNom = StringUtil.Upper( cgiGet( edtTabela_PaiNom_Internalname));
                              n182Tabela_PaiNom = false;
                              A174Tabela_Ativo = StringUtil.StrToBool( cgiGet( chkTabela_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E193R2 */
                                          E193R2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E203R2 */
                                          E203R2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E213R2 */
                                          E213R2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME1"), AV18Tabela_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_NOME"), AV62TFTabela_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_NOME_SEL"), AV63TFTabela_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_sistemades Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_SISTEMADES"), AV66TFTabela_SistemaDes) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_sistemades_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_SISTEMADES_SEL"), AV67TFTabela_SistemaDes_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_painom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_PAINOM"), AV70TFTabela_PaiNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_painom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_PAINOM_SEL"), AV71TFTabela_PaiNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tftabela_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTABELA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV74TFTabela_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP3R0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3R2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3R2( ) ;
            }
         }
      }

      protected void PA3R2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TABELA_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            GXCCtl = "TABELA_ATIVO_" + sGXsfl_46_idx;
            chkTabela_Ativo.Name = GXCCtl;
            chkTabela_Ativo.WebTags = "";
            chkTabela_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkTabela_Ativo_Internalname, "TitleCaption", chkTabela_Ativo.Caption);
            chkTabela_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_462( ) ;
         while ( nGXsfl_46_idx <= nRC_GXsfl_46 )
         {
            sendrow_462( ) ;
            nGXsfl_46_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_46_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_46_idx+1));
            sGXsfl_46_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_46_idx), 4, 0)), 4, "0");
            SubsflControlProps_462( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Tabela_Nome1 ,
                                       String AV62TFTabela_Nome ,
                                       String AV63TFTabela_Nome_Sel ,
                                       String AV66TFTabela_SistemaDes ,
                                       String AV67TFTabela_SistemaDes_Sel ,
                                       String AV70TFTabela_PaiNom ,
                                       String AV71TFTabela_PaiNom_Sel ,
                                       short AV74TFTabela_Ativo_Sel ,
                                       int AV7Tabela_ModuloCod ,
                                       String AV64ddo_Tabela_NomeTitleControlIdToReplace ,
                                       String AV68ddo_Tabela_SistemaDesTitleControlIdToReplace ,
                                       String AV72ddo_Tabela_PaiNomTitleControlIdToReplace ,
                                       String AV75ddo_Tabela_AtivoTitleControlIdToReplace ,
                                       String AV85Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A172Tabela_Codigo ,
                                       int A190Tabela_SistemaCod ,
                                       int A181Tabela_PaiCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3R2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_PAICOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_PAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_ATIVO", GetSecureSignedToken( sPrefix, A174Tabela_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_ATIVO", StringUtil.BoolToStr( A174Tabela_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3R2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV85Pgmname = "ModuloTabelaWC";
         context.Gx_err = 0;
      }

      protected void RF3R2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 46;
         /* Execute user event: E203R2 */
         E203R2 ();
         nGXsfl_46_idx = 1;
         sGXsfl_46_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_46_idx), 4, 0)), 4, "0");
         SubsflControlProps_462( ) ;
         nGXsfl_46_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_462( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Tabela_Nome1 ,
                                                 AV63TFTabela_Nome_Sel ,
                                                 AV62TFTabela_Nome ,
                                                 AV67TFTabela_SistemaDes_Sel ,
                                                 AV66TFTabela_SistemaDes ,
                                                 AV71TFTabela_PaiNom_Sel ,
                                                 AV70TFTabela_PaiNom ,
                                                 AV74TFTabela_Ativo_Sel ,
                                                 A173Tabela_Nome ,
                                                 A191Tabela_SistemaDes ,
                                                 A182Tabela_PaiNom ,
                                                 A174Tabela_Ativo ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A188Tabela_ModuloCod ,
                                                 AV7Tabela_ModuloCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT
                                                 }
            });
            lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            lV62TFTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV62TFTabela_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
            lV66TFTabela_SistemaDes = StringUtil.Concat( StringUtil.RTrim( AV66TFTabela_SistemaDes), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
            lV70TFTabela_PaiNom = StringUtil.PadR( StringUtil.RTrim( AV70TFTabela_PaiNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
            /* Using cursor H003R2 */
            pr_default.execute(0, new Object[] {AV7Tabela_ModuloCod, lV18Tabela_Nome1, lV18Tabela_Nome1, lV62TFTabela_Nome, AV63TFTabela_Nome_Sel, lV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, lV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_46_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A188Tabela_ModuloCod = H003R2_A188Tabela_ModuloCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
               n188Tabela_ModuloCod = H003R2_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H003R2_A174Tabela_Ativo[0];
               A182Tabela_PaiNom = H003R2_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H003R2_n182Tabela_PaiNom[0];
               A181Tabela_PaiCod = H003R2_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = H003R2_n181Tabela_PaiCod[0];
               A191Tabela_SistemaDes = H003R2_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H003R2_n191Tabela_SistemaDes[0];
               A190Tabela_SistemaCod = H003R2_A190Tabela_SistemaCod[0];
               A173Tabela_Nome = H003R2_A173Tabela_Nome[0];
               A172Tabela_Codigo = H003R2_A172Tabela_Codigo[0];
               A182Tabela_PaiNom = H003R2_A182Tabela_PaiNom[0];
               n182Tabela_PaiNom = H003R2_n182Tabela_PaiNom[0];
               A191Tabela_SistemaDes = H003R2_A191Tabela_SistemaDes[0];
               n191Tabela_SistemaDes = H003R2_n191Tabela_SistemaDes[0];
               /* Execute user event: E213R2 */
               E213R2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 46;
            WB3R0( ) ;
         }
         nGXsfl_46_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Tabela_Nome1 ,
                                              AV63TFTabela_Nome_Sel ,
                                              AV62TFTabela_Nome ,
                                              AV67TFTabela_SistemaDes_Sel ,
                                              AV66TFTabela_SistemaDes ,
                                              AV71TFTabela_PaiNom_Sel ,
                                              AV70TFTabela_PaiNom ,
                                              AV74TFTabela_Ativo_Sel ,
                                              A173Tabela_Nome ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A174Tabela_Ativo ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A188Tabela_ModuloCod ,
                                              AV7Tabela_ModuloCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT
                                              }
         });
         lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
         lV18Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18Tabela_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
         lV62TFTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV62TFTabela_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
         lV66TFTabela_SistemaDes = StringUtil.Concat( StringUtil.RTrim( AV66TFTabela_SistemaDes), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
         lV70TFTabela_PaiNom = StringUtil.PadR( StringUtil.RTrim( AV70TFTabela_PaiNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
         /* Using cursor H003R3 */
         pr_default.execute(1, new Object[] {AV7Tabela_ModuloCod, lV18Tabela_Nome1, lV18Tabela_Nome1, lV62TFTabela_Nome, AV63TFTabela_Nome_Sel, lV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, lV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel});
         GRID_nRecordCount = H003R3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Tabela_Nome1, AV62TFTabela_Nome, AV63TFTabela_Nome_Sel, AV66TFTabela_SistemaDes, AV67TFTabela_SistemaDes_Sel, AV70TFTabela_PaiNom, AV71TFTabela_PaiNom_Sel, AV74TFTabela_Ativo_Sel, AV7Tabela_ModuloCod, AV64ddo_Tabela_NomeTitleControlIdToReplace, AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, AV72ddo_Tabela_PaiNomTitleControlIdToReplace, AV75ddo_Tabela_AtivoTitleControlIdToReplace, AV85Pgmname, AV11GridState, A172Tabela_Codigo, A190Tabela_SistemaCod, A181Tabela_PaiCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3R0( )
      {
         /* Before Start, stand alone formulas. */
         AV85Pgmname = "ModuloTabelaWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E193R2 */
         E193R2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV76DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vTABELA_NOMETITLEFILTERDATA"), AV61Tabela_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vTABELA_SISTEMADESTITLEFILTERDATA"), AV65Tabela_SistemaDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vTABELA_PAINOMTITLEFILTERDATA"), AV69Tabela_PaiNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vTABELA_ATIVOTITLEFILTERDATA"), AV73Tabela_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Tabela_Nome1 = StringUtil.Upper( cgiGet( edtavTabela_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            A188Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_ModuloCod_Internalname), ",", "."));
            n188Tabela_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            AV62TFTabela_Nome = StringUtil.Upper( cgiGet( edtavTftabela_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
            AV63TFTabela_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftabela_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFTabela_Nome_Sel", AV63TFTabela_Nome_Sel);
            AV66TFTabela_SistemaDes = StringUtil.Upper( cgiGet( edtavTftabela_sistemades_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
            AV67TFTabela_SistemaDes_Sel = StringUtil.Upper( cgiGet( edtavTftabela_sistemades_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFTabela_SistemaDes_Sel", AV67TFTabela_SistemaDes_Sel);
            AV70TFTabela_PaiNom = StringUtil.Upper( cgiGet( edtavTftabela_painom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
            AV71TFTabela_PaiNom_Sel = StringUtil.Upper( cgiGet( edtavTftabela_painom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFTabela_PaiNom_Sel", AV71TFTabela_PaiNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTABELA_ATIVO_SEL");
               GX_FocusControl = edtavTftabela_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFTabela_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
            }
            else
            {
               AV74TFTabela_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTftabela_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
            }
            AV64ddo_Tabela_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Tabela_NomeTitleControlIdToReplace", AV64ddo_Tabela_NomeTitleControlIdToReplace);
            AV68ddo_Tabela_SistemaDesTitleControlIdToReplace = cgiGet( edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Tabela_SistemaDesTitleControlIdToReplace", AV68ddo_Tabela_SistemaDesTitleControlIdToReplace);
            AV72ddo_Tabela_PaiNomTitleControlIdToReplace = cgiGet( edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_Tabela_PaiNomTitleControlIdToReplace", AV72ddo_Tabela_PaiNomTitleControlIdToReplace);
            AV75ddo_Tabela_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_Tabela_AtivoTitleControlIdToReplace", AV75ddo_Tabela_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_46 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_46"), ",", "."));
            AV78GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV79GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tabela_ModuloCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tabela_nome_Caption = cgiGet( sPrefix+"DDO_TABELA_NOME_Caption");
            Ddo_tabela_nome_Tooltip = cgiGet( sPrefix+"DDO_TABELA_NOME_Tooltip");
            Ddo_tabela_nome_Cls = cgiGet( sPrefix+"DDO_TABELA_NOME_Cls");
            Ddo_tabela_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_TABELA_NOME_Filteredtext_set");
            Ddo_tabela_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_TABELA_NOME_Selectedvalue_set");
            Ddo_tabela_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_TABELA_NOME_Dropdownoptionstype");
            Ddo_tabela_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_TABELA_NOME_Titlecontrolidtoreplace");
            Ddo_tabela_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_NOME_Includesortasc"));
            Ddo_tabela_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_NOME_Includesortdsc"));
            Ddo_tabela_nome_Sortedstatus = cgiGet( sPrefix+"DDO_TABELA_NOME_Sortedstatus");
            Ddo_tabela_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_NOME_Includefilter"));
            Ddo_tabela_nome_Filtertype = cgiGet( sPrefix+"DDO_TABELA_NOME_Filtertype");
            Ddo_tabela_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_NOME_Filterisrange"));
            Ddo_tabela_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_NOME_Includedatalist"));
            Ddo_tabela_nome_Datalisttype = cgiGet( sPrefix+"DDO_TABELA_NOME_Datalisttype");
            Ddo_tabela_nome_Datalistproc = cgiGet( sPrefix+"DDO_TABELA_NOME_Datalistproc");
            Ddo_tabela_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_TABELA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_nome_Sortasc = cgiGet( sPrefix+"DDO_TABELA_NOME_Sortasc");
            Ddo_tabela_nome_Sortdsc = cgiGet( sPrefix+"DDO_TABELA_NOME_Sortdsc");
            Ddo_tabela_nome_Loadingdata = cgiGet( sPrefix+"DDO_TABELA_NOME_Loadingdata");
            Ddo_tabela_nome_Cleanfilter = cgiGet( sPrefix+"DDO_TABELA_NOME_Cleanfilter");
            Ddo_tabela_nome_Noresultsfound = cgiGet( sPrefix+"DDO_TABELA_NOME_Noresultsfound");
            Ddo_tabela_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_TABELA_NOME_Searchbuttontext");
            Ddo_tabela_sistemades_Caption = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Caption");
            Ddo_tabela_sistemades_Tooltip = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Tooltip");
            Ddo_tabela_sistemades_Cls = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Cls");
            Ddo_tabela_sistemades_Filteredtext_set = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Filteredtext_set");
            Ddo_tabela_sistemades_Selectedvalue_set = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Selectedvalue_set");
            Ddo_tabela_sistemades_Dropdownoptionstype = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Dropdownoptionstype");
            Ddo_tabela_sistemades_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Titlecontrolidtoreplace");
            Ddo_tabela_sistemades_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Includesortasc"));
            Ddo_tabela_sistemades_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Includesortdsc"));
            Ddo_tabela_sistemades_Sortedstatus = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Sortedstatus");
            Ddo_tabela_sistemades_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Includefilter"));
            Ddo_tabela_sistemades_Filtertype = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Filtertype");
            Ddo_tabela_sistemades_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Filterisrange"));
            Ddo_tabela_sistemades_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Includedatalist"));
            Ddo_tabela_sistemades_Datalisttype = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Datalisttype");
            Ddo_tabela_sistemades_Datalistproc = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Datalistproc");
            Ddo_tabela_sistemades_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_sistemades_Sortasc = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Sortasc");
            Ddo_tabela_sistemades_Sortdsc = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Sortdsc");
            Ddo_tabela_sistemades_Loadingdata = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Loadingdata");
            Ddo_tabela_sistemades_Cleanfilter = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Cleanfilter");
            Ddo_tabela_sistemades_Noresultsfound = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Noresultsfound");
            Ddo_tabela_sistemades_Searchbuttontext = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Searchbuttontext");
            Ddo_tabela_painom_Caption = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Caption");
            Ddo_tabela_painom_Tooltip = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Tooltip");
            Ddo_tabela_painom_Cls = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Cls");
            Ddo_tabela_painom_Filteredtext_set = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Filteredtext_set");
            Ddo_tabela_painom_Selectedvalue_set = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Selectedvalue_set");
            Ddo_tabela_painom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Dropdownoptionstype");
            Ddo_tabela_painom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Titlecontrolidtoreplace");
            Ddo_tabela_painom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Includesortasc"));
            Ddo_tabela_painom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Includesortdsc"));
            Ddo_tabela_painom_Sortedstatus = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Sortedstatus");
            Ddo_tabela_painom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Includefilter"));
            Ddo_tabela_painom_Filtertype = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Filtertype");
            Ddo_tabela_painom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Filterisrange"));
            Ddo_tabela_painom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Includedatalist"));
            Ddo_tabela_painom_Datalisttype = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Datalisttype");
            Ddo_tabela_painom_Datalistproc = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Datalistproc");
            Ddo_tabela_painom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_TABELA_PAINOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tabela_painom_Sortasc = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Sortasc");
            Ddo_tabela_painom_Sortdsc = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Sortdsc");
            Ddo_tabela_painom_Loadingdata = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Loadingdata");
            Ddo_tabela_painom_Cleanfilter = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Cleanfilter");
            Ddo_tabela_painom_Noresultsfound = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Noresultsfound");
            Ddo_tabela_painom_Searchbuttontext = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Searchbuttontext");
            Ddo_tabela_ativo_Caption = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Caption");
            Ddo_tabela_ativo_Tooltip = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Tooltip");
            Ddo_tabela_ativo_Cls = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Cls");
            Ddo_tabela_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Selectedvalue_set");
            Ddo_tabela_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Dropdownoptionstype");
            Ddo_tabela_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Titlecontrolidtoreplace");
            Ddo_tabela_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_ATIVO_Includesortasc"));
            Ddo_tabela_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_ATIVO_Includesortdsc"));
            Ddo_tabela_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Sortedstatus");
            Ddo_tabela_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_ATIVO_Includefilter"));
            Ddo_tabela_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_TABELA_ATIVO_Includedatalist"));
            Ddo_tabela_ativo_Datalisttype = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Datalisttype");
            Ddo_tabela_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Datalistfixedvalues");
            Ddo_tabela_ativo_Sortasc = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Sortasc");
            Ddo_tabela_ativo_Sortdsc = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Sortdsc");
            Ddo_tabela_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Cleanfilter");
            Ddo_tabela_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tabela_nome_Activeeventkey = cgiGet( sPrefix+"DDO_TABELA_NOME_Activeeventkey");
            Ddo_tabela_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_TABELA_NOME_Filteredtext_get");
            Ddo_tabela_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_TABELA_NOME_Selectedvalue_get");
            Ddo_tabela_sistemades_Activeeventkey = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Activeeventkey");
            Ddo_tabela_sistemades_Filteredtext_get = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Filteredtext_get");
            Ddo_tabela_sistemades_Selectedvalue_get = cgiGet( sPrefix+"DDO_TABELA_SISTEMADES_Selectedvalue_get");
            Ddo_tabela_painom_Activeeventkey = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Activeeventkey");
            Ddo_tabela_painom_Filteredtext_get = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Filteredtext_get");
            Ddo_tabela_painom_Selectedvalue_get = cgiGet( sPrefix+"DDO_TABELA_PAINOM_Selectedvalue_get");
            Ddo_tabela_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Activeeventkey");
            Ddo_tabela_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_TABELA_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME1"), AV18Tabela_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_NOME"), AV62TFTabela_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_NOME_SEL"), AV63TFTabela_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_SISTEMADES"), AV66TFTabela_SistemaDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_SISTEMADES_SEL"), AV67TFTabela_SistemaDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_PAINOM"), AV70TFTabela_PaiNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFTABELA_PAINOM_SEL"), AV71TFTabela_PaiNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFTABELA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV74TFTabela_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E193R2 */
         E193R2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E193R2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV16DynamicFiltersSelector1 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTftabela_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_nome_Visible), 5, 0)));
         edtavTftabela_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_nome_sel_Visible), 5, 0)));
         edtavTftabela_sistemades_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_sistemades_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_sistemades_Visible), 5, 0)));
         edtavTftabela_sistemades_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_sistemades_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_sistemades_sel_Visible), 5, 0)));
         edtavTftabela_painom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_painom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_painom_Visible), 5, 0)));
         edtavTftabela_painom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_painom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_painom_sel_Visible), 5, 0)));
         edtavTftabela_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTftabela_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftabela_ativo_sel_Visible), 5, 0)));
         Ddo_tabela_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "TitleControlIdToReplace", Ddo_tabela_nome_Titlecontrolidtoreplace);
         AV64ddo_Tabela_NomeTitleControlIdToReplace = Ddo_tabela_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64ddo_Tabela_NomeTitleControlIdToReplace", AV64ddo_Tabela_NomeTitleControlIdToReplace);
         edtavDdo_tabela_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_sistemades_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_SistemaDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "TitleControlIdToReplace", Ddo_tabela_sistemades_Titlecontrolidtoreplace);
         AV68ddo_Tabela_SistemaDesTitleControlIdToReplace = Ddo_tabela_sistemades_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68ddo_Tabela_SistemaDesTitleControlIdToReplace", AV68ddo_Tabela_SistemaDesTitleControlIdToReplace);
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_painom_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_PaiNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "TitleControlIdToReplace", Ddo_tabela_painom_Titlecontrolidtoreplace);
         AV72ddo_Tabela_PaiNomTitleControlIdToReplace = Ddo_tabela_painom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72ddo_Tabela_PaiNomTitleControlIdToReplace", AV72ddo_Tabela_PaiNomTitleControlIdToReplace);
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tabela_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Tabela_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "TitleControlIdToReplace", Ddo_tabela_ativo_Titlecontrolidtoreplace);
         AV75ddo_Tabela_AtivoTitleControlIdToReplace = Ddo_tabela_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV75ddo_Tabela_AtivoTitleControlIdToReplace", AV75ddo_Tabela_AtivoTitleControlIdToReplace);
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtTabela_ModuloCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_ModuloCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_ModuloCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Sistema", 0);
         cmbavOrderedby.addItem("3", "Tabela Pai", 0);
         cmbavOrderedby.addItem("4", "Ativo", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV76DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV76DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E203R2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV61Tabela_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Tabela_SistemaDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Tabela_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Tabela_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTabela_Nome_Titleformat = 2;
         edtTabela_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV64ddo_Tabela_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Nome_Internalname, "Title", edtTabela_Nome_Title);
         edtTabela_SistemaDes_Titleformat = 2;
         edtTabela_SistemaDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV68ddo_Tabela_SistemaDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_SistemaDes_Internalname, "Title", edtTabela_SistemaDes_Title);
         edtTabela_PaiNom_Titleformat = 2;
         edtTabela_PaiNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela Pai", AV72ddo_Tabela_PaiNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_PaiNom_Internalname, "Title", edtTabela_PaiNom_Title);
         chkTabela_Ativo_Titleformat = 2;
         chkTabela_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV75ddo_Tabela_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkTabela_Ativo_Internalname, "Title", chkTabela_Ativo.Title.Text);
         AV78GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV78GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78GridCurrentPage), 10, 0)));
         AV79GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV79GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV61Tabela_NomeTitleFilterData", AV61Tabela_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65Tabela_SistemaDesTitleFilterData", AV65Tabela_SistemaDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV69Tabela_PaiNomTitleFilterData", AV69Tabela_PaiNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV73Tabela_AtivoTitleFilterData", AV73Tabela_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E113R2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV77PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV77PageToGo) ;
         }
      }

      protected void E123R2( )
      {
         /* Ddo_tabela_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV62TFTabela_Nome = Ddo_tabela_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
            AV63TFTabela_Nome_Sel = Ddo_tabela_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFTabela_Nome_Sel", AV63TFTabela_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E133R2( )
      {
         /* Ddo_tabela_sistemades_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_sistemades_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_sistemades_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_sistemades_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFTabela_SistemaDes = Ddo_tabela_sistemades_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
            AV67TFTabela_SistemaDes_Sel = Ddo_tabela_sistemades_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFTabela_SistemaDes_Sel", AV67TFTabela_SistemaDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E143R2( )
      {
         /* Ddo_tabela_painom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_painom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_painom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_painom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFTabela_PaiNom = Ddo_tabela_painom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
            AV71TFTabela_PaiNom_Sel = Ddo_tabela_painom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFTabela_PaiNom_Sel", AV71TFTabela_PaiNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E153R2( )
      {
         /* Ddo_tabela_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tabela_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_tabela_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFTabela_Ativo_Sel = (short)(NumberUtil.Val( Ddo_tabela_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E213R2( )
      {
         /* Grid_Load Routine */
         AV36Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV36Update);
         AV83Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV7Tabela_ModuloCod);
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV35Delete);
         AV84Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV7Tabela_ModuloCod);
         edtTabela_Nome_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTabela_PaiNom_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A181Tabela_PaiCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 46;
         }
         sendrow_462( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_46_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(46, GridRow);
         }
      }

      protected void E163R2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E183R2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E173R2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tabela_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
         Ddo_tabela_sistemades_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
         Ddo_tabela_painom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
         Ddo_tabela_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_tabela_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SortedStatus", Ddo_tabela_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_tabela_sistemades_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SortedStatus", Ddo_tabela_sistemades_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_tabela_painom_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SortedStatus", Ddo_tabela_painom_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_tabela_ativo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SortedStatus", Ddo_tabela_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTabela_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 )
         {
            edtavTabela_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV62TFTabela_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
         Ddo_tabela_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "FilteredText_set", Ddo_tabela_nome_Filteredtext_set);
         AV63TFTabela_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFTabela_Nome_Sel", AV63TFTabela_Nome_Sel);
         Ddo_tabela_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SelectedValue_set", Ddo_tabela_nome_Selectedvalue_set);
         AV66TFTabela_SistemaDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
         Ddo_tabela_sistemades_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "FilteredText_set", Ddo_tabela_sistemades_Filteredtext_set);
         AV67TFTabela_SistemaDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFTabela_SistemaDes_Sel", AV67TFTabela_SistemaDes_Sel);
         Ddo_tabela_sistemades_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SelectedValue_set", Ddo_tabela_sistemades_Selectedvalue_set);
         AV70TFTabela_PaiNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
         Ddo_tabela_painom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "FilteredText_set", Ddo_tabela_painom_Filteredtext_set);
         AV71TFTabela_PaiNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFTabela_PaiNom_Sel", AV71TFTabela_PaiNom_Sel);
         Ddo_tabela_painom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SelectedValue_set", Ddo_tabela_painom_Selectedvalue_set);
         AV74TFTabela_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
         Ddo_tabela_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SelectedValue_set", Ddo_tabela_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "TABELA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18Tabela_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get(AV85Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV85Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV37Session.Get(AV85Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV86GXV1 = 1;
         while ( AV86GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV86GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME") == 0 )
            {
               AV62TFTabela_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62TFTabela_Nome", AV62TFTabela_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFTabela_Nome)) )
               {
                  Ddo_tabela_nome_Filteredtext_set = AV62TFTabela_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "FilteredText_set", Ddo_tabela_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME_SEL") == 0 )
            {
               AV63TFTabela_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFTabela_Nome_Sel", AV63TFTabela_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) )
               {
                  Ddo_tabela_nome_Selectedvalue_set = AV63TFTabela_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_nome_Internalname, "SelectedValue_set", Ddo_tabela_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES") == 0 )
            {
               AV66TFTabela_SistemaDes = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66TFTabela_SistemaDes", AV66TFTabela_SistemaDes);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFTabela_SistemaDes)) )
               {
                  Ddo_tabela_sistemades_Filteredtext_set = AV66TFTabela_SistemaDes;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "FilteredText_set", Ddo_tabela_sistemades_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES_SEL") == 0 )
            {
               AV67TFTabela_SistemaDes_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFTabela_SistemaDes_Sel", AV67TFTabela_SistemaDes_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) )
               {
                  Ddo_tabela_sistemades_Selectedvalue_set = AV67TFTabela_SistemaDes_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_sistemades_Internalname, "SelectedValue_set", Ddo_tabela_sistemades_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM") == 0 )
            {
               AV70TFTabela_PaiNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70TFTabela_PaiNom", AV70TFTabela_PaiNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_PaiNom)) )
               {
                  Ddo_tabela_painom_Filteredtext_set = AV70TFTabela_PaiNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "FilteredText_set", Ddo_tabela_painom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM_SEL") == 0 )
            {
               AV71TFTabela_PaiNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFTabela_PaiNom_Sel", AV71TFTabela_PaiNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) )
               {
                  Ddo_tabela_painom_Selectedvalue_set = AV71TFTabela_PaiNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_painom_Internalname, "SelectedValue_set", Ddo_tabela_painom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFTABELA_ATIVO_SEL") == 0 )
            {
               AV74TFTabela_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74TFTabela_Ativo_Sel", StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0));
               if ( ! (0==AV74TFTabela_Ativo_Sel) )
               {
                  Ddo_tabela_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_tabela_ativo_Internalname, "SelectedValue_set", Ddo_tabela_ativo_Selectedvalue_set);
               }
            }
            AV86GXV1 = (int)(AV86GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Tabela_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Tabela_Nome1", AV18Tabela_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV37Session.Get(AV85Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFTabela_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV62TFTabela_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV63TFTabela_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFTabela_SistemaDes)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_SISTEMADES";
            AV12GridStateFilterValue.gxTpr_Value = AV66TFTabela_SistemaDes;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_SISTEMADES_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV67TFTabela_SistemaDes_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_PaiNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_PAINOM";
            AV12GridStateFilterValue.gxTpr_Value = AV70TFTabela_PaiNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_PAINOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV71TFTabela_PaiNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV74TFTabela_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTABELA_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV74TFTabela_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Tabela_ModuloCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&TABELA_MODULOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV85Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV18Tabela_Nome1;
            AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV85Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Tabela";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Tabela_ModuloCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV37Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_3R2( true) ;
         }
         else
         {
            wb_table2_8_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_43_3R2( true) ;
         }
         else
         {
            wb_table3_43_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table3_43_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3R2e( true) ;
         }
         else
         {
            wb_table1_2_3R2e( false) ;
         }
      }

      protected void wb_table3_43_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"46\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_SistemaDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_SistemaDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_SistemaDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTabela_PaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtTabela_PaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTabela_PaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkTabela_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkTabela_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkTabela_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A191Tabela_SistemaDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_SistemaDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_SistemaDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A182Tabela_PaiNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_PaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_PaiNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTabela_PaiNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A174Tabela_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkTabela_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkTabela_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 46 )
         {
            wbEnd = 0;
            nRC_GXsfl_46 = (short)(nGXsfl_46_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_43_3R2e( true) ;
         }
         else
         {
            wb_table3_43_3R2e( false) ;
         }
      }

      protected void wb_table2_8_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_3R2( true) ;
         }
         else
         {
            wb_table4_11_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_ModuloTabelaWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_20_3R2( true) ;
         }
         else
         {
            wb_table5_20_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table5_20_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3R2e( true) ;
         }
         else
         {
            wb_table2_8_3R2e( false) ;
         }
      }

      protected void wb_table5_20_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_25_3R2( true) ;
         }
         else
         {
            wb_table6_25_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table6_25_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_20_3R2e( true) ;
         }
         else
         {
            wb_table5_20_3R2e( false) ;
         }
      }

      protected void wb_table6_25_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_ModuloTabelaWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_34_3R2( true) ;
         }
         else
         {
            wb_table7_34_3R2( false) ;
         }
         return  ;
      }

      protected void wb_table7_34_3R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_25_3R2e( true) ;
         }
         else
         {
            wb_table6_25_3R2e( false) ;
         }
      }

      protected void wb_table7_34_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_ModuloTabelaWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_46_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome1_Internalname, StringUtil.RTrim( AV18Tabela_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Tabela_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTabela_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloTabelaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_34_3R2e( true) ;
         }
         else
         {
            wb_table7_34_3R2e( false) ;
         }
      }

      protected void wb_table4_11_3R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_3R2e( true) ;
         }
         else
         {
            wb_table4_11_3R2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Tabela_ModuloCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3R2( ) ;
         WS3R2( ) ;
         WE3R2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Tabela_ModuloCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA3R2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "modulotabelawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA3R2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Tabela_ModuloCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0)));
         }
         wcpOAV7Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Tabela_ModuloCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Tabela_ModuloCod != wcpOAV7Tabela_ModuloCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Tabela_ModuloCod = AV7Tabela_ModuloCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Tabela_ModuloCod = cgiGet( sPrefix+"AV7Tabela_ModuloCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7Tabela_ModuloCod) > 0 )
         {
            AV7Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Tabela_ModuloCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_ModuloCod), 6, 0)));
         }
         else
         {
            AV7Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Tabela_ModuloCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA3R2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS3R2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS3R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tabela_ModuloCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_ModuloCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Tabela_ModuloCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Tabela_ModuloCod_CTRL", StringUtil.RTrim( sCtrlAV7Tabela_ModuloCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE3R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117185726");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("modulotabelawc.js", "?20203117185726");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_462( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_46_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_46_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_46_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_46_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_46_idx;
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES_"+sGXsfl_46_idx;
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD_"+sGXsfl_46_idx;
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM_"+sGXsfl_46_idx;
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO_"+sGXsfl_46_idx;
      }

      protected void SubsflControlProps_fel_462( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_46_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_46_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_46_fel_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_46_fel_idx;
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD_"+sGXsfl_46_fel_idx;
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES_"+sGXsfl_46_fel_idx;
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD_"+sGXsfl_46_fel_idx;
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM_"+sGXsfl_46_fel_idx;
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO_"+sGXsfl_46_fel_idx;
      }

      protected void sendrow_462( )
      {
         SubsflControlProps_462( ) ;
         WB3R0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_46_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_46_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_46_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV83Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV83Update_GXI : context.PathToRelativeUrl( AV36Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV84Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV84Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtTabela_Nome_Link,(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_SistemaDes_Internalname,(String)A191Tabela_SistemaDes,StringUtil.RTrim( context.localUtil.Format( A191Tabela_SistemaDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_SistemaDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_PaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_PaiNom_Internalname,StringUtil.RTrim( A182Tabela_PaiNom),StringUtil.RTrim( context.localUtil.Format( A182Tabela_PaiNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtTabela_PaiNom_Link,(String)"",(String)"",(String)"",(String)edtTabela_PaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)46,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkTabela_Ativo_Internalname,StringUtil.BoolToStr( A174Tabela_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO"+"_"+sGXsfl_46_idx, GetSecureSignedToken( sPrefix+sGXsfl_46_idx, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME"+"_"+sGXsfl_46_idx, GetSecureSignedToken( sPrefix+sGXsfl_46_idx, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_SISTEMACOD"+"_"+sGXsfl_46_idx, GetSecureSignedToken( sPrefix+sGXsfl_46_idx, context.localUtil.Format( (decimal)(A190Tabela_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_PAICOD"+"_"+sGXsfl_46_idx, GetSecureSignedToken( sPrefix+sGXsfl_46_idx, context.localUtil.Format( (decimal)(A181Tabela_PaiCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_ATIVO"+"_"+sGXsfl_46_idx, GetSecureSignedToken( sPrefix+sGXsfl_46_idx, A174Tabela_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_46_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_46_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_46_idx+1));
            sGXsfl_46_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_46_idx), 4, 0)), 4, "0");
            SubsflControlProps_462( ) ;
         }
         /* End function sendrow_462 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavTabela_nome1_Internalname = sPrefix+"vTABELA_NOME1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         edtTabela_SistemaCod_Internalname = sPrefix+"TABELA_SISTEMACOD";
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES";
         edtTabela_PaiCod_Internalname = sPrefix+"TABELA_PAICOD";
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM";
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtTabela_ModuloCod_Internalname = sPrefix+"TABELA_MODULOCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavTftabela_nome_Internalname = sPrefix+"vTFTABELA_NOME";
         edtavTftabela_nome_sel_Internalname = sPrefix+"vTFTABELA_NOME_SEL";
         edtavTftabela_sistemades_Internalname = sPrefix+"vTFTABELA_SISTEMADES";
         edtavTftabela_sistemades_sel_Internalname = sPrefix+"vTFTABELA_SISTEMADES_SEL";
         edtavTftabela_painom_Internalname = sPrefix+"vTFTABELA_PAINOM";
         edtavTftabela_painom_sel_Internalname = sPrefix+"vTFTABELA_PAINOM_SEL";
         edtavTftabela_ativo_sel_Internalname = sPrefix+"vTFTABELA_ATIVO_SEL";
         Ddo_tabela_nome_Internalname = sPrefix+"DDO_TABELA_NOME";
         edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tabela_sistemades_Internalname = sPrefix+"DDO_TABELA_SISTEMADES";
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE";
         Ddo_tabela_painom_Internalname = sPrefix+"DDO_TABELA_PAINOM";
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE";
         Ddo_tabela_ativo_Internalname = sPrefix+"DDO_TABELA_ATIVO";
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtTabela_PaiNom_Jsonclick = "";
         edtTabela_PaiCod_Jsonclick = "";
         edtTabela_SistemaDes_Jsonclick = "";
         edtTabela_SistemaCod_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavTabela_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTabela_PaiNom_Link = "";
         edtTabela_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkTabela_Ativo_Titleformat = 0;
         edtTabela_PaiNom_Titleformat = 0;
         edtTabela_SistemaDes_Titleformat = 0;
         edtTabela_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavTabela_nome1_Visible = 1;
         chkTabela_Ativo.Title.Text = "Ativo";
         edtTabela_PaiNom_Title = "Tabela Pai";
         edtTabela_SistemaDes_Title = "Sistema";
         edtTabela_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkTabela_Ativo.Caption = "";
         edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tabela_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftabela_ativo_sel_Jsonclick = "";
         edtavTftabela_ativo_sel_Visible = 1;
         edtavTftabela_painom_sel_Jsonclick = "";
         edtavTftabela_painom_sel_Visible = 1;
         edtavTftabela_painom_Jsonclick = "";
         edtavTftabela_painom_Visible = 1;
         edtavTftabela_sistemades_sel_Jsonclick = "";
         edtavTftabela_sistemades_sel_Visible = 1;
         edtavTftabela_sistemades_Jsonclick = "";
         edtavTftabela_sistemades_Visible = 1;
         edtavTftabela_nome_sel_Jsonclick = "";
         edtavTftabela_nome_sel_Visible = 1;
         edtavTftabela_nome_Jsonclick = "";
         edtavTftabela_nome_Visible = 1;
         edtTabela_ModuloCod_Jsonclick = "";
         edtTabela_ModuloCod_Visible = 1;
         Ddo_tabela_ativo_Searchbuttontext = "Pesquisar";
         Ddo_tabela_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_tabela_ativo_Datalisttype = "FixedValues";
         Ddo_tabela_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_tabela_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_ativo_Titlecontrolidtoreplace = "";
         Ddo_tabela_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_ativo_Cls = "ColumnSettings";
         Ddo_tabela_ativo_Tooltip = "Op��es";
         Ddo_tabela_ativo_Caption = "";
         Ddo_tabela_painom_Searchbuttontext = "Pesquisar";
         Ddo_tabela_painom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_painom_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_painom_Loadingdata = "Carregando dados...";
         Ddo_tabela_painom_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_painom_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_painom_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_painom_Datalistproc = "GetModuloTabelaWCFilterData";
         Ddo_tabela_painom_Datalisttype = "Dynamic";
         Ddo_tabela_painom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_painom_Filtertype = "Character";
         Ddo_tabela_painom_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_painom_Titlecontrolidtoreplace = "";
         Ddo_tabela_painom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_painom_Cls = "ColumnSettings";
         Ddo_tabela_painom_Tooltip = "Op��es";
         Ddo_tabela_painom_Caption = "";
         Ddo_tabela_sistemades_Searchbuttontext = "Pesquisar";
         Ddo_tabela_sistemades_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_sistemades_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_sistemades_Loadingdata = "Carregando dados...";
         Ddo_tabela_sistemades_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_sistemades_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_sistemades_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_sistemades_Datalistproc = "GetModuloTabelaWCFilterData";
         Ddo_tabela_sistemades_Datalisttype = "Dynamic";
         Ddo_tabela_sistemades_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_sistemades_Filtertype = "Character";
         Ddo_tabela_sistemades_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_sistemades_Titlecontrolidtoreplace = "";
         Ddo_tabela_sistemades_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_sistemades_Cls = "ColumnSettings";
         Ddo_tabela_sistemades_Tooltip = "Op��es";
         Ddo_tabela_sistemades_Caption = "";
         Ddo_tabela_nome_Searchbuttontext = "Pesquisar";
         Ddo_tabela_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tabela_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tabela_nome_Loadingdata = "Carregando dados...";
         Ddo_tabela_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tabela_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tabela_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tabela_nome_Datalistproc = "GetModuloTabelaWCFilterData";
         Ddo_tabela_nome_Datalisttype = "Dynamic";
         Ddo_tabela_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tabela_nome_Filtertype = "Character";
         Ddo_tabela_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tabela_nome_Titlecontrolidtoreplace = "";
         Ddo_tabela_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tabela_nome_Cls = "ColumnSettings";
         Ddo_tabela_nome_Tooltip = "Op��es";
         Ddo_tabela_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0}],oparms:[{av:'AV61Tabela_NomeTitleFilterData',fld:'vTABELA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV65Tabela_SistemaDesTitleFilterData',fld:'vTABELA_SISTEMADESTITLEFILTERDATA',pic:'',nv:null},{av:'AV69Tabela_PaiNomTitleFilterData',fld:'vTABELA_PAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV73Tabela_AtivoTitleFilterData',fld:'vTABELA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtTabela_Nome_Titleformat',ctrl:'TABELA_NOME',prop:'Titleformat'},{av:'edtTabela_Nome_Title',ctrl:'TABELA_NOME',prop:'Title'},{av:'edtTabela_SistemaDes_Titleformat',ctrl:'TABELA_SISTEMADES',prop:'Titleformat'},{av:'edtTabela_SistemaDes_Title',ctrl:'TABELA_SISTEMADES',prop:'Title'},{av:'edtTabela_PaiNom_Titleformat',ctrl:'TABELA_PAINOM',prop:'Titleformat'},{av:'edtTabela_PaiNom_Title',ctrl:'TABELA_PAINOM',prop:'Title'},{av:'chkTabela_Ativo_Titleformat',ctrl:'TABELA_ATIVO',prop:'Titleformat'},{av:'chkTabela_Ativo.Title.Text',ctrl:'TABELA_ATIVO',prop:'Title'},{av:'AV78GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV79GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TABELA_NOME.ONOPTIONCLICKED","{handler:'E123R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_tabela_nome_Activeeventkey',ctrl:'DDO_TABELA_NOME',prop:'ActiveEventKey'},{av:'Ddo_tabela_nome_Filteredtext_get',ctrl:'DDO_TABELA_NOME',prop:'FilteredText_get'},{av:'Ddo_tabela_nome_Selectedvalue_get',ctrl:'DDO_TABELA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_SISTEMADES.ONOPTIONCLICKED","{handler:'E133R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_tabela_sistemades_Activeeventkey',ctrl:'DDO_TABELA_SISTEMADES',prop:'ActiveEventKey'},{av:'Ddo_tabela_sistemades_Filteredtext_get',ctrl:'DDO_TABELA_SISTEMADES',prop:'FilteredText_get'},{av:'Ddo_tabela_sistemades_Selectedvalue_get',ctrl:'DDO_TABELA_SISTEMADES',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_PAINOM.ONOPTIONCLICKED","{handler:'E143R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_tabela_painom_Activeeventkey',ctrl:'DDO_TABELA_PAINOM',prop:'ActiveEventKey'},{av:'Ddo_tabela_painom_Filteredtext_get',ctrl:'DDO_TABELA_PAINOM',prop:'FilteredText_get'},{av:'Ddo_tabela_painom_Selectedvalue_get',ctrl:'DDO_TABELA_PAINOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TABELA_ATIVO.ONOPTIONCLICKED","{handler:'E153R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_tabela_ativo_Activeeventkey',ctrl:'DDO_TABELA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_tabela_ativo_Selectedvalue_get',ctrl:'DDO_TABELA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tabela_ativo_Sortedstatus',ctrl:'DDO_TABELA_ATIVO',prop:'SortedStatus'},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tabela_nome_Sortedstatus',ctrl:'DDO_TABELA_NOME',prop:'SortedStatus'},{av:'Ddo_tabela_sistemades_Sortedstatus',ctrl:'DDO_TABELA_SISTEMADES',prop:'SortedStatus'},{av:'Ddo_tabela_painom_Sortedstatus',ctrl:'DDO_TABELA_PAINOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E213R2',iparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV36Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtTabela_Nome_Link',ctrl:'TABELA_NOME',prop:'Link'},{av:'edtTabela_PaiNom_Link',ctrl:'TABELA_PAINOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E163R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E183R2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E173R2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64ddo_Tabela_NomeTitleControlIdToReplace',fld:'vDDO_TABELA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Tabela_SistemaDesTitleControlIdToReplace',fld:'vDDO_TABELA_SISTEMADESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Tabela_PaiNomTitleControlIdToReplace',fld:'vDDO_TABELA_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Tabela_AtivoTitleControlIdToReplace',fld:'vDDO_TABELA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A181Tabela_PaiCod',fld:'TABELA_PAICOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV62TFTabela_Nome',fld:'vTFTABELA_NOME',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Filteredtext_set',ctrl:'DDO_TABELA_NOME',prop:'FilteredText_set'},{av:'AV63TFTabela_Nome_Sel',fld:'vTFTABELA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_nome_Selectedvalue_set',ctrl:'DDO_TABELA_NOME',prop:'SelectedValue_set'},{av:'AV66TFTabela_SistemaDes',fld:'vTFTABELA_SISTEMADES',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Filteredtext_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'FilteredText_set'},{av:'AV67TFTabela_SistemaDes_Sel',fld:'vTFTABELA_SISTEMADES_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_sistemades_Selectedvalue_set',ctrl:'DDO_TABELA_SISTEMADES',prop:'SelectedValue_set'},{av:'AV70TFTabela_PaiNom',fld:'vTFTABELA_PAINOM',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Filteredtext_set',ctrl:'DDO_TABELA_PAINOM',prop:'FilteredText_set'},{av:'AV71TFTabela_PaiNom_Sel',fld:'vTFTABELA_PAINOM_SEL',pic:'@!',nv:''},{av:'Ddo_tabela_painom_Selectedvalue_set',ctrl:'DDO_TABELA_PAINOM',prop:'SelectedValue_set'},{av:'AV74TFTabela_Ativo_Sel',fld:'vTFTABELA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tabela_ativo_Selectedvalue_set',ctrl:'DDO_TABELA_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Tabela_Nome1',fld:'vTABELA_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTabela_nome1_Visible',ctrl:'vTABELA_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tabela_nome_Activeeventkey = "";
         Ddo_tabela_nome_Filteredtext_get = "";
         Ddo_tabela_nome_Selectedvalue_get = "";
         Ddo_tabela_sistemades_Activeeventkey = "";
         Ddo_tabela_sistemades_Filteredtext_get = "";
         Ddo_tabela_sistemades_Selectedvalue_get = "";
         Ddo_tabela_painom_Activeeventkey = "";
         Ddo_tabela_painom_Filteredtext_get = "";
         Ddo_tabela_painom_Selectedvalue_get = "";
         Ddo_tabela_ativo_Activeeventkey = "";
         Ddo_tabela_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Tabela_Nome1 = "";
         AV62TFTabela_Nome = "";
         AV63TFTabela_Nome_Sel = "";
         AV66TFTabela_SistemaDes = "";
         AV67TFTabela_SistemaDes_Sel = "";
         AV70TFTabela_PaiNom = "";
         AV71TFTabela_PaiNom_Sel = "";
         AV64ddo_Tabela_NomeTitleControlIdToReplace = "";
         AV68ddo_Tabela_SistemaDesTitleControlIdToReplace = "";
         AV72ddo_Tabela_PaiNomTitleControlIdToReplace = "";
         AV75ddo_Tabela_AtivoTitleControlIdToReplace = "";
         AV85Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV76DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV61Tabela_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65Tabela_SistemaDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Tabela_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Tabela_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tabela_nome_Filteredtext_set = "";
         Ddo_tabela_nome_Selectedvalue_set = "";
         Ddo_tabela_nome_Sortedstatus = "";
         Ddo_tabela_sistemades_Filteredtext_set = "";
         Ddo_tabela_sistemades_Selectedvalue_set = "";
         Ddo_tabela_sistemades_Sortedstatus = "";
         Ddo_tabela_painom_Filteredtext_set = "";
         Ddo_tabela_painom_Selectedvalue_set = "";
         Ddo_tabela_painom_Sortedstatus = "";
         Ddo_tabela_ativo_Selectedvalue_set = "";
         Ddo_tabela_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV36Update = "";
         AV83Update_GXI = "";
         AV35Delete = "";
         AV84Delete_GXI = "";
         A173Tabela_Nome = "";
         A191Tabela_SistemaDes = "";
         A182Tabela_PaiNom = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Tabela_Nome1 = "";
         lV62TFTabela_Nome = "";
         lV66TFTabela_SistemaDes = "";
         lV70TFTabela_PaiNom = "";
         H003R2_A188Tabela_ModuloCod = new int[1] ;
         H003R2_n188Tabela_ModuloCod = new bool[] {false} ;
         H003R2_A174Tabela_Ativo = new bool[] {false} ;
         H003R2_A182Tabela_PaiNom = new String[] {""} ;
         H003R2_n182Tabela_PaiNom = new bool[] {false} ;
         H003R2_A181Tabela_PaiCod = new int[1] ;
         H003R2_n181Tabela_PaiCod = new bool[] {false} ;
         H003R2_A191Tabela_SistemaDes = new String[] {""} ;
         H003R2_n191Tabela_SistemaDes = new bool[] {false} ;
         H003R2_A190Tabela_SistemaCod = new int[1] ;
         H003R2_A173Tabela_Nome = new String[] {""} ;
         H003R2_A172Tabela_Codigo = new int[1] ;
         H003R3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV37Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Tabela_ModuloCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulotabelawc__default(),
            new Object[][] {
                new Object[] {
               H003R2_A188Tabela_ModuloCod, H003R2_n188Tabela_ModuloCod, H003R2_A174Tabela_Ativo, H003R2_A182Tabela_PaiNom, H003R2_n182Tabela_PaiNom, H003R2_A181Tabela_PaiCod, H003R2_n181Tabela_PaiCod, H003R2_A191Tabela_SistemaDes, H003R2_n191Tabela_SistemaDes, H003R2_A190Tabela_SistemaCod,
               H003R2_A173Tabela_Nome, H003R2_A172Tabela_Codigo
               }
               , new Object[] {
               H003R3_AGRID_nRecordCount
               }
            }
         );
         AV85Pgmname = "ModuloTabelaWC";
         /* GeneXus formulas. */
         AV85Pgmname = "ModuloTabelaWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_46 ;
      private short nGXsfl_46_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV74TFTabela_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_46_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTabela_Nome_Titleformat ;
      private short edtTabela_SistemaDes_Titleformat ;
      private short edtTabela_PaiNom_Titleformat ;
      private short chkTabela_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Tabela_ModuloCod ;
      private int wcpOAV7Tabela_ModuloCod ;
      private int subGrid_Rows ;
      private int A172Tabela_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int A181Tabela_PaiCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tabela_nome_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_sistemades_Datalistupdateminimumcharacters ;
      private int Ddo_tabela_painom_Datalistupdateminimumcharacters ;
      private int A188Tabela_ModuloCod ;
      private int edtTabela_ModuloCod_Visible ;
      private int edtavTftabela_nome_Visible ;
      private int edtavTftabela_nome_sel_Visible ;
      private int edtavTftabela_sistemades_Visible ;
      private int edtavTftabela_sistemades_sel_Visible ;
      private int edtavTftabela_painom_Visible ;
      private int edtavTftabela_painom_sel_Visible ;
      private int edtavTftabela_ativo_sel_Visible ;
      private int edtavDdo_tabela_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_painomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tabela_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV77PageToGo ;
      private int edtavTabela_nome1_Visible ;
      private int AV86GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV78GridCurrentPage ;
      private long AV79GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tabela_nome_Activeeventkey ;
      private String Ddo_tabela_nome_Filteredtext_get ;
      private String Ddo_tabela_nome_Selectedvalue_get ;
      private String Ddo_tabela_sistemades_Activeeventkey ;
      private String Ddo_tabela_sistemades_Filteredtext_get ;
      private String Ddo_tabela_sistemades_Selectedvalue_get ;
      private String Ddo_tabela_painom_Activeeventkey ;
      private String Ddo_tabela_painom_Filteredtext_get ;
      private String Ddo_tabela_painom_Selectedvalue_get ;
      private String Ddo_tabela_ativo_Activeeventkey ;
      private String Ddo_tabela_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_46_idx="0001" ;
      private String AV18Tabela_Nome1 ;
      private String AV62TFTabela_Nome ;
      private String AV63TFTabela_Nome_Sel ;
      private String AV70TFTabela_PaiNom ;
      private String AV71TFTabela_PaiNom_Sel ;
      private String AV85Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tabela_nome_Caption ;
      private String Ddo_tabela_nome_Tooltip ;
      private String Ddo_tabela_nome_Cls ;
      private String Ddo_tabela_nome_Filteredtext_set ;
      private String Ddo_tabela_nome_Selectedvalue_set ;
      private String Ddo_tabela_nome_Dropdownoptionstype ;
      private String Ddo_tabela_nome_Titlecontrolidtoreplace ;
      private String Ddo_tabela_nome_Sortedstatus ;
      private String Ddo_tabela_nome_Filtertype ;
      private String Ddo_tabela_nome_Datalisttype ;
      private String Ddo_tabela_nome_Datalistproc ;
      private String Ddo_tabela_nome_Sortasc ;
      private String Ddo_tabela_nome_Sortdsc ;
      private String Ddo_tabela_nome_Loadingdata ;
      private String Ddo_tabela_nome_Cleanfilter ;
      private String Ddo_tabela_nome_Noresultsfound ;
      private String Ddo_tabela_nome_Searchbuttontext ;
      private String Ddo_tabela_sistemades_Caption ;
      private String Ddo_tabela_sistemades_Tooltip ;
      private String Ddo_tabela_sistemades_Cls ;
      private String Ddo_tabela_sistemades_Filteredtext_set ;
      private String Ddo_tabela_sistemades_Selectedvalue_set ;
      private String Ddo_tabela_sistemades_Dropdownoptionstype ;
      private String Ddo_tabela_sistemades_Titlecontrolidtoreplace ;
      private String Ddo_tabela_sistemades_Sortedstatus ;
      private String Ddo_tabela_sistemades_Filtertype ;
      private String Ddo_tabela_sistemades_Datalisttype ;
      private String Ddo_tabela_sistemades_Datalistproc ;
      private String Ddo_tabela_sistemades_Sortasc ;
      private String Ddo_tabela_sistemades_Sortdsc ;
      private String Ddo_tabela_sistemades_Loadingdata ;
      private String Ddo_tabela_sistemades_Cleanfilter ;
      private String Ddo_tabela_sistemades_Noresultsfound ;
      private String Ddo_tabela_sistemades_Searchbuttontext ;
      private String Ddo_tabela_painom_Caption ;
      private String Ddo_tabela_painom_Tooltip ;
      private String Ddo_tabela_painom_Cls ;
      private String Ddo_tabela_painom_Filteredtext_set ;
      private String Ddo_tabela_painom_Selectedvalue_set ;
      private String Ddo_tabela_painom_Dropdownoptionstype ;
      private String Ddo_tabela_painom_Titlecontrolidtoreplace ;
      private String Ddo_tabela_painom_Sortedstatus ;
      private String Ddo_tabela_painom_Filtertype ;
      private String Ddo_tabela_painom_Datalisttype ;
      private String Ddo_tabela_painom_Datalistproc ;
      private String Ddo_tabela_painom_Sortasc ;
      private String Ddo_tabela_painom_Sortdsc ;
      private String Ddo_tabela_painom_Loadingdata ;
      private String Ddo_tabela_painom_Cleanfilter ;
      private String Ddo_tabela_painom_Noresultsfound ;
      private String Ddo_tabela_painom_Searchbuttontext ;
      private String Ddo_tabela_ativo_Caption ;
      private String Ddo_tabela_ativo_Tooltip ;
      private String Ddo_tabela_ativo_Cls ;
      private String Ddo_tabela_ativo_Selectedvalue_set ;
      private String Ddo_tabela_ativo_Dropdownoptionstype ;
      private String Ddo_tabela_ativo_Titlecontrolidtoreplace ;
      private String Ddo_tabela_ativo_Sortedstatus ;
      private String Ddo_tabela_ativo_Datalisttype ;
      private String Ddo_tabela_ativo_Datalistfixedvalues ;
      private String Ddo_tabela_ativo_Sortasc ;
      private String Ddo_tabela_ativo_Sortdsc ;
      private String Ddo_tabela_ativo_Cleanfilter ;
      private String Ddo_tabela_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtTabela_ModuloCod_Internalname ;
      private String edtTabela_ModuloCod_Jsonclick ;
      private String TempTags ;
      private String edtavTftabela_nome_Internalname ;
      private String edtavTftabela_nome_Jsonclick ;
      private String edtavTftabela_nome_sel_Internalname ;
      private String edtavTftabela_nome_sel_Jsonclick ;
      private String edtavTftabela_sistemades_Internalname ;
      private String edtavTftabela_sistemades_Jsonclick ;
      private String edtavTftabela_sistemades_sel_Internalname ;
      private String edtavTftabela_sistemades_sel_Jsonclick ;
      private String edtavTftabela_painom_Internalname ;
      private String edtavTftabela_painom_Jsonclick ;
      private String edtavTftabela_painom_sel_Internalname ;
      private String edtavTftabela_painom_sel_Jsonclick ;
      private String edtavTftabela_ativo_sel_Internalname ;
      private String edtavTftabela_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_tabela_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_sistemadestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_painomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tabela_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String edtTabela_SistemaCod_Internalname ;
      private String edtTabela_SistemaDes_Internalname ;
      private String edtTabela_PaiCod_Internalname ;
      private String A182Tabela_PaiNom ;
      private String edtTabela_PaiNom_Internalname ;
      private String chkTabela_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV18Tabela_Nome1 ;
      private String lV62TFTabela_Nome ;
      private String lV70TFTabela_PaiNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavTabela_nome1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tabela_nome_Internalname ;
      private String Ddo_tabela_sistemades_Internalname ;
      private String Ddo_tabela_painom_Internalname ;
      private String Ddo_tabela_ativo_Internalname ;
      private String edtTabela_Nome_Title ;
      private String edtTabela_SistemaDes_Title ;
      private String edtTabela_PaiNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtTabela_Nome_Link ;
      private String edtTabela_PaiNom_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavTabela_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7Tabela_ModuloCod ;
      private String sGXsfl_46_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_SistemaCod_Jsonclick ;
      private String edtTabela_SistemaDes_Jsonclick ;
      private String edtTabela_PaiCod_Jsonclick ;
      private String edtTabela_PaiNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool n181Tabela_PaiCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tabela_nome_Includesortasc ;
      private bool Ddo_tabela_nome_Includesortdsc ;
      private bool Ddo_tabela_nome_Includefilter ;
      private bool Ddo_tabela_nome_Filterisrange ;
      private bool Ddo_tabela_nome_Includedatalist ;
      private bool Ddo_tabela_sistemades_Includesortasc ;
      private bool Ddo_tabela_sistemades_Includesortdsc ;
      private bool Ddo_tabela_sistemades_Includefilter ;
      private bool Ddo_tabela_sistemades_Filterisrange ;
      private bool Ddo_tabela_sistemades_Includedatalist ;
      private bool Ddo_tabela_painom_Includesortasc ;
      private bool Ddo_tabela_painom_Includesortdsc ;
      private bool Ddo_tabela_painom_Includefilter ;
      private bool Ddo_tabela_painom_Filterisrange ;
      private bool Ddo_tabela_painom_Includedatalist ;
      private bool Ddo_tabela_ativo_Includesortasc ;
      private bool Ddo_tabela_ativo_Includesortdsc ;
      private bool Ddo_tabela_ativo_Includefilter ;
      private bool Ddo_tabela_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n191Tabela_SistemaDes ;
      private bool n182Tabela_PaiNom ;
      private bool A174Tabela_Ativo ;
      private bool n188Tabela_ModuloCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV36Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV66TFTabela_SistemaDes ;
      private String AV67TFTabela_SistemaDes_Sel ;
      private String AV64ddo_Tabela_NomeTitleControlIdToReplace ;
      private String AV68ddo_Tabela_SistemaDesTitleControlIdToReplace ;
      private String AV72ddo_Tabela_PaiNomTitleControlIdToReplace ;
      private String AV75ddo_Tabela_AtivoTitleControlIdToReplace ;
      private String AV83Update_GXI ;
      private String AV84Delete_GXI ;
      private String A191Tabela_SistemaDes ;
      private String lV66TFTabela_SistemaDes ;
      private String AV36Update ;
      private String AV35Delete ;
      private IGxSession AV37Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCheckbox chkTabela_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H003R2_A188Tabela_ModuloCod ;
      private bool[] H003R2_n188Tabela_ModuloCod ;
      private bool[] H003R2_A174Tabela_Ativo ;
      private String[] H003R2_A182Tabela_PaiNom ;
      private bool[] H003R2_n182Tabela_PaiNom ;
      private int[] H003R2_A181Tabela_PaiCod ;
      private bool[] H003R2_n181Tabela_PaiCod ;
      private String[] H003R2_A191Tabela_SistemaDes ;
      private bool[] H003R2_n191Tabela_SistemaDes ;
      private int[] H003R2_A190Tabela_SistemaCod ;
      private String[] H003R2_A173Tabela_Nome ;
      private int[] H003R2_A172Tabela_Codigo ;
      private long[] H003R3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV61Tabela_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65Tabela_SistemaDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Tabela_PaiNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Tabela_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV76DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class modulotabelawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003R2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Tabela_Nome1 ,
                                             String AV63TFTabela_Nome_Sel ,
                                             String AV62TFTabela_Nome ,
                                             String AV67TFTabela_SistemaDes_Sel ,
                                             String AV66TFTabela_SistemaDes ,
                                             String AV71TFTabela_PaiNom_Sel ,
                                             String AV70TFTabela_PaiNom ,
                                             short AV74TFTabela_Ativo_Sel ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             bool A174Tabela_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A188Tabela_ModuloCod ,
                                             int AV7Tabela_ModuloCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Tabela_ModuloCod], T1.[Tabela_Ativo], T2.[Tabela_Nome] AS Tabela_PaiNom, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T3.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Nome], T1.[Tabela_Codigo]";
         sFromString = " FROM (([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Tabela_ModuloCod] = @AV7Tabela_ModuloCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFTabela_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV62TFTabela_Nome)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV63TFTabela_Nome_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFTabela_SistemaDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV66TFTabela_SistemaDes)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV67TFTabela_SistemaDes_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_PaiNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV70TFTabela_PaiNom)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV71TFTabela_PaiNom_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV74TFTabela_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         }
         if ( AV74TFTabela_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod], T1.[Tabela_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod] DESC, T1.[Tabela_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod], T3.[Sistema_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod] DESC, T3.[Sistema_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod], T2.[Tabela_Nome]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod] DESC, T2.[Tabela_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod], T1.[Tabela_Ativo]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_ModuloCod] DESC, T1.[Tabela_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Tabela_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H003R3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Tabela_Nome1 ,
                                             String AV63TFTabela_Nome_Sel ,
                                             String AV62TFTabela_Nome ,
                                             String AV67TFTabela_SistemaDes_Sel ,
                                             String AV66TFTabela_SistemaDes ,
                                             String AV71TFTabela_PaiNom_Sel ,
                                             String AV70TFTabela_PaiNom ,
                                             short AV74TFTabela_Ativo_Sel ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             bool A174Tabela_Ativo ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A188Tabela_ModuloCod ,
                                             int AV7Tabela_ModuloCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod]) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_ModuloCod] = @AV7Tabela_ModuloCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV18Tabela_Nome1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFTabela_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV62TFTabela_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFTabela_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV63TFTabela_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFTabela_SistemaDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV66TFTabela_SistemaDes)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTabela_SistemaDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV67TFTabela_SistemaDes_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFTabela_PaiNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV70TFTabela_PaiNom)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFTabela_PaiNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV71TFTabela_PaiNom_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV74TFTabela_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         }
         if ( AV74TFTabela_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003R2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] );
               case 1 :
                     return conditional_H003R3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003R2 ;
          prmH003R2 = new Object[] {
          new Object[] {"@AV7Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62TFTabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63TFTabela_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFTabela_SistemaDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV67TFTabela_SistemaDes_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV70TFTabela_PaiNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71TFTabela_PaiNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH003R3 ;
          prmH003R3 = new Object[] {
          new Object[] {"@AV7Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62TFTabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63TFTabela_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFTabela_SistemaDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV67TFTabela_SistemaDes_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV70TFTabela_PaiNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71TFTabela_PaiNom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003R2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003R2,11,0,true,false )
             ,new CursorDef("H003R3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003R3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

}
