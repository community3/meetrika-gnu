/*
               File: PRC_NovaPessoaContratante
        Description: Nova Pessoa Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:41.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novapessoacontratante : GXProcedure
   {
      public prc_novapessoacontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novapessoacontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Contratante_CNPJ ,
                           String aP1_Contratante_RazaoSocial ,
                           String aP2_Contratante_NomeFantasia ,
                           String aP3_Contratante_IE ,
                           String aP4_Contratante_Telefone ,
                           String aP5_Contratante_Ramal ,
                           String aP6_Contratante_Fax ,
                           String aP7_Contratante_Email ,
                           String aP8_Contratante_WebSite ,
                           int aP9_Municipio_Codigo ,
                           String aP10_Contratante_EmailSdaHost ,
                           String aP11_Contratante_EmailSdaUser ,
                           String aP12_Contratante_EmailSdaPass ,
                           short aP13_Contratante_EmailSdaPort ,
                           bool aP14_Contratante_EmailSdaAut ,
                           short aP15_Contratante_EmailSdaSec ,
                           out int aP16_Contratante_Codigo )
      {
         this.AV9Contratante_CNPJ = aP0_Contratante_CNPJ;
         this.AV16Contratante_RazaoSocial = aP1_Contratante_RazaoSocial;
         this.AV14Contratante_NomeFantasia = aP2_Contratante_NomeFantasia;
         this.AV13Contratante_IE = aP3_Contratante_IE;
         this.AV17Contratante_Telefone = aP4_Contratante_Telefone;
         this.AV15Contratante_Ramal = aP5_Contratante_Ramal;
         this.AV12Contratante_Fax = aP6_Contratante_Fax;
         this.AV11Contratante_Email = aP7_Contratante_Email;
         this.AV18Contratante_WebSite = aP8_Contratante_WebSite;
         this.AV19Municipio_Codigo = aP9_Municipio_Codigo;
         this.AV24Contratante_EmailSdaHost = aP10_Contratante_EmailSdaHost;
         this.AV27Contratante_EmailSdaUser = aP11_Contratante_EmailSdaUser;
         this.AV25Contratante_EmailSdaPass = aP12_Contratante_EmailSdaPass;
         this.AV23Contratante_EmailSdaPort = aP13_Contratante_EmailSdaPort;
         this.AV26Contratante_EmailSdaAut = aP14_Contratante_EmailSdaAut;
         this.AV22Contratante_EmailSdaSec = aP15_Contratante_EmailSdaSec;
         this.AV10Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP16_Contratante_Codigo=this.AV10Contratante_Codigo;
      }

      public int executeUdp( String aP0_Contratante_CNPJ ,
                             String aP1_Contratante_RazaoSocial ,
                             String aP2_Contratante_NomeFantasia ,
                             String aP3_Contratante_IE ,
                             String aP4_Contratante_Telefone ,
                             String aP5_Contratante_Ramal ,
                             String aP6_Contratante_Fax ,
                             String aP7_Contratante_Email ,
                             String aP8_Contratante_WebSite ,
                             int aP9_Municipio_Codigo ,
                             String aP10_Contratante_EmailSdaHost ,
                             String aP11_Contratante_EmailSdaUser ,
                             String aP12_Contratante_EmailSdaPass ,
                             short aP13_Contratante_EmailSdaPort ,
                             bool aP14_Contratante_EmailSdaAut ,
                             short aP15_Contratante_EmailSdaSec )
      {
         this.AV9Contratante_CNPJ = aP0_Contratante_CNPJ;
         this.AV16Contratante_RazaoSocial = aP1_Contratante_RazaoSocial;
         this.AV14Contratante_NomeFantasia = aP2_Contratante_NomeFantasia;
         this.AV13Contratante_IE = aP3_Contratante_IE;
         this.AV17Contratante_Telefone = aP4_Contratante_Telefone;
         this.AV15Contratante_Ramal = aP5_Contratante_Ramal;
         this.AV12Contratante_Fax = aP6_Contratante_Fax;
         this.AV11Contratante_Email = aP7_Contratante_Email;
         this.AV18Contratante_WebSite = aP8_Contratante_WebSite;
         this.AV19Municipio_Codigo = aP9_Municipio_Codigo;
         this.AV24Contratante_EmailSdaHost = aP10_Contratante_EmailSdaHost;
         this.AV27Contratante_EmailSdaUser = aP11_Contratante_EmailSdaUser;
         this.AV25Contratante_EmailSdaPass = aP12_Contratante_EmailSdaPass;
         this.AV23Contratante_EmailSdaPort = aP13_Contratante_EmailSdaPort;
         this.AV26Contratante_EmailSdaAut = aP14_Contratante_EmailSdaAut;
         this.AV22Contratante_EmailSdaSec = aP15_Contratante_EmailSdaSec;
         this.AV10Contratante_Codigo = 0 ;
         initialize();
         executePrivate();
         aP16_Contratante_Codigo=this.AV10Contratante_Codigo;
         return AV10Contratante_Codigo ;
      }

      public void executeSubmit( String aP0_Contratante_CNPJ ,
                                 String aP1_Contratante_RazaoSocial ,
                                 String aP2_Contratante_NomeFantasia ,
                                 String aP3_Contratante_IE ,
                                 String aP4_Contratante_Telefone ,
                                 String aP5_Contratante_Ramal ,
                                 String aP6_Contratante_Fax ,
                                 String aP7_Contratante_Email ,
                                 String aP8_Contratante_WebSite ,
                                 int aP9_Municipio_Codigo ,
                                 String aP10_Contratante_EmailSdaHost ,
                                 String aP11_Contratante_EmailSdaUser ,
                                 String aP12_Contratante_EmailSdaPass ,
                                 short aP13_Contratante_EmailSdaPort ,
                                 bool aP14_Contratante_EmailSdaAut ,
                                 short aP15_Contratante_EmailSdaSec ,
                                 out int aP16_Contratante_Codigo )
      {
         prc_novapessoacontratante objprc_novapessoacontratante;
         objprc_novapessoacontratante = new prc_novapessoacontratante();
         objprc_novapessoacontratante.AV9Contratante_CNPJ = aP0_Contratante_CNPJ;
         objprc_novapessoacontratante.AV16Contratante_RazaoSocial = aP1_Contratante_RazaoSocial;
         objprc_novapessoacontratante.AV14Contratante_NomeFantasia = aP2_Contratante_NomeFantasia;
         objprc_novapessoacontratante.AV13Contratante_IE = aP3_Contratante_IE;
         objprc_novapessoacontratante.AV17Contratante_Telefone = aP4_Contratante_Telefone;
         objprc_novapessoacontratante.AV15Contratante_Ramal = aP5_Contratante_Ramal;
         objprc_novapessoacontratante.AV12Contratante_Fax = aP6_Contratante_Fax;
         objprc_novapessoacontratante.AV11Contratante_Email = aP7_Contratante_Email;
         objprc_novapessoacontratante.AV18Contratante_WebSite = aP8_Contratante_WebSite;
         objprc_novapessoacontratante.AV19Municipio_Codigo = aP9_Municipio_Codigo;
         objprc_novapessoacontratante.AV24Contratante_EmailSdaHost = aP10_Contratante_EmailSdaHost;
         objprc_novapessoacontratante.AV27Contratante_EmailSdaUser = aP11_Contratante_EmailSdaUser;
         objprc_novapessoacontratante.AV25Contratante_EmailSdaPass = aP12_Contratante_EmailSdaPass;
         objprc_novapessoacontratante.AV23Contratante_EmailSdaPort = aP13_Contratante_EmailSdaPort;
         objprc_novapessoacontratante.AV26Contratante_EmailSdaAut = aP14_Contratante_EmailSdaAut;
         objprc_novapessoacontratante.AV22Contratante_EmailSdaSec = aP15_Contratante_EmailSdaSec;
         objprc_novapessoacontratante.AV10Contratante_Codigo = 0 ;
         objprc_novapessoacontratante.context.SetSubmitInitialConfig(context);
         objprc_novapessoacontratante.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novapessoacontratante);
         aP16_Contratante_Codigo=this.AV10Contratante_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novapessoacontratante)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P002B2 */
         pr_default.execute(0, new Object[] {AV9Contratante_CNPJ});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A36Pessoa_TipoPessoa = P002B2_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = P002B2_A37Pessoa_Docto[0];
            A34Pessoa_Codigo = P002B2_A34Pessoa_Codigo[0];
            AV21Pessoa_Codigo = A34Pessoa_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV20Pessoa.Load(AV21Pessoa_Codigo);
         if ( AV20Pessoa.Fail() )
         {
            AV20Pessoa = new SdtPessoa(context);
         }
         AV20Pessoa.gxTpr_Pessoa_docto = AV9Contratante_CNPJ;
         AV20Pessoa.gxTpr_Pessoa_nome = AV16Contratante_RazaoSocial;
         AV20Pessoa.gxTpr_Pessoa_tipopessoa = "J";
         AV20Pessoa.gxTpr_Pessoa_ativo = true;
         AV20Pessoa.Save();
         if ( AV20Pessoa.Success() )
         {
            AV8Contratante = new SdtContratante(context);
            AV8Contratante.gxTpr_Contratante_pessoacod = AV20Pessoa.gxTpr_Pessoa_codigo;
            AV8Contratante.gxTpr_Contratante_nomefantasia = AV14Contratante_NomeFantasia;
            AV8Contratante.gxTpr_Contratante_ie = AV13Contratante_IE;
            AV8Contratante.gxTpr_Contratante_telefone = AV17Contratante_Telefone;
            AV8Contratante.gxTpr_Contratante_fax = AV12Contratante_Fax;
            AV8Contratante.gxTpr_Contratante_ramal = AV15Contratante_Ramal;
            AV8Contratante.gxTpr_Contratante_email = AV11Contratante_Email;
            AV8Contratante.gxTpr_Contratante_website = AV18Contratante_WebSite;
            AV8Contratante.gxTpr_Municipio_codigo = AV19Municipio_Codigo;
            AV8Contratante.gxTpr_Contratante_emailsdahost = AV24Contratante_EmailSdaHost;
            AV8Contratante.gxTpr_Contratante_emailsdauser = AV27Contratante_EmailSdaUser;
            AV8Contratante.gxTpr_Contratante_emailsdakey = Crypto.GetEncryptionKey( );
            AV8Contratante.gxTpr_Contratante_emailsdapass = Crypto.Encrypt64( AV25Contratante_EmailSdaPass, AV8Contratante.gxTpr_Contratante_emailsdakey);
            AV8Contratante.gxTpr_Contratante_emailsdaport = AV23Contratante_EmailSdaPort;
            AV8Contratante.gxTpr_Contratante_emailsdaaut = AV26Contratante_EmailSdaAut;
            AV8Contratante.gxTpr_Contratante_emailsdasec = AV22Contratante_EmailSdaSec;
            AV8Contratante.Save();
            if ( AV8Contratante.Success() )
            {
               AV10Contratante_Codigo = AV8Contratante.gxTpr_Contratante_codigo;
               context.CommitDataStores( "PRC_NovaPessoaContratante");
            }
            else
            {
               context.RollbackDataStores( "PRC_NovaPessoaContratante");
            }
         }
         else
         {
            context.RollbackDataStores( "PRC_NovaPessoaContratante");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002B2_A36Pessoa_TipoPessoa = new String[] {""} ;
         P002B2_A37Pessoa_Docto = new String[] {""} ;
         P002B2_A34Pessoa_Codigo = new int[1] ;
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         AV20Pessoa = new SdtPessoa(context);
         AV8Contratante = new SdtContratante(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novapessoacontratante__default(),
            new Object[][] {
                new Object[] {
               P002B2_A36Pessoa_TipoPessoa, P002B2_A37Pessoa_Docto, P002B2_A34Pessoa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV23Contratante_EmailSdaPort ;
      private short AV22Contratante_EmailSdaSec ;
      private int AV19Municipio_Codigo ;
      private int AV10Contratante_Codigo ;
      private int A34Pessoa_Codigo ;
      private int AV21Pessoa_Codigo ;
      private String AV16Contratante_RazaoSocial ;
      private String AV14Contratante_NomeFantasia ;
      private String AV13Contratante_IE ;
      private String AV17Contratante_Telefone ;
      private String AV15Contratante_Ramal ;
      private String AV12Contratante_Fax ;
      private String scmdbuf ;
      private String A36Pessoa_TipoPessoa ;
      private bool AV26Contratante_EmailSdaAut ;
      private String AV9Contratante_CNPJ ;
      private String AV11Contratante_Email ;
      private String AV18Contratante_WebSite ;
      private String AV24Contratante_EmailSdaHost ;
      private String AV27Contratante_EmailSdaUser ;
      private String AV25Contratante_EmailSdaPass ;
      private String A37Pessoa_Docto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P002B2_A36Pessoa_TipoPessoa ;
      private String[] P002B2_A37Pessoa_Docto ;
      private int[] P002B2_A34Pessoa_Codigo ;
      private int aP16_Contratante_Codigo ;
      private SdtContratante AV8Contratante ;
      private SdtPessoa AV20Pessoa ;
   }

   public class prc_novapessoacontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002B2 ;
          prmP002B2 = new Object[] {
          new Object[] {"@AV9Contratante_CNPJ",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002B2", "SELECT [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @AV9Contratante_CNPJ) AND ([Pessoa_TipoPessoa] = 'J') ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002B2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
