/*
               File: ExportReportExtraWWContagemResultadoWWAceiteFaturamento
        Description: Export Report Extra WWContagem Resultado WWAceite Faturamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:57:45.83
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportreportextrawwcontagemresultadowwaceitefaturamento : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV90Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14ContagemResultado_StatusCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16ContagemResultado_StatusDmn = GetNextPar( );
                  AV102TFContagemResultado_LoteAceite = GetNextPar( );
                  AV103TFContagemResultado_LoteAceite_Sel = GetNextPar( );
                  AV104TFContagemResultado_OsFsOsFm = GetNextPar( );
                  AV105TFContagemResultado_OsFsOsFm_Sel = GetNextPar( );
                  AV106TFContagemResultado_Descricao = GetNextPar( );
                  AV107TFContagemResultado_Descricao_Sel = GetNextPar( );
                  AV108TFContagemResultado_DataAceite = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  AV109TFContagemResultado_DataAceite_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  AV110TFContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV111TFContagemResultado_DataUltCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV112TFContagemResultado_ContratadaOrigemSigla = GetNextPar( );
                  AV113TFContagemResultado_ContratadaOrigemSigla_Sel = GetNextPar( );
                  AV114TFContagemrResultado_SistemaSigla = GetNextPar( );
                  AV115TFContagemrResultado_SistemaSigla_Sel = GetNextPar( );
                  AV116TFContagemResultado_StatusDmn_SelsJson = GetNextPar( );
                  AV120TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV121TFContagemResultado_ServicoSigla = GetNextPar( );
                  AV122TFContagemResultado_ServicoSigla_Sel = GetNextPar( );
                  AV131TFContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  AV132TFContagemResultado_PFFinal_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV133TFContagemResultado_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
                  AV134TFContagemResultado_ValorPF_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV42GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aexportreportextrawwcontagemresultadowwaceitefaturamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aexportreportextrawwcontagemresultadowwaceitefaturamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           short aP2_ContagemResultado_StatusCnt ,
                           String aP3_ContagemResultado_StatusDmn ,
                           String aP4_TFContagemResultado_LoteAceite ,
                           String aP5_TFContagemResultado_LoteAceite_Sel ,
                           String aP6_TFContagemResultado_OsFsOsFm ,
                           String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataAceite ,
                           DateTime aP11_TFContagemResultado_DataAceite_To ,
                           DateTime aP12_TFContagemResultado_DataUltCnt ,
                           DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                           String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                           String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                           String aP16_TFContagemrResultado_SistemaSigla ,
                           String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP19_TFContagemResultado_Baseline_Sel ,
                           String aP20_TFContagemResultado_ServicoSigla ,
                           String aP21_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP22_TFContagemResultado_PFFinal ,
                           decimal aP23_TFContagemResultado_PFFinal_To ,
                           decimal aP24_TFContagemResultado_ValorPF ,
                           decimal aP25_TFContagemResultado_ValorPF_To ,
                           short aP26_OrderedBy ,
                           String aP27_GridStateXML )
      {
         this.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV90Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV14ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         this.AV16ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         this.AV102TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         this.AV103TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         this.AV104TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         this.AV105TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         this.AV106TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV107TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV108TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         this.AV109TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         this.AV110TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         this.AV111TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         this.AV112TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         this.AV113TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         this.AV114TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         this.AV115TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         this.AV116TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         this.AV120TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         this.AV121TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         this.AV122TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         this.AV131TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         this.AV132TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         this.AV133TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         this.AV134TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         this.AV10OrderedBy = aP26_OrderedBy;
         this.AV42GridStateXML = aP27_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 short aP2_ContagemResultado_StatusCnt ,
                                 String aP3_ContagemResultado_StatusDmn ,
                                 String aP4_TFContagemResultado_LoteAceite ,
                                 String aP5_TFContagemResultado_LoteAceite_Sel ,
                                 String aP6_TFContagemResultado_OsFsOsFm ,
                                 String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataAceite ,
                                 DateTime aP11_TFContagemResultado_DataAceite_To ,
                                 DateTime aP12_TFContagemResultado_DataUltCnt ,
                                 DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                                 String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                                 String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                                 String aP16_TFContagemrResultado_SistemaSigla ,
                                 String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP19_TFContagemResultado_Baseline_Sel ,
                                 String aP20_TFContagemResultado_ServicoSigla ,
                                 String aP21_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP22_TFContagemResultado_PFFinal ,
                                 decimal aP23_TFContagemResultado_PFFinal_To ,
                                 decimal aP24_TFContagemResultado_ValorPF ,
                                 decimal aP25_TFContagemResultado_ValorPF_To ,
                                 short aP26_OrderedBy ,
                                 String aP27_GridStateXML )
      {
         aexportreportextrawwcontagemresultadowwaceitefaturamento objaexportreportextrawwcontagemresultadowwaceitefaturamento;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento = new aexportreportextrawwcontagemresultadowwaceitefaturamento();
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV90Contratada_Codigo = aP1_Contratada_Codigo;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV14ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV16ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV102TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV103TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV104TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV105TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV106TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV107TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV108TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV109TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV110TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV111TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV112TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV113TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV114TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV115TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV116TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV120TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV121TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV122TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV131TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV132TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV133TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV134TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV10OrderedBy = aP26_OrderedBy;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.AV42GridStateXML = aP27_GridStateXML;
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.context.SetSubmitInitialConfig(context);
         objaexportreportextrawwcontagemresultadowwaceitefaturamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportreportextrawwcontagemresultadowwaceitefaturamento);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportreportextrawwcontagemresultadowwaceitefaturamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H330( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         H330( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Lista de Resultado das Contagens", 5, Gx_line+5, 785, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( ! (0==AV12Contratada_AreaTrabalhoCod) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea de Trabalho", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Contratada_AreaTrabalhoCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV90Contratada_Codigo) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contratada", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV90Contratada_Codigo), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV14ContagemResultado_StatusCnt) )
         {
            AV13FilterContagemResultado_StatusCntValueDescription = gxdomainstatuscontagem.getDescription(context,AV14ContagemResultado_StatusCnt);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV13FilterContagemResultado_StatusCntValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ContagemResultado_StatusDmn)) )
         {
            AV15FilterContagemResultado_StatusDmnValueDescription = gxdomainstatusdemanda.getDescription(context,AV16ContagemResultado_StatusDmn);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status da Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV43GridState.gxTpr_Dynamicfilters.FromXml(AV42GridStateXML, "");
         if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(1));
            AV19DynamicFiltersSelector1 = AV44GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV82ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
               AV83ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV82ContagemResultado_DataUltCnt1) )
               {
                  AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                  AV89ContagemResultado_DataUltCnt = AV82ContagemResultado_DataUltCnt1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV83ContagemResultado_DataUltCnt_To1) )
               {
                  AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                  AV89ContagemResultado_DataUltCnt = AV83ContagemResultado_DataUltCnt_To1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV52DynamicFiltersOperator1 = AV44GridStateDynamicFilter.gxTpr_Operator;
               AV22ContagemResultado_OsFsOsFm1 = AV44GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContagemResultado_OsFsOsFm1)) )
               {
                  if ( AV52DynamicFiltersOperator1 == 0 )
                  {
                     AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV52DynamicFiltersOperator1 == 1 )
                  {
                     AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV52DynamicFiltersOperator1 == 2 )
                  {
                     AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV47ContagemResultado_OsFsOsFm = AV22ContagemResultado_OsFsOsFm1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV138ContagemResultado_DataDmn1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
               AV139ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV138ContagemResultado_DataDmn1) )
               {
                  AV140FilterContagemResultado_DataDmnDescription = "Data Demanda";
                  AV141ContagemResultado_DataDmn = AV138ContagemResultado_DataDmn1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV139ContagemResultado_DataDmn_To1) )
               {
                  AV140FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                  AV141ContagemResultado_DataDmn = AV139ContagemResultado_DataDmn_To1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
            {
               AV142ContagemResultado_DataAceite1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
               AV143ContagemResultado_DataAceite_To1 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV142ContagemResultado_DataAceite1) )
               {
                  AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite";
                  AV145ContagemResultado_DataAceite = AV142ContagemResultado_DataAceite1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV143ContagemResultado_DataAceite_To1) )
               {
                  AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite (to)";
                  AV145ContagemResultado_DataAceite = AV143ContagemResultado_DataAceite_To1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV23ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV23ContagemResultado_SistemaCod1) )
               {
                  AV48ContagemResultado_SistemaCod = AV23ContagemResultado_SistemaCod1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV24ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV24ContagemResultado_ContratadaCod1) )
               {
                  AV49ContagemResultado_ContratadaCod = AV24ContagemResultado_ContratadaCod1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               AV146ContagemResultado_ContratadaOrigemCod1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV146ContagemResultado_ContratadaOrigemCod1) )
               {
                  AV147ContagemResultado_ContratadaOrigemCod = AV146ContagemResultado_ContratadaOrigemCod1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV147ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV25ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV25ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV50ContagemResultado_NaoCnfDmnCod = AV25ContagemResultado_NaoCnfDmnCod1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
            {
               AV53ContagemResultado_LoteAceite1 = AV44GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_LoteAceite1)) )
               {
                  AV62ContagemResultado_LoteAceite = AV53ContagemResultado_LoteAceite1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Lote", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV62ContagemResultado_LoteAceite, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
            {
               AV54ContagemResultado_DemandaFM_ORDER1 = (long)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV54ContagemResultado_DemandaFM_ORDER1) )
               {
                  AV64ContagemResultado_DemandaFM_ORDER = AV54ContagemResultado_DemandaFM_ORDER1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("OS FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64ContagemResultado_DemandaFM_ORDER), "ZZZZZZZZZZZZZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV66ContagemResultado_Baseline1 = AV44GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66ContagemResultado_Baseline1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV66ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV71FilterContagemResultado_Baseline1ValueDescription = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV66ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV71FilterContagemResultado_Baseline1ValueDescription = "N�o";
                  }
                  AV70FilterContagemResultado_BaselineValueDescription = AV71FilterContagemResultado_Baseline1ValueDescription;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV78ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV78ContagemResultado_Servico1) )
               {
                  AV81ContagemResultado_Servico = AV78ContagemResultado_Servico1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV91ContagemResultado_Descricao1 = AV44GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ContagemResultado_Descricao1)) )
               {
                  AV92ContagemResultado_Descricao = AV91ContagemResultado_Descricao1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV148ContagemResultado_Agrupador1 = AV44GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148ContagemResultado_Agrupador1)) )
               {
                  AV149ContagemResultado_Agrupador = AV148ContagemResultado_Agrupador1;
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV149ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV26DynamicFiltersEnabled2 = true;
               AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(2));
               AV27DynamicFiltersSelector2 = AV44GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV84ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                  AV85ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV84ContagemResultado_DataUltCnt2) )
                  {
                     AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                     AV89ContagemResultado_DataUltCnt = AV84ContagemResultado_DataUltCnt2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV85ContagemResultado_DataUltCnt_To2) )
                  {
                     AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                     AV89ContagemResultado_DataUltCnt = AV85ContagemResultado_DataUltCnt_To2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV55DynamicFiltersOperator2 = AV44GridStateDynamicFilter.gxTpr_Operator;
                  AV30ContagemResultado_OsFsOsFm2 = AV44GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_OsFsOsFm2)) )
                  {
                     if ( AV55DynamicFiltersOperator2 == 0 )
                     {
                        AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV55DynamicFiltersOperator2 == 1 )
                     {
                        AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV55DynamicFiltersOperator2 == 2 )
                     {
                        AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV47ContagemResultado_OsFsOsFm = AV30ContagemResultado_OsFsOsFm2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV150ContagemResultado_DataDmn2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                  AV151ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV150ContagemResultado_DataDmn2) )
                  {
                     AV140FilterContagemResultado_DataDmnDescription = "Data Demanda";
                     AV141ContagemResultado_DataDmn = AV150ContagemResultado_DataDmn2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV151ContagemResultado_DataDmn_To2) )
                  {
                     AV140FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                     AV141ContagemResultado_DataDmn = AV151ContagemResultado_DataDmn_To2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
               {
                  AV152ContagemResultado_DataAceite2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                  AV153ContagemResultado_DataAceite_To2 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV152ContagemResultado_DataAceite2) )
                  {
                     AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite";
                     AV145ContagemResultado_DataAceite = AV152ContagemResultado_DataAceite2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV153ContagemResultado_DataAceite_To2) )
                  {
                     AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite (to)";
                     AV145ContagemResultado_DataAceite = AV153ContagemResultado_DataAceite_To2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV31ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV31ContagemResultado_SistemaCod2) )
                  {
                     AV48ContagemResultado_SistemaCod = AV31ContagemResultado_SistemaCod2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV32ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV32ContagemResultado_ContratadaCod2) )
                  {
                     AV49ContagemResultado_ContratadaCod = AV32ContagemResultado_ContratadaCod2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
               {
                  AV154ContagemResultado_ContratadaOrigemCod2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV154ContagemResultado_ContratadaOrigemCod2) )
                  {
                     AV147ContagemResultado_ContratadaOrigemCod = AV154ContagemResultado_ContratadaOrigemCod2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV147ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV33ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV33ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV50ContagemResultado_NaoCnfDmnCod = AV33ContagemResultado_NaoCnfDmnCod2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
               {
                  AV56ContagemResultado_LoteAceite2 = AV44GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_LoteAceite2)) )
                  {
                     AV62ContagemResultado_LoteAceite = AV56ContagemResultado_LoteAceite2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Lote", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV62ContagemResultado_LoteAceite, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
               {
                  AV57ContagemResultado_DemandaFM_ORDER2 = (long)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV57ContagemResultado_DemandaFM_ORDER2) )
                  {
                     AV64ContagemResultado_DemandaFM_ORDER = AV57ContagemResultado_DemandaFM_ORDER2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("OS FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64ContagemResultado_DemandaFM_ORDER), "ZZZZZZZZZZZZZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV67ContagemResultado_Baseline2 = AV44GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemResultado_Baseline2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV67ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV72FilterContagemResultado_Baseline2ValueDescription = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV67ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV72FilterContagemResultado_Baseline2ValueDescription = "N�o";
                     }
                     AV70FilterContagemResultado_BaselineValueDescription = AV72FilterContagemResultado_Baseline2ValueDescription;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV79ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV79ContagemResultado_Servico2) )
                  {
                     AV81ContagemResultado_Servico = AV79ContagemResultado_Servico2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV93ContagemResultado_Descricao2 = AV44GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93ContagemResultado_Descricao2)) )
                  {
                     AV92ContagemResultado_Descricao = AV93ContagemResultado_Descricao2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV155ContagemResultado_Agrupador2 = AV44GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155ContagemResultado_Agrupador2)) )
                  {
                     AV149ContagemResultado_Agrupador = AV155ContagemResultado_Agrupador2;
                     H330( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV149ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV43GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV34DynamicFiltersEnabled3 = true;
                  AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV43GridState.gxTpr_Dynamicfilters.Item(3));
                  AV35DynamicFiltersSelector3 = AV44GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV86ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                     AV87ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV86ContagemResultado_DataUltCnt3) )
                     {
                        AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                        AV89ContagemResultado_DataUltCnt = AV86ContagemResultado_DataUltCnt3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV87ContagemResultado_DataUltCnt_To3) )
                     {
                        AV88FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                        AV89ContagemResultado_DataUltCnt = AV87ContagemResultado_DataUltCnt_To3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV88FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV89ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV44GridStateDynamicFilter.gxTpr_Operator;
                     AV38ContagemResultado_OsFsOsFm3 = AV44GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_OsFsOsFm3)) )
                     {
                        if ( AV58DynamicFiltersOperator3 == 0 )
                        {
                           AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV58DynamicFiltersOperator3 == 1 )
                        {
                           AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV58DynamicFiltersOperator3 == 2 )
                        {
                           AV95FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV47ContagemResultado_OsFsOsFm = AV38ContagemResultado_OsFsOsFm3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV47ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV156ContagemResultado_DataDmn3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                     AV157ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV156ContagemResultado_DataDmn3) )
                     {
                        AV140FilterContagemResultado_DataDmnDescription = "Data Demanda";
                        AV141ContagemResultado_DataDmn = AV156ContagemResultado_DataDmn3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV157ContagemResultado_DataDmn_To3) )
                     {
                        AV140FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                        AV141ContagemResultado_DataDmn = AV157ContagemResultado_DataDmn_To3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV140FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV141ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
                  {
                     AV158ContagemResultado_DataAceite3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Value, 2);
                     AV159ContagemResultado_DataAceite_To3 = context.localUtil.CToD( AV44GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV158ContagemResultado_DataAceite3) )
                     {
                        AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite";
                        AV145ContagemResultado_DataAceite = AV158ContagemResultado_DataAceite3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV159ContagemResultado_DataAceite_To3) )
                     {
                        AV144FilterContagemResultado_DataAceiteDescription = "Data do Aceite (to)";
                        AV145ContagemResultado_DataAceite = AV159ContagemResultado_DataAceite_To3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV144FilterContagemResultado_DataAceiteDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV145ContagemResultado_DataAceite, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV39ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV39ContagemResultado_SistemaCod3) )
                     {
                        AV48ContagemResultado_SistemaCod = AV39ContagemResultado_SistemaCod3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV40ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV40ContagemResultado_ContratadaCod3) )
                     {
                        AV49ContagemResultado_ContratadaCod = AV40ContagemResultado_ContratadaCod3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                  {
                     AV160ContagemResultado_ContratadaOrigemCod3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV160ContagemResultado_ContratadaOrigemCod3) )
                     {
                        AV147ContagemResultado_ContratadaOrigemCod = AV160ContagemResultado_ContratadaOrigemCod3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV147ContagemResultado_ContratadaOrigemCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV41ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV41ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV50ContagemResultado_NaoCnfDmnCod = AV41ContagemResultado_NaoCnfDmnCod3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
                  {
                     AV59ContagemResultado_LoteAceite3 = AV44GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_LoteAceite3)) )
                     {
                        AV62ContagemResultado_LoteAceite = AV59ContagemResultado_LoteAceite3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Lote", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV62ContagemResultado_LoteAceite, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
                  {
                     AV60ContagemResultado_DemandaFM_ORDER3 = (long)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV60ContagemResultado_DemandaFM_ORDER3) )
                     {
                        AV64ContagemResultado_DemandaFM_ORDER = AV60ContagemResultado_DemandaFM_ORDER3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("OS FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64ContagemResultado_DemandaFM_ORDER), "ZZZZZZZZZZZZZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV68ContagemResultado_Baseline3 = AV44GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContagemResultado_Baseline3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV68ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV73FilterContagemResultado_Baseline3ValueDescription = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV68ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV73FilterContagemResultado_Baseline3ValueDescription = "N�o";
                        }
                        AV70FilterContagemResultado_BaselineValueDescription = AV73FilterContagemResultado_Baseline3ValueDescription;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV80ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV80ContagemResultado_Servico3) )
                     {
                        AV81ContagemResultado_Servico = AV80ContagemResultado_Servico3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV94ContagemResultado_Descricao3 = AV44GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94ContagemResultado_Descricao3)) )
                     {
                        AV92ContagemResultado_Descricao = AV94ContagemResultado_Descricao3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92ContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV161ContagemResultado_Agrupador3 = AV44GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161ContagemResultado_Agrupador3)) )
                     {
                        AV149ContagemResultado_Agrupador = AV161ContagemResultado_Agrupador3;
                        H330( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV149ContagemResultado_Agrupador, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103TFContagemResultado_LoteAceite_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Lote", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103TFContagemResultado_LoteAceite_Sel, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102TFContagemResultado_LoteAceite)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Lote", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102TFContagemResultado_LoteAceite, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105TFContagemResultado_OsFsOsFm_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105TFContagemResultado_OsFsOsFm_Sel, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFContagemResultado_OsFsOsFm)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV104TFContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107TFContagemResultado_Descricao_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV107TFContagemResultado_Descricao_Sel, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106TFContagemResultado_Descricao)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Titulo", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106TFContagemResultado_Descricao, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (DateTime.MinValue==AV108TFContagemResultado_DataAceite) && (DateTime.MinValue==AV109TFContagemResultado_DataAceite_To) ) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Aceite", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV108TFContagemResultado_DataAceite, "99/99/99 99:99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Aceite (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV109TFContagemResultado_DataAceite_To, "99/99/99 99:99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV110TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV111TFContagemResultado_DataUltCnt_To) ) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV110TFContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV111TFContagemResultado_DataUltCnt_To, "99/99/99"), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113TFContagemResultado_ContratadaOrigemSigla_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV113TFContagemResultado_ContratadaOrigemSigla_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112TFContagemResultado_ContratadaOrigemSigla)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Origem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV112TFContagemResultado_ContratadaOrigemSigla, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115TFContagemrResultado_SistemaSigla_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV115TFContagemrResultado_SistemaSigla_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114TFContagemrResultado_SistemaSigla)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV114TFContagemrResultado_SistemaSigla, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         AV118TFContagemResultado_StatusDmn_Sels.FromJSonString(AV116TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( AV118TFContagemResultado_StatusDmn_Sels.Count == 0 ) )
         {
            AV137i = 1;
            AV168GXV1 = 1;
            while ( AV168GXV1 <= AV118TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV119TFContagemResultado_StatusDmn_Sel = AV118TFContagemResultado_StatusDmn_Sels.GetString(AV168GXV1);
               if ( AV137i == 1 )
               {
                  AV117TFContagemResultado_StatusDmn_SelDscs = "";
               }
               else
               {
                  AV117TFContagemResultado_StatusDmn_SelDscs = AV117TFContagemResultado_StatusDmn_SelDscs + ", ";
               }
               AV135FilterTFContagemResultado_StatusDmn_SelValueDescription = gxdomainstatusdemanda.getDescription(context,AV119TFContagemResultado_StatusDmn_Sel);
               AV117TFContagemResultado_StatusDmn_SelDscs = AV117TFContagemResultado_StatusDmn_SelDscs + AV135FilterTFContagemResultado_StatusDmn_SelValueDescription;
               AV137i = (long)(AV137i+1);
               AV168GXV1 = (int)(AV168GXV1+1);
            }
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV117TFContagemResultado_StatusDmn_SelDscs, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV120TFContagemResultado_Baseline_Sel) )
         {
            if ( AV120TFContagemResultado_Baseline_Sel == 1 )
            {
               AV136FilterTFContagemResultado_Baseline_SelValueDescription = "Marcado";
            }
            else if ( AV120TFContagemResultado_Baseline_Sel == 2 )
            {
               AV136FilterTFContagemResultado_Baseline_SelValueDescription = "Desmarcado";
            }
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("BS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV136FilterTFContagemResultado_Baseline_SelValueDescription, "")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122TFContagemResultado_ServicoSigla_Sel)) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV122TFContagemResultado_ServicoSigla_Sel, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121TFContagemResultado_ServicoSigla)) )
            {
               H330( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV121TFContagemResultado_ServicoSigla, "@!")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV131TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV132TFContagemResultado_PFFinal_To) ) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV131TFContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV132TFContagemResultado_PFFinal_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV133TFContagemResultado_ValorPF) && (Convert.ToDecimal(0)==AV134TFContagemResultado_ValorPF_To) ) )
         {
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Valor da Unidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV133TFContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H330( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Valor da Unidade (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV134TFContagemResultado_ValorPF_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H330( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 65, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(70, Gx_line+30, 130, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(135, Gx_line+30, 195, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(200, Gx_line+30, 260, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(265, Gx_line+30, 325, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(330, Gx_line+30, 390, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(395, Gx_line+30, 455, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(460, Gx_line+30, 520, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(525, Gx_line+30, 585, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(590, Gx_line+30, 650, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(655, Gx_line+30, 715, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(720, Gx_line+30, 780, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Lote", 5, Gx_line+15, 65, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("OS Ref|OS", 70, Gx_line+15, 130, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Titulo", 135, Gx_line+15, 195, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Aceite", 200, Gx_line+15, 260, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contagem", 265, Gx_line+15, 325, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Origem", 330, Gx_line+15, 390, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Sistema", 395, Gx_line+15, 455, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Status Dmn", 460, Gx_line+15, 520, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("BS", 525, Gx_line+15, 585, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Servi�o", 590, Gx_line+15, 650, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PF Final", 655, Gx_line+15, 715, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("Valor da Unidade", 720, Gx_line+15, 780, Gx_line+29, 2, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod = AV12Contratada_AreaTrabalhoCod;
         AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_2_Contratada_codigo = AV90Contratada_Codigo;
         AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_3_Contagemresultado_statuscnt = AV14ContagemResultado_StatusCnt;
         AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn = AV16ContagemResultado_StatusDmn;
         AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = AV19DynamicFiltersSelector1;
         AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 = AV52DynamicFiltersOperator1;
         AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = AV82ContagemResultado_DataUltCnt1;
         AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = AV83ContagemResultado_DataUltCnt_To1;
         AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = AV22ContagemResultado_OsFsOsFm1;
         AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 = AV138ContagemResultado_DataDmn1;
         AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 = AV139ContagemResultado_DataDmn_To1;
         AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 = AV142ContagemResultado_DataAceite1;
         AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 = AV143ContagemResultado_DataAceite_To1;
         AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 = AV23ContagemResultado_SistemaCod1;
         AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 = AV24ContagemResultado_ContratadaCod1;
         AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 = AV146ContagemResultado_ContratadaOrigemCod1;
         AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 = AV25ContagemResultado_NaoCnfDmnCod1;
         AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = AV53ContagemResultado_LoteAceite1;
         AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 = AV54ContagemResultado_DemandaFM_ORDER1;
         AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 = AV66ContagemResultado_Baseline1;
         AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 = AV78ContagemResultado_Servico1;
         AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = AV91ContagemResultado_Descricao1;
         AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 = AV148ContagemResultado_Agrupador1;
         AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 = AV55DynamicFiltersOperator2;
         AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = AV84ContagemResultado_DataUltCnt2;
         AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = AV85ContagemResultado_DataUltCnt_To2;
         AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = AV30ContagemResultado_OsFsOsFm2;
         AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 = AV150ContagemResultado_DataDmn2;
         AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 = AV151ContagemResultado_DataDmn_To2;
         AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 = AV152ContagemResultado_DataAceite2;
         AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 = AV153ContagemResultado_DataAceite_To2;
         AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 = AV31ContagemResultado_SistemaCod2;
         AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 = AV32ContagemResultado_ContratadaCod2;
         AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 = AV154ContagemResultado_ContratadaOrigemCod2;
         AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 = AV33ContagemResultado_NaoCnfDmnCod2;
         AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = AV56ContagemResultado_LoteAceite2;
         AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 = AV57ContagemResultado_DemandaFM_ORDER2;
         AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 = AV67ContagemResultado_Baseline2;
         AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 = AV79ContagemResultado_Servico2;
         AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = AV93ContagemResultado_Descricao2;
         AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 = AV155ContagemResultado_Agrupador2;
         AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = AV34DynamicFiltersEnabled3;
         AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = AV35DynamicFiltersSelector3;
         AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 = AV58DynamicFiltersOperator3;
         AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = AV86ContagemResultado_DataUltCnt3;
         AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = AV87ContagemResultado_DataUltCnt_To3;
         AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = AV38ContagemResultado_OsFsOsFm3;
         AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 = AV156ContagemResultado_DataDmn3;
         AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 = AV157ContagemResultado_DataDmn_To3;
         AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 = AV158ContagemResultado_DataAceite3;
         AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 = AV159ContagemResultado_DataAceite_To3;
         AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 = AV39ContagemResultado_SistemaCod3;
         AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 = AV40ContagemResultado_ContratadaCod3;
         AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 = AV160ContagemResultado_ContratadaOrigemCod3;
         AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 = AV41ContagemResultado_NaoCnfDmnCod3;
         AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = AV59ContagemResultado_LoteAceite3;
         AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 = AV60ContagemResultado_DemandaFM_ORDER3;
         AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 = AV68ContagemResultado_Baseline3;
         AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 = AV80ContagemResultado_Servico3;
         AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = AV94ContagemResultado_Descricao3;
         AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 = AV161ContagemResultado_Agrupador3;
         AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = AV102TFContagemResultado_LoteAceite;
         AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel = AV103TFContagemResultado_LoteAceite_Sel;
         AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = AV104TFContagemResultado_OsFsOsFm;
         AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel = AV105TFContagemResultado_OsFsOsFm_Sel;
         AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = AV106TFContagemResultado_Descricao;
         AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel = AV107TFContagemResultado_Descricao_Sel;
         AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite = AV108TFContagemResultado_DataAceite;
         AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to = AV109TFContagemResultado_DataAceite_To;
         AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = AV110TFContagemResultado_DataUltCnt;
         AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = AV111TFContagemResultado_DataUltCnt_To;
         AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = AV112TFContagemResultado_ContratadaOrigemSigla;
         AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel = AV113TFContagemResultado_ContratadaOrigemSigla_Sel;
         AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = AV114TFContagemrResultado_SistemaSigla;
         AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel = AV115TFContagemrResultado_SistemaSigla_Sel;
         AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels = AV118TFContagemResultado_StatusDmn_Sels;
         AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel = AV120TFContagemResultado_Baseline_Sel;
         AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = AV121TFContagemResultado_ServicoSigla;
         AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel = AV122TFContagemResultado_ServicoSigla_Sel;
         AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal = AV131TFContagemResultado_PFFinal;
         AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to = AV132TFContagemResultado_PFFinal_To;
         AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf = AV133TFContagemResultado_ValorPF;
         AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to = AV134TFContagemResultado_ValorPF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ,
                                              AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ,
                                              AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ,
                                              AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ,
                                              AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ,
                                              AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ,
                                              AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ,
                                              AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ,
                                              AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ,
                                              AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ,
                                              AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ,
                                              AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ,
                                              AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ,
                                              AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ,
                                              AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ,
                                              AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ,
                                              AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ,
                                              AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ,
                                              AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ,
                                              AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ,
                                              AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ,
                                              AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ,
                                              AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ,
                                              AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ,
                                              AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ,
                                              AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ,
                                              AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ,
                                              AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ,
                                              AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ,
                                              AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ,
                                              AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ,
                                              AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ,
                                              AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ,
                                              AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ,
                                              AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ,
                                              AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ,
                                              AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ,
                                              AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ,
                                              AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ,
                                              AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ,
                                              AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ,
                                              AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ,
                                              AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ,
                                              AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ,
                                              AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ,
                                              AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ,
                                              AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ,
                                              AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ,
                                              AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ,
                                              AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ,
                                              AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ,
                                              AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ,
                                              AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ,
                                              AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ,
                                              AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ,
                                              AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ,
                                              AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ,
                                              AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ,
                                              AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ,
                                              AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ,
                                              AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ,
                                              AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ,
                                              AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ,
                                              AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ,
                                              AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ,
                                              AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ,
                                              AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ,
                                              AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ,
                                              AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ,
                                              AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ,
                                              AV43GridState.gxTpr_Dynamicfilters.Count ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A529ContagemResultado_DataAceite ,
                                              A489ContagemResultado_SistemaCod ,
                                              A805ContagemResultado_ContratadaOrigemCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A528ContagemResultado_LoteAceite ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A494ContagemResultado_Descricao ,
                                              A1046ContagemResultado_Agrupador ,
                                              A866ContagemResultado_ContratadaOrigemSigla ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A456ContagemResultado_Codigo ,
                                              AV10OrderedBy ,
                                              AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ,
                                              AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ,
                                              AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ,
                                              AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ,
                                              AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ,
                                              AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ,
                                              AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ,
                                              AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A517ContagemResultado_Ultima ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = StringUtil.PadR( StringUtil.RTrim( AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1), 10, "%");
         lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1), "%", "");
         lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = StringUtil.PadR( StringUtil.RTrim( AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2), 10, "%");
         lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2), "%", "");
         lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = StringUtil.PadR( StringUtil.RTrim( AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3), 10, "%");
         lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3), "%", "");
         lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = StringUtil.PadR( StringUtil.RTrim( AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite), 10, "%");
         lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm), "%", "");
         lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao), "%", "");
         lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = StringUtil.PadR( StringUtil.RTrim( AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla), 15, "%");
         lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00333 */
         pr_default.execute(0, new Object[] {AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1, AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1, AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1, AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1, AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2, AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2, AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2, AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2, AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2, AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2, AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3, AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3, AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3, AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3, AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3, AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3, AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt, AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt, AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to, AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1, AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1, AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1, AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1, AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1, AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1, AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1, AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1, lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1, AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1, AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1, lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1, AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1, AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2, AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2, AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2, AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2, AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2, AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2, AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2, AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2, lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2, AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2, AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2, lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2, AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2, AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3, AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3, AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3, AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3, AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3, AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3, AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3, AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3, lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3, AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3, AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3, lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3, AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3, lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite, AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel, lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm, AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel, lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao, AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel, AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite, AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to, lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla, AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel, lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla, AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel, lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla, AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel, AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf, AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00333_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00333_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P00333_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00333_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P00333_A1583ContagemResultado_TipoRegistro[0];
            A517ContagemResultado_Ultima = P00333_A517ContagemResultado_Ultima[0];
            A512ContagemResultado_ValorPF = P00333_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00333_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P00333_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00333_n801ContagemResultado_ServicoSigla[0];
            A509ContagemrResultado_SistemaSigla = P00333_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00333_n509ContagemrResultado_SistemaSigla[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00333_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00333_n866ContagemResultado_ContratadaOrigemSigla[0];
            A1046ContagemResultado_Agrupador = P00333_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00333_n1046ContagemResultado_Agrupador[0];
            A494ContagemResultado_Descricao = P00333_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00333_n494ContagemResultado_Descricao[0];
            A601ContagemResultado_Servico = P00333_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00333_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00333_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00333_n598ContagemResultado_Baseline[0];
            A528ContagemResultado_LoteAceite = P00333_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00333_n528ContagemResultado_LoteAceite[0];
            A468ContagemResultado_NaoCnfDmnCod = P00333_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00333_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P00333_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00333_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P00333_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00333_n489ContagemResultado_SistemaCod[0];
            A529ContagemResultado_DataAceite = P00333_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00333_n529ContagemResultado_DataAceite[0];
            A471ContagemResultado_DataDmn = P00333_A471ContagemResultado_DataDmn[0];
            A484ContagemResultado_StatusDmn = P00333_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00333_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00333_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00333_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00333_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00333_n52Contratada_AreaTrabalhoCod[0];
            A566ContagemResultado_DataUltCnt = P00333_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00333_A531ContagemResultado_StatusUltCnt[0];
            A493ContagemResultado_DemandaFM = P00333_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00333_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00333_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00333_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00333_A456ContagemResultado_Codigo[0];
            A473ContagemResultado_DataCnt = P00333_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00333_A511ContagemResultado_HoraCnt[0];
            A597ContagemResultado_LoteAceiteCod = P00333_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00333_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P00333_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00333_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P00333_A1583ContagemResultado_TipoRegistro[0];
            A512ContagemResultado_ValorPF = P00333_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00333_n512ContagemResultado_ValorPF[0];
            A1046ContagemResultado_Agrupador = P00333_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00333_n1046ContagemResultado_Agrupador[0];
            A494ContagemResultado_Descricao = P00333_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00333_n494ContagemResultado_Descricao[0];
            A598ContagemResultado_Baseline = P00333_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00333_n598ContagemResultado_Baseline[0];
            A468ContagemResultado_NaoCnfDmnCod = P00333_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00333_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P00333_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00333_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P00333_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00333_n489ContagemResultado_SistemaCod[0];
            A471ContagemResultado_DataDmn = P00333_A471ContagemResultado_DataDmn[0];
            A484ContagemResultado_StatusDmn = P00333_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00333_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00333_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00333_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P00333_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00333_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00333_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00333_n457ContagemResultado_Demanda[0];
            A528ContagemResultado_LoteAceite = P00333_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P00333_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P00333_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P00333_n529ContagemResultado_DataAceite[0];
            A601ContagemResultado_Servico = P00333_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00333_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00333_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00333_n801ContagemResultado_ServicoSigla[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00333_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00333_n866ContagemResultado_ContratadaOrigemSigla[0];
            A509ContagemrResultado_SistemaSigla = P00333_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00333_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00333_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00333_n52Contratada_AreaTrabalhoCod[0];
            A566ContagemResultado_DataUltCnt = P00333_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00333_A531ContagemResultado_StatusUltCnt[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A553ContagemResultado_DemandaFM_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A493ContagemResultado_DemandaFM, 1, 18), "."));
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV18ContagemResultado_StatusDmnDescription = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                  {
                     AV65ContagemResultado_BaselineDescription = "*";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                  {
                     AV65ContagemResultado_BaselineDescription = "";
                  }
                  /* Execute user subroutine: 'BEFOREPRINTLINE' */
                  S152 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
                  H330( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A528ContagemResultado_LoteAceite, "")), 5, Gx_line+2, 65, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 70, Gx_line+2, 130, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), 135, Gx_line+2, 195, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A529ContagemResultado_DataAceite, "99/99/99 99:99"), 200, Gx_line+2, 260, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 265, Gx_line+2, 325, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A866ContagemResultado_ContratadaOrigemSigla, "@!")), 330, Gx_line+2, 390, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 395, Gx_line+2, 455, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_StatusDmnDescription, "")), 460, Gx_line+2, 520, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV65ContagemResultado_BaselineDescription, "")), 525, Gx_line+2, 585, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 590, Gx_line+2, 650, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 655, Gx_line+2, 715, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A512ContagemResultado_ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), 720, Gx_line+2, 780, Gx_line+17, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
                  /* Execute user subroutine: 'AFTERPRINTLINE' */
                  S162 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H330( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV13FilterContagemResultado_StatusCntValueDescription = "";
         AV15FilterContagemResultado_StatusDmnValueDescription = "";
         AV43GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV44GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV19DynamicFiltersSelector1 = "";
         AV82ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV83ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV88FilterContagemResultado_DataUltCntDescription = "";
         AV89ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV22ContagemResultado_OsFsOsFm1 = "";
         AV95FilterContagemResultado_OsFsOsFmDescription = "";
         AV47ContagemResultado_OsFsOsFm = "";
         AV138ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV139ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV140FilterContagemResultado_DataDmnDescription = "";
         AV141ContagemResultado_DataDmn = DateTime.MinValue;
         AV142ContagemResultado_DataAceite1 = DateTime.MinValue;
         AV143ContagemResultado_DataAceite_To1 = DateTime.MinValue;
         AV144FilterContagemResultado_DataAceiteDescription = "";
         AV145ContagemResultado_DataAceite = DateTime.MinValue;
         AV53ContagemResultado_LoteAceite1 = "";
         AV62ContagemResultado_LoteAceite = "";
         AV66ContagemResultado_Baseline1 = "";
         AV71FilterContagemResultado_Baseline1ValueDescription = "";
         AV70FilterContagemResultado_BaselineValueDescription = "";
         AV91ContagemResultado_Descricao1 = "";
         AV92ContagemResultado_Descricao = "";
         AV148ContagemResultado_Agrupador1 = "";
         AV149ContagemResultado_Agrupador = "";
         AV27DynamicFiltersSelector2 = "";
         AV84ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV85ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV30ContagemResultado_OsFsOsFm2 = "";
         AV150ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV151ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV152ContagemResultado_DataAceite2 = DateTime.MinValue;
         AV153ContagemResultado_DataAceite_To2 = DateTime.MinValue;
         AV56ContagemResultado_LoteAceite2 = "";
         AV67ContagemResultado_Baseline2 = "";
         AV72FilterContagemResultado_Baseline2ValueDescription = "";
         AV93ContagemResultado_Descricao2 = "";
         AV155ContagemResultado_Agrupador2 = "";
         AV35DynamicFiltersSelector3 = "";
         AV86ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV87ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV38ContagemResultado_OsFsOsFm3 = "";
         AV156ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV157ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV158ContagemResultado_DataAceite3 = DateTime.MinValue;
         AV159ContagemResultado_DataAceite_To3 = DateTime.MinValue;
         AV59ContagemResultado_LoteAceite3 = "";
         AV68ContagemResultado_Baseline3 = "";
         AV73FilterContagemResultado_Baseline3ValueDescription = "";
         AV94ContagemResultado_Descricao3 = "";
         AV161ContagemResultado_Agrupador3 = "";
         AV118TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV119TFContagemResultado_StatusDmn_Sel = "";
         AV117TFContagemResultado_StatusDmn_SelDscs = "";
         AV135FilterTFContagemResultado_StatusDmn_SelValueDescription = "";
         AV136FilterTFContagemResultado_Baseline_SelValueDescription = "";
         AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn = "";
         AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = "";
         AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = "";
         AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 = DateTime.MinValue;
         AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 = DateTime.MinValue;
         AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = "";
         AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 = "";
         AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = "";
         AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 = "";
         AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = "";
         AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = "";
         AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 = DateTime.MinValue;
         AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 = DateTime.MinValue;
         AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = "";
         AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 = "";
         AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = "";
         AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 = "";
         AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = "";
         AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = "";
         AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 = DateTime.MinValue;
         AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 = DateTime.MinValue;
         AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = "";
         AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 = "";
         AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = "";
         AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 = "";
         AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = "";
         AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel = "";
         AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = "";
         AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel = "";
         AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = "";
         AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel = "";
         AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite = (DateTime)(DateTime.MinValue);
         AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to = (DateTime)(DateTime.MinValue);
         AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = "";
         AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel = "";
         AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = "";
         AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel = "";
         AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = "";
         AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = "";
         lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = "";
         lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = "";
         lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = "";
         lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = "";
         lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = "";
         lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = "";
         lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = "";
         lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = "";
         lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = "";
         lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = "";
         lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = "";
         lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = "";
         lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = "";
         lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A529ContagemResultado_DataAceite = (DateTime)(DateTime.MinValue);
         A528ContagemResultado_LoteAceite = "";
         A494ContagemResultado_Descricao = "";
         A1046ContagemResultado_Agrupador = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00333_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00333_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00333_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00333_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00333_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00333_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00333_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00333_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00333_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00333_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00333_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00333_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00333_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         P00333_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         P00333_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00333_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00333_A494ContagemResultado_Descricao = new String[] {""} ;
         P00333_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00333_A601ContagemResultado_Servico = new int[1] ;
         P00333_n601ContagemResultado_Servico = new bool[] {false} ;
         P00333_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00333_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00333_A528ContagemResultado_LoteAceite = new String[] {""} ;
         P00333_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         P00333_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00333_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00333_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00333_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00333_A489ContagemResultado_SistemaCod = new int[1] ;
         P00333_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00333_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         P00333_n529ContagemResultado_DataAceite = new bool[] {false} ;
         P00333_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00333_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00333_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00333_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00333_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00333_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00333_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00333_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00333_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00333_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00333_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00333_A457ContagemResultado_Demanda = new String[] {""} ;
         P00333_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00333_A456ContagemResultado_Codigo = new int[1] ;
         P00333_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00333_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A501ContagemResultado_OsFsOsFm = "";
         AV18ContagemResultado_StatusDmnDescription = "";
         AV65ContagemResultado_BaselineDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportreportextrawwcontagemresultadowwaceitefaturamento__default(),
            new Object[][] {
                new Object[] {
               P00333_A597ContagemResultado_LoteAceiteCod, P00333_n597ContagemResultado_LoteAceiteCod, P00333_A1553ContagemResultado_CntSrvCod, P00333_n1553ContagemResultado_CntSrvCod, P00333_A1583ContagemResultado_TipoRegistro, P00333_A517ContagemResultado_Ultima, P00333_A512ContagemResultado_ValorPF, P00333_n512ContagemResultado_ValorPF, P00333_A801ContagemResultado_ServicoSigla, P00333_n801ContagemResultado_ServicoSigla,
               P00333_A509ContagemrResultado_SistemaSigla, P00333_n509ContagemrResultado_SistemaSigla, P00333_A866ContagemResultado_ContratadaOrigemSigla, P00333_n866ContagemResultado_ContratadaOrigemSigla, P00333_A1046ContagemResultado_Agrupador, P00333_n1046ContagemResultado_Agrupador, P00333_A494ContagemResultado_Descricao, P00333_n494ContagemResultado_Descricao, P00333_A601ContagemResultado_Servico, P00333_n601ContagemResultado_Servico,
               P00333_A598ContagemResultado_Baseline, P00333_n598ContagemResultado_Baseline, P00333_A528ContagemResultado_LoteAceite, P00333_n528ContagemResultado_LoteAceite, P00333_A468ContagemResultado_NaoCnfDmnCod, P00333_n468ContagemResultado_NaoCnfDmnCod, P00333_A805ContagemResultado_ContratadaOrigemCod, P00333_n805ContagemResultado_ContratadaOrigemCod, P00333_A489ContagemResultado_SistemaCod, P00333_n489ContagemResultado_SistemaCod,
               P00333_A529ContagemResultado_DataAceite, P00333_n529ContagemResultado_DataAceite, P00333_A471ContagemResultado_DataDmn, P00333_A484ContagemResultado_StatusDmn, P00333_n484ContagemResultado_StatusDmn, P00333_A490ContagemResultado_ContratadaCod, P00333_n490ContagemResultado_ContratadaCod, P00333_A52Contratada_AreaTrabalhoCod, P00333_n52Contratada_AreaTrabalhoCod, P00333_A566ContagemResultado_DataUltCnt,
               P00333_A531ContagemResultado_StatusUltCnt, P00333_A493ContagemResultado_DemandaFM, P00333_n493ContagemResultado_DemandaFM, P00333_A457ContagemResultado_Demanda, P00333_n457ContagemResultado_Demanda, P00333_A456ContagemResultado_Codigo, P00333_A473ContagemResultado_DataCnt, P00333_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV14ContagemResultado_StatusCnt ;
      private short AV120TFContagemResultado_Baseline_Sel ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV52DynamicFiltersOperator1 ;
      private short AV55DynamicFiltersOperator2 ;
      private short AV58DynamicFiltersOperator3 ;
      private short AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_3_Contagemresultado_statuscnt ;
      private short AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ;
      private short AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ;
      private short AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ;
      private short AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV12Contratada_AreaTrabalhoCod ;
      private int AV90Contratada_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int AV23ContagemResultado_SistemaCod1 ;
      private int AV48ContagemResultado_SistemaCod ;
      private int AV24ContagemResultado_ContratadaCod1 ;
      private int AV49ContagemResultado_ContratadaCod ;
      private int AV146ContagemResultado_ContratadaOrigemCod1 ;
      private int AV147ContagemResultado_ContratadaOrigemCod ;
      private int AV25ContagemResultado_NaoCnfDmnCod1 ;
      private int AV50ContagemResultado_NaoCnfDmnCod ;
      private int AV78ContagemResultado_Servico1 ;
      private int AV81ContagemResultado_Servico ;
      private int AV31ContagemResultado_SistemaCod2 ;
      private int AV32ContagemResultado_ContratadaCod2 ;
      private int AV154ContagemResultado_ContratadaOrigemCod2 ;
      private int AV33ContagemResultado_NaoCnfDmnCod2 ;
      private int AV79ContagemResultado_Servico2 ;
      private int AV39ContagemResultado_SistemaCod3 ;
      private int AV40ContagemResultado_ContratadaCod3 ;
      private int AV160ContagemResultado_ContratadaOrigemCod3 ;
      private int AV41ContagemResultado_NaoCnfDmnCod3 ;
      private int AV80ContagemResultado_Servico3 ;
      private int AV168GXV1 ;
      private int AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ;
      private int AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_2_Contratada_codigo ;
      private int AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ;
      private int AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ;
      private int AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ;
      private int AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ;
      private int AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ;
      private int AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ;
      private int AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ;
      private int AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ;
      private int AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ;
      private int AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ;
      private int AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ;
      private int AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ;
      private int AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ;
      private int AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ;
      private int AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV43GridState_gxTpr_Dynamicfilters_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private long AV54ContagemResultado_DemandaFM_ORDER1 ;
      private long AV64ContagemResultado_DemandaFM_ORDER ;
      private long AV57ContagemResultado_DemandaFM_ORDER2 ;
      private long AV60ContagemResultado_DemandaFM_ORDER3 ;
      private long AV137i ;
      private long AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ;
      private long AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ;
      private long AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ;
      private long A553ContagemResultado_DemandaFM_ORDER ;
      private decimal AV131TFContagemResultado_PFFinal ;
      private decimal AV132TFContagemResultado_PFFinal_To ;
      private decimal AV133TFContagemResultado_ValorPF ;
      private decimal AV134TFContagemResultado_ValorPF_To ;
      private decimal AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ;
      private decimal AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ;
      private decimal AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ;
      private decimal AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV16ContagemResultado_StatusDmn ;
      private String AV102TFContagemResultado_LoteAceite ;
      private String AV103TFContagemResultado_LoteAceite_Sel ;
      private String AV112TFContagemResultado_ContratadaOrigemSigla ;
      private String AV113TFContagemResultado_ContratadaOrigemSigla_Sel ;
      private String AV114TFContagemrResultado_SistemaSigla ;
      private String AV115TFContagemrResultado_SistemaSigla_Sel ;
      private String AV121TFContagemResultado_ServicoSigla ;
      private String AV122TFContagemResultado_ServicoSigla_Sel ;
      private String AV53ContagemResultado_LoteAceite1 ;
      private String AV62ContagemResultado_LoteAceite ;
      private String AV66ContagemResultado_Baseline1 ;
      private String AV148ContagemResultado_Agrupador1 ;
      private String AV149ContagemResultado_Agrupador ;
      private String AV56ContagemResultado_LoteAceite2 ;
      private String AV67ContagemResultado_Baseline2 ;
      private String AV155ContagemResultado_Agrupador2 ;
      private String AV59ContagemResultado_LoteAceite3 ;
      private String AV68ContagemResultado_Baseline3 ;
      private String AV161ContagemResultado_Agrupador3 ;
      private String AV119TFContagemResultado_StatusDmn_Sel ;
      private String AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn ;
      private String AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ;
      private String AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ;
      private String AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ;
      private String AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ;
      private String AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ;
      private String AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ;
      private String AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ;
      private String AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ;
      private String AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ;
      private String AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ;
      private String AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ;
      private String AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ;
      private String AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ;
      private String AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ;
      private String AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ;
      private String AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ;
      private String lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ;
      private String lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ;
      private String lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ;
      private String lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ;
      private String lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ;
      private String lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A528ContagemResultado_LoteAceite ;
      private String A1046ContagemResultado_Agrupador ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV108TFContagemResultado_DataAceite ;
      private DateTime AV109TFContagemResultado_DataAceite_To ;
      private DateTime AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ;
      private DateTime AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ;
      private DateTime A529ContagemResultado_DataAceite ;
      private DateTime AV110TFContagemResultado_DataUltCnt ;
      private DateTime AV111TFContagemResultado_DataUltCnt_To ;
      private DateTime AV82ContagemResultado_DataUltCnt1 ;
      private DateTime AV83ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV89ContagemResultado_DataUltCnt ;
      private DateTime AV138ContagemResultado_DataDmn1 ;
      private DateTime AV139ContagemResultado_DataDmn_To1 ;
      private DateTime AV141ContagemResultado_DataDmn ;
      private DateTime AV142ContagemResultado_DataAceite1 ;
      private DateTime AV143ContagemResultado_DataAceite_To1 ;
      private DateTime AV145ContagemResultado_DataAceite ;
      private DateTime AV84ContagemResultado_DataUltCnt2 ;
      private DateTime AV85ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV150ContagemResultado_DataDmn2 ;
      private DateTime AV151ContagemResultado_DataDmn_To2 ;
      private DateTime AV152ContagemResultado_DataAceite2 ;
      private DateTime AV153ContagemResultado_DataAceite_To2 ;
      private DateTime AV86ContagemResultado_DataUltCnt3 ;
      private DateTime AV87ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV156ContagemResultado_DataDmn3 ;
      private DateTime AV157ContagemResultado_DataDmn_To3 ;
      private DateTime AV158ContagemResultado_DataAceite3 ;
      private DateTime AV159ContagemResultado_DataAceite_To3 ;
      private DateTime AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ;
      private DateTime AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ;
      private DateTime AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ;
      private DateTime AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ;
      private DateTime AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ;
      private DateTime AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ;
      private DateTime AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ;
      private DateTime AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ;
      private DateTime AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ;
      private DateTime AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ;
      private DateTime AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ;
      private DateTime AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ;
      private DateTime AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ;
      private DateTime AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ;
      private DateTime AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ;
      private DateTime AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ;
      private DateTime AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool AV26DynamicFiltersEnabled2 ;
      private bool AV34DynamicFiltersEnabled3 ;
      private bool AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ;
      private bool AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ;
      private bool A598ContagemResultado_Baseline ;
      private bool A517ContagemResultado_Ultima ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n494ContagemResultado_Descricao ;
      private bool n601ContagemResultado_Servico ;
      private bool n598ContagemResultado_Baseline ;
      private bool n528ContagemResultado_LoteAceite ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n529ContagemResultado_DataAceite ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private String AV116TFContagemResultado_StatusDmn_SelsJson ;
      private String AV42GridStateXML ;
      private String AV104TFContagemResultado_OsFsOsFm ;
      private String AV105TFContagemResultado_OsFsOsFm_Sel ;
      private String AV106TFContagemResultado_Descricao ;
      private String AV107TFContagemResultado_Descricao_Sel ;
      private String AV13FilterContagemResultado_StatusCntValueDescription ;
      private String AV15FilterContagemResultado_StatusDmnValueDescription ;
      private String AV19DynamicFiltersSelector1 ;
      private String AV88FilterContagemResultado_DataUltCntDescription ;
      private String AV22ContagemResultado_OsFsOsFm1 ;
      private String AV95FilterContagemResultado_OsFsOsFmDescription ;
      private String AV47ContagemResultado_OsFsOsFm ;
      private String AV140FilterContagemResultado_DataDmnDescription ;
      private String AV144FilterContagemResultado_DataAceiteDescription ;
      private String AV71FilterContagemResultado_Baseline1ValueDescription ;
      private String AV70FilterContagemResultado_BaselineValueDescription ;
      private String AV91ContagemResultado_Descricao1 ;
      private String AV92ContagemResultado_Descricao ;
      private String AV27DynamicFiltersSelector2 ;
      private String AV30ContagemResultado_OsFsOsFm2 ;
      private String AV72FilterContagemResultado_Baseline2ValueDescription ;
      private String AV93ContagemResultado_Descricao2 ;
      private String AV35DynamicFiltersSelector3 ;
      private String AV38ContagemResultado_OsFsOsFm3 ;
      private String AV73FilterContagemResultado_Baseline3ValueDescription ;
      private String AV94ContagemResultado_Descricao3 ;
      private String AV117TFContagemResultado_StatusDmn_SelDscs ;
      private String AV135FilterTFContagemResultado_StatusDmn_SelValueDescription ;
      private String AV136FilterTFContagemResultado_Baseline_SelValueDescription ;
      private String AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ;
      private String AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ;
      private String AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ;
      private String AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ;
      private String AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ;
      private String AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ;
      private String AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ;
      private String AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ;
      private String AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ;
      private String AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ;
      private String AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ;
      private String AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ;
      private String AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ;
      private String lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ;
      private String lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ;
      private String lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ;
      private String lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ;
      private String lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ;
      private String lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ;
      private String lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ;
      private String lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV18ContagemResultado_StatusDmnDescription ;
      private String AV65ContagemResultado_BaselineDescription ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00333_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00333_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00333_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00333_n1553ContagemResultado_CntSrvCod ;
      private short[] P00333_A1583ContagemResultado_TipoRegistro ;
      private bool[] P00333_A517ContagemResultado_Ultima ;
      private decimal[] P00333_A512ContagemResultado_ValorPF ;
      private bool[] P00333_n512ContagemResultado_ValorPF ;
      private String[] P00333_A801ContagemResultado_ServicoSigla ;
      private bool[] P00333_n801ContagemResultado_ServicoSigla ;
      private String[] P00333_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00333_n509ContagemrResultado_SistemaSigla ;
      private String[] P00333_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] P00333_n866ContagemResultado_ContratadaOrigemSigla ;
      private String[] P00333_A1046ContagemResultado_Agrupador ;
      private bool[] P00333_n1046ContagemResultado_Agrupador ;
      private String[] P00333_A494ContagemResultado_Descricao ;
      private bool[] P00333_n494ContagemResultado_Descricao ;
      private int[] P00333_A601ContagemResultado_Servico ;
      private bool[] P00333_n601ContagemResultado_Servico ;
      private bool[] P00333_A598ContagemResultado_Baseline ;
      private bool[] P00333_n598ContagemResultado_Baseline ;
      private String[] P00333_A528ContagemResultado_LoteAceite ;
      private bool[] P00333_n528ContagemResultado_LoteAceite ;
      private int[] P00333_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00333_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00333_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00333_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P00333_A489ContagemResultado_SistemaCod ;
      private bool[] P00333_n489ContagemResultado_SistemaCod ;
      private DateTime[] P00333_A529ContagemResultado_DataAceite ;
      private bool[] P00333_n529ContagemResultado_DataAceite ;
      private DateTime[] P00333_A471ContagemResultado_DataDmn ;
      private String[] P00333_A484ContagemResultado_StatusDmn ;
      private bool[] P00333_n484ContagemResultado_StatusDmn ;
      private int[] P00333_A490ContagemResultado_ContratadaCod ;
      private bool[] P00333_n490ContagemResultado_ContratadaCod ;
      private int[] P00333_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00333_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] P00333_A566ContagemResultado_DataUltCnt ;
      private short[] P00333_A531ContagemResultado_StatusUltCnt ;
      private String[] P00333_A493ContagemResultado_DemandaFM ;
      private bool[] P00333_n493ContagemResultado_DemandaFM ;
      private String[] P00333_A457ContagemResultado_Demanda ;
      private bool[] P00333_n457ContagemResultado_Demanda ;
      private int[] P00333_A456ContagemResultado_Codigo ;
      private DateTime[] P00333_A473ContagemResultado_DataCnt ;
      private String[] P00333_A511ContagemResultado_HoraCnt ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV118TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV43GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV44GridStateDynamicFilter ;
   }

   public class aexportreportextrawwcontagemresultadowwaceitefaturamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00333( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ,
                                             short AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ,
                                             String AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ,
                                             DateTime AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ,
                                             DateTime AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ,
                                             DateTime AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ,
                                             DateTime AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ,
                                             int AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ,
                                             int AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ,
                                             int AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ,
                                             int AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ,
                                             int AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ,
                                             String AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ,
                                             long AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ,
                                             String AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ,
                                             int AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ,
                                             String AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ,
                                             String AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ,
                                             bool AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ,
                                             String AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ,
                                             short AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ,
                                             String AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ,
                                             DateTime AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ,
                                             DateTime AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ,
                                             DateTime AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ,
                                             DateTime AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ,
                                             int AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ,
                                             int AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ,
                                             int AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ,
                                             int AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ,
                                             String AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ,
                                             long AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ,
                                             String AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ,
                                             int AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ,
                                             String AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ,
                                             String AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ,
                                             bool AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ,
                                             String AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ,
                                             short AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ,
                                             String AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ,
                                             DateTime AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ,
                                             DateTime AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ,
                                             DateTime AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ,
                                             DateTime AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ,
                                             int AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ,
                                             int AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ,
                                             int AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ,
                                             int AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ,
                                             String AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ,
                                             long AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ,
                                             String AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ,
                                             int AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ,
                                             String AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ,
                                             String AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ,
                                             String AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ,
                                             String AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ,
                                             String AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ,
                                             String AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ,
                                             String AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ,
                                             DateTime AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ,
                                             DateTime AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ,
                                             String AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ,
                                             String AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ,
                                             String AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ,
                                             int AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ,
                                             String AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ,
                                             String AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ,
                                             decimal AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ,
                                             decimal AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ,
                                             int AV43GridState_gxTpr_Dynamicfilters_Count ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A529ContagemResultado_DataAceite ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A528ContagemResultado_LoteAceite ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A494ContagemResultado_Descricao ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A866ContagemResultado_ContratadaOrigemSigla ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV10OrderedBy ,
                                             DateTime AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ,
                                             DateTime AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ,
                                             DateTime AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ,
                                             DateTime AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ,
                                             DateTime AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ,
                                             DateTime AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV251ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV252ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             bool A517ContagemResultado_Ultima ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [101] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_ValorPF], T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T7.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_Descricao], T4.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_Baseline], T3.[Lote_Numero] AS ContagemResultado_LoteAceite, T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T3.[Lote_Data] AS ContagemResultado_DataAceite, T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T8.[Contratada_AreaTrabalhoCod], COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T9.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Lote] T3 WITH (NOLOCK) ON T3.[Lote_Codigo] = T2.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo]";
         scmdbuf = scmdbuf + " = T4.[Servico_Codigo]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Sistema] T7 WITH (NOLOCK) ON T7.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_StatusDmn] = 'O' or T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L')";
         scmdbuf = scmdbuf + " and (Not ( @AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = 1 and @AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = 1 and @AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = 1 and @AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = 1 and @AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and ((@AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (COALESCE( T9.[ContagemResultado_StatusUltCnt], 0) = 5)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[28] = 1;
            GXv_int2[29] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[30] = 1;
            GXv_int2[31] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[32] = 1;
            GXv_int2[33] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1))";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 > 0 ) && ( AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1)";
         }
         else
         {
            GXv_int2[40] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int2[41] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 + '%')";
         }
         else
         {
            GXv_int2[42] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1)";
         }
         else
         {
            GXv_int2[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int2[44] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int2[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[46] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[47] = 1;
            GXv_int2[48] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[49] = 1;
            GXv_int2[50] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[51] = 1;
            GXv_int2[52] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[53] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[54] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2)";
         }
         else
         {
            GXv_int2[55] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2))";
         }
         else
         {
            GXv_int2[56] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int2[57] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 > 0 ) && ( AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int2[58] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2)";
         }
         else
         {
            GXv_int2[59] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int2[60] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 + '%')";
         }
         else
         {
            GXv_int2[61] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2)";
         }
         else
         {
            GXv_int2[62] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int2[63] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int2[64] = 1;
         }
         if ( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[65] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[66] = 1;
            GXv_int2[67] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[68] = 1;
            GXv_int2[69] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[70] = 1;
            GXv_int2[71] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[72] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[73] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3)";
         }
         else
         {
            GXv_int2[74] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3))";
         }
         else
         {
            GXv_int2[75] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int2[76] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 > 0 ) && ( AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int2[77] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3)";
         }
         else
         {
            GXv_int2[78] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int2[79] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 + '%')";
         }
         else
         {
            GXv_int2[80] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3)";
         }
         else
         {
            GXv_int2[81] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV229ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int2[82] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int2[83] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[84] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite)";
         }
         else
         {
            GXv_int2[85] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] = @AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)";
         }
         else
         {
            GXv_int2[86] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int2[87] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int2[88] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int2[89] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int2[90] = 1;
         }
         if ( ! (DateTime.MinValue==AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite)";
         }
         else
         {
            GXv_int2[91] = 1;
         }
         if ( ! (DateTime.MinValue==AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] <= @AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to)";
         }
         else
         {
            GXv_int2[92] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla)";
         }
         else
         {
            GXv_int2[93] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)";
         }
         else
         {
            GXv_int2[94] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] like @lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int2[95] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] = @AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int2[96] = 1;
         }
         if ( AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV247ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels, "T2.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV248ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int2[97] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int2[98] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] >= @AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int2[99] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] <= @AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int2[100] = 1;
         }
         if ( AV43GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV10OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[ContagemResultado_DataUltCnt]";
         }
         else if ( AV10OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00333(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (long)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (long)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (String)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (String)dynConstraints[51] , (long)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (String)dynConstraints[67] , (String)dynConstraints[68] , (int)dynConstraints[69] , (short)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (decimal)dynConstraints[73] , (decimal)dynConstraints[74] , (int)dynConstraints[75] , (int)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (bool)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (String)dynConstraints[89] , (String)dynConstraints[90] , (String)dynConstraints[91] , (decimal)dynConstraints[92] , (int)dynConstraints[93] , (short)dynConstraints[94] , (DateTime)dynConstraints[95] , (DateTime)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (short)dynConstraints[109] , (bool)dynConstraints[110] , (short)dynConstraints[111] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00333 ;
          prmP00333 = new Object[] {
          new Object[] {"@AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1",SqlDbType.Char,10,0} ,
          new Object[] {"@AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2",SqlDbType.Char,10,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV224ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV227ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3",SqlDbType.Char,10,0} ,
          new Object[] {"@AV228ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV231ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV233ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite",SqlDbType.Char,10,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV235ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV236ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV237ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV240ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV243ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV249ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV254ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00333", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00333,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 25) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 10) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[30])[0] = rslt.getGXDateTime(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((int[]) buf[35])[0] = rslt.getInt(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[39])[0] = rslt.getGXDate(22) ;
                ((short[]) buf[40])[0] = rslt.getShort(23) ;
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((int[]) buf[45])[0] = rslt.getInt(26) ;
                ((DateTime[]) buf[46])[0] = rslt.getGXDate(27) ;
                ((String[]) buf[47])[0] = rslt.getString(28, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[102]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[103]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[107]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[111]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[114]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[115]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[119]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[121]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[122]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[127]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[128]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[135]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[139]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[140]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[141]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[142]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[144]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[146]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[149]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[150]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[153]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[155]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[160]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[161]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[162]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[163]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[164]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[165]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[166]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[168]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[169]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[170]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[172]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[177]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[180]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[182]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[183]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[186]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[192]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[195]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[200]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[201]);
                }
                return;
       }
    }

 }

}
