/*
               File: WS_Meetrika_Integracao_OLD
        Description: Web Service de Integração entre sistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 7/11/2019 2:23:42.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ws_meetrika_integracao_old : GXProcedure
   {
      public ws_meetrika_integracao_old( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public ws_meetrika_integracao_old( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         ws_meetrika_integracao_old objws_meetrika_integracao_old;
         objws_meetrika_integracao_old = new ws_meetrika_integracao_old();
         objws_meetrika_integracao_old.context.SetSubmitInitialConfig(context);
         objws_meetrika_integracao_old.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objws_meetrika_integracao_old);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((ws_meetrika_integracao_old)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public void gxep_consultardemandas( SdtSDT_WSAutenticacao aP0_Autenticacao ,
                                          SdtSDT_WsFiltros aP1_Filtros ,
                                          out String aP2_Retorno )
      {
         this.AV8Autenticacao = aP0_Autenticacao;
         this.AV44Filtros = aP1_Filtros;
         this.AV10Retorno = "" ;
         initialize();
         initialized = 1;
         /* ConsultarDemandas Constructor */
         AV10Retorno = AV8Autenticacao.ToJSonString(false);
         AV10Retorno = AV10Retorno + StringUtil.RTrim( context.localUtil.Format( AV23Status, ""));
         executePrivate();
         aP2_Retorno=this.AV10Retorno;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10Retorno = "";
         AV23Status = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short initialized ;
      private String AV23Status ;
      private String AV10Retorno ;
      private SdtSDT_WSAutenticacao AV8Autenticacao ;
      private SdtSDT_WsFiltros AV44Filtros ;
   }

}
