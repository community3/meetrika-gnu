/*
               File: GetPromptParametrosPlanilhasFilterData
        Description: Get Prompt Parametros Planilhas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:25.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptparametrosplanilhasfilterdata : GXProcedure
   {
      public getpromptparametrosplanilhasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptparametrosplanilhasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptparametrosplanilhasfilterdata objgetpromptparametrosplanilhasfilterdata;
         objgetpromptparametrosplanilhasfilterdata = new getpromptparametrosplanilhasfilterdata();
         objgetpromptparametrosplanilhasfilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptparametrosplanilhasfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptparametrosplanilhasfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptparametrosplanilhasfilterdata.AV26OptionsJson = "" ;
         objgetpromptparametrosplanilhasfilterdata.AV29OptionsDescJson = "" ;
         objgetpromptparametrosplanilhasfilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptparametrosplanilhasfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptparametrosplanilhasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptparametrosplanilhasfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptparametrosplanilhasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_PARAMETROSPLN_CAMPO") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_CAMPOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_PARAMETROSPLN_COLUNA") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_COLUNAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptParametrosPlanilhasGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptParametrosPlanilhasGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptParametrosPlanilhasGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CODIGO") == 0 )
            {
               AV10TFParametrosPln_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFParametrosPln_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_AREATRABALHOCOD") == 0 )
            {
               AV12TFParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV13TFParametrosPln_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO") == 0 )
            {
               AV14TFParametrosPln_Campo = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO_SEL") == 0 )
            {
               AV15TFParametrosPln_Campo_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA") == 0 )
            {
               AV16TFParametrosPln_Coluna = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA_SEL") == 0 )
            {
               AV17TFParametrosPln_Coluna_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_LINHA") == 0 )
            {
               AV18TFParametrosPln_Linha = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV19TFParametrosPln_Linha_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 )
            {
               AV39ParametrosPln_Campo1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 )
               {
                  AV42ParametrosPln_Campo2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 )
                  {
                     AV45ParametrosPln_Campo3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPARAMETROSPLN_CAMPOOPTIONS' Routine */
         AV14TFParametrosPln_Campo = AV20SearchTxt;
         AV15TFParametrosPln_Campo_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ParametrosPln_Campo1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ParametrosPln_Campo2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ParametrosPln_Campo3 ,
                                              AV10TFParametrosPln_Codigo ,
                                              AV11TFParametrosPln_Codigo_To ,
                                              AV12TFParametrosPln_AreaTrabalhoCod ,
                                              AV13TFParametrosPln_AreaTrabalhoCod_To ,
                                              AV15TFParametrosPln_Campo_Sel ,
                                              AV14TFParametrosPln_Campo ,
                                              AV17TFParametrosPln_Coluna_Sel ,
                                              AV16TFParametrosPln_Coluna ,
                                              AV18TFParametrosPln_Linha ,
                                              AV19TFParametrosPln_Linha_To ,
                                              A849ParametrosPln_Campo ,
                                              A848ParametrosPln_Codigo ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV39ParametrosPln_Campo1 = StringUtil.Concat( StringUtil.RTrim( AV39ParametrosPln_Campo1), "%", "");
         lV42ParametrosPln_Campo2 = StringUtil.Concat( StringUtil.RTrim( AV42ParametrosPln_Campo2), "%", "");
         lV45ParametrosPln_Campo3 = StringUtil.Concat( StringUtil.RTrim( AV45ParametrosPln_Campo3), "%", "");
         lV14TFParametrosPln_Campo = StringUtil.Concat( StringUtil.RTrim( AV14TFParametrosPln_Campo), "%", "");
         lV16TFParametrosPln_Coluna = StringUtil.PadR( StringUtil.RTrim( AV16TFParametrosPln_Coluna), 2, "%");
         /* Using cursor P00OO2 */
         pr_default.execute(0, new Object[] {lV39ParametrosPln_Campo1, lV42ParametrosPln_Campo2, lV45ParametrosPln_Campo3, AV10TFParametrosPln_Codigo, AV11TFParametrosPln_Codigo_To, AV12TFParametrosPln_AreaTrabalhoCod, AV13TFParametrosPln_AreaTrabalhoCod_To, lV14TFParametrosPln_Campo, AV15TFParametrosPln_Campo_Sel, lV16TFParametrosPln_Coluna, AV17TFParametrosPln_Coluna_Sel, AV18TFParametrosPln_Linha, AV19TFParametrosPln_Linha_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOO2 = false;
            A849ParametrosPln_Campo = P00OO2_A849ParametrosPln_Campo[0];
            A851ParametrosPln_Linha = P00OO2_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = P00OO2_A850ParametrosPln_Coluna[0];
            A847ParametrosPln_AreaTrabalhoCod = P00OO2_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00OO2_n847ParametrosPln_AreaTrabalhoCod[0];
            A848ParametrosPln_Codigo = P00OO2_A848ParametrosPln_Codigo[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00OO2_A849ParametrosPln_Campo[0], A849ParametrosPln_Campo) == 0 ) )
            {
               BRKOO2 = false;
               A848ParametrosPln_Codigo = P00OO2_A848ParametrosPln_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKOO2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A849ParametrosPln_Campo)) )
            {
               AV24Option = A849ParametrosPln_Campo;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOO2 )
            {
               BRKOO2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADPARAMETROSPLN_COLUNAOPTIONS' Routine */
         AV16TFParametrosPln_Coluna = AV20SearchTxt;
         AV17TFParametrosPln_Coluna_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ParametrosPln_Campo1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ParametrosPln_Campo2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ParametrosPln_Campo3 ,
                                              AV10TFParametrosPln_Codigo ,
                                              AV11TFParametrosPln_Codigo_To ,
                                              AV12TFParametrosPln_AreaTrabalhoCod ,
                                              AV13TFParametrosPln_AreaTrabalhoCod_To ,
                                              AV15TFParametrosPln_Campo_Sel ,
                                              AV14TFParametrosPln_Campo ,
                                              AV17TFParametrosPln_Coluna_Sel ,
                                              AV16TFParametrosPln_Coluna ,
                                              AV18TFParametrosPln_Linha ,
                                              AV19TFParametrosPln_Linha_To ,
                                              A849ParametrosPln_Campo ,
                                              A848ParametrosPln_Codigo ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV39ParametrosPln_Campo1 = StringUtil.Concat( StringUtil.RTrim( AV39ParametrosPln_Campo1), "%", "");
         lV42ParametrosPln_Campo2 = StringUtil.Concat( StringUtil.RTrim( AV42ParametrosPln_Campo2), "%", "");
         lV45ParametrosPln_Campo3 = StringUtil.Concat( StringUtil.RTrim( AV45ParametrosPln_Campo3), "%", "");
         lV14TFParametrosPln_Campo = StringUtil.Concat( StringUtil.RTrim( AV14TFParametrosPln_Campo), "%", "");
         lV16TFParametrosPln_Coluna = StringUtil.PadR( StringUtil.RTrim( AV16TFParametrosPln_Coluna), 2, "%");
         /* Using cursor P00OO3 */
         pr_default.execute(1, new Object[] {lV39ParametrosPln_Campo1, lV42ParametrosPln_Campo2, lV45ParametrosPln_Campo3, AV10TFParametrosPln_Codigo, AV11TFParametrosPln_Codigo_To, AV12TFParametrosPln_AreaTrabalhoCod, AV13TFParametrosPln_AreaTrabalhoCod_To, lV14TFParametrosPln_Campo, AV15TFParametrosPln_Campo_Sel, lV16TFParametrosPln_Coluna, AV17TFParametrosPln_Coluna_Sel, AV18TFParametrosPln_Linha, AV19TFParametrosPln_Linha_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOO4 = false;
            A850ParametrosPln_Coluna = P00OO3_A850ParametrosPln_Coluna[0];
            A851ParametrosPln_Linha = P00OO3_A851ParametrosPln_Linha[0];
            A847ParametrosPln_AreaTrabalhoCod = P00OO3_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00OO3_n847ParametrosPln_AreaTrabalhoCod[0];
            A848ParametrosPln_Codigo = P00OO3_A848ParametrosPln_Codigo[0];
            A849ParametrosPln_Campo = P00OO3_A849ParametrosPln_Campo[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OO3_A850ParametrosPln_Coluna[0], A850ParametrosPln_Coluna) == 0 ) )
            {
               BRKOO4 = false;
               A848ParametrosPln_Codigo = P00OO3_A848ParametrosPln_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKOO4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A850ParametrosPln_Coluna)) )
            {
               AV24Option = A850ParametrosPln_Coluna;
               AV27OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!")));
               AV25Options.Add(AV24Option, 0);
               AV28OptionsDesc.Add(AV27OptionDesc, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOO4 )
            {
               BRKOO4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFParametrosPln_Campo = "";
         AV15TFParametrosPln_Campo_Sel = "";
         AV16TFParametrosPln_Coluna = "";
         AV17TFParametrosPln_Coluna_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV39ParametrosPln_Campo1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42ParametrosPln_Campo2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45ParametrosPln_Campo3 = "";
         scmdbuf = "";
         lV39ParametrosPln_Campo1 = "";
         lV42ParametrosPln_Campo2 = "";
         lV45ParametrosPln_Campo3 = "";
         lV14TFParametrosPln_Campo = "";
         lV16TFParametrosPln_Coluna = "";
         A849ParametrosPln_Campo = "";
         A850ParametrosPln_Coluna = "";
         P00OO2_A849ParametrosPln_Campo = new String[] {""} ;
         P00OO2_A851ParametrosPln_Linha = new int[1] ;
         P00OO2_A850ParametrosPln_Coluna = new String[] {""} ;
         P00OO2_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00OO2_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00OO2_A848ParametrosPln_Codigo = new int[1] ;
         AV24Option = "";
         P00OO3_A850ParametrosPln_Coluna = new String[] {""} ;
         P00OO3_A851ParametrosPln_Linha = new int[1] ;
         P00OO3_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00OO3_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00OO3_A848ParametrosPln_Codigo = new int[1] ;
         P00OO3_A849ParametrosPln_Campo = new String[] {""} ;
         AV27OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptparametrosplanilhasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OO2_A849ParametrosPln_Campo, P00OO2_A851ParametrosPln_Linha, P00OO2_A850ParametrosPln_Coluna, P00OO2_A847ParametrosPln_AreaTrabalhoCod, P00OO2_n847ParametrosPln_AreaTrabalhoCod, P00OO2_A848ParametrosPln_Codigo
               }
               , new Object[] {
               P00OO3_A850ParametrosPln_Coluna, P00OO3_A851ParametrosPln_Linha, P00OO3_A847ParametrosPln_AreaTrabalhoCod, P00OO3_n847ParametrosPln_AreaTrabalhoCod, P00OO3_A848ParametrosPln_Codigo, P00OO3_A849ParametrosPln_Campo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV48GXV1 ;
      private int AV10TFParametrosPln_Codigo ;
      private int AV11TFParametrosPln_Codigo_To ;
      private int AV12TFParametrosPln_AreaTrabalhoCod ;
      private int AV13TFParametrosPln_AreaTrabalhoCod_To ;
      private int AV18TFParametrosPln_Linha ;
      private int AV19TFParametrosPln_Linha_To ;
      private int A848ParametrosPln_Codigo ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private long AV32count ;
      private String AV16TFParametrosPln_Coluna ;
      private String AV17TFParametrosPln_Coluna_Sel ;
      private String scmdbuf ;
      private String lV16TFParametrosPln_Coluna ;
      private String A850ParametrosPln_Coluna ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool BRKOO2 ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private bool BRKOO4 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV14TFParametrosPln_Campo ;
      private String AV15TFParametrosPln_Campo_Sel ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV39ParametrosPln_Campo1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV42ParametrosPln_Campo2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV45ParametrosPln_Campo3 ;
      private String lV39ParametrosPln_Campo1 ;
      private String lV42ParametrosPln_Campo2 ;
      private String lV45ParametrosPln_Campo3 ;
      private String lV14TFParametrosPln_Campo ;
      private String A849ParametrosPln_Campo ;
      private String AV24Option ;
      private String AV27OptionDesc ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00OO2_A849ParametrosPln_Campo ;
      private int[] P00OO2_A851ParametrosPln_Linha ;
      private String[] P00OO2_A850ParametrosPln_Coluna ;
      private int[] P00OO2_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00OO2_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] P00OO2_A848ParametrosPln_Codigo ;
      private String[] P00OO3_A850ParametrosPln_Coluna ;
      private int[] P00OO3_A851ParametrosPln_Linha ;
      private int[] P00OO3_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00OO3_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] P00OO3_A848ParametrosPln_Codigo ;
      private String[] P00OO3_A849ParametrosPln_Campo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getpromptparametrosplanilhasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OO2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ParametrosPln_Campo1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ParametrosPln_Campo2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ParametrosPln_Campo3 ,
                                             int AV10TFParametrosPln_Codigo ,
                                             int AV11TFParametrosPln_Codigo_To ,
                                             int AV12TFParametrosPln_AreaTrabalhoCod ,
                                             int AV13TFParametrosPln_AreaTrabalhoCod_To ,
                                             String AV15TFParametrosPln_Campo_Sel ,
                                             String AV14TFParametrosPln_Campo ,
                                             String AV17TFParametrosPln_Coluna_Sel ,
                                             String AV16TFParametrosPln_Coluna ,
                                             int AV18TFParametrosPln_Linha ,
                                             int AV19TFParametrosPln_Linha_To ,
                                             String A849ParametrosPln_Campo ,
                                             int A848ParametrosPln_Codigo ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ParametrosPln_Campo], [ParametrosPln_Linha], [ParametrosPln_Coluna], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ParametrosPln_Campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV39ParametrosPln_Campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV39ParametrosPln_Campo1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ParametrosPln_Campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV42ParametrosPln_Campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV42ParametrosPln_Campo2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ParametrosPln_Campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV45ParametrosPln_Campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV45ParametrosPln_Campo3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV10TFParametrosPln_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Codigo] >= @AV10TFParametrosPln_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Codigo] >= @AV10TFParametrosPln_Codigo)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV11TFParametrosPln_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Codigo] <= @AV11TFParametrosPln_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Codigo] <= @AV11TFParametrosPln_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV12TFParametrosPln_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_AreaTrabalhoCod] >= @AV12TFParametrosPln_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_AreaTrabalhoCod] >= @AV12TFParametrosPln_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV13TFParametrosPln_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_AreaTrabalhoCod] <= @AV13TFParametrosPln_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_AreaTrabalhoCod] <= @AV13TFParametrosPln_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFParametrosPln_Campo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFParametrosPln_Campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like @lV14TFParametrosPln_Campo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like @lV14TFParametrosPln_Campo)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFParametrosPln_Campo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] = @AV15TFParametrosPln_Campo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] = @AV15TFParametrosPln_Campo_Sel)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFParametrosPln_Coluna_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFParametrosPln_Coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Coluna] like @lV16TFParametrosPln_Coluna)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Coluna] like @lV16TFParametrosPln_Coluna)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFParametrosPln_Coluna_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Coluna] = @AV17TFParametrosPln_Coluna_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Coluna] = @AV17TFParametrosPln_Coluna_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV18TFParametrosPln_Linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Linha] >= @AV18TFParametrosPln_Linha)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Linha] >= @AV18TFParametrosPln_Linha)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV19TFParametrosPln_Linha_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Linha] <= @AV19TFParametrosPln_Linha_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Linha] <= @AV19TFParametrosPln_Linha_To)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ParametrosPln_Campo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OO3( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ParametrosPln_Campo1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ParametrosPln_Campo2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ParametrosPln_Campo3 ,
                                             int AV10TFParametrosPln_Codigo ,
                                             int AV11TFParametrosPln_Codigo_To ,
                                             int AV12TFParametrosPln_AreaTrabalhoCod ,
                                             int AV13TFParametrosPln_AreaTrabalhoCod_To ,
                                             String AV15TFParametrosPln_Campo_Sel ,
                                             String AV14TFParametrosPln_Campo ,
                                             String AV17TFParametrosPln_Coluna_Sel ,
                                             String AV16TFParametrosPln_Coluna ,
                                             int AV18TFParametrosPln_Linha ,
                                             int AV19TFParametrosPln_Linha_To ,
                                             String A849ParametrosPln_Campo ,
                                             int A848ParametrosPln_Codigo ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ParametrosPln_Coluna], [ParametrosPln_Linha], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Codigo], [ParametrosPln_Campo] FROM [ParametrosPlanilhas] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ParametrosPln_Campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV39ParametrosPln_Campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV39ParametrosPln_Campo1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ParametrosPln_Campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV42ParametrosPln_Campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV42ParametrosPln_Campo2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ParametrosPln_Campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like '%' + @lV45ParametrosPln_Campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like '%' + @lV45ParametrosPln_Campo3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV10TFParametrosPln_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Codigo] >= @AV10TFParametrosPln_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Codigo] >= @AV10TFParametrosPln_Codigo)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV11TFParametrosPln_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Codigo] <= @AV11TFParametrosPln_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Codigo] <= @AV11TFParametrosPln_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV12TFParametrosPln_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_AreaTrabalhoCod] >= @AV12TFParametrosPln_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_AreaTrabalhoCod] >= @AV12TFParametrosPln_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV13TFParametrosPln_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_AreaTrabalhoCod] <= @AV13TFParametrosPln_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_AreaTrabalhoCod] <= @AV13TFParametrosPln_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFParametrosPln_Campo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFParametrosPln_Campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] like @lV14TFParametrosPln_Campo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] like @lV14TFParametrosPln_Campo)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFParametrosPln_Campo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Campo] = @AV15TFParametrosPln_Campo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Campo] = @AV15TFParametrosPln_Campo_Sel)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFParametrosPln_Coluna_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFParametrosPln_Coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Coluna] like @lV16TFParametrosPln_Coluna)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Coluna] like @lV16TFParametrosPln_Coluna)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFParametrosPln_Coluna_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Coluna] = @AV17TFParametrosPln_Coluna_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Coluna] = @AV17TFParametrosPln_Coluna_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV18TFParametrosPln_Linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Linha] >= @AV18TFParametrosPln_Linha)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Linha] >= @AV18TFParametrosPln_Linha)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV19TFParametrosPln_Linha_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ParametrosPln_Linha] <= @AV19TFParametrosPln_Linha_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ParametrosPln_Linha] <= @AV19TFParametrosPln_Linha_To)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ParametrosPln_Coluna]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OO2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] );
               case 1 :
                     return conditional_P00OO3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OO2 ;
          prmP00OO2 = new Object[] {
          new Object[] {"@lV39ParametrosPln_Campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42ParametrosPln_Campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV45ParametrosPln_Campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFParametrosPln_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFParametrosPln_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFParametrosPln_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFParametrosPln_Campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFParametrosPln_Campo_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFParametrosPln_Coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV17TFParametrosPln_Coluna_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV18TFParametrosPln_Linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFParametrosPln_Linha_To",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00OO3 ;
          prmP00OO3 = new Object[] {
          new Object[] {"@lV39ParametrosPln_Campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV42ParametrosPln_Campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV45ParametrosPln_Campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFParametrosPln_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFParametrosPln_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFParametrosPln_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFParametrosPln_Campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFParametrosPln_Campo_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV16TFParametrosPln_Coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV17TFParametrosPln_Coluna_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV18TFParametrosPln_Linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFParametrosPln_Linha_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OO2,100,0,true,false )
             ,new CursorDef("P00OO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OO3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptparametrosplanilhasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptparametrosplanilhasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptparametrosplanilhasfilterdata") )
          {
             return  ;
          }
          getpromptparametrosplanilhasfilterdata worker = new getpromptparametrosplanilhasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
