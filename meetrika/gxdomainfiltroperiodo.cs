/*
               File: FiltroPeriodo
        Description: FiltroPeriodo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:38.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainfiltroperiodo
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainfiltroperiodo ()
      {
         domain[(short)1] = "Hoje";
         domain[(short)2] = "Nesta Semana";
         domain[(short)3] = "Nesta Pr�xima Semana";
         domain[(short)4] = "Nesta Quinzena";
         domain[(short)5] = "Neste M�s";
         domain[(short)6] = "Neste Trimestre";
         domain[(short)7] = "Neste Semestre";
         domain[(short)8] = "Neste Ano";
         domain[(short)9] = "No �ltimo M�s";
         domain[(short)10] = "No �ltimo Trimestre";
         domain[(short)11] = "No �ltimo Semestre";
         domain[(short)12] = "No �ltimo Ano";
         domain[(short)13] = "Per�odo Personalizado";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
