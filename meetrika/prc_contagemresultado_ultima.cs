/*
               File: PRC_ContagemResultado_Ultima
        Description: Contagem Resultado_Ultima Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:48.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultado_ultima : GXProcedure
   {
      public prc_contagemresultado_ultima( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultado_ultima( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_contagemresultado_ultima objprc_contagemresultado_ultima;
         objprc_contagemresultado_ultima = new prc_contagemresultado_ultima();
         objprc_contagemresultado_ultima.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contagemresultado_ultima.context.SetSubmitInitialConfig(context);
         objprc_contagemresultado_ultima.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultado_ultima);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultado_ultima)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00362 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         /* End optimized UPDATE. */
         AV16GXLvl8 = 0;
         /* Using cursor P00363 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A483ContagemResultado_StatusCnt = P00363_A483ContagemResultado_StatusCnt[0];
            A517ContagemResultado_Ultima = P00363_A517ContagemResultado_Ultima[0];
            A511ContagemResultado_HoraCnt = P00363_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00363_A473ContagemResultado_DataCnt[0];
            AV16GXLvl8 = 1;
            A517ContagemResultado_Ultima = true;
            /* Using cursor P00364 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A484ContagemResultado_StatusDmn = P00364_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00364_n484ContagemResultado_StatusDmn[0];
               if ( ! ( A483ContagemResultado_StatusCnt == 5 ) )
               {
                  A484ContagemResultado_StatusDmn = "A";
                  n484ContagemResultado_StatusDmn = false;
               }
               else
               {
                  A484ContagemResultado_StatusDmn = "R";
                  n484ContagemResultado_StatusDmn = false;
               }
               /* Using cursor P00365 */
               pr_default.execute(3, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00366 */
            pr_default.execute(4, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00367 */
            pr_default.execute(5, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV16GXLvl8 == 0 )
         {
            /* Using cursor P00368 */
            pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A484ContagemResultado_StatusDmn = P00368_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00368_n484ContagemResultado_StatusDmn[0];
               A890ContagemResultado_Responsavel = P00368_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00368_n890ContagemResultado_Responsavel[0];
               AV8Qtd = 0;
               /* Using cursor P00369 */
               pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A1756ContagemResultado_NvlCnt = P00369_A1756ContagemResultado_NvlCnt[0];
                  n1756ContagemResultado_NvlCnt = P00369_n1756ContagemResultado_NvlCnt[0];
                  A473ContagemResultado_DataCnt = P00369_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P00369_A511ContagemResultado_HoraCnt[0];
                  AV8Qtd = (short)(AV8Qtd+1);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(7);
               }
               pr_default.close(7);
               if ( (0==AV8Qtd) )
               {
                  /* Using cursor P003610 */
                  pr_default.execute(8, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(8) != 101) )
                  {
                     A473ContagemResultado_DataCnt = P003610_A473ContagemResultado_DataCnt[0];
                     A511ContagemResultado_HoraCnt = P003610_A511ContagemResultado_HoraCnt[0];
                     AV8Qtd = (short)(AV8Qtd+1);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(8);
                  }
                  pr_default.close(8);
                  if ( (0==AV8Qtd) )
                  {
                     AV21GXLvl37 = 0;
                     /* Using cursor P003611 */
                     pr_default.execute(9, new Object[] {A456ContagemResultado_Codigo});
                     while ( (pr_default.getStatus(9) != 101) )
                     {
                        A892LogResponsavel_DemandaCod = P003611_A892LogResponsavel_DemandaCod[0];
                        n892LogResponsavel_DemandaCod = P003611_n892LogResponsavel_DemandaCod[0];
                        A1130LogResponsavel_Status = P003611_A1130LogResponsavel_Status[0];
                        n1130LogResponsavel_Status = P003611_n1130LogResponsavel_Status[0];
                        A896LogResponsavel_Owner = P003611_A896LogResponsavel_Owner[0];
                        A1797LogResponsavel_Codigo = P003611_A1797LogResponsavel_Codigo[0];
                        AV21GXLvl37 = 1;
                        AV11Status = "J";
                        AV10Responsavel = A896LogResponsavel_Owner;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(9);
                     }
                     pr_default.close(9);
                     if ( AV21GXLvl37 == 0 )
                     {
                        AV11Status = "S";
                     }
                  }
                  if ( StringUtil.StrCmp(AV11Status, "S") == 0 )
                  {
                     /* Execute user subroutine: 'RESPONSAVEL' */
                     S111 ();
                     if ( returnInSub )
                     {
                        pr_default.close(6);
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  A484ContagemResultado_StatusDmn = AV11Status;
                  n484ContagemResultado_StatusDmn = false;
                  if ( AV10Responsavel > 0 )
                  {
                     A890ContagemResultado_Responsavel = AV10Responsavel;
                     n890ContagemResultado_Responsavel = false;
                  }
                  else
                  {
                     A890ContagemResultado_Responsavel = 0;
                     n890ContagemResultado_Responsavel = false;
                     n890ContagemResultado_Responsavel = true;
                  }
               }
               /* Using cursor P003612 */
               pr_default.execute(10, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'RESPONSAVEL' Routine */
         /* Using cursor P003613 */
         pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo, AV11Status});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1130LogResponsavel_Status = P003613_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = P003613_n1130LogResponsavel_Status[0];
            A1797LogResponsavel_Codigo = P003613_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P003613_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P003613_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P003613_n892LogResponsavel_DemandaCod[0];
            GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               /* Using cursor P003614 */
               pr_default.execute(12, new Object[] {AV9Contratada_Codigo, A896LogResponsavel_Owner});
               while ( (pr_default.getStatus(12) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = P003614_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = P003614_A66ContratadaUsuario_ContratadaCod[0];
                  AV10Responsavel = A896LogResponsavel_Owner;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(12);
            }
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( (0==AV10Responsavel) )
         {
            GXt_int2 = (short)(AV10Responsavel);
            new prc_gestorprepostocontrato(context ).execute( ref  AV12Codigo, out  GXt_int2) ;
            AV10Responsavel = GXt_int2;
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ContagemResultado_Ultima");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00363_A456ContagemResultado_Codigo = new int[1] ;
         P00363_A483ContagemResultado_StatusCnt = new short[1] ;
         P00363_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00363_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00363_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         P00364_A456ContagemResultado_Codigo = new int[1] ;
         P00364_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00364_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         P00368_A456ContagemResultado_Codigo = new int[1] ;
         P00368_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00368_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00368_A890ContagemResultado_Responsavel = new int[1] ;
         P00368_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00369_A456ContagemResultado_Codigo = new int[1] ;
         P00369_A1756ContagemResultado_NvlCnt = new short[1] ;
         P00369_n1756ContagemResultado_NvlCnt = new bool[] {false} ;
         P00369_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00369_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003610_A456ContagemResultado_Codigo = new int[1] ;
         P003610_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003610_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003611_A892LogResponsavel_DemandaCod = new int[1] ;
         P003611_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P003611_A1130LogResponsavel_Status = new String[] {""} ;
         P003611_n1130LogResponsavel_Status = new bool[] {false} ;
         P003611_A896LogResponsavel_Owner = new int[1] ;
         P003611_A1797LogResponsavel_Codigo = new long[1] ;
         A1130LogResponsavel_Status = "";
         AV11Status = "";
         P003613_A1130LogResponsavel_Status = new String[] {""} ;
         P003613_n1130LogResponsavel_Status = new bool[] {false} ;
         P003613_A1797LogResponsavel_Codigo = new long[1] ;
         P003613_A896LogResponsavel_Owner = new int[1] ;
         P003613_A892LogResponsavel_DemandaCod = new int[1] ;
         P003613_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P003614_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P003614_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultado_ultima__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               P00363_A456ContagemResultado_Codigo, P00363_A483ContagemResultado_StatusCnt, P00363_A517ContagemResultado_Ultima, P00363_A511ContagemResultado_HoraCnt, P00363_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P00364_A456ContagemResultado_Codigo, P00364_A484ContagemResultado_StatusDmn, P00364_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00368_A456ContagemResultado_Codigo, P00368_A484ContagemResultado_StatusDmn, P00368_n484ContagemResultado_StatusDmn, P00368_A890ContagemResultado_Responsavel, P00368_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               P00369_A456ContagemResultado_Codigo, P00369_A1756ContagemResultado_NvlCnt, P00369_n1756ContagemResultado_NvlCnt, P00369_A473ContagemResultado_DataCnt, P00369_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P003610_A456ContagemResultado_Codigo, P003610_A473ContagemResultado_DataCnt, P003610_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P003611_A892LogResponsavel_DemandaCod, P003611_n892LogResponsavel_DemandaCod, P003611_A1130LogResponsavel_Status, P003611_n1130LogResponsavel_Status, P003611_A896LogResponsavel_Owner, P003611_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P003613_A1130LogResponsavel_Status, P003613_n1130LogResponsavel_Status, P003613_A1797LogResponsavel_Codigo, P003613_A896LogResponsavel_Owner, P003613_A892LogResponsavel_DemandaCod, P003613_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P003614_A69ContratadaUsuario_UsuarioCod, P003614_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16GXLvl8 ;
      private short A483ContagemResultado_StatusCnt ;
      private short AV8Qtd ;
      private short A1756ContagemResultado_NvlCnt ;
      private short AV21GXLvl37 ;
      private short GXt_int2 ;
      private int A456ContagemResultado_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int AV10Responsavel ;
      private int AV9Contratada_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int AV12Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1130LogResponsavel_Status ;
      private String AV11Status ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool A517ContagemResultado_Ultima ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1130LogResponsavel_Status ;
      private bool returnInSub ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00363_A456ContagemResultado_Codigo ;
      private short[] P00363_A483ContagemResultado_StatusCnt ;
      private bool[] P00363_A517ContagemResultado_Ultima ;
      private String[] P00363_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00363_A473ContagemResultado_DataCnt ;
      private int[] P00364_A456ContagemResultado_Codigo ;
      private String[] P00364_A484ContagemResultado_StatusDmn ;
      private bool[] P00364_n484ContagemResultado_StatusDmn ;
      private int[] P00368_A456ContagemResultado_Codigo ;
      private String[] P00368_A484ContagemResultado_StatusDmn ;
      private bool[] P00368_n484ContagemResultado_StatusDmn ;
      private int[] P00368_A890ContagemResultado_Responsavel ;
      private bool[] P00368_n890ContagemResultado_Responsavel ;
      private int[] P00369_A456ContagemResultado_Codigo ;
      private short[] P00369_A1756ContagemResultado_NvlCnt ;
      private bool[] P00369_n1756ContagemResultado_NvlCnt ;
      private DateTime[] P00369_A473ContagemResultado_DataCnt ;
      private String[] P00369_A511ContagemResultado_HoraCnt ;
      private int[] P003610_A456ContagemResultado_Codigo ;
      private DateTime[] P003610_A473ContagemResultado_DataCnt ;
      private String[] P003610_A511ContagemResultado_HoraCnt ;
      private int[] P003611_A892LogResponsavel_DemandaCod ;
      private bool[] P003611_n892LogResponsavel_DemandaCod ;
      private String[] P003611_A1130LogResponsavel_Status ;
      private bool[] P003611_n1130LogResponsavel_Status ;
      private int[] P003611_A896LogResponsavel_Owner ;
      private long[] P003611_A1797LogResponsavel_Codigo ;
      private String[] P003613_A1130LogResponsavel_Status ;
      private bool[] P003613_n1130LogResponsavel_Status ;
      private long[] P003613_A1797LogResponsavel_Codigo ;
      private int[] P003613_A896LogResponsavel_Owner ;
      private int[] P003613_A892LogResponsavel_DemandaCod ;
      private bool[] P003613_n892LogResponsavel_DemandaCod ;
      private int[] P003614_A69ContratadaUsuario_UsuarioCod ;
      private int[] P003614_A66ContratadaUsuario_ContratadaCod ;
   }

   public class prc_contagemresultado_ultima__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00362 ;
          prmP00362 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00363 ;
          prmP00363 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00364 ;
          prmP00364 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00365 ;
          prmP00365 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00366 ;
          prmP00366 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00367 ;
          prmP00367 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00368 ;
          prmP00368 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00369 ;
          prmP00369 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003610 ;
          prmP003610 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003611 ;
          prmP003611 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003612 ;
          prmP003612 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003613 ;
          prmP003613 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Status",SqlDbType.Char,1,0}
          } ;
          Object[] prmP003614 ;
          prmP003614 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00362", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=CONVERT(BIT, 0)  WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00362)
             ,new CursorDef("P00363", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_Ultima], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00363,1,0,true,true )
             ,new CursorDef("P00364", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00364,1,0,true,true )
             ,new CursorDef("P00365", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00365)
             ,new CursorDef("P00366", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00366)
             ,new CursorDef("P00367", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00367)
             ,new CursorDef("P00368", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_Responsavel] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00368,1,0,true,true )
             ,new CursorDef("P00369", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_NvlCnt], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_NvlCnt] = 2) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00369,1,0,false,true )
             ,new CursorDef("P003610", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003610,1,0,false,true )
             ,new CursorDef("P003611", "SELECT TOP 1 [LogResponsavel_DemandaCod], [LogResponsavel_Status], [LogResponsavel_Owner], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND ([LogResponsavel_Status] = 'J') ORDER BY [LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003611,1,0,false,true )
             ,new CursorDef("P003612", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003612)
             ,new CursorDef("P003613", "SELECT [LogResponsavel_Status], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND ([LogResponsavel_Status] = @AV11Status) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003613,100,0,true,false )
             ,new CursorDef("P003614", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV9Contratada_Codigo and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003614,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
