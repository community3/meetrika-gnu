/*
               File: DP_WS_Demandas
        Description: Retorna as Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 7/1/2019 22:8:56.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_ws_demandas : GXProcedure
   {
      public dp_ws_demandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_ws_demandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "GxEv3Up14_Meetrika", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "GxEv3Up14_Meetrika", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         dp_ws_demandas objdp_ws_demandas;
         objdp_ws_demandas = new dp_ws_demandas();
         objdp_ws_demandas.Gxm2rootcol = new GxObjectCollection( context, "SDT_WS_Demandas.Demanda", "GxEv3Up14_Meetrika", "SdtSDT_WS_Demandas_Demanda", "GeneXus.Programs") ;
         objdp_ws_demandas.context.SetSubmitInitialConfig(context);
         objdp_ws_demandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_ws_demandas);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_ws_demandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000S3 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P000S3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P000S3_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P000S3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P000S3_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P000S3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P000S3_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P000S3_A508ContagemResultado_Owner[0];
            A890ContagemResultado_Responsavel = P000S3_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P000S3_n890ContagemResultado_Responsavel[0];
            A456ContagemResultado_Codigo = P000S3_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = P000S3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P000S3_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P000S3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P000S3_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P000S3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P000S3_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P000S3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P000S3_n801ContagemResultado_ServicoSigla[0];
            A484ContagemResultado_StatusDmn = P000S3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P000S3_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P000S3_A471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P000S3_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P000S3_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P000S3_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P000S3_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P000S3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P000S3_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P000S3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P000S3_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P000S3_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P000S3_n2118ContagemResultado_Owner_Identificao[0];
            A509ContagemrResultado_SistemaSigla = P000S3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P000S3_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P000S3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P000S3_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P000S3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P000S3_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P000S3_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P000S3_n2118ContagemResultado_Owner_Identificao[0];
            Gxm1sdt_ws_demandas = new SdtSDT_WS_Demandas_Demanda(context);
            Gxm2rootcol.Add(Gxm1sdt_ws_demandas, 0);
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_descricao = A494ContagemResultado_Descricao;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_servicosigla = A801ContagemResultado_ServicoSigla;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            Gxm1sdt_ws_demandas.gxTpr_Contagemrresultado_sistemasigla = A509ContagemrResultado_SistemaSigla;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_owner_identificao = A2118ContagemResultado_Owner_Identificao;
            Gxm1sdt_ws_demandas.gxTpr_Contagemresultado_agrupador = A1046ContagemResultado_Agrupador;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000S3_A489ContagemResultado_SistemaCod = new int[1] ;
         P000S3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P000S3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P000S3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P000S3_A601ContagemResultado_Servico = new int[1] ;
         P000S3_n601ContagemResultado_Servico = new bool[] {false} ;
         P000S3_A508ContagemResultado_Owner = new int[1] ;
         P000S3_A890ContagemResultado_Responsavel = new int[1] ;
         P000S3_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P000S3_A456ContagemResultado_Codigo = new int[1] ;
         P000S3_A457ContagemResultado_Demanda = new String[] {""} ;
         P000S3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P000S3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P000S3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P000S3_A494ContagemResultado_Descricao = new String[] {""} ;
         P000S3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P000S3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P000S3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P000S3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P000S3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P000S3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P000S3_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P000S3_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P000S3_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P000S3_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P000S3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P000S3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P000S3_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P000S3_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P000S3_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P000S3_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A801ContagemResultado_ServicoSigla = "";
         A484ContagemResultado_StatusDmn = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A509ContagemrResultado_SistemaSigla = "";
         A1046ContagemResultado_Agrupador = "";
         A2118ContagemResultado_Owner_Identificao = "";
         Gxm1sdt_ws_demandas = new SdtSDT_WS_Demandas_Demanda(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_ws_demandas__default(),
            new Object[][] {
                new Object[] {
               P000S3_A489ContagemResultado_SistemaCod, P000S3_n489ContagemResultado_SistemaCod, P000S3_A1553ContagemResultado_CntSrvCod, P000S3_n1553ContagemResultado_CntSrvCod, P000S3_A601ContagemResultado_Servico, P000S3_n601ContagemResultado_Servico, P000S3_A508ContagemResultado_Owner, P000S3_A890ContagemResultado_Responsavel, P000S3_n890ContagemResultado_Responsavel, P000S3_A456ContagemResultado_Codigo,
               P000S3_A457ContagemResultado_Demanda, P000S3_n457ContagemResultado_Demanda, P000S3_A493ContagemResultado_DemandaFM, P000S3_n493ContagemResultado_DemandaFM, P000S3_A494ContagemResultado_Descricao, P000S3_n494ContagemResultado_Descricao, P000S3_A801ContagemResultado_ServicoSigla, P000S3_n801ContagemResultado_ServicoSigla, P000S3_A484ContagemResultado_StatusDmn, P000S3_n484ContagemResultado_StatusDmn,
               P000S3_A471ContagemResultado_DataDmn, P000S3_A1351ContagemResultado_DataPrevista, P000S3_n1351ContagemResultado_DataPrevista, P000S3_A472ContagemResultado_DataEntrega, P000S3_n472ContagemResultado_DataEntrega, P000S3_A509ContagemrResultado_SistemaSigla, P000S3_n509ContagemrResultado_SistemaSigla, P000S3_A1046ContagemResultado_Agrupador, P000S3_n1046ContagemResultado_Agrupador, P000S3_A2118ContagemResultado_Owner_Identificao,
               P000S3_n2118ContagemResultado_Owner_Identificao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A508ContagemResultado_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1046ContagemResultado_Agrupador ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n2118ContagemResultado_Owner_Identificao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A2118ContagemResultado_Owner_Identificao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000S3_A489ContagemResultado_SistemaCod ;
      private bool[] P000S3_n489ContagemResultado_SistemaCod ;
      private int[] P000S3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P000S3_n1553ContagemResultado_CntSrvCod ;
      private int[] P000S3_A601ContagemResultado_Servico ;
      private bool[] P000S3_n601ContagemResultado_Servico ;
      private int[] P000S3_A508ContagemResultado_Owner ;
      private int[] P000S3_A890ContagemResultado_Responsavel ;
      private bool[] P000S3_n890ContagemResultado_Responsavel ;
      private int[] P000S3_A456ContagemResultado_Codigo ;
      private String[] P000S3_A457ContagemResultado_Demanda ;
      private bool[] P000S3_n457ContagemResultado_Demanda ;
      private String[] P000S3_A493ContagemResultado_DemandaFM ;
      private bool[] P000S3_n493ContagemResultado_DemandaFM ;
      private String[] P000S3_A494ContagemResultado_Descricao ;
      private bool[] P000S3_n494ContagemResultado_Descricao ;
      private String[] P000S3_A801ContagemResultado_ServicoSigla ;
      private bool[] P000S3_n801ContagemResultado_ServicoSigla ;
      private String[] P000S3_A484ContagemResultado_StatusDmn ;
      private bool[] P000S3_n484ContagemResultado_StatusDmn ;
      private DateTime[] P000S3_A471ContagemResultado_DataDmn ;
      private DateTime[] P000S3_A1351ContagemResultado_DataPrevista ;
      private bool[] P000S3_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P000S3_A472ContagemResultado_DataEntrega ;
      private bool[] P000S3_n472ContagemResultado_DataEntrega ;
      private String[] P000S3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P000S3_n509ContagemrResultado_SistemaSigla ;
      private String[] P000S3_A1046ContagemResultado_Agrupador ;
      private bool[] P000S3_n1046ContagemResultado_Agrupador ;
      private String[] P000S3_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P000S3_n2118ContagemResultado_Owner_Identificao ;
      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_Demandas_Demanda ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_WS_Demandas_Demanda Gxm1sdt_ws_demandas ;
   }

   public class dp_ws_demandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000S3 ;
          prmP000S3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P000S3", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) WHERE T1.[ContagemResultado_Responsavel] = 2257 ORDER BY T1.[ContagemResultado_Responsavel] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000S3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
