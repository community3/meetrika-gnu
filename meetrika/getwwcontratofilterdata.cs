/*
               File: GetWWContratoFilterData
        Description: Get WWContrato Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/11/2019 13:27:23.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratofilterdata : GXProcedure
   {
      public getwwcontratofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratofilterdata objgetwwcontratofilterdata;
         objgetwwcontratofilterdata = new getwwcontratofilterdata();
         objgetwwcontratofilterdata.AV26DDOName = aP0_DDOName;
         objgetwwcontratofilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwcontratofilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratofilterdata.AV30OptionsJson = "" ;
         objgetwwcontratofilterdata.AV33OptionsDescJson = "" ;
         objgetwwcontratofilterdata.AV35OptionIndexesJson = "" ;
         objgetwwcontratofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratofilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATO_NUMEROATA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROATAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWContratoGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWContratoGridState"), "");
         }
         AV82GXV1 = 1;
         while ( AV82GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV82GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "CONTRATO_AREATRABALHOCOD") == 0 )
            {
               AV42Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV20TFContratada_PessoaNom = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV21TFContratada_PessoaNom_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMEROATA") == 0 )
            {
               AV12TFContrato_NumeroAta = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMEROATA_SEL") == 0 )
            {
               AV13TFContrato_NumeroAta_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_ANO") == 0 )
            {
               AV14TFContrato_Ano = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContrato_Ano_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_DATATERMINO") == 0 )
            {
               AV64TFContrato_DataTermino = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV65TFContrato_DataTermino_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_VALORUNDCNTATUAL") == 0 )
            {
               AV66TFContrato_ValorUndCntAtual = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV67TFContrato_ValorUndCntAtual_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV22TFContratada_PessoaCNPJ = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV23TFContratada_PessoaCNPJ_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTRATO_ATIVO_SEL") == 0 )
            {
               AV63TFContrato_Ativo_Sel = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            AV82GXV1 = (int)(AV82GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV43DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV44Contrato_Numero1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
            {
               AV45Contrato_NumeroAta1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV46Contratada_PessoaNom1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
            {
               AV47Contrato_Ano1 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Value, "."));
               AV48Contrato_Ano_To1 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Valueto, "."));
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV49DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV50DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV51Contrato_Numero2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
               {
                  AV52Contrato_NumeroAta2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV53Contratada_PessoaNom2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
               {
                  AV54Contrato_Ano2 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Value, "."));
                  AV55Contrato_Ano_To2 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Valueto, "."));
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV56DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV57DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV58Contrato_Numero3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
                  {
                     AV59Contrato_NumeroAta3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV60Contratada_PessoaNom3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
                  {
                     AV61Contrato_Ano3 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Value, "."));
                     AV62Contrato_Ano_To3 = (short)(NumberUtil.Val( AV41GridStateDynamicFilter.gxTpr_Valueto, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV20TFContratada_PessoaNom = AV24SearchTxt;
         AV21TFContratada_PessoaNom_Sel = "";
         AV84WWContratoDS_1_Contrato_areatrabalhocod = AV42Contrato_AreaTrabalhoCod;
         AV85WWContratoDS_2_Dynamicfiltersselector1 = AV43DynamicFiltersSelector1;
         AV86WWContratoDS_3_Contrato_numero1 = AV44Contrato_Numero1;
         AV87WWContratoDS_4_Contrato_numeroata1 = AV45Contrato_NumeroAta1;
         AV88WWContratoDS_5_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV89WWContratoDS_6_Contrato_ano1 = AV47Contrato_Ano1;
         AV90WWContratoDS_7_Contrato_ano_to1 = AV48Contrato_Ano_To1;
         AV91WWContratoDS_8_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV92WWContratoDS_9_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV93WWContratoDS_10_Contrato_numero2 = AV51Contrato_Numero2;
         AV94WWContratoDS_11_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV95WWContratoDS_12_Contratada_pessoanom2 = AV53Contratada_PessoaNom2;
         AV96WWContratoDS_13_Contrato_ano2 = AV54Contrato_Ano2;
         AV97WWContratoDS_14_Contrato_ano_to2 = AV55Contrato_Ano_To2;
         AV98WWContratoDS_15_Dynamicfiltersenabled3 = AV56DynamicFiltersEnabled3;
         AV99WWContratoDS_16_Dynamicfiltersselector3 = AV57DynamicFiltersSelector3;
         AV100WWContratoDS_17_Contrato_numero3 = AV58Contrato_Numero3;
         AV101WWContratoDS_18_Contrato_numeroata3 = AV59Contrato_NumeroAta3;
         AV102WWContratoDS_19_Contratada_pessoanom3 = AV60Contratada_PessoaNom3;
         AV103WWContratoDS_20_Contrato_ano3 = AV61Contrato_Ano3;
         AV104WWContratoDS_21_Contrato_ano_to3 = AV62Contrato_Ano_To3;
         AV105WWContratoDS_22_Tfcontratada_pessoanom = AV20TFContratada_PessoaNom;
         AV106WWContratoDS_23_Tfcontratada_pessoanom_sel = AV21TFContratada_PessoaNom_Sel;
         AV107WWContratoDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV108WWContratoDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV109WWContratoDS_26_Tfcontrato_numeroata = AV12TFContrato_NumeroAta;
         AV110WWContratoDS_27_Tfcontrato_numeroata_sel = AV13TFContrato_NumeroAta_Sel;
         AV111WWContratoDS_28_Tfcontrato_ano = AV14TFContrato_Ano;
         AV112WWContratoDS_29_Tfcontrato_ano_to = AV15TFContrato_Ano_To;
         AV113WWContratoDS_30_Tfcontrato_datatermino = AV64TFContrato_DataTermino;
         AV114WWContratoDS_31_Tfcontrato_datatermino_to = AV65TFContrato_DataTermino_To;
         AV115WWContratoDS_32_Tfcontrato_valorundcntatual = AV66TFContrato_ValorUndCntAtual;
         AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = AV67TFContrato_ValorUndCntAtual_To;
         AV117WWContratoDS_34_Tfcontratada_pessoacnpj = AV22TFContratada_PessoaCNPJ;
         AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel = AV23TFContratada_PessoaCNPJ_Sel;
         AV119WWContratoDS_36_Tfcontrato_ativo_sel = AV63TFContrato_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              AV86WWContratoDS_3_Contrato_numero1 ,
                                              AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              AV89WWContratoDS_6_Contrato_ano1 ,
                                              AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              AV93WWContratoDS_10_Contrato_numero2 ,
                                              AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              AV96WWContratoDS_13_Contrato_ano2 ,
                                              AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              AV100WWContratoDS_17_Contrato_numero3 ,
                                              AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              AV103WWContratoDS_20_Contrato_ano3 ,
                                              AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              AV107WWContratoDS_24_Tfcontrato_numero ,
                                              AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              AV111WWContratoDS_28_Tfcontrato_ano ,
                                              AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A92Contrato_Ativo ,
                                              AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              A1869Contrato_DataTermino ,
                                              AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              A1870Contrato_ValorUndCntAtual ,
                                              AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV86WWContratoDS_3_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1), 20, "%");
         lV87WWContratoDS_4_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1), 10, "%");
         lV88WWContratoDS_5_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1), 100, "%");
         lV93WWContratoDS_10_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2), 20, "%");
         lV94WWContratoDS_11_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2), 10, "%");
         lV95WWContratoDS_12_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2), 100, "%");
         lV100WWContratoDS_17_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3), 20, "%");
         lV101WWContratoDS_18_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3), 10, "%");
         lV102WWContratoDS_19_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3), 100, "%");
         lV105WWContratoDS_22_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom), 100, "%");
         lV107WWContratoDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero), 20, "%");
         lV109WWContratoDS_26_Tfcontrato_numeroata = StringUtil.PadR( StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata), 10, "%");
         lV117WWContratoDS_34_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00IP7 */
         pr_default.execute(0, new Object[] {AV113WWContratoDS_30_Tfcontrato_datatermino, AV113WWContratoDS_30_Tfcontrato_datatermino, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV84WWContratoDS_1_Contrato_areatrabalhocod, AV9WWPContext.gxTpr_Contratada_codigo, lV86WWContratoDS_3_Contrato_numero1, lV87WWContratoDS_4_Contrato_numeroata1, lV88WWContratoDS_5_Contratada_pessoanom1, AV89WWContratoDS_6_Contrato_ano1, AV90WWContratoDS_7_Contrato_ano_to1, lV93WWContratoDS_10_Contrato_numero2, lV94WWContratoDS_11_Contrato_numeroata2, lV95WWContratoDS_12_Contratada_pessoanom2, AV96WWContratoDS_13_Contrato_ano2, AV97WWContratoDS_14_Contrato_ano_to2, lV100WWContratoDS_17_Contrato_numero3, lV101WWContratoDS_18_Contrato_numeroata3, lV102WWContratoDS_19_Contratada_pessoanom3, AV103WWContratoDS_20_Contrato_ano3, AV104WWContratoDS_21_Contrato_ano_to3, lV105WWContratoDS_22_Tfcontratada_pessoanom, AV106WWContratoDS_23_Tfcontratada_pessoanom_sel, lV107WWContratoDS_24_Tfcontrato_numero, AV108WWContratoDS_25_Tfcontrato_numero_sel, lV109WWContratoDS_26_Tfcontrato_numeroata, AV110WWContratoDS_27_Tfcontrato_numeroata_sel, AV111WWContratoDS_28_Tfcontrato_ano, AV112WWContratoDS_29_Tfcontrato_ano_to, lV117WWContratoDS_34_Tfcontratada_pessoacnpj, AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKIP2 = false;
            A40Contratada_PessoaCod = P00IP7_A40Contratada_PessoaCod[0];
            A74Contrato_Codigo = P00IP7_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00IP7_n74Contrato_Codigo[0];
            A41Contratada_PessoaNom = P00IP7_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP7_n41Contratada_PessoaNom[0];
            A92Contrato_Ativo = P00IP7_A92Contrato_Ativo[0];
            A42Contratada_PessoaCNPJ = P00IP7_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP7_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00IP7_A79Contrato_Ano[0];
            A78Contrato_NumeroAta = P00IP7_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00IP7_n78Contrato_NumeroAta[0];
            A77Contrato_Numero = P00IP7_A77Contrato_Numero[0];
            A39Contratada_Codigo = P00IP7_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00IP7_A75Contrato_AreaTrabalhoCod[0];
            A1870Contrato_ValorUndCntAtual = P00IP7_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP7_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP7_n1869Contrato_DataTermino[0];
            A1870Contrato_ValorUndCntAtual = P00IP7_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP7_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP7_n1869Contrato_DataTermino[0];
            A40Contratada_PessoaCod = P00IP7_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00IP7_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00IP7_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP7_n42Contratada_PessoaCNPJ[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00IP7_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKIP2 = false;
               A40Contratada_PessoaCod = P00IP7_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = P00IP7_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00IP7_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00IP7_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00IP7_A40Contratada_PessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKIP2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV28Option = A41Contratada_PessoaNom;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIP2 )
            {
               BRKIP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV24SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV84WWContratoDS_1_Contrato_areatrabalhocod = AV42Contrato_AreaTrabalhoCod;
         AV85WWContratoDS_2_Dynamicfiltersselector1 = AV43DynamicFiltersSelector1;
         AV86WWContratoDS_3_Contrato_numero1 = AV44Contrato_Numero1;
         AV87WWContratoDS_4_Contrato_numeroata1 = AV45Contrato_NumeroAta1;
         AV88WWContratoDS_5_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV89WWContratoDS_6_Contrato_ano1 = AV47Contrato_Ano1;
         AV90WWContratoDS_7_Contrato_ano_to1 = AV48Contrato_Ano_To1;
         AV91WWContratoDS_8_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV92WWContratoDS_9_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV93WWContratoDS_10_Contrato_numero2 = AV51Contrato_Numero2;
         AV94WWContratoDS_11_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV95WWContratoDS_12_Contratada_pessoanom2 = AV53Contratada_PessoaNom2;
         AV96WWContratoDS_13_Contrato_ano2 = AV54Contrato_Ano2;
         AV97WWContratoDS_14_Contrato_ano_to2 = AV55Contrato_Ano_To2;
         AV98WWContratoDS_15_Dynamicfiltersenabled3 = AV56DynamicFiltersEnabled3;
         AV99WWContratoDS_16_Dynamicfiltersselector3 = AV57DynamicFiltersSelector3;
         AV100WWContratoDS_17_Contrato_numero3 = AV58Contrato_Numero3;
         AV101WWContratoDS_18_Contrato_numeroata3 = AV59Contrato_NumeroAta3;
         AV102WWContratoDS_19_Contratada_pessoanom3 = AV60Contratada_PessoaNom3;
         AV103WWContratoDS_20_Contrato_ano3 = AV61Contrato_Ano3;
         AV104WWContratoDS_21_Contrato_ano_to3 = AV62Contrato_Ano_To3;
         AV105WWContratoDS_22_Tfcontratada_pessoanom = AV20TFContratada_PessoaNom;
         AV106WWContratoDS_23_Tfcontratada_pessoanom_sel = AV21TFContratada_PessoaNom_Sel;
         AV107WWContratoDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV108WWContratoDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV109WWContratoDS_26_Tfcontrato_numeroata = AV12TFContrato_NumeroAta;
         AV110WWContratoDS_27_Tfcontrato_numeroata_sel = AV13TFContrato_NumeroAta_Sel;
         AV111WWContratoDS_28_Tfcontrato_ano = AV14TFContrato_Ano;
         AV112WWContratoDS_29_Tfcontrato_ano_to = AV15TFContrato_Ano_To;
         AV113WWContratoDS_30_Tfcontrato_datatermino = AV64TFContrato_DataTermino;
         AV114WWContratoDS_31_Tfcontrato_datatermino_to = AV65TFContrato_DataTermino_To;
         AV115WWContratoDS_32_Tfcontrato_valorundcntatual = AV66TFContrato_ValorUndCntAtual;
         AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = AV67TFContrato_ValorUndCntAtual_To;
         AV117WWContratoDS_34_Tfcontratada_pessoacnpj = AV22TFContratada_PessoaCNPJ;
         AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel = AV23TFContratada_PessoaCNPJ_Sel;
         AV119WWContratoDS_36_Tfcontrato_ativo_sel = AV63TFContrato_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              AV86WWContratoDS_3_Contrato_numero1 ,
                                              AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              AV89WWContratoDS_6_Contrato_ano1 ,
                                              AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              AV93WWContratoDS_10_Contrato_numero2 ,
                                              AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              AV96WWContratoDS_13_Contrato_ano2 ,
                                              AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              AV100WWContratoDS_17_Contrato_numero3 ,
                                              AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              AV103WWContratoDS_20_Contrato_ano3 ,
                                              AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              AV107WWContratoDS_24_Tfcontrato_numero ,
                                              AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              AV111WWContratoDS_28_Tfcontrato_ano ,
                                              AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A92Contrato_Ativo ,
                                              AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              A1869Contrato_DataTermino ,
                                              AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              A1870Contrato_ValorUndCntAtual ,
                                              AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV86WWContratoDS_3_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1), 20, "%");
         lV87WWContratoDS_4_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1), 10, "%");
         lV88WWContratoDS_5_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1), 100, "%");
         lV93WWContratoDS_10_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2), 20, "%");
         lV94WWContratoDS_11_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2), 10, "%");
         lV95WWContratoDS_12_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2), 100, "%");
         lV100WWContratoDS_17_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3), 20, "%");
         lV101WWContratoDS_18_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3), 10, "%");
         lV102WWContratoDS_19_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3), 100, "%");
         lV105WWContratoDS_22_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom), 100, "%");
         lV107WWContratoDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero), 20, "%");
         lV109WWContratoDS_26_Tfcontrato_numeroata = StringUtil.PadR( StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata), 10, "%");
         lV117WWContratoDS_34_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00IP13 */
         pr_default.execute(1, new Object[] {AV113WWContratoDS_30_Tfcontrato_datatermino, AV113WWContratoDS_30_Tfcontrato_datatermino, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV84WWContratoDS_1_Contrato_areatrabalhocod, AV9WWPContext.gxTpr_Contratada_codigo, lV86WWContratoDS_3_Contrato_numero1, lV87WWContratoDS_4_Contrato_numeroata1, lV88WWContratoDS_5_Contratada_pessoanom1, AV89WWContratoDS_6_Contrato_ano1, AV90WWContratoDS_7_Contrato_ano_to1, lV93WWContratoDS_10_Contrato_numero2, lV94WWContratoDS_11_Contrato_numeroata2, lV95WWContratoDS_12_Contratada_pessoanom2, AV96WWContratoDS_13_Contrato_ano2, AV97WWContratoDS_14_Contrato_ano_to2, lV100WWContratoDS_17_Contrato_numero3, lV101WWContratoDS_18_Contrato_numeroata3, lV102WWContratoDS_19_Contratada_pessoanom3, AV103WWContratoDS_20_Contrato_ano3, AV104WWContratoDS_21_Contrato_ano_to3, lV105WWContratoDS_22_Tfcontratada_pessoanom, AV106WWContratoDS_23_Tfcontratada_pessoanom_sel, lV107WWContratoDS_24_Tfcontrato_numero, AV108WWContratoDS_25_Tfcontrato_numero_sel, lV109WWContratoDS_26_Tfcontrato_numeroata, AV110WWContratoDS_27_Tfcontrato_numeroata_sel, AV111WWContratoDS_28_Tfcontrato_ano, AV112WWContratoDS_29_Tfcontrato_ano_to, lV117WWContratoDS_34_Tfcontratada_pessoacnpj, AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKIP4 = false;
            A40Contratada_PessoaCod = P00IP13_A40Contratada_PessoaCod[0];
            A74Contrato_Codigo = P00IP13_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00IP13_n74Contrato_Codigo[0];
            A77Contrato_Numero = P00IP13_A77Contrato_Numero[0];
            A92Contrato_Ativo = P00IP13_A92Contrato_Ativo[0];
            A42Contratada_PessoaCNPJ = P00IP13_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP13_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00IP13_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00IP13_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP13_n41Contratada_PessoaNom[0];
            A78Contrato_NumeroAta = P00IP13_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00IP13_n78Contrato_NumeroAta[0];
            A39Contratada_Codigo = P00IP13_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00IP13_A75Contrato_AreaTrabalhoCod[0];
            A1870Contrato_ValorUndCntAtual = P00IP13_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP13_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP13_n1869Contrato_DataTermino[0];
            A1870Contrato_ValorUndCntAtual = P00IP13_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP13_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP13_n1869Contrato_DataTermino[0];
            A40Contratada_PessoaCod = P00IP13_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00IP13_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP13_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00IP13_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP13_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00IP13_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKIP4 = false;
               A74Contrato_Codigo = P00IP13_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00IP13_n74Contrato_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKIP4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV28Option = A77Contrato_Numero;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIP4 )
            {
               BRKIP4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATO_NUMEROATAOPTIONS' Routine */
         AV12TFContrato_NumeroAta = AV24SearchTxt;
         AV13TFContrato_NumeroAta_Sel = "";
         AV84WWContratoDS_1_Contrato_areatrabalhocod = AV42Contrato_AreaTrabalhoCod;
         AV85WWContratoDS_2_Dynamicfiltersselector1 = AV43DynamicFiltersSelector1;
         AV86WWContratoDS_3_Contrato_numero1 = AV44Contrato_Numero1;
         AV87WWContratoDS_4_Contrato_numeroata1 = AV45Contrato_NumeroAta1;
         AV88WWContratoDS_5_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV89WWContratoDS_6_Contrato_ano1 = AV47Contrato_Ano1;
         AV90WWContratoDS_7_Contrato_ano_to1 = AV48Contrato_Ano_To1;
         AV91WWContratoDS_8_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV92WWContratoDS_9_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV93WWContratoDS_10_Contrato_numero2 = AV51Contrato_Numero2;
         AV94WWContratoDS_11_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV95WWContratoDS_12_Contratada_pessoanom2 = AV53Contratada_PessoaNom2;
         AV96WWContratoDS_13_Contrato_ano2 = AV54Contrato_Ano2;
         AV97WWContratoDS_14_Contrato_ano_to2 = AV55Contrato_Ano_To2;
         AV98WWContratoDS_15_Dynamicfiltersenabled3 = AV56DynamicFiltersEnabled3;
         AV99WWContratoDS_16_Dynamicfiltersselector3 = AV57DynamicFiltersSelector3;
         AV100WWContratoDS_17_Contrato_numero3 = AV58Contrato_Numero3;
         AV101WWContratoDS_18_Contrato_numeroata3 = AV59Contrato_NumeroAta3;
         AV102WWContratoDS_19_Contratada_pessoanom3 = AV60Contratada_PessoaNom3;
         AV103WWContratoDS_20_Contrato_ano3 = AV61Contrato_Ano3;
         AV104WWContratoDS_21_Contrato_ano_to3 = AV62Contrato_Ano_To3;
         AV105WWContratoDS_22_Tfcontratada_pessoanom = AV20TFContratada_PessoaNom;
         AV106WWContratoDS_23_Tfcontratada_pessoanom_sel = AV21TFContratada_PessoaNom_Sel;
         AV107WWContratoDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV108WWContratoDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV109WWContratoDS_26_Tfcontrato_numeroata = AV12TFContrato_NumeroAta;
         AV110WWContratoDS_27_Tfcontrato_numeroata_sel = AV13TFContrato_NumeroAta_Sel;
         AV111WWContratoDS_28_Tfcontrato_ano = AV14TFContrato_Ano;
         AV112WWContratoDS_29_Tfcontrato_ano_to = AV15TFContrato_Ano_To;
         AV113WWContratoDS_30_Tfcontrato_datatermino = AV64TFContrato_DataTermino;
         AV114WWContratoDS_31_Tfcontrato_datatermino_to = AV65TFContrato_DataTermino_To;
         AV115WWContratoDS_32_Tfcontrato_valorundcntatual = AV66TFContrato_ValorUndCntAtual;
         AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = AV67TFContrato_ValorUndCntAtual_To;
         AV117WWContratoDS_34_Tfcontratada_pessoacnpj = AV22TFContratada_PessoaCNPJ;
         AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel = AV23TFContratada_PessoaCNPJ_Sel;
         AV119WWContratoDS_36_Tfcontrato_ativo_sel = AV63TFContrato_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              AV86WWContratoDS_3_Contrato_numero1 ,
                                              AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              AV89WWContratoDS_6_Contrato_ano1 ,
                                              AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              AV93WWContratoDS_10_Contrato_numero2 ,
                                              AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              AV96WWContratoDS_13_Contrato_ano2 ,
                                              AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              AV100WWContratoDS_17_Contrato_numero3 ,
                                              AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              AV103WWContratoDS_20_Contrato_ano3 ,
                                              AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              AV107WWContratoDS_24_Tfcontrato_numero ,
                                              AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              AV111WWContratoDS_28_Tfcontrato_ano ,
                                              AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A92Contrato_Ativo ,
                                              AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              A1869Contrato_DataTermino ,
                                              AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              A1870Contrato_ValorUndCntAtual ,
                                              AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV86WWContratoDS_3_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1), 20, "%");
         lV87WWContratoDS_4_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1), 10, "%");
         lV88WWContratoDS_5_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1), 100, "%");
         lV93WWContratoDS_10_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2), 20, "%");
         lV94WWContratoDS_11_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2), 10, "%");
         lV95WWContratoDS_12_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2), 100, "%");
         lV100WWContratoDS_17_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3), 20, "%");
         lV101WWContratoDS_18_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3), 10, "%");
         lV102WWContratoDS_19_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3), 100, "%");
         lV105WWContratoDS_22_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom), 100, "%");
         lV107WWContratoDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero), 20, "%");
         lV109WWContratoDS_26_Tfcontrato_numeroata = StringUtil.PadR( StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata), 10, "%");
         lV117WWContratoDS_34_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00IP19 */
         pr_default.execute(2, new Object[] {AV113WWContratoDS_30_Tfcontrato_datatermino, AV113WWContratoDS_30_Tfcontrato_datatermino, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV84WWContratoDS_1_Contrato_areatrabalhocod, AV9WWPContext.gxTpr_Contratada_codigo, lV86WWContratoDS_3_Contrato_numero1, lV87WWContratoDS_4_Contrato_numeroata1, lV88WWContratoDS_5_Contratada_pessoanom1, AV89WWContratoDS_6_Contrato_ano1, AV90WWContratoDS_7_Contrato_ano_to1, lV93WWContratoDS_10_Contrato_numero2, lV94WWContratoDS_11_Contrato_numeroata2, lV95WWContratoDS_12_Contratada_pessoanom2, AV96WWContratoDS_13_Contrato_ano2, AV97WWContratoDS_14_Contrato_ano_to2, lV100WWContratoDS_17_Contrato_numero3, lV101WWContratoDS_18_Contrato_numeroata3, lV102WWContratoDS_19_Contratada_pessoanom3, AV103WWContratoDS_20_Contrato_ano3, AV104WWContratoDS_21_Contrato_ano_to3, lV105WWContratoDS_22_Tfcontratada_pessoanom, AV106WWContratoDS_23_Tfcontratada_pessoanom_sel, lV107WWContratoDS_24_Tfcontrato_numero, AV108WWContratoDS_25_Tfcontrato_numero_sel, lV109WWContratoDS_26_Tfcontrato_numeroata, AV110WWContratoDS_27_Tfcontrato_numeroata_sel, AV111WWContratoDS_28_Tfcontrato_ano, AV112WWContratoDS_29_Tfcontrato_ano_to, lV117WWContratoDS_34_Tfcontratada_pessoacnpj, AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKIP6 = false;
            A40Contratada_PessoaCod = P00IP19_A40Contratada_PessoaCod[0];
            A74Contrato_Codigo = P00IP19_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00IP19_n74Contrato_Codigo[0];
            A78Contrato_NumeroAta = P00IP19_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00IP19_n78Contrato_NumeroAta[0];
            A92Contrato_Ativo = P00IP19_A92Contrato_Ativo[0];
            A42Contratada_PessoaCNPJ = P00IP19_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP19_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00IP19_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00IP19_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP19_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00IP19_A77Contrato_Numero[0];
            A39Contratada_Codigo = P00IP19_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00IP19_A75Contrato_AreaTrabalhoCod[0];
            A1870Contrato_ValorUndCntAtual = P00IP19_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP19_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP19_n1869Contrato_DataTermino[0];
            A1870Contrato_ValorUndCntAtual = P00IP19_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP19_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP19_n1869Contrato_DataTermino[0];
            A40Contratada_PessoaCod = P00IP19_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00IP19_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP19_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00IP19_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP19_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00IP19_A78Contrato_NumeroAta[0], A78Contrato_NumeroAta) == 0 ) )
            {
               BRKIP6 = false;
               A74Contrato_Codigo = P00IP19_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00IP19_n74Contrato_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKIP6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A78Contrato_NumeroAta)) )
            {
               AV28Option = A78Contrato_NumeroAta;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIP6 )
            {
               BRKIP6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV22TFContratada_PessoaCNPJ = AV24SearchTxt;
         AV23TFContratada_PessoaCNPJ_Sel = "";
         AV84WWContratoDS_1_Contrato_areatrabalhocod = AV42Contrato_AreaTrabalhoCod;
         AV85WWContratoDS_2_Dynamicfiltersselector1 = AV43DynamicFiltersSelector1;
         AV86WWContratoDS_3_Contrato_numero1 = AV44Contrato_Numero1;
         AV87WWContratoDS_4_Contrato_numeroata1 = AV45Contrato_NumeroAta1;
         AV88WWContratoDS_5_Contratada_pessoanom1 = AV46Contratada_PessoaNom1;
         AV89WWContratoDS_6_Contrato_ano1 = AV47Contrato_Ano1;
         AV90WWContratoDS_7_Contrato_ano_to1 = AV48Contrato_Ano_To1;
         AV91WWContratoDS_8_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV92WWContratoDS_9_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV93WWContratoDS_10_Contrato_numero2 = AV51Contrato_Numero2;
         AV94WWContratoDS_11_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV95WWContratoDS_12_Contratada_pessoanom2 = AV53Contratada_PessoaNom2;
         AV96WWContratoDS_13_Contrato_ano2 = AV54Contrato_Ano2;
         AV97WWContratoDS_14_Contrato_ano_to2 = AV55Contrato_Ano_To2;
         AV98WWContratoDS_15_Dynamicfiltersenabled3 = AV56DynamicFiltersEnabled3;
         AV99WWContratoDS_16_Dynamicfiltersselector3 = AV57DynamicFiltersSelector3;
         AV100WWContratoDS_17_Contrato_numero3 = AV58Contrato_Numero3;
         AV101WWContratoDS_18_Contrato_numeroata3 = AV59Contrato_NumeroAta3;
         AV102WWContratoDS_19_Contratada_pessoanom3 = AV60Contratada_PessoaNom3;
         AV103WWContratoDS_20_Contrato_ano3 = AV61Contrato_Ano3;
         AV104WWContratoDS_21_Contrato_ano_to3 = AV62Contrato_Ano_To3;
         AV105WWContratoDS_22_Tfcontratada_pessoanom = AV20TFContratada_PessoaNom;
         AV106WWContratoDS_23_Tfcontratada_pessoanom_sel = AV21TFContratada_PessoaNom_Sel;
         AV107WWContratoDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV108WWContratoDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV109WWContratoDS_26_Tfcontrato_numeroata = AV12TFContrato_NumeroAta;
         AV110WWContratoDS_27_Tfcontrato_numeroata_sel = AV13TFContrato_NumeroAta_Sel;
         AV111WWContratoDS_28_Tfcontrato_ano = AV14TFContrato_Ano;
         AV112WWContratoDS_29_Tfcontrato_ano_to = AV15TFContrato_Ano_To;
         AV113WWContratoDS_30_Tfcontrato_datatermino = AV64TFContrato_DataTermino;
         AV114WWContratoDS_31_Tfcontrato_datatermino_to = AV65TFContrato_DataTermino_To;
         AV115WWContratoDS_32_Tfcontrato_valorundcntatual = AV66TFContrato_ValorUndCntAtual;
         AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = AV67TFContrato_ValorUndCntAtual_To;
         AV117WWContratoDS_34_Tfcontratada_pessoacnpj = AV22TFContratada_PessoaCNPJ;
         AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel = AV23TFContratada_PessoaCNPJ_Sel;
         AV119WWContratoDS_36_Tfcontrato_ativo_sel = AV63TFContrato_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              AV86WWContratoDS_3_Contrato_numero1 ,
                                              AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              AV89WWContratoDS_6_Contrato_ano1 ,
                                              AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              AV93WWContratoDS_10_Contrato_numero2 ,
                                              AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              AV96WWContratoDS_13_Contrato_ano2 ,
                                              AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              AV100WWContratoDS_17_Contrato_numero3 ,
                                              AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              AV103WWContratoDS_20_Contrato_ano3 ,
                                              AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              AV107WWContratoDS_24_Tfcontrato_numero ,
                                              AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              AV111WWContratoDS_28_Tfcontrato_ano ,
                                              AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A92Contrato_Ativo ,
                                              AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              A1869Contrato_DataTermino ,
                                              AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              A1870Contrato_ValorUndCntAtual ,
                                              AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV86WWContratoDS_3_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1), 20, "%");
         lV87WWContratoDS_4_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1), 10, "%");
         lV88WWContratoDS_5_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1), 100, "%");
         lV93WWContratoDS_10_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2), 20, "%");
         lV94WWContratoDS_11_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2), 10, "%");
         lV95WWContratoDS_12_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2), 100, "%");
         lV100WWContratoDS_17_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3), 20, "%");
         lV101WWContratoDS_18_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3), 10, "%");
         lV102WWContratoDS_19_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3), 100, "%");
         lV105WWContratoDS_22_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom), 100, "%");
         lV107WWContratoDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero), 20, "%");
         lV109WWContratoDS_26_Tfcontrato_numeroata = StringUtil.PadR( StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata), 10, "%");
         lV117WWContratoDS_34_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor P00IP25 */
         pr_default.execute(3, new Object[] {AV113WWContratoDS_30_Tfcontrato_datatermino, AV113WWContratoDS_30_Tfcontrato_datatermino, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV114WWContratoDS_31_Tfcontrato_datatermino_to, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV115WWContratoDS_32_Tfcontrato_valorundcntatual, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to, AV84WWContratoDS_1_Contrato_areatrabalhocod, AV9WWPContext.gxTpr_Contratada_codigo, lV86WWContratoDS_3_Contrato_numero1, lV87WWContratoDS_4_Contrato_numeroata1, lV88WWContratoDS_5_Contratada_pessoanom1, AV89WWContratoDS_6_Contrato_ano1, AV90WWContratoDS_7_Contrato_ano_to1, lV93WWContratoDS_10_Contrato_numero2, lV94WWContratoDS_11_Contrato_numeroata2, lV95WWContratoDS_12_Contratada_pessoanom2, AV96WWContratoDS_13_Contrato_ano2, AV97WWContratoDS_14_Contrato_ano_to2, lV100WWContratoDS_17_Contrato_numero3, lV101WWContratoDS_18_Contrato_numeroata3, lV102WWContratoDS_19_Contratada_pessoanom3, AV103WWContratoDS_20_Contrato_ano3, AV104WWContratoDS_21_Contrato_ano_to3, lV105WWContratoDS_22_Tfcontratada_pessoanom, AV106WWContratoDS_23_Tfcontratada_pessoanom_sel, lV107WWContratoDS_24_Tfcontrato_numero, AV108WWContratoDS_25_Tfcontrato_numero_sel, lV109WWContratoDS_26_Tfcontrato_numeroata, AV110WWContratoDS_27_Tfcontrato_numeroata_sel, AV111WWContratoDS_28_Tfcontrato_ano, AV112WWContratoDS_29_Tfcontrato_ano_to, lV117WWContratoDS_34_Tfcontratada_pessoacnpj, AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKIP8 = false;
            A40Contratada_PessoaCod = P00IP25_A40Contratada_PessoaCod[0];
            A74Contrato_Codigo = P00IP25_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00IP25_n74Contrato_Codigo[0];
            A42Contratada_PessoaCNPJ = P00IP25_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP25_n42Contratada_PessoaCNPJ[0];
            A92Contrato_Ativo = P00IP25_A92Contrato_Ativo[0];
            A79Contrato_Ano = P00IP25_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00IP25_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP25_n41Contratada_PessoaNom[0];
            A78Contrato_NumeroAta = P00IP25_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00IP25_n78Contrato_NumeroAta[0];
            A77Contrato_Numero = P00IP25_A77Contrato_Numero[0];
            A39Contratada_Codigo = P00IP25_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00IP25_A75Contrato_AreaTrabalhoCod[0];
            A1870Contrato_ValorUndCntAtual = P00IP25_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP25_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP25_n1869Contrato_DataTermino[0];
            A1870Contrato_ValorUndCntAtual = P00IP25_A1870Contrato_ValorUndCntAtual[0];
            A1869Contrato_DataTermino = P00IP25_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = P00IP25_n1869Contrato_DataTermino[0];
            A40Contratada_PessoaCod = P00IP25_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00IP25_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00IP25_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00IP25_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00IP25_n41Contratada_PessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00IP25_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKIP8 = false;
               A40Contratada_PessoaCod = P00IP25_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = P00IP25_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00IP25_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00IP25_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00IP25_A40Contratada_PessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKIP8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV28Option = A42Contratada_PessoaCNPJ;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIP8 )
            {
               BRKIP8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV20TFContratada_PessoaNom = "";
         AV21TFContratada_PessoaNom_Sel = "";
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContrato_NumeroAta = "";
         AV13TFContrato_NumeroAta_Sel = "";
         AV64TFContrato_DataTermino = DateTime.MinValue;
         AV65TFContrato_DataTermino_To = DateTime.MinValue;
         AV22TFContratada_PessoaCNPJ = "";
         AV23TFContratada_PessoaCNPJ_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV43DynamicFiltersSelector1 = "";
         AV44Contrato_Numero1 = "";
         AV45Contrato_NumeroAta1 = "";
         AV46Contratada_PessoaNom1 = "";
         AV50DynamicFiltersSelector2 = "";
         AV51Contrato_Numero2 = "";
         AV52Contrato_NumeroAta2 = "";
         AV53Contratada_PessoaNom2 = "";
         AV57DynamicFiltersSelector3 = "";
         AV58Contrato_Numero3 = "";
         AV59Contrato_NumeroAta3 = "";
         AV60Contratada_PessoaNom3 = "";
         AV85WWContratoDS_2_Dynamicfiltersselector1 = "";
         AV86WWContratoDS_3_Contrato_numero1 = "";
         AV87WWContratoDS_4_Contrato_numeroata1 = "";
         AV88WWContratoDS_5_Contratada_pessoanom1 = "";
         AV92WWContratoDS_9_Dynamicfiltersselector2 = "";
         AV93WWContratoDS_10_Contrato_numero2 = "";
         AV94WWContratoDS_11_Contrato_numeroata2 = "";
         AV95WWContratoDS_12_Contratada_pessoanom2 = "";
         AV99WWContratoDS_16_Dynamicfiltersselector3 = "";
         AV100WWContratoDS_17_Contrato_numero3 = "";
         AV101WWContratoDS_18_Contrato_numeroata3 = "";
         AV102WWContratoDS_19_Contratada_pessoanom3 = "";
         AV105WWContratoDS_22_Tfcontratada_pessoanom = "";
         AV106WWContratoDS_23_Tfcontratada_pessoanom_sel = "";
         AV107WWContratoDS_24_Tfcontrato_numero = "";
         AV108WWContratoDS_25_Tfcontrato_numero_sel = "";
         AV109WWContratoDS_26_Tfcontrato_numeroata = "";
         AV110WWContratoDS_27_Tfcontrato_numeroata_sel = "";
         AV113WWContratoDS_30_Tfcontrato_datatermino = DateTime.MinValue;
         AV114WWContratoDS_31_Tfcontrato_datatermino_to = DateTime.MinValue;
         AV117WWContratoDS_34_Tfcontratada_pessoacnpj = "";
         AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel = "";
         scmdbuf = "";
         lV86WWContratoDS_3_Contrato_numero1 = "";
         lV87WWContratoDS_4_Contrato_numeroata1 = "";
         lV88WWContratoDS_5_Contratada_pessoanom1 = "";
         lV93WWContratoDS_10_Contrato_numero2 = "";
         lV94WWContratoDS_11_Contrato_numeroata2 = "";
         lV95WWContratoDS_12_Contratada_pessoanom2 = "";
         lV100WWContratoDS_17_Contrato_numero3 = "";
         lV101WWContratoDS_18_Contrato_numeroata3 = "";
         lV102WWContratoDS_19_Contratada_pessoanom3 = "";
         lV105WWContratoDS_22_Tfcontratada_pessoanom = "";
         lV107WWContratoDS_24_Tfcontrato_numero = "";
         lV109WWContratoDS_26_Tfcontrato_numeroata = "";
         lV117WWContratoDS_34_Tfcontratada_pessoacnpj = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         P00IP7_A40Contratada_PessoaCod = new int[1] ;
         P00IP7_A74Contrato_Codigo = new int[1] ;
         P00IP7_n74Contrato_Codigo = new bool[] {false} ;
         P00IP7_A41Contratada_PessoaNom = new String[] {""} ;
         P00IP7_n41Contratada_PessoaNom = new bool[] {false} ;
         P00IP7_A92Contrato_Ativo = new bool[] {false} ;
         P00IP7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00IP7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00IP7_A79Contrato_Ano = new short[1] ;
         P00IP7_A78Contrato_NumeroAta = new String[] {""} ;
         P00IP7_n78Contrato_NumeroAta = new bool[] {false} ;
         P00IP7_A77Contrato_Numero = new String[] {""} ;
         P00IP7_A39Contratada_Codigo = new int[1] ;
         P00IP7_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00IP7_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00IP7_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         P00IP7_n1869Contrato_DataTermino = new bool[] {false} ;
         AV28Option = "";
         P00IP13_A40Contratada_PessoaCod = new int[1] ;
         P00IP13_A74Contrato_Codigo = new int[1] ;
         P00IP13_n74Contrato_Codigo = new bool[] {false} ;
         P00IP13_A77Contrato_Numero = new String[] {""} ;
         P00IP13_A92Contrato_Ativo = new bool[] {false} ;
         P00IP13_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00IP13_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00IP13_A79Contrato_Ano = new short[1] ;
         P00IP13_A41Contratada_PessoaNom = new String[] {""} ;
         P00IP13_n41Contratada_PessoaNom = new bool[] {false} ;
         P00IP13_A78Contrato_NumeroAta = new String[] {""} ;
         P00IP13_n78Contrato_NumeroAta = new bool[] {false} ;
         P00IP13_A39Contratada_Codigo = new int[1] ;
         P00IP13_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00IP13_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00IP13_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         P00IP13_n1869Contrato_DataTermino = new bool[] {false} ;
         P00IP19_A40Contratada_PessoaCod = new int[1] ;
         P00IP19_A74Contrato_Codigo = new int[1] ;
         P00IP19_n74Contrato_Codigo = new bool[] {false} ;
         P00IP19_A78Contrato_NumeroAta = new String[] {""} ;
         P00IP19_n78Contrato_NumeroAta = new bool[] {false} ;
         P00IP19_A92Contrato_Ativo = new bool[] {false} ;
         P00IP19_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00IP19_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00IP19_A79Contrato_Ano = new short[1] ;
         P00IP19_A41Contratada_PessoaNom = new String[] {""} ;
         P00IP19_n41Contratada_PessoaNom = new bool[] {false} ;
         P00IP19_A77Contrato_Numero = new String[] {""} ;
         P00IP19_A39Contratada_Codigo = new int[1] ;
         P00IP19_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00IP19_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00IP19_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         P00IP19_n1869Contrato_DataTermino = new bool[] {false} ;
         P00IP25_A40Contratada_PessoaCod = new int[1] ;
         P00IP25_A74Contrato_Codigo = new int[1] ;
         P00IP25_n74Contrato_Codigo = new bool[] {false} ;
         P00IP25_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00IP25_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00IP25_A92Contrato_Ativo = new bool[] {false} ;
         P00IP25_A79Contrato_Ano = new short[1] ;
         P00IP25_A41Contratada_PessoaNom = new String[] {""} ;
         P00IP25_n41Contratada_PessoaNom = new bool[] {false} ;
         P00IP25_A78Contrato_NumeroAta = new String[] {""} ;
         P00IP25_n78Contrato_NumeroAta = new bool[] {false} ;
         P00IP25_A77Contrato_Numero = new String[] {""} ;
         P00IP25_A39Contratada_Codigo = new int[1] ;
         P00IP25_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00IP25_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         P00IP25_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         P00IP25_n1869Contrato_DataTermino = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00IP7_A40Contratada_PessoaCod, P00IP7_A74Contrato_Codigo, P00IP7_A41Contratada_PessoaNom, P00IP7_n41Contratada_PessoaNom, P00IP7_A92Contrato_Ativo, P00IP7_A42Contratada_PessoaCNPJ, P00IP7_n42Contratada_PessoaCNPJ, P00IP7_A79Contrato_Ano, P00IP7_A78Contrato_NumeroAta, P00IP7_n78Contrato_NumeroAta,
               P00IP7_A77Contrato_Numero, P00IP7_A39Contratada_Codigo, P00IP7_A75Contrato_AreaTrabalhoCod, P00IP7_A1870Contrato_ValorUndCntAtual, P00IP7_A1869Contrato_DataTermino, P00IP7_n1869Contrato_DataTermino
               }
               , new Object[] {
               P00IP13_A40Contratada_PessoaCod, P00IP13_A74Contrato_Codigo, P00IP13_A77Contrato_Numero, P00IP13_A92Contrato_Ativo, P00IP13_A42Contratada_PessoaCNPJ, P00IP13_n42Contratada_PessoaCNPJ, P00IP13_A79Contrato_Ano, P00IP13_A41Contratada_PessoaNom, P00IP13_n41Contratada_PessoaNom, P00IP13_A78Contrato_NumeroAta,
               P00IP13_n78Contrato_NumeroAta, P00IP13_A39Contratada_Codigo, P00IP13_A75Contrato_AreaTrabalhoCod, P00IP13_A1870Contrato_ValorUndCntAtual, P00IP13_A1869Contrato_DataTermino, P00IP13_n1869Contrato_DataTermino
               }
               , new Object[] {
               P00IP19_A40Contratada_PessoaCod, P00IP19_A74Contrato_Codigo, P00IP19_A78Contrato_NumeroAta, P00IP19_n78Contrato_NumeroAta, P00IP19_A92Contrato_Ativo, P00IP19_A42Contratada_PessoaCNPJ, P00IP19_n42Contratada_PessoaCNPJ, P00IP19_A79Contrato_Ano, P00IP19_A41Contratada_PessoaNom, P00IP19_n41Contratada_PessoaNom,
               P00IP19_A77Contrato_Numero, P00IP19_A39Contratada_Codigo, P00IP19_A75Contrato_AreaTrabalhoCod, P00IP19_A1870Contrato_ValorUndCntAtual, P00IP19_A1869Contrato_DataTermino, P00IP19_n1869Contrato_DataTermino
               }
               , new Object[] {
               P00IP25_A40Contratada_PessoaCod, P00IP25_A74Contrato_Codigo, P00IP25_A42Contratada_PessoaCNPJ, P00IP25_n42Contratada_PessoaCNPJ, P00IP25_A92Contrato_Ativo, P00IP25_A79Contrato_Ano, P00IP25_A41Contratada_PessoaNom, P00IP25_n41Contratada_PessoaNom, P00IP25_A78Contrato_NumeroAta, P00IP25_n78Contrato_NumeroAta,
               P00IP25_A77Contrato_Numero, P00IP25_A39Contratada_Codigo, P00IP25_A75Contrato_AreaTrabalhoCod, P00IP25_A1870Contrato_ValorUndCntAtual, P00IP25_A1869Contrato_DataTermino, P00IP25_n1869Contrato_DataTermino
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFContrato_Ano ;
      private short AV15TFContrato_Ano_To ;
      private short AV63TFContrato_Ativo_Sel ;
      private short AV47Contrato_Ano1 ;
      private short AV48Contrato_Ano_To1 ;
      private short AV54Contrato_Ano2 ;
      private short AV55Contrato_Ano_To2 ;
      private short AV61Contrato_Ano3 ;
      private short AV62Contrato_Ano_To3 ;
      private short AV89WWContratoDS_6_Contrato_ano1 ;
      private short AV90WWContratoDS_7_Contrato_ano_to1 ;
      private short AV96WWContratoDS_13_Contrato_ano2 ;
      private short AV97WWContratoDS_14_Contrato_ano_to2 ;
      private short AV103WWContratoDS_20_Contrato_ano3 ;
      private short AV104WWContratoDS_21_Contrato_ano_to3 ;
      private short AV111WWContratoDS_28_Tfcontrato_ano ;
      private short AV112WWContratoDS_29_Tfcontrato_ano_to ;
      private short AV119WWContratoDS_36_Tfcontrato_ativo_sel ;
      private short A79Contrato_Ano ;
      private int AV82GXV1 ;
      private int AV42Contrato_AreaTrabalhoCod ;
      private int AV84WWContratoDS_1_Contrato_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A74Contrato_Codigo ;
      private long AV36count ;
      private decimal AV66TFContrato_ValorUndCntAtual ;
      private decimal AV67TFContrato_ValorUndCntAtual_To ;
      private decimal AV115WWContratoDS_32_Tfcontrato_valorundcntatual ;
      private decimal AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to ;
      private decimal A1870Contrato_ValorUndCntAtual ;
      private String AV20TFContratada_PessoaNom ;
      private String AV21TFContratada_PessoaNom_Sel ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContrato_NumeroAta ;
      private String AV13TFContrato_NumeroAta_Sel ;
      private String AV44Contrato_Numero1 ;
      private String AV45Contrato_NumeroAta1 ;
      private String AV46Contratada_PessoaNom1 ;
      private String AV51Contrato_Numero2 ;
      private String AV52Contrato_NumeroAta2 ;
      private String AV53Contratada_PessoaNom2 ;
      private String AV58Contrato_Numero3 ;
      private String AV59Contrato_NumeroAta3 ;
      private String AV60Contratada_PessoaNom3 ;
      private String AV86WWContratoDS_3_Contrato_numero1 ;
      private String AV87WWContratoDS_4_Contrato_numeroata1 ;
      private String AV88WWContratoDS_5_Contratada_pessoanom1 ;
      private String AV93WWContratoDS_10_Contrato_numero2 ;
      private String AV94WWContratoDS_11_Contrato_numeroata2 ;
      private String AV95WWContratoDS_12_Contratada_pessoanom2 ;
      private String AV100WWContratoDS_17_Contrato_numero3 ;
      private String AV101WWContratoDS_18_Contrato_numeroata3 ;
      private String AV102WWContratoDS_19_Contratada_pessoanom3 ;
      private String AV105WWContratoDS_22_Tfcontratada_pessoanom ;
      private String AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ;
      private String AV107WWContratoDS_24_Tfcontrato_numero ;
      private String AV108WWContratoDS_25_Tfcontrato_numero_sel ;
      private String AV109WWContratoDS_26_Tfcontrato_numeroata ;
      private String AV110WWContratoDS_27_Tfcontrato_numeroata_sel ;
      private String scmdbuf ;
      private String lV86WWContratoDS_3_Contrato_numero1 ;
      private String lV87WWContratoDS_4_Contrato_numeroata1 ;
      private String lV88WWContratoDS_5_Contratada_pessoanom1 ;
      private String lV93WWContratoDS_10_Contrato_numero2 ;
      private String lV94WWContratoDS_11_Contrato_numeroata2 ;
      private String lV95WWContratoDS_12_Contratada_pessoanom2 ;
      private String lV100WWContratoDS_17_Contrato_numero3 ;
      private String lV101WWContratoDS_18_Contrato_numeroata3 ;
      private String lV102WWContratoDS_19_Contratada_pessoanom3 ;
      private String lV105WWContratoDS_22_Tfcontratada_pessoanom ;
      private String lV107WWContratoDS_24_Tfcontrato_numero ;
      private String lV109WWContratoDS_26_Tfcontrato_numeroata ;
      private String A77Contrato_Numero ;
      private String A78Contrato_NumeroAta ;
      private String A41Contratada_PessoaNom ;
      private DateTime AV64TFContrato_DataTermino ;
      private DateTime AV65TFContrato_DataTermino_To ;
      private DateTime AV113WWContratoDS_30_Tfcontrato_datatermino ;
      private DateTime AV114WWContratoDS_31_Tfcontrato_datatermino_to ;
      private DateTime A1869Contrato_DataTermino ;
      private bool returnInSub ;
      private bool AV49DynamicFiltersEnabled2 ;
      private bool AV56DynamicFiltersEnabled3 ;
      private bool AV91WWContratoDS_8_Dynamicfiltersenabled2 ;
      private bool AV98WWContratoDS_15_Dynamicfiltersenabled3 ;
      private bool A92Contrato_Ativo ;
      private bool BRKIP2 ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n78Contrato_NumeroAta ;
      private bool n1869Contrato_DataTermino ;
      private bool BRKIP4 ;
      private bool BRKIP6 ;
      private bool BRKIP8 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV22TFContratada_PessoaCNPJ ;
      private String AV23TFContratada_PessoaCNPJ_Sel ;
      private String AV43DynamicFiltersSelector1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String AV57DynamicFiltersSelector3 ;
      private String AV85WWContratoDS_2_Dynamicfiltersselector1 ;
      private String AV92WWContratoDS_9_Dynamicfiltersselector2 ;
      private String AV99WWContratoDS_16_Dynamicfiltersselector3 ;
      private String AV117WWContratoDS_34_Tfcontratada_pessoacnpj ;
      private String AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ;
      private String lV117WWContratoDS_34_Tfcontratada_pessoacnpj ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00IP7_A40Contratada_PessoaCod ;
      private int[] P00IP7_A74Contrato_Codigo ;
      private bool[] P00IP7_n74Contrato_Codigo ;
      private String[] P00IP7_A41Contratada_PessoaNom ;
      private bool[] P00IP7_n41Contratada_PessoaNom ;
      private bool[] P00IP7_A92Contrato_Ativo ;
      private String[] P00IP7_A42Contratada_PessoaCNPJ ;
      private bool[] P00IP7_n42Contratada_PessoaCNPJ ;
      private short[] P00IP7_A79Contrato_Ano ;
      private String[] P00IP7_A78Contrato_NumeroAta ;
      private bool[] P00IP7_n78Contrato_NumeroAta ;
      private String[] P00IP7_A77Contrato_Numero ;
      private int[] P00IP7_A39Contratada_Codigo ;
      private int[] P00IP7_A75Contrato_AreaTrabalhoCod ;
      private decimal[] P00IP7_A1870Contrato_ValorUndCntAtual ;
      private DateTime[] P00IP7_A1869Contrato_DataTermino ;
      private bool[] P00IP7_n1869Contrato_DataTermino ;
      private int[] P00IP13_A40Contratada_PessoaCod ;
      private int[] P00IP13_A74Contrato_Codigo ;
      private bool[] P00IP13_n74Contrato_Codigo ;
      private String[] P00IP13_A77Contrato_Numero ;
      private bool[] P00IP13_A92Contrato_Ativo ;
      private String[] P00IP13_A42Contratada_PessoaCNPJ ;
      private bool[] P00IP13_n42Contratada_PessoaCNPJ ;
      private short[] P00IP13_A79Contrato_Ano ;
      private String[] P00IP13_A41Contratada_PessoaNom ;
      private bool[] P00IP13_n41Contratada_PessoaNom ;
      private String[] P00IP13_A78Contrato_NumeroAta ;
      private bool[] P00IP13_n78Contrato_NumeroAta ;
      private int[] P00IP13_A39Contratada_Codigo ;
      private int[] P00IP13_A75Contrato_AreaTrabalhoCod ;
      private decimal[] P00IP13_A1870Contrato_ValorUndCntAtual ;
      private DateTime[] P00IP13_A1869Contrato_DataTermino ;
      private bool[] P00IP13_n1869Contrato_DataTermino ;
      private int[] P00IP19_A40Contratada_PessoaCod ;
      private int[] P00IP19_A74Contrato_Codigo ;
      private bool[] P00IP19_n74Contrato_Codigo ;
      private String[] P00IP19_A78Contrato_NumeroAta ;
      private bool[] P00IP19_n78Contrato_NumeroAta ;
      private bool[] P00IP19_A92Contrato_Ativo ;
      private String[] P00IP19_A42Contratada_PessoaCNPJ ;
      private bool[] P00IP19_n42Contratada_PessoaCNPJ ;
      private short[] P00IP19_A79Contrato_Ano ;
      private String[] P00IP19_A41Contratada_PessoaNom ;
      private bool[] P00IP19_n41Contratada_PessoaNom ;
      private String[] P00IP19_A77Contrato_Numero ;
      private int[] P00IP19_A39Contratada_Codigo ;
      private int[] P00IP19_A75Contrato_AreaTrabalhoCod ;
      private decimal[] P00IP19_A1870Contrato_ValorUndCntAtual ;
      private DateTime[] P00IP19_A1869Contrato_DataTermino ;
      private bool[] P00IP19_n1869Contrato_DataTermino ;
      private int[] P00IP25_A40Contratada_PessoaCod ;
      private int[] P00IP25_A74Contrato_Codigo ;
      private bool[] P00IP25_n74Contrato_Codigo ;
      private String[] P00IP25_A42Contratada_PessoaCNPJ ;
      private bool[] P00IP25_n42Contratada_PessoaCNPJ ;
      private bool[] P00IP25_A92Contrato_Ativo ;
      private short[] P00IP25_A79Contrato_Ano ;
      private String[] P00IP25_A41Contratada_PessoaNom ;
      private bool[] P00IP25_n41Contratada_PessoaNom ;
      private String[] P00IP25_A78Contrato_NumeroAta ;
      private bool[] P00IP25_n78Contrato_NumeroAta ;
      private String[] P00IP25_A77Contrato_Numero ;
      private int[] P00IP25_A39Contratada_Codigo ;
      private int[] P00IP25_A75Contrato_AreaTrabalhoCod ;
      private decimal[] P00IP25_A1870Contrato_ValorUndCntAtual ;
      private DateTime[] P00IP25_A1869Contrato_DataTermino ;
      private bool[] P00IP25_n1869Contrato_DataTermino ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwcontratofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00IP7( IGxContext context ,
                                             int AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                             String AV86WWContratoDS_3_Contrato_numero1 ,
                                             String AV87WWContratoDS_4_Contrato_numeroata1 ,
                                             String AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                             short AV89WWContratoDS_6_Contrato_ano1 ,
                                             short AV90WWContratoDS_7_Contrato_ano_to1 ,
                                             bool AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                             String AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                             String AV93WWContratoDS_10_Contrato_numero2 ,
                                             String AV94WWContratoDS_11_Contrato_numeroata2 ,
                                             String AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                             short AV96WWContratoDS_13_Contrato_ano2 ,
                                             short AV97WWContratoDS_14_Contrato_ano_to2 ,
                                             bool AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                             String AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                             String AV100WWContratoDS_17_Contrato_numero3 ,
                                             String AV101WWContratoDS_18_Contrato_numeroata3 ,
                                             String AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                             short AV103WWContratoDS_20_Contrato_ano3 ,
                                             short AV104WWContratoDS_21_Contrato_ano_to3 ,
                                             String AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                             String AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                             String AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                             String AV107WWContratoDS_24_Tfcontrato_numero ,
                                             String AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                             String AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                             short AV111WWContratoDS_28_Tfcontrato_ano ,
                                             short AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                             String AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                             String AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                             short AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                             int A75Contrato_AreaTrabalhoCod ,
                                             int A39Contratada_Codigo ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             String A42Contratada_PessoaCNPJ ,
                                             bool A92Contrato_Ativo ,
                                             DateTime AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                             DateTime A1869Contrato_DataTermino ,
                                             DateTime AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                             decimal AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                             decimal A1870Contrato_ValorUndCntAtual ,
                                             decimal AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [35] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ativo], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], COALESCE( T2.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT CASE  WHEN T5.[Contrato_DataVigenciaTermino] > COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T5.[Contrato_DataVigenciaTermino] ELSE COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T5.[Contrato_Codigo], CASE  WHEN COALESCE( T7.[GXC3], 0) > 0 THEN COALESCE( T7.[GXC3], 0) ELSE T5.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual FROM (([Contrato] T5 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC7] AS GXC7 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC7, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC7] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC3, T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH";
         scmdbuf = scmdbuf + " (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC5] ) T7 ON T7.[Contrato_Codigo] = T5.[Contrato_Codigo]) ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE ((@AV113WWContratoDS_30_Tfcontrato_datatermino = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) >= @AV113WWContratoDS_30_Tfcontrato_datatermino))";
         scmdbuf = scmdbuf + " and ((@AV114WWContratoDS_31_Tfcontrato_datatermino_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) <= @AV114WWContratoDS_31_Tfcontrato_datatermino_to))";
         scmdbuf = scmdbuf + " and ((@AV115WWContratoDS_32_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) >= @AV115WWContratoDS_32_Tfcontrato_valorundcntatual))";
         scmdbuf = scmdbuf + " and ((@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) <= @AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV84WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV84WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV86WWContratoDS_3_Contrato_numero1)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV87WWContratoDS_4_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV88WWContratoDS_5_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV89WWContratoDS_6_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV89WWContratoDS_6_Contrato_ano1)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV90WWContratoDS_7_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV90WWContratoDS_7_Contrato_ano_to1)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV93WWContratoDS_10_Contrato_numero2)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV94WWContratoDS_11_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV95WWContratoDS_12_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV96WWContratoDS_13_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV96WWContratoDS_13_Contrato_ano2)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV97WWContratoDS_14_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV97WWContratoDS_14_Contrato_ano_to2)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV100WWContratoDS_17_Contrato_numero3)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV101WWContratoDS_18_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV102WWContratoDS_19_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV103WWContratoDS_20_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV103WWContratoDS_20_Contrato_ano3)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV104WWContratoDS_21_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV104WWContratoDS_21_Contrato_ano_to3)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoDS_22_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV107WWContratoDS_24_Tfcontrato_numero)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV108WWContratoDS_25_Tfcontrato_numero_sel)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV109WWContratoDS_26_Tfcontrato_numeroata)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV110WWContratoDS_27_Tfcontrato_numeroata_sel)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (0==AV111WWContratoDS_28_Tfcontrato_ano) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV111WWContratoDS_28_Tfcontrato_ano)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! (0==AV112WWContratoDS_29_Tfcontrato_ano_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV112WWContratoDS_29_Tfcontrato_ano_to)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV117WWContratoDS_34_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00IP13( IGxContext context ,
                                              int AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              String AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              String AV86WWContratoDS_3_Contrato_numero1 ,
                                              String AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              String AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              short AV89WWContratoDS_6_Contrato_ano1 ,
                                              short AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              bool AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              String AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              String AV93WWContratoDS_10_Contrato_numero2 ,
                                              String AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              String AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              short AV96WWContratoDS_13_Contrato_ano2 ,
                                              short AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              bool AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              String AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              String AV100WWContratoDS_17_Contrato_numero3 ,
                                              String AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              String AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              short AV103WWContratoDS_20_Contrato_ano3 ,
                                              short AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              String AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              String AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              String AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              String AV107WWContratoDS_24_Tfcontrato_numero ,
                                              String AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              String AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              short AV111WWContratoDS_28_Tfcontrato_ano ,
                                              short AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              String AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              String AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              short AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A39Contratada_Codigo ,
                                              String A77Contrato_Numero ,
                                              String A78Contrato_NumeroAta ,
                                              String A41Contratada_PessoaNom ,
                                              short A79Contrato_Ano ,
                                              String A42Contratada_PessoaCNPJ ,
                                              bool A92Contrato_Ativo ,
                                              DateTime AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              DateTime A1869Contrato_DataTermino ,
                                              DateTime AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              decimal AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              decimal A1870Contrato_ValorUndCntAtual ,
                                              decimal AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [35] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T1.[Contrato_Numero], T1.[Contrato_Ativo], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_NumeroAta], T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], COALESCE( T2.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT CASE  WHEN T5.[Contrato_DataVigenciaTermino] > COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T5.[Contrato_DataVigenciaTermino] ELSE COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T5.[Contrato_Codigo], CASE  WHEN COALESCE( T7.[GXC3], 0) > 0 THEN COALESCE( T7.[GXC3], 0) ELSE T5.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual FROM (([Contrato] T5 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC7] AS GXC7 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC7, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC7] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC3, T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH";
         scmdbuf = scmdbuf + " (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC5] ) T7 ON T7.[Contrato_Codigo] = T5.[Contrato_Codigo]) ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE ((@AV113WWContratoDS_30_Tfcontrato_datatermino = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) >= @AV113WWContratoDS_30_Tfcontrato_datatermino))";
         scmdbuf = scmdbuf + " and ((@AV114WWContratoDS_31_Tfcontrato_datatermino_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) <= @AV114WWContratoDS_31_Tfcontrato_datatermino_to))";
         scmdbuf = scmdbuf + " and ((@AV115WWContratoDS_32_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) >= @AV115WWContratoDS_32_Tfcontrato_valorundcntatual))";
         scmdbuf = scmdbuf + " and ((@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) <= @AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV84WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV84WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV86WWContratoDS_3_Contrato_numero1)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV87WWContratoDS_4_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV88WWContratoDS_5_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV89WWContratoDS_6_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV89WWContratoDS_6_Contrato_ano1)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV90WWContratoDS_7_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV90WWContratoDS_7_Contrato_ano_to1)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV93WWContratoDS_10_Contrato_numero2)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV94WWContratoDS_11_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV95WWContratoDS_12_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV96WWContratoDS_13_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV96WWContratoDS_13_Contrato_ano2)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV97WWContratoDS_14_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV97WWContratoDS_14_Contrato_ano_to2)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV100WWContratoDS_17_Contrato_numero3)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV101WWContratoDS_18_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV102WWContratoDS_19_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV103WWContratoDS_20_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV103WWContratoDS_20_Contrato_ano3)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV104WWContratoDS_21_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV104WWContratoDS_21_Contrato_ano_to3)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoDS_22_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV107WWContratoDS_24_Tfcontrato_numero)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV108WWContratoDS_25_Tfcontrato_numero_sel)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV109WWContratoDS_26_Tfcontrato_numeroata)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV110WWContratoDS_27_Tfcontrato_numeroata_sel)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (0==AV111WWContratoDS_28_Tfcontrato_ano) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV111WWContratoDS_28_Tfcontrato_ano)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (0==AV112WWContratoDS_29_Tfcontrato_ano_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV112WWContratoDS_29_Tfcontrato_ano_to)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV117WWContratoDS_34_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Numero]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00IP19( IGxContext context ,
                                              int AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              String AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              String AV86WWContratoDS_3_Contrato_numero1 ,
                                              String AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              String AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              short AV89WWContratoDS_6_Contrato_ano1 ,
                                              short AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              bool AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              String AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              String AV93WWContratoDS_10_Contrato_numero2 ,
                                              String AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              String AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              short AV96WWContratoDS_13_Contrato_ano2 ,
                                              short AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              bool AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              String AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              String AV100WWContratoDS_17_Contrato_numero3 ,
                                              String AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              String AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              short AV103WWContratoDS_20_Contrato_ano3 ,
                                              short AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              String AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              String AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              String AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              String AV107WWContratoDS_24_Tfcontrato_numero ,
                                              String AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              String AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              short AV111WWContratoDS_28_Tfcontrato_ano ,
                                              short AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              String AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              String AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              short AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A39Contratada_Codigo ,
                                              String A77Contrato_Numero ,
                                              String A78Contrato_NumeroAta ,
                                              String A41Contratada_PessoaNom ,
                                              short A79Contrato_Ano ,
                                              String A42Contratada_PessoaCNPJ ,
                                              bool A92Contrato_Ativo ,
                                              DateTime AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              DateTime A1869Contrato_DataTermino ,
                                              DateTime AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              decimal AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              decimal A1870Contrato_ValorUndCntAtual ,
                                              decimal AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [35] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T1.[Contrato_NumeroAta], T1.[Contrato_Ativo], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Numero], T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], COALESCE( T2.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT CASE  WHEN T5.[Contrato_DataVigenciaTermino] > COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T5.[Contrato_DataVigenciaTermino] ELSE COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T5.[Contrato_Codigo], CASE  WHEN COALESCE( T7.[GXC3], 0) > 0 THEN COALESCE( T7.[GXC3], 0) ELSE T5.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual FROM (([Contrato] T5 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC7] AS GXC7 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC7, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC7] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC3, T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH";
         scmdbuf = scmdbuf + " (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC5] ) T7 ON T7.[Contrato_Codigo] = T5.[Contrato_Codigo]) ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE ((@AV113WWContratoDS_30_Tfcontrato_datatermino = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) >= @AV113WWContratoDS_30_Tfcontrato_datatermino))";
         scmdbuf = scmdbuf + " and ((@AV114WWContratoDS_31_Tfcontrato_datatermino_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) <= @AV114WWContratoDS_31_Tfcontrato_datatermino_to))";
         scmdbuf = scmdbuf + " and ((@AV115WWContratoDS_32_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) >= @AV115WWContratoDS_32_Tfcontrato_valorundcntatual))";
         scmdbuf = scmdbuf + " and ((@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) <= @AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV84WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV84WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV86WWContratoDS_3_Contrato_numero1)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV87WWContratoDS_4_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV88WWContratoDS_5_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV89WWContratoDS_6_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV89WWContratoDS_6_Contrato_ano1)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV90WWContratoDS_7_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV90WWContratoDS_7_Contrato_ano_to1)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV93WWContratoDS_10_Contrato_numero2)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV94WWContratoDS_11_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV95WWContratoDS_12_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV96WWContratoDS_13_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV96WWContratoDS_13_Contrato_ano2)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV97WWContratoDS_14_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV97WWContratoDS_14_Contrato_ano_to2)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV100WWContratoDS_17_Contrato_numero3)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV101WWContratoDS_18_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV102WWContratoDS_19_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV103WWContratoDS_20_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV103WWContratoDS_20_Contrato_ano3)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV104WWContratoDS_21_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV104WWContratoDS_21_Contrato_ano_to3)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoDS_22_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV107WWContratoDS_24_Tfcontrato_numero)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV108WWContratoDS_25_Tfcontrato_numero_sel)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV109WWContratoDS_26_Tfcontrato_numeroata)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV110WWContratoDS_27_Tfcontrato_numeroata_sel)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (0==AV111WWContratoDS_28_Tfcontrato_ano) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV111WWContratoDS_28_Tfcontrato_ano)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! (0==AV112WWContratoDS_29_Tfcontrato_ano_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV112WWContratoDS_29_Tfcontrato_ano_to)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV117WWContratoDS_34_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_NumeroAta]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00IP25( IGxContext context ,
                                              int AV84WWContratoDS_1_Contrato_areatrabalhocod ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              String AV85WWContratoDS_2_Dynamicfiltersselector1 ,
                                              String AV86WWContratoDS_3_Contrato_numero1 ,
                                              String AV87WWContratoDS_4_Contrato_numeroata1 ,
                                              String AV88WWContratoDS_5_Contratada_pessoanom1 ,
                                              short AV89WWContratoDS_6_Contrato_ano1 ,
                                              short AV90WWContratoDS_7_Contrato_ano_to1 ,
                                              bool AV91WWContratoDS_8_Dynamicfiltersenabled2 ,
                                              String AV92WWContratoDS_9_Dynamicfiltersselector2 ,
                                              String AV93WWContratoDS_10_Contrato_numero2 ,
                                              String AV94WWContratoDS_11_Contrato_numeroata2 ,
                                              String AV95WWContratoDS_12_Contratada_pessoanom2 ,
                                              short AV96WWContratoDS_13_Contrato_ano2 ,
                                              short AV97WWContratoDS_14_Contrato_ano_to2 ,
                                              bool AV98WWContratoDS_15_Dynamicfiltersenabled3 ,
                                              String AV99WWContratoDS_16_Dynamicfiltersselector3 ,
                                              String AV100WWContratoDS_17_Contrato_numero3 ,
                                              String AV101WWContratoDS_18_Contrato_numeroata3 ,
                                              String AV102WWContratoDS_19_Contratada_pessoanom3 ,
                                              short AV103WWContratoDS_20_Contrato_ano3 ,
                                              short AV104WWContratoDS_21_Contrato_ano_to3 ,
                                              String AV106WWContratoDS_23_Tfcontratada_pessoanom_sel ,
                                              String AV105WWContratoDS_22_Tfcontratada_pessoanom ,
                                              String AV108WWContratoDS_25_Tfcontrato_numero_sel ,
                                              String AV107WWContratoDS_24_Tfcontrato_numero ,
                                              String AV110WWContratoDS_27_Tfcontrato_numeroata_sel ,
                                              String AV109WWContratoDS_26_Tfcontrato_numeroata ,
                                              short AV111WWContratoDS_28_Tfcontrato_ano ,
                                              short AV112WWContratoDS_29_Tfcontrato_ano_to ,
                                              String AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel ,
                                              String AV117WWContratoDS_34_Tfcontratada_pessoacnpj ,
                                              short AV119WWContratoDS_36_Tfcontrato_ativo_sel ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A39Contratada_Codigo ,
                                              String A77Contrato_Numero ,
                                              String A78Contrato_NumeroAta ,
                                              String A41Contratada_PessoaNom ,
                                              short A79Contrato_Ano ,
                                              String A42Contratada_PessoaCNPJ ,
                                              bool A92Contrato_Ativo ,
                                              DateTime AV113WWContratoDS_30_Tfcontrato_datatermino ,
                                              DateTime A1869Contrato_DataTermino ,
                                              DateTime AV114WWContratoDS_31_Tfcontrato_datatermino_to ,
                                              decimal AV115WWContratoDS_32_Tfcontrato_valorundcntatual ,
                                              decimal A1870Contrato_ValorUndCntAtual ,
                                              decimal AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [35] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ativo], T1.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], COALESCE( T2.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT CASE  WHEN T5.[Contrato_DataVigenciaTermino] > COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T5.[Contrato_DataVigenciaTermino] ELSE COALESCE( T6.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T5.[Contrato_Codigo], CASE  WHEN COALESCE( T7.[GXC3], 0) > 0 THEN COALESCE( T7.[GXC3], 0) ELSE T5.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual FROM (([Contrato] T5 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC7] AS GXC7 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC7, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC7] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC3, T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH";
         scmdbuf = scmdbuf + " (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC5] ) T7 ON T7.[Contrato_Codigo] = T5.[Contrato_Codigo]) ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE ((@AV113WWContratoDS_30_Tfcontrato_datatermino = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) >= @AV113WWContratoDS_30_Tfcontrato_datatermino))";
         scmdbuf = scmdbuf + " and ((@AV114WWContratoDS_31_Tfcontrato_datatermino_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T2.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) <= @AV114WWContratoDS_31_Tfcontrato_datatermino_to))";
         scmdbuf = scmdbuf + " and ((@AV115WWContratoDS_32_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) >= @AV115WWContratoDS_32_Tfcontrato_valorundcntatual))";
         scmdbuf = scmdbuf + " and ((@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T2.[Contrato_ValorUndCntAtual], 0) <= @AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV84WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV84WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV9WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDS_3_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV86WWContratoDS_3_Contrato_numero1)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoDS_4_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV87WWContratoDS_4_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDS_5_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV88WWContratoDS_5_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV89WWContratoDS_6_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV89WWContratoDS_6_Contrato_ano1)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV85WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV90WWContratoDS_7_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV90WWContratoDS_7_Contrato_ano_to1)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDS_10_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV93WWContratoDS_10_Contrato_numero2)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoDS_11_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV94WWContratoDS_11_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoDS_12_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV95WWContratoDS_12_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV96WWContratoDS_13_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV96WWContratoDS_13_Contrato_ano2)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV91WWContratoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWContratoDS_9_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV97WWContratoDS_14_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV97WWContratoDS_14_Contrato_ano_to2)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWContratoDS_17_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV100WWContratoDS_17_Contrato_numero3)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWContratoDS_18_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV101WWContratoDS_18_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWContratoDS_19_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV102WWContratoDS_19_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV103WWContratoDS_20_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV103WWContratoDS_20_Contrato_ano3)";
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( AV98WWContratoDS_15_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWContratoDS_16_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV104WWContratoDS_21_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV104WWContratoDS_21_Contrato_ano_to3)";
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoDS_22_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV105WWContratoDS_22_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV106WWContratoDS_23_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoDS_24_Tfcontrato_numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV107WWContratoDS_24_Tfcontrato_numero)";
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWContratoDS_25_Tfcontrato_numero_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV108WWContratoDS_25_Tfcontrato_numero_sel)";
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWContratoDS_26_Tfcontrato_numeroata)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV109WWContratoDS_26_Tfcontrato_numeroata)";
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContratoDS_27_Tfcontrato_numeroata_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV110WWContratoDS_27_Tfcontrato_numeroata_sel)";
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! (0==AV111WWContratoDS_28_Tfcontrato_ano) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV111WWContratoDS_28_Tfcontrato_ano)";
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( ! (0==AV112WWContratoDS_29_Tfcontrato_ano_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV112WWContratoDS_29_Tfcontrato_ano_to)";
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContratoDS_34_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV117WWContratoDS_34_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int7[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int7[34] = 1;
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV119WWContratoDS_36_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00IP7(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (String)dynConstraints[39] , (bool)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
               case 1 :
                     return conditional_P00IP13(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (String)dynConstraints[39] , (bool)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
               case 2 :
                     return conditional_P00IP19(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (String)dynConstraints[39] , (bool)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
               case 3 :
                     return conditional_P00IP25(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (short)dynConstraints[38] , (String)dynConstraints[39] , (bool)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00IP7 ;
          prmP00IP7 = new Object[] {
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV84WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContratoDS_3_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV87WWContratoDS_4_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV88WWContratoDS_5_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV89WWContratoDS_6_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWContratoDS_7_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV93WWContratoDS_10_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV94WWContratoDS_11_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWContratoDS_12_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV96WWContratoDS_13_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV97WWContratoDS_14_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV100WWContratoDS_17_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV101WWContratoDS_18_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWContratoDS_19_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV103WWContratoDS_20_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWContratoDS_21_Contrato_ano_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV105WWContratoDS_22_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoDS_23_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV108WWContratoDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV109WWContratoDS_26_Tfcontrato_numeroata",SqlDbType.Char,10,0} ,
          new Object[] {"@AV110WWContratoDS_27_Tfcontrato_numeroata_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV111WWContratoDS_28_Tfcontrato_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV112WWContratoDS_29_Tfcontrato_ano_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWContratoDS_34_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00IP13 ;
          prmP00IP13 = new Object[] {
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV84WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContratoDS_3_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV87WWContratoDS_4_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV88WWContratoDS_5_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV89WWContratoDS_6_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWContratoDS_7_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV93WWContratoDS_10_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV94WWContratoDS_11_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWContratoDS_12_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV96WWContratoDS_13_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV97WWContratoDS_14_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV100WWContratoDS_17_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV101WWContratoDS_18_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWContratoDS_19_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV103WWContratoDS_20_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWContratoDS_21_Contrato_ano_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV105WWContratoDS_22_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoDS_23_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV108WWContratoDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV109WWContratoDS_26_Tfcontrato_numeroata",SqlDbType.Char,10,0} ,
          new Object[] {"@AV110WWContratoDS_27_Tfcontrato_numeroata_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV111WWContratoDS_28_Tfcontrato_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV112WWContratoDS_29_Tfcontrato_ano_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWContratoDS_34_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00IP19 ;
          prmP00IP19 = new Object[] {
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV84WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContratoDS_3_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV87WWContratoDS_4_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV88WWContratoDS_5_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV89WWContratoDS_6_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWContratoDS_7_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV93WWContratoDS_10_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV94WWContratoDS_11_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWContratoDS_12_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV96WWContratoDS_13_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV97WWContratoDS_14_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV100WWContratoDS_17_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV101WWContratoDS_18_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWContratoDS_19_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV103WWContratoDS_20_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWContratoDS_21_Contrato_ano_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV105WWContratoDS_22_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoDS_23_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV108WWContratoDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV109WWContratoDS_26_Tfcontrato_numeroata",SqlDbType.Char,10,0} ,
          new Object[] {"@AV110WWContratoDS_27_Tfcontrato_numeroata_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV111WWContratoDS_28_Tfcontrato_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV112WWContratoDS_29_Tfcontrato_ano_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWContratoDS_34_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00IP25 ;
          prmP00IP25 = new Object[] {
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContratoDS_30_Tfcontrato_datatermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContratoDS_31_Tfcontrato_datatermino_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV115WWContratoDS_32_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV116WWContratoDS_33_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV84WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWContratoDS_3_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV87WWContratoDS_4_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV88WWContratoDS_5_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV89WWContratoDS_6_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV90WWContratoDS_7_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV93WWContratoDS_10_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV94WWContratoDS_11_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWContratoDS_12_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV96WWContratoDS_13_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV97WWContratoDS_14_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV100WWContratoDS_17_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV101WWContratoDS_18_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWContratoDS_19_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV103WWContratoDS_20_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWContratoDS_21_Contrato_ano_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV105WWContratoDS_22_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV106WWContratoDS_23_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV107WWContratoDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV108WWContratoDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV109WWContratoDS_26_Tfcontrato_numeroata",SqlDbType.Char,10,0} ,
          new Object[] {"@AV110WWContratoDS_27_Tfcontrato_numeroata_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV111WWContratoDS_28_Tfcontrato_ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV112WWContratoDS_29_Tfcontrato_ano_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV117WWContratoDS_34_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV118WWContratoDS_35_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00IP7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IP7,100,0,true,false )
             ,new CursorDef("P00IP13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IP13,100,0,true,false )
             ,new CursorDef("P00IP19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IP19,100,0,true,false )
             ,new CursorDef("P00IP25", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IP25,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[48]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[66]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[67]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratofilterdata") )
          {
             return  ;
          }
          getwwcontratofilterdata worker = new getwwcontratofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
