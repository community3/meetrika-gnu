/*
               File: type_SdtSDT_WSParametros
        Description: SDT_WSParametros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 7/2/2019 1:12:14.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WSParametros" )]
   [XmlType(TypeName =  "SDT_WSParametros" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_WSParametros : GxUserType
   {
      public SdtSDT_WSParametros( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WSParametros_Status = "";
      }

      public SdtSDT_WSParametros( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
            mapper[ "Usuario" ] =  "Usuario" ;
            mapper[ "Status" ] =  "Status" ;
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WSParametros deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WSParametros)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WSParametros obj ;
         obj = this;
         obj.gxTpr_Usuario = deserialized.gxTpr_Usuario;
         obj.gxTpr_Status = deserialized.gxTpr_Status;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Usuario") == 0 ) )
               {
                  gxTv_SdtSDT_WSParametros_Usuario = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Status") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "Status") == 0 ) )
               {
                  gxTv_SdtSDT_WSParametros_Status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WSParametros";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Usuario", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WSParametros_Usuario), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "Usuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "Usuario");
         }
         oWriter.WriteElement("Status", StringUtil.RTrim( gxTv_SdtSDT_WSParametros_Status));
         if ( StringUtil.StrCmp(sNameSpace, "Status") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "Status");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Usuario", gxTv_SdtSDT_WSParametros_Usuario, false);
         AddObjectProperty("Status", gxTv_SdtSDT_WSParametros_Status, false);
         return  ;
      }

      [  SoapElement( ElementName = "Usuario" )]
      [  XmlElement( ElementName = "Usuario" , Namespace = "Usuario"  )]
      public int gxTpr_Usuario
      {
         get {
            return gxTv_SdtSDT_WSParametros_Usuario ;
         }

         set {
            gxTv_SdtSDT_WSParametros_Usuario = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Status" )]
      [  XmlElement( ElementName = "Status" , Namespace = "Status"  )]
      public String gxTpr_Status
      {
         get {
            return gxTv_SdtSDT_WSParametros_Status ;
         }

         set {
            gxTv_SdtSDT_WSParametros_Status = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WSParametros_Status = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WSParametros_Usuario ;
      protected String gxTv_SdtSDT_WSParametros_Status ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_WSParametros", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_WSParametros_RESTInterface : GxGenericCollectionItem<SdtSDT_WSParametros>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WSParametros_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WSParametros_RESTInterface( SdtSDT_WSParametros psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Usuario" , Order = 0 )]
      public Nullable<int> gxTpr_Usuario
      {
         get {
            return sdt.gxTpr_Usuario ;
         }

         set {
            sdt.gxTpr_Usuario = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Status" , Order = 1 )]
      public String gxTpr_Status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Status) ;
         }

         set {
            sdt.gxTpr_Status = (String)(value);
         }

      }

      public SdtSDT_WSParametros sdt
      {
         get {
            return (SdtSDT_WSParametros)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WSParametros() ;
         }
      }

   }

}
