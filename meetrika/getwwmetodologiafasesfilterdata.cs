/*
               File: GetWWMetodologiaFasesFilterData
        Description: Get WWMetodologia Fases Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:51.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwmetodologiafasesfilterdata : GXProcedure
   {
      public getwwmetodologiafasesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwmetodologiafasesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwmetodologiafasesfilterdata objgetwwmetodologiafasesfilterdata;
         objgetwwmetodologiafasesfilterdata = new getwwmetodologiafasesfilterdata();
         objgetwwmetodologiafasesfilterdata.AV18DDOName = aP0_DDOName;
         objgetwwmetodologiafasesfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwmetodologiafasesfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwmetodologiafasesfilterdata.AV22OptionsJson = "" ;
         objgetwwmetodologiafasesfilterdata.AV25OptionsDescJson = "" ;
         objgetwwmetodologiafasesfilterdata.AV27OptionIndexesJson = "" ;
         objgetwwmetodologiafasesfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwmetodologiafasesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwmetodologiafasesfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwmetodologiafasesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_METODOLOGIAFASES_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIAFASES_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_METODOLOGIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWMetodologiaFasesGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWMetodologiaFasesGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWMetodologiaFasesGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME") == 0 )
            {
               AV10TFMetodologiaFases_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME_SEL") == 0 )
            {
               AV11TFMetodologiaFases_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_PERCENTUAL") == 0 )
            {
               AV12TFMetodologiaFases_Percentual = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFMetodologiaFases_Percentual_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV14TFMetodologia_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV15TFMetodologia_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36MetodologiaFases_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37Metodologia_Descricao1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV41MetodologiaFases_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "METODOLOGIA_DESCRICAO") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42Metodologia_Descricao2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMETODOLOGIAFASES_NOMEOPTIONS' Routine */
         AV10TFMetodologiaFases_Nome = AV16SearchTxt;
         AV11TFMetodologiaFases_Nome_Sel = "";
         AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV36MetodologiaFases_Nome1;
         AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV37Metodologia_Descricao1;
         AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV41MetodologiaFases_Nome2;
         AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV42Metodologia_Descricao2;
         AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV10TFMetodologiaFases_Nome;
         AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV11TFMetodologiaFases_Nome_Sel;
         AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV12TFMetodologiaFases_Percentual;
         AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV13TFMetodologiaFases_Percentual_To;
         AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = AV14TFMetodologia_Descricao;
         AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel = AV15TFMetodologia_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                              AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                              AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                              AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                              AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                              AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                              AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                              AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                              AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                              AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                              AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                              AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                              AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel ,
                                              AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ,
                                              A148MetodologiaFases_Nome ,
                                              A138Metodologia_Descricao ,
                                              A149MetodologiaFases_Percentual },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL
                                              }
         });
         lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome), 50, "%");
         lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao), "%", "");
         /* Using cursor P00KV2 */
         pr_default.execute(0, new Object[] {lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV50WWMetodologiaFasesDS_4_Metodologia_descricao1, lV50WWMetodologiaFasesDS_4_Metodologia_descricao1, lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV55WWMetodologiaFasesDS_9_Metodologia_descricao2, lV55WWMetodologiaFasesDS_9_Metodologia_descricao2, lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome, AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel, AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual, AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to, lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao, AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKV2 = false;
            A137Metodologia_Codigo = P00KV2_A137Metodologia_Codigo[0];
            A148MetodologiaFases_Nome = P00KV2_A148MetodologiaFases_Nome[0];
            A149MetodologiaFases_Percentual = P00KV2_A149MetodologiaFases_Percentual[0];
            A138Metodologia_Descricao = P00KV2_A138Metodologia_Descricao[0];
            A147MetodologiaFases_Codigo = P00KV2_A147MetodologiaFases_Codigo[0];
            A138Metodologia_Descricao = P00KV2_A138Metodologia_Descricao[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KV2_A148MetodologiaFases_Nome[0], A148MetodologiaFases_Nome) == 0 ) )
            {
               BRKKV2 = false;
               A147MetodologiaFases_Codigo = P00KV2_A147MetodologiaFases_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKKV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A148MetodologiaFases_Nome)) )
            {
               AV20Option = A148MetodologiaFases_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKV2 )
            {
               BRKKV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMETODOLOGIA_DESCRICAOOPTIONS' Routine */
         AV14TFMetodologia_Descricao = AV16SearchTxt;
         AV15TFMetodologia_Descricao_Sel = "";
         AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = AV36MetodologiaFases_Nome1;
         AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = AV37Metodologia_Descricao1;
         AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = AV41MetodologiaFases_Nome2;
         AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = AV42Metodologia_Descricao2;
         AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = AV10TFMetodologiaFases_Nome;
         AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = AV11TFMetodologiaFases_Nome_Sel;
         AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual = AV12TFMetodologiaFases_Percentual;
         AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to = AV13TFMetodologiaFases_Percentual_To;
         AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = AV14TFMetodologia_Descricao;
         AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel = AV15TFMetodologia_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                              AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                              AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                              AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                              AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                              AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                              AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                              AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                              AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                              AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                              AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                              AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                              AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel ,
                                              AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ,
                                              A148MetodologiaFases_Nome ,
                                              A138Metodologia_Descricao ,
                                              A149MetodologiaFases_Percentual },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL
                                              }
         });
         lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1), 50, "%");
         lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1), "%", "");
         lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2), 50, "%");
         lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2), "%", "");
         lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome), 50, "%");
         lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao), "%", "");
         /* Using cursor P00KV3 */
         pr_default.execute(1, new Object[] {lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1, lV50WWMetodologiaFasesDS_4_Metodologia_descricao1, lV50WWMetodologiaFasesDS_4_Metodologia_descricao1, lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2, lV55WWMetodologiaFasesDS_9_Metodologia_descricao2, lV55WWMetodologiaFasesDS_9_Metodologia_descricao2, lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome, AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel, AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual, AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to, lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao, AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKV4 = false;
            A137Metodologia_Codigo = P00KV3_A137Metodologia_Codigo[0];
            A149MetodologiaFases_Percentual = P00KV3_A149MetodologiaFases_Percentual[0];
            A138Metodologia_Descricao = P00KV3_A138Metodologia_Descricao[0];
            A148MetodologiaFases_Nome = P00KV3_A148MetodologiaFases_Nome[0];
            A147MetodologiaFases_Codigo = P00KV3_A147MetodologiaFases_Codigo[0];
            A138Metodologia_Descricao = P00KV3_A138Metodologia_Descricao[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00KV3_A137Metodologia_Codigo[0] == A137Metodologia_Codigo ) )
            {
               BRKKV4 = false;
               A147MetodologiaFases_Codigo = P00KV3_A147MetodologiaFases_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKKV4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A138Metodologia_Descricao)) )
            {
               AV20Option = A138Metodologia_Descricao;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKV4 )
            {
               BRKKV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFMetodologiaFases_Nome = "";
         AV11TFMetodologiaFases_Nome_Sel = "";
         AV14TFMetodologia_Descricao = "";
         AV15TFMetodologia_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36MetodologiaFases_Nome1 = "";
         AV37Metodologia_Descricao1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV41MetodologiaFases_Nome2 = "";
         AV42Metodologia_Descricao2 = "";
         AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 = "";
         AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = "";
         AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = "";
         AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 = "";
         AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = "";
         AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = "";
         AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = "";
         AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel = "";
         AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = "";
         AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel = "";
         scmdbuf = "";
         lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 = "";
         lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 = "";
         lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 = "";
         lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 = "";
         lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome = "";
         lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao = "";
         A148MetodologiaFases_Nome = "";
         A138Metodologia_Descricao = "";
         P00KV2_A137Metodologia_Codigo = new int[1] ;
         P00KV2_A148MetodologiaFases_Nome = new String[] {""} ;
         P00KV2_A149MetodologiaFases_Percentual = new decimal[1] ;
         P00KV2_A138Metodologia_Descricao = new String[] {""} ;
         P00KV2_A147MetodologiaFases_Codigo = new int[1] ;
         AV20Option = "";
         P00KV3_A137Metodologia_Codigo = new int[1] ;
         P00KV3_A149MetodologiaFases_Percentual = new decimal[1] ;
         P00KV3_A138Metodologia_Descricao = new String[] {""} ;
         P00KV3_A148MetodologiaFases_Nome = new String[] {""} ;
         P00KV3_A147MetodologiaFases_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwmetodologiafasesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KV2_A137Metodologia_Codigo, P00KV2_A148MetodologiaFases_Nome, P00KV2_A149MetodologiaFases_Percentual, P00KV2_A138Metodologia_Descricao, P00KV2_A147MetodologiaFases_Codigo
               }
               , new Object[] {
               P00KV3_A137Metodologia_Codigo, P00KV3_A149MetodologiaFases_Percentual, P00KV3_A138Metodologia_Descricao, P00KV3_A148MetodologiaFases_Nome, P00KV3_A147MetodologiaFases_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private short AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ;
      private short AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ;
      private int AV45GXV1 ;
      private int A137Metodologia_Codigo ;
      private int A147MetodologiaFases_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private decimal AV12TFMetodologiaFases_Percentual ;
      private decimal AV13TFMetodologiaFases_Percentual_To ;
      private decimal AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ;
      private decimal AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ;
      private decimal A149MetodologiaFases_Percentual ;
      private String AV10TFMetodologiaFases_Nome ;
      private String AV11TFMetodologiaFases_Nome_Sel ;
      private String AV36MetodologiaFases_Nome1 ;
      private String AV41MetodologiaFases_Nome2 ;
      private String AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ;
      private String AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ;
      private String AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ;
      private String AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ;
      private String scmdbuf ;
      private String lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ;
      private String lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ;
      private String lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ;
      private String A148MetodologiaFases_Nome ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ;
      private bool BRKKV2 ;
      private bool BRKKV4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFMetodologia_Descricao ;
      private String AV15TFMetodologia_Descricao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV37Metodologia_Descricao1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV42Metodologia_Descricao2 ;
      private String AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ;
      private String AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ;
      private String AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ;
      private String AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ;
      private String AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ;
      private String AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel ;
      private String lV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ;
      private String lV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ;
      private String lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ;
      private String A138Metodologia_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KV2_A137Metodologia_Codigo ;
      private String[] P00KV2_A148MetodologiaFases_Nome ;
      private decimal[] P00KV2_A149MetodologiaFases_Percentual ;
      private String[] P00KV2_A138Metodologia_Descricao ;
      private int[] P00KV2_A147MetodologiaFases_Codigo ;
      private int[] P00KV3_A137Metodologia_Codigo ;
      private decimal[] P00KV3_A149MetodologiaFases_Percentual ;
      private String[] P00KV3_A138Metodologia_Descricao ;
      private String[] P00KV3_A148MetodologiaFases_Nome ;
      private int[] P00KV3_A147MetodologiaFases_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwmetodologiafasesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KV2( IGxContext context ,
                                             String AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                             String AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                             bool AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                             String AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                             short AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                             String AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                             String AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                             String AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                             String AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                             decimal AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                             decimal AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                             String AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel ,
                                             String AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[MetodologiaFases_Nome], T1.[MetodologiaFases_Percentual], T2.[Metodologia_Descricao], T1.[MetodologiaFases_Codigo] FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[MetodologiaFases_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KV3( IGxContext context ,
                                             String AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1 ,
                                             String AV50WWMetodologiaFasesDS_4_Metodologia_descricao1 ,
                                             bool AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 ,
                                             String AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2 ,
                                             short AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 ,
                                             String AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2 ,
                                             String AV55WWMetodologiaFasesDS_9_Metodologia_descricao2 ,
                                             String AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel ,
                                             String AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome ,
                                             decimal AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual ,
                                             decimal AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to ,
                                             String AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel ,
                                             String AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             String A138Metodologia_Descricao ,
                                             decimal A149MetodologiaFases_Percentual )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[MetodologiaFases_Percentual], T2.[Metodologia_Descricao], T1.[MetodologiaFases_Nome], T1.[MetodologiaFases_Codigo] FROM ([MetodologiaFases] T1 WITH (NOLOCK) INNER JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWMetodologiaFasesDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV48WWMetodologiaFasesDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWMetodologiaFasesDS_4_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV50WWMetodologiaFasesDS_4_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like '%' + @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like '%' + @lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV51WWMetodologiaFasesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWMetodologiaFasesDS_6_Dynamicfiltersselector2, "METODOLOGIA_DESCRICAO") == 0 ) && ( AV53WWMetodologiaFasesDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMetodologiaFasesDS_9_Metodologia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like '%' + @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like '%' + @lV55WWMetodologiaFasesDS_9_Metodologia_descricao2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] like @lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] like @lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Nome] = @AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Nome] = @AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] >= @AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] >= @AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[MetodologiaFases_Percentual] <= @AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[MetodologiaFases_Percentual] <= @AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] like @lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Metodologia_Descricao] = @AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Metodologia_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KV2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] );
               case 1 :
                     return conditional_P00KV3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KV2 ;
          prmP00KV2 = new Object[] {
          new Object[] {"@lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV50WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00KV3 ;
          prmP00KV3 = new Object[] {
          new Object[] {"@lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWMetodologiaFasesDS_3_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV50WWMetodologiaFasesDS_4_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWMetodologiaFasesDS_8_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWMetodologiaFasesDS_9_Metodologia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWMetodologiaFasesDS_10_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWMetodologiaFasesDS_11_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWMetodologiaFasesDS_12_Tfmetodologiafases_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV59WWMetodologiaFasesDS_13_Tfmetodologiafases_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@lV60WWMetodologiaFasesDS_14_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV61WWMetodologiaFasesDS_15_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KV2,100,0,true,false )
             ,new CursorDef("P00KV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KV3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwmetodologiafasesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwmetodologiafasesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwmetodologiafasesfilterdata") )
          {
             return  ;
          }
          getwwmetodologiafasesfilterdata worker = new getwwmetodologiafasesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
