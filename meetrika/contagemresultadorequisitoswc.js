/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 22:57:17.40
*/
gx.evt.autoSkip = false;
gx.define('contagemresultadorequisitoswc', true, function (CmpContext) {
   this.ServerClass =  "contagemresultadorequisitoswc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV67ContagemResultadoRequisito_OSCod=gx.fn.getIntegerValue("vCONTAGEMRESULTADOREQUISITO_OSCOD",'.') ;
      this.AV70Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.A1636ContagemResultado_ServicoSS=gx.fn.getIntegerValue("CONTAGEMRESULTADO_SERVICOSS",'.') ;
      this.A1932Requisito_Pontuacao=gx.fn.getDecimalValue("REQUISITO_PONTUACAO",'.',',') ;
      this.AV65PontosFilhos=gx.fn.getDecimalValue("vPONTOSFILHOS",'.',',') ;
      this.AV60Total=gx.fn.getDecimalValue("vTOTAL",'.',',') ;
      this.A1926Requisito_Agrupador=gx.fn.getControlValue("REQUISITO_AGRUPADOR") ;
      this.A2001Requisito_Identificador=gx.fn.getControlValue("REQUISITO_IDENTIFICADOR") ;
      this.A1927Requisito_Titulo=gx.fn.getControlValue("REQUISITO_TITULO") ;
      this.A1934Requisito_Status=gx.fn.getIntegerValue("REQUISITO_STATUS",'.') ;
      this.A1923Requisito_Descricao=gx.fn.getControlValue("REQUISITO_DESCRICAO") ;
      this.A508ContagemResultado_Owner=gx.fn.getIntegerValue("CONTAGEMRESULTADO_OWNER",'.') ;
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A2006ContagemResultadoRequisito_Owner=gx.fn.getControlValue("CONTAGEMRESULTADOREQUISITO_OWNER") ;
      this.A1999Requisito_ReqCod=gx.fn.getIntegerValue("REQUISITO_REQCOD",'.') ;
      this.AV63Requisito_Codigo=gx.fn.getIntegerValue("vREQUISITO_CODIGO",'.') ;
      this.AV7ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.Valid_Contagemresultadorequisito_reqcod=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(10) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADOREQUISITO_REQCOD", gx.fn.currentGridRowImpl(10));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultadorequisito_oscod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADOREQUISITO_OSCOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e18pg1_client=function()
   {
      this.clearMessages();
      this.INNEWWINDOWContainer.Target =  gx.http.formatLink("wp_requisitos.aspx",[this.AV7ContagemResultado_Codigo])  ;
      this.INNEWWINDOWContainer.OpenWindow() ;
      this.refreshOutputs([{ctrl:this.INNEWWINDOWContainer}]);
   };
   this.e19pg2_client=function()
   {
      this.clearMessages();
      this.CONFIRMPANELContainer.Confirm() ;
      this.refreshOutputs([]);
   };
   this.e12pg2_client=function()
   {
      this.executeServerEvent("'DONEWRN'", true, null, false, false);
   };
   this.e16pg2_client=function()
   {
      this.executeServerEvent("'DOUPDATE'", true, arguments[0], false, false);
   };
   this.e17pg2_client=function()
   {
      this.executeServerEvent("'DONEWREQTEC'", true, arguments[0], false, false);
   };
   this.e11pg2_client=function()
   {
      this.executeServerEvent("CONFIRMPANEL.CLOSE", false, null, true, true);
   };
   this.e20pg2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e21pg2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,7,15,18,20,25,33,35,38,45,47];
   this.GXLastCtrlId =47;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",10,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"contagemresultadorequisitoswc",[],true,1,true,true,0,true,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.startRow("","","","","","");
   GridContainer.startCell("","","top","","","","","","","");
   this.setGridUCProp("Dvpanel_tblreqneg","Width", "Width", "100%", "str");
   this.setGridUCProp("Dvpanel_tblreqneg","Height", "Height", "100", "str");
   this.setGridUCProp("Dvpanel_tblreqneg","AutoWidth", "Autowidth", false, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","AutoHeight", "Autoheight", true, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","Cls", "Cls", "GXUI-DVelop-Panel", "str");
   this.setGridUCProp("Dvpanel_tblreqneg","ShowHeader", "Showheader", true, "bool");
   this.setGridUCDynProp("Dvpanel_tblreqneg","Title", "Title", "Requisito", "str");
   this.setGridUCDynProp("Dvpanel_tblreqneg","Collapsible", "Collapsible", true, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","Collapsed", "Collapsed", false, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","ShowCollapseIcon", "Showcollapseicon", false, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","IconPosition", "Iconposition", "left", "str");
   this.setGridUCProp("Dvpanel_tblreqneg","AutoScroll", "Autoscroll", false, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","Visible", "Visible", true, "bool");
   this.setGridUCProp("Dvpanel_tblreqneg","Enabled", "Enabled", true, "boolean");
   this.setGridUCProp("Dvpanel_tblreqneg","Class", "Class", "", "char");
   function Dvpanel_tblreqnegshow(UC,gRow) { UC.show(); }
   GridContainer.addUsercontrol(13, 0, "BootstrapPanel", this.CmpContext + "DVPANEL_TBLREQNEGContainer", "Dvpanel_tblreqneg",Dvpanel_tblreqnegshow,[],[],true);
   GridContainer.startContainer();
   GridContainer.startTable("Tblreqneg",15,"0px");
   GridContainer.addHtmlCode("<tbody>");
   GridContainer.startRow("","","","","","");
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.addTextBlock('TEXTBLOCKDESCRICAO',null);
   GridContainer.endCell();
   GridContainer.startCell("","","","","","","","9","","");
   GridContainer.addMultipleLineEdit("Descricao",20,"vDESCRICAO","","Descricao","vchar",80,"chr",4,"row","500",500,"left",null,true,false,2,"");
   GridContainer.endCell();
   GridContainer.endRow();
   GridContainer.startRow("","","","","","");
   GridContainer.startCell("","","","","","","","","","");
   GridContainer.endCell();
   GridContainer.startCell("","","","","","","","9","","");
   GridContainer.addWebComponent("Wcwc_requisitostecnicos");
   GridContainer.endCell();
   GridContainer.endRow();
   GridContainer.addHtmlCode("</tbody>");
   GridContainer.endTable();
   GridContainer.endContainer();
   GridContainer.addSingleLineEdit(2004,25,"CONTAGEMRESULTADOREQUISITO_REQCOD","","","ContagemResultadoRequisito_ReqCod","int",6,"chr",6,6,"right",null,[],2004,"ContagemResultadoRequisito_ReqCod",true,0,false,false,"Attribute",1,"");
   GridContainer.endCell();
   GridContainer.startCell("","","top","","","","","","","");
   GridContainer.addBitmap("Update","UPDATE",27,0,"px",0,"px","e16pg2_client","","Alterar Requisito","Image","");
   GridContainer.endCell();
   GridContainer.startCell("","","top","","","","","","","");
   GridContainer.addBitmap("Actiondelete","ACTIONDELETE",29,0,"px",0,"px","e19pg2_client","","","Image","");
   GridContainer.endCell();
   GridContainer.startCell("","","top","","","","","","","");
   GridContainer.addBitmap("Newreqtec","NEWREQTEC",31,0,"px",0,"px","e17pg2_client","","Criar um novo Requisito associado","Image","");
   GridContainer.endCell();
   GridContainer.endRow();
   this.setGrid(GridContainer);
   this.DVPANEL_UNNAMEDTABLE2Container = gx.uc.getNew(this, 5, 0, "BootstrapPanel", this.CmpContext + "DVPANEL_UNNAMEDTABLE2Container", "Dvpanel_unnamedtable2");
   var DVPANEL_UNNAMEDTABLE2Container = this.DVPANEL_UNNAMEDTABLE2Container;
   DVPANEL_UNNAMEDTABLE2Container.setProp("Width", "Width", "100%", "str");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Height", "Height", "100", "str");
   DVPANEL_UNNAMEDTABLE2Container.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_UNNAMEDTABLE2Container.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setDynProp("Title", "Title", "Pontuação total:", "str");
   DVPANEL_UNNAMEDTABLE2Container.setDynProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_UNNAMEDTABLE2Container.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Visible", "Visible", true, "bool");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_UNNAMEDTABLE2Container.setProp("Class", "Class", "", "char");
   DVPANEL_UNNAMEDTABLE2Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_UNNAMEDTABLE2Container);
   this.INNEWWINDOWContainer = gx.uc.getNew(this, 41, 20, "InNewWindow", this.CmpContext + "INNEWWINDOWContainer", "Innewwindow");
   var INNEWWINDOWContainer = this.INNEWWINDOWContainer;
   INNEWWINDOWContainer.setProp("Width", "Width", "50", "str");
   INNEWWINDOWContainer.setProp("Height", "Height", "50", "str");
   INNEWWINDOWContainer.setProp("Name", "Name", "", "str");
   INNEWWINDOWContainer.setDynProp("Target", "Target", "", "str");
   INNEWWINDOWContainer.setProp("Fullscreen", "Fullscreen", false, "bool");
   INNEWWINDOWContainer.setProp("Location", "Location", true, "bool");
   INNEWWINDOWContainer.setProp("MenuBar", "Menubar", true, "bool");
   INNEWWINDOWContainer.setProp("Resizable", "Resizable", true, "bool");
   INNEWWINDOWContainer.setProp("Scrollbars", "Scrollbars", true, "bool");
   INNEWWINDOWContainer.setProp("TitleBar", "Titlebar", true, "bool");
   INNEWWINDOWContainer.setProp("ToolBar", "Toolbar", true, "bool");
   INNEWWINDOWContainer.setProp("directories", "Directories", true, "bool");
   INNEWWINDOWContainer.setProp("status", "Status", true, "bool");
   INNEWWINDOWContainer.setProp("copyhistory", "Copyhistory", true, "bool");
   INNEWWINDOWContainer.setProp("top", "Top", "5", "str");
   INNEWWINDOWContainer.setProp("left", "Left", "5", "str");
   INNEWWINDOWContainer.setProp("fitscreen", "Fitscreen", false, "bool");
   INNEWWINDOWContainer.setProp("RefreshParentOnClose", "Refreshparentonclose", false, "bool");
   INNEWWINDOWContainer.setProp("Targets", "Targets", '', "str");
   INNEWWINDOWContainer.setProp("Visible", "Visible", true, "bool");
   INNEWWINDOWContainer.setProp("Enabled", "Enabled", true, "boolean");
   INNEWWINDOWContainer.setProp("Class", "Class", "", "char");
   INNEWWINDOWContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(INNEWWINDOWContainer);
   this.CONFIRMPANELContainer = gx.uc.getNew(this, 42, 20, "BootstrapConfirmPanel", this.CmpContext + "CONFIRMPANELContainer", "Confirmpanel");
   var CONFIRMPANELContainer = this.CONFIRMPANELContainer;
   CONFIRMPANELContainer.setProp("Width", "Width", "100", "str");
   CONFIRMPANELContainer.setProp("Height", "Height", "100", "str");
   CONFIRMPANELContainer.setProp("Title", "Title", "Confirmação", "str");
   CONFIRMPANELContainer.setProp("ConfirmationText", "Confirmationtext", "Confirma eliminar este requisito?", "str");
   CONFIRMPANELContainer.setProp("YesButtonCaption", "Yesbuttoncaption", "Sim", "str");
   CONFIRMPANELContainer.setProp("NoButtonCaption", "Nobuttoncaption", "Não", "str");
   CONFIRMPANELContainer.setProp("CancelButtonCaption", "Cancelbuttoncaption", "Cancel", "str");
   CONFIRMPANELContainer.setProp("YesButtonPosition", "Yesbuttonposition", "left", "str");
   CONFIRMPANELContainer.setProp("ConfirmType", "Confirmtype", "1", "str");
   CONFIRMPANELContainer.setProp("Result", "Result", "", "char");
   CONFIRMPANELContainer.setProp("TextType", "Texttype", "1", "str");
   CONFIRMPANELContainer.setProp("Visible", "Visible", true, "bool");
   CONFIRMPANELContainer.setProp("Enabled", "Enabled", true, "boolean");
   CONFIRMPANELContainer.setProp("Class", "Class", "", "char");
   CONFIRMPANELContainer.setC2ShowFunction(function(UC) { UC.show(); });
   CONFIRMPANELContainer.addEventHandler("Close", this.e11pg2_client);
   this.setUserControl(CONFIRMPANELContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[7]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[15]={fld:"TBLREQNEG",grid:10};
   GXValidFnc[18]={fld:"TEXTBLOCKDESCRICAO", format:0,grid:10};
   GXValidFnc[20]={lvl:2,type:"vchar",len:500,dec:0,sign:false,ro:0,isacc:0, multiline:true,grid:10,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDESCRICAO",gxz:"ZV54Descricao",gxold:"OV54Descricao",gxvar:"AV54Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV54Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV54Descricao=Value},v2c:function(row){gx.fn.setGridControlValue("vDESCRICAO",row || gx.fn.currentGridRowImpl(10),gx.O.AV54Descricao,2)},c2v:function(){if(this.val()!==undefined)gx.O.AV54Descricao=this.val()},val:function(row){return gx.fn.getGridControlValue("vDESCRICAO",row || gx.fn.currentGridRowImpl(10))},nac:gx.falseFn};
   GXValidFnc[25]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:10,gxgrid:this.GridContainer,fnc:this.Valid_Contagemresultadorequisito_reqcod,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOREQUISITO_REQCOD",gxz:"Z2004ContagemResultadoRequisito_ReqCod",gxold:"O2004ContagemResultadoRequisito_ReqCod",gxvar:"A2004ContagemResultadoRequisito_ReqCod",ucs:[],op:[],ip:[25,47],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A2004ContagemResultadoRequisito_ReqCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2004ContagemResultadoRequisito_ReqCod=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOREQUISITO_REQCOD",row || gx.fn.currentGridRowImpl(10),gx.O.A2004ContagemResultadoRequisito_ReqCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2004ContagemResultadoRequisito_ReqCod=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOREQUISITO_REQCOD",row || gx.fn.currentGridRowImpl(10),'.')},nac:gx.falseFn};
   GXValidFnc[33]={fld:"NEWRN",grid:0};
   GXValidFnc[35]={fld:"PDF",grid:0};
   GXValidFnc[38]={fld:"USERTABLE",grid:0};
   GXValidFnc[45]={fld:"TBJAVA", format:1,grid:0};
   GXValidFnc[47]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultadorequisito_oscod,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOREQUISITO_OSCOD",gxz:"Z2003ContagemResultadoRequisito_OSCod",gxold:"O2003ContagemResultadoRequisito_OSCod",gxvar:"A2003ContagemResultadoRequisito_OSCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A2003ContagemResultadoRequisito_OSCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2003ContagemResultadoRequisito_OSCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADOREQUISITO_OSCOD",gx.O.A2003ContagemResultadoRequisito_OSCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2003ContagemResultadoRequisito_OSCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADOREQUISITO_OSCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 47 , function() {
   });
   this.ZV54Descricao = "" ;
   this.OV54Descricao = "" ;
   this.Z2004ContagemResultadoRequisito_ReqCod = 0 ;
   this.O2004ContagemResultadoRequisito_ReqCod = 0 ;
   this.A2003ContagemResultadoRequisito_OSCod = 0 ;
   this.Z2003ContagemResultadoRequisito_OSCod = 0 ;
   this.O2003ContagemResultadoRequisito_OSCod = 0 ;
   this.A2003ContagemResultadoRequisito_OSCod = 0 ;
   this.AV67ContagemResultadoRequisito_OSCod = 0 ;
   this.A1999Requisito_ReqCod = 0 ;
   this.A1636ContagemResultado_ServicoSS = 0 ;
   this.A1932Requisito_Pontuacao = 0 ;
   this.A1934Requisito_Status = 0 ;
   this.A1927Requisito_Titulo = "" ;
   this.A2001Requisito_Identificador = "" ;
   this.A1926Requisito_Agrupador = "" ;
   this.A1923Requisito_Descricao = "" ;
   this.A508ContagemResultado_Owner = 0 ;
   this.A2006ContagemResultadoRequisito_Owner = false ;
   this.AV54Descricao = "" ;
   this.A2004ContagemResultadoRequisito_ReqCod = 0 ;
   this.AV70Pgmname = "" ;
   this.AV65PontosFilhos = 0 ;
   this.AV60Total = 0 ;
   this.AV6WWPContext = {} ;
   this.AV63Requisito_Codigo = 0 ;
   this.AV7ContagemResultado_Codigo = 0 ;
   this.Events = {"e12pg2_client": ["'DONEWRN'", true] ,"e16pg2_client": ["'DOUPDATE'", true] ,"e17pg2_client": ["'DONEWREQTEC'", true] ,"e11pg2_client": ["CONFIRMPANEL.CLOSE", true] ,"e20pg2_client": ["ENTER", true] ,"e21pg2_client": ["CANCEL", true] ,"e18pg1_client": ["'DOPDF'", false] ,"e19pg2_client": ["'DOACTIONDELETE'", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Linktarget")',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("PDF","Tooltiptext")',ctrl:'PDF',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],[{ctrl:'WCWC_REQUISITOSTECNICOS'},{ctrl:'WCWC_REQUISITOSTECNICOS',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'this.DVPANEL_TBLREQNEGContainer.Title',ctrl:'DVPANEL_TBLREQNEG',prop:'Title'},{av:'AV54Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'this.DVPANEL_UNNAMEDTABLE2Container.Title',ctrl:'DVPANEL_UNNAMEDTABLE2',prop:'Title'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]];
   this.EvtParms["'DONEWRN'"] = [[{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],[]];
   this.EvtParms["'DOPDF'"] = [[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'this.INNEWWINDOWContainer.Target',ctrl:'INNEWWINDOW',prop:'Target'}]];
   this.EvtParms["'DOUPDATE'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["'DOACTIONDELETE'"] = [[],[]];
   this.EvtParms["'DONEWREQTEC'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["CONFIRMPANEL.CLOSE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.CONFIRMPANELContainer.Result',ctrl:'CONFIRMPANEL',prop:'Result'}],[{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_FIRSTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Linktarget")',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("PDF","Tooltiptext")',ctrl:'PDF',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'}]];
   this.EvtParms["GRID_PREVPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Linktarget")',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("PDF","Tooltiptext")',ctrl:'PDF',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'}]];
   this.EvtParms["GRID_NEXTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Linktarget")',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("PDF","Tooltiptext")',ctrl:'PDF',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'}]];
   this.EvtParms["GRID_LASTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultadoRequisito_OSCod',fld:'vCONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOREQUISITO_REQCOD","Visible")',ctrl:'CONTAGEMRESULTADOREQUISITO_REQCOD',prop:'Visible'},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV65PontosFilhos',fld:'vPONTOSFILHOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2006ContagemResultadoRequisito_Owner',fld:'CONTAGEMRESULTADOREQUISITO_OWNER',pic:'',nv:false},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV63Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV70Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV60Total',fld:'vTOTAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Linktarget")',ctrl:'NEWREQTEC',prop:'Linktarget'},{av:'gx.fn.getCtrlProperty("PDF","Visible")',ctrl:'PDF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("PDF","Tooltiptext")',ctrl:'PDF',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("NEWRN","Visible")',ctrl:'NEWRN',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NEWREQTEC","Visible")',ctrl:'NEWREQTEC',prop:'Visible'},{av:'gx.fn.getCtrlProperty("UPDATE","Visible")',ctrl:'UPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ACTIONDELETE","Visible")',ctrl:'ACTIONDELETE',prop:'Visible'}]];
   this.setVCMap("AV67ContagemResultadoRequisito_OSCod", "vCONTAGEMRESULTADOREQUISITO_OSCOD", 0, "int");
   this.setVCMap("AV70Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A1636ContagemResultado_ServicoSS", "CONTAGEMRESULTADO_SERVICOSS", 0, "int");
   this.setVCMap("A1932Requisito_Pontuacao", "REQUISITO_PONTUACAO", 0, "decimal");
   this.setVCMap("AV65PontosFilhos", "vPONTOSFILHOS", 0, "decimal");
   this.setVCMap("AV60Total", "vTOTAL", 0, "decimal");
   this.setVCMap("A1926Requisito_Agrupador", "REQUISITO_AGRUPADOR", 0, "svchar");
   this.setVCMap("A2001Requisito_Identificador", "REQUISITO_IDENTIFICADOR", 0, "svchar");
   this.setVCMap("A1927Requisito_Titulo", "REQUISITO_TITULO", 0, "svchar");
   this.setVCMap("A1934Requisito_Status", "REQUISITO_STATUS", 0, "int");
   this.setVCMap("A1923Requisito_Descricao", "REQUISITO_DESCRICAO", 0, "vchar");
   this.setVCMap("A508ContagemResultado_Owner", "CONTAGEMRESULTADO_OWNER", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A2006ContagemResultadoRequisito_Owner", "CONTAGEMRESULTADOREQUISITO_OWNER", 0, "boolean");
   this.setVCMap("A1999Requisito_ReqCod", "REQUISITO_REQCOD", 0, "int");
   this.setVCMap("AV63Requisito_Codigo", "vREQUISITO_CODIGO", 0, "int");
   this.setVCMap("AV7ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV67ContagemResultadoRequisito_OSCod", "vCONTAGEMRESULTADOREQUISITO_OSCOD", 0, "int");
   this.setVCMap("AV70Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A1636ContagemResultado_ServicoSS", "CONTAGEMRESULTADO_SERVICOSS", 0, "int");
   this.setVCMap("A1932Requisito_Pontuacao", "REQUISITO_PONTUACAO", 0, "decimal");
   this.setVCMap("AV65PontosFilhos", "vPONTOSFILHOS", 0, "decimal");
   this.setVCMap("AV60Total", "vTOTAL", 0, "decimal");
   this.setVCMap("A1926Requisito_Agrupador", "REQUISITO_AGRUPADOR", 0, "svchar");
   this.setVCMap("A2001Requisito_Identificador", "REQUISITO_IDENTIFICADOR", 0, "svchar");
   this.setVCMap("A1927Requisito_Titulo", "REQUISITO_TITULO", 0, "svchar");
   this.setVCMap("A1934Requisito_Status", "REQUISITO_STATUS", 0, "int");
   this.setVCMap("A1923Requisito_Descricao", "REQUISITO_DESCRICAO", 0, "vchar");
   this.setVCMap("A508ContagemResultado_Owner", "CONTAGEMRESULTADO_OWNER", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A2006ContagemResultadoRequisito_Owner", "CONTAGEMRESULTADOREQUISITO_OWNER", 0, "boolean");
   this.setVCMap("A1999Requisito_ReqCod", "REQUISITO_REQCOD", 0, "int");
   this.setVCMap("AV63Requisito_Codigo", "vREQUISITO_CODIGO", 0, "int");
   GridContainer.addRefreshingVar({rfrVar:"AV67ContagemResultadoRequisito_OSCod"});
   GridContainer.addRefreshingVar({rfrVar:"A2004ContagemResultadoRequisito_ReqCod", rfrProp:"Visible", gxAttId:"2004"});
   GridContainer.addRefreshingVar({rfrVar:"AV70Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"A1636ContagemResultado_ServicoSS"});
   GridContainer.addRefreshingVar({rfrVar:"A2004ContagemResultadoRequisito_ReqCod", rfrProp:"Value", gxAttId:"2004"});
   GridContainer.addRefreshingVar({rfrVar:"A1932Requisito_Pontuacao"});
   GridContainer.addRefreshingVar({rfrVar:"AV65PontosFilhos"});
   GridContainer.addRefreshingVar({rfrVar:"AV60Total"});
   GridContainer.addRefreshingVar({rfrVar:"A1926Requisito_Agrupador"});
   GridContainer.addRefreshingVar({rfrVar:"A2001Requisito_Identificador"});
   GridContainer.addRefreshingVar({rfrVar:"A1927Requisito_Titulo"});
   GridContainer.addRefreshingVar({rfrVar:"A1934Requisito_Status"});
   GridContainer.addRefreshingVar({rfrVar:"A1923Requisito_Descricao"});
   GridContainer.addRefreshingVar({rfrVar:"A508ContagemResultado_Owner"});
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"A2006ContagemResultadoRequisito_Owner"});
   GridContainer.addRefreshingVar({rfrVar:"A1999Requisito_ReqCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV63Requisito_Codigo"});
   this.InitStandaloneVars( );
   this.setComponent({id: "GX_PROCESS" ,GXClass: null , Prefix: "W00-1" , lvl: 1 });
   this.setComponent({id: "WCWC_REQUISITOSTECNICOS" ,GXClass: null , Prefix: "W0024" , lvl: 2 });
});
