/*
               File: GetWWMetodologiaFilterData
        Description: Get WWMetodologia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:29.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwmetodologiafilterdata : GXProcedure
   {
      public getwwmetodologiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwmetodologiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwmetodologiafilterdata objgetwwmetodologiafilterdata;
         objgetwwmetodologiafilterdata = new getwwmetodologiafilterdata();
         objgetwwmetodologiafilterdata.AV14DDOName = aP0_DDOName;
         objgetwwmetodologiafilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetwwmetodologiafilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetwwmetodologiafilterdata.AV18OptionsJson = "" ;
         objgetwwmetodologiafilterdata.AV21OptionsDescJson = "" ;
         objgetwwmetodologiafilterdata.AV23OptionIndexesJson = "" ;
         objgetwwmetodologiafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwmetodologiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwmetodologiafilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwmetodologiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_METODOLOGIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIA_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("WWMetodologiaGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWMetodologiaGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("WWMetodologiaGridState"), "");
         }
         AV34GXV1 = 1;
         while ( AV34GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV34GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV10TFMetodologia_Descricao = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV11TFMetodologia_Descricao_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            AV34GXV1 = (int)(AV34GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "METODOLOGIA_DESCRICAO") == 0 )
            {
               AV31Metodologia_Descricao1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMETODOLOGIA_DESCRICAOOPTIONS' Routine */
         AV10TFMetodologia_Descricao = AV12SearchTxt;
         AV11TFMetodologia_Descricao_Sel = "";
         AV36WWMetodologiaDS_1_Dynamicfiltersselector1 = AV30DynamicFiltersSelector1;
         AV37WWMetodologiaDS_2_Metodologia_descricao1 = AV31Metodologia_Descricao1;
         AV38WWMetodologiaDS_3_Tfmetodologia_descricao = AV10TFMetodologia_Descricao;
         AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel = AV11TFMetodologia_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV36WWMetodologiaDS_1_Dynamicfiltersselector1 ,
                                              AV37WWMetodologiaDS_2_Metodologia_descricao1 ,
                                              AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel ,
                                              AV38WWMetodologiaDS_3_Tfmetodologia_descricao ,
                                              A138Metodologia_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV37WWMetodologiaDS_2_Metodologia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37WWMetodologiaDS_2_Metodologia_descricao1), "%", "");
         lV38WWMetodologiaDS_3_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV38WWMetodologiaDS_3_Tfmetodologia_descricao), "%", "");
         /* Using cursor P00HH2 */
         pr_default.execute(0, new Object[] {lV37WWMetodologiaDS_2_Metodologia_descricao1, lV38WWMetodologiaDS_3_Tfmetodologia_descricao, AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKHH2 = false;
            A138Metodologia_Descricao = P00HH2_A138Metodologia_Descricao[0];
            A137Metodologia_Codigo = P00HH2_A137Metodologia_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00HH2_A138Metodologia_Descricao[0], A138Metodologia_Descricao) == 0 ) )
            {
               BRKHH2 = false;
               A137Metodologia_Codigo = P00HH2_A137Metodologia_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKHH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A138Metodologia_Descricao)) )
            {
               AV16Option = A138Metodologia_Descricao;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHH2 )
            {
               BRKHH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFMetodologia_Descricao = "";
         AV11TFMetodologia_Descricao_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV31Metodologia_Descricao1 = "";
         AV36WWMetodologiaDS_1_Dynamicfiltersselector1 = "";
         AV37WWMetodologiaDS_2_Metodologia_descricao1 = "";
         AV38WWMetodologiaDS_3_Tfmetodologia_descricao = "";
         AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel = "";
         scmdbuf = "";
         lV37WWMetodologiaDS_2_Metodologia_descricao1 = "";
         lV38WWMetodologiaDS_3_Tfmetodologia_descricao = "";
         A138Metodologia_Descricao = "";
         P00HH2_A138Metodologia_Descricao = new String[] {""} ;
         P00HH2_A137Metodologia_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwmetodologiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00HH2_A138Metodologia_Descricao, P00HH2_A137Metodologia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV34GXV1 ;
      private int A137Metodologia_Codigo ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKHH2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFMetodologia_Descricao ;
      private String AV11TFMetodologia_Descricao_Sel ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV31Metodologia_Descricao1 ;
      private String AV36WWMetodologiaDS_1_Dynamicfiltersselector1 ;
      private String AV37WWMetodologiaDS_2_Metodologia_descricao1 ;
      private String AV38WWMetodologiaDS_3_Tfmetodologia_descricao ;
      private String AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel ;
      private String lV37WWMetodologiaDS_2_Metodologia_descricao1 ;
      private String lV38WWMetodologiaDS_3_Tfmetodologia_descricao ;
      private String A138Metodologia_Descricao ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00HH2_A138Metodologia_Descricao ;
      private int[] P00HH2_A137Metodologia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getwwmetodologiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00HH2( IGxContext context ,
                                             String AV36WWMetodologiaDS_1_Dynamicfiltersselector1 ,
                                             String AV37WWMetodologiaDS_2_Metodologia_descricao1 ,
                                             String AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel ,
                                             String AV38WWMetodologiaDS_3_Tfmetodologia_descricao ,
                                             String A138Metodologia_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Metodologia_Descricao], [Metodologia_Codigo] FROM [Metodologia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV36WWMetodologiaDS_1_Dynamicfiltersselector1, "METODOLOGIA_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37WWMetodologiaDS_2_Metodologia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Metodologia_Descricao] like '%' + @lV37WWMetodologiaDS_2_Metodologia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Metodologia_Descricao] like '%' + @lV37WWMetodologiaDS_2_Metodologia_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38WWMetodologiaDS_3_Tfmetodologia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Metodologia_Descricao] like @lV38WWMetodologiaDS_3_Tfmetodologia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Metodologia_Descricao] like @lV38WWMetodologiaDS_3_Tfmetodologia_descricao)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Metodologia_Descricao] = @AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Metodologia_Descricao] = @AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Metodologia_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00HH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00HH2 ;
          prmP00HH2 = new Object[] {
          new Object[] {"@lV37WWMetodologiaDS_2_Metodologia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV38WWMetodologiaDS_3_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV39WWMetodologiaDS_4_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00HH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwmetodologiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwmetodologiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwmetodologiafilterdata") )
          {
             return  ;
          }
          getwwmetodologiafilterdata worker = new getwwmetodologiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
