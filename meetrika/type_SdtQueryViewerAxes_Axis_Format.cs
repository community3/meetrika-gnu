/*
               File: type_SdtQueryViewerAxes_Axis_Format
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:57.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis.Format" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis.Format" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_Format_ValueStyle ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtQueryViewerAxes_Axis_Format_ConditionalStyle ))]
   [Serializable]
   public class SdtQueryViewerAxes_Axis_Format : GxUserType
   {
      public SdtQueryViewerAxes_Axis_Format( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_Format_Picture = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_Style = "";
      }

      public SdtQueryViewerAxes_Axis_Format( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis_Format deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis_Format)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis_Format obj ;
         obj = this;
         obj.gxTpr_Picture = deserialized.gxTpr_Picture;
         obj.gxTpr_Subtotals = deserialized.gxTpr_Subtotals;
         obj.gxTpr_Candragtopages = deserialized.gxTpr_Candragtopages;
         obj.gxTpr_Style = deserialized.gxTpr_Style;
         obj.gxTpr_Valuesstyles = deserialized.gxTpr_Valuesstyles;
         obj.gxTpr_Conditionalstyles = deserialized.gxTpr_Conditionalstyles;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Picture") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_Picture = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Subtotals") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CanDragToPages") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Style") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_Style = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ValuesStyles") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ValueStyle", "", "SdtQueryViewerAxes_Axis_Format_ValueStyle", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles.readxmlcollection(oReader, "ValuesStyles", "ValueStyle");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ConditionalStyles") )
               {
                  if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles == null )
                  {
                     gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ConditionalStyle", "", "SdtQueryViewerAxes_Axis_Format_ConditionalStyle", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles.readxmlcollection(oReader, "ConditionalStyles", "ConditionalStyle");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis.Format";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Picture", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_Picture));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Subtotals", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CanDragToPages", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Style", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_Style));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles.writexmlcollection(oWriter, "ValuesStyles", sNameSpace1, "ValueStyle", sNameSpace1);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles.writexmlcollection(oWriter, "ConditionalStyles", sNameSpace1, "ConditionalStyle", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Picture", gxTv_SdtQueryViewerAxes_Axis_Format_Picture, false);
         AddObjectProperty("Subtotals", gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals, false);
         AddObjectProperty("CanDragToPages", gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages, false);
         AddObjectProperty("Style", gxTv_SdtQueryViewerAxes_Axis_Format_Style, false);
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles != null )
         {
            AddObjectProperty("ValuesStyles", gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles, false);
         }
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles != null )
         {
            AddObjectProperty("ConditionalStyles", gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Picture" )]
      [  XmlElement( ElementName = "Picture"   )]
      public String gxTpr_Picture
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_Picture ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Picture = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Subtotals" )]
      [  XmlElement( ElementName = "Subtotals"   )]
      public String gxTpr_Subtotals
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals = (String)(value);
         }

      }

      [  SoapElement( ElementName = "CanDragToPages" )]
      [  XmlElement( ElementName = "CanDragToPages"   )]
      public bool gxTpr_Candragtopages
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages = value;
         }

      }

      [  SoapElement( ElementName = "Style" )]
      [  XmlElement( ElementName = "Style"   )]
      public String gxTpr_Style
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_Style ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Style = (String)(value);
         }

      }

      public class gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles_SdtQueryViewerAxes_Axis_Format_ValueStyle_80compatibility:SdtQueryViewerAxes_Axis_Format_ValueStyle {}
      [  SoapElement( ElementName = "ValuesStyles" )]
      [  XmlArray( ElementName = "ValuesStyles"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtQueryViewerAxes_Axis_Format_ValueStyle ), ElementName= "ValueStyle"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles_SdtQueryViewerAxes_Axis_Format_ValueStyle_80compatibility ), ElementName= "QueryViewerAxes.Axis.Format.ValueStyle"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Valuesstyles_GxObjectCollection
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ValueStyle", "", "SdtQueryViewerAxes_Axis_Format_ValueStyle", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles ;
         }

         set {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ValueStyle", "", "SdtQueryViewerAxes_Axis_Format_ValueStyle", "GeneXus.Programs");
            }
            gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Valuesstyles
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ValueStyle", "", "SdtQueryViewerAxes_Axis_Format_ValueStyle", "GeneXus.Programs");
            }
            return gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_80compatibility:SdtQueryViewerAxes_Axis_Format_ConditionalStyle {}
      [  SoapElement( ElementName = "ConditionalStyles" )]
      [  XmlArray( ElementName = "ConditionalStyles"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtQueryViewerAxes_Axis_Format_ConditionalStyle ), ElementName= "ConditionalStyle"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_80compatibility ), ElementName= "QueryViewerAxes.Axis.Format.ConditionalStyle"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Conditionalstyles_GxObjectCollection
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ConditionalStyle", "", "SdtQueryViewerAxes_Axis_Format_ConditionalStyle", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles ;
         }

         set {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ConditionalStyle", "", "SdtQueryViewerAxes_Axis_Format_ConditionalStyle", "GeneXus.Programs");
            }
            gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Conditionalstyles
      {
         get {
            if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles == null )
            {
               gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = new GxObjectCollection( context, "QueryViewerAxes.Axis.Format.ConditionalStyle", "", "SdtQueryViewerAxes_Axis_Format_ConditionalStyle", "GeneXus.Programs");
            }
            return gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = value;
         }

      }

      public void gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles_SetNull( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles_IsNull( )
      {
         if ( gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format_Picture = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_Style = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_Picture ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_Subtotals ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_Style ;
      protected String sTagName ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Format_Candragtopages ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerAxes_Axis_Format_ValueStyle ))]
      protected IGxCollection gxTv_SdtQueryViewerAxes_Axis_Format_Valuesstyles=null ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerAxes_Axis_Format_ConditionalStyle ))]
      protected IGxCollection gxTv_SdtQueryViewerAxes_Axis_Format_Conditionalstyles=null ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis.Format", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtQueryViewerAxes_Axis_Format_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis_Format>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_Format_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_Format_RESTInterface( SdtQueryViewerAxes_Axis_Format psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Picture" , Order = 0 )]
      public String gxTpr_Picture
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Picture) ;
         }

         set {
            sdt.gxTpr_Picture = (String)(value);
         }

      }

      [DataMember( Name = "Subtotals" , Order = 1 )]
      public String gxTpr_Subtotals
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Subtotals) ;
         }

         set {
            sdt.gxTpr_Subtotals = (String)(value);
         }

      }

      [DataMember( Name = "CanDragToPages" , Order = 2 )]
      public bool gxTpr_Candragtopages
      {
         get {
            return sdt.gxTpr_Candragtopages ;
         }

         set {
            sdt.gxTpr_Candragtopages = value;
         }

      }

      [DataMember( Name = "Style" , Order = 3 )]
      public String gxTpr_Style
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Style) ;
         }

         set {
            sdt.gxTpr_Style = (String)(value);
         }

      }

      [DataMember( Name = "ValuesStyles" , Order = 4 )]
      public GxGenericCollection<SdtQueryViewerAxes_Axis_Format_ValueStyle_RESTInterface> gxTpr_Valuesstyles
      {
         get {
            return new GxGenericCollection<SdtQueryViewerAxes_Axis_Format_ValueStyle_RESTInterface>(sdt.gxTpr_Valuesstyles) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Valuesstyles);
         }

      }

      [DataMember( Name = "ConditionalStyles" , Order = 5 )]
      public GxGenericCollection<SdtQueryViewerAxes_Axis_Format_ConditionalStyle_RESTInterface> gxTpr_Conditionalstyles
      {
         get {
            return new GxGenericCollection<SdtQueryViewerAxes_Axis_Format_ConditionalStyle_RESTInterface>(sdt.gxTpr_Conditionalstyles) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Conditionalstyles);
         }

      }

      public SdtQueryViewerAxes_Axis_Format sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis_Format)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis_Format() ;
         }
      }

   }

}
