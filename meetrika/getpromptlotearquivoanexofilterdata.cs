/*
               File: GetPromptLoteArquivoAnexoFilterData
        Description: Get Prompt Lote Arquivo Anexo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:24.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptlotearquivoanexofilterdata : GXProcedure
   {
      public getpromptlotearquivoanexofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptlotearquivoanexofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptlotearquivoanexofilterdata objgetpromptlotearquivoanexofilterdata;
         objgetpromptlotearquivoanexofilterdata = new getpromptlotearquivoanexofilterdata();
         objgetpromptlotearquivoanexofilterdata.AV24DDOName = aP0_DDOName;
         objgetpromptlotearquivoanexofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetpromptlotearquivoanexofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptlotearquivoanexofilterdata.AV28OptionsJson = "" ;
         objgetpromptlotearquivoanexofilterdata.AV31OptionsDescJson = "" ;
         objgetpromptlotearquivoanexofilterdata.AV33OptionIndexesJson = "" ;
         objgetpromptlotearquivoanexofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptlotearquivoanexofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptlotearquivoanexofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptlotearquivoanexofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTEARQUIVOANEXO_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_LOTEARQUIVOANEXO_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTEARQUIVOANEXO_TIPOARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_LOTEARQUIVOANEXO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADLOTEARQUIVOANEXO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_TIPODOCUMENTO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODOCUMENTO_NOMEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("PromptLoteArquivoAnexoGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptLoteArquivoAnexoGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("PromptLoteArquivoAnexoGridState"), "");
         }
         AV56GXV1 = 1;
         while ( AV56GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV56GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_LOTECOD") == 0 )
            {
               AV10TFLoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV11TFLoteArquivoAnexo_LoteCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_DATA") == 0 )
            {
               AV12TFLoteArquivoAnexo_Data = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV13TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV14TFLoteArquivoAnexo_NomeArq = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ_SEL") == 0 )
            {
               AV15TFLoteArquivoAnexo_NomeArq_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_TIPOARQ") == 0 )
            {
               AV16TFLoteArquivoAnexo_TipoArq = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_TIPOARQ_SEL") == 0 )
            {
               AV17TFLoteArquivoAnexo_TipoArq_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_DESCRICAO") == 0 )
            {
               AV18TFLoteArquivoAnexo_Descricao = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_DESCRICAO_SEL") == 0 )
            {
               AV19TFLoteArquivoAnexo_Descricao_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME") == 0 )
            {
               AV20TFTipoDocumento_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME_SEL") == 0 )
            {
               AV21TFTipoDocumento_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            AV56GXV1 = (int)(AV56GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42LoteArquivoAnexo_NomeArq1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV43TipoDocumento_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV47LoteArquivoAnexo_NomeArq2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV48TipoDocumento_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV52LoteArquivoAnexo_NomeArq3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV53TipoDocumento_Nome3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADLOTEARQUIVOANEXO_NOMEARQOPTIONS' Routine */
         AV14TFLoteArquivoAnexo_NomeArq = AV22SearchTxt;
         AV15TFLoteArquivoAnexo_NomeArq_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42LoteArquivoAnexo_NomeArq1 ,
                                              AV43TipoDocumento_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47LoteArquivoAnexo_NomeArq2 ,
                                              AV48TipoDocumento_Nome2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52LoteArquivoAnexo_NomeArq3 ,
                                              AV53TipoDocumento_Nome3 ,
                                              AV10TFLoteArquivoAnexo_LoteCod ,
                                              AV11TFLoteArquivoAnexo_LoteCod_To ,
                                              AV12TFLoteArquivoAnexo_Data ,
                                              AV13TFLoteArquivoAnexo_Data_To ,
                                              AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                              AV14TFLoteArquivoAnexo_NomeArq ,
                                              AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                              AV16TFLoteArquivoAnexo_TipoArq ,
                                              AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                              AV18TFLoteArquivoAnexo_Descricao ,
                                              AV21TFTipoDocumento_Nome_Sel ,
                                              AV20TFTipoDocumento_Nome ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              A836LoteArquivoAnexo_Data ,
                                              A840LoteArquivoAnexo_TipoArq ,
                                              A837LoteArquivoAnexo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV14TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq), 50, "%");
         lV16TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq), 10, "%");
         lV18TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao), "%", "");
         lV20TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV20TFTipoDocumento_Nome), 50, "%");
         /* Using cursor P00OK2 */
         pr_default.execute(0, new Object[] {lV42LoteArquivoAnexo_NomeArq1, lV42LoteArquivoAnexo_NomeArq1, lV43TipoDocumento_Nome1, lV43TipoDocumento_Nome1, lV47LoteArquivoAnexo_NomeArq2, lV47LoteArquivoAnexo_NomeArq2, lV48TipoDocumento_Nome2, lV48TipoDocumento_Nome2, lV52LoteArquivoAnexo_NomeArq3, lV52LoteArquivoAnexo_NomeArq3, lV53TipoDocumento_Nome3, lV53TipoDocumento_Nome3, AV10TFLoteArquivoAnexo_LoteCod, AV11TFLoteArquivoAnexo_LoteCod_To, AV12TFLoteArquivoAnexo_Data, AV13TFLoteArquivoAnexo_Data_To, lV14TFLoteArquivoAnexo_NomeArq, AV15TFLoteArquivoAnexo_NomeArq_Sel, lV16TFLoteArquivoAnexo_TipoArq, AV17TFLoteArquivoAnexo_TipoArq_Sel, lV18TFLoteArquivoAnexo_Descricao, AV19TFLoteArquivoAnexo_Descricao_Sel, lV20TFTipoDocumento_Nome, AV21TFTipoDocumento_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOK2 = false;
            A645TipoDocumento_Codigo = P00OK2_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OK2_n645TipoDocumento_Codigo[0];
            A839LoteArquivoAnexo_NomeArq = P00OK2_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OK2_n839LoteArquivoAnexo_NomeArq[0];
            A837LoteArquivoAnexo_Descricao = P00OK2_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = P00OK2_n837LoteArquivoAnexo_Descricao[0];
            A840LoteArquivoAnexo_TipoArq = P00OK2_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = P00OK2_n840LoteArquivoAnexo_TipoArq[0];
            A836LoteArquivoAnexo_Data = P00OK2_A836LoteArquivoAnexo_Data[0];
            A841LoteArquivoAnexo_LoteCod = P00OK2_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OK2_A646TipoDocumento_Nome[0];
            A646TipoDocumento_Nome = P00OK2_A646TipoDocumento_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00OK2_A839LoteArquivoAnexo_NomeArq[0], A839LoteArquivoAnexo_NomeArq) == 0 ) )
            {
               BRKOK2 = false;
               A836LoteArquivoAnexo_Data = P00OK2_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OK2_A841LoteArquivoAnexo_LoteCod[0];
               AV34count = (long)(AV34count+1);
               BRKOK2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq)) )
            {
               AV26Option = A839LoteArquivoAnexo_NomeArq;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOK2 )
            {
               BRKOK2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADLOTEARQUIVOANEXO_TIPOARQOPTIONS' Routine */
         AV16TFLoteArquivoAnexo_TipoArq = AV22SearchTxt;
         AV17TFLoteArquivoAnexo_TipoArq_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42LoteArquivoAnexo_NomeArq1 ,
                                              AV43TipoDocumento_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47LoteArquivoAnexo_NomeArq2 ,
                                              AV48TipoDocumento_Nome2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52LoteArquivoAnexo_NomeArq3 ,
                                              AV53TipoDocumento_Nome3 ,
                                              AV10TFLoteArquivoAnexo_LoteCod ,
                                              AV11TFLoteArquivoAnexo_LoteCod_To ,
                                              AV12TFLoteArquivoAnexo_Data ,
                                              AV13TFLoteArquivoAnexo_Data_To ,
                                              AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                              AV14TFLoteArquivoAnexo_NomeArq ,
                                              AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                              AV16TFLoteArquivoAnexo_TipoArq ,
                                              AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                              AV18TFLoteArquivoAnexo_Descricao ,
                                              AV21TFTipoDocumento_Nome_Sel ,
                                              AV20TFTipoDocumento_Nome ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              A836LoteArquivoAnexo_Data ,
                                              A840LoteArquivoAnexo_TipoArq ,
                                              A837LoteArquivoAnexo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV14TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq), 50, "%");
         lV16TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq), 10, "%");
         lV18TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao), "%", "");
         lV20TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV20TFTipoDocumento_Nome), 50, "%");
         /* Using cursor P00OK3 */
         pr_default.execute(1, new Object[] {lV42LoteArquivoAnexo_NomeArq1, lV42LoteArquivoAnexo_NomeArq1, lV43TipoDocumento_Nome1, lV43TipoDocumento_Nome1, lV47LoteArquivoAnexo_NomeArq2, lV47LoteArquivoAnexo_NomeArq2, lV48TipoDocumento_Nome2, lV48TipoDocumento_Nome2, lV52LoteArquivoAnexo_NomeArq3, lV52LoteArquivoAnexo_NomeArq3, lV53TipoDocumento_Nome3, lV53TipoDocumento_Nome3, AV10TFLoteArquivoAnexo_LoteCod, AV11TFLoteArquivoAnexo_LoteCod_To, AV12TFLoteArquivoAnexo_Data, AV13TFLoteArquivoAnexo_Data_To, lV14TFLoteArquivoAnexo_NomeArq, AV15TFLoteArquivoAnexo_NomeArq_Sel, lV16TFLoteArquivoAnexo_TipoArq, AV17TFLoteArquivoAnexo_TipoArq_Sel, lV18TFLoteArquivoAnexo_Descricao, AV19TFLoteArquivoAnexo_Descricao_Sel, lV20TFTipoDocumento_Nome, AV21TFTipoDocumento_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOK4 = false;
            A645TipoDocumento_Codigo = P00OK3_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OK3_n645TipoDocumento_Codigo[0];
            A840LoteArquivoAnexo_TipoArq = P00OK3_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = P00OK3_n840LoteArquivoAnexo_TipoArq[0];
            A837LoteArquivoAnexo_Descricao = P00OK3_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = P00OK3_n837LoteArquivoAnexo_Descricao[0];
            A836LoteArquivoAnexo_Data = P00OK3_A836LoteArquivoAnexo_Data[0];
            A841LoteArquivoAnexo_LoteCod = P00OK3_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OK3_A646TipoDocumento_Nome[0];
            A839LoteArquivoAnexo_NomeArq = P00OK3_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OK3_n839LoteArquivoAnexo_NomeArq[0];
            A646TipoDocumento_Nome = P00OK3_A646TipoDocumento_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OK3_A840LoteArquivoAnexo_TipoArq[0], A840LoteArquivoAnexo_TipoArq) == 0 ) )
            {
               BRKOK4 = false;
               A836LoteArquivoAnexo_Data = P00OK3_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OK3_A841LoteArquivoAnexo_LoteCod[0];
               AV34count = (long)(AV34count+1);
               BRKOK4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq)) )
            {
               AV26Option = A840LoteArquivoAnexo_TipoArq;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOK4 )
            {
               BRKOK4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADLOTEARQUIVOANEXO_DESCRICAOOPTIONS' Routine */
         AV18TFLoteArquivoAnexo_Descricao = AV22SearchTxt;
         AV19TFLoteArquivoAnexo_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42LoteArquivoAnexo_NomeArq1 ,
                                              AV43TipoDocumento_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47LoteArquivoAnexo_NomeArq2 ,
                                              AV48TipoDocumento_Nome2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52LoteArquivoAnexo_NomeArq3 ,
                                              AV53TipoDocumento_Nome3 ,
                                              AV10TFLoteArquivoAnexo_LoteCod ,
                                              AV11TFLoteArquivoAnexo_LoteCod_To ,
                                              AV12TFLoteArquivoAnexo_Data ,
                                              AV13TFLoteArquivoAnexo_Data_To ,
                                              AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                              AV14TFLoteArquivoAnexo_NomeArq ,
                                              AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                              AV16TFLoteArquivoAnexo_TipoArq ,
                                              AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                              AV18TFLoteArquivoAnexo_Descricao ,
                                              AV21TFTipoDocumento_Nome_Sel ,
                                              AV20TFTipoDocumento_Nome ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              A836LoteArquivoAnexo_Data ,
                                              A840LoteArquivoAnexo_TipoArq ,
                                              A837LoteArquivoAnexo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV14TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq), 50, "%");
         lV16TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq), 10, "%");
         lV18TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao), "%", "");
         lV20TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV20TFTipoDocumento_Nome), 50, "%");
         /* Using cursor P00OK4 */
         pr_default.execute(2, new Object[] {lV42LoteArquivoAnexo_NomeArq1, lV42LoteArquivoAnexo_NomeArq1, lV43TipoDocumento_Nome1, lV43TipoDocumento_Nome1, lV47LoteArquivoAnexo_NomeArq2, lV47LoteArquivoAnexo_NomeArq2, lV48TipoDocumento_Nome2, lV48TipoDocumento_Nome2, lV52LoteArquivoAnexo_NomeArq3, lV52LoteArquivoAnexo_NomeArq3, lV53TipoDocumento_Nome3, lV53TipoDocumento_Nome3, AV10TFLoteArquivoAnexo_LoteCod, AV11TFLoteArquivoAnexo_LoteCod_To, AV12TFLoteArquivoAnexo_Data, AV13TFLoteArquivoAnexo_Data_To, lV14TFLoteArquivoAnexo_NomeArq, AV15TFLoteArquivoAnexo_NomeArq_Sel, lV16TFLoteArquivoAnexo_TipoArq, AV17TFLoteArquivoAnexo_TipoArq_Sel, lV18TFLoteArquivoAnexo_Descricao, AV19TFLoteArquivoAnexo_Descricao_Sel, lV20TFTipoDocumento_Nome, AV21TFTipoDocumento_Nome_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKOK6 = false;
            A645TipoDocumento_Codigo = P00OK4_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OK4_n645TipoDocumento_Codigo[0];
            A837LoteArquivoAnexo_Descricao = P00OK4_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = P00OK4_n837LoteArquivoAnexo_Descricao[0];
            A840LoteArquivoAnexo_TipoArq = P00OK4_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = P00OK4_n840LoteArquivoAnexo_TipoArq[0];
            A836LoteArquivoAnexo_Data = P00OK4_A836LoteArquivoAnexo_Data[0];
            A841LoteArquivoAnexo_LoteCod = P00OK4_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OK4_A646TipoDocumento_Nome[0];
            A839LoteArquivoAnexo_NomeArq = P00OK4_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OK4_n839LoteArquivoAnexo_NomeArq[0];
            A646TipoDocumento_Nome = P00OK4_A646TipoDocumento_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00OK4_A837LoteArquivoAnexo_Descricao[0], A837LoteArquivoAnexo_Descricao) == 0 ) )
            {
               BRKOK6 = false;
               A836LoteArquivoAnexo_Data = P00OK4_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OK4_A841LoteArquivoAnexo_LoteCod[0];
               AV34count = (long)(AV34count+1);
               BRKOK6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A837LoteArquivoAnexo_Descricao)) )
            {
               AV26Option = A837LoteArquivoAnexo_Descricao;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOK6 )
            {
               BRKOK6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADTIPODOCUMENTO_NOMEOPTIONS' Routine */
         AV20TFTipoDocumento_Nome = AV22SearchTxt;
         AV21TFTipoDocumento_Nome_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42LoteArquivoAnexo_NomeArq1 ,
                                              AV43TipoDocumento_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47LoteArquivoAnexo_NomeArq2 ,
                                              AV48TipoDocumento_Nome2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52LoteArquivoAnexo_NomeArq3 ,
                                              AV53TipoDocumento_Nome3 ,
                                              AV10TFLoteArquivoAnexo_LoteCod ,
                                              AV11TFLoteArquivoAnexo_LoteCod_To ,
                                              AV12TFLoteArquivoAnexo_Data ,
                                              AV13TFLoteArquivoAnexo_Data_To ,
                                              AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                              AV14TFLoteArquivoAnexo_NomeArq ,
                                              AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                              AV16TFLoteArquivoAnexo_TipoArq ,
                                              AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                              AV18TFLoteArquivoAnexo_Descricao ,
                                              AV21TFTipoDocumento_Nome_Sel ,
                                              AV20TFTipoDocumento_Nome ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              A836LoteArquivoAnexo_Data ,
                                              A840LoteArquivoAnexo_TipoArq ,
                                              A837LoteArquivoAnexo_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV42LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV43TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43TipoDocumento_Nome1), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV47LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV48TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV48TipoDocumento_Nome2), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV52LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV53TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV53TipoDocumento_Nome3), 50, "%");
         lV14TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq), 50, "%");
         lV16TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq), 10, "%");
         lV18TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao), "%", "");
         lV20TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV20TFTipoDocumento_Nome), 50, "%");
         /* Using cursor P00OK5 */
         pr_default.execute(3, new Object[] {lV42LoteArquivoAnexo_NomeArq1, lV42LoteArquivoAnexo_NomeArq1, lV43TipoDocumento_Nome1, lV43TipoDocumento_Nome1, lV47LoteArquivoAnexo_NomeArq2, lV47LoteArquivoAnexo_NomeArq2, lV48TipoDocumento_Nome2, lV48TipoDocumento_Nome2, lV52LoteArquivoAnexo_NomeArq3, lV52LoteArquivoAnexo_NomeArq3, lV53TipoDocumento_Nome3, lV53TipoDocumento_Nome3, AV10TFLoteArquivoAnexo_LoteCod, AV11TFLoteArquivoAnexo_LoteCod_To, AV12TFLoteArquivoAnexo_Data, AV13TFLoteArquivoAnexo_Data_To, lV14TFLoteArquivoAnexo_NomeArq, AV15TFLoteArquivoAnexo_NomeArq_Sel, lV16TFLoteArquivoAnexo_TipoArq, AV17TFLoteArquivoAnexo_TipoArq_Sel, lV18TFLoteArquivoAnexo_Descricao, AV19TFLoteArquivoAnexo_Descricao_Sel, lV20TFTipoDocumento_Nome, AV21TFTipoDocumento_Nome_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKOK8 = false;
            A645TipoDocumento_Codigo = P00OK5_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = P00OK5_n645TipoDocumento_Codigo[0];
            A837LoteArquivoAnexo_Descricao = P00OK5_A837LoteArquivoAnexo_Descricao[0];
            n837LoteArquivoAnexo_Descricao = P00OK5_n837LoteArquivoAnexo_Descricao[0];
            A840LoteArquivoAnexo_TipoArq = P00OK5_A840LoteArquivoAnexo_TipoArq[0];
            n840LoteArquivoAnexo_TipoArq = P00OK5_n840LoteArquivoAnexo_TipoArq[0];
            A836LoteArquivoAnexo_Data = P00OK5_A836LoteArquivoAnexo_Data[0];
            A841LoteArquivoAnexo_LoteCod = P00OK5_A841LoteArquivoAnexo_LoteCod[0];
            A646TipoDocumento_Nome = P00OK5_A646TipoDocumento_Nome[0];
            A839LoteArquivoAnexo_NomeArq = P00OK5_A839LoteArquivoAnexo_NomeArq[0];
            n839LoteArquivoAnexo_NomeArq = P00OK5_n839LoteArquivoAnexo_NomeArq[0];
            A646TipoDocumento_Nome = P00OK5_A646TipoDocumento_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00OK5_A645TipoDocumento_Codigo[0] == A645TipoDocumento_Codigo ) )
            {
               BRKOK8 = false;
               A836LoteArquivoAnexo_Data = P00OK5_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = P00OK5_A841LoteArquivoAnexo_LoteCod[0];
               AV34count = (long)(AV34count+1);
               BRKOK8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A646TipoDocumento_Nome)) )
            {
               AV26Option = A646TipoDocumento_Nome;
               AV25InsertIndex = 1;
               while ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) < 0 ) )
               {
                  AV25InsertIndex = (int)(AV25InsertIndex+1);
               }
               AV27Options.Add(AV26Option, AV25InsertIndex);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), AV25InsertIndex);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOK8 )
            {
               BRKOK8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFLoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV13TFLoteArquivoAnexo_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV14TFLoteArquivoAnexo_NomeArq = "";
         AV15TFLoteArquivoAnexo_NomeArq_Sel = "";
         AV16TFLoteArquivoAnexo_TipoArq = "";
         AV17TFLoteArquivoAnexo_TipoArq_Sel = "";
         AV18TFLoteArquivoAnexo_Descricao = "";
         AV19TFLoteArquivoAnexo_Descricao_Sel = "";
         AV20TFTipoDocumento_Nome = "";
         AV21TFTipoDocumento_Nome_Sel = "";
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42LoteArquivoAnexo_NomeArq1 = "";
         AV43TipoDocumento_Nome1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47LoteArquivoAnexo_NomeArq2 = "";
         AV48TipoDocumento_Nome2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52LoteArquivoAnexo_NomeArq3 = "";
         AV53TipoDocumento_Nome3 = "";
         scmdbuf = "";
         lV42LoteArquivoAnexo_NomeArq1 = "";
         lV43TipoDocumento_Nome1 = "";
         lV47LoteArquivoAnexo_NomeArq2 = "";
         lV48TipoDocumento_Nome2 = "";
         lV52LoteArquivoAnexo_NomeArq3 = "";
         lV53TipoDocumento_Nome3 = "";
         lV14TFLoteArquivoAnexo_NomeArq = "";
         lV16TFLoteArquivoAnexo_TipoArq = "";
         lV18TFLoteArquivoAnexo_Descricao = "";
         lV20TFTipoDocumento_Nome = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A646TipoDocumento_Nome = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A840LoteArquivoAnexo_TipoArq = "";
         A837LoteArquivoAnexo_Descricao = "";
         P00OK2_A645TipoDocumento_Codigo = new int[1] ;
         P00OK2_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OK2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OK2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P00OK2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         P00OK2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         P00OK2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         P00OK2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         P00OK2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OK2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P00OK2_A646TipoDocumento_Nome = new String[] {""} ;
         AV26Option = "";
         P00OK3_A645TipoDocumento_Codigo = new int[1] ;
         P00OK3_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OK3_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         P00OK3_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         P00OK3_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         P00OK3_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         P00OK3_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OK3_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P00OK3_A646TipoDocumento_Nome = new String[] {""} ;
         P00OK3_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OK3_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P00OK4_A645TipoDocumento_Codigo = new int[1] ;
         P00OK4_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OK4_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         P00OK4_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         P00OK4_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         P00OK4_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         P00OK4_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OK4_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P00OK4_A646TipoDocumento_Nome = new String[] {""} ;
         P00OK4_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OK4_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         P00OK5_A645TipoDocumento_Codigo = new int[1] ;
         P00OK5_n645TipoDocumento_Codigo = new bool[] {false} ;
         P00OK5_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         P00OK5_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         P00OK5_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         P00OK5_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         P00OK5_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P00OK5_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         P00OK5_A646TipoDocumento_Nome = new String[] {""} ;
         P00OK5_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         P00OK5_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptlotearquivoanexofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OK2_A645TipoDocumento_Codigo, P00OK2_n645TipoDocumento_Codigo, P00OK2_A839LoteArquivoAnexo_NomeArq, P00OK2_n839LoteArquivoAnexo_NomeArq, P00OK2_A837LoteArquivoAnexo_Descricao, P00OK2_n837LoteArquivoAnexo_Descricao, P00OK2_A840LoteArquivoAnexo_TipoArq, P00OK2_n840LoteArquivoAnexo_TipoArq, P00OK2_A836LoteArquivoAnexo_Data, P00OK2_A841LoteArquivoAnexo_LoteCod,
               P00OK2_A646TipoDocumento_Nome
               }
               , new Object[] {
               P00OK3_A645TipoDocumento_Codigo, P00OK3_n645TipoDocumento_Codigo, P00OK3_A840LoteArquivoAnexo_TipoArq, P00OK3_n840LoteArquivoAnexo_TipoArq, P00OK3_A837LoteArquivoAnexo_Descricao, P00OK3_n837LoteArquivoAnexo_Descricao, P00OK3_A836LoteArquivoAnexo_Data, P00OK3_A841LoteArquivoAnexo_LoteCod, P00OK3_A646TipoDocumento_Nome, P00OK3_A839LoteArquivoAnexo_NomeArq,
               P00OK3_n839LoteArquivoAnexo_NomeArq
               }
               , new Object[] {
               P00OK4_A645TipoDocumento_Codigo, P00OK4_n645TipoDocumento_Codigo, P00OK4_A837LoteArquivoAnexo_Descricao, P00OK4_n837LoteArquivoAnexo_Descricao, P00OK4_A840LoteArquivoAnexo_TipoArq, P00OK4_n840LoteArquivoAnexo_TipoArq, P00OK4_A836LoteArquivoAnexo_Data, P00OK4_A841LoteArquivoAnexo_LoteCod, P00OK4_A646TipoDocumento_Nome, P00OK4_A839LoteArquivoAnexo_NomeArq,
               P00OK4_n839LoteArquivoAnexo_NomeArq
               }
               , new Object[] {
               P00OK5_A645TipoDocumento_Codigo, P00OK5_n645TipoDocumento_Codigo, P00OK5_A837LoteArquivoAnexo_Descricao, P00OK5_n837LoteArquivoAnexo_Descricao, P00OK5_A840LoteArquivoAnexo_TipoArq, P00OK5_n840LoteArquivoAnexo_TipoArq, P00OK5_A836LoteArquivoAnexo_Data, P00OK5_A841LoteArquivoAnexo_LoteCod, P00OK5_A646TipoDocumento_Nome, P00OK5_A839LoteArquivoAnexo_NomeArq,
               P00OK5_n839LoteArquivoAnexo_NomeArq
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private int AV56GXV1 ;
      private int AV10TFLoteArquivoAnexo_LoteCod ;
      private int AV11TFLoteArquivoAnexo_LoteCod_To ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int A645TipoDocumento_Codigo ;
      private int AV25InsertIndex ;
      private long AV34count ;
      private String AV14TFLoteArquivoAnexo_NomeArq ;
      private String AV15TFLoteArquivoAnexo_NomeArq_Sel ;
      private String AV16TFLoteArquivoAnexo_TipoArq ;
      private String AV17TFLoteArquivoAnexo_TipoArq_Sel ;
      private String AV20TFTipoDocumento_Nome ;
      private String AV21TFTipoDocumento_Nome_Sel ;
      private String AV42LoteArquivoAnexo_NomeArq1 ;
      private String AV43TipoDocumento_Nome1 ;
      private String AV47LoteArquivoAnexo_NomeArq2 ;
      private String AV48TipoDocumento_Nome2 ;
      private String AV52LoteArquivoAnexo_NomeArq3 ;
      private String AV53TipoDocumento_Nome3 ;
      private String scmdbuf ;
      private String lV42LoteArquivoAnexo_NomeArq1 ;
      private String lV43TipoDocumento_Nome1 ;
      private String lV47LoteArquivoAnexo_NomeArq2 ;
      private String lV48TipoDocumento_Nome2 ;
      private String lV52LoteArquivoAnexo_NomeArq3 ;
      private String lV53TipoDocumento_Nome3 ;
      private String lV14TFLoteArquivoAnexo_NomeArq ;
      private String lV16TFLoteArquivoAnexo_TipoArq ;
      private String lV20TFTipoDocumento_Nome ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A646TipoDocumento_Nome ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private DateTime AV12TFLoteArquivoAnexo_Data ;
      private DateTime AV13TFLoteArquivoAnexo_Data_To ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool BRKOK2 ;
      private bool n645TipoDocumento_Codigo ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool BRKOK4 ;
      private bool BRKOK6 ;
      private bool BRKOK8 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV18TFLoteArquivoAnexo_Descricao ;
      private String AV19TFLoteArquivoAnexo_Descricao_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String lV18TFLoteArquivoAnexo_Descricao ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OK2_A645TipoDocumento_Codigo ;
      private bool[] P00OK2_n645TipoDocumento_Codigo ;
      private String[] P00OK2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OK2_n839LoteArquivoAnexo_NomeArq ;
      private String[] P00OK2_A837LoteArquivoAnexo_Descricao ;
      private bool[] P00OK2_n837LoteArquivoAnexo_Descricao ;
      private String[] P00OK2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] P00OK2_n840LoteArquivoAnexo_TipoArq ;
      private DateTime[] P00OK2_A836LoteArquivoAnexo_Data ;
      private int[] P00OK2_A841LoteArquivoAnexo_LoteCod ;
      private String[] P00OK2_A646TipoDocumento_Nome ;
      private int[] P00OK3_A645TipoDocumento_Codigo ;
      private bool[] P00OK3_n645TipoDocumento_Codigo ;
      private String[] P00OK3_A840LoteArquivoAnexo_TipoArq ;
      private bool[] P00OK3_n840LoteArquivoAnexo_TipoArq ;
      private String[] P00OK3_A837LoteArquivoAnexo_Descricao ;
      private bool[] P00OK3_n837LoteArquivoAnexo_Descricao ;
      private DateTime[] P00OK3_A836LoteArquivoAnexo_Data ;
      private int[] P00OK3_A841LoteArquivoAnexo_LoteCod ;
      private String[] P00OK3_A646TipoDocumento_Nome ;
      private String[] P00OK3_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OK3_n839LoteArquivoAnexo_NomeArq ;
      private int[] P00OK4_A645TipoDocumento_Codigo ;
      private bool[] P00OK4_n645TipoDocumento_Codigo ;
      private String[] P00OK4_A837LoteArquivoAnexo_Descricao ;
      private bool[] P00OK4_n837LoteArquivoAnexo_Descricao ;
      private String[] P00OK4_A840LoteArquivoAnexo_TipoArq ;
      private bool[] P00OK4_n840LoteArquivoAnexo_TipoArq ;
      private DateTime[] P00OK4_A836LoteArquivoAnexo_Data ;
      private int[] P00OK4_A841LoteArquivoAnexo_LoteCod ;
      private String[] P00OK4_A646TipoDocumento_Nome ;
      private String[] P00OK4_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OK4_n839LoteArquivoAnexo_NomeArq ;
      private int[] P00OK5_A645TipoDocumento_Codigo ;
      private bool[] P00OK5_n645TipoDocumento_Codigo ;
      private String[] P00OK5_A837LoteArquivoAnexo_Descricao ;
      private bool[] P00OK5_n837LoteArquivoAnexo_Descricao ;
      private String[] P00OK5_A840LoteArquivoAnexo_TipoArq ;
      private bool[] P00OK5_n840LoteArquivoAnexo_TipoArq ;
      private DateTime[] P00OK5_A836LoteArquivoAnexo_Data ;
      private int[] P00OK5_A841LoteArquivoAnexo_LoteCod ;
      private String[] P00OK5_A646TipoDocumento_Nome ;
      private String[] P00OK5_A839LoteArquivoAnexo_NomeArq ;
      private bool[] P00OK5_n839LoteArquivoAnexo_NomeArq ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getpromptlotearquivoanexofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OK2( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42LoteArquivoAnexo_NomeArq1 ,
                                             String AV43TipoDocumento_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47LoteArquivoAnexo_NomeArq2 ,
                                             String AV48TipoDocumento_Nome2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52LoteArquivoAnexo_NomeArq3 ,
                                             String AV53TipoDocumento_Nome3 ,
                                             int AV10TFLoteArquivoAnexo_LoteCod ,
                                             int AV11TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV12TFLoteArquivoAnexo_Data ,
                                             DateTime AV13TFLoteArquivoAnexo_Data_To ,
                                             String AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV14TFLoteArquivoAnexo_NomeArq ,
                                             String AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV16TFLoteArquivoAnexo_TipoArq ,
                                             String AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV18TFLoteArquivoAnexo_Descricao ,
                                             String AV21TFTipoDocumento_Nome_Sel ,
                                             String AV20TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_Descricao], T1.[LoteArquivoAnexo_TipoArq], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_LoteCod], T2.[TipoDocumento_Nome] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV10TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV11TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[LoteArquivoAnexo_NomeArq]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OK3( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42LoteArquivoAnexo_NomeArq1 ,
                                             String AV43TipoDocumento_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47LoteArquivoAnexo_NomeArq2 ,
                                             String AV48TipoDocumento_Nome2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52LoteArquivoAnexo_NomeArq3 ,
                                             String AV53TipoDocumento_Nome3 ,
                                             int AV10TFLoteArquivoAnexo_LoteCod ,
                                             int AV11TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV12TFLoteArquivoAnexo_Data ,
                                             DateTime AV13TFLoteArquivoAnexo_Data_To ,
                                             String AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV14TFLoteArquivoAnexo_NomeArq ,
                                             String AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV16TFLoteArquivoAnexo_TipoArq ,
                                             String AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV18TFLoteArquivoAnexo_Descricao ,
                                             String AV21TFTipoDocumento_Nome_Sel ,
                                             String AV20TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_TipoArq], T1.[LoteArquivoAnexo_Descricao], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_LoteCod], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_NomeArq] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV10TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV11TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[LoteArquivoAnexo_TipoArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00OK4( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42LoteArquivoAnexo_NomeArq1 ,
                                             String AV43TipoDocumento_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47LoteArquivoAnexo_NomeArq2 ,
                                             String AV48TipoDocumento_Nome2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52LoteArquivoAnexo_NomeArq3 ,
                                             String AV53TipoDocumento_Nome3 ,
                                             int AV10TFLoteArquivoAnexo_LoteCod ,
                                             int AV11TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV12TFLoteArquivoAnexo_Data ,
                                             DateTime AV13TFLoteArquivoAnexo_Data_To ,
                                             String AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV14TFLoteArquivoAnexo_NomeArq ,
                                             String AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV16TFLoteArquivoAnexo_TipoArq ,
                                             String AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV18TFLoteArquivoAnexo_Descricao ,
                                             String AV21TFTipoDocumento_Nome_Sel ,
                                             String AV20TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Descricao], T1.[LoteArquivoAnexo_TipoArq], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_LoteCod], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_NomeArq] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV10TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV11TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[LoteArquivoAnexo_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00OK5( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42LoteArquivoAnexo_NomeArq1 ,
                                             String AV43TipoDocumento_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47LoteArquivoAnexo_NomeArq2 ,
                                             String AV48TipoDocumento_Nome2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52LoteArquivoAnexo_NomeArq3 ,
                                             String AV53TipoDocumento_Nome3 ,
                                             int AV10TFLoteArquivoAnexo_LoteCod ,
                                             int AV11TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV12TFLoteArquivoAnexo_Data ,
                                             DateTime AV13TFLoteArquivoAnexo_Data_To ,
                                             String AV15TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV14TFLoteArquivoAnexo_NomeArq ,
                                             String AV17TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV16TFLoteArquivoAnexo_TipoArq ,
                                             String AV19TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV18TFLoteArquivoAnexo_Descricao ,
                                             String AV21TFTipoDocumento_Nome_Sel ,
                                             String AV20TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [24] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Descricao], T1.[LoteArquivoAnexo_TipoArq], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_LoteCod], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_NomeArq] FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV42LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV43TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV47LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV48TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV52LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV53TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV10TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV10TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! (0==AV11TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV11TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV12TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV13TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV14TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV15TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV16TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV17TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV18TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV19TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV20TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV21TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[TipoDocumento_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OK2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 1 :
                     return conditional_P00OK3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 2 :
                     return conditional_P00OK4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
               case 3 :
                     return conditional_P00OK5(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OK2 ;
          prmP00OK2 = new Object[] {
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV20TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00OK3 ;
          prmP00OK3 = new Object[] {
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV20TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00OK4 ;
          prmP00OK4 = new Object[] {
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV20TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00OK5 ;
          prmP00OK5 = new Object[] {
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV20TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV21TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OK2,100,0,true,false )
             ,new CursorDef("P00OK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OK3,100,0,true,false )
             ,new CursorDef("P00OK4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OK4,100,0,true,false )
             ,new CursorDef("P00OK5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OK5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptlotearquivoanexofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptlotearquivoanexofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptlotearquivoanexofilterdata") )
          {
             return  ;
          }
          getpromptlotearquivoanexofilterdata worker = new getpromptlotearquivoanexofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
