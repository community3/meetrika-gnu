/*
               File: Modulo_BC
        Description: Modulo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:19:36.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulo_bc : GXHttpHandler, IGxSilentTrn
   {
      public modulo_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public modulo_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0R28( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0R28( ) ;
         standaloneModal( ) ;
         AddRow0R28( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110R2 */
            E110R2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z146Modulo_Codigo = A146Modulo_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0R0( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0R28( ) ;
            }
            else
            {
               CheckExtendedTable0R28( ) ;
               if ( AnyError == 0 )
               {
                  ZM0R28( 5) ;
               }
               CloseExtendedTableCursors0R28( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120R2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Sistema_Codigo") == 0 )
               {
                  AV11Insert_Sistema_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
            }
         }
         AV14Tabela_ModuloCod = A146Modulo_Codigo;
      }

      protected void E110R2( )
      {
         /* After Trn Routine */
      }

      protected void E130R2( )
      {
         /* 'DoCopiarColarModulos' Routine */
         context.wjLoc = formatLink("wp_copiarcolarmodulos.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void ZM0R28( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z143Modulo_Nome = A143Modulo_Nome;
            Z145Modulo_Sigla = A145Modulo_Sigla;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z416Sistema_Nome = A416Sistema_Nome;
         }
         if ( GX_JID == -4 )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z143Modulo_Nome = A143Modulo_Nome;
            Z145Modulo_Sigla = A145Modulo_Sigla;
            Z144Modulo_Descricao = A144Modulo_Descricao;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z416Sistema_Nome = A416Sistema_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         AV16Pgmname = "Modulo_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load0R28( )
      {
         /* Using cursor BC000R5 */
         pr_default.execute(3, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound28 = 1;
            A143Modulo_Nome = BC000R5_A143Modulo_Nome[0];
            A145Modulo_Sigla = BC000R5_A145Modulo_Sigla[0];
            A144Modulo_Descricao = BC000R5_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC000R5_n144Modulo_Descricao[0];
            A416Sistema_Nome = BC000R5_A416Sistema_Nome[0];
            A127Sistema_Codigo = BC000R5_A127Sistema_Codigo[0];
            ZM0R28( -4) ;
         }
         pr_default.close(3);
         OnLoadActions0R28( ) ;
      }

      protected void OnLoadActions0R28( )
      {
      }

      protected void CheckExtendedTable0R28( )
      {
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A143Modulo_Nome)) )
         {
            GX_msglist.addItem("M�dulo � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000R4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
         }
         A416Sistema_Nome = BC000R4_A416Sistema_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0R28( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0R28( )
      {
         /* Using cursor BC000R6 */
         pr_default.execute(4, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound28 = 1;
         }
         else
         {
            RcdFound28 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000R3 */
         pr_default.execute(1, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0R28( 4) ;
            RcdFound28 = 1;
            A146Modulo_Codigo = BC000R3_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC000R3_n146Modulo_Codigo[0];
            A143Modulo_Nome = BC000R3_A143Modulo_Nome[0];
            A145Modulo_Sigla = BC000R3_A145Modulo_Sigla[0];
            A144Modulo_Descricao = BC000R3_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC000R3_n144Modulo_Descricao[0];
            A127Sistema_Codigo = BC000R3_A127Sistema_Codigo[0];
            Z146Modulo_Codigo = A146Modulo_Codigo;
            sMode28 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0R28( ) ;
            if ( AnyError == 1 )
            {
               RcdFound28 = 0;
               InitializeNonKey0R28( ) ;
            }
            Gx_mode = sMode28;
         }
         else
         {
            RcdFound28 = 0;
            InitializeNonKey0R28( ) ;
            sMode28 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode28;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0R28( ) ;
         if ( RcdFound28 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0R0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0R28( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000R2 */
            pr_default.execute(0, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Modulo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z143Modulo_Nome, BC000R2_A143Modulo_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z145Modulo_Sigla, BC000R2_A145Modulo_Sigla[0]) != 0 ) || ( Z127Sistema_Codigo != BC000R2_A127Sistema_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Modulo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0R28( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0R28( 0) ;
            CheckOptimisticConcurrency0R28( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0R28( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0R28( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000R7 */
                     pr_default.execute(5, new Object[] {A143Modulo_Nome, A145Modulo_Sigla, n144Modulo_Descricao, A144Modulo_Descricao, A127Sistema_Codigo});
                     A146Modulo_Codigo = BC000R7_A146Modulo_Codigo[0];
                     n146Modulo_Codigo = BC000R7_n146Modulo_Codigo[0];
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0R28( ) ;
            }
            EndLevel0R28( ) ;
         }
         CloseExtendedTableCursors0R28( ) ;
      }

      protected void Update0R28( )
      {
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0R28( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0R28( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0R28( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000R8 */
                     pr_default.execute(6, new Object[] {A143Modulo_Nome, A145Modulo_Sigla, n144Modulo_Descricao, A144Modulo_Descricao, A127Sistema_Codigo, n146Modulo_Codigo, A146Modulo_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Modulo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0R28( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0R28( ) ;
         }
         CloseExtendedTableCursors0R28( ) ;
      }

      protected void DeferredUpdate0R28( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0R28( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0R28( ) ;
            AfterConfirm0R28( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0R28( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000R9 */
                  pr_default.execute(7, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Modulo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode28 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0R28( ) ;
         Gx_mode = sMode28;
      }

      protected void OnDeleteControls0R28( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000R10 */
            pr_default.execute(8, new Object[] {A127Sistema_Codigo});
            A416Sistema_Nome = BC000R10_A416Sistema_Nome[0];
            pr_default.close(8);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000R11 */
            pr_default.execute(9, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC000R12 */
            pr_default.execute(10, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC000R13 */
            pr_default.execute(11, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC000R14 */
            pr_default.execute(12, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo Funcoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel0R28( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0R28( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0R28( )
      {
         /* Scan By routine */
         /* Using cursor BC000R15 */
         pr_default.execute(13, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         RcdFound28 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound28 = 1;
            A146Modulo_Codigo = BC000R15_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC000R15_n146Modulo_Codigo[0];
            A143Modulo_Nome = BC000R15_A143Modulo_Nome[0];
            A145Modulo_Sigla = BC000R15_A145Modulo_Sigla[0];
            A144Modulo_Descricao = BC000R15_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC000R15_n144Modulo_Descricao[0];
            A416Sistema_Nome = BC000R15_A416Sistema_Nome[0];
            A127Sistema_Codigo = BC000R15_A127Sistema_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0R28( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound28 = 0;
         ScanKeyLoad0R28( ) ;
      }

      protected void ScanKeyLoad0R28( )
      {
         sMode28 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound28 = 1;
            A146Modulo_Codigo = BC000R15_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC000R15_n146Modulo_Codigo[0];
            A143Modulo_Nome = BC000R15_A143Modulo_Nome[0];
            A145Modulo_Sigla = BC000R15_A145Modulo_Sigla[0];
            A144Modulo_Descricao = BC000R15_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC000R15_n144Modulo_Descricao[0];
            A416Sistema_Nome = BC000R15_A416Sistema_Nome[0];
            A127Sistema_Codigo = BC000R15_A127Sistema_Codigo[0];
         }
         Gx_mode = sMode28;
      }

      protected void ScanKeyEnd0R28( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm0R28( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0R28( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0R28( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0R28( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0R28( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0R28( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0R28( )
      {
      }

      protected void AddRow0R28( )
      {
         VarsToRow28( bcModulo) ;
      }

      protected void ReadRow0R28( )
      {
         RowToVars28( bcModulo, 1) ;
      }

      protected void InitializeNonKey0R28( )
      {
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         A144Modulo_Descricao = "";
         n144Modulo_Descricao = false;
         A127Sistema_Codigo = 0;
         A416Sistema_Nome = "";
         Z143Modulo_Nome = "";
         Z145Modulo_Sigla = "";
         Z127Sistema_Codigo = 0;
      }

      protected void InitAll0R28( )
      {
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         InitializeNonKey0R28( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow28( SdtModulo obj28 )
      {
         obj28.gxTpr_Mode = Gx_mode;
         obj28.gxTpr_Modulo_nome = A143Modulo_Nome;
         obj28.gxTpr_Modulo_sigla = A145Modulo_Sigla;
         obj28.gxTpr_Modulo_descricao = A144Modulo_Descricao;
         obj28.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj28.gxTpr_Sistema_nome = A416Sistema_Nome;
         obj28.gxTpr_Modulo_codigo = A146Modulo_Codigo;
         obj28.gxTpr_Modulo_codigo_Z = Z146Modulo_Codigo;
         obj28.gxTpr_Modulo_nome_Z = Z143Modulo_Nome;
         obj28.gxTpr_Modulo_sigla_Z = Z145Modulo_Sigla;
         obj28.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj28.gxTpr_Sistema_nome_Z = Z416Sistema_Nome;
         obj28.gxTpr_Modulo_codigo_N = (short)(Convert.ToInt16(n146Modulo_Codigo));
         obj28.gxTpr_Modulo_descricao_N = (short)(Convert.ToInt16(n144Modulo_Descricao));
         obj28.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow28( SdtModulo obj28 )
      {
         obj28.gxTpr_Modulo_codigo = A146Modulo_Codigo;
         return  ;
      }

      public void RowToVars28( SdtModulo obj28 ,
                               int forceLoad )
      {
         Gx_mode = obj28.gxTpr_Mode;
         A143Modulo_Nome = obj28.gxTpr_Modulo_nome;
         A145Modulo_Sigla = obj28.gxTpr_Modulo_sigla;
         A144Modulo_Descricao = obj28.gxTpr_Modulo_descricao;
         n144Modulo_Descricao = false;
         A127Sistema_Codigo = obj28.gxTpr_Sistema_codigo;
         A416Sistema_Nome = obj28.gxTpr_Sistema_nome;
         A146Modulo_Codigo = obj28.gxTpr_Modulo_codigo;
         n146Modulo_Codigo = false;
         Z146Modulo_Codigo = obj28.gxTpr_Modulo_codigo_Z;
         Z143Modulo_Nome = obj28.gxTpr_Modulo_nome_Z;
         Z145Modulo_Sigla = obj28.gxTpr_Modulo_sigla_Z;
         Z127Sistema_Codigo = obj28.gxTpr_Sistema_codigo_Z;
         Z416Sistema_Nome = obj28.gxTpr_Sistema_nome_Z;
         n146Modulo_Codigo = (bool)(Convert.ToBoolean(obj28.gxTpr_Modulo_codigo_N));
         n144Modulo_Descricao = (bool)(Convert.ToBoolean(obj28.gxTpr_Modulo_descricao_N));
         Gx_mode = obj28.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A146Modulo_Codigo = (int)getParm(obj,0);
         n146Modulo_Codigo = false;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0R28( ) ;
         ScanKeyStart0R28( ) ;
         if ( RcdFound28 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z146Modulo_Codigo = A146Modulo_Codigo;
         }
         ZM0R28( -4) ;
         OnLoadActions0R28( ) ;
         AddRow0R28( ) ;
         ScanKeyEnd0R28( ) ;
         if ( RcdFound28 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars28( bcModulo, 0) ;
         ScanKeyStart0R28( ) ;
         if ( RcdFound28 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z146Modulo_Codigo = A146Modulo_Codigo;
         }
         ZM0R28( -4) ;
         OnLoadActions0R28( ) ;
         AddRow0R28( ) ;
         ScanKeyEnd0R28( ) ;
         if ( RcdFound28 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars28( bcModulo, 0) ;
         nKeyPressed = 1;
         GetKey0R28( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0R28( ) ;
         }
         else
         {
            if ( RcdFound28 == 1 )
            {
               if ( A146Modulo_Codigo != Z146Modulo_Codigo )
               {
                  A146Modulo_Codigo = Z146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0R28( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A146Modulo_Codigo != Z146Modulo_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0R28( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0R28( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow28( bcModulo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars28( bcModulo, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0R28( ) ;
         if ( RcdFound28 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A146Modulo_Codigo != Z146Modulo_Codigo )
            {
               A146Modulo_Codigo = Z146Modulo_Codigo;
               n146Modulo_Codigo = false;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A146Modulo_Codigo != Z146Modulo_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         context.RollbackDataStores( "Modulo_BC");
         VarsToRow28( bcModulo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcModulo.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcModulo.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcModulo )
         {
            bcModulo = (SdtModulo)(sdt);
            if ( StringUtil.StrCmp(bcModulo.gxTpr_Mode, "") == 0 )
            {
               bcModulo.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow28( bcModulo) ;
            }
            else
            {
               RowToVars28( bcModulo, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcModulo.gxTpr_Mode, "") == 0 )
            {
               bcModulo.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars28( bcModulo, 1) ;
         return  ;
      }

      public SdtModulo Modulo_BC
      {
         get {
            return bcModulo ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV16Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z143Modulo_Nome = "";
         A143Modulo_Nome = "";
         Z145Modulo_Sigla = "";
         A145Modulo_Sigla = "";
         Z416Sistema_Nome = "";
         A416Sistema_Nome = "";
         Z144Modulo_Descricao = "";
         A144Modulo_Descricao = "";
         BC000R5_A146Modulo_Codigo = new int[1] ;
         BC000R5_n146Modulo_Codigo = new bool[] {false} ;
         BC000R5_A143Modulo_Nome = new String[] {""} ;
         BC000R5_A145Modulo_Sigla = new String[] {""} ;
         BC000R5_A144Modulo_Descricao = new String[] {""} ;
         BC000R5_n144Modulo_Descricao = new bool[] {false} ;
         BC000R5_A416Sistema_Nome = new String[] {""} ;
         BC000R5_A127Sistema_Codigo = new int[1] ;
         BC000R4_A416Sistema_Nome = new String[] {""} ;
         BC000R6_A146Modulo_Codigo = new int[1] ;
         BC000R6_n146Modulo_Codigo = new bool[] {false} ;
         BC000R3_A146Modulo_Codigo = new int[1] ;
         BC000R3_n146Modulo_Codigo = new bool[] {false} ;
         BC000R3_A143Modulo_Nome = new String[] {""} ;
         BC000R3_A145Modulo_Sigla = new String[] {""} ;
         BC000R3_A144Modulo_Descricao = new String[] {""} ;
         BC000R3_n144Modulo_Descricao = new bool[] {false} ;
         BC000R3_A127Sistema_Codigo = new int[1] ;
         sMode28 = "";
         BC000R2_A146Modulo_Codigo = new int[1] ;
         BC000R2_n146Modulo_Codigo = new bool[] {false} ;
         BC000R2_A143Modulo_Nome = new String[] {""} ;
         BC000R2_A145Modulo_Sigla = new String[] {""} ;
         BC000R2_A144Modulo_Descricao = new String[] {""} ;
         BC000R2_n144Modulo_Descricao = new bool[] {false} ;
         BC000R2_A127Sistema_Codigo = new int[1] ;
         BC000R7_A146Modulo_Codigo = new int[1] ;
         BC000R7_n146Modulo_Codigo = new bool[] {false} ;
         BC000R10_A416Sistema_Nome = new String[] {""} ;
         BC000R11_A165FuncaoAPF_Codigo = new int[1] ;
         BC000R12_A172Tabela_Codigo = new int[1] ;
         BC000R13_A456ContagemResultado_Codigo = new int[1] ;
         BC000R14_A146Modulo_Codigo = new int[1] ;
         BC000R14_n146Modulo_Codigo = new bool[] {false} ;
         BC000R14_A161FuncaoUsuario_Codigo = new int[1] ;
         BC000R15_A146Modulo_Codigo = new int[1] ;
         BC000R15_n146Modulo_Codigo = new bool[] {false} ;
         BC000R15_A143Modulo_Nome = new String[] {""} ;
         BC000R15_A145Modulo_Sigla = new String[] {""} ;
         BC000R15_A144Modulo_Descricao = new String[] {""} ;
         BC000R15_n144Modulo_Descricao = new bool[] {false} ;
         BC000R15_A416Sistema_Nome = new String[] {""} ;
         BC000R15_A127Sistema_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulo_bc__default(),
            new Object[][] {
                new Object[] {
               BC000R2_A146Modulo_Codigo, BC000R2_A143Modulo_Nome, BC000R2_A145Modulo_Sigla, BC000R2_A144Modulo_Descricao, BC000R2_n144Modulo_Descricao, BC000R2_A127Sistema_Codigo
               }
               , new Object[] {
               BC000R3_A146Modulo_Codigo, BC000R3_A143Modulo_Nome, BC000R3_A145Modulo_Sigla, BC000R3_A144Modulo_Descricao, BC000R3_n144Modulo_Descricao, BC000R3_A127Sistema_Codigo
               }
               , new Object[] {
               BC000R4_A416Sistema_Nome
               }
               , new Object[] {
               BC000R5_A146Modulo_Codigo, BC000R5_A143Modulo_Nome, BC000R5_A145Modulo_Sigla, BC000R5_A144Modulo_Descricao, BC000R5_n144Modulo_Descricao, BC000R5_A416Sistema_Nome, BC000R5_A127Sistema_Codigo
               }
               , new Object[] {
               BC000R6_A146Modulo_Codigo
               }
               , new Object[] {
               BC000R7_A146Modulo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000R10_A416Sistema_Nome
               }
               , new Object[] {
               BC000R11_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               BC000R12_A172Tabela_Codigo
               }
               , new Object[] {
               BC000R13_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000R14_A146Modulo_Codigo, BC000R14_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC000R15_A146Modulo_Codigo, BC000R15_A143Modulo_Nome, BC000R15_A145Modulo_Sigla, BC000R15_A144Modulo_Descricao, BC000R15_n144Modulo_Descricao, BC000R15_A416Sistema_Nome, BC000R15_A127Sistema_Codigo
               }
            }
         );
         AV16Pgmname = "Modulo_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120R2 */
         E120R2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound28 ;
      private int trnEnded ;
      private int Z146Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int AV17GXV1 ;
      private int AV11Insert_Sistema_Codigo ;
      private int AV14Tabela_ModuloCod ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV16Pgmname ;
      private String Z143Modulo_Nome ;
      private String A143Modulo_Nome ;
      private String Z145Modulo_Sigla ;
      private String A145Modulo_Sigla ;
      private String sMode28 ;
      private bool n146Modulo_Codigo ;
      private bool n144Modulo_Descricao ;
      private String Z144Modulo_Descricao ;
      private String A144Modulo_Descricao ;
      private String Z416Sistema_Nome ;
      private String A416Sistema_Nome ;
      private IGxSession AV10WebSession ;
      private SdtModulo bcModulo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC000R5_A146Modulo_Codigo ;
      private bool[] BC000R5_n146Modulo_Codigo ;
      private String[] BC000R5_A143Modulo_Nome ;
      private String[] BC000R5_A145Modulo_Sigla ;
      private String[] BC000R5_A144Modulo_Descricao ;
      private bool[] BC000R5_n144Modulo_Descricao ;
      private String[] BC000R5_A416Sistema_Nome ;
      private int[] BC000R5_A127Sistema_Codigo ;
      private String[] BC000R4_A416Sistema_Nome ;
      private int[] BC000R6_A146Modulo_Codigo ;
      private bool[] BC000R6_n146Modulo_Codigo ;
      private int[] BC000R3_A146Modulo_Codigo ;
      private bool[] BC000R3_n146Modulo_Codigo ;
      private String[] BC000R3_A143Modulo_Nome ;
      private String[] BC000R3_A145Modulo_Sigla ;
      private String[] BC000R3_A144Modulo_Descricao ;
      private bool[] BC000R3_n144Modulo_Descricao ;
      private int[] BC000R3_A127Sistema_Codigo ;
      private int[] BC000R2_A146Modulo_Codigo ;
      private bool[] BC000R2_n146Modulo_Codigo ;
      private String[] BC000R2_A143Modulo_Nome ;
      private String[] BC000R2_A145Modulo_Sigla ;
      private String[] BC000R2_A144Modulo_Descricao ;
      private bool[] BC000R2_n144Modulo_Descricao ;
      private int[] BC000R2_A127Sistema_Codigo ;
      private int[] BC000R7_A146Modulo_Codigo ;
      private bool[] BC000R7_n146Modulo_Codigo ;
      private String[] BC000R10_A416Sistema_Nome ;
      private int[] BC000R11_A165FuncaoAPF_Codigo ;
      private int[] BC000R12_A172Tabela_Codigo ;
      private int[] BC000R13_A456ContagemResultado_Codigo ;
      private int[] BC000R14_A146Modulo_Codigo ;
      private bool[] BC000R14_n146Modulo_Codigo ;
      private int[] BC000R14_A161FuncaoUsuario_Codigo ;
      private int[] BC000R15_A146Modulo_Codigo ;
      private bool[] BC000R15_n146Modulo_Codigo ;
      private String[] BC000R15_A143Modulo_Nome ;
      private String[] BC000R15_A145Modulo_Sigla ;
      private String[] BC000R15_A144Modulo_Descricao ;
      private bool[] BC000R15_n144Modulo_Descricao ;
      private String[] BC000R15_A416Sistema_Nome ;
      private int[] BC000R15_A127Sistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class modulo_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000R5 ;
          prmBC000R5 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R4 ;
          prmBC000R4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R6 ;
          prmBC000R6 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R3 ;
          prmBC000R3 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R2 ;
          prmBC000R2 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R7 ;
          prmBC000R7 = new Object[] {
          new Object[] {"@Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Modulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Modulo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R8 ;
          prmBC000R8 = new Object[] {
          new Object[] {"@Modulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Modulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Modulo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R9 ;
          prmBC000R9 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R10 ;
          prmBC000R10 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R11 ;
          prmBC000R11 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R12 ;
          prmBC000R12 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R13 ;
          prmBC000R13 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R14 ;
          prmBC000R14 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000R15 ;
          prmBC000R15 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000R2", "SELECT [Modulo_Codigo], [Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo] FROM [Modulo] WITH (UPDLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R2,1,0,true,false )
             ,new CursorDef("BC000R3", "SELECT [Modulo_Codigo], [Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R3,1,0,true,false )
             ,new CursorDef("BC000R4", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R4,1,0,true,false )
             ,new CursorDef("BC000R5", "SELECT TM1.[Modulo_Codigo], TM1.[Modulo_Nome], TM1.[Modulo_Sigla], TM1.[Modulo_Descricao], T2.[Sistema_Nome], TM1.[Sistema_Codigo] FROM ([Modulo] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Sistema_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo ORDER BY TM1.[Modulo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R5,100,0,true,false )
             ,new CursorDef("BC000R6", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R6,1,0,true,false )
             ,new CursorDef("BC000R7", "INSERT INTO [Modulo]([Modulo_Nome], [Modulo_Sigla], [Modulo_Descricao], [Sistema_Codigo]) VALUES(@Modulo_Nome, @Modulo_Sigla, @Modulo_Descricao, @Sistema_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000R7)
             ,new CursorDef("BC000R8", "UPDATE [Modulo] SET [Modulo_Nome]=@Modulo_Nome, [Modulo_Sigla]=@Modulo_Sigla, [Modulo_Descricao]=@Modulo_Descricao, [Sistema_Codigo]=@Sistema_Codigo  WHERE [Modulo_Codigo] = @Modulo_Codigo", GxErrorMask.GX_NOMASK,prmBC000R8)
             ,new CursorDef("BC000R9", "DELETE FROM [Modulo]  WHERE [Modulo_Codigo] = @Modulo_Codigo", GxErrorMask.GX_NOMASK,prmBC000R9)
             ,new CursorDef("BC000R10", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R10,1,0,true,false )
             ,new CursorDef("BC000R11", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_ModuloCod] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R11,1,0,true,true )
             ,new CursorDef("BC000R12", "SELECT TOP 1 [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_ModuloCod] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R12,1,0,true,true )
             ,new CursorDef("BC000R13", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R13,1,0,true,true )
             ,new CursorDef("BC000R14", "SELECT TOP 1 [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R14,1,0,true,true )
             ,new CursorDef("BC000R15", "SELECT TM1.[Modulo_Codigo], TM1.[Modulo_Nome], TM1.[Modulo_Sigla], TM1.[Modulo_Descricao], T2.[Sistema_Nome], TM1.[Sistema_Codigo] FROM ([Modulo] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Sistema_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo ORDER BY TM1.[Modulo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000R15,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
