/*
               File: PRC_PodeDesativarArea
        Description: PRC_Pode Desativar Area
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:43.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_podedesativararea : GXProcedure
   {
      public prc_podedesativararea( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_podedesativararea( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out String aP1_Causa ,
                           out bool aP2_PodeDesativar )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Causa = "" ;
         this.AV8PodeDesativar = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Causa=this.AV9Causa;
         aP2_PodeDesativar=this.AV8PodeDesativar;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo ,
                              out String aP1_Causa )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Causa = "" ;
         this.AV8PodeDesativar = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Causa=this.AV9Causa;
         aP2_PodeDesativar=this.AV8PodeDesativar;
         return AV8PodeDesativar ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out String aP1_Causa ,
                                 out bool aP2_PodeDesativar )
      {
         prc_podedesativararea objprc_podedesativararea;
         objprc_podedesativararea = new prc_podedesativararea();
         objprc_podedesativararea.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_podedesativararea.AV9Causa = "" ;
         objprc_podedesativararea.AV8PodeDesativar = false ;
         objprc_podedesativararea.context.SetSubmitInitialConfig(context);
         objprc_podedesativararea.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_podedesativararea);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Causa=this.AV9Causa;
         aP2_PodeDesativar=this.AV8PodeDesativar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_podedesativararea)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Causa = "N�o � possivel desativar: ";
         AV8PodeDesativar = true;
         /* Using cursor P00X14 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00X14_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00X14_n74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = P00X14_A75Contrato_AreaTrabalhoCod[0];
            A83Contrato_DataVigenciaTermino = P00X14_A83Contrato_DataVigenciaTermino[0];
            A92Contrato_Ativo = P00X14_A92Contrato_Ativo[0];
            A843Contrato_DataFimTA = P00X14_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00X14_n843Contrato_DataFimTA[0];
            A843Contrato_DataFimTA = P00X14_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = P00X14_n843Contrato_DataFimTA[0];
            if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
            {
               AV10Count = (short)(AV10Count+1);
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV10Count > 0 )
         {
            if ( AV10Count == 1 )
            {
               AV9Causa = "Existe 1 contrato ativo";
            }
            else
            {
               AV9Causa = "Existem " + StringUtil.Trim( StringUtil.Str( (decimal)(AV10Count), 4, 0)) + " contratos ativos";
            }
            AV8PodeDesativar = false;
            AV10Count = 0;
         }
         AV14GXLvl22 = 0;
         /* Using cursor P00X15 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00X15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00X15_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = P00X15_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00X15_n484ContagemResultado_StatusDmn[0];
            A52Contratada_AreaTrabalhoCod = P00X15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00X15_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = P00X15_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00X15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00X15_n52Contratada_AreaTrabalhoCod[0];
            AV14GXLvl22 = 1;
            AV10Count = (short)(AV10Count+1);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV14GXLvl22 == 0 )
         {
            AV9Causa = "";
         }
         if ( AV10Count > 0 )
         {
            AV8PodeDesativar = false;
            if ( StringUtil.StringSearch( AV9Causa, "contrato", 1) > 0 )
            {
               if ( AV10Count == 1 )
               {
                  AV9Causa = AV9Causa + " e 1 demanda sem loquidar";
               }
               else
               {
                  AV9Causa = AV9Causa + " e " + StringUtil.Trim( StringUtil.Str( (decimal)(AV10Count), 4, 0)) + " demandas sem liquidar";
               }
            }
            else
            {
               if ( AV10Count == 1 )
               {
                  AV9Causa = AV9Causa + "Existe 1 demanda sem liquidar";
               }
               else
               {
                  AV9Causa = AV9Causa + "Existem " + StringUtil.Trim( StringUtil.Str( (decimal)(AV10Count), 4, 0)) + " demandas sem liquidar";
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X14_A74Contrato_Codigo = new int[1] ;
         P00X14_n74Contrato_Codigo = new bool[] {false} ;
         P00X14_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00X14_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00X14_A92Contrato_Ativo = new bool[] {false} ;
         P00X14_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00X14_n843Contrato_DataFimTA = new bool[] {false} ;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         P00X15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00X15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00X15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00X15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00X15_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00X15_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00X15_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_podedesativararea__default(),
            new Object[][] {
                new Object[] {
               P00X14_A74Contrato_Codigo, P00X14_A75Contrato_AreaTrabalhoCod, P00X14_A83Contrato_DataVigenciaTermino, P00X14_A92Contrato_Ativo, P00X14_A843Contrato_DataFimTA, P00X14_n843Contrato_DataFimTA
               }
               , new Object[] {
               P00X15_A490ContagemResultado_ContratadaCod, P00X15_n490ContagemResultado_ContratadaCod, P00X15_A484ContagemResultado_StatusDmn, P00X15_n484ContagemResultado_StatusDmn, P00X15_A52Contratada_AreaTrabalhoCod, P00X15_n52Contratada_AreaTrabalhoCod, P00X15_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Count ;
      private short AV14GXLvl22 ;
      private int A5AreaTrabalho_Codigo ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A456ContagemResultado_Codigo ;
      private String AV9Causa ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private bool AV8PodeDesativar ;
      private bool n74Contrato_Codigo ;
      private bool A92Contrato_Ativo ;
      private bool n843Contrato_DataFimTA ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X14_A74Contrato_Codigo ;
      private bool[] P00X14_n74Contrato_Codigo ;
      private int[] P00X14_A75Contrato_AreaTrabalhoCod ;
      private DateTime[] P00X14_A83Contrato_DataVigenciaTermino ;
      private bool[] P00X14_A92Contrato_Ativo ;
      private DateTime[] P00X14_A843Contrato_DataFimTA ;
      private bool[] P00X14_n843Contrato_DataFimTA ;
      private int[] P00X15_A490ContagemResultado_ContratadaCod ;
      private bool[] P00X15_n490ContagemResultado_ContratadaCod ;
      private String[] P00X15_A484ContagemResultado_StatusDmn ;
      private bool[] P00X15_n484ContagemResultado_StatusDmn ;
      private int[] P00X15_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00X15_n52Contratada_AreaTrabalhoCod ;
      private int[] P00X15_A456ContagemResultado_Codigo ;
      private String aP1_Causa ;
      private bool aP2_PodeDesativar ;
   }

   public class prc_podedesativararea__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X14 ;
          prmP00X14 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00X15 ;
          prmP00X15 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X14", "SELECT T1.[Contrato_Codigo], T1.[Contrato_AreaTrabalhoCod], T1.[Contrato_DataVigenciaTermino], T1.[Contrato_Ativo], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM ([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T3.[ContratoTermoAditivo_DataFim], T3.[Contrato_Codigo], T3.[ContratoTermoAditivo_Codigo], T4.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T3 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T4 ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T3.[ContratoTermoAditivo_Codigo] = T4.[GXC4] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contrato_AreaTrabalhoCod] = @AreaTrabalho_Codigo) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contrato_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X14,100,0,true,false )
             ,new CursorDef("P00X15", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_StatusDmn], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE (T2.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo) AND (T1.[ContagemResultado_StatusDmn] <> 'L') AND (T1.[ContagemResultado_StatusDmn] <> 'X') ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X15,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
