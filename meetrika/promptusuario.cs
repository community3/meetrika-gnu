/*
               File: PromptUsuario
        Description: Select Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:35.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptusuario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutUsuario_Codigo ,
                           ref String aP1_InOutUsuario_PessoaNom )
      {
         this.AV7InOutUsuario_Codigo = aP0_InOutUsuario_Codigo;
         this.AV33InOutUsuario_PessoaNom = aP1_InOutUsuario_PessoaNom;
         executePrivate();
         aP0_InOutUsuario_Codigo=this.AV7InOutUsuario_Codigo;
         aP1_InOutUsuario_PessoaNom=this.AV33InOutUsuario_PessoaNom;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbUsuario_PessoaTip = new GXCombobox();
         chkUsuario_EhContador = new GXCheckbox();
         chkUsuario_EhAuditorFM = new GXCheckbox();
         chkUsuario_EhContratada = new GXCheckbox();
         chkUsuario_EhContratante = new GXCheckbox();
         chkUsuario_EhFinanceiro = new GXCheckbox();
         chkUsuario_EhGestor = new GXCheckbox();
         chkUsuario_EhPreposto = new GXCheckbox();
         chkUsuario_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_90 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_90_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_90_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV30Usuario_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
               AV73Usuario_CargoNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
               AV57Usuario_PessoaDoc1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_PessoaDoc1", AV57Usuario_PessoaDoc1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV31Usuario_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
               AV75Usuario_CargoNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
               AV58Usuario_PessoaDoc2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Usuario_PessoaDoc2", AV58Usuario_PessoaDoc2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV32Usuario_PessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
               AV77Usuario_CargoNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
               AV59Usuario_PessoaDoc3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Usuario_PessoaDoc3", AV59Usuario_PessoaDoc3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               A1647Usuario_Email = GetNextPar( );
               n1647Usuario_Email = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutUsuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV33InOutUsuario_PessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutUsuario_PessoaNom", AV33InOutUsuario_PessoaNom);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA0J2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0J2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0J2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117303528");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptusuario.aspx") + "?" + UrlEncode("" +AV7InOutUsuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV33InOutUsuario_PessoaNom))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM1", StringUtil.RTrim( AV30Usuario_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM1", AV73Usuario_CargoNom1);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC1", AV57Usuario_PessoaDoc1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM2", StringUtil.RTrim( AV31Usuario_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM2", AV75Usuario_CargoNom2);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC2", AV58Usuario_PessoaDoc2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM3", StringUtil.RTrim( AV32Usuario_PessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_CARGONOM3", AV77Usuario_CargoNom3);
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOADOC3", AV59Usuario_PessoaDoc3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_90", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_90), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTUSUARIO_PESSOANOM", StringUtil.RTrim( AV33InOutUsuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm0J2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Select Usuario" ;
      }

      protected void WB0J0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_0J2( true) ;
         }
         else
         {
            wb_table1_2_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_90_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(117, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_90_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
         }
         wbLoad = true;
      }

      protected void START0J2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Select Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0J0( ) ;
      }

      protected void WS0J2( )
      {
         START0J2( ) ;
         EVT0J2( ) ;
      }

      protected void EVT0J2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110J2 */
                           E110J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120J2 */
                           E120J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130J2 */
                           E130J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140J2 */
                           E140J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E150J2 */
                           E150J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E160J2 */
                           E160J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E170J2 */
                           E170J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E180J2 */
                           E180J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_90_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
                           SubsflControlProps_902( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV80Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                           A1073Usuario_CargoCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_CargoCod_Internalname), ",", "."));
                           n1073Usuario_CargoCod = false;
                           A1074Usuario_CargoNom = StringUtil.Upper( cgiGet( edtUsuario_CargoNom_Internalname));
                           n1074Usuario_CargoNom = false;
                           A1075Usuario_CargoUOCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_CargoUOCod_Internalname), ",", "."));
                           n1075Usuario_CargoUOCod = false;
                           A1076Usuario_CargoUONom = StringUtil.Upper( cgiGet( edtUsuario_CargoUONom_Internalname));
                           n1076Usuario_CargoUONom = false;
                           A1083Usuario_Entidade = StringUtil.Upper( cgiGet( edtUsuario_Entidade_Internalname));
                           A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
                           A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
                           n58Usuario_PessoaNom = false;
                           cmbUsuario_PessoaTip.Name = cmbUsuario_PessoaTip_Internalname;
                           cmbUsuario_PessoaTip.CurrentValue = cgiGet( cmbUsuario_PessoaTip_Internalname);
                           A59Usuario_PessoaTip = cgiGet( cmbUsuario_PessoaTip_Internalname);
                           n59Usuario_PessoaTip = false;
                           A325Usuario_PessoaDoc = cgiGet( edtUsuario_PessoaDoc_Internalname);
                           n325Usuario_PessoaDoc = false;
                           A341Usuario_UserGamGuid = cgiGet( edtUsuario_UserGamGuid_Internalname);
                           A289Usuario_EhContador = StringUtil.StrToBool( cgiGet( chkUsuario_EhContador_Internalname));
                           A290Usuario_EhAuditorFM = StringUtil.StrToBool( cgiGet( chkUsuario_EhAuditorFM_Internalname));
                           A291Usuario_EhContratada = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratada_Internalname));
                           A292Usuario_EhContratante = StringUtil.StrToBool( cgiGet( chkUsuario_EhContratante_Internalname));
                           A293Usuario_EhFinanceiro = StringUtil.StrToBool( cgiGet( chkUsuario_EhFinanceiro_Internalname));
                           A538Usuario_EhGestor = StringUtil.StrToBool( cgiGet( chkUsuario_EhGestor_Internalname));
                           A1093Usuario_EhPreposto = StringUtil.StrToBool( cgiGet( chkUsuario_EhPreposto_Internalname));
                           A1017Usuario_CrtfPath = cgiGet( edtUsuario_CrtfPath_Internalname);
                           n1017Usuario_CrtfPath = false;
                           A54Usuario_Ativo = StringUtil.StrToBool( cgiGet( chkUsuario_Ativo_Internalname));
                           A1647Usuario_Email = cgiGet( edtUsuario_Email_Internalname);
                           n1647Usuario_Email = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E190J2 */
                                 E190J2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E200J2 */
                                 E200J2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E210J2 */
                                 E210J2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV30Usuario_PessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_cargonom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM1"), AV73Usuario_CargoNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoadoc1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC1"), AV57Usuario_PessoaDoc1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV31Usuario_PessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_cargonom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM2"), AV75Usuario_CargoNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoadoc2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC2"), AV58Usuario_PessoaDoc2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV32Usuario_PessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_cargonom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM3"), AV77Usuario_CargoNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_pessoadoc3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC3"), AV59Usuario_PessoaDoc3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E220J2 */
                                       E220J2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0J2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0J2( ) ;
            }
         }
      }

      protected void PA0J2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOADOC", "CPF)", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOADOC", "CPF)", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_CARGONOM", "Cargo", 0);
            cmbavDynamicfiltersselector3.addItem("USUARIO_PESSOADOC", "CPF)", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "USUARIO_PESSOATIP_" + sGXsfl_90_idx;
            cmbUsuario_PessoaTip.Name = GXCCtl;
            cmbUsuario_PessoaTip.WebTags = "";
            cmbUsuario_PessoaTip.addItem("", "(Nenhum)", 0);
            cmbUsuario_PessoaTip.addItem("F", "F�sica", 0);
            cmbUsuario_PessoaTip.addItem("J", "Jur�dica", 0);
            if ( cmbUsuario_PessoaTip.ItemCount > 0 )
            {
               A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
               n59Usuario_PessoaTip = false;
            }
            GXCCtl = "USUARIO_EHCONTADOR_" + sGXsfl_90_idx;
            chkUsuario_EhContador.Name = GXCCtl;
            chkUsuario_EhContador.WebTags = "";
            chkUsuario_EhContador.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "TitleCaption", chkUsuario_EhContador.Caption);
            chkUsuario_EhContador.CheckedValue = "false";
            GXCCtl = "USUARIO_EHAUDITORFM_" + sGXsfl_90_idx;
            chkUsuario_EhAuditorFM.Name = GXCCtl;
            chkUsuario_EhAuditorFM.WebTags = "";
            chkUsuario_EhAuditorFM.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhAuditorFM_Internalname, "TitleCaption", chkUsuario_EhAuditorFM.Caption);
            chkUsuario_EhAuditorFM.CheckedValue = "false";
            GXCCtl = "USUARIO_EHCONTRATADA_" + sGXsfl_90_idx;
            chkUsuario_EhContratada.Name = GXCCtl;
            chkUsuario_EhContratada.WebTags = "";
            chkUsuario_EhContratada.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "TitleCaption", chkUsuario_EhContratada.Caption);
            chkUsuario_EhContratada.CheckedValue = "false";
            GXCCtl = "USUARIO_EHCONTRATANTE_" + sGXsfl_90_idx;
            chkUsuario_EhContratante.Name = GXCCtl;
            chkUsuario_EhContratante.WebTags = "";
            chkUsuario_EhContratante.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "TitleCaption", chkUsuario_EhContratante.Caption);
            chkUsuario_EhContratante.CheckedValue = "false";
            GXCCtl = "USUARIO_EHFINANCEIRO_" + sGXsfl_90_idx;
            chkUsuario_EhFinanceiro.Name = GXCCtl;
            chkUsuario_EhFinanceiro.WebTags = "";
            chkUsuario_EhFinanceiro.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "TitleCaption", chkUsuario_EhFinanceiro.Caption);
            chkUsuario_EhFinanceiro.CheckedValue = "false";
            GXCCtl = "USUARIO_EHGESTOR_" + sGXsfl_90_idx;
            chkUsuario_EhGestor.Name = GXCCtl;
            chkUsuario_EhGestor.WebTags = "";
            chkUsuario_EhGestor.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhGestor_Internalname, "TitleCaption", chkUsuario_EhGestor.Caption);
            chkUsuario_EhGestor.CheckedValue = "false";
            GXCCtl = "USUARIO_EHPREPOSTO_" + sGXsfl_90_idx;
            chkUsuario_EhPreposto.Name = GXCCtl;
            chkUsuario_EhPreposto.WebTags = "";
            chkUsuario_EhPreposto.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "TitleCaption", chkUsuario_EhPreposto.Caption);
            chkUsuario_EhPreposto.CheckedValue = "false";
            GXCCtl = "USUARIO_ATIVO_" + sGXsfl_90_idx;
            chkUsuario_Ativo.Name = GXCCtl;
            chkUsuario_Ativo.WebTags = "";
            chkUsuario_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "TitleCaption", chkUsuario_Ativo.Caption);
            chkUsuario_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_902( ) ;
         while ( nGXsfl_90_idx <= nRC_GXsfl_90 )
         {
            sendrow_902( ) ;
            nGXsfl_90_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_90_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV30Usuario_PessoaNom1 ,
                                       String AV73Usuario_CargoNom1 ,
                                       String AV57Usuario_PessoaDoc1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV31Usuario_PessoaNom2 ,
                                       String AV75Usuario_CargoNom2 ,
                                       String AV58Usuario_PessoaDoc2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV32Usuario_PessoaNom3 ,
                                       String AV77Usuario_CargoNom3 ,
                                       String AV59Usuario_PessoaDoc3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String A1647Usuario_Email )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0J2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CARGOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ENTIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!"))));
         GxWebStd.gx_hidden_field( context, "USUARIO_ENTIDADE", StringUtil.RTrim( A1083Usuario_Entidade));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_PESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_USERGAMGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A341Usuario_UserGamGuid, ""))));
         GxWebStd.gx_hidden_field( context, "USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTADOR", GetSecureSignedToken( "", A289Usuario_EhContador));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHCONTADOR", StringUtil.BoolToStr( A289Usuario_EhContador));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHAUDITORFM", GetSecureSignedToken( "", A290Usuario_EhAuditorFM));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHAUDITORFM", StringUtil.BoolToStr( A290Usuario_EhAuditorFM));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTRATADA", GetSecureSignedToken( "", A291Usuario_EhContratada));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHCONTRATADA", StringUtil.BoolToStr( A291Usuario_EhContratada));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTRATANTE", GetSecureSignedToken( "", A292Usuario_EhContratante));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHCONTRATANTE", StringUtil.BoolToStr( A292Usuario_EhContratante));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHFINANCEIRO", GetSecureSignedToken( "", A293Usuario_EhFinanceiro));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHFINANCEIRO", StringUtil.BoolToStr( A293Usuario_EhFinanceiro));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHGESTOR", GetSecureSignedToken( "", A538Usuario_EhGestor));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHGESTOR", StringUtil.BoolToStr( A538Usuario_EhGestor));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHPREPOSTO", GetSecureSignedToken( "", A1093Usuario_EhPreposto));
         GxWebStd.gx_hidden_field( context, "USUARIO_EHPREPOSTO", StringUtil.BoolToStr( A1093Usuario_EhPreposto));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CRTFPATH", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, ""))));
         GxWebStd.gx_hidden_field( context, "USUARIO_CRTFPATH", A1017Usuario_CrtfPath);
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ATIVO", GetSecureSignedToken( "", A54Usuario_Ativo));
         GxWebStd.gx_hidden_field( context, "USUARIO_ATIVO", StringUtil.BoolToStr( A54Usuario_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1647Usuario_Email, ""))));
         GxWebStd.gx_hidden_field( context, "USUARIO_EMAIL", A1647Usuario_Email);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0J2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0J2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 90;
         /* Execute user event: E200J2 */
         E200J2 ();
         nGXsfl_90_idx = 1;
         sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
         SubsflControlProps_902( ) ;
         nGXsfl_90_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_902( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV30Usuario_PessoaNom1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV73Usuario_CargoNom1 ,
                                                 AV57Usuario_PessoaDoc1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV31Usuario_PessoaNom2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV75Usuario_CargoNom2 ,
                                                 AV58Usuario_PessoaDoc2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV32Usuario_PessoaNom3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV77Usuario_CargoNom3 ,
                                                 AV59Usuario_PessoaDoc3 ,
                                                 A58Usuario_PessoaNom ,
                                                 A1074Usuario_CargoNom ,
                                                 A325Usuario_PessoaDoc ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV30Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV30Usuario_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
            lV73Usuario_CargoNom1 = StringUtil.Concat( StringUtil.RTrim( AV73Usuario_CargoNom1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
            lV73Usuario_CargoNom1 = StringUtil.Concat( StringUtil.RTrim( AV73Usuario_CargoNom1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
            lV57Usuario_PessoaDoc1 = StringUtil.Concat( StringUtil.RTrim( AV57Usuario_PessoaDoc1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_PessoaDoc1", AV57Usuario_PessoaDoc1);
            lV31Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV31Usuario_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
            lV75Usuario_CargoNom2 = StringUtil.Concat( StringUtil.RTrim( AV75Usuario_CargoNom2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
            lV75Usuario_CargoNom2 = StringUtil.Concat( StringUtil.RTrim( AV75Usuario_CargoNom2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
            lV58Usuario_PessoaDoc2 = StringUtil.Concat( StringUtil.RTrim( AV58Usuario_PessoaDoc2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Usuario_PessoaDoc2", AV58Usuario_PessoaDoc2);
            lV32Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32Usuario_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
            lV77Usuario_CargoNom3 = StringUtil.Concat( StringUtil.RTrim( AV77Usuario_CargoNom3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
            lV77Usuario_CargoNom3 = StringUtil.Concat( StringUtil.RTrim( AV77Usuario_CargoNom3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
            lV59Usuario_PessoaDoc3 = StringUtil.Concat( StringUtil.RTrim( AV59Usuario_PessoaDoc3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Usuario_PessoaDoc3", AV59Usuario_PessoaDoc3);
            /* Using cursor H000J2 */
            pr_default.execute(0, new Object[] {lV30Usuario_PessoaNom1, lV73Usuario_CargoNom1, lV73Usuario_CargoNom1, lV57Usuario_PessoaDoc1, lV31Usuario_PessoaNom2, lV75Usuario_CargoNom2, lV75Usuario_CargoNom2, lV58Usuario_PessoaDoc2, lV32Usuario_PessoaNom3, lV77Usuario_CargoNom3, lV77Usuario_CargoNom3, lV59Usuario_PessoaDoc3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_90_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1647Usuario_Email = H000J2_A1647Usuario_Email[0];
               n1647Usuario_Email = H000J2_n1647Usuario_Email[0];
               A54Usuario_Ativo = H000J2_A54Usuario_Ativo[0];
               A1017Usuario_CrtfPath = H000J2_A1017Usuario_CrtfPath[0];
               n1017Usuario_CrtfPath = H000J2_n1017Usuario_CrtfPath[0];
               A1093Usuario_EhPreposto = H000J2_A1093Usuario_EhPreposto[0];
               A538Usuario_EhGestor = H000J2_A538Usuario_EhGestor[0];
               A293Usuario_EhFinanceiro = H000J2_A293Usuario_EhFinanceiro[0];
               A292Usuario_EhContratante = H000J2_A292Usuario_EhContratante[0];
               A291Usuario_EhContratada = H000J2_A291Usuario_EhContratada[0];
               A289Usuario_EhContador = H000J2_A289Usuario_EhContador[0];
               A341Usuario_UserGamGuid = H000J2_A341Usuario_UserGamGuid[0];
               A325Usuario_PessoaDoc = H000J2_A325Usuario_PessoaDoc[0];
               n325Usuario_PessoaDoc = H000J2_n325Usuario_PessoaDoc[0];
               A59Usuario_PessoaTip = H000J2_A59Usuario_PessoaTip[0];
               n59Usuario_PessoaTip = H000J2_n59Usuario_PessoaTip[0];
               A58Usuario_PessoaNom = H000J2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H000J2_n58Usuario_PessoaNom[0];
               A57Usuario_PessoaCod = H000J2_A57Usuario_PessoaCod[0];
               A1076Usuario_CargoUONom = H000J2_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H000J2_n1076Usuario_CargoUONom[0];
               A1075Usuario_CargoUOCod = H000J2_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H000J2_n1075Usuario_CargoUOCod[0];
               A1074Usuario_CargoNom = H000J2_A1074Usuario_CargoNom[0];
               n1074Usuario_CargoNom = H000J2_n1074Usuario_CargoNom[0];
               A1073Usuario_CargoCod = H000J2_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H000J2_n1073Usuario_CargoCod[0];
               A1Usuario_Codigo = H000J2_A1Usuario_Codigo[0];
               A325Usuario_PessoaDoc = H000J2_A325Usuario_PessoaDoc[0];
               n325Usuario_PessoaDoc = H000J2_n325Usuario_PessoaDoc[0];
               A59Usuario_PessoaTip = H000J2_A59Usuario_PessoaTip[0];
               n59Usuario_PessoaTip = H000J2_n59Usuario_PessoaTip[0];
               A58Usuario_PessoaNom = H000J2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H000J2_n58Usuario_PessoaNom[0];
               A1075Usuario_CargoUOCod = H000J2_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H000J2_n1075Usuario_CargoUOCod[0];
               A1074Usuario_CargoNom = H000J2_A1074Usuario_CargoNom[0];
               n1074Usuario_CargoNom = H000J2_n1074Usuario_CargoNom[0];
               A1076Usuario_CargoUONom = H000J2_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H000J2_n1076Usuario_CargoUONom[0];
               GXt_char1 = A1083Usuario_Entidade;
               new prc_entidadedousuario(context ).execute(  A1Usuario_Codigo,  0, out  GXt_char1) ;
               A1083Usuario_Entidade = GXt_char1;
               GXt_boolean2 = A290Usuario_EhAuditorFM;
               new prc_usuarioehauditorfm(context ).execute(  A1Usuario_Codigo, out  GXt_boolean2) ;
               A290Usuario_EhAuditorFM = GXt_boolean2;
               /* Execute user event: E210J2 */
               E210J2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 90;
            WB0J0( ) ;
         }
         nGXsfl_90_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV30Usuario_PessoaNom1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV73Usuario_CargoNom1 ,
                                              AV57Usuario_PessoaDoc1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV31Usuario_PessoaNom2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV75Usuario_CargoNom2 ,
                                              AV58Usuario_PessoaDoc2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV32Usuario_PessoaNom3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV77Usuario_CargoNom3 ,
                                              AV59Usuario_PessoaDoc3 ,
                                              A58Usuario_PessoaNom ,
                                              A1074Usuario_CargoNom ,
                                              A325Usuario_PessoaDoc ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV30Usuario_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV30Usuario_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
         lV73Usuario_CargoNom1 = StringUtil.Concat( StringUtil.RTrim( AV73Usuario_CargoNom1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
         lV73Usuario_CargoNom1 = StringUtil.Concat( StringUtil.RTrim( AV73Usuario_CargoNom1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
         lV57Usuario_PessoaDoc1 = StringUtil.Concat( StringUtil.RTrim( AV57Usuario_PessoaDoc1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_PessoaDoc1", AV57Usuario_PessoaDoc1);
         lV31Usuario_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV31Usuario_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
         lV75Usuario_CargoNom2 = StringUtil.Concat( StringUtil.RTrim( AV75Usuario_CargoNom2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
         lV75Usuario_CargoNom2 = StringUtil.Concat( StringUtil.RTrim( AV75Usuario_CargoNom2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
         lV58Usuario_PessoaDoc2 = StringUtil.Concat( StringUtil.RTrim( AV58Usuario_PessoaDoc2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Usuario_PessoaDoc2", AV58Usuario_PessoaDoc2);
         lV32Usuario_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32Usuario_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
         lV77Usuario_CargoNom3 = StringUtil.Concat( StringUtil.RTrim( AV77Usuario_CargoNom3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
         lV77Usuario_CargoNom3 = StringUtil.Concat( StringUtil.RTrim( AV77Usuario_CargoNom3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
         lV59Usuario_PessoaDoc3 = StringUtil.Concat( StringUtil.RTrim( AV59Usuario_PessoaDoc3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Usuario_PessoaDoc3", AV59Usuario_PessoaDoc3);
         /* Using cursor H000J3 */
         pr_default.execute(1, new Object[] {lV30Usuario_PessoaNom1, lV73Usuario_CargoNom1, lV73Usuario_CargoNom1, lV57Usuario_PessoaDoc1, lV31Usuario_PessoaNom2, lV75Usuario_CargoNom2, lV75Usuario_CargoNom2, lV58Usuario_PessoaDoc2, lV32Usuario_PessoaNom3, lV77Usuario_CargoNom3, lV77Usuario_CargoNom3, lV59Usuario_PessoaDoc3});
         GRID_nRecordCount = H000J3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0J0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E190J2 */
         E190J2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV30Usuario_PessoaNom1 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
            AV73Usuario_CargoNom1 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
            AV57Usuario_PessoaDoc1 = cgiGet( edtavUsuario_pessoadoc1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_PessoaDoc1", AV57Usuario_PessoaDoc1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV31Usuario_PessoaNom2 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
            AV75Usuario_CargoNom2 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
            AV58Usuario_PessoaDoc2 = cgiGet( edtavUsuario_pessoadoc2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Usuario_PessoaDoc2", AV58Usuario_PessoaDoc2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV32Usuario_PessoaNom3 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
            AV77Usuario_CargoNom3 = StringUtil.Upper( cgiGet( edtavUsuario_cargonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
            AV59Usuario_PessoaDoc3 = cgiGet( edtavUsuario_pessoadoc3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Usuario_PessoaDoc3", AV59Usuario_PessoaDoc3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_90 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_90"), ",", "."));
            AV70GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV71GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV30Usuario_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM1"), AV73Usuario_CargoNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC1"), AV57Usuario_PessoaDoc1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV31Usuario_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM2"), AV75Usuario_CargoNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC2"), AV58Usuario_PessoaDoc2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM3"), AV32Usuario_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_CARGONOM3"), AV77Usuario_CargoNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOADOC3"), AV59Usuario_PessoaDoc3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E190J2 */
         E190J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190J2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Sele��o de Usu�rio";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pessoa", 0);
         cmbavOrderedby.addItem("2", "Usu�rio", 0);
         cmbavOrderedby.addItem("3", "Cargo", 0);
         cmbavOrderedby.addItem("4", "organizacional", 0);
         cmbavOrderedby.addItem("5", "da Pessoa", 0);
         cmbavOrderedby.addItem("6", "da Pessoa", 0);
         cmbavOrderedby.addItem("7", "Documento", 0);
         cmbavOrderedby.addItem("8", "Guid", 0);
         cmbavOrderedby.addItem("9", "contador?", 0);
         cmbavOrderedby.addItem("10", "uma contratada?", 0);
         cmbavOrderedby.addItem("11", "um contratante?", 0);
         cmbavOrderedby.addItem("12", "do Financeiro?", 0);
         cmbavOrderedby.addItem("13", "Gestor?", 0);
         cmbavOrderedby.addItem("14", "Preposto?", 0);
         cmbavOrderedby.addItem("15", "Digital", 0);
         cmbavOrderedby.addItem("16", "Ativo?", 0);
         cmbavOrderedby.addItem("17", "Usuario_Email", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E200J2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtUsuario_CargoNom_Titleformat = 2;
         edtUsuario_CargoNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Cargo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CargoNom_Internalname, "Title", edtUsuario_CargoNom_Title);
         edtUsuario_CargoUONom_Titleformat = 2;
         edtUsuario_CargoUONom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "organizacional", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CargoUONom_Internalname, "Title", edtUsuario_CargoUONom_Title);
         edtUsuario_PessoaCod_Titleformat = 2;
         edtUsuario_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Pessoa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaCod_Internalname, "Title", edtUsuario_PessoaCod_Title);
         edtUsuario_PessoaNom_Titleformat = 2;
         edtUsuario_PessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Title", edtUsuario_PessoaNom_Title);
         cmbUsuario_PessoaTip_Titleformat = 2;
         cmbUsuario_PessoaTip.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Pessoa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_PessoaTip_Internalname, "Title", cmbUsuario_PessoaTip.Title.Text);
         edtUsuario_PessoaDoc_Titleformat = 2;
         edtUsuario_PessoaDoc_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Documento", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaDoc_Internalname, "Title", edtUsuario_PessoaDoc_Title);
         edtUsuario_UserGamGuid_Titleformat = 2;
         edtUsuario_UserGamGuid_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Guid", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_UserGamGuid_Internalname, "Title", edtUsuario_UserGamGuid_Title);
         chkUsuario_EhContador_Titleformat = 2;
         chkUsuario_EhContador.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV13OrderedBy==9) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "contador?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContador_Internalname, "Title", chkUsuario_EhContador.Title.Text);
         chkUsuario_EhContratada_Titleformat = 2;
         chkUsuario_EhContratada.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV13OrderedBy==10) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "uma contratada?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratada_Internalname, "Title", chkUsuario_EhContratada.Title.Text);
         chkUsuario_EhContratante_Titleformat = 2;
         chkUsuario_EhContratante.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV13OrderedBy==11) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "um contratante?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhContratante_Internalname, "Title", chkUsuario_EhContratante.Title.Text);
         chkUsuario_EhFinanceiro_Titleformat = 2;
         chkUsuario_EhFinanceiro.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV13OrderedBy==12) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "do Financeiro?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhFinanceiro_Internalname, "Title", chkUsuario_EhFinanceiro.Title.Text);
         chkUsuario_EhGestor_Titleformat = 2;
         chkUsuario_EhGestor.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV13OrderedBy==13) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Gestor?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhGestor_Internalname, "Title", chkUsuario_EhGestor.Title.Text);
         chkUsuario_EhPreposto_Titleformat = 2;
         chkUsuario_EhPreposto.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV13OrderedBy==14) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Preposto?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_EhPreposto_Internalname, "Title", chkUsuario_EhPreposto.Title.Text);
         edtUsuario_CrtfPath_Titleformat = 2;
         edtUsuario_CrtfPath_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV13OrderedBy==15) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Digital", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_CrtfPath_Internalname, "Title", edtUsuario_CrtfPath_Title);
         chkUsuario_Ativo_Titleformat = 2;
         chkUsuario_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV13OrderedBy==16) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuario_Ativo_Internalname, "Title", chkUsuario_Ativo.Title.Text);
         edtUsuario_Email_Titleformat = 2;
         edtUsuario_Email_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV13OrderedBy==17) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usuario_Email", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Email_Internalname, "Title", edtUsuario_Email_Title);
         AV70GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridCurrentPage), 10, 0)));
         AV71GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71GridPageCount), 10, 0)));
      }

      protected void E110J2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV69PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV69PageToGo) ;
         }
      }

      private void E210J2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV80Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         edtUsuario_Email_Link = "mailto:"+A1647Usuario_Email;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 90;
         }
         sendrow_902( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_90_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(90, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E220J2 */
         E220J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E220J2( )
      {
         /* Enter Routine */
         AV7InOutUsuario_Codigo = A1Usuario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
         AV33InOutUsuario_PessoaNom = A58Usuario_PessoaNom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutUsuario_PessoaNom", AV33InOutUsuario_PessoaNom);
         context.setWebReturnParms(new Object[] {(int)AV7InOutUsuario_Codigo,(String)AV33InOutUsuario_PessoaNom});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E120J2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E170J2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E130J2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E180J2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E140J2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E150J2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV30Usuario_PessoaNom1, AV73Usuario_CargoNom1, AV57Usuario_PessoaDoc1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV31Usuario_PessoaNom2, AV75Usuario_CargoNom2, AV58Usuario_PessoaDoc2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV32Usuario_PessoaNom3, AV77Usuario_CargoNom3, AV59Usuario_PessoaDoc3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, A1647Usuario_Email) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E160J2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUsuario_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         edtavUsuario_cargonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom1_Visible), 5, 0)));
         edtavUsuario_pessoadoc1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUsuario_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         edtavUsuario_cargonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom2_Visible), 5, 0)));
         edtavUsuario_pessoadoc2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUsuario_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         edtavUsuario_cargonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom3_Visible), 5, 0)));
         edtavUsuario_pessoadoc3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 )
         {
            edtavUsuario_cargonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargonom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 )
         {
            edtavUsuario_pessoadoc3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoadoc3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoadoc3_Visible), 5, 0)));
         }
      }

      protected void S162( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV31Usuario_PessoaNom2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV32Usuario_PessoaNom3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "USUARIO_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV30Usuario_PessoaNom1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV30Usuario_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Usuario_PessoaNom1", AV30Usuario_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV73Usuario_CargoNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Usuario_CargoNom1", AV73Usuario_CargoNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 )
            {
               AV57Usuario_PessoaDoc1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Usuario_PessoaDoc1", AV57Usuario_PessoaDoc1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV31Usuario_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Usuario_PessoaNom2", AV31Usuario_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV75Usuario_CargoNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Usuario_CargoNom2", AV75Usuario_CargoNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 )
               {
                  AV58Usuario_PessoaDoc2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Usuario_PessoaDoc2", AV58Usuario_PessoaDoc2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 )
                  {
                     AV32Usuario_PessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Usuario_PessoaNom3", AV32Usuario_PessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV77Usuario_CargoNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Usuario_CargoNom3", AV77Usuario_CargoNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 )
                  {
                     AV59Usuario_PessoaDoc3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Usuario_PessoaDoc3", AV59Usuario_PessoaDoc3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Usuario_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30Usuario_PessoaNom1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_CargoNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV73Usuario_CargoNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Usuario_PessoaDoc1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57Usuario_PessoaDoc1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Usuario_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Usuario_PessoaNom2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Usuario_CargoNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV75Usuario_CargoNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Usuario_PessoaDoc2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV58Usuario_PessoaDoc2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Usuario_PessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32Usuario_PessoaNom3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Usuario_CargoNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV77Usuario_CargoNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Usuario_PessoaDoc3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV59Usuario_PessoaDoc3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_0J2( true) ;
         }
         else
         {
            wb_table2_5_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_84_0J2( true) ;
         }
         else
         {
            wb_table3_84_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table3_84_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0J2e( true) ;
         }
         else
         {
            wb_table1_2_0J2e( false) ;
         }
      }

      protected void wb_table3_84_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_87_0J2( true) ;
         }
         else
         {
            wb_table4_87_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table4_87_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_84_0J2e( true) ;
         }
         else
         {
            wb_table3_84_0J2e( false) ;
         }
      }

      protected void wb_table4_87_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"90\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cargo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_CargoNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_CargoNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_CargoNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "organizacional") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_CargoUONom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_CargoUONom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_CargoUONom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Entidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbUsuario_PessoaTip_Titleformat == 0 )
               {
                  context.SendWebValue( cmbUsuario_PessoaTip.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbUsuario_PessoaTip.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaDoc_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaDoc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaDoc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_UserGamGuid_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_UserGamGuid_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_UserGamGuid_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhContador_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhContador.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhContador.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Auditor FM?") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhContratada_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhContratada.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhContratada.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhContratante_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhContratante.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhContratante.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhFinanceiro_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhFinanceiro.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhFinanceiro.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhGestor_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhGestor.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhGestor.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_EhPreposto_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_EhPreposto.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_EhPreposto.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_CrtfPath_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_CrtfPath_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_CrtfPath_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuario_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuario_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuario_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Email_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Email_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Email_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1074Usuario_CargoNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_CargoNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_CargoNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1076Usuario_CargoUONom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_CargoUONom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_CargoUONom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1083Usuario_Entidade));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A59Usuario_PessoaTip));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbUsuario_PessoaTip.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbUsuario_PessoaTip_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A325Usuario_PessoaDoc);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaDoc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaDoc_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A341Usuario_UserGamGuid));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_UserGamGuid_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_UserGamGuid_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A289Usuario_EhContador));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhContador.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhContador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A290Usuario_EhAuditorFM));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A291Usuario_EhContratada));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhContratada.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhContratada_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A292Usuario_EhContratante));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhContratante.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhContratante_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A293Usuario_EhFinanceiro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhFinanceiro.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhFinanceiro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A538Usuario_EhGestor));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhGestor.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhGestor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1093Usuario_EhPreposto));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_EhPreposto.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_EhPreposto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1017Usuario_CrtfPath);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_CrtfPath_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_CrtfPath_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A54Usuario_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuario_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuario_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1647Usuario_Email);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Email_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Email_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUsuario_Email_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 90 )
         {
            wbEnd = 0;
            nRC_GXsfl_90 = (short)(nGXsfl_90_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_87_0J2e( true) ;
         }
         else
         {
            wb_table4_87_0J2e( false) ;
         }
      }

      protected void wb_table2_5_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_0J2( true) ;
         }
         else
         {
            wb_table5_13_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0J2e( true) ;
         }
         else
         {
            wb_table2_5_0J2e( false) ;
         }
      }

      protected void wb_table5_13_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_18_0J2( true) ;
         }
         else
         {
            wb_table6_18_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_0J2e( true) ;
         }
         else
         {
            wb_table5_13_0J2e( false) ;
         }
      }

      protected void wb_table6_18_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_21_0J2( true) ;
         }
         else
         {
            wb_table7_21_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table7_21_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_0J2e( true) ;
         }
         else
         {
            wb_table6_18_0J2e( false) ;
         }
      }

      protected void wb_table7_21_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table8_24_0J2( true) ;
         }
         else
         {
            wb_table8_24_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table8_24_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_21_0J2e( true) ;
         }
         else
         {
            wb_table7_21_0J2e( false) ;
         }
      }

      protected void wb_table8_24_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e230j1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_33_0J2( true) ;
         }
         else
         {
            wb_table9_33_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table9_33_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e240j1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_52_0J2( true) ;
         }
         else
         {
            wb_table10_52_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table10_52_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e250j1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_71_0J2( true) ;
         }
         else
         {
            wb_table11_71_0J2( false) ;
         }
         return  ;
      }

      protected void wb_table11_71_0J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_24_0J2e( true) ;
         }
         else
         {
            wb_table8_24_0J2e( false) ;
         }
      }

      protected void wb_table11_71_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom3_Internalname, StringUtil.RTrim( AV32Usuario_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV32Usuario_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom3_Internalname, AV77Usuario_CargoNom3, StringUtil.RTrim( context.localUtil.Format( AV77Usuario_CargoNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc3_Internalname, AV59Usuario_PessoaDoc3, StringUtil.RTrim( context.localUtil.Format( AV59Usuario_PessoaDoc3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_71_0J2e( true) ;
         }
         else
         {
            wb_table11_71_0J2e( false) ;
         }
      }

      protected void wb_table10_52_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom2_Internalname, StringUtil.RTrim( AV31Usuario_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV31Usuario_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom2_Internalname, AV75Usuario_CargoNom2, StringUtil.RTrim( context.localUtil.Format( AV75Usuario_CargoNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc2_Internalname, AV58Usuario_PessoaDoc2, StringUtil.RTrim( context.localUtil.Format( AV58Usuario_PessoaDoc2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_52_0J2e( true) ;
         }
         else
         {
            wb_table10_52_0J2e( false) ;
         }
      }

      protected void wb_table9_33_0J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_PromptUsuario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom1_Internalname, StringUtil.RTrim( AV30Usuario_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV30Usuario_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargonom1_Internalname, AV73Usuario_CargoNom1, StringUtil.RTrim( context.localUtil.Format( AV73Usuario_CargoNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargonom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoadoc1_Internalname, AV57Usuario_PessoaDoc1, StringUtil.RTrim( context.localUtil.Format( AV57Usuario_PessoaDoc1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoadoc1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoadoc1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_33_0J2e( true) ;
         }
         else
         {
            wb_table9_33_0J2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutUsuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutUsuario_Codigo), 6, 0)));
         AV33InOutUsuario_PessoaNom = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33InOutUsuario_PessoaNom", AV33InOutUsuario_PessoaNom);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0J2( ) ;
         WS0J2( ) ;
         WE0J2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117303870");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptusuario.js", "?20203117303870");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_902( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_90_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_90_idx;
         edtUsuario_CargoCod_Internalname = "USUARIO_CARGOCOD_"+sGXsfl_90_idx;
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM_"+sGXsfl_90_idx;
         edtUsuario_CargoUOCod_Internalname = "USUARIO_CARGOUOCOD_"+sGXsfl_90_idx;
         edtUsuario_CargoUONom_Internalname = "USUARIO_CARGOUONOM_"+sGXsfl_90_idx;
         edtUsuario_Entidade_Internalname = "USUARIO_ENTIDADE_"+sGXsfl_90_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_90_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_90_idx;
         cmbUsuario_PessoaTip_Internalname = "USUARIO_PESSOATIP_"+sGXsfl_90_idx;
         edtUsuario_PessoaDoc_Internalname = "USUARIO_PESSOADOC_"+sGXsfl_90_idx;
         edtUsuario_UserGamGuid_Internalname = "USUARIO_USERGAMGUID_"+sGXsfl_90_idx;
         chkUsuario_EhContador_Internalname = "USUARIO_EHCONTADOR_"+sGXsfl_90_idx;
         chkUsuario_EhAuditorFM_Internalname = "USUARIO_EHAUDITORFM_"+sGXsfl_90_idx;
         chkUsuario_EhContratada_Internalname = "USUARIO_EHCONTRATADA_"+sGXsfl_90_idx;
         chkUsuario_EhContratante_Internalname = "USUARIO_EHCONTRATANTE_"+sGXsfl_90_idx;
         chkUsuario_EhFinanceiro_Internalname = "USUARIO_EHFINANCEIRO_"+sGXsfl_90_idx;
         chkUsuario_EhGestor_Internalname = "USUARIO_EHGESTOR_"+sGXsfl_90_idx;
         chkUsuario_EhPreposto_Internalname = "USUARIO_EHPREPOSTO_"+sGXsfl_90_idx;
         edtUsuario_CrtfPath_Internalname = "USUARIO_CRTFPATH_"+sGXsfl_90_idx;
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO_"+sGXsfl_90_idx;
         edtUsuario_Email_Internalname = "USUARIO_EMAIL_"+sGXsfl_90_idx;
      }

      protected void SubsflControlProps_fel_902( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_90_fel_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_90_fel_idx;
         edtUsuario_CargoCod_Internalname = "USUARIO_CARGOCOD_"+sGXsfl_90_fel_idx;
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM_"+sGXsfl_90_fel_idx;
         edtUsuario_CargoUOCod_Internalname = "USUARIO_CARGOUOCOD_"+sGXsfl_90_fel_idx;
         edtUsuario_CargoUONom_Internalname = "USUARIO_CARGOUONOM_"+sGXsfl_90_fel_idx;
         edtUsuario_Entidade_Internalname = "USUARIO_ENTIDADE_"+sGXsfl_90_fel_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_90_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_90_fel_idx;
         cmbUsuario_PessoaTip_Internalname = "USUARIO_PESSOATIP_"+sGXsfl_90_fel_idx;
         edtUsuario_PessoaDoc_Internalname = "USUARIO_PESSOADOC_"+sGXsfl_90_fel_idx;
         edtUsuario_UserGamGuid_Internalname = "USUARIO_USERGAMGUID_"+sGXsfl_90_fel_idx;
         chkUsuario_EhContador_Internalname = "USUARIO_EHCONTADOR_"+sGXsfl_90_fel_idx;
         chkUsuario_EhAuditorFM_Internalname = "USUARIO_EHAUDITORFM_"+sGXsfl_90_fel_idx;
         chkUsuario_EhContratada_Internalname = "USUARIO_EHCONTRATADA_"+sGXsfl_90_fel_idx;
         chkUsuario_EhContratante_Internalname = "USUARIO_EHCONTRATANTE_"+sGXsfl_90_fel_idx;
         chkUsuario_EhFinanceiro_Internalname = "USUARIO_EHFINANCEIRO_"+sGXsfl_90_fel_idx;
         chkUsuario_EhGestor_Internalname = "USUARIO_EHGESTOR_"+sGXsfl_90_fel_idx;
         chkUsuario_EhPreposto_Internalname = "USUARIO_EHPREPOSTO_"+sGXsfl_90_fel_idx;
         edtUsuario_CrtfPath_Internalname = "USUARIO_CRTFPATH_"+sGXsfl_90_fel_idx;
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO_"+sGXsfl_90_fel_idx;
         edtUsuario_Email_Internalname = "USUARIO_EMAIL_"+sGXsfl_90_fel_idx;
      }

      protected void sendrow_902( )
      {
         SubsflControlProps_902( ) ;
         WB0J0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_90_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_90_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_90_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 91,'',false,'',90)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV80Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_90_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CargoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1073Usuario_CargoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_CargoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CargoNom_Internalname,(String)A1074Usuario_CargoNom,StringUtil.RTrim( context.localUtil.Format( A1074Usuario_CargoNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_CargoNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CargoUOCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1075Usuario_CargoUOCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_CargoUOCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CargoUONom_Internalname,StringUtil.RTrim( A1076Usuario_CargoUONom),StringUtil.RTrim( context.localUtil.Format( A1076Usuario_CargoUONom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_CargoUONom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Entidade_Internalname,StringUtil.RTrim( A1083Usuario_Entidade),StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Entidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "USUARIO_PESSOATIP_" + sGXsfl_90_idx;
            cmbUsuario_PessoaTip.Name = GXCCtl;
            cmbUsuario_PessoaTip.WebTags = "";
            cmbUsuario_PessoaTip.addItem("", "(Nenhum)", 0);
            cmbUsuario_PessoaTip.addItem("F", "F�sica", 0);
            cmbUsuario_PessoaTip.addItem("J", "Jur�dica", 0);
            if ( cmbUsuario_PessoaTip.ItemCount > 0 )
            {
               A59Usuario_PessoaTip = cmbUsuario_PessoaTip.getValidValue(A59Usuario_PessoaTip);
               n59Usuario_PessoaTip = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbUsuario_PessoaTip,(String)cmbUsuario_PessoaTip_Internalname,StringUtil.RTrim( A59Usuario_PessoaTip),(short)1,(String)cmbUsuario_PessoaTip_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbUsuario_PessoaTip.CurrentValue = StringUtil.RTrim( A59Usuario_PessoaTip);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbUsuario_PessoaTip_Internalname, "Values", (String)(cmbUsuario_PessoaTip.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaDoc_Internalname,(String)A325Usuario_PessoaDoc,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaDoc_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_UserGamGuid_Internalname,StringUtil.RTrim( A341Usuario_UserGamGuid),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_UserGamGuid_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMGUID",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhContador_Internalname,StringUtil.BoolToStr( A289Usuario_EhContador),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhAuditorFM_Internalname,StringUtil.BoolToStr( A290Usuario_EhAuditorFM),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhContratada_Internalname,StringUtil.BoolToStr( A291Usuario_EhContratada),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhContratante_Internalname,StringUtil.BoolToStr( A292Usuario_EhContratante),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhFinanceiro_Internalname,StringUtil.BoolToStr( A293Usuario_EhFinanceiro),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhGestor_Internalname,StringUtil.BoolToStr( A538Usuario_EhGestor),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_EhPreposto_Internalname,StringUtil.BoolToStr( A1093Usuario_EhPreposto),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_CrtfPath_Internalname,(String)A1017Usuario_CrtfPath,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"(path + file name)",(String)edtUsuario_CrtfPath_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuario_Ativo_Internalname,StringUtil.BoolToStr( A54Usuario_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Email_Internalname,(String)A1647Usuario_Email,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUsuario_Email_Link,(String)"",(String)"",(String)"",(String)edtUsuario_Email_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"email",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Email",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CARGOCOD"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, context.localUtil.Format( (decimal)(A1073Usuario_CargoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ENTIDADE"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, StringUtil.RTrim( context.localUtil.Format( A1083Usuario_Entidade, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_PESSOACOD"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_USERGAMGUID"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, StringUtil.RTrim( context.localUtil.Format( A341Usuario_UserGamGuid, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTADOR"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A289Usuario_EhContador));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHAUDITORFM"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A290Usuario_EhAuditorFM));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTRATADA"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A291Usuario_EhContratada));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHCONTRATANTE"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A292Usuario_EhContratante));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHFINANCEIRO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A293Usuario_EhFinanceiro));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHGESTOR"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A538Usuario_EhGestor));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EHPREPOSTO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A1093Usuario_EhPreposto));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CRTFPATH"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, StringUtil.RTrim( context.localUtil.Format( A1017Usuario_CrtfPath, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_ATIVO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, A54Usuario_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_EMAIL"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, StringUtil.RTrim( context.localUtil.Format( A1647Usuario_Email, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_90_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_90_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         /* End function sendrow_902 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavUsuario_pessoanom1_Internalname = "vUSUARIO_PESSOANOM1";
         edtavUsuario_cargonom1_Internalname = "vUSUARIO_CARGONOM1";
         edtavUsuario_pessoadoc1_Internalname = "vUSUARIO_PESSOADOC1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavUsuario_pessoanom2_Internalname = "vUSUARIO_PESSOANOM2";
         edtavUsuario_cargonom2_Internalname = "vUSUARIO_CARGONOM2";
         edtavUsuario_pessoadoc2_Internalname = "vUSUARIO_PESSOADOC2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavUsuario_pessoanom3_Internalname = "vUSUARIO_PESSOANOM3";
         edtavUsuario_cargonom3_Internalname = "vUSUARIO_CARGONOM3";
         edtavUsuario_pessoadoc3_Internalname = "vUSUARIO_PESSOADOC3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtUsuario_CargoCod_Internalname = "USUARIO_CARGOCOD";
         edtUsuario_CargoNom_Internalname = "USUARIO_CARGONOM";
         edtUsuario_CargoUOCod_Internalname = "USUARIO_CARGOUOCOD";
         edtUsuario_CargoUONom_Internalname = "USUARIO_CARGOUONOM";
         edtUsuario_Entidade_Internalname = "USUARIO_ENTIDADE";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         cmbUsuario_PessoaTip_Internalname = "USUARIO_PESSOATIP";
         edtUsuario_PessoaDoc_Internalname = "USUARIO_PESSOADOC";
         edtUsuario_UserGamGuid_Internalname = "USUARIO_USERGAMGUID";
         chkUsuario_EhContador_Internalname = "USUARIO_EHCONTADOR";
         chkUsuario_EhAuditorFM_Internalname = "USUARIO_EHAUDITORFM";
         chkUsuario_EhContratada_Internalname = "USUARIO_EHCONTRATADA";
         chkUsuario_EhContratante_Internalname = "USUARIO_EHCONTRATANTE";
         chkUsuario_EhFinanceiro_Internalname = "USUARIO_EHFINANCEIRO";
         chkUsuario_EhGestor_Internalname = "USUARIO_EHGESTOR";
         chkUsuario_EhPreposto_Internalname = "USUARIO_EHPREPOSTO";
         edtUsuario_CrtfPath_Internalname = "USUARIO_CRTFPATH";
         chkUsuario_Ativo_Internalname = "USUARIO_ATIVO";
         edtUsuario_Email_Internalname = "USUARIO_EMAIL";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtUsuario_Email_Jsonclick = "";
         edtUsuario_CrtfPath_Jsonclick = "";
         edtUsuario_UserGamGuid_Jsonclick = "";
         edtUsuario_PessoaDoc_Jsonclick = "";
         cmbUsuario_PessoaTip_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_PessoaCod_Jsonclick = "";
         edtUsuario_Entidade_Jsonclick = "";
         edtUsuario_CargoUONom_Jsonclick = "";
         edtUsuario_CargoUOCod_Jsonclick = "";
         edtUsuario_CargoNom_Jsonclick = "";
         edtUsuario_CargoCod_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavUsuario_pessoadoc1_Jsonclick = "";
         edtavUsuario_cargonom1_Jsonclick = "";
         edtavUsuario_pessoanom1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavUsuario_pessoadoc2_Jsonclick = "";
         edtavUsuario_cargonom2_Jsonclick = "";
         edtavUsuario_pessoanom2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavUsuario_pessoadoc3_Jsonclick = "";
         edtavUsuario_cargonom3_Jsonclick = "";
         edtavUsuario_pessoanom3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUsuario_Email_Link = "";
         edtavSelect_Tooltiptext = "Selecionar";
         edtUsuario_Email_Titleformat = 0;
         chkUsuario_Ativo_Titleformat = 0;
         edtUsuario_CrtfPath_Titleformat = 0;
         chkUsuario_EhPreposto_Titleformat = 0;
         chkUsuario_EhGestor_Titleformat = 0;
         chkUsuario_EhFinanceiro_Titleformat = 0;
         chkUsuario_EhContratante_Titleformat = 0;
         chkUsuario_EhContratada_Titleformat = 0;
         chkUsuario_EhContador_Titleformat = 0;
         edtUsuario_UserGamGuid_Titleformat = 0;
         edtUsuario_PessoaDoc_Titleformat = 0;
         cmbUsuario_PessoaTip_Titleformat = 0;
         edtUsuario_PessoaNom_Titleformat = 0;
         edtUsuario_PessoaCod_Titleformat = 0;
         edtUsuario_CargoUONom_Titleformat = 0;
         edtUsuario_CargoNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavUsuario_pessoadoc3_Visible = 1;
         edtavUsuario_cargonom3_Visible = 1;
         edtavUsuario_pessoanom3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavUsuario_pessoadoc2_Visible = 1;
         edtavUsuario_cargonom2_Visible = 1;
         edtavUsuario_pessoanom2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavUsuario_pessoadoc1_Visible = 1;
         edtavUsuario_cargonom1_Visible = 1;
         edtavUsuario_pessoanom1_Visible = 1;
         edtUsuario_Email_Title = "Usuario_Email";
         chkUsuario_Ativo.Title.Text = "Ativo?";
         edtUsuario_CrtfPath_Title = "Digital";
         chkUsuario_EhPreposto.Title.Text = "Preposto?";
         chkUsuario_EhGestor.Title.Text = "Gestor?";
         chkUsuario_EhFinanceiro.Title.Text = "do Financeiro?";
         chkUsuario_EhContratante.Title.Text = "um contratante?";
         chkUsuario_EhContratada.Title.Text = "uma contratada?";
         chkUsuario_EhContador.Title.Text = "contador?";
         edtUsuario_UserGamGuid_Title = "Guid";
         edtUsuario_PessoaDoc_Title = "Documento";
         cmbUsuario_PessoaTip.Title.Text = "da Pessoa";
         edtUsuario_PessoaNom_Title = "Nome";
         edtUsuario_PessoaCod_Title = "da Pessoa";
         edtUsuario_CargoUONom_Title = "organizacional";
         edtUsuario_CargoNom_Title = "Cargo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkUsuario_Ativo.Caption = "";
         chkUsuario_EhPreposto.Caption = "";
         chkUsuario_EhGestor.Caption = "";
         chkUsuario_EhFinanceiro.Caption = "";
         chkUsuario_EhContratante.Caption = "";
         chkUsuario_EhContratada.Caption = "";
         chkUsuario_EhAuditorFM.Caption = "";
         chkUsuario_EhContador.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Select Usuario";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'edtUsuario_CargoNom_Titleformat',ctrl:'USUARIO_CARGONOM',prop:'Titleformat'},{av:'edtUsuario_CargoNom_Title',ctrl:'USUARIO_CARGONOM',prop:'Title'},{av:'edtUsuario_CargoUONom_Titleformat',ctrl:'USUARIO_CARGOUONOM',prop:'Titleformat'},{av:'edtUsuario_CargoUONom_Title',ctrl:'USUARIO_CARGOUONOM',prop:'Title'},{av:'edtUsuario_PessoaCod_Titleformat',ctrl:'USUARIO_PESSOACOD',prop:'Titleformat'},{av:'edtUsuario_PessoaCod_Title',ctrl:'USUARIO_PESSOACOD',prop:'Title'},{av:'edtUsuario_PessoaNom_Titleformat',ctrl:'USUARIO_PESSOANOM',prop:'Titleformat'},{av:'edtUsuario_PessoaNom_Title',ctrl:'USUARIO_PESSOANOM',prop:'Title'},{av:'cmbUsuario_PessoaTip'},{av:'edtUsuario_PessoaDoc_Titleformat',ctrl:'USUARIO_PESSOADOC',prop:'Titleformat'},{av:'edtUsuario_PessoaDoc_Title',ctrl:'USUARIO_PESSOADOC',prop:'Title'},{av:'edtUsuario_UserGamGuid_Titleformat',ctrl:'USUARIO_USERGAMGUID',prop:'Titleformat'},{av:'edtUsuario_UserGamGuid_Title',ctrl:'USUARIO_USERGAMGUID',prop:'Title'},{av:'chkUsuario_EhContador_Titleformat',ctrl:'USUARIO_EHCONTADOR',prop:'Titleformat'},{av:'chkUsuario_EhContador.Title.Text',ctrl:'USUARIO_EHCONTADOR',prop:'Title'},{av:'chkUsuario_EhContratada_Titleformat',ctrl:'USUARIO_EHCONTRATADA',prop:'Titleformat'},{av:'chkUsuario_EhContratada.Title.Text',ctrl:'USUARIO_EHCONTRATADA',prop:'Title'},{av:'chkUsuario_EhContratante_Titleformat',ctrl:'USUARIO_EHCONTRATANTE',prop:'Titleformat'},{av:'chkUsuario_EhContratante.Title.Text',ctrl:'USUARIO_EHCONTRATANTE',prop:'Title'},{av:'chkUsuario_EhFinanceiro_Titleformat',ctrl:'USUARIO_EHFINANCEIRO',prop:'Titleformat'},{av:'chkUsuario_EhFinanceiro.Title.Text',ctrl:'USUARIO_EHFINANCEIRO',prop:'Title'},{av:'chkUsuario_EhGestor_Titleformat',ctrl:'USUARIO_EHGESTOR',prop:'Titleformat'},{av:'chkUsuario_EhGestor.Title.Text',ctrl:'USUARIO_EHGESTOR',prop:'Title'},{av:'chkUsuario_EhPreposto_Titleformat',ctrl:'USUARIO_EHPREPOSTO',prop:'Titleformat'},{av:'chkUsuario_EhPreposto.Title.Text',ctrl:'USUARIO_EHPREPOSTO',prop:'Title'},{av:'edtUsuario_CrtfPath_Titleformat',ctrl:'USUARIO_CRTFPATH',prop:'Titleformat'},{av:'edtUsuario_CrtfPath_Title',ctrl:'USUARIO_CRTFPATH',prop:'Title'},{av:'chkUsuario_Ativo_Titleformat',ctrl:'USUARIO_ATIVO',prop:'Titleformat'},{av:'chkUsuario_Ativo.Title.Text',ctrl:'USUARIO_ATIVO',prop:'Title'},{av:'edtUsuario_Email_Titleformat',ctrl:'USUARIO_EMAIL',prop:'Titleformat'},{av:'edtUsuario_Email_Title',ctrl:'USUARIO_EMAIL',prop:'Title'},{av:'AV70GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV71GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E210J2',iparms:[{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''}],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'},{av:'edtUsuario_Email_Link',ctrl:'USUARIO_EMAIL',prop:'Link'}]}");
         setEventMetadata("ENTER","{handler:'E220J2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV7InOutUsuario_Codigo',fld:'vINOUTUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33InOutUsuario_PessoaNom',fld:'vINOUTUSUARIO_PESSOANOM',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E120J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E170J2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E130J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E230J1',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E180J2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E140J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E240J1',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E150J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E250J1',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E160J2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',hsh:true,nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV30Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'edtavUsuario_cargonom1_Visible',ctrl:'vUSUARIO_CARGONOM1',prop:'Visible'},{av:'edtavUsuario_pessoadoc1_Visible',ctrl:'vUSUARIO_PESSOADOC1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV32Usuario_PessoaNom3',fld:'vUSUARIO_PESSOANOM3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV73Usuario_CargoNom1',fld:'vUSUARIO_CARGONOM1',pic:'@!',nv:''},{av:'AV57Usuario_PessoaDoc1',fld:'vUSUARIO_PESSOADOC1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV75Usuario_CargoNom2',fld:'vUSUARIO_CARGONOM2',pic:'@!',nv:''},{av:'AV58Usuario_PessoaDoc2',fld:'vUSUARIO_PESSOADOC2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV77Usuario_CargoNom3',fld:'vUSUARIO_CARGONOM3',pic:'@!',nv:''},{av:'AV59Usuario_PessoaDoc3',fld:'vUSUARIO_PESSOADOC3',pic:'',nv:''},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavUsuario_cargonom2_Visible',ctrl:'vUSUARIO_CARGONOM2',prop:'Visible'},{av:'edtavUsuario_pessoadoc2_Visible',ctrl:'vUSUARIO_PESSOADOC2',prop:'Visible'},{av:'edtavUsuario_pessoanom3_Visible',ctrl:'vUSUARIO_PESSOANOM3',prop:'Visible'},{av:'edtavUsuario_cargonom3_Visible',ctrl:'vUSUARIO_CARGONOM3',prop:'Visible'},{av:'edtavUsuario_pessoadoc3_Visible',ctrl:'vUSUARIO_PESSOADOC3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV33InOutUsuario_PessoaNom = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV30Usuario_PessoaNom1 = "";
         AV73Usuario_CargoNom1 = "";
         AV57Usuario_PessoaDoc1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV31Usuario_PessoaNom2 = "";
         AV75Usuario_CargoNom2 = "";
         AV58Usuario_PessoaDoc2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV32Usuario_PessoaNom3 = "";
         AV77Usuario_CargoNom3 = "";
         AV59Usuario_PessoaDoc3 = "";
         A1647Usuario_Email = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV80Select_GXI = "";
         A1074Usuario_CargoNom = "";
         A1076Usuario_CargoUONom = "";
         A1083Usuario_Entidade = "";
         A58Usuario_PessoaNom = "";
         A59Usuario_PessoaTip = "";
         A325Usuario_PessoaDoc = "";
         A341Usuario_UserGamGuid = "";
         A1017Usuario_CrtfPath = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV30Usuario_PessoaNom1 = "";
         lV73Usuario_CargoNom1 = "";
         lV57Usuario_PessoaDoc1 = "";
         lV31Usuario_PessoaNom2 = "";
         lV75Usuario_CargoNom2 = "";
         lV58Usuario_PessoaDoc2 = "";
         lV32Usuario_PessoaNom3 = "";
         lV77Usuario_CargoNom3 = "";
         lV59Usuario_PessoaDoc3 = "";
         H000J2_A1647Usuario_Email = new String[] {""} ;
         H000J2_n1647Usuario_Email = new bool[] {false} ;
         H000J2_A54Usuario_Ativo = new bool[] {false} ;
         H000J2_A1017Usuario_CrtfPath = new String[] {""} ;
         H000J2_n1017Usuario_CrtfPath = new bool[] {false} ;
         H000J2_A1093Usuario_EhPreposto = new bool[] {false} ;
         H000J2_A538Usuario_EhGestor = new bool[] {false} ;
         H000J2_A293Usuario_EhFinanceiro = new bool[] {false} ;
         H000J2_A292Usuario_EhContratante = new bool[] {false} ;
         H000J2_A291Usuario_EhContratada = new bool[] {false} ;
         H000J2_A289Usuario_EhContador = new bool[] {false} ;
         H000J2_A341Usuario_UserGamGuid = new String[] {""} ;
         H000J2_A325Usuario_PessoaDoc = new String[] {""} ;
         H000J2_n325Usuario_PessoaDoc = new bool[] {false} ;
         H000J2_A59Usuario_PessoaTip = new String[] {""} ;
         H000J2_n59Usuario_PessoaTip = new bool[] {false} ;
         H000J2_A58Usuario_PessoaNom = new String[] {""} ;
         H000J2_n58Usuario_PessoaNom = new bool[] {false} ;
         H000J2_A57Usuario_PessoaCod = new int[1] ;
         H000J2_A1076Usuario_CargoUONom = new String[] {""} ;
         H000J2_n1076Usuario_CargoUONom = new bool[] {false} ;
         H000J2_A1075Usuario_CargoUOCod = new int[1] ;
         H000J2_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H000J2_A1074Usuario_CargoNom = new String[] {""} ;
         H000J2_n1074Usuario_CargoNom = new bool[] {false} ;
         H000J2_A1073Usuario_CargoCod = new int[1] ;
         H000J2_n1073Usuario_CargoCod = new bool[] {false} ;
         H000J2_A1Usuario_Codigo = new int[1] ;
         GXt_char1 = "";
         H000J3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptusuario__default(),
            new Object[][] {
                new Object[] {
               H000J2_A1647Usuario_Email, H000J2_n1647Usuario_Email, H000J2_A54Usuario_Ativo, H000J2_A1017Usuario_CrtfPath, H000J2_n1017Usuario_CrtfPath, H000J2_A1093Usuario_EhPreposto, H000J2_A538Usuario_EhGestor, H000J2_A293Usuario_EhFinanceiro, H000J2_A292Usuario_EhContratante, H000J2_A291Usuario_EhContratada,
               H000J2_A289Usuario_EhContador, H000J2_A341Usuario_UserGamGuid, H000J2_A325Usuario_PessoaDoc, H000J2_n325Usuario_PessoaDoc, H000J2_A59Usuario_PessoaTip, H000J2_n59Usuario_PessoaTip, H000J2_A58Usuario_PessoaNom, H000J2_n58Usuario_PessoaNom, H000J2_A57Usuario_PessoaCod, H000J2_A1076Usuario_CargoUONom,
               H000J2_n1076Usuario_CargoUONom, H000J2_A1075Usuario_CargoUOCod, H000J2_n1075Usuario_CargoUOCod, H000J2_A1074Usuario_CargoNom, H000J2_n1074Usuario_CargoNom, H000J2_A1073Usuario_CargoCod, H000J2_n1073Usuario_CargoCod, H000J2_A1Usuario_Codigo
               }
               , new Object[] {
               H000J3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_90 ;
      private short nGXsfl_90_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_90_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuario_CargoNom_Titleformat ;
      private short edtUsuario_CargoUONom_Titleformat ;
      private short edtUsuario_PessoaCod_Titleformat ;
      private short edtUsuario_PessoaNom_Titleformat ;
      private short cmbUsuario_PessoaTip_Titleformat ;
      private short edtUsuario_PessoaDoc_Titleformat ;
      private short edtUsuario_UserGamGuid_Titleformat ;
      private short chkUsuario_EhContador_Titleformat ;
      private short chkUsuario_EhContratada_Titleformat ;
      private short chkUsuario_EhContratante_Titleformat ;
      private short chkUsuario_EhFinanceiro_Titleformat ;
      private short chkUsuario_EhGestor_Titleformat ;
      private short chkUsuario_EhPreposto_Titleformat ;
      private short edtUsuario_CrtfPath_Titleformat ;
      private short chkUsuario_Ativo_Titleformat ;
      private short edtUsuario_Email_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutUsuario_Codigo ;
      private int wcpOAV7InOutUsuario_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A1Usuario_Codigo ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A57Usuario_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV69PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUsuario_pessoanom1_Visible ;
      private int edtavUsuario_cargonom1_Visible ;
      private int edtavUsuario_pessoadoc1_Visible ;
      private int edtavUsuario_pessoanom2_Visible ;
      private int edtavUsuario_cargonom2_Visible ;
      private int edtavUsuario_pessoadoc2_Visible ;
      private int edtavUsuario_pessoanom3_Visible ;
      private int edtavUsuario_cargonom3_Visible ;
      private int edtavUsuario_pessoadoc3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV70GridCurrentPage ;
      private long AV71GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV33InOutUsuario_PessoaNom ;
      private String wcpOAV33InOutUsuario_PessoaNom ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_90_idx="0001" ;
      private String AV30Usuario_PessoaNom1 ;
      private String AV31Usuario_PessoaNom2 ;
      private String AV32Usuario_PessoaNom3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtUsuario_CargoCod_Internalname ;
      private String edtUsuario_CargoNom_Internalname ;
      private String edtUsuario_CargoUOCod_Internalname ;
      private String A1076Usuario_CargoUONom ;
      private String edtUsuario_CargoUONom_Internalname ;
      private String A1083Usuario_Entidade ;
      private String edtUsuario_Entidade_Internalname ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String cmbUsuario_PessoaTip_Internalname ;
      private String A59Usuario_PessoaTip ;
      private String edtUsuario_PessoaDoc_Internalname ;
      private String A341Usuario_UserGamGuid ;
      private String edtUsuario_UserGamGuid_Internalname ;
      private String chkUsuario_EhContador_Internalname ;
      private String chkUsuario_EhAuditorFM_Internalname ;
      private String chkUsuario_EhContratada_Internalname ;
      private String chkUsuario_EhContratante_Internalname ;
      private String chkUsuario_EhFinanceiro_Internalname ;
      private String chkUsuario_EhGestor_Internalname ;
      private String chkUsuario_EhPreposto_Internalname ;
      private String edtUsuario_CrtfPath_Internalname ;
      private String chkUsuario_Ativo_Internalname ;
      private String edtUsuario_Email_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV30Usuario_PessoaNom1 ;
      private String lV31Usuario_PessoaNom2 ;
      private String lV32Usuario_PessoaNom3 ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavUsuario_pessoanom1_Internalname ;
      private String edtavUsuario_cargonom1_Internalname ;
      private String edtavUsuario_pessoadoc1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavUsuario_pessoanom2_Internalname ;
      private String edtavUsuario_cargonom2_Internalname ;
      private String edtavUsuario_pessoadoc2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavUsuario_pessoanom3_Internalname ;
      private String edtavUsuario_cargonom3_Internalname ;
      private String edtavUsuario_pessoadoc3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUsuario_CargoNom_Title ;
      private String edtUsuario_CargoUONom_Title ;
      private String edtUsuario_PessoaCod_Title ;
      private String edtUsuario_PessoaNom_Title ;
      private String edtUsuario_PessoaDoc_Title ;
      private String edtUsuario_UserGamGuid_Title ;
      private String edtUsuario_CrtfPath_Title ;
      private String edtUsuario_Email_Title ;
      private String edtavSelect_Tooltiptext ;
      private String edtUsuario_Email_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavUsuario_pessoanom3_Jsonclick ;
      private String edtavUsuario_cargonom3_Jsonclick ;
      private String edtavUsuario_pessoadoc3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavUsuario_pessoanom2_Jsonclick ;
      private String edtavUsuario_cargonom2_Jsonclick ;
      private String edtavUsuario_pessoadoc2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavUsuario_pessoanom1_Jsonclick ;
      private String edtavUsuario_cargonom1_Jsonclick ;
      private String edtavUsuario_pessoadoc1_Jsonclick ;
      private String sGXsfl_90_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtUsuario_CargoCod_Jsonclick ;
      private String edtUsuario_CargoNom_Jsonclick ;
      private String edtUsuario_CargoUOCod_Jsonclick ;
      private String edtUsuario_CargoUONom_Jsonclick ;
      private String edtUsuario_Entidade_Jsonclick ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String cmbUsuario_PessoaTip_Jsonclick ;
      private String edtUsuario_PessoaDoc_Jsonclick ;
      private String edtUsuario_UserGamGuid_Jsonclick ;
      private String edtUsuario_CrtfPath_Jsonclick ;
      private String edtUsuario_Email_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool n1647Usuario_Email ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1073Usuario_CargoCod ;
      private bool n1074Usuario_CargoNom ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1076Usuario_CargoUONom ;
      private bool n58Usuario_PessoaNom ;
      private bool n59Usuario_PessoaTip ;
      private bool n325Usuario_PessoaDoc ;
      private bool A289Usuario_EhContador ;
      private bool A290Usuario_EhAuditorFM ;
      private bool A291Usuario_EhContratada ;
      private bool A292Usuario_EhContratante ;
      private bool A293Usuario_EhFinanceiro ;
      private bool A538Usuario_EhGestor ;
      private bool A1093Usuario_EhPreposto ;
      private bool n1017Usuario_CrtfPath ;
      private bool A54Usuario_Ativo ;
      private bool GXt_boolean2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV73Usuario_CargoNom1 ;
      private String AV57Usuario_PessoaDoc1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV75Usuario_CargoNom2 ;
      private String AV58Usuario_PessoaDoc2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV77Usuario_CargoNom3 ;
      private String AV59Usuario_PessoaDoc3 ;
      private String A1647Usuario_Email ;
      private String AV80Select_GXI ;
      private String A1074Usuario_CargoNom ;
      private String A325Usuario_PessoaDoc ;
      private String A1017Usuario_CrtfPath ;
      private String lV73Usuario_CargoNom1 ;
      private String lV57Usuario_PessoaDoc1 ;
      private String lV75Usuario_CargoNom2 ;
      private String lV58Usuario_PessoaDoc2 ;
      private String lV77Usuario_CargoNom3 ;
      private String lV59Usuario_PessoaDoc3 ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutUsuario_Codigo ;
      private String aP1_InOutUsuario_PessoaNom ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbUsuario_PessoaTip ;
      private GXCheckbox chkUsuario_EhContador ;
      private GXCheckbox chkUsuario_EhAuditorFM ;
      private GXCheckbox chkUsuario_EhContratada ;
      private GXCheckbox chkUsuario_EhContratante ;
      private GXCheckbox chkUsuario_EhFinanceiro ;
      private GXCheckbox chkUsuario_EhGestor ;
      private GXCheckbox chkUsuario_EhPreposto ;
      private GXCheckbox chkUsuario_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H000J2_A1647Usuario_Email ;
      private bool[] H000J2_n1647Usuario_Email ;
      private bool[] H000J2_A54Usuario_Ativo ;
      private String[] H000J2_A1017Usuario_CrtfPath ;
      private bool[] H000J2_n1017Usuario_CrtfPath ;
      private bool[] H000J2_A1093Usuario_EhPreposto ;
      private bool[] H000J2_A538Usuario_EhGestor ;
      private bool[] H000J2_A293Usuario_EhFinanceiro ;
      private bool[] H000J2_A292Usuario_EhContratante ;
      private bool[] H000J2_A291Usuario_EhContratada ;
      private bool[] H000J2_A289Usuario_EhContador ;
      private String[] H000J2_A341Usuario_UserGamGuid ;
      private String[] H000J2_A325Usuario_PessoaDoc ;
      private bool[] H000J2_n325Usuario_PessoaDoc ;
      private String[] H000J2_A59Usuario_PessoaTip ;
      private bool[] H000J2_n59Usuario_PessoaTip ;
      private String[] H000J2_A58Usuario_PessoaNom ;
      private bool[] H000J2_n58Usuario_PessoaNom ;
      private int[] H000J2_A57Usuario_PessoaCod ;
      private String[] H000J2_A1076Usuario_CargoUONom ;
      private bool[] H000J2_n1076Usuario_CargoUONom ;
      private int[] H000J2_A1075Usuario_CargoUOCod ;
      private bool[] H000J2_n1075Usuario_CargoUOCod ;
      private String[] H000J2_A1074Usuario_CargoNom ;
      private bool[] H000J2_n1074Usuario_CargoNom ;
      private int[] H000J2_A1073Usuario_CargoCod ;
      private bool[] H000J2_n1073Usuario_CargoCod ;
      private int[] H000J2_A1Usuario_Codigo ;
      private long[] H000J3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class promptusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000J2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV30Usuario_PessoaNom1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV73Usuario_CargoNom1 ,
                                             String AV57Usuario_PessoaDoc1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV31Usuario_PessoaNom2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV75Usuario_CargoNom2 ,
                                             String AV58Usuario_PessoaDoc2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV32Usuario_PessoaNom3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV77Usuario_CargoNom3 ,
                                             String AV59Usuario_PessoaDoc3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [17] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Usuario_Email], T1.[Usuario_Ativo], T1.[Usuario_CrtfPath], T1.[Usuario_EhPreposto], T1.[Usuario_EhGestor], T1.[Usuario_EhFinanceiro], T1.[Usuario_EhContratante], T1.[Usuario_EhContratada], T1.[Usuario_EhContador], T1.[Usuario_UserGamGuid], T2.[Pessoa_Docto] AS Usuario_PessoaDoc, T2.[Pessoa_TipoPessoa] AS Usuario_PessoaTip, T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, T3.[Cargo_Nome] AS Usuario_CargoNom, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Codigo]";
         sFromString = " FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV30Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV30Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_CargoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV73Usuario_CargoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV73Usuario_CargoNom1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_CargoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV73Usuario_CargoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV73Usuario_CargoNom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Usuario_PessoaDoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV57Usuario_PessoaDoc1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV57Usuario_PessoaDoc1 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV31Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV31Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Usuario_CargoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV75Usuario_CargoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV75Usuario_CargoNom2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Usuario_CargoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV75Usuario_CargoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV75Usuario_CargoNom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Usuario_PessoaDoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV58Usuario_PessoaDoc2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV58Usuario_PessoaDoc2 + '%')";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV32Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV32Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Usuario_CargoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV77Usuario_CargoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV77Usuario_CargoNom3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Usuario_CargoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV77Usuario_CargoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV77Usuario_CargoNom3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Usuario_PessoaDoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV59Usuario_PessoaDoc3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV59Usuario_PessoaDoc3 + '%')";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( AV13OrderedBy == 2 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Cargo_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Cargo_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_TipoPessoa]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_TipoPessoa] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_UserGamGuid]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_UserGamGuid] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContador]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContador] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContratada]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContratada] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContratante]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhContratante] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhFinanceiro]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhFinanceiro] DESC";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhGestor]";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhGestor] DESC";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhPreposto]";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_EhPreposto] DESC";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_CrtfPath]";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_CrtfPath] DESC";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Ativo]";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Ativo] DESC";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Email]";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Email] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H000J3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV30Usuario_PessoaNom1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV73Usuario_CargoNom1 ,
                                             String AV57Usuario_PessoaDoc1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV31Usuario_PessoaNom2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV75Usuario_CargoNom2 ,
                                             String AV58Usuario_PessoaDoc2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV32Usuario_PessoaNom3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV77Usuario_CargoNom3 ,
                                             String AV59Usuario_PessoaDoc3 ,
                                             String A58Usuario_PessoaNom ,
                                             String A1074Usuario_CargoNom ,
                                             String A325Usuario_PessoaDoc ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Usuario_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV30Usuario_PessoaNom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV30Usuario_PessoaNom1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_CargoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV73Usuario_CargoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV73Usuario_CargoNom1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_CARGONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Usuario_CargoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV73Usuario_CargoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV73Usuario_CargoNom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Usuario_PessoaDoc1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV57Usuario_PessoaDoc1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV57Usuario_PessoaDoc1 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Usuario_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV31Usuario_PessoaNom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV31Usuario_PessoaNom2 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Usuario_CargoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV75Usuario_CargoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV75Usuario_CargoNom2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_CARGONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Usuario_CargoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV75Usuario_CargoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV75Usuario_CargoNom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58Usuario_PessoaDoc2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV58Usuario_PessoaDoc2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV58Usuario_PessoaDoc2 + '%')";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Usuario_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV32Usuario_PessoaNom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV32Usuario_PessoaNom3 + '%')";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Usuario_CargoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like @lV77Usuario_CargoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like @lV77Usuario_CargoNom3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_CARGONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Usuario_CargoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Cargo_Nome] like '%' + @lV77Usuario_CargoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Cargo_Nome] like '%' + @lV77Usuario_CargoNom3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "USUARIO_PESSOADOC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Usuario_PessoaDoc3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV59Usuario_PessoaDoc3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like '%' + @lV59Usuario_PessoaDoc3 + '%')";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 13 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 14 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 15 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 16 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 17 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000J2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H000J3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000J2 ;
          prmH000J2 = new Object[] {
          new Object[] {"@lV30Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV73Usuario_CargoNom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV73Usuario_CargoNom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV57Usuario_PessoaDoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV31Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV75Usuario_CargoNom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV75Usuario_CargoNom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV58Usuario_PessoaDoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV32Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV77Usuario_CargoNom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV77Usuario_CargoNom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV59Usuario_PessoaDoc3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000J3 ;
          prmH000J3 = new Object[] {
          new Object[] {"@lV30Usuario_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV73Usuario_CargoNom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV73Usuario_CargoNom1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV57Usuario_PessoaDoc1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV31Usuario_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV75Usuario_CargoNom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV75Usuario_CargoNom2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV58Usuario_PessoaDoc2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV32Usuario_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV77Usuario_CargoNom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV77Usuario_CargoNom3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV59Usuario_PessoaDoc3",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000J2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000J2,11,0,true,false )
             ,new CursorDef("H000J3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000J3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.getBool(8) ;
                ((bool[]) buf[10])[0] = rslt.getBool(9) ;
                ((String[]) buf[11])[0] = rslt.getString(10, 40) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                ((String[]) buf[14])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                ((String[]) buf[16])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((int[]) buf[18])[0] = rslt.getInt(14) ;
                ((String[]) buf[19])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((String[]) buf[23])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((int[]) buf[25])[0] = rslt.getInt(18) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                ((int[]) buf[27])[0] = rslt.getInt(19) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
