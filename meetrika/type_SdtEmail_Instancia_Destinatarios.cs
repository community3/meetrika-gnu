/*
               File: type_SdtEmail_Instancia_Destinatarios
        Description: Email_Instancia_Destinatarios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:37.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Email_Instancia_Destinatarios" )]
   [XmlType(TypeName =  "Email_Instancia_Destinatarios" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtEmail_Instancia_Destinatarios : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmail_Instancia_Destinatarios( )
      {
         /* Constructor for serialization */
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email = "";
         gxTv_SdtEmail_Instancia_Destinatarios_Mode = "";
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z = "";
      }

      public SdtEmail_Instancia_Destinatarios( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV1666Email_Instancia_Guid ,
                        Guid AV1667Email_Instancia_Destinatarios_Guid )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV1666Email_Instancia_Guid,(Guid)AV1667Email_Instancia_Destinatarios_Guid});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Email_Instancia_Guid", typeof(Guid)}, new Object[]{"Email_Instancia_Destinatarios_Guid", typeof(Guid)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Email_Instancia_Destinatarios");
         metadata.Set("BT", "Email_Instancia_Destinatarios");
         metadata.Set("PK", "[ \"Email_Instancia_Guid\",\"Email_Instancia_Destinatarios_Guid\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Email_Instancia_Guid\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_guid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_destinatarios_guid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_destinatarios_email_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Email_instancia_destinatarios_isenviado_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtEmail_Instancia_Destinatarios deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtEmail_Instancia_Destinatarios)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtEmail_Instancia_Destinatarios obj ;
         obj = this;
         obj.gxTpr_Email_instancia_guid = (Guid)(deserialized.gxTpr_Email_instancia_guid);
         obj.gxTpr_Email_instancia_destinatarios_guid = (Guid)(deserialized.gxTpr_Email_instancia_destinatarios_guid);
         obj.gxTpr_Email_instancia_destinatarios_email = deserialized.gxTpr_Email_instancia_destinatarios_email;
         obj.gxTpr_Email_instancia_destinatarios_isenviado = deserialized.gxTpr_Email_instancia_destinatarios_isenviado;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Email_instancia_guid_Z = (Guid)(deserialized.gxTpr_Email_instancia_guid_Z);
         obj.gxTpr_Email_instancia_destinatarios_guid_Z = (Guid)(deserialized.gxTpr_Email_instancia_destinatarios_guid_Z);
         obj.gxTpr_Email_instancia_destinatarios_email_Z = deserialized.gxTpr_Email_instancia_destinatarios_email_Z;
         obj.gxTpr_Email_instancia_destinatarios_isenviado_Z = deserialized.gxTpr_Email_instancia_destinatarios_isenviado_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Guid") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_Guid") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_Email") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_IsEnviado") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Guid_Z") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_Guid_Z") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_Email_Z") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Email_Instancia_Destinatarios_IsEnviado_Z") )
               {
                  gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Email_Instancia_Destinatarios";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Email_Instancia_Guid", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Destinatarios_Guid", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Destinatarios_Email", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Email_Instancia_Destinatarios_IsEnviado", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtEmail_Instancia_Destinatarios_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Guid_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z.ToString()));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Destinatarios_Guid_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z.ToString()));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Destinatarios_Email_Z", StringUtil.RTrim( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Email_Instancia_Destinatarios_IsEnviado_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Email_Instancia_Guid", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid, false);
         AddObjectProperty("Email_Instancia_Destinatarios_Guid", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid, false);
         AddObjectProperty("Email_Instancia_Destinatarios_Email", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email, false);
         AddObjectProperty("Email_Instancia_Destinatarios_IsEnviado", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtEmail_Instancia_Destinatarios_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtEmail_Instancia_Destinatarios_Initialized, false);
            AddObjectProperty("Email_Instancia_Guid_Z", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z, false);
            AddObjectProperty("Email_Instancia_Destinatarios_Guid_Z", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z, false);
            AddObjectProperty("Email_Instancia_Destinatarios_Email_Z", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z, false);
            AddObjectProperty("Email_Instancia_Destinatarios_IsEnviado_Z", gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Guid" )]
      [  XmlElement( ElementName = "Email_Instancia_Guid"   )]
      public Guid gxTpr_Email_instancia_guid
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid ;
         }

         set {
            if ( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid != value )
            {
               gxTv_SdtEmail_Instancia_Destinatarios_Mode = "INS";
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z_SetNull( );
            }
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_Guid" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_Guid"   )]
      public Guid gxTpr_Email_instancia_destinatarios_guid
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid ;
         }

         set {
            if ( gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid != value )
            {
               gxTv_SdtEmail_Instancia_Destinatarios_Mode = "INS";
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z_SetNull( );
               this.gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z_SetNull( );
            }
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_Email" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_Email"   )]
      public String gxTpr_Email_instancia_destinatarios_email
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_IsEnviado" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_IsEnviado"   )]
      public bool gxTpr_Email_instancia_destinatarios_isenviado
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Mode ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Mode = (String)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Mode_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Mode = "";
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Initialized ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Initialized_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Guid_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Guid_Z"   )]
      public Guid gxTpr_Email_instancia_guid_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z = (Guid)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z = (Guid)(System.Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_Guid_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_Guid_Z"   )]
      public Guid gxTpr_Email_instancia_destinatarios_guid_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z = (Guid)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z = (Guid)(System.Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_Email_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_Email_Z"   )]
      public String gxTpr_Email_instancia_destinatarios_email_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z = (String)(value);
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z = "";
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Email_Instancia_Destinatarios_IsEnviado_Z" )]
      [  XmlElement( ElementName = "Email_Instancia_Destinatarios_IsEnviado_Z"   )]
      public bool gxTpr_Email_instancia_destinatarios_isenviado_Z
      {
         get {
            return gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z ;
         }

         set {
            gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z = value;
         }

      }

      public void gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z_SetNull( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z = false;
         return  ;
      }

      public bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email = "";
         gxTv_SdtEmail_Instancia_Destinatarios_Mode = "";
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z = "";
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z = (Guid)(System.Guid.Empty);
         gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z = (Guid)(System.Guid.Empty);
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "email_instancia_destinatarios", "GeneXus.Programs.email_instancia_destinatarios_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtEmail_Instancia_Destinatarios_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private String gxTv_SdtEmail_Instancia_Destinatarios_Mode ;
      private String sTagName ;
      private bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado ;
      private bool gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_isenviado_Z ;
      private String gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email ;
      private String gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_email_Z ;
      private Guid gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid ;
      private Guid gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid ;
      private Guid gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_guid_Z ;
      private Guid gxTv_SdtEmail_Instancia_Destinatarios_Email_instancia_destinatarios_guid_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Email_Instancia_Destinatarios", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtEmail_Instancia_Destinatarios_RESTInterface : GxGenericCollectionItem<SdtEmail_Instancia_Destinatarios>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmail_Instancia_Destinatarios_RESTInterface( ) : base()
      {
      }

      public SdtEmail_Instancia_Destinatarios_RESTInterface( SdtEmail_Instancia_Destinatarios psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Email_Instancia_Guid" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Email_instancia_guid
      {
         get {
            return sdt.gxTpr_Email_instancia_guid ;
         }

         set {
            sdt.gxTpr_Email_instancia_guid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "Email_Instancia_Destinatarios_Guid" , Order = 1 )]
      [GxSeudo()]
      public Guid gxTpr_Email_instancia_destinatarios_guid
      {
         get {
            return sdt.gxTpr_Email_instancia_destinatarios_guid ;
         }

         set {
            sdt.gxTpr_Email_instancia_destinatarios_guid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "Email_Instancia_Destinatarios_Email" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Email_instancia_destinatarios_email
      {
         get {
            return sdt.gxTpr_Email_instancia_destinatarios_email ;
         }

         set {
            sdt.gxTpr_Email_instancia_destinatarios_email = (String)(value);
         }

      }

      [DataMember( Name = "Email_Instancia_Destinatarios_IsEnviado" , Order = 3 )]
      [GxSeudo()]
      public bool gxTpr_Email_instancia_destinatarios_isenviado
      {
         get {
            return sdt.gxTpr_Email_instancia_destinatarios_isenviado ;
         }

         set {
            sdt.gxTpr_Email_instancia_destinatarios_isenviado = value;
         }

      }

      public SdtEmail_Instancia_Destinatarios sdt
      {
         get {
            return (SdtEmail_Instancia_Destinatarios)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtEmail_Instancia_Destinatarios() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 10 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
