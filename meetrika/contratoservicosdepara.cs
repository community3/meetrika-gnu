/*
               File: ContratoServicosDePara
        Description: Servicos (De Para)
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:35.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosdepara : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A160ContratoServicos_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoServicosDePara_Origem.Name = "CONTRATOSERVICOSDEPARA_ORIGEM";
         cmbContratoServicosDePara_Origem.WebTags = "";
         cmbContratoServicosDePara_Origem.addItem("R", "Redmine", 0);
         if ( cmbContratoServicosDePara_Origem.ItemCount > 0 )
         {
            A1466ContratoServicosDePara_Origem = cmbContratoServicosDePara_Origem.getValidValue(A1466ContratoServicosDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Servicos (De Para)", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoServicos_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosdepara( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosdepara( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoServicosDePara_Origem = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosDePara_Origem.ItemCount > 0 )
         {
            A1466ContratoServicosDePara_Origem = cmbContratoServicosDePara_Origem.getValidValue(A1466ContratoServicosDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3S173( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3S173e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3S173( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3S173( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3S173e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Servicos (De Para)", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContratoServicosDePara.htm");
            wb_table3_28_3S173( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3S173e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3S173e( true) ;
         }
         else
         {
            wb_table1_2_3S173e( false) ;
         }
      }

      protected void wb_table3_28_3S173( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3S173( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3S173e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3S173e( true) ;
         }
         else
         {
            wb_table3_28_3S173e( false) ;
         }
      }

      protected void wb_table4_34_3S173( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigo_Internalname, "C�digo do Servi�o", "", "", lblTextblockcontratoservicos_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), ((edtContratoServicos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicos_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_codigo_Internalname, "Para Codigo", "", "", lblTextblockcontratoservicosdepara_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosDePara_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0, ",", "")), ((edtContratoServicosDePara_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1465ContratoServicosDePara_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1465ContratoServicosDePara_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosDePara_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosDePara_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_origem_Internalname, "Origem", "", "", lblTextblockcontratoservicosdepara_origem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosDePara_Origem, cmbContratoServicosDePara_Origem_Internalname, StringUtil.RTrim( A1466ContratoServicosDePara_Origem), 1, cmbContratoServicosDePara_Origem_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoServicosDePara_Origem.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_ContratoServicosDePara.htm");
            cmbContratoServicosDePara_Origem.CurrentValue = StringUtil.RTrim( A1466ContratoServicosDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosDePara_Origem_Internalname, "Values", (String)(cmbContratoServicosDePara_Origem.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_origenid_Internalname, "Id", "", "", lblTextblockcontratoservicosdepara_origenid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosDePara_OrigenId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0, ",", "")), ((edtContratoServicosDePara_OrigenId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1467ContratoServicosDePara_OrigenId), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1467ContratoServicosDePara_OrigenId), "ZZZZZZZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosDePara_OrigenId_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosDePara_OrigenId_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_origemdsc_Internalname, "Descri��o", "", "", lblTextblockcontratoservicosdepara_origemdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosDePara_OrigemDsc_Internalname, A1468ContratoServicosDePara_OrigemDsc, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtContratoServicosDePara_OrigemDsc_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_origenid2_Internalname, "Id 2", "", "", lblTextblockcontratoservicosdepara_origenid2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosDePara_OrigenId2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0, ",", "")), ((edtContratoServicosDePara_OrigenId2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1469ContratoServicosDePara_OrigenId2), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1469ContratoServicosDePara_OrigenId2), "ZZZZZZZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosDePara_OrigenId2_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoServicosDePara_OrigenId2_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosdepara_origemdsc2_Internalname, "Descri��o", "", "", lblTextblockcontratoservicosdepara_origemdsc2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosDePara_OrigemDsc2_Internalname, A1470ContratoServicosDePara_OrigemDsc2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, 1, edtContratoServicosDePara_OrigemDsc2_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3S173e( true) ;
         }
         else
         {
            wb_table4_34_3S173e( false) ;
         }
      }

      protected void wb_table2_5_3S173( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosDePara.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3S173e( true) ;
         }
         else
         {
            wb_table2_5_3S173e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A160ContratoServicos_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               else
               {
                  A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicos_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSDEPARA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosDePara_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1465ContratoServicosDePara_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
               }
               else
               {
                  A1465ContratoServicosDePara_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosDePara_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
               }
               cmbContratoServicosDePara_Origem.CurrentValue = cgiGet( cmbContratoServicosDePara_Origem_Internalname);
               A1466ContratoServicosDePara_Origem = cgiGet( cmbContratoServicosDePara_Origem_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId_Internalname), ",", ".") > Convert.ToDecimal( 999999999999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSDEPARA_ORIGENID");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosDePara_OrigenId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1467ContratoServicosDePara_OrigenId = 0;
                  n1467ContratoServicosDePara_OrigenId = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0)));
               }
               else
               {
                  A1467ContratoServicosDePara_OrigenId = (long)(context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId_Internalname), ",", "."));
                  n1467ContratoServicosDePara_OrigenId = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0)));
               }
               n1467ContratoServicosDePara_OrigenId = ((0==A1467ContratoServicosDePara_OrigenId) ? true : false);
               A1468ContratoServicosDePara_OrigemDsc = cgiGet( edtContratoServicosDePara_OrigemDsc_Internalname);
               n1468ContratoServicosDePara_OrigemDsc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1468ContratoServicosDePara_OrigemDsc", A1468ContratoServicosDePara_OrigemDsc);
               n1468ContratoServicosDePara_OrigemDsc = (String.IsNullOrEmpty(StringUtil.RTrim( A1468ContratoServicosDePara_OrigemDsc)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId2_Internalname), ",", ".") > Convert.ToDecimal( 999999999999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSDEPARA_ORIGENID2");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosDePara_OrigenId2_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1469ContratoServicosDePara_OrigenId2 = 0;
                  n1469ContratoServicosDePara_OrigenId2 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0)));
               }
               else
               {
                  A1469ContratoServicosDePara_OrigenId2 = (long)(context.localUtil.CToN( cgiGet( edtContratoServicosDePara_OrigenId2_Internalname), ",", "."));
                  n1469ContratoServicosDePara_OrigenId2 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0)));
               }
               n1469ContratoServicosDePara_OrigenId2 = ((0==A1469ContratoServicosDePara_OrigenId2) ? true : false);
               A1470ContratoServicosDePara_OrigemDsc2 = cgiGet( edtContratoServicosDePara_OrigemDsc2_Internalname);
               n1470ContratoServicosDePara_OrigemDsc2 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1470ContratoServicosDePara_OrigemDsc2", A1470ContratoServicosDePara_OrigemDsc2);
               n1470ContratoServicosDePara_OrigemDsc2 = (String.IsNullOrEmpty(StringUtil.RTrim( A1470ContratoServicosDePara_OrigemDsc2)) ? true : false);
               /* Read saved values. */
               Z160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z160ContratoServicos_Codigo"), ",", "."));
               Z1465ContratoServicosDePara_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1465ContratoServicosDePara_Codigo"), ",", "."));
               Z1466ContratoServicosDePara_Origem = cgiGet( "Z1466ContratoServicosDePara_Origem");
               Z1467ContratoServicosDePara_OrigenId = (long)(context.localUtil.CToN( cgiGet( "Z1467ContratoServicosDePara_OrigenId"), ",", "."));
               n1467ContratoServicosDePara_OrigenId = ((0==A1467ContratoServicosDePara_OrigenId) ? true : false);
               Z1469ContratoServicosDePara_OrigenId2 = (long)(context.localUtil.CToN( cgiGet( "Z1469ContratoServicosDePara_OrigenId2"), ",", "."));
               n1469ContratoServicosDePara_OrigenId2 = ((0==A1469ContratoServicosDePara_OrigenId2) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1465ContratoServicosDePara_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3S173( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3S173( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3S0( )
      {
      }

      protected void ZM3S173( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1466ContratoServicosDePara_Origem = T003S3_A1466ContratoServicosDePara_Origem[0];
               Z1467ContratoServicosDePara_OrigenId = T003S3_A1467ContratoServicosDePara_OrigenId[0];
               Z1469ContratoServicosDePara_OrigenId2 = T003S3_A1469ContratoServicosDePara_OrigenId2[0];
            }
            else
            {
               Z1466ContratoServicosDePara_Origem = A1466ContratoServicosDePara_Origem;
               Z1467ContratoServicosDePara_OrigenId = A1467ContratoServicosDePara_OrigenId;
               Z1469ContratoServicosDePara_OrigenId2 = A1469ContratoServicosDePara_OrigenId2;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
            Z1466ContratoServicosDePara_Origem = A1466ContratoServicosDePara_Origem;
            Z1467ContratoServicosDePara_OrigenId = A1467ContratoServicosDePara_OrigenId;
            Z1468ContratoServicosDePara_OrigemDsc = A1468ContratoServicosDePara_OrigemDsc;
            Z1469ContratoServicosDePara_OrigenId2 = A1469ContratoServicosDePara_OrigenId2;
            Z1470ContratoServicosDePara_OrigemDsc2 = A1470ContratoServicosDePara_OrigemDsc2;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3S173( )
      {
         /* Using cursor T003S5 */
         pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound173 = 1;
            A1466ContratoServicosDePara_Origem = T003S5_A1466ContratoServicosDePara_Origem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
            A1467ContratoServicosDePara_OrigenId = T003S5_A1467ContratoServicosDePara_OrigenId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0)));
            n1467ContratoServicosDePara_OrigenId = T003S5_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = T003S5_A1468ContratoServicosDePara_OrigemDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1468ContratoServicosDePara_OrigemDsc", A1468ContratoServicosDePara_OrigemDsc);
            n1468ContratoServicosDePara_OrigemDsc = T003S5_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = T003S5_A1469ContratoServicosDePara_OrigenId2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0)));
            n1469ContratoServicosDePara_OrigenId2 = T003S5_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = T003S5_A1470ContratoServicosDePara_OrigemDsc2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1470ContratoServicosDePara_OrigemDsc2", A1470ContratoServicosDePara_OrigemDsc2);
            n1470ContratoServicosDePara_OrigemDsc2 = T003S5_n1470ContratoServicosDePara_OrigemDsc2[0];
            ZM3S173( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3S173( ) ;
      }

      protected void OnLoadActions3S173( )
      {
      }

      protected void CheckExtendedTable3S173( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003S4 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1466ContratoServicosDePara_Origem, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Origem fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOSDEPARA_ORIGEM");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3S173( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A160ContratoServicos_Codigo )
      {
         /* Using cursor T003S6 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3S173( )
      {
         /* Using cursor T003S7 */
         pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound173 = 1;
         }
         else
         {
            RcdFound173 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003S3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3S173( 2) ;
            RcdFound173 = 1;
            A1465ContratoServicosDePara_Codigo = T003S3_A1465ContratoServicosDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
            A1466ContratoServicosDePara_Origem = T003S3_A1466ContratoServicosDePara_Origem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
            A1467ContratoServicosDePara_OrigenId = T003S3_A1467ContratoServicosDePara_OrigenId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0)));
            n1467ContratoServicosDePara_OrigenId = T003S3_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = T003S3_A1468ContratoServicosDePara_OrigemDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1468ContratoServicosDePara_OrigemDsc", A1468ContratoServicosDePara_OrigemDsc);
            n1468ContratoServicosDePara_OrigemDsc = T003S3_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = T003S3_A1469ContratoServicosDePara_OrigenId2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0)));
            n1469ContratoServicosDePara_OrigenId2 = T003S3_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = T003S3_A1470ContratoServicosDePara_OrigemDsc2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1470ContratoServicosDePara_OrigemDsc2", A1470ContratoServicosDePara_OrigemDsc2);
            n1470ContratoServicosDePara_OrigemDsc2 = T003S3_n1470ContratoServicosDePara_OrigemDsc2[0];
            A160ContratoServicos_Codigo = T003S3_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
            sMode173 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3S173( ) ;
            if ( AnyError == 1 )
            {
               RcdFound173 = 0;
               InitializeNonKey3S173( ) ;
            }
            Gx_mode = sMode173;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound173 = 0;
            InitializeNonKey3S173( ) ;
            sMode173 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode173;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound173 = 0;
         /* Using cursor T003S8 */
         pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003S8_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T003S8_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003S8_A1465ContratoServicosDePara_Codigo[0] < A1465ContratoServicosDePara_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003S8_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T003S8_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003S8_A1465ContratoServicosDePara_Codigo[0] > A1465ContratoServicosDePara_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T003S8_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1465ContratoServicosDePara_Codigo = T003S8_A1465ContratoServicosDePara_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
               RcdFound173 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound173 = 0;
         /* Using cursor T003S9 */
         pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003S9_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T003S9_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003S9_A1465ContratoServicosDePara_Codigo[0] > A1465ContratoServicosDePara_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003S9_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T003S9_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T003S9_A1465ContratoServicosDePara_Codigo[0] < A1465ContratoServicosDePara_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T003S9_A160ContratoServicos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
               A1465ContratoServicosDePara_Codigo = T003S9_A1465ContratoServicosDePara_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
               RcdFound173 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3S173( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3S173( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound173 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1465ContratoServicosDePara_Codigo = Z1465ContratoServicosDePara_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3S173( ) ;
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3S173( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContratoServicos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3S173( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
         {
            A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1465ContratoServicosDePara_Codigo = Z1465ContratoServicosDePara_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3S173( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound173 != 0 )
            {
               ScanNext3S173( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3S173( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3S173( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003S2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosDePara"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1466ContratoServicosDePara_Origem, T003S2_A1466ContratoServicosDePara_Origem[0]) != 0 ) || ( Z1467ContratoServicosDePara_OrigenId != T003S2_A1467ContratoServicosDePara_OrigenId[0] ) || ( Z1469ContratoServicosDePara_OrigenId2 != T003S2_A1469ContratoServicosDePara_OrigenId2[0] ) )
            {
               if ( StringUtil.StrCmp(Z1466ContratoServicosDePara_Origem, T003S2_A1466ContratoServicosDePara_Origem[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosdepara:[seudo value changed for attri]"+"ContratoServicosDePara_Origem");
                  GXUtil.WriteLogRaw("Old: ",Z1466ContratoServicosDePara_Origem);
                  GXUtil.WriteLogRaw("Current: ",T003S2_A1466ContratoServicosDePara_Origem[0]);
               }
               if ( Z1467ContratoServicosDePara_OrigenId != T003S2_A1467ContratoServicosDePara_OrigenId[0] )
               {
                  GXUtil.WriteLog("contratoservicosdepara:[seudo value changed for attri]"+"ContratoServicosDePara_OrigenId");
                  GXUtil.WriteLogRaw("Old: ",Z1467ContratoServicosDePara_OrigenId);
                  GXUtil.WriteLogRaw("Current: ",T003S2_A1467ContratoServicosDePara_OrigenId[0]);
               }
               if ( Z1469ContratoServicosDePara_OrigenId2 != T003S2_A1469ContratoServicosDePara_OrigenId2[0] )
               {
                  GXUtil.WriteLog("contratoservicosdepara:[seudo value changed for attri]"+"ContratoServicosDePara_OrigenId2");
                  GXUtil.WriteLogRaw("Old: ",Z1469ContratoServicosDePara_OrigenId2);
                  GXUtil.WriteLogRaw("Current: ",T003S2_A1469ContratoServicosDePara_OrigenId2[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosDePara"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3S173( )
      {
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3S173( 0) ;
            CheckOptimisticConcurrency3S173( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3S173( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3S173( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003S10 */
                     pr_default.execute(8, new Object[] {A1465ContratoServicosDePara_Codigo, A1466ContratoServicosDePara_Origem, n1467ContratoServicosDePara_OrigenId, A1467ContratoServicosDePara_OrigenId, n1468ContratoServicosDePara_OrigemDsc, A1468ContratoServicosDePara_OrigemDsc, n1469ContratoServicosDePara_OrigenId2, A1469ContratoServicosDePara_OrigenId2, n1470ContratoServicosDePara_OrigemDsc2, A1470ContratoServicosDePara_OrigemDsc2, A160ContratoServicos_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3S0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3S173( ) ;
            }
            EndLevel3S173( ) ;
         }
         CloseExtendedTableCursors3S173( ) ;
      }

      protected void Update3S173( )
      {
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3S173( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3S173( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3S173( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003S11 */
                     pr_default.execute(9, new Object[] {A1466ContratoServicosDePara_Origem, n1467ContratoServicosDePara_OrigenId, A1467ContratoServicosDePara_OrigenId, n1468ContratoServicosDePara_OrigemDsc, A1468ContratoServicosDePara_OrigemDsc, n1469ContratoServicosDePara_OrigenId2, A1469ContratoServicosDePara_OrigenId2, n1470ContratoServicosDePara_OrigemDsc2, A1470ContratoServicosDePara_OrigemDsc2, A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosDePara"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3S173( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3S0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3S173( ) ;
         }
         CloseExtendedTableCursors3S173( ) ;
      }

      protected void DeferredUpdate3S173( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3S173( ) ;
            AfterConfirm3S173( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3S173( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003S12 */
                  pr_default.execute(10, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound173 == 0 )
                        {
                           InitAll3S173( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3S0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode173 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3S173( ) ;
         Gx_mode = sMode173;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3S173( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3S173( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoServicosDePara");
            if ( AnyError == 0 )
            {
               ConfirmValues3S0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoServicosDePara");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3S173( )
      {
         /* Using cursor T003S13 */
         pr_default.execute(11);
         RcdFound173 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound173 = 1;
            A160ContratoServicos_Codigo = T003S13_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1465ContratoServicosDePara_Codigo = T003S13_A1465ContratoServicosDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3S173( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound173 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound173 = 1;
            A160ContratoServicos_Codigo = T003S13_A160ContratoServicos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1465ContratoServicosDePara_Codigo = T003S13_A1465ContratoServicosDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3S173( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3S173( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3S173( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3S173( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3S173( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3S173( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3S173( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3S173( )
      {
         edtContratoServicos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Enabled), 5, 0)));
         edtContratoServicosDePara_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosDePara_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosDePara_Codigo_Enabled), 5, 0)));
         cmbContratoServicosDePara_Origem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosDePara_Origem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosDePara_Origem.Enabled), 5, 0)));
         edtContratoServicosDePara_OrigenId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosDePara_OrigenId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosDePara_OrigenId_Enabled), 5, 0)));
         edtContratoServicosDePara_OrigemDsc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosDePara_OrigemDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosDePara_OrigemDsc_Enabled), 5, 0)));
         edtContratoServicosDePara_OrigenId2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosDePara_OrigenId2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosDePara_OrigenId2_Enabled), 5, 0)));
         edtContratoServicosDePara_OrigemDsc2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosDePara_OrigemDsc2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosDePara_OrigemDsc2_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3S0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311728366");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosdepara.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1465ContratoServicosDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1466ContratoServicosDePara_Origem", StringUtil.RTrim( Z1466ContratoServicosDePara_Origem));
         GxWebStd.gx_hidden_field( context, "Z1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1467ContratoServicosDePara_OrigenId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1469ContratoServicosDePara_OrigenId2), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosdepara.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosDePara" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servicos (De Para)" ;
      }

      protected void InitializeNonKey3S173( )
      {
         A1466ContratoServicosDePara_Origem = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1466ContratoServicosDePara_Origem", A1466ContratoServicosDePara_Origem);
         A1467ContratoServicosDePara_OrigenId = 0;
         n1467ContratoServicosDePara_OrigenId = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1467ContratoServicosDePara_OrigenId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0)));
         n1467ContratoServicosDePara_OrigenId = ((0==A1467ContratoServicosDePara_OrigenId) ? true : false);
         A1468ContratoServicosDePara_OrigemDsc = "";
         n1468ContratoServicosDePara_OrigemDsc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1468ContratoServicosDePara_OrigemDsc", A1468ContratoServicosDePara_OrigemDsc);
         n1468ContratoServicosDePara_OrigemDsc = (String.IsNullOrEmpty(StringUtil.RTrim( A1468ContratoServicosDePara_OrigemDsc)) ? true : false);
         A1469ContratoServicosDePara_OrigenId2 = 0;
         n1469ContratoServicosDePara_OrigenId2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1469ContratoServicosDePara_OrigenId2", StringUtil.LTrim( StringUtil.Str( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0)));
         n1469ContratoServicosDePara_OrigenId2 = ((0==A1469ContratoServicosDePara_OrigenId2) ? true : false);
         A1470ContratoServicosDePara_OrigemDsc2 = "";
         n1470ContratoServicosDePara_OrigemDsc2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1470ContratoServicosDePara_OrigemDsc2", A1470ContratoServicosDePara_OrigemDsc2);
         n1470ContratoServicosDePara_OrigemDsc2 = (String.IsNullOrEmpty(StringUtil.RTrim( A1470ContratoServicosDePara_OrigemDsc2)) ? true : false);
         Z1466ContratoServicosDePara_Origem = "";
         Z1467ContratoServicosDePara_OrigenId = 0;
         Z1469ContratoServicosDePara_OrigenId2 = 0;
      }

      protected void InitAll3S173( )
      {
         A160ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         A1465ContratoServicosDePara_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1465ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0)));
         InitializeNonKey3S173( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117283610");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosdepara.js", "?20203117283610");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontratoservicos_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CODIGO";
         edtContratoServicos_Codigo_Internalname = "CONTRATOSERVICOS_CODIGO";
         lblTextblockcontratoservicosdepara_codigo_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_CODIGO";
         edtContratoServicosDePara_Codigo_Internalname = "CONTRATOSERVICOSDEPARA_CODIGO";
         lblTextblockcontratoservicosdepara_origem_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_ORIGEM";
         cmbContratoServicosDePara_Origem_Internalname = "CONTRATOSERVICOSDEPARA_ORIGEM";
         lblTextblockcontratoservicosdepara_origenid_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_ORIGENID";
         edtContratoServicosDePara_OrigenId_Internalname = "CONTRATOSERVICOSDEPARA_ORIGENID";
         lblTextblockcontratoservicosdepara_origemdsc_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_ORIGEMDSC";
         edtContratoServicosDePara_OrigemDsc_Internalname = "CONTRATOSERVICOSDEPARA_ORIGEMDSC";
         lblTextblockcontratoservicosdepara_origenid2_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_ORIGENID2";
         edtContratoServicosDePara_OrigenId2_Internalname = "CONTRATOSERVICOSDEPARA_ORIGENID2";
         lblTextblockcontratoservicosdepara_origemdsc2_Internalname = "TEXTBLOCKCONTRATOSERVICOSDEPARA_ORIGEMDSC2";
         edtContratoServicosDePara_OrigemDsc2_Internalname = "CONTRATOSERVICOSDEPARA_ORIGEMDSC2";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Servicos (De Para)";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContratoServicosDePara_OrigemDsc2_Enabled = 1;
         edtContratoServicosDePara_OrigenId2_Jsonclick = "";
         edtContratoServicosDePara_OrigenId2_Enabled = 1;
         edtContratoServicosDePara_OrigemDsc_Enabled = 1;
         edtContratoServicosDePara_OrigenId_Jsonclick = "";
         edtContratoServicosDePara_OrigenId_Enabled = 1;
         cmbContratoServicosDePara_Origem_Jsonclick = "";
         cmbContratoServicosDePara_Origem.Enabled = 1;
         edtContratoServicosDePara_Codigo_Jsonclick = "";
         edtContratoServicosDePara_Codigo_Enabled = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003S14 */
         pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(12);
         GX_FocusControl = cmbContratoServicosDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contratoservicos_codigo( int GX_Parm1 )
      {
         A160ContratoServicos_Codigo = GX_Parm1;
         /* Using cursor T003S14 */
         pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContratoServicos_Codigo_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratoservicosdepara_codigo( int GX_Parm1 ,
                                                       int GX_Parm2 ,
                                                       GXCombobox cmbGX_Parm3 ,
                                                       long GX_Parm4 ,
                                                       String GX_Parm5 ,
                                                       long GX_Parm6 ,
                                                       String GX_Parm7 )
      {
         A160ContratoServicos_Codigo = GX_Parm1;
         A1465ContratoServicosDePara_Codigo = GX_Parm2;
         cmbContratoServicosDePara_Origem = cmbGX_Parm3;
         A1466ContratoServicosDePara_Origem = cmbContratoServicosDePara_Origem.CurrentValue;
         cmbContratoServicosDePara_Origem.CurrentValue = A1466ContratoServicosDePara_Origem;
         A1467ContratoServicosDePara_OrigenId = GX_Parm4;
         n1467ContratoServicosDePara_OrigenId = false;
         A1468ContratoServicosDePara_OrigemDsc = GX_Parm5;
         n1468ContratoServicosDePara_OrigemDsc = false;
         A1469ContratoServicosDePara_OrigenId2 = GX_Parm6;
         n1469ContratoServicosDePara_OrigenId2 = false;
         A1470ContratoServicosDePara_OrigemDsc2 = GX_Parm7;
         n1470ContratoServicosDePara_OrigemDsc2 = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbContratoServicosDePara_Origem.CurrentValue = A1466ContratoServicosDePara_Origem;
         isValidOutput.Add(cmbContratoServicosDePara_Origem);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0, ".", "")));
         isValidOutput.Add(A1468ContratoServicosDePara_OrigemDsc);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0, ".", "")));
         isValidOutput.Add(A1470ContratoServicosDePara_OrigemDsc2);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1465ContratoServicosDePara_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1466ContratoServicosDePara_Origem));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1467ContratoServicosDePara_OrigenId), 18, 0, ",", "")));
         isValidOutput.Add(Z1468ContratoServicosDePara_OrigemDsc);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1469ContratoServicosDePara_OrigenId2), 18, 0, ",", "")));
         isValidOutput.Add(Z1470ContratoServicosDePara_OrigemDsc2);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1466ContratoServicosDePara_Origem = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1466ContratoServicosDePara_Origem = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontratoservicos_codigo_Jsonclick = "";
         lblTextblockcontratoservicosdepara_codigo_Jsonclick = "";
         lblTextblockcontratoservicosdepara_origem_Jsonclick = "";
         lblTextblockcontratoservicosdepara_origenid_Jsonclick = "";
         lblTextblockcontratoservicosdepara_origemdsc_Jsonclick = "";
         A1468ContratoServicosDePara_OrigemDsc = "";
         lblTextblockcontratoservicosdepara_origenid2_Jsonclick = "";
         lblTextblockcontratoservicosdepara_origemdsc2_Jsonclick = "";
         A1470ContratoServicosDePara_OrigemDsc2 = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1468ContratoServicosDePara_OrigemDsc = "";
         Z1470ContratoServicosDePara_OrigemDsc2 = "";
         T003S5_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S5_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         T003S5_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         T003S5_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         T003S5_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         T003S5_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         T003S5_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         T003S5_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         T003S5_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         T003S5_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         T003S5_A160ContratoServicos_Codigo = new int[1] ;
         T003S4_A160ContratoServicos_Codigo = new int[1] ;
         T003S6_A160ContratoServicos_Codigo = new int[1] ;
         T003S7_A160ContratoServicos_Codigo = new int[1] ;
         T003S7_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S3_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S3_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         T003S3_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         T003S3_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         T003S3_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         T003S3_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         T003S3_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         T003S3_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         T003S3_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         T003S3_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         T003S3_A160ContratoServicos_Codigo = new int[1] ;
         sMode173 = "";
         T003S8_A160ContratoServicos_Codigo = new int[1] ;
         T003S8_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S9_A160ContratoServicos_Codigo = new int[1] ;
         T003S9_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S2_A1465ContratoServicosDePara_Codigo = new int[1] ;
         T003S2_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         T003S2_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         T003S2_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         T003S2_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         T003S2_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         T003S2_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         T003S2_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         T003S2_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         T003S2_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         T003S2_A160ContratoServicos_Codigo = new int[1] ;
         T003S13_A160ContratoServicos_Codigo = new int[1] ;
         T003S13_A1465ContratoServicosDePara_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T003S14_A160ContratoServicos_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosdepara__default(),
            new Object[][] {
                new Object[] {
               T003S2_A1465ContratoServicosDePara_Codigo, T003S2_A1466ContratoServicosDePara_Origem, T003S2_A1467ContratoServicosDePara_OrigenId, T003S2_n1467ContratoServicosDePara_OrigenId, T003S2_A1468ContratoServicosDePara_OrigemDsc, T003S2_n1468ContratoServicosDePara_OrigemDsc, T003S2_A1469ContratoServicosDePara_OrigenId2, T003S2_n1469ContratoServicosDePara_OrigenId2, T003S2_A1470ContratoServicosDePara_OrigemDsc2, T003S2_n1470ContratoServicosDePara_OrigemDsc2,
               T003S2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003S3_A1465ContratoServicosDePara_Codigo, T003S3_A1466ContratoServicosDePara_Origem, T003S3_A1467ContratoServicosDePara_OrigenId, T003S3_n1467ContratoServicosDePara_OrigenId, T003S3_A1468ContratoServicosDePara_OrigemDsc, T003S3_n1468ContratoServicosDePara_OrigemDsc, T003S3_A1469ContratoServicosDePara_OrigenId2, T003S3_n1469ContratoServicosDePara_OrigenId2, T003S3_A1470ContratoServicosDePara_OrigemDsc2, T003S3_n1470ContratoServicosDePara_OrigemDsc2,
               T003S3_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003S4_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003S5_A1465ContratoServicosDePara_Codigo, T003S5_A1466ContratoServicosDePara_Origem, T003S5_A1467ContratoServicosDePara_OrigenId, T003S5_n1467ContratoServicosDePara_OrigenId, T003S5_A1468ContratoServicosDePara_OrigemDsc, T003S5_n1468ContratoServicosDePara_OrigemDsc, T003S5_A1469ContratoServicosDePara_OrigenId2, T003S5_n1469ContratoServicosDePara_OrigenId2, T003S5_A1470ContratoServicosDePara_OrigemDsc2, T003S5_n1470ContratoServicosDePara_OrigemDsc2,
               T003S5_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003S6_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003S7_A160ContratoServicos_Codigo, T003S7_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               T003S8_A160ContratoServicos_Codigo, T003S8_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               T003S9_A160ContratoServicos_Codigo, T003S9_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003S13_A160ContratoServicos_Codigo, T003S13_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               T003S14_A160ContratoServicos_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound173 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z160ContratoServicos_Codigo ;
      private int Z1465ContratoServicosDePara_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContratoServicos_Codigo_Enabled ;
      private int A1465ContratoServicosDePara_Codigo ;
      private int edtContratoServicosDePara_Codigo_Enabled ;
      private int edtContratoServicosDePara_OrigenId_Enabled ;
      private int edtContratoServicosDePara_OrigemDsc_Enabled ;
      private int edtContratoServicosDePara_OrigenId2_Enabled ;
      private int edtContratoServicosDePara_OrigemDsc2_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private long Z1467ContratoServicosDePara_OrigenId ;
      private long Z1469ContratoServicosDePara_OrigenId2 ;
      private long A1467ContratoServicosDePara_OrigenId ;
      private long A1469ContratoServicosDePara_OrigenId2 ;
      private String sPrefix ;
      private String Z1466ContratoServicosDePara_Origem ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1466ContratoServicosDePara_Origem ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontratoservicos_codigo_Internalname ;
      private String lblTextblockcontratoservicos_codigo_Jsonclick ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicosdepara_codigo_Internalname ;
      private String lblTextblockcontratoservicosdepara_codigo_Jsonclick ;
      private String edtContratoServicosDePara_Codigo_Internalname ;
      private String edtContratoServicosDePara_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicosdepara_origem_Internalname ;
      private String lblTextblockcontratoservicosdepara_origem_Jsonclick ;
      private String cmbContratoServicosDePara_Origem_Internalname ;
      private String cmbContratoServicosDePara_Origem_Jsonclick ;
      private String lblTextblockcontratoservicosdepara_origenid_Internalname ;
      private String lblTextblockcontratoservicosdepara_origenid_Jsonclick ;
      private String edtContratoServicosDePara_OrigenId_Internalname ;
      private String edtContratoServicosDePara_OrigenId_Jsonclick ;
      private String lblTextblockcontratoservicosdepara_origemdsc_Internalname ;
      private String lblTextblockcontratoservicosdepara_origemdsc_Jsonclick ;
      private String edtContratoServicosDePara_OrigemDsc_Internalname ;
      private String lblTextblockcontratoservicosdepara_origenid2_Internalname ;
      private String lblTextblockcontratoservicosdepara_origenid2_Jsonclick ;
      private String edtContratoServicosDePara_OrigenId2_Internalname ;
      private String edtContratoServicosDePara_OrigenId2_Jsonclick ;
      private String lblTextblockcontratoservicosdepara_origemdsc2_Internalname ;
      private String lblTextblockcontratoservicosdepara_origemdsc2_Jsonclick ;
      private String edtContratoServicosDePara_OrigemDsc2_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode173 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1467ContratoServicosDePara_OrigenId ;
      private bool n1468ContratoServicosDePara_OrigemDsc ;
      private bool n1469ContratoServicosDePara_OrigenId2 ;
      private bool n1470ContratoServicosDePara_OrigemDsc2 ;
      private String A1468ContratoServicosDePara_OrigemDsc ;
      private String A1470ContratoServicosDePara_OrigemDsc2 ;
      private String Z1468ContratoServicosDePara_OrigemDsc ;
      private String Z1470ContratoServicosDePara_OrigemDsc2 ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosDePara_Origem ;
      private IDataStoreProvider pr_default ;
      private int[] T003S5_A1465ContratoServicosDePara_Codigo ;
      private String[] T003S5_A1466ContratoServicosDePara_Origem ;
      private long[] T003S5_A1467ContratoServicosDePara_OrigenId ;
      private bool[] T003S5_n1467ContratoServicosDePara_OrigenId ;
      private String[] T003S5_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] T003S5_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] T003S5_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] T003S5_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] T003S5_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] T003S5_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] T003S5_A160ContratoServicos_Codigo ;
      private int[] T003S4_A160ContratoServicos_Codigo ;
      private int[] T003S6_A160ContratoServicos_Codigo ;
      private int[] T003S7_A160ContratoServicos_Codigo ;
      private int[] T003S7_A1465ContratoServicosDePara_Codigo ;
      private int[] T003S3_A1465ContratoServicosDePara_Codigo ;
      private String[] T003S3_A1466ContratoServicosDePara_Origem ;
      private long[] T003S3_A1467ContratoServicosDePara_OrigenId ;
      private bool[] T003S3_n1467ContratoServicosDePara_OrigenId ;
      private String[] T003S3_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] T003S3_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] T003S3_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] T003S3_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] T003S3_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] T003S3_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] T003S3_A160ContratoServicos_Codigo ;
      private int[] T003S8_A160ContratoServicos_Codigo ;
      private int[] T003S8_A1465ContratoServicosDePara_Codigo ;
      private int[] T003S9_A160ContratoServicos_Codigo ;
      private int[] T003S9_A1465ContratoServicosDePara_Codigo ;
      private int[] T003S2_A1465ContratoServicosDePara_Codigo ;
      private String[] T003S2_A1466ContratoServicosDePara_Origem ;
      private long[] T003S2_A1467ContratoServicosDePara_OrigenId ;
      private bool[] T003S2_n1467ContratoServicosDePara_OrigenId ;
      private String[] T003S2_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] T003S2_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] T003S2_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] T003S2_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] T003S2_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] T003S2_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] T003S2_A160ContratoServicos_Codigo ;
      private int[] T003S13_A160ContratoServicos_Codigo ;
      private int[] T003S13_A1465ContratoServicosDePara_Codigo ;
      private int[] T003S14_A160ContratoServicos_Codigo ;
      private GXWebForm Form ;
   }

   public class contratoservicosdepara__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003S5 ;
          prmT003S5 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S4 ;
          prmT003S4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S6 ;
          prmT003S6 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S7 ;
          prmT003S7 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S3 ;
          prmT003S3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S8 ;
          prmT003S8 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S9 ;
          prmT003S9 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S2 ;
          prmT003S2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S10 ;
          prmT003S10 = new Object[] {
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S11 ;
          prmT003S11 = new Object[] {
          new Object[] {"@ContratoServicosDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S12 ;
          prmT003S12 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003S13 ;
          prmT003S13 = new Object[] {
          } ;
          Object[] prmT003S14 ;
          prmT003S14 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003S2", "SELECT [ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo] FROM [ContratoServicosDePara] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003S2,1,0,true,false )
             ,new CursorDef("T003S3", "SELECT [ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003S3,1,0,true,false )
             ,new CursorDef("T003S4", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003S4,1,0,true,false )
             ,new CursorDef("T003S5", "SELECT TM1.[ContratoServicosDePara_Codigo], TM1.[ContratoServicosDePara_Origem], TM1.[ContratoServicosDePara_OrigenId], TM1.[ContratoServicosDePara_OrigemDsc], TM1.[ContratoServicosDePara_OrigenId2], TM1.[ContratoServicosDePara_OrigemDsc2], TM1.[ContratoServicos_Codigo] FROM [ContratoServicosDePara] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003S5,100,0,true,false )
             ,new CursorDef("T003S6", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003S6,1,0,true,false )
             ,new CursorDef("T003S7", "SELECT [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003S7,1,0,true,false )
             ,new CursorDef("T003S8", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] > @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosDePara_Codigo] > @ContratoServicosDePara_Codigo) ORDER BY [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003S8,1,0,true,true )
             ,new CursorDef("T003S9", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] < @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosDePara_Codigo] < @ContratoServicosDePara_Codigo) ORDER BY [ContratoServicos_Codigo] DESC, [ContratoServicosDePara_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003S9,1,0,true,true )
             ,new CursorDef("T003S10", "INSERT INTO [ContratoServicosDePara]([ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo]) VALUES(@ContratoServicosDePara_Codigo, @ContratoServicosDePara_Origem, @ContratoServicosDePara_OrigenId, @ContratoServicosDePara_OrigemDsc, @ContratoServicosDePara_OrigenId2, @ContratoServicosDePara_OrigemDsc2, @ContratoServicos_Codigo)", GxErrorMask.GX_NOMASK,prmT003S10)
             ,new CursorDef("T003S11", "UPDATE [ContratoServicosDePara] SET [ContratoServicosDePara_Origem]=@ContratoServicosDePara_Origem, [ContratoServicosDePara_OrigenId]=@ContratoServicosDePara_OrigenId, [ContratoServicosDePara_OrigemDsc]=@ContratoServicosDePara_OrigemDsc, [ContratoServicosDePara_OrigenId2]=@ContratoServicosDePara_OrigenId2, [ContratoServicosDePara_OrigemDsc2]=@ContratoServicosDePara_OrigemDsc2  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo", GxErrorMask.GX_NOMASK,prmT003S11)
             ,new CursorDef("T003S12", "DELETE FROM [ContratoServicosDePara]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo", GxErrorMask.GX_NOMASK,prmT003S12)
             ,new CursorDef("T003S13", "SELECT [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) ORDER BY [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003S13,100,0,true,false )
             ,new CursorDef("T003S14", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003S14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (long)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (int)parms[10]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (long)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (long)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                stmt.SetParameter(6, (int)parms[9]);
                stmt.SetParameter(7, (int)parms[10]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
