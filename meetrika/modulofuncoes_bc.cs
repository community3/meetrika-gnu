/*
               File: ModuloFuncoes_BC
        Description: Modulo Funcoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:54.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulofuncoes_bc : GXHttpHandler, IGxSilentTrn
   {
      public modulofuncoes_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public modulofuncoes_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1441( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1441( ) ;
         standaloneModal( ) ;
         AddRow1441( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z146Modulo_Codigo = A146Modulo_Codigo;
               Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_140( )
      {
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1441( ) ;
            }
            else
            {
               CheckExtendedTable1441( ) ;
               if ( AnyError == 0 )
               {
                  ZM1441( 2) ;
                  ZM1441( 3) ;
               }
               CloseExtendedTableCursors1441( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1441( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
         }
         if ( GX_JID == -1 )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z144Modulo_Descricao = A144Modulo_Descricao;
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1441( )
      {
         /* Using cursor BC00146 */
         pr_default.execute(4, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound41 = 1;
            A144Modulo_Descricao = BC00146_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC00146_n144Modulo_Descricao[0];
            A162FuncaoUsuario_Nome = BC00146_A162FuncaoUsuario_Nome[0];
            ZM1441( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1441( ) ;
      }

      protected void OnLoadActions1441( )
      {
      }

      protected void CheckExtendedTable1441( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00144 */
         pr_default.execute(2, new Object[] {A146Modulo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
         }
         A144Modulo_Descricao = BC00144_A144Modulo_Descricao[0];
         n144Modulo_Descricao = BC00144_n144Modulo_Descricao[0];
         pr_default.close(2);
         /* Using cursor BC00145 */
         pr_default.execute(3, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
         }
         A162FuncaoUsuario_Nome = BC00145_A162FuncaoUsuario_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1441( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1441( )
      {
         /* Using cursor BC00147 */
         pr_default.execute(5, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound41 = 1;
         }
         else
         {
            RcdFound41 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00143 */
         pr_default.execute(1, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1441( 1) ;
            RcdFound41 = 1;
            A146Modulo_Codigo = BC00143_A146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = BC00143_A161FuncaoUsuario_Codigo[0];
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            sMode41 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1441( ) ;
            if ( AnyError == 1 )
            {
               RcdFound41 = 0;
               InitializeNonKey1441( ) ;
            }
            Gx_mode = sMode41;
         }
         else
         {
            RcdFound41 = 0;
            InitializeNonKey1441( ) ;
            sMode41 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode41;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1441( ) ;
         if ( RcdFound41 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_140( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1441( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00142 */
            pr_default.execute(0, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ModuloFuncoes1"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ModuloFuncoes1"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1441( )
      {
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1441( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1441( 0) ;
            CheckOptimisticConcurrency1441( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1441( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1441( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00148 */
                     pr_default.execute(6, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes1") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1441( ) ;
            }
            EndLevel1441( ) ;
         }
         CloseExtendedTableCursors1441( ) ;
      }

      protected void Update1441( )
      {
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1441( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1441( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1441( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1441( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ModuloFuncoes1] */
                     DeferredUpdate1441( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1441( ) ;
         }
         CloseExtendedTableCursors1441( ) ;
      }

      protected void DeferredUpdate1441( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1441( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1441( ) ;
            AfterConfirm1441( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1441( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00149 */
                  pr_default.execute(7, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes1") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode41 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1441( ) ;
         Gx_mode = sMode41;
      }

      protected void OnDeleteControls1441( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001410 */
            pr_default.execute(8, new Object[] {A146Modulo_Codigo});
            A144Modulo_Descricao = BC001410_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC001410_n144Modulo_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC001411 */
            pr_default.execute(9, new Object[] {A161FuncaoUsuario_Codigo});
            A162FuncaoUsuario_Nome = BC001411_A162FuncaoUsuario_Nome[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel1441( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1441( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1441( )
      {
         /* Using cursor BC001412 */
         pr_default.execute(10, new Object[] {A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         RcdFound41 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound41 = 1;
            A144Modulo_Descricao = BC001412_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC001412_n144Modulo_Descricao[0];
            A162FuncaoUsuario_Nome = BC001412_A162FuncaoUsuario_Nome[0];
            A146Modulo_Codigo = BC001412_A146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001412_A161FuncaoUsuario_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1441( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound41 = 0;
         ScanKeyLoad1441( ) ;
      }

      protected void ScanKeyLoad1441( )
      {
         sMode41 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound41 = 1;
            A144Modulo_Descricao = BC001412_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC001412_n144Modulo_Descricao[0];
            A162FuncaoUsuario_Nome = BC001412_A162FuncaoUsuario_Nome[0];
            A146Modulo_Codigo = BC001412_A146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001412_A161FuncaoUsuario_Codigo[0];
         }
         Gx_mode = sMode41;
      }

      protected void ScanKeyEnd1441( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1441( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1441( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1441( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1441( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1441( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1441( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1441( )
      {
      }

      protected void AddRow1441( )
      {
         VarsToRow41( bcModuloFuncoes) ;
      }

      protected void ReadRow1441( )
      {
         RowToVars41( bcModuloFuncoes, 1) ;
      }

      protected void InitializeNonKey1441( )
      {
         A144Modulo_Descricao = "";
         n144Modulo_Descricao = false;
         A162FuncaoUsuario_Nome = "";
      }

      protected void InitAll1441( )
      {
         A146Modulo_Codigo = 0;
         A161FuncaoUsuario_Codigo = 0;
         InitializeNonKey1441( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow41( SdtModuloFuncoes obj41 )
      {
         obj41.gxTpr_Mode = Gx_mode;
         obj41.gxTpr_Modulo_descricao = A144Modulo_Descricao;
         obj41.gxTpr_Funcaousuario_nome = A162FuncaoUsuario_Nome;
         obj41.gxTpr_Modulo_codigo = A146Modulo_Codigo;
         obj41.gxTpr_Funcaousuario_codigo = A161FuncaoUsuario_Codigo;
         obj41.gxTpr_Modulo_codigo_Z = Z146Modulo_Codigo;
         obj41.gxTpr_Funcaousuario_codigo_Z = Z161FuncaoUsuario_Codigo;
         obj41.gxTpr_Funcaousuario_nome_Z = Z162FuncaoUsuario_Nome;
         obj41.gxTpr_Modulo_descricao_N = (short)(Convert.ToInt16(n144Modulo_Descricao));
         obj41.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow41( SdtModuloFuncoes obj41 )
      {
         obj41.gxTpr_Modulo_codigo = A146Modulo_Codigo;
         obj41.gxTpr_Funcaousuario_codigo = A161FuncaoUsuario_Codigo;
         return  ;
      }

      public void RowToVars41( SdtModuloFuncoes obj41 ,
                               int forceLoad )
      {
         Gx_mode = obj41.gxTpr_Mode;
         A144Modulo_Descricao = obj41.gxTpr_Modulo_descricao;
         n144Modulo_Descricao = false;
         A162FuncaoUsuario_Nome = obj41.gxTpr_Funcaousuario_nome;
         A146Modulo_Codigo = obj41.gxTpr_Modulo_codigo;
         A161FuncaoUsuario_Codigo = obj41.gxTpr_Funcaousuario_codigo;
         Z146Modulo_Codigo = obj41.gxTpr_Modulo_codigo_Z;
         Z161FuncaoUsuario_Codigo = obj41.gxTpr_Funcaousuario_codigo_Z;
         Z162FuncaoUsuario_Nome = obj41.gxTpr_Funcaousuario_nome_Z;
         n144Modulo_Descricao = (bool)(Convert.ToBoolean(obj41.gxTpr_Modulo_descricao_N));
         Gx_mode = obj41.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A146Modulo_Codigo = (int)getParm(obj,0);
         A161FuncaoUsuario_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1441( ) ;
         ScanKeyStart1441( ) ;
         if ( RcdFound41 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001410 */
            pr_default.execute(8, new Object[] {A146Modulo_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
            }
            A144Modulo_Descricao = BC001410_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC001410_n144Modulo_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC001411 */
            pr_default.execute(9, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
               AnyError = 1;
            }
            A162FuncaoUsuario_Nome = BC001411_A162FuncaoUsuario_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         }
         ZM1441( -1) ;
         OnLoadActions1441( ) ;
         AddRow1441( ) ;
         ScanKeyEnd1441( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars41( bcModuloFuncoes, 0) ;
         ScanKeyStart1441( ) ;
         if ( RcdFound41 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001410 */
            pr_default.execute(8, new Object[] {A146Modulo_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
            }
            A144Modulo_Descricao = BC001410_A144Modulo_Descricao[0];
            n144Modulo_Descricao = BC001410_n144Modulo_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC001411 */
            pr_default.execute(9, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
               AnyError = 1;
            }
            A162FuncaoUsuario_Nome = BC001411_A162FuncaoUsuario_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         }
         ZM1441( -1) ;
         OnLoadActions1441( ) ;
         AddRow1441( ) ;
         ScanKeyEnd1441( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars41( bcModuloFuncoes, 0) ;
         nKeyPressed = 1;
         GetKey1441( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1441( ) ;
         }
         else
         {
            if ( RcdFound41 == 1 )
            {
               if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
               {
                  A146Modulo_Codigo = Z146Modulo_Codigo;
                  A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1441( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1441( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1441( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow41( bcModuloFuncoes) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars41( bcModuloFuncoes, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1441( ) ;
         if ( RcdFound41 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
            {
               A146Modulo_Codigo = Z146Modulo_Codigo;
               A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "ModuloFuncoes_BC");
         VarsToRow41( bcModuloFuncoes) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcModuloFuncoes.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcModuloFuncoes.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcModuloFuncoes )
         {
            bcModuloFuncoes = (SdtModuloFuncoes)(sdt);
            if ( StringUtil.StrCmp(bcModuloFuncoes.gxTpr_Mode, "") == 0 )
            {
               bcModuloFuncoes.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow41( bcModuloFuncoes) ;
            }
            else
            {
               RowToVars41( bcModuloFuncoes, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcModuloFuncoes.gxTpr_Mode, "") == 0 )
            {
               bcModuloFuncoes.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars41( bcModuloFuncoes, 1) ;
         return  ;
      }

      public SdtModuloFuncoes ModuloFuncoes_BC
      {
         get {
            return bcModuloFuncoes ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z162FuncaoUsuario_Nome = "";
         A162FuncaoUsuario_Nome = "";
         Z144Modulo_Descricao = "";
         A144Modulo_Descricao = "";
         BC00146_A144Modulo_Descricao = new String[] {""} ;
         BC00146_n144Modulo_Descricao = new bool[] {false} ;
         BC00146_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC00146_A146Modulo_Codigo = new int[1] ;
         BC00146_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00144_A144Modulo_Descricao = new String[] {""} ;
         BC00144_n144Modulo_Descricao = new bool[] {false} ;
         BC00145_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC00147_A146Modulo_Codigo = new int[1] ;
         BC00147_A161FuncaoUsuario_Codigo = new int[1] ;
         BC00143_A146Modulo_Codigo = new int[1] ;
         BC00143_A161FuncaoUsuario_Codigo = new int[1] ;
         sMode41 = "";
         BC00142_A146Modulo_Codigo = new int[1] ;
         BC00142_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001410_A144Modulo_Descricao = new String[] {""} ;
         BC001410_n144Modulo_Descricao = new bool[] {false} ;
         BC001411_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC001412_A144Modulo_Descricao = new String[] {""} ;
         BC001412_n144Modulo_Descricao = new bool[] {false} ;
         BC001412_A162FuncaoUsuario_Nome = new String[] {""} ;
         BC001412_A146Modulo_Codigo = new int[1] ;
         BC001412_A161FuncaoUsuario_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulofuncoes_bc__default(),
            new Object[][] {
                new Object[] {
               BC00142_A146Modulo_Codigo, BC00142_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC00143_A146Modulo_Codigo, BC00143_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC00144_A144Modulo_Descricao, BC00144_n144Modulo_Descricao
               }
               , new Object[] {
               BC00145_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               BC00146_A144Modulo_Descricao, BC00146_n144Modulo_Descricao, BC00146_A162FuncaoUsuario_Nome, BC00146_A146Modulo_Codigo, BC00146_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC00147_A146Modulo_Codigo, BC00147_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001410_A144Modulo_Descricao, BC001410_n144Modulo_Descricao
               }
               , new Object[] {
               BC001411_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               BC001412_A144Modulo_Descricao, BC001412_n144Modulo_Descricao, BC001412_A162FuncaoUsuario_Nome, BC001412_A146Modulo_Codigo, BC001412_A161FuncaoUsuario_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound41 ;
      private int trnEnded ;
      private int Z146Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int Z161FuncaoUsuario_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode41 ;
      private bool n144Modulo_Descricao ;
      private String Z144Modulo_Descricao ;
      private String A144Modulo_Descricao ;
      private String Z162FuncaoUsuario_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private SdtModuloFuncoes bcModuloFuncoes ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00146_A144Modulo_Descricao ;
      private bool[] BC00146_n144Modulo_Descricao ;
      private String[] BC00146_A162FuncaoUsuario_Nome ;
      private int[] BC00146_A146Modulo_Codigo ;
      private int[] BC00146_A161FuncaoUsuario_Codigo ;
      private String[] BC00144_A144Modulo_Descricao ;
      private bool[] BC00144_n144Modulo_Descricao ;
      private String[] BC00145_A162FuncaoUsuario_Nome ;
      private int[] BC00147_A146Modulo_Codigo ;
      private int[] BC00147_A161FuncaoUsuario_Codigo ;
      private int[] BC00143_A146Modulo_Codigo ;
      private int[] BC00143_A161FuncaoUsuario_Codigo ;
      private int[] BC00142_A146Modulo_Codigo ;
      private int[] BC00142_A161FuncaoUsuario_Codigo ;
      private String[] BC001410_A144Modulo_Descricao ;
      private bool[] BC001410_n144Modulo_Descricao ;
      private String[] BC001411_A162FuncaoUsuario_Nome ;
      private String[] BC001412_A144Modulo_Descricao ;
      private bool[] BC001412_n144Modulo_Descricao ;
      private String[] BC001412_A162FuncaoUsuario_Nome ;
      private int[] BC001412_A146Modulo_Codigo ;
      private int[] BC001412_A161FuncaoUsuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class modulofuncoes_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00146 ;
          prmBC00146 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00144 ;
          prmBC00144 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00145 ;
          prmBC00145 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00147 ;
          prmBC00147 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00143 ;
          prmBC00143 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00142 ;
          prmBC00142 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00148 ;
          prmBC00148 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00149 ;
          prmBC00149 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001412 ;
          prmBC001412 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001410 ;
          prmBC001410 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001411 ;
          prmBC001411 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00142", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (UPDLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00142,1,0,true,false )
             ,new CursorDef("BC00143", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00143,1,0,true,false )
             ,new CursorDef("BC00144", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00144,1,0,true,false )
             ,new CursorDef("BC00145", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00145,1,0,true,false )
             ,new CursorDef("BC00146", "SELECT T2.[Modulo_Descricao], T3.[FuncaoUsuario_Nome], TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo] FROM (([ModuloFuncoes1] TM1 WITH (NOLOCK) INNER JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = TM1.[Modulo_Codigo]) INNER JOIN [ModuloFuncoes] T3 WITH (NOLOCK) ON T3.[FuncaoUsuario_Codigo] = TM1.[FuncaoUsuario_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo and TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ORDER BY TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00146,100,0,true,false )
             ,new CursorDef("BC00147", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00147,1,0,true,false )
             ,new CursorDef("BC00148", "INSERT INTO [ModuloFuncoes1]([Modulo_Codigo], [FuncaoUsuario_Codigo]) VALUES(@Modulo_Codigo, @FuncaoUsuario_Codigo)", GxErrorMask.GX_NOMASK,prmBC00148)
             ,new CursorDef("BC00149", "DELETE FROM [ModuloFuncoes1]  WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo", GxErrorMask.GX_NOMASK,prmBC00149)
             ,new CursorDef("BC001410", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001410,1,0,true,false )
             ,new CursorDef("BC001411", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001411,1,0,true,false )
             ,new CursorDef("BC001412", "SELECT T2.[Modulo_Descricao], T3.[FuncaoUsuario_Nome], TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo] FROM (([ModuloFuncoes1] TM1 WITH (NOLOCK) INNER JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = TM1.[Modulo_Codigo]) INNER JOIN [ModuloFuncoes] T3 WITH (NOLOCK) ON T3.[FuncaoUsuario_Codigo] = TM1.[FuncaoUsuario_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo and TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ORDER BY TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001412,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
