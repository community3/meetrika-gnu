/*
               File: PRC_GetParmIndiceDivergenciaDaOs
        Description: Get Parm Indice Divergencia Da Os
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:54.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getparmindicedivergenciadaos : GXProcedure
   {
      public prc_getparmindicedivergenciadaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getparmindicedivergenciadaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out decimal aP1_Contrato_IndiceDivergencia ,
                           out String aP2_Contrato_CalculoDivergencia ,
                           out decimal aP3_ContagemResultado_ValorPF )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12Contrato_IndiceDivergencia = 0 ;
         this.AV11Contrato_CalculoDivergencia = "" ;
         this.AV9ContagemResultado_ValorPF = 0 ;
         initialize();
         executePrivate();
         aP1_Contrato_IndiceDivergencia=this.AV12Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV11Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV9ContagemResultado_ValorPF;
      }

      public decimal executeUdp( int aP0_ContagemResultado_Codigo ,
                                 out decimal aP1_Contrato_IndiceDivergencia ,
                                 out String aP2_Contrato_CalculoDivergencia )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12Contrato_IndiceDivergencia = 0 ;
         this.AV11Contrato_CalculoDivergencia = "" ;
         this.AV9ContagemResultado_ValorPF = 0 ;
         initialize();
         executePrivate();
         aP1_Contrato_IndiceDivergencia=this.AV12Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV11Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV9ContagemResultado_ValorPF;
         return AV9ContagemResultado_ValorPF ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out decimal aP1_Contrato_IndiceDivergencia ,
                                 out String aP2_Contrato_CalculoDivergencia ,
                                 out decimal aP3_ContagemResultado_ValorPF )
      {
         prc_getparmindicedivergenciadaos objprc_getparmindicedivergenciadaos;
         objprc_getparmindicedivergenciadaos = new prc_getparmindicedivergenciadaos();
         objprc_getparmindicedivergenciadaos.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_getparmindicedivergenciadaos.AV12Contrato_IndiceDivergencia = 0 ;
         objprc_getparmindicedivergenciadaos.AV11Contrato_CalculoDivergencia = "" ;
         objprc_getparmindicedivergenciadaos.AV9ContagemResultado_ValorPF = 0 ;
         objprc_getparmindicedivergenciadaos.context.SetSubmitInitialConfig(context);
         objprc_getparmindicedivergenciadaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getparmindicedivergenciadaos);
         aP1_Contrato_IndiceDivergencia=this.AV12Contrato_IndiceDivergencia;
         aP2_Contrato_CalculoDivergencia=this.AV11Contrato_CalculoDivergencia;
         aP3_ContagemResultado_ValorPF=this.AV9ContagemResultado_ValorPF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getparmindicedivergenciadaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008B2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P008B2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008B2_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P008B2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008B2_n1603ContagemResultado_CntCod[0];
            A1617ContagemResultado_CntClcDvr = P008B2_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P008B2_n1617ContagemResultado_CntClcDvr[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P008B2_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P008B2_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1616ContagemResultado_CntVlrUndCnt = P008B2_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P008B2_n1616ContagemResultado_CntVlrUndCnt[0];
            A1601ContagemResultado_CntSrvIndDvr = P008B2_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P008B2_n1601ContagemResultado_CntSrvIndDvr[0];
            A1615ContagemResultado_CntIndDvr = P008B2_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P008B2_n1615ContagemResultado_CntIndDvr[0];
            A1603ContagemResultado_CntCod = P008B2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P008B2_n1603ContagemResultado_CntCod[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P008B2_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P008B2_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1601ContagemResultado_CntSrvIndDvr = P008B2_A1601ContagemResultado_CntSrvIndDvr[0];
            n1601ContagemResultado_CntSrvIndDvr = P008B2_n1601ContagemResultado_CntSrvIndDvr[0];
            A1617ContagemResultado_CntClcDvr = P008B2_A1617ContagemResultado_CntClcDvr[0];
            n1617ContagemResultado_CntClcDvr = P008B2_n1617ContagemResultado_CntClcDvr[0];
            A1616ContagemResultado_CntVlrUndCnt = P008B2_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P008B2_n1616ContagemResultado_CntVlrUndCnt[0];
            A1615ContagemResultado_CntIndDvr = P008B2_A1615ContagemResultado_CntIndDvr[0];
            n1615ContagemResultado_CntIndDvr = P008B2_n1615ContagemResultado_CntIndDvr[0];
            OV11Contrato_CalculoDivergencia = AV11Contrato_CalculoDivergencia;
            OV12Contrato_IndiceDivergencia = AV12Contrato_IndiceDivergencia;
            AV11Contrato_CalculoDivergencia = A1617ContagemResultado_CntClcDvr;
            if ( ( A1597ContagemResultado_CntSrvVlrUndCnt > Convert.ToDecimal( 0 )) )
            {
               AV9ContagemResultado_ValorPF = A1597ContagemResultado_CntSrvVlrUndCnt;
            }
            else
            {
               AV9ContagemResultado_ValorPF = A1616ContagemResultado_CntVlrUndCnt;
            }
            if ( ( A1601ContagemResultado_CntSrvIndDvr > Convert.ToDecimal( 0 )) )
            {
               AV12Contrato_IndiceDivergencia = A1601ContagemResultado_CntSrvIndDvr;
            }
            else
            {
               AV12Contrato_IndiceDivergencia = A1615ContagemResultado_CntIndDvr;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008B2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008B2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008B2_A1603ContagemResultado_CntCod = new int[1] ;
         P008B2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P008B2_A456ContagemResultado_Codigo = new int[1] ;
         P008B2_A1617ContagemResultado_CntClcDvr = new String[] {""} ;
         P008B2_n1617ContagemResultado_CntClcDvr = new bool[] {false} ;
         P008B2_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P008B2_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         P008B2_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         P008B2_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         P008B2_A1601ContagemResultado_CntSrvIndDvr = new decimal[1] ;
         P008B2_n1601ContagemResultado_CntSrvIndDvr = new bool[] {false} ;
         P008B2_A1615ContagemResultado_CntIndDvr = new decimal[1] ;
         P008B2_n1615ContagemResultado_CntIndDvr = new bool[] {false} ;
         A1617ContagemResultado_CntClcDvr = "";
         OV11Contrato_CalculoDivergencia = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getparmindicedivergenciadaos__default(),
            new Object[][] {
                new Object[] {
               P008B2_A1553ContagemResultado_CntSrvCod, P008B2_n1553ContagemResultado_CntSrvCod, P008B2_A1603ContagemResultado_CntCod, P008B2_n1603ContagemResultado_CntCod, P008B2_A456ContagemResultado_Codigo, P008B2_A1617ContagemResultado_CntClcDvr, P008B2_n1617ContagemResultado_CntClcDvr, P008B2_A1597ContagemResultado_CntSrvVlrUndCnt, P008B2_n1597ContagemResultado_CntSrvVlrUndCnt, P008B2_A1616ContagemResultado_CntVlrUndCnt,
               P008B2_n1616ContagemResultado_CntVlrUndCnt, P008B2_A1601ContagemResultado_CntSrvIndDvr, P008B2_n1601ContagemResultado_CntSrvIndDvr, P008B2_A1615ContagemResultado_CntIndDvr, P008B2_n1615ContagemResultado_CntIndDvr
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private decimal AV9ContagemResultado_ValorPF ;
      private decimal A1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal A1616ContagemResultado_CntVlrUndCnt ;
      private decimal A1601ContagemResultado_CntSrvIndDvr ;
      private decimal A1615ContagemResultado_CntIndDvr ;
      private decimal OV12Contrato_IndiceDivergencia ;
      private decimal AV12Contrato_IndiceDivergencia ;
      private String scmdbuf ;
      private String A1617ContagemResultado_CntClcDvr ;
      private String OV11Contrato_CalculoDivergencia ;
      private String AV11Contrato_CalculoDivergencia ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1617ContagemResultado_CntClcDvr ;
      private bool n1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool n1616ContagemResultado_CntVlrUndCnt ;
      private bool n1601ContagemResultado_CntSrvIndDvr ;
      private bool n1615ContagemResultado_CntIndDvr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008B2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008B2_n1553ContagemResultado_CntSrvCod ;
      private int[] P008B2_A1603ContagemResultado_CntCod ;
      private bool[] P008B2_n1603ContagemResultado_CntCod ;
      private int[] P008B2_A456ContagemResultado_Codigo ;
      private String[] P008B2_A1617ContagemResultado_CntClcDvr ;
      private bool[] P008B2_n1617ContagemResultado_CntClcDvr ;
      private decimal[] P008B2_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P008B2_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal[] P008B2_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] P008B2_n1616ContagemResultado_CntVlrUndCnt ;
      private decimal[] P008B2_A1601ContagemResultado_CntSrvIndDvr ;
      private bool[] P008B2_n1601ContagemResultado_CntSrvIndDvr ;
      private decimal[] P008B2_A1615ContagemResultado_CntIndDvr ;
      private bool[] P008B2_n1615ContagemResultado_CntIndDvr ;
      private decimal aP1_Contrato_IndiceDivergencia ;
      private String aP2_Contrato_CalculoDivergencia ;
      private decimal aP3_ContagemResultado_ValorPF ;
   }

   public class prc_getparmindicedivergenciadaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008B2 ;
          prmP008B2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008B2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T3.[Contrato_CalculoDivergencia] AS ContagemResultado_CntClcDvr, T2.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt, T3.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T2.[ContratoServicos_IndiceDivergencia] AS ContagemResultado_CntSrvIndDvr, T3.[Contrato_IndiceDivergencia] AS ContagemResultado_CntIndDvr FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008B2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
