/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:23:51.93
*/
gx.evt.autoSkip = false;
gx.define('wwautorizacaoconsumo', false, function () {
   this.ServerClass =  "wwautorizacaoconsumo" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV90Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV27DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV26DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
   };
   this.Validv_Autorizacaoconsumo_vigenciainicio1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV16AutorizacaoConsumo_VigenciaInicio1)==0) || new gx.date.gxdate( this.AV16AutorizacaoConsumo_VigenciaInicio1 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Autorizacaoconsumo_vigenciainicio_to1=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV17AutorizacaoConsumo_VigenciaInicio_To1)==0) || new gx.date.gxdate( this.AV17AutorizacaoConsumo_VigenciaInicio_To1 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio_To1 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Autorizacaoconsumo_vigenciainicio2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV20AutorizacaoConsumo_VigenciaInicio2)==0) || new gx.date.gxdate( this.AV20AutorizacaoConsumo_VigenciaInicio2 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Autorizacaoconsumo_vigenciainicio_to2=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV21AutorizacaoConsumo_VigenciaInicio_To2)==0) || new gx.date.gxdate( this.AV21AutorizacaoConsumo_VigenciaInicio_To2 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio_To2 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Autorizacaoconsumo_vigenciainicio3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV24AutorizacaoConsumo_VigenciaInicio3)==0) || new gx.date.gxdate( this.AV24AutorizacaoConsumo_VigenciaInicio3 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Autorizacaoconsumo_vigenciainicio_to3=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV25AutorizacaoConsumo_VigenciaInicio_To3)==0) || new gx.date.gxdate( this.AV25AutorizacaoConsumo_VigenciaInicio_To3 ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Autorizacao Consumo_Vigencia Inicio_To3 fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contrato_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(94) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTRATO_CODIGO", gx.fn.currentGridRowImpl(94));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Autorizacaoconsumo_unidademedicaocod=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(94) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", gx.fn.currentGridRowImpl(94));
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfautorizacaoconsumo_vigenciainicio=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV42TFAutorizacaoConsumo_VigenciaInicio)==0) || new gx.date.gxdate( this.AV42TFAutorizacaoConsumo_VigenciaInicio ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFAutorizacao Consumo_Vigencia Inicio fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfautorizacaoconsumo_vigenciainicio_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV43TFAutorizacaoConsumo_VigenciaInicio_To)==0) || new gx.date.gxdate( this.AV43TFAutorizacaoConsumo_VigenciaInicio_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFAutorizacao Consumo_Vigencia Inicio_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_autorizacaoconsumo_vigenciainicioauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate)==0) || new gx.date.gxdate( this.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Autorizacao Consumo_Vigencia Inicio Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_autorizacaoconsumo_vigenciainicioauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo)==0) || new gx.date.gxdate( this.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Autorizacao Consumo_Vigencia Inicio Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfautorizacaoconsumo_vigenciafim=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV48TFAutorizacaoConsumo_VigenciaFim)==0) || new gx.date.gxdate( this.AV48TFAutorizacaoConsumo_VigenciaFim ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFAutorizacao Consumo_Vigencia Fim fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfautorizacaoconsumo_vigenciafim_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV49TFAutorizacaoConsumo_VigenciaFim_To)==0) || new gx.date.gxdate( this.AV49TFAutorizacaoConsumo_VigenciaFim_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFAutorizacao Consumo_Vigencia Fim_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_autorizacaoconsumo_vigenciafimauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate)==0) || new gx.date.gxdate( this.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Autorizacao Consumo_Vigencia Fim Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_autorizacaoconsumo_vigenciafimauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo)==0) || new gx.date.gxdate( this.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Autorizacao Consumo_Vigencia Fim Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "AUTORIZACAOCONSUMO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible", false );
      if ( this.AV19DynamicFiltersSelector2 == "AUTORIZACAOCONSUMO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible", false );
      if ( this.AV23DynamicFiltersSelector3 == "AUTORIZACAOCONSUMO_VIGENCIAINICIO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV13OrderedBy == 2 )
      {
         this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 3 )
      {
         this.DDO_CONTRATO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 1 )
      {
         this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 4 )
      {
         this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 5 )
      {
         this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 6 )
      {
         this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_CONTRATO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus =  ""  ;
      this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus =  ""  ;
      this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus =  ""  ;
      this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV18DynamicFiltersEnabled2 =  false  ;
      this.AV19DynamicFiltersSelector2 =  "AUTORIZACAOCONSUMO_VIGENCIAINICIO"  ;
      this.AV20AutorizacaoConsumo_VigenciaInicio2 =  ''  ;
      this.AV21AutorizacaoConsumo_VigenciaInicio_To2 =  ''  ;
      this.s122_client();
      this.AV22DynamicFiltersEnabled3 =  false  ;
      this.AV23DynamicFiltersSelector3 =  "AUTORIZACAOCONSUMO_VIGENCIAINICIO"  ;
      this.AV24AutorizacaoConsumo_VigenciaInicio3 =  ''  ;
      this.AV25AutorizacaoConsumo_VigenciaInicio_To3 =  ''  ;
      this.s132_client();
   };
   this.e11na2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12na2_client=function()
   {
      this.executeServerEvent("DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13na2_client=function()
   {
      this.executeServerEvent("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14na2_client=function()
   {
      this.executeServerEvent("DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15na2_client=function()
   {
      this.executeServerEvent("DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16na2_client=function()
   {
      this.executeServerEvent("DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17na2_client=function()
   {
      this.executeServerEvent("DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e18na2_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e19na2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e20na2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e21na2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e22na2_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e23na2_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e24na2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e25na2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e26na2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e27na2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e28na2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e32na2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e33na2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,20,21,23,26,28,31,33,35,37,40,42,44,46,47,50,52,54,56,59,61,63,65,66,69,71,73,75,78,80,82,84,85,88,91,95,96,97,98,99,100,101,102,103,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,129,131,133,135,137,139];
   this.GXLastCtrlId =139;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",94,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwautorizacaoconsumo",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(1774,95,"AUTORIZACAOCONSUMO_CODIGO","","","AutorizacaoConsumo_Codigo","int",0,"px",6,6,"right",null,[],1774,"AutorizacaoConsumo_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(74,96,"CONTRATO_CODIGO","","","Contrato_Codigo","int",0,"px",6,6,"right",null,[],74,"Contrato_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(83,97,"CONTRATO_DATAVIGENCIATERMINO","Vigência Término","","Contrato_DataVigenciaTermino","date",0,"px",8,8,"right",null,[],83,"Contrato_DataVigenciaTermino",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(82,98,"CONTRATO_DATAVIGENCIAINICIO","Vigência Inicio","","Contrato_DataVigenciaInicio","date",0,"px",8,8,"right",null,[],82,"Contrato_DataVigenciaInicio",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1775,99,"AUTORIZACAOCONSUMO_VIGENCIAINICIO","","","AutorizacaoConsumo_VigenciaInicio","date",0,"px",8,8,"right",null,[],1775,"AutorizacaoConsumo_VigenciaInicio",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1776,100,"AUTORIZACAOCONSUMO_VIGENCIAFIM","","","AutorizacaoConsumo_VigenciaFim","date",0,"px",8,8,"right",null,[],1776,"AutorizacaoConsumo_VigenciaFim",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1777,101,"AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD","de Medição","","AutorizacaoConsumo_UnidadeMedicaoCod","int",0,"px",6,6,"right",null,[],1777,"AutorizacaoConsumo_UnidadeMedicaoCod",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1778,102,"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM","","","AutorizacaoConsumo_UnidadeMedicaoNom","char",0,"px",50,50,"left",null,[],1778,"AutorizacaoConsumo_UnidadeMedicaoNom",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1779,103,"AUTORIZACAOCONSUMO_QUANTIDADE","","","AutorizacaoConsumo_Quantidade","int",0,"px",3,3,"right",null,[],1779,"AutorizacaoConsumo_Quantidade",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 107, 20, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer = gx.uc.getNew(this, 128, 20, "BootstrapDropDownOptions", "DDO_AUTORIZACAOCONSUMO_CODIGOContainer", "Ddo_autorizacaoconsumo_codigo");
   var DDO_AUTORIZACAOCONSUMO_CODIGOContainer = this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer;
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.addV2CFunction('AV33AutorizacaoConsumo_CodigoTitleFilterData', "vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33AutorizacaoConsumo_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV33AutorizacaoConsumo_CodigoTitleFilterData); });
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_AUTORIZACAOCONSUMO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e12na2_client);
   this.setUserControl(DDO_AUTORIZACAOCONSUMO_CODIGOContainer);
   this.DDO_CONTRATO_CODIGOContainer = gx.uc.getNew(this, 130, 20, "BootstrapDropDownOptions", "DDO_CONTRATO_CODIGOContainer", "Ddo_contrato_codigo");
   var DDO_CONTRATO_CODIGOContainer = this.DDO_CONTRATO_CODIGOContainer;
   DDO_CONTRATO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTRATO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTRATO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTRATO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTRATO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTRATO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTRATO_CODIGOContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_CONTRATO_CODIGOContainer.addV2CFunction('AV37Contrato_CodigoTitleFilterData', "vCONTRATO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37Contrato_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTRATO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV37Contrato_CodigoTitleFilterData); });
   DDO_CONTRATO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTRATO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTRATO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_CONTRATO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTRATO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e13na2_client);
   this.setUserControl(DDO_CONTRATO_CODIGOContainer);
   this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer = gx.uc.getNew(this, 132, 20, "BootstrapDropDownOptions", "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer", "Ddo_autorizacaoconsumo_vigenciainicio");
   var DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer = this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer;
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Icon", "Icon", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Caption", "Caption", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.addV2CFunction('AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData', "vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA",UC.ParentObject.AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setProp("Class", "Class", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.addEventHandler("OnOptionClicked", this.e14na2_client);
   this.setUserControl(DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer);
   this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer = gx.uc.getNew(this, 134, 20, "BootstrapDropDownOptions", "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer", "Ddo_autorizacaoconsumo_vigenciafim");
   var DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer = this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer;
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Icon", "Icon", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Caption", "Caption", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.addV2CFunction('AV47AutorizacaoConsumo_VigenciaFimTitleFilterData', "vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47AutorizacaoConsumo_VigenciaFimTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA",UC.ParentObject.AV47AutorizacaoConsumo_VigenciaFimTitleFilterData); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setProp("Class", "Class", "", "char");
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.addEventHandler("OnOptionClicked", this.e15na2_client);
   this.setUserControl(DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer);
   this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer = gx.uc.getNew(this, 136, 20, "BootstrapDropDownOptions", "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer", "Ddo_autorizacaoconsumo_unidademedicaonom");
   var DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer = this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer;
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Icon", "Icon", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Caption", "Caption", "", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("DataListProc", "Datalistproc", "GetWWAutorizacaoConsumoFilterData", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.addV2CFunction('AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData', "vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA",UC.ParentObject.AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData); });
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setProp("Class", "Class", "", "char");
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.addEventHandler("OnOptionClicked", this.e16na2_client);
   this.setUserControl(DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer);
   this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer = gx.uc.getNew(this, 138, 20, "BootstrapDropDownOptions", "DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer", "Ddo_autorizacaoconsumo_quantidade");
   var DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer = this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer;
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Icon", "Icon", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Caption", "Caption", "", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.addV2CFunction('AV61DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV61DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV61DDO_TitleSettingsIcons); });
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.addV2CFunction('AV57AutorizacaoConsumo_QuantidadeTitleFilterData', "vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV57AutorizacaoConsumo_QuantidadeTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA",UC.ParentObject.AV57AutorizacaoConsumo_QuantidadeTitleFilterData); });
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Visible", "Visible", true, "bool");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setProp("Class", "Class", "", "char");
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.addEventHandler("OnOptionClicked", this.e17na2_client);
   this.setUserControl(DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 106, 20, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV63GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV63GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV63GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV64GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV64GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV64GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11na2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"AUTORIZACAOCONSUMOTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[26]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[28]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[31]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[37]={fld:"TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1",grid:0};
   GXValidFnc[40]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio1,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO1",gxz:"ZV16AutorizacaoConsumo_VigenciaInicio1",gxold:"OV16AutorizacaoConsumo_VigenciaInicio1",gxvar:"AV16AutorizacaoConsumo_VigenciaInicio1",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[40],ip:[40],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV16AutorizacaoConsumo_VigenciaInicio1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16AutorizacaoConsumo_VigenciaInicio1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO1",gx.O.AV16AutorizacaoConsumo_VigenciaInicio1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16AutorizacaoConsumo_VigenciaInicio1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO1")},nac:gx.falseFn};
   GXValidFnc[42]={fld:"DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio_to1,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1",gxz:"ZV17AutorizacaoConsumo_VigenciaInicio_To1",gxold:"OV17AutorizacaoConsumo_VigenciaInicio_To1",gxvar:"AV17AutorizacaoConsumo_VigenciaInicio_To1",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[44],ip:[44],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17AutorizacaoConsumo_VigenciaInicio_To1=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17AutorizacaoConsumo_VigenciaInicio_To1=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1",gx.O.AV17AutorizacaoConsumo_VigenciaInicio_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17AutorizacaoConsumo_VigenciaInicio_To1=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1")},nac:gx.falseFn};
   GXValidFnc[46]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[47]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[50]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[52]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV19DynamicFiltersSelector2",gxold:"OV19DynamicFiltersSelector2",gxvar:"AV19DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV19DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[54]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[56]={fld:"TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2",grid:0};
   GXValidFnc[59]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio2,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO2",gxz:"ZV20AutorizacaoConsumo_VigenciaInicio2",gxold:"OV20AutorizacaoConsumo_VigenciaInicio2",gxvar:"AV20AutorizacaoConsumo_VigenciaInicio2",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[59],ip:[59],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20AutorizacaoConsumo_VigenciaInicio2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20AutorizacaoConsumo_VigenciaInicio2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO2",gx.O.AV20AutorizacaoConsumo_VigenciaInicio2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20AutorizacaoConsumo_VigenciaInicio2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO2")},nac:gx.falseFn};
   GXValidFnc[61]={fld:"DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[63]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio_to2,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2",gxz:"ZV21AutorizacaoConsumo_VigenciaInicio_To2",gxold:"OV21AutorizacaoConsumo_VigenciaInicio_To2",gxvar:"AV21AutorizacaoConsumo_VigenciaInicio_To2",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[63],ip:[63],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21AutorizacaoConsumo_VigenciaInicio_To2=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21AutorizacaoConsumo_VigenciaInicio_To2=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2",gx.O.AV21AutorizacaoConsumo_VigenciaInicio_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21AutorizacaoConsumo_VigenciaInicio_To2=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2")},nac:gx.falseFn};
   GXValidFnc[65]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[66]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[69]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[71]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV23DynamicFiltersSelector3",gxold:"OV23DynamicFiltersSelector3",gxvar:"AV23DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV23DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV23DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[73]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[75]={fld:"TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3",grid:0};
   GXValidFnc[78]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio3,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO3",gxz:"ZV24AutorizacaoConsumo_VigenciaInicio3",gxold:"OV24AutorizacaoConsumo_VigenciaInicio3",gxvar:"AV24AutorizacaoConsumo_VigenciaInicio3",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[78],ip:[78],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24AutorizacaoConsumo_VigenciaInicio3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24AutorizacaoConsumo_VigenciaInicio3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO3",gx.O.AV24AutorizacaoConsumo_VigenciaInicio3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24AutorizacaoConsumo_VigenciaInicio3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO3")},nac:gx.falseFn};
   GXValidFnc[80]={fld:"DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[82]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Autorizacaoconsumo_vigenciainicio_to3,isvalid:null,rgrid:[this.GridContainer],fld:"vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3",gxz:"ZV25AutorizacaoConsumo_VigenciaInicio_To3",gxold:"OV25AutorizacaoConsumo_VigenciaInicio_To3",gxvar:"AV25AutorizacaoConsumo_VigenciaInicio_To3",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[82],ip:[82],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25AutorizacaoConsumo_VigenciaInicio_To3=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25AutorizacaoConsumo_VigenciaInicio_To3=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3",gx.O.AV25AutorizacaoConsumo_VigenciaInicio_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25AutorizacaoConsumo_VigenciaInicio_To3=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3")},nac:gx.falseFn};
   GXValidFnc[84]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[85]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[88]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[91]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[95]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_CODIGO",gxz:"Z1774AutorizacaoConsumo_Codigo",gxold:"O1774AutorizacaoConsumo_Codigo",gxvar:"A1774AutorizacaoConsumo_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1774AutorizacaoConsumo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1774AutorizacaoConsumo_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_CODIGO",row || gx.fn.currentGridRowImpl(94),gx.O.A1774AutorizacaoConsumo_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1774AutorizacaoConsumo_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("AUTORIZACAOCONSUMO_CODIGO",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[96]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:this.Valid_Contrato_codigo,isvalid:null,rgrid:[],fld:"CONTRATO_CODIGO",gxz:"Z74Contrato_Codigo",gxold:"O74Contrato_Codigo",gxvar:"A74Contrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z74Contrato_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),gx.O.A74Contrato_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[97]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_DATAVIGENCIATERMINO",gxz:"Z83Contrato_DataVigenciaTermino",gxold:"O83Contrato_DataVigenciaTermino",gxvar:"A83Contrato_DataVigenciaTermino",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A83Contrato_DataVigenciaTermino=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z83Contrato_DataVigenciaTermino=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_DATAVIGENCIATERMINO",row || gx.fn.currentGridRowImpl(94),gx.O.A83Contrato_DataVigenciaTermino,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A83Contrato_DataVigenciaTermino=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTRATO_DATAVIGENCIATERMINO",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[98]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_DATAVIGENCIAINICIO",gxz:"Z82Contrato_DataVigenciaInicio",gxold:"O82Contrato_DataVigenciaInicio",gxvar:"A82Contrato_DataVigenciaInicio",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A82Contrato_DataVigenciaInicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z82Contrato_DataVigenciaInicio=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_DATAVIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94),gx.O.A82Contrato_DataVigenciaInicio,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A82Contrato_DataVigenciaInicio=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTRATO_DATAVIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[99]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_VIGENCIAINICIO",gxz:"Z1775AutorizacaoConsumo_VigenciaInicio",gxold:"O1775AutorizacaoConsumo_VigenciaInicio",gxvar:"A1775AutorizacaoConsumo_VigenciaInicio",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1775AutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1775AutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_VIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94),gx.O.A1775AutorizacaoConsumo_VigenciaInicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1775AutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("AUTORIZACAOCONSUMO_VIGENCIAINICIO",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[100]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_VIGENCIAFIM",gxz:"Z1776AutorizacaoConsumo_VigenciaFim",gxold:"O1776AutorizacaoConsumo_VigenciaFim",gxvar:"A1776AutorizacaoConsumo_VigenciaFim",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1776AutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1776AutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_VIGENCIAFIM",row || gx.fn.currentGridRowImpl(94),gx.O.A1776AutorizacaoConsumo_VigenciaFim,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1776AutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("AUTORIZACAOCONSUMO_VIGENCIAFIM",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[101]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:this.Valid_Autorizacaoconsumo_unidademedicaocod,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD",gxz:"Z1777AutorizacaoConsumo_UnidadeMedicaoCod",gxold:"O1777AutorizacaoConsumo_UnidadeMedicaoCod",gxvar:"A1777AutorizacaoConsumo_UnidadeMedicaoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1777AutorizacaoConsumo_UnidadeMedicaoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1777AutorizacaoConsumo_UnidadeMedicaoCod=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD",row || gx.fn.currentGridRowImpl(94),gx.O.A1777AutorizacaoConsumo_UnidadeMedicaoCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1777AutorizacaoConsumo_UnidadeMedicaoCod=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[102]={lvl:2,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM",gxz:"Z1778AutorizacaoConsumo_UnidadeMedicaoNom",gxold:"O1778AutorizacaoConsumo_UnidadeMedicaoNom",gxvar:"A1778AutorizacaoConsumo_UnidadeMedicaoNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A1778AutorizacaoConsumo_UnidadeMedicaoNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1778AutorizacaoConsumo_UnidadeMedicaoNom=Value},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM",row || gx.fn.currentGridRowImpl(94),gx.O.A1778AutorizacaoConsumo_UnidadeMedicaoNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1778AutorizacaoConsumo_UnidadeMedicaoNom=this.val()},val:function(row){return gx.fn.getGridControlValue("AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM",row || gx.fn.currentGridRowImpl(94))},nac:gx.falseFn};
   GXValidFnc[103]={lvl:2,type:"int",len:3,dec:0,sign:false,pic:"ZZ9",ro:1,isacc:0,grid:94,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"AUTORIZACAOCONSUMO_QUANTIDADE",gxz:"Z1779AutorizacaoConsumo_Quantidade",gxold:"O1779AutorizacaoConsumo_Quantidade",gxvar:"A1779AutorizacaoConsumo_Quantidade",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1779AutorizacaoConsumo_Quantidade=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1779AutorizacaoConsumo_Quantidade=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("AUTORIZACAOCONSUMO_QUANTIDADE",row || gx.fn.currentGridRowImpl(94),gx.O.A1779AutorizacaoConsumo_Quantidade,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1779AutorizacaoConsumo_Quantidade=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("AUTORIZACAOCONSUMO_QUANTIDADE",row || gx.fn.currentGridRowImpl(94),'.')},nac:gx.falseFn};
   GXValidFnc[108]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV18DynamicFiltersEnabled2",gxold:"OV18DynamicFiltersEnabled2",gxvar:"AV18DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV18DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[109]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV22DynamicFiltersEnabled3",gxold:"OV22DynamicFiltersEnabled3",gxvar:"AV22DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV22DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[110]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_CODIGO",gxz:"ZV34TFAutorizacaoConsumo_Codigo",gxold:"OV34TFAutorizacaoConsumo_Codigo",gxvar:"AV34TFAutorizacaoConsumo_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV34TFAutorizacaoConsumo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV34TFAutorizacaoConsumo_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_CODIGO",gx.O.AV34TFAutorizacaoConsumo_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV34TFAutorizacaoConsumo_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFAUTORIZACAOCONSUMO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[111]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_CODIGO_TO",gxz:"ZV35TFAutorizacaoConsumo_Codigo_To",gxold:"OV35TFAutorizacaoConsumo_Codigo_To",gxvar:"AV35TFAutorizacaoConsumo_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35TFAutorizacaoConsumo_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV35TFAutorizacaoConsumo_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_CODIGO_TO",gx.O.AV35TFAutorizacaoConsumo_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35TFAutorizacaoConsumo_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFAUTORIZACAOCONSUMO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[112]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATO_CODIGO",gxz:"ZV38TFContrato_Codigo",gxold:"OV38TFContrato_Codigo",gxvar:"AV38TFContrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38TFContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38TFContrato_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTRATO_CODIGO",gx.O.AV38TFContrato_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38TFContrato_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTRATO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[113]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATO_CODIGO_TO",gxz:"ZV39TFContrato_Codigo_To",gxold:"OV39TFContrato_Codigo_To",gxvar:"AV39TFContrato_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39TFContrato_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV39TFContrato_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTRATO_CODIGO_TO",gx.O.AV39TFContrato_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39TFContrato_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTRATO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[114]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfautorizacaoconsumo_vigenciainicio,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO",gxz:"ZV42TFAutorizacaoConsumo_VigenciaInicio",gxold:"OV42TFAutorizacaoConsumo_VigenciaInicio",gxvar:"AV42TFAutorizacaoConsumo_VigenciaInicio",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[114],ip:[114],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42TFAutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV42TFAutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO",gx.O.AV42TFAutorizacaoConsumo_VigenciaInicio,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42TFAutorizacaoConsumo_VigenciaInicio=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO")},nac:gx.falseFn};
   GXValidFnc[115]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfautorizacaoconsumo_vigenciainicio_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO",gxz:"ZV43TFAutorizacaoConsumo_VigenciaInicio_To",gxold:"OV43TFAutorizacaoConsumo_VigenciaInicio_To",gxvar:"AV43TFAutorizacaoConsumo_VigenciaInicio_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[115],ip:[115],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43TFAutorizacaoConsumo_VigenciaInicio_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV43TFAutorizacaoConsumo_VigenciaInicio_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO",gx.O.AV43TFAutorizacaoConsumo_VigenciaInicio_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43TFAutorizacaoConsumo_VigenciaInicio_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO")},nac:gx.falseFn};
   GXValidFnc[116]={fld:"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATES",grid:0};
   GXValidFnc[117]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_autorizacaoconsumo_vigenciainicioauxdate,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE",gxz:"ZV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate",gxold:"OV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate",gxvar:"AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[117],ip:[117],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE",gx.O.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[118]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_autorizacaoconsumo_vigenciainicioauxdateto,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO",gxz:"ZV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo",gxold:"OV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo",gxvar:"AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[118],ip:[118],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO",gx.O.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[119]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfautorizacaoconsumo_vigenciafim,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_VIGENCIAFIM",gxz:"ZV48TFAutorizacaoConsumo_VigenciaFim",gxold:"OV48TFAutorizacaoConsumo_VigenciaFim",gxvar:"AV48TFAutorizacaoConsumo_VigenciaFim",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[119],ip:[119],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV48TFAutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV48TFAutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM",gx.O.AV48TFAutorizacaoConsumo_VigenciaFim,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV48TFAutorizacaoConsumo_VigenciaFim=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM")},nac:gx.falseFn};
   GXValidFnc[120]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfautorizacaoconsumo_vigenciafim_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO",gxz:"ZV49TFAutorizacaoConsumo_VigenciaFim_To",gxold:"OV49TFAutorizacaoConsumo_VigenciaFim_To",gxvar:"AV49TFAutorizacaoConsumo_VigenciaFim_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[120],ip:[120],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV49TFAutorizacaoConsumo_VigenciaFim_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV49TFAutorizacaoConsumo_VigenciaFim_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO",gx.O.AV49TFAutorizacaoConsumo_VigenciaFim_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV49TFAutorizacaoConsumo_VigenciaFim_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO")},nac:gx.falseFn};
   GXValidFnc[121]={fld:"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATES",grid:0};
   GXValidFnc[122]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_autorizacaoconsumo_vigenciafimauxdate,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE",gxz:"ZV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate",gxold:"OV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate",gxvar:"AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[122],ip:[122],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE",gx.O.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE")},nac:gx.falseFn};
   GXValidFnc[123]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_autorizacaoconsumo_vigenciafimauxdateto,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO",gxz:"ZV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo",gxold:"OV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo",gxvar:"AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[123],ip:[123],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO",gx.O.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[124]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM",gxz:"ZV54TFAutorizacaoConsumo_UnidadeMedicaoNom",gxold:"OV54TFAutorizacaoConsumo_UnidadeMedicaoNom",gxvar:"AV54TFAutorizacaoConsumo_UnidadeMedicaoNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV54TFAutorizacaoConsumo_UnidadeMedicaoNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV54TFAutorizacaoConsumo_UnidadeMedicaoNom=Value},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM",gx.O.AV54TFAutorizacaoConsumo_UnidadeMedicaoNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV54TFAutorizacaoConsumo_UnidadeMedicaoNom=this.val()},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM")},nac:gx.falseFn};
   GXValidFnc[125]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL",gxz:"ZV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",gxold:"OV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",gxvar:"AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL",gx.O.AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL")},nac:gx.falseFn};
   GXValidFnc[126]={lvl:0,type:"int",len:3,dec:0,sign:false,pic:"ZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_QUANTIDADE",gxz:"ZV58TFAutorizacaoConsumo_Quantidade",gxold:"OV58TFAutorizacaoConsumo_Quantidade",gxvar:"AV58TFAutorizacaoConsumo_Quantidade",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV58TFAutorizacaoConsumo_Quantidade=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV58TFAutorizacaoConsumo_Quantidade=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_QUANTIDADE",gx.O.AV58TFAutorizacaoConsumo_Quantidade,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV58TFAutorizacaoConsumo_Quantidade=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFAUTORIZACAOCONSUMO_QUANTIDADE",'.')},nac:gx.falseFn};
   GXValidFnc[127]={lvl:0,type:"int",len:3,dec:0,sign:false,pic:"ZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO",gxz:"ZV59TFAutorizacaoConsumo_Quantidade_To",gxold:"OV59TFAutorizacaoConsumo_Quantidade_To",gxvar:"AV59TFAutorizacaoConsumo_Quantidade_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV59TFAutorizacaoConsumo_Quantidade_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV59TFAutorizacaoConsumo_Quantidade_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO",gx.O.AV59TFAutorizacaoConsumo_Quantidade_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV59TFAutorizacaoConsumo_Quantidade_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO",'.')},nac:gx.falseFn};
   GXValidFnc[129]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace",gxold:"OV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace",gxvar:"AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[131]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV40ddo_Contrato_CodigoTitleControlIdToReplace",gxold:"OV40ddo_Contrato_CodigoTitleControlIdToReplace",gxvar:"AV40ddo_Contrato_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40ddo_Contrato_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40ddo_Contrato_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[133]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE",gxz:"ZV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace",gxold:"OV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace",gxvar:"AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE",gx.O.AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[135]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE",gxz:"ZV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace",gxold:"OV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace",gxvar:"AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE",gx.O.AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[137]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE",gxz:"ZV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace",gxold:"OV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace",gxvar:"AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE",gx.O.AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[139]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE",gxz:"ZV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace",gxold:"OV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace",gxvar:"AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE",gx.O.AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16AutorizacaoConsumo_VigenciaInicio1 = gx.date.nullDate() ;
   this.ZV16AutorizacaoConsumo_VigenciaInicio1 = gx.date.nullDate() ;
   this.OV16AutorizacaoConsumo_VigenciaInicio1 = gx.date.nullDate() ;
   this.AV17AutorizacaoConsumo_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.ZV17AutorizacaoConsumo_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.OV17AutorizacaoConsumo_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.ZV19DynamicFiltersSelector2 = "" ;
   this.OV19DynamicFiltersSelector2 = "" ;
   this.AV20AutorizacaoConsumo_VigenciaInicio2 = gx.date.nullDate() ;
   this.ZV20AutorizacaoConsumo_VigenciaInicio2 = gx.date.nullDate() ;
   this.OV20AutorizacaoConsumo_VigenciaInicio2 = gx.date.nullDate() ;
   this.AV21AutorizacaoConsumo_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.ZV21AutorizacaoConsumo_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.OV21AutorizacaoConsumo_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.ZV23DynamicFiltersSelector3 = "" ;
   this.OV23DynamicFiltersSelector3 = "" ;
   this.AV24AutorizacaoConsumo_VigenciaInicio3 = gx.date.nullDate() ;
   this.ZV24AutorizacaoConsumo_VigenciaInicio3 = gx.date.nullDate() ;
   this.OV24AutorizacaoConsumo_VigenciaInicio3 = gx.date.nullDate() ;
   this.AV25AutorizacaoConsumo_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.ZV25AutorizacaoConsumo_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.OV25AutorizacaoConsumo_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.Z1774AutorizacaoConsumo_Codigo = 0 ;
   this.O1774AutorizacaoConsumo_Codigo = 0 ;
   this.Z74Contrato_Codigo = 0 ;
   this.O74Contrato_Codigo = 0 ;
   this.Z83Contrato_DataVigenciaTermino = gx.date.nullDate() ;
   this.O83Contrato_DataVigenciaTermino = gx.date.nullDate() ;
   this.Z82Contrato_DataVigenciaInicio = gx.date.nullDate() ;
   this.O82Contrato_DataVigenciaInicio = gx.date.nullDate() ;
   this.Z1775AutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.O1775AutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.Z1776AutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.O1776AutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.Z1777AutorizacaoConsumo_UnidadeMedicaoCod = 0 ;
   this.O1777AutorizacaoConsumo_UnidadeMedicaoCod = 0 ;
   this.Z1778AutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.O1778AutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.Z1779AutorizacaoConsumo_Quantidade = 0 ;
   this.O1779AutorizacaoConsumo_Quantidade = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.ZV18DynamicFiltersEnabled2 = false ;
   this.OV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.ZV22DynamicFiltersEnabled3 = false ;
   this.OV22DynamicFiltersEnabled3 = false ;
   this.AV34TFAutorizacaoConsumo_Codigo = 0 ;
   this.ZV34TFAutorizacaoConsumo_Codigo = 0 ;
   this.OV34TFAutorizacaoConsumo_Codigo = 0 ;
   this.AV35TFAutorizacaoConsumo_Codigo_To = 0 ;
   this.ZV35TFAutorizacaoConsumo_Codigo_To = 0 ;
   this.OV35TFAutorizacaoConsumo_Codigo_To = 0 ;
   this.AV38TFContrato_Codigo = 0 ;
   this.ZV38TFContrato_Codigo = 0 ;
   this.OV38TFContrato_Codigo = 0 ;
   this.AV39TFContrato_Codigo_To = 0 ;
   this.ZV39TFContrato_Codigo_To = 0 ;
   this.OV39TFContrato_Codigo_To = 0 ;
   this.AV42TFAutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.ZV42TFAutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.OV42TFAutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.AV43TFAutorizacaoConsumo_VigenciaInicio_To = gx.date.nullDate() ;
   this.ZV43TFAutorizacaoConsumo_VigenciaInicio_To = gx.date.nullDate() ;
   this.OV43TFAutorizacaoConsumo_VigenciaInicio_To = gx.date.nullDate() ;
   this.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.ZV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.OV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.ZV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.OV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.AV48TFAutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.ZV48TFAutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.OV48TFAutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.AV49TFAutorizacaoConsumo_VigenciaFim_To = gx.date.nullDate() ;
   this.ZV49TFAutorizacaoConsumo_VigenciaFim_To = gx.date.nullDate() ;
   this.OV49TFAutorizacaoConsumo_VigenciaFim_To = gx.date.nullDate() ;
   this.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.ZV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.OV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.ZV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.OV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.ZV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.OV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "" ;
   this.ZV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "" ;
   this.OV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "" ;
   this.AV58TFAutorizacaoConsumo_Quantidade = 0 ;
   this.ZV58TFAutorizacaoConsumo_Quantidade = 0 ;
   this.OV58TFAutorizacaoConsumo_Quantidade = 0 ;
   this.AV59TFAutorizacaoConsumo_Quantidade_To = 0 ;
   this.ZV59TFAutorizacaoConsumo_Quantidade_To = 0 ;
   this.OV59TFAutorizacaoConsumo_Quantidade_To = 0 ;
   this.AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "" ;
   this.ZV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "" ;
   this.OV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.ZV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.OV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "" ;
   this.ZV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "" ;
   this.OV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "" ;
   this.AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "" ;
   this.ZV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "" ;
   this.OV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "" ;
   this.AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "" ;
   this.ZV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "" ;
   this.OV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "" ;
   this.AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "" ;
   this.ZV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "" ;
   this.OV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "" ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16AutorizacaoConsumo_VigenciaInicio1 = gx.date.nullDate() ;
   this.AV17AutorizacaoConsumo_VigenciaInicio_To1 = gx.date.nullDate() ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.AV20AutorizacaoConsumo_VigenciaInicio2 = gx.date.nullDate() ;
   this.AV21AutorizacaoConsumo_VigenciaInicio_To2 = gx.date.nullDate() ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.AV24AutorizacaoConsumo_VigenciaInicio3 = gx.date.nullDate() ;
   this.AV25AutorizacaoConsumo_VigenciaInicio_To3 = gx.date.nullDate() ;
   this.AV63GridCurrentPage = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.AV34TFAutorizacaoConsumo_Codigo = 0 ;
   this.AV35TFAutorizacaoConsumo_Codigo_To = 0 ;
   this.AV38TFContrato_Codigo = 0 ;
   this.AV39TFContrato_Codigo_To = 0 ;
   this.AV42TFAutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.AV43TFAutorizacaoConsumo_VigenciaInicio_To = gx.date.nullDate() ;
   this.AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = gx.date.nullDate() ;
   this.AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = gx.date.nullDate() ;
   this.AV48TFAutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.AV49TFAutorizacaoConsumo_VigenciaFim_To = gx.date.nullDate() ;
   this.AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = gx.date.nullDate() ;
   this.AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = gx.date.nullDate() ;
   this.AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "" ;
   this.AV58TFAutorizacaoConsumo_Quantidade = 0 ;
   this.AV59TFAutorizacaoConsumo_Quantidade_To = 0 ;
   this.AV61DDO_TitleSettingsIcons = {} ;
   this.AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_Contrato_CodigoTitleControlIdToReplace = "" ;
   this.AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "" ;
   this.AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "" ;
   this.AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "" ;
   this.AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "" ;
   this.A1774AutorizacaoConsumo_Codigo = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.A83Contrato_DataVigenciaTermino = gx.date.nullDate() ;
   this.A82Contrato_DataVigenciaInicio = gx.date.nullDate() ;
   this.A1775AutorizacaoConsumo_VigenciaInicio = gx.date.nullDate() ;
   this.A1776AutorizacaoConsumo_VigenciaFim = gx.date.nullDate() ;
   this.A1777AutorizacaoConsumo_UnidadeMedicaoCod = 0 ;
   this.A1778AutorizacaoConsumo_UnidadeMedicaoNom = "" ;
   this.A1779AutorizacaoConsumo_Quantidade = 0 ;
   this.AV90Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV27DynamicFiltersIgnoreFirst = false ;
   this.AV26DynamicFiltersRemoving = false ;
   this.Events = {"e11na2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12na2_client": ["DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED", true] ,"e13na2_client": ["DDO_CONTRATO_CODIGO.ONOPTIONCLICKED", true] ,"e14na2_client": ["DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED", true] ,"e15na2_client": ["DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED", true] ,"e16na2_client": ["DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED", true] ,"e17na2_client": ["DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED", true] ,"e18na2_client": ["VORDEREDBY.CLICK", true] ,"e19na2_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e20na2_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e21na2_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e22na2_client": ["'DOCLEANFILTERS'", true] ,"e23na2_client": ["'DOINSERT'", true] ,"e24na2_client": ["'ADDDYNAMICFILTERS1'", true] ,"e25na2_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e26na2_client": ["'ADDDYNAMICFILTERS2'", true] ,"e27na2_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e28na2_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e32na2_client": ["ENTER", true] ,"e33na2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV33AutorizacaoConsumo_CodigoTitleFilterData',fld:'vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47AutorizacaoConsumo_VigenciaFimTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData',fld:'vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV57AutorizacaoConsumo_QuantidadeTitleFilterData',fld:'vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA',pic:'',nv:null},{ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("AUTORIZACAOCONSUMO_CODIGO","Title")',ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Title'},{ctrl:'CONTRATO_CODIGO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATO_CODIGO","Title")',ctrl:'CONTRATO_CODIGO',prop:'Title'},{ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("AUTORIZACAOCONSUMO_VIGENCIAINICIO","Title")',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Title'},{ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("AUTORIZACAOCONSUMO_VIGENCIAFIM","Title")',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Title'},{ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM","Title")',ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Title'},{ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("AUTORIZACAOCONSUMO_QUANTIDADE","Title")',ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Title'},{av:'AV63GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.FilteredText_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]];
   this.EvtParms["DDO_CONTRATO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_CONTRATO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredText_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]];
   this.EvtParms["DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.ActiveEventKey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.FilteredText_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.FilteredTextTo_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]];
   this.EvtParms["DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.ActiveEventKey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.FilteredText_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.FilteredTextTo_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]];
   this.EvtParms["DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.ActiveEventKey',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'ActiveEventKey'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.FilteredText_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'FilteredText_get'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SelectedValue_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]];
   this.EvtParms["DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.ActiveEventKey',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'ActiveEventKey'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.FilteredText_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredText_get'},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.FilteredTextTo_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_CONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SortedStatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[],[]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.FilteredText_set',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredText_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTRATO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.FilteredText_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredText_set'},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer.FilteredTextTo_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredTextTo_set'},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.FilteredText_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredText_set'},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer.FilteredTextTo_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredTextTo_set'},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.FilteredText_set',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'FilteredText_set'},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'this.DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer.SelectedValue_set',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SelectedValue_set'},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.FilteredText_set',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredText_set'},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'this.DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer.FilteredTextTo_set',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'}]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.setVCMap("AV90Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("AV90Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   GridContainer.addRefreshingVar(this.GXValidFnc[20]);
   GridContainer.addRefreshingVar(this.GXValidFnc[21]);
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[44]);
   GridContainer.addRefreshingVar(this.GXValidFnc[52]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[63]);
   GridContainer.addRefreshingVar(this.GXValidFnc[71]);
   GridContainer.addRefreshingVar(this.GXValidFnc[78]);
   GridContainer.addRefreshingVar(this.GXValidFnc[82]);
   GridContainer.addRefreshingVar(this.GXValidFnc[108]);
   GridContainer.addRefreshingVar(this.GXValidFnc[109]);
   GridContainer.addRefreshingVar(this.GXValidFnc[110]);
   GridContainer.addRefreshingVar(this.GXValidFnc[111]);
   GridContainer.addRefreshingVar(this.GXValidFnc[112]);
   GridContainer.addRefreshingVar(this.GXValidFnc[113]);
   GridContainer.addRefreshingVar(this.GXValidFnc[114]);
   GridContainer.addRefreshingVar(this.GXValidFnc[115]);
   GridContainer.addRefreshingVar(this.GXValidFnc[119]);
   GridContainer.addRefreshingVar(this.GXValidFnc[120]);
   GridContainer.addRefreshingVar(this.GXValidFnc[124]);
   GridContainer.addRefreshingVar(this.GXValidFnc[125]);
   GridContainer.addRefreshingVar(this.GXValidFnc[126]);
   GridContainer.addRefreshingVar(this.GXValidFnc[127]);
   GridContainer.addRefreshingVar(this.GXValidFnc[129]);
   GridContainer.addRefreshingVar(this.GXValidFnc[131]);
   GridContainer.addRefreshingVar(this.GXValidFnc[133]);
   GridContainer.addRefreshingVar(this.GXValidFnc[135]);
   GridContainer.addRefreshingVar(this.GXValidFnc[137]);
   GridContainer.addRefreshingVar(this.GXValidFnc[139]);
   GridContainer.addRefreshingVar({rfrVar:"AV90Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV27DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV26DynamicFiltersRemoving"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwautorizacaoconsumo);
