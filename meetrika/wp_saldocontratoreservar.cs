/*
               File: WP_SaldoContratoReservar
        Description: Reservar Saldo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:23:31.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_saldocontratoreservar : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_saldocontratoreservar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_saldocontratoreservar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_SaldoContrato_Codigo ,
                           ref int aP1_Contrato_Codigo ,
                           ref int aP2_NotaEmpenho_Codigo ,
                           ref int aP3_Contagem_Codigo ,
                           ref decimal aP4_SaldoContrato_Credito ,
                           ref String aP5_tipo )
      {
         this.AV12SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         this.AV16Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV17NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         this.AV18Contagem_Codigo = aP3_Contagem_Codigo;
         this.AV15SaldoContrato_Credito = aP4_SaldoContrato_Credito;
         this.AV13tipo = aP5_tipo;
         executePrivate();
         aP0_SaldoContrato_Codigo=this.AV12SaldoContrato_Codigo;
         aP1_Contrato_Codigo=this.AV16Contrato_Codigo;
         aP2_NotaEmpenho_Codigo=this.AV17NotaEmpenho_Codigo;
         aP3_Contagem_Codigo=this.AV18Contagem_Codigo;
         aP4_SaldoContrato_Credito=this.AV15SaldoContrato_Credito;
         aP5_tipo=this.AV13tipo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV12SaldoContrato_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12SaldoContrato_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV16Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Contrato_Codigo), 6, 0)));
                  AV17NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17NotaEmpenho_Codigo), 6, 0)));
                  AV18Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Contagem_Codigo), 6, 0)));
                  AV15SaldoContrato_Credito = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV15SaldoContrato_Credito, 18, 5)));
                  AV13tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13tipo", AV13tipo);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMR2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMR2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122123313");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_saldocontratoreservar.aspx") + "?" + UrlEncode("" +AV12SaldoContrato_Codigo) + "," + UrlEncode("" +AV16Contrato_Codigo) + "," + UrlEncode("" +AV17NotaEmpenho_Codigo) + "," + UrlEncode("" +AV18Contagem_Codigo) + "," + UrlEncode(StringUtil.Str(AV15SaldoContrato_Credito,18,5)) + "," + UrlEncode(StringUtil.RTrim(AV13tipo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV11CheckRequiredFieldsResult);
         GxWebStd.gx_hidden_field( context, "vTIPO", StringUtil.RTrim( AV13tipo));
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CREDITO", StringUtil.LTrim( StringUtil.NToC( AV15SaldoContrato_Credito, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Contagem_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMESSAGES", AV6Messages);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMESSAGES", AV6Messages);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO_DISPONIVEL", GetSecureSignedToken( "", context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Width", StringUtil.RTrim( Dvpanel_tablereservar_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Cls", StringUtil.RTrim( Dvpanel_tablereservar_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Title", StringUtil.RTrim( Dvpanel_tablereservar_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Collapsible", StringUtil.BoolToStr( Dvpanel_tablereservar_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Collapsed", StringUtil.BoolToStr( Dvpanel_tablereservar_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Autowidth", StringUtil.BoolToStr( Dvpanel_tablereservar_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Autoheight", StringUtil.BoolToStr( Dvpanel_tablereservar_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tablereservar_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Iconposition", StringUtil.RTrim( Dvpanel_tablereservar_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLERESERVAR_Autoscroll", StringUtil.BoolToStr( Dvpanel_tablereservar_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_SaldoContratoReservar";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_saldocontratoreservar:[SendSecurityCheck value for]"+"SaldoContrato_Saldo:"+context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("wp_saldocontratoreservar:[SendSecurityCheck value for]"+"SaldoContrato_Reservado:"+context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("wp_saldocontratoreservar:[SendSecurityCheck value for]"+"SaldoContrato_Disponivel:"+context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMR2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMR2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_saldocontratoreservar.aspx") + "?" + UrlEncode("" +AV12SaldoContrato_Codigo) + "," + UrlEncode("" +AV16Contrato_Codigo) + "," + UrlEncode("" +AV17NotaEmpenho_Codigo) + "," + UrlEncode("" +AV18Contagem_Codigo) + "," + UrlEncode(StringUtil.Str(AV15SaldoContrato_Credito,18,5)) + "," + UrlEncode(StringUtil.RTrim(AV13tipo)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_SaldoContratoReservar" ;
      }

      public override String GetPgmdesc( )
      {
         return "Reservar Saldo" ;
      }

      protected void WBMR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MR2( true) ;
         }
         else
         {
            wb_table1_2_MR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MR2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Reservar Saldo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMR0( ) ;
      }

      protected void WSMR2( )
      {
         STARTMR2( ) ;
         EVTMR2( ) ;
      }

      protected void EVTMR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MR2 */
                              E11MR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12MR2 */
                                    E12MR2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MR2 */
                              E13MR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14MR2 */
                              E14MR2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSaldocontrato_saldo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_saldo_Enabled), 5, 0)));
         edtavSaldocontrato_reservado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_reservado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_reservado_Enabled), 5, 0)));
         edtavSaldocontrato_disponivel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_disponivel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_disponivel_Enabled), 5, 0)));
      }

      protected void RFMR2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13MR2 */
         E13MR2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14MR2 */
            E14MR2 ();
            WBMR0( ) ;
         }
      }

      protected void STRUPMR0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_saldo_Enabled), 5, 0)));
         edtavSaldocontrato_reservado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_reservado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_reservado_Enabled), 5, 0)));
         edtavSaldocontrato_disponivel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_disponivel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_disponivel_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MR2 */
         E11MR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_SALDO");
               GX_FocusControl = edtavSaldocontrato_saldo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SaldoContrato_Saldo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV9SaldoContrato_Saldo, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV9SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV9SaldoContrato_Saldo, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_reservado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_reservado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_RESERVADO");
               GX_FocusControl = edtavSaldocontrato_reservado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10SaldoContrato_Reservado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV10SaldoContrato_Reservado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV10SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtavSaldocontrato_reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV10SaldoContrato_Reservado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_disponivel_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_disponivel_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_DISPONIVEL");
               GX_FocusControl = edtavSaldocontrato_disponivel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14SaldoContrato_Disponivel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Disponivel", StringUtil.LTrim( StringUtil.Str( AV14SaldoContrato_Disponivel, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_DISPONIVEL", GetSecureSignedToken( "", context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV14SaldoContrato_Disponivel = context.localUtil.CToN( cgiGet( edtavSaldocontrato_disponivel_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Disponivel", StringUtil.LTrim( StringUtil.Str( AV14SaldoContrato_Disponivel, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_DISPONIVEL", GetSecureSignedToken( "", context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAuxsaldocontrato_reservado_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavAuxsaldocontrato_reservado_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAUXSALDOCONTRATO_RESERVADO");
               GX_FocusControl = edtavAuxsaldocontrato_reservado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8AuxSaldoContrato_Reservado = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AuxSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV8AuxSaldoContrato_Reservado, 18, 5)));
            }
            else
            {
               AV8AuxSaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtavAuxsaldocontrato_reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AuxSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV8AuxSaldoContrato_Reservado, 18, 5)));
            }
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            Dvpanel_tablereservar_Width = cgiGet( "DVPANEL_TABLERESERVAR_Width");
            Dvpanel_tablereservar_Cls = cgiGet( "DVPANEL_TABLERESERVAR_Cls");
            Dvpanel_tablereservar_Title = cgiGet( "DVPANEL_TABLERESERVAR_Title");
            Dvpanel_tablereservar_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Collapsible"));
            Dvpanel_tablereservar_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Collapsed"));
            Dvpanel_tablereservar_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Autowidth"));
            Dvpanel_tablereservar_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Autoheight"));
            Dvpanel_tablereservar_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Showcollapseicon"));
            Dvpanel_tablereservar_Iconposition = cgiGet( "DVPANEL_TABLERESERVAR_Iconposition");
            Dvpanel_tablereservar_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLERESERVAR_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_SaldoContratoReservar";
            AV9SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV9SaldoContrato_Saldo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99");
            AV10SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtavSaldocontrato_reservado_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV10SaldoContrato_Reservado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
            AV14SaldoContrato_Disponivel = context.localUtil.CToN( cgiGet( edtavSaldocontrato_disponivel_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Disponivel", StringUtil.LTrim( StringUtil.Str( AV14SaldoContrato_Disponivel, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_DISPONIVEL", GetSecureSignedToken( "", context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_saldocontratoreservar:[SecurityCheckFailed value for]"+"SaldoContrato_Saldo:"+context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GXUtil.WriteLog("wp_saldocontratoreservar:[SecurityCheckFailed value for]"+"SaldoContrato_Reservado:"+context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GXUtil.WriteLog("wp_saldocontratoreservar:[SecurityCheckFailed value for]"+"SaldoContrato_Disponivel:"+context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MR2 */
         E11MR2 ();
         if (returnInSub) return;
      }

      protected void E11MR2( )
      {
         /* Start Routine */
         if ( StringUtil.StrCmp(AV13tipo, "RSV") == 0 )
         {
            Dvpanel_tablereservar_Title = "Reservar";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tablereservar_Internalname, "Title", Dvpanel_tablereservar_Title);
         }
         else if ( StringUtil.StrCmp(AV13tipo, "SLD") == 0 )
         {
            Dvpanel_tablereservar_Title = "Debitar Saldo Contrato";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tablereservar_Internalname, "Title", Dvpanel_tablereservar_Title);
         }
         else if ( StringUtil.StrCmp(AV13tipo, "DSR") == 0 )
         {
            Dvpanel_tablereservar_Title = "Debitar Saldo Reservado";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tablereservar_Internalname, "Title", Dvpanel_tablereservar_Title);
         }
         /* Using cursor H00MR2 */
         pr_default.execute(0, new Object[] {AV12SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = H00MR2_A1561SaldoContrato_Codigo[0];
            A1574SaldoContrato_Reservado = H00MR2_A1574SaldoContrato_Reservado[0];
            A1576SaldoContrato_Saldo = H00MR2_A1576SaldoContrato_Saldo[0];
            AV10SaldoContrato_Reservado = A1574SaldoContrato_Reservado;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV10SaldoContrato_Reservado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            AV9SaldoContrato_Saldo = A1576SaldoContrato_Saldo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( AV9SaldoContrato_Saldo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            AV14SaldoContrato_Disponivel = (decimal)(A1576SaldoContrato_Saldo-A1574SaldoContrato_Reservado);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Disponivel", StringUtil.LTrim( StringUtil.Str( AV14SaldoContrato_Disponivel, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_DISPONIVEL", GetSecureSignedToken( "", context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      public void GXEnter( )
      {
         /* Execute user event: E12MR2 */
         E12MR2 ();
         if (returnInSub) return;
      }

      protected void E12MR2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S112 ();
         if (returnInSub) return;
         if ( AV11CheckRequiredFieldsResult )
         {
            if ( StringUtil.StrCmp(AV13tipo, "RSV") == 0 )
            {
               if ( AV8AuxSaldoContrato_Reservado <= AV14SaldoContrato_Disponivel )
               {
                  new prc_saldocontratoreservar(context ).execute(  AV12SaldoContrato_Codigo,  AV16Contrato_Codigo,  AV17NotaEmpenho_Codigo,  AV19ContagemResultado_Codigo,  0,  AV8AuxSaldoContrato_Reservado) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12SaldoContrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Contrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17NotaEmpenho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AuxSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV8AuxSaldoContrato_Reservado, 18, 5)));
               }
               else
               {
                  GX_msglist.addItem("Saldo insuficiente!");
               }
            }
            else if ( StringUtil.StrCmp(AV13tipo, "SLD") == 0 )
            {
               if ( AV8AuxSaldoContrato_Reservado <= AV9SaldoContrato_Saldo )
               {
                  new prc_saldocontratodebitosaldo(context ).execute(  AV12SaldoContrato_Codigo,  AV16Contrato_Codigo,  AV17NotaEmpenho_Codigo,  AV19ContagemResultado_Codigo,  AV8AuxSaldoContrato_Reservado) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12SaldoContrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Contrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17NotaEmpenho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AuxSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV8AuxSaldoContrato_Reservado, 18, 5)));
               }
               else
               {
                  GX_msglist.addItem("Saldo insuficiente!");
               }
            }
            else if ( StringUtil.StrCmp(AV13tipo, "DSR") == 0 )
            {
               if ( AV8AuxSaldoContrato_Reservado <= AV10SaldoContrato_Reservado )
               {
                  new prc_saldocontratodebitosaldoreserva(context ).execute(  AV12SaldoContrato_Codigo,  AV16Contrato_Codigo,  AV17NotaEmpenho_Codigo,  AV19ContagemResultado_Codigo,  AV8AuxSaldoContrato_Reservado) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12SaldoContrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Contrato_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17NotaEmpenho_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AuxSaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( AV8AuxSaldoContrato_Reservado, 18, 5)));
               }
               else
               {
                  GX_msglist.addItem("Saldo insuficiente!");
                  /* Execute user subroutine: 'SHOW MESSAGES' */
                  S122 ();
                  if (returnInSub) return;
               }
            }
            context.CommitDataStores( "WP_SaldoContratoReservar");
            context.setWebReturnParms(new Object[] {(int)AV12SaldoContrato_Codigo,(int)AV16Contrato_Codigo,(int)AV17NotaEmpenho_Codigo,(int)AV18Contagem_Codigo,(decimal)AV15SaldoContrato_Credito,(String)AV13tipo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV11CheckRequiredFieldsResult = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11CheckRequiredFieldsResult", AV11CheckRequiredFieldsResult);
         if ( (Convert.ToDecimal(0)==AV8AuxSaldoContrato_Reservado) )
         {
            GX_msglist.addItem("Valor � obrigat�rio.");
            AV11CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11CheckRequiredFieldsResult", AV11CheckRequiredFieldsResult);
         }
      }

      protected void S122( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV23GXV1 = 1;
         while ( AV23GXV1 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV23GXV1));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV23GXV1 = (int)(AV23GXV1+1);
         }
      }

      protected void E13MR2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
      }

      protected void nextLoad( )
      {
      }

      protected void E14MR2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Reservar Saldo", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MR2( true) ;
         }
         else
         {
            wb_table2_8_MR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MR2e( true) ;
         }
         else
         {
            wb_table1_2_MR2e( false) ;
         }
      }

      protected void wb_table2_8_MR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table3_16_MR2( true) ;
         }
         else
         {
            wb_table3_16_MR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_MR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLERESERVARContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLERESERVARContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_36_MR2( true) ;
         }
         else
         {
            wb_table4_36_MR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_36_MR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table5_44_MR2( true) ;
         }
         else
         {
            wb_table5_44_MR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_44_MR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MR2e( true) ;
         }
         else
         {
            wb_table2_8_MR2e( false) ;
         }
      }

      protected void wb_table5_44_MR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_44_MR2e( true) ;
         }
         else
         {
            wb_table5_44_MR2e( false) ;
         }
      }

      protected void wb_table4_36_MR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablereservar_Internalname, tblTablereservar_Internalname, "", "TableData", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauxsaldocontrato_reservado_Internalname, "Valor", "", "", lblTextblockauxsaldocontrato_reservado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAuxsaldocontrato_reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV8AuxSaldoContrato_Reservado, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV8AuxSaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAuxsaldocontrato_reservado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_36_MR2e( true) ;
         }
         else
         {
            wb_table4_36_MR2e( false) ;
         }
      }

      protected void wb_table3_16_MR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_saldo_Internalname, "Saldo", "", "", lblTextblocksaldocontrato_saldo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9SaldoContrato_Saldo, 18, 5, ",", "")), ((edtavSaldocontrato_saldo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV9SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_saldo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSaldocontrato_saldo_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_reservado_Internalname, "Reservado", "", "", lblTextblocksaldocontrato_reservado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( AV10SaldoContrato_Reservado, 18, 5, ",", "")), ((edtavSaldocontrato_reservado_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV10SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_reservado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSaldocontrato_reservado_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_disponivel_Internalname, "Dispon�vel", "", "", lblTextblocksaldocontrato_disponivel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_disponivel_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14SaldoContrato_Disponivel, 18, 5, ",", "")), ((edtavSaldocontrato_disponivel_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV14SaldoContrato_Disponivel, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_disponivel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSaldocontrato_disponivel_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_SaldoContratoReservar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_MR2e( true) ;
         }
         else
         {
            wb_table3_16_MR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV12SaldoContrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12SaldoContrato_Codigo), 6, 0)));
         AV16Contrato_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Contrato_Codigo), 6, 0)));
         AV17NotaEmpenho_Codigo = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17NotaEmpenho_Codigo), 6, 0)));
         AV18Contagem_Codigo = Convert.ToInt32(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Contagem_Codigo), 6, 0)));
         AV15SaldoContrato_Credito = (decimal)(Convert.ToDecimal((decimal)getParm(obj,4)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( AV15SaldoContrato_Credito, 18, 5)));
         AV13tipo = (String)getParm(obj,5);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13tipo", AV13tipo);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMR2( ) ;
         WSMR2( ) ;
         WEMR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221233168");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_saldocontratoreservar.js", "?202031221233168");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblocksaldocontrato_saldo_Internalname = "TEXTBLOCKSALDOCONTRATO_SALDO";
         edtavSaldocontrato_saldo_Internalname = "vSALDOCONTRATO_SALDO";
         lblTextblocksaldocontrato_reservado_Internalname = "TEXTBLOCKSALDOCONTRATO_RESERVADO";
         edtavSaldocontrato_reservado_Internalname = "vSALDOCONTRATO_RESERVADO";
         lblTextblocksaldocontrato_disponivel_Internalname = "TEXTBLOCKSALDOCONTRATO_DISPONIVEL";
         edtavSaldocontrato_disponivel_Internalname = "vSALDOCONTRATO_DISPONIVEL";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         lblTextblockauxsaldocontrato_reservado_Internalname = "TEXTBLOCKAUXSALDOCONTRATO_RESERVADO";
         edtavAuxsaldocontrato_reservado_Internalname = "vAUXSALDOCONTRATO_RESERVADO";
         tblTablereservar_Internalname = "TABLERESERVAR";
         Dvpanel_tablereservar_Internalname = "DVPANEL_TABLERESERVAR";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavSaldocontrato_disponivel_Jsonclick = "";
         edtavSaldocontrato_disponivel_Enabled = 1;
         edtavSaldocontrato_reservado_Jsonclick = "";
         edtavSaldocontrato_reservado_Enabled = 1;
         edtavSaldocontrato_saldo_Jsonclick = "";
         edtavSaldocontrato_saldo_Enabled = 1;
         edtavAuxsaldocontrato_reservado_Jsonclick = "";
         Dvpanel_tablereservar_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tablereservar_Iconposition = "left";
         Dvpanel_tablereservar_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tablereservar_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tablereservar_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tablereservar_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tablereservar_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tablereservar_Title = "Reservar";
         Dvpanel_tablereservar_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tablereservar_Width = "100%";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Valores";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Reservar Saldo";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12MR2',iparms:[{av:'AV11CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV13tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV8AuxSaldoContrato_Reservado',fld:'vAUXSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV14SaldoContrato_Disponivel',fld:'vSALDOCONTRATO_DISPONIVEL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV12SaldoContrato_Codigo',fld:'vSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV17NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9SaldoContrato_Saldo',fld:'vSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV10SaldoContrato_Reservado',fld:'vSALDOCONTRATO_RESERVADO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV15SaldoContrato_Credito',fld:'vSALDOCONTRATO_CREDITO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Messages',fld:'vMESSAGES',pic:'',nv:null}],oparms:[{av:'AV11CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV13tipo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         hsh = "";
         scmdbuf = "";
         H00MR2_A1561SaldoContrato_Codigo = new int[1] ;
         H00MR2_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00MR2_A1576SaldoContrato_Saldo = new decimal[1] ;
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockauxsaldocontrato_reservado_Jsonclick = "";
         lblTextblocksaldocontrato_saldo_Jsonclick = "";
         lblTextblocksaldocontrato_reservado_Jsonclick = "";
         lblTextblocksaldocontrato_disponivel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_saldocontratoreservar__default(),
            new Object[][] {
                new Object[] {
               H00MR2_A1561SaldoContrato_Codigo, H00MR2_A1574SaldoContrato_Reservado, H00MR2_A1576SaldoContrato_Saldo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
         edtavSaldocontrato_reservado_Enabled = 0;
         edtavSaldocontrato_disponivel_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV12SaldoContrato_Codigo ;
      private int AV16Contrato_Codigo ;
      private int AV17NotaEmpenho_Codigo ;
      private int AV18Contagem_Codigo ;
      private int wcpOAV12SaldoContrato_Codigo ;
      private int wcpOAV16Contrato_Codigo ;
      private int wcpOAV17NotaEmpenho_Codigo ;
      private int wcpOAV18Contagem_Codigo ;
      private int AV19ContagemResultado_Codigo ;
      private int edtavSaldocontrato_saldo_Enabled ;
      private int edtavSaldocontrato_reservado_Enabled ;
      private int edtavSaldocontrato_disponivel_Enabled ;
      private int A1561SaldoContrato_Codigo ;
      private int AV23GXV1 ;
      private int idxLst ;
      private decimal AV15SaldoContrato_Credito ;
      private decimal wcpOAV15SaldoContrato_Credito ;
      private decimal AV9SaldoContrato_Saldo ;
      private decimal AV10SaldoContrato_Reservado ;
      private decimal AV14SaldoContrato_Disponivel ;
      private decimal AV8AuxSaldoContrato_Reservado ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String AV13tipo ;
      private String wcpOAV13tipo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvpanel_tablereservar_Width ;
      private String Dvpanel_tablereservar_Cls ;
      private String Dvpanel_tablereservar_Title ;
      private String Dvpanel_tablereservar_Iconposition ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSaldocontrato_saldo_Internalname ;
      private String edtavSaldocontrato_reservado_Internalname ;
      private String edtavSaldocontrato_disponivel_Internalname ;
      private String edtavAuxsaldocontrato_reservado_Internalname ;
      private String hsh ;
      private String Dvpanel_tablereservar_Internalname ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablereservar_Internalname ;
      private String lblTextblockauxsaldocontrato_reservado_Internalname ;
      private String lblTextblockauxsaldocontrato_reservado_Jsonclick ;
      private String edtavAuxsaldocontrato_reservado_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksaldocontrato_saldo_Internalname ;
      private String lblTextblocksaldocontrato_saldo_Jsonclick ;
      private String edtavSaldocontrato_saldo_Jsonclick ;
      private String lblTextblocksaldocontrato_reservado_Internalname ;
      private String lblTextblocksaldocontrato_reservado_Jsonclick ;
      private String edtavSaldocontrato_reservado_Jsonclick ;
      private String lblTextblocksaldocontrato_disponivel_Internalname ;
      private String lblTextblocksaldocontrato_disponivel_Jsonclick ;
      private String edtavSaldocontrato_disponivel_Jsonclick ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV11CheckRequiredFieldsResult ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tablereservar_Collapsible ;
      private bool Dvpanel_tablereservar_Collapsed ;
      private bool Dvpanel_tablereservar_Autowidth ;
      private bool Dvpanel_tablereservar_Autoheight ;
      private bool Dvpanel_tablereservar_Showcollapseicon ;
      private bool Dvpanel_tablereservar_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_SaldoContrato_Codigo ;
      private int aP1_Contrato_Codigo ;
      private int aP2_NotaEmpenho_Codigo ;
      private int aP3_Contagem_Codigo ;
      private decimal aP4_SaldoContrato_Credito ;
      private String aP5_tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00MR2_A1561SaldoContrato_Codigo ;
      private decimal[] H00MR2_A1574SaldoContrato_Reservado ;
      private decimal[] H00MR2_A1576SaldoContrato_Saldo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private GXWebForm Form ;
      private SdtMessages_Message AV5Message ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
   }

   public class wp_saldocontratoreservar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MR2 ;
          prmH00MR2 = new Object[] {
          new Object[] {"@AV12SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MR2", "SELECT [SaldoContrato_Codigo], [SaldoContrato_Reservado], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @AV12SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MR2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
