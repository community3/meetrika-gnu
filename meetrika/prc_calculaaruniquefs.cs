/*
               File: PRC_CalculaARUniqueFS
        Description: PRC_Calcula ARUnique FS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:40.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_calculaaruniquefs : GXProcedure
   {
      public prc_calculaaruniquefs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_calculaaruniquefs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemItem_Lancamento ,
                           out int aP1_Contador )
      {
         this.AV8ContagemItem_Lancamento = aP0_ContagemItem_Lancamento;
         this.AV9Contador = 0 ;
         initialize();
         executePrivate();
         aP1_Contador=this.AV9Contador;
      }

      public int executeUdp( int aP0_ContagemItem_Lancamento )
      {
         this.AV8ContagemItem_Lancamento = aP0_ContagemItem_Lancamento;
         this.AV9Contador = 0 ;
         initialize();
         executePrivate();
         aP1_Contador=this.AV9Contador;
         return AV9Contador ;
      }

      public void executeSubmit( int aP0_ContagemItem_Lancamento ,
                                 out int aP1_Contador )
      {
         prc_calculaaruniquefs objprc_calculaaruniquefs;
         objprc_calculaaruniquefs = new prc_calculaaruniquefs();
         objprc_calculaaruniquefs.AV8ContagemItem_Lancamento = aP0_ContagemItem_Lancamento;
         objprc_calculaaruniquefs.AV9Contador = 0 ;
         objprc_calculaaruniquefs.context.SetSubmitInitialConfig(context);
         objprc_calculaaruniquefs.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_calculaaruniquefs);
         aP1_Contador=this.AV9Contador;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_calculaaruniquefs)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized group. */
         /* Using cursor P00182 */
         pr_default.execute(0, new Object[] {AV8ContagemItem_Lancamento});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A224ContagemItem_Lancamento = P00182_A224ContagemItem_Lancamento[0];
            cV9Contador = (int)(cV9Contador+1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00182_A224ContagemItem_Lancamento = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_calculaaruniquefs__default(),
            new Object[][] {
                new Object[] {
               P00182_A224ContagemItem_Lancamento
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemItem_Lancamento ;
      private int AV9Contador ;
      private int A224ContagemItem_Lancamento ;
      private int cV9Contador ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00182_A224ContagemItem_Lancamento ;
      private int aP1_Contador ;
   }

   public class prc_calculaaruniquefs__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00182 ;
          prmP00182 = new Object[] {
          new Object[] {"@AV8ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00182", "SELECT DISTINCT NULL AS [ContagemItem_Lancamento] FROM ( SELECT TOP(100) PERCENT [ContagemItem_Lancamento] FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @AV8ContagemItem_Lancamento ORDER BY [ContagemItem_Lancamento]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00182,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
