/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:22:36.42
*/
gx.evt.autoSkip = false;
gx.define('wp_novoprojeto', false, function () {
   this.ServerClass =  "wp_novoprojeto" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV9Projeto_Introducao=gx.fn.getControlValue("vPROJETO_INTRODUCAO") ;
      this.AV8Projeto_Escopo=gx.fn.getControlValue("vPROJETO_ESCOPO") ;
      this.AV17Codigo=gx.fn.getIntegerValue("vCODIGO",'.') ;
      this.A648Projeto_Codigo=gx.fn.getIntegerValue("PROJETO_CODIGO",'.') ;
      this.A650Projeto_Sigla=gx.fn.getControlValue("PROJETO_SIGLA") ;
      this.A649Projeto_Nome=gx.fn.getControlValue("PROJETO_NOME") ;
      this.A1541Projeto_Previsao=gx.fn.getDateValue("PROJETO_PREVISAO") ;
      this.A1542Projeto_GerenteCod=gx.fn.getIntegerValue("PROJETO_GERENTECOD",'.') ;
      this.A1543Projeto_ServicoCod=gx.fn.getIntegerValue("PROJETO_SERVICOCOD",'.') ;
      this.A1540Projeto_Introducao=gx.fn.getControlValue("PROJETO_INTRODUCAO") ;
      this.A653Projeto_Escopo=gx.fn.getControlValue("PROJETO_ESCOPO") ;
      this.AV18Demanda=gx.fn.getControlValue("vDEMANDA") ;
   };
   this.Validv_Projeto_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPROJETO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Projeto_previsao=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPROJETO_PREVISAO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV12Projeto_Previsao)==0) || new gx.date.gxdate( this.AV12Projeto_Previsao ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Projeto_Previsao fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e12lc2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e14lc2_client=function()
   {
      this.executeServerEvent("VPROJETO_CODIGO.ISVALID", true, null, false, true);
   };
   this.e16lc1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,14,17,19,22,24,27,29,32,34,37,39,42,44,47,49,52,58,64,72];
   this.GXLastCtrlId =72;
   this.GXUITABSPANEL_TABContainer = gx.uc.getNew(this, 11, 0, "BootstrapTabsPanel", "GXUITABSPANEL_TABContainer", "Gxuitabspanel_tab");
   var GXUITABSPANEL_TABContainer = this.GXUITABSPANEL_TABContainer;
   GXUITABSPANEL_TABContainer.setProp("Width", "Width", "100%", "str");
   GXUITABSPANEL_TABContainer.setProp("Height", "Height", "100", "str");
   GXUITABSPANEL_TABContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   GXUITABSPANEL_TABContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   GXUITABSPANEL_TABContainer.setProp("AutoScroll", "Autoscroll", true, "bool");
   GXUITABSPANEL_TABContainer.setProp("Cls", "Cls", "GXUI-DVelop-Tabs", "str");
   GXUITABSPANEL_TABContainer.setProp("ActiveTabId", "Activetabid", "", "char");
   GXUITABSPANEL_TABContainer.setProp("DesignTimeTabs", "Designtimetabs", "[{\"id\":\"Dados\"},{\"id\":\"Introducao\"},{\"id\":\"Escopo\"}]", "str");
   GXUITABSPANEL_TABContainer.setProp("SelectedTabIndex", "Selectedtabindex", 0, "num");
   GXUITABSPANEL_TABContainer.setProp("Visible", "Visible", true, "bool");
   GXUITABSPANEL_TABContainer.setProp("Enabled", "Enabled", true, "boolean");
   GXUITABSPANEL_TABContainer.setProp("Class", "Class", "", "char");
   GXUITABSPANEL_TABContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(GXUITABSPANEL_TABContainer);
   this.PROJETO_ESCOPOContainer = gx.uc.getNew(this, 61, 19, "CKEditorControl", "PROJETO_ESCOPOContainer", "Projeto_escopo");
   var PROJETO_ESCOPOContainer = this.PROJETO_ESCOPOContainer;
   PROJETO_ESCOPOContainer.setProp("Width", "Width", "800", "str");
   PROJETO_ESCOPOContainer.setProp("Height", "Height", "350", "str");
   PROJETO_ESCOPOContainer.addV2CFunction('AV8Projeto_Escopo', "vPROJETO_ESCOPO", 'SetAttribute');
   PROJETO_ESCOPOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV8Projeto_Escopo=UC.GetAttribute();gx.fn.setControlValue("vPROJETO_ESCOPO",UC.ParentObject.AV8Projeto_Escopo); });
   PROJETO_ESCOPOContainer.setProp("Skin", "Skin", "silver", "str");
   PROJETO_ESCOPOContainer.setProp("Toolbar", "Toolbar", "Default", "str");
   PROJETO_ESCOPOContainer.setProp("CustomToolbar", "Customtoolbar", "", "str");
   PROJETO_ESCOPOContainer.setProp("CustomConfiguration", "Customconfiguration", "", "str");
   PROJETO_ESCOPOContainer.setProp("ToolbarCanCollapse", "Toolbarcancollapse", true, "bool");
   PROJETO_ESCOPOContainer.setProp("ToolbarExpanded", "Toolbarexpanded", true, "bool");
   PROJETO_ESCOPOContainer.setProp("Color", "Color", gx.color.fromRGB(211,211,211), "color");
   PROJETO_ESCOPOContainer.setProp("ButtonPressedId", "Buttonpressedid", "", "char");
   PROJETO_ESCOPOContainer.setProp("SdtItemObject", "Prop_sdt_item_object", "", "char");
   PROJETO_ESCOPOContainer.setProp("Dimensions", "Attnumdim", "", "char");
   PROJETO_ESCOPOContainer.setProp("BaseAttType", "Baseatttype", '', "int");
   PROJETO_ESCOPOContainer.setProp("PROP_EXT_BASE_ATT_COLLECTION", "Prop_ext_base_att_collection", false, "boolean");
   PROJETO_ESCOPOContainer.setProp("FieldSpecifier", "Fieldspecifier", "", "char");
   PROJETO_ESCOPOContainer.setProp("CaptionValue", "Captionvalue", "", "str");
   PROJETO_ESCOPOContainer.setProp("CaptionClass", "Captionclass", "", "str");
   PROJETO_ESCOPOContainer.setProp("CaptionPosition", "Captionposition", "", "str");
   PROJETO_ESCOPOContainer.setProp("InternalTitle", "Coltitle", "", "char");
   PROJETO_ESCOPOContainer.setProp("TitleFont", "Coltitlefont", "", "char");
   PROJETO_ESCOPOContainer.setProp("TitleForeColor", "Coltitlecolor", '', "int");
   PROJETO_ESCOPOContainer.setProp("UserControlIsColumn", "Usercontroliscolumn", false, "boolean");
   PROJETO_ESCOPOContainer.setProp("Visible", "Visible", true, "bool");
   PROJETO_ESCOPOContainer.setDynProp("Enabled", "Enabled", true, "boolean");
   PROJETO_ESCOPOContainer.setProp("Class", "Class", "", "char");
   PROJETO_ESCOPOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(PROJETO_ESCOPOContainer);
   this.PROJETO_INTRODUCAOContainer = gx.uc.getNew(this, 55, 19, "CKEditorControl", "PROJETO_INTRODUCAOContainer", "Projeto_introducao");
   var PROJETO_INTRODUCAOContainer = this.PROJETO_INTRODUCAOContainer;
   PROJETO_INTRODUCAOContainer.setProp("Width", "Width", "800", "str");
   PROJETO_INTRODUCAOContainer.setProp("Height", "Height", "350", "str");
   PROJETO_INTRODUCAOContainer.addV2CFunction('AV9Projeto_Introducao', "vPROJETO_INTRODUCAO", 'SetAttribute');
   PROJETO_INTRODUCAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV9Projeto_Introducao=UC.GetAttribute();gx.fn.setControlValue("vPROJETO_INTRODUCAO",UC.ParentObject.AV9Projeto_Introducao); });
   PROJETO_INTRODUCAOContainer.setProp("Skin", "Skin", "silver", "str");
   PROJETO_INTRODUCAOContainer.setProp("Toolbar", "Toolbar", "Default", "str");
   PROJETO_INTRODUCAOContainer.setProp("CustomToolbar", "Customtoolbar", "", "str");
   PROJETO_INTRODUCAOContainer.setProp("CustomConfiguration", "Customconfiguration", "", "str");
   PROJETO_INTRODUCAOContainer.setProp("ToolbarCanCollapse", "Toolbarcancollapse", true, "bool");
   PROJETO_INTRODUCAOContainer.setProp("ToolbarExpanded", "Toolbarexpanded", true, "bool");
   PROJETO_INTRODUCAOContainer.setProp("Color", "Color", gx.color.fromRGB(211,211,211), "color");
   PROJETO_INTRODUCAOContainer.setProp("ButtonPressedId", "Buttonpressedid", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("SdtItemObject", "Prop_sdt_item_object", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("Dimensions", "Attnumdim", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("BaseAttType", "Baseatttype", '', "int");
   PROJETO_INTRODUCAOContainer.setProp("PROP_EXT_BASE_ATT_COLLECTION", "Prop_ext_base_att_collection", false, "boolean");
   PROJETO_INTRODUCAOContainer.setProp("FieldSpecifier", "Fieldspecifier", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("CaptionValue", "Captionvalue", "", "str");
   PROJETO_INTRODUCAOContainer.setProp("CaptionClass", "Captionclass", "", "str");
   PROJETO_INTRODUCAOContainer.setProp("CaptionPosition", "Captionposition", "", "str");
   PROJETO_INTRODUCAOContainer.setProp("InternalTitle", "Coltitle", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("TitleFont", "Coltitlefont", "", "char");
   PROJETO_INTRODUCAOContainer.setProp("TitleForeColor", "Coltitlecolor", '', "int");
   PROJETO_INTRODUCAOContainer.setProp("UserControlIsColumn", "Usercontroliscolumn", false, "boolean");
   PROJETO_INTRODUCAOContainer.setProp("Visible", "Visible", true, "bool");
   PROJETO_INTRODUCAOContainer.setDynProp("Enabled", "Enabled", true, "boolean");
   PROJETO_INTRODUCAOContainer.setProp("Class", "Class", "", "char");
   PROJETO_INTRODUCAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(PROJETO_INTRODUCAOContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[14]={fld:"UNNAMEDTABLE3",grid:0};
   GXValidFnc[17]={fld:"TEXTBLOCKPROJETO_CODIGO", format:0,grid:0};
   GXValidFnc[19]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Projeto_codigo,isvalid:'e14lc2_client',rgrid:[],fld:"vPROJETO_CODIGO",gxz:"ZV15Projeto_Codigo",gxold:"OV15Projeto_Codigo",gxvar:"AV15Projeto_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV15Projeto_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15Projeto_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vPROJETO_CODIGO",gx.O.AV15Projeto_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV15Projeto_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vPROJETO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[22]={fld:"TEXTBLOCKTIPOPROJETO_CODIGO", format:0,grid:0};
   GXValidFnc[24]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vTIPOPROJETO_CODIGO",gxz:"ZV19TipoProjeto_Codigo",gxold:"OV19TipoProjeto_Codigo",gxvar:"AV19TipoProjeto_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV19TipoProjeto_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV19TipoProjeto_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vTIPOPROJETO_CODIGO",gx.O.AV19TipoProjeto_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV19TipoProjeto_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTIPOPROJETO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[27]={fld:"TEXTBLOCKPROJETO_NOME", format:0,grid:0};
   GXValidFnc[29]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPROJETO_NOME",gxz:"ZV11Projeto_Nome",gxold:"OV11Projeto_Nome",gxvar:"AV11Projeto_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV11Projeto_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV11Projeto_Nome=Value},v2c:function(){gx.fn.setControlValue("vPROJETO_NOME",gx.O.AV11Projeto_Nome,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV11Projeto_Nome=this.val()},val:function(){return gx.fn.getControlValue("vPROJETO_NOME")},nac:gx.falseFn};
   GXValidFnc[32]={fld:"TEXTBLOCKPROJETO_SIGLA", format:0,grid:0};
   GXValidFnc[34]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPROJETO_SIGLA",gxz:"ZV10Projeto_Sigla",gxold:"OV10Projeto_Sigla",gxvar:"AV10Projeto_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV10Projeto_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV10Projeto_Sigla=Value},v2c:function(){gx.fn.setControlValue("vPROJETO_SIGLA",gx.O.AV10Projeto_Sigla,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV10Projeto_Sigla=this.val()},val:function(){return gx.fn.getControlValue("vPROJETO_SIGLA")},nac:gx.falseFn};
   GXValidFnc[37]={fld:"TEXTBLOCKPROJETO_SERVICOCOD", format:0,grid:0};
   GXValidFnc[39]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPROJETO_SERVICOCOD",gxz:"ZV14Projeto_ServicoCod",gxold:"OV14Projeto_ServicoCod",gxvar:"AV14Projeto_ServicoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV14Projeto_ServicoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14Projeto_ServicoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vPROJETO_SERVICOCOD",gx.O.AV14Projeto_ServicoCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV14Projeto_ServicoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vPROJETO_SERVICOCOD",'.')},nac:gx.falseFn};
   GXValidFnc[42]={fld:"TEXTBLOCKPROJETO_PREVISAO", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Projeto_previsao,isvalid:null,rgrid:[],fld:"vPROJETO_PREVISAO",gxz:"ZV12Projeto_Previsao",gxold:"OV12Projeto_Previsao",gxvar:"AV12Projeto_Previsao",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[44],ip:[44],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV12Projeto_Previsao=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV12Projeto_Previsao=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vPROJETO_PREVISAO",gx.O.AV12Projeto_Previsao,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV12Projeto_Previsao=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vPROJETO_PREVISAO")},nac:gx.falseFn};
   GXValidFnc[47]={fld:"TEXTBLOCKPROJETO_GERENTECOD", format:0,grid:0};
   GXValidFnc[49]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPROJETO_GERENTECOD",gxz:"ZV13Projeto_GerenteCod",gxold:"OV13Projeto_GerenteCod",gxvar:"AV13Projeto_GerenteCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV13Projeto_GerenteCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13Projeto_GerenteCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vPROJETO_GERENTECOD",gx.O.AV13Projeto_GerenteCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV13Projeto_GerenteCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vPROJETO_GERENTECOD",'.')},nac:gx.falseFn};
   GXValidFnc[52]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[58]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[64]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[72]={fld:"TBJAVA", format:1,grid:0};
   this.AV15Projeto_Codigo = 0 ;
   this.ZV15Projeto_Codigo = 0 ;
   this.OV15Projeto_Codigo = 0 ;
   this.AV19TipoProjeto_Codigo = 0 ;
   this.ZV19TipoProjeto_Codigo = 0 ;
   this.OV19TipoProjeto_Codigo = 0 ;
   this.AV11Projeto_Nome = "" ;
   this.ZV11Projeto_Nome = "" ;
   this.OV11Projeto_Nome = "" ;
   this.AV10Projeto_Sigla = "" ;
   this.ZV10Projeto_Sigla = "" ;
   this.OV10Projeto_Sigla = "" ;
   this.AV14Projeto_ServicoCod = 0 ;
   this.ZV14Projeto_ServicoCod = 0 ;
   this.OV14Projeto_ServicoCod = 0 ;
   this.AV12Projeto_Previsao = gx.date.nullDate() ;
   this.ZV12Projeto_Previsao = gx.date.nullDate() ;
   this.OV12Projeto_Previsao = gx.date.nullDate() ;
   this.AV13Projeto_GerenteCod = 0 ;
   this.ZV13Projeto_GerenteCod = 0 ;
   this.OV13Projeto_GerenteCod = 0 ;
   this.AV15Projeto_Codigo = 0 ;
   this.AV19TipoProjeto_Codigo = 0 ;
   this.AV11Projeto_Nome = "" ;
   this.AV10Projeto_Sigla = "" ;
   this.AV14Projeto_ServicoCod = 0 ;
   this.AV12Projeto_Previsao = gx.date.nullDate() ;
   this.AV13Projeto_GerenteCod = 0 ;
   this.AV9Projeto_Introducao = "" ;
   this.AV8Projeto_Escopo = "" ;
   this.AV17Codigo = 0 ;
   this.AV18Demanda = "" ;
   this.A648Projeto_Codigo = 0 ;
   this.A650Projeto_Sigla = "" ;
   this.A649Projeto_Nome = "" ;
   this.A1541Projeto_Previsao = gx.date.nullDate() ;
   this.A1542Projeto_GerenteCod = 0 ;
   this.A1543Projeto_ServicoCod = 0 ;
   this.A1540Projeto_Introducao = "" ;
   this.A653Projeto_Escopo = "" ;
   this.Events = {"e12lc2_client": ["ENTER", true] ,"e14lc2_client": ["VPROJETO_CODIGO.ISVALID", true] ,"e16lc1_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["ENTER"] = [[{av:'AV10Projeto_Sigla',fld:'vPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV11Projeto_Nome',fld:'vPROJETO_NOME',pic:'@!',nv:''},{av:'AV14Projeto_ServicoCod',fld:'vPROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV12Projeto_Previsao',fld:'vPROJETO_PREVISAO',pic:'',nv:''},{av:'AV9Projeto_Introducao',fld:'vPROJETO_INTRODUCAO',pic:'',nv:''},{av:'AV8Projeto_Escopo',fld:'vPROJETO_ESCOPO',pic:'',nv:''},{av:'AV17Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV17Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["VPROJETO_CODIGO.ISVALID"] = [[{av:'AV15Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A650Projeto_Sigla',fld:'PROJETO_SIGLA',pic:'@!',nv:''},{av:'A649Projeto_Nome',fld:'PROJETO_NOME',pic:'@!',nv:''},{av:'A1541Projeto_Previsao',fld:'PROJETO_PREVISAO',pic:'',nv:''},{av:'A1542Projeto_GerenteCod',fld:'PROJETO_GERENTECOD',pic:'ZZZZZ9',nv:0},{av:'A1543Projeto_ServicoCod',fld:'PROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1540Projeto_Introducao',fld:'PROJETO_INTRODUCAO',pic:'',nv:''},{av:'A653Projeto_Escopo',fld:'PROJETO_ESCOPO',pic:'',nv:''}],[{av:'AV10Projeto_Sigla',fld:'vPROJETO_SIGLA',pic:'@!',nv:''},{av:'AV11Projeto_Nome',fld:'vPROJETO_NOME',pic:'@!',nv:''},{av:'AV12Projeto_Previsao',fld:'vPROJETO_PREVISAO',pic:'',nv:''},{av:'AV13Projeto_GerenteCod',fld:'vPROJETO_GERENTECOD',pic:'ZZZZZ9',nv:0},{av:'AV14Projeto_ServicoCod',fld:'vPROJETO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV9Projeto_Introducao',fld:'vPROJETO_INTRODUCAO',pic:'',nv:''},{av:'AV8Projeto_Escopo',fld:'vPROJETO_ESCOPO',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vPROJETO_SIGLA","Enabled")',ctrl:'vPROJETO_SIGLA',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vPROJETO_NOME","Enabled")',ctrl:'vPROJETO_NOME',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vPROJETO_PREVISAO","Enabled")',ctrl:'vPROJETO_PREVISAO',prop:'Enabled'},{ctrl:'vPROJETO_GERENTECOD'},{ctrl:'vPROJETO_SERVICOCOD'},{av:'this.PROJETO_INTRODUCAOContainer.Enabled',ctrl:'vPROJETO_INTRODUCAO',prop:'Enabled'},{av:'this.PROJETO_ESCOPOContainer.Enabled',ctrl:'vPROJETO_ESCOPO',prop:'Enabled'}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV9Projeto_Introducao", "vPROJETO_INTRODUCAO", 0, "vchar");
   this.setVCMap("AV8Projeto_Escopo", "vPROJETO_ESCOPO", 0, "vchar");
   this.setVCMap("AV17Codigo", "vCODIGO", 0, "int");
   this.setVCMap("A648Projeto_Codigo", "PROJETO_CODIGO", 0, "int");
   this.setVCMap("A650Projeto_Sigla", "PROJETO_SIGLA", 0, "char");
   this.setVCMap("A649Projeto_Nome", "PROJETO_NOME", 0, "char");
   this.setVCMap("A1541Projeto_Previsao", "PROJETO_PREVISAO", 0, "date");
   this.setVCMap("A1542Projeto_GerenteCod", "PROJETO_GERENTECOD", 0, "int");
   this.setVCMap("A1543Projeto_ServicoCod", "PROJETO_SERVICOCOD", 0, "int");
   this.setVCMap("A1540Projeto_Introducao", "PROJETO_INTRODUCAO", 0, "vchar");
   this.setVCMap("A653Projeto_Escopo", "PROJETO_ESCOPO", 0, "vchar");
   this.setVCMap("AV18Demanda", "vDEMANDA", 0, "svchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_novoprojeto);
