/*
               File: PRC_ContagemResultadoContagensDLT
        Description: Contagem Resultado Contagens DLT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:49.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultadocontagensdlt : GXProcedure
   {
      public prc_contagemresultadocontagensdlt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultadocontagensdlt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref DateTime aP1_ContagemResultado_DataCnt ,
                           ref String aP2_ContagemResultado_HoraCnt )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV10ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV8ContagemResultado_Codigo;
         aP1_ContagemResultado_DataCnt=this.AV9ContagemResultado_DataCnt;
         aP2_ContagemResultado_HoraCnt=this.AV10ContagemResultado_HoraCnt;
      }

      public String executeUdp( ref int aP0_ContagemResultado_Codigo ,
                                ref DateTime aP1_ContagemResultado_DataCnt )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV10ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV8ContagemResultado_Codigo;
         aP1_ContagemResultado_DataCnt=this.AV9ContagemResultado_DataCnt;
         aP2_ContagemResultado_HoraCnt=this.AV10ContagemResultado_HoraCnt;
         return AV10ContagemResultado_HoraCnt ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref DateTime aP1_ContagemResultado_DataCnt ,
                                 ref String aP2_ContagemResultado_HoraCnt )
      {
         prc_contagemresultadocontagensdlt objprc_contagemresultadocontagensdlt;
         objprc_contagemresultadocontagensdlt = new prc_contagemresultadocontagensdlt();
         objprc_contagemresultadocontagensdlt.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contagemresultadocontagensdlt.AV9ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         objprc_contagemresultadocontagensdlt.AV10ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         objprc_contagemresultadocontagensdlt.context.SetSubmitInitialConfig(context);
         objprc_contagemresultadocontagensdlt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultadocontagensdlt);
         aP0_ContagemResultado_Codigo=this.AV8ContagemResultado_Codigo;
         aP1_ContagemResultado_DataCnt=this.AV9ContagemResultado_DataCnt;
         aP2_ContagemResultado_HoraCnt=this.AV10ContagemResultado_HoraCnt;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultadocontagensdlt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P003L2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, AV9ContagemResultado_DataCnt, AV10ContagemResultado_HoraCnt});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultadocontagensdlt__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_Codigo ;
      private String AV10ContagemResultado_HoraCnt ;
      private DateTime AV9ContagemResultado_DataCnt ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private DateTime aP1_ContagemResultado_DataCnt ;
      private String aP2_ContagemResultado_HoraCnt ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_contagemresultadocontagensdlt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003L2 ;
          prmP003L2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003L2", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo and [ContagemResultado_DataCnt] = @AV9ContagemResultado_DataCnt and [ContagemResultado_HoraCnt] = @AV10ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003L2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
