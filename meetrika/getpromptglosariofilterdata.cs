/*
               File: GetPromptGlosarioFilterData
        Description: Get Prompt Glosario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:25.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptglosariofilterdata : GXProcedure
   {
      public getpromptglosariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptglosariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptglosariofilterdata objgetpromptglosariofilterdata;
         objgetpromptglosariofilterdata = new getpromptglosariofilterdata();
         objgetpromptglosariofilterdata.AV14DDOName = aP0_DDOName;
         objgetpromptglosariofilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetpromptglosariofilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptglosariofilterdata.AV18OptionsJson = "" ;
         objgetpromptglosariofilterdata.AV21OptionsDescJson = "" ;
         objgetpromptglosariofilterdata.AV23OptionIndexesJson = "" ;
         objgetpromptglosariofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptglosariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptglosariofilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptglosariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_GLOSARIO_TERMO") == 0 )
         {
            /* Execute user subroutine: 'LOADGLOSARIO_TERMOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("PromptGlosarioGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptGlosarioGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("PromptGlosarioGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "GLOSARIO_AREATRABALHOCOD") == 0 )
            {
               AV30Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO") == 0 )
            {
               AV10TFGlosario_Termo = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFGLOSARIO_TERMO_SEL") == 0 )
            {
               AV11TFGlosario_Termo_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 )
            {
               AV32Glosario_Termo1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 )
               {
                  AV35Glosario_Termo2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 )
                  {
                     AV38Glosario_Termo3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADGLOSARIO_TERMOOPTIONS' Routine */
         AV10TFGlosario_Termo = AV12SearchTxt;
         AV11TFGlosario_Termo_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV31DynamicFiltersSelector1 ,
                                              AV32Glosario_Termo1 ,
                                              AV33DynamicFiltersEnabled2 ,
                                              AV34DynamicFiltersSelector2 ,
                                              AV35Glosario_Termo2 ,
                                              AV36DynamicFiltersEnabled3 ,
                                              AV37DynamicFiltersSelector3 ,
                                              AV38Glosario_Termo3 ,
                                              AV11TFGlosario_Termo_Sel ,
                                              AV10TFGlosario_Termo ,
                                              A858Glosario_Termo ,
                                              A1346Glosario_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV32Glosario_Termo1 = StringUtil.Concat( StringUtil.RTrim( AV32Glosario_Termo1), "%", "");
         lV35Glosario_Termo2 = StringUtil.Concat( StringUtil.RTrim( AV35Glosario_Termo2), "%", "");
         lV38Glosario_Termo3 = StringUtil.Concat( StringUtil.RTrim( AV38Glosario_Termo3), "%", "");
         lV10TFGlosario_Termo = StringUtil.Concat( StringUtil.RTrim( AV10TFGlosario_Termo), "%", "");
         /* Using cursor P00OS2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV32Glosario_Termo1, lV35Glosario_Termo2, lV38Glosario_Termo3, lV10TFGlosario_Termo, AV11TFGlosario_Termo_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOS2 = false;
            A1346Glosario_AreaTrabalhoCod = P00OS2_A1346Glosario_AreaTrabalhoCod[0];
            A858Glosario_Termo = P00OS2_A858Glosario_Termo[0];
            A1347Glosario_Codigo = P00OS2_A1347Glosario_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00OS2_A858Glosario_Termo[0], A858Glosario_Termo) == 0 ) )
            {
               BRKOS2 = false;
               A1347Glosario_Codigo = P00OS2_A1347Glosario_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKOS2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A858Glosario_Termo)) )
            {
               AV16Option = A858Glosario_Termo;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOS2 )
            {
               BRKOS2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFGlosario_Termo = "";
         AV11TFGlosario_Termo_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32Glosario_Termo1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35Glosario_Termo2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38Glosario_Termo3 = "";
         scmdbuf = "";
         lV32Glosario_Termo1 = "";
         lV35Glosario_Termo2 = "";
         lV38Glosario_Termo3 = "";
         lV10TFGlosario_Termo = "";
         A858Glosario_Termo = "";
         P00OS2_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         P00OS2_A858Glosario_Termo = new String[] {""} ;
         P00OS2_A1347Glosario_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptglosariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OS2_A1346Glosario_AreaTrabalhoCod, P00OS2_A858Glosario_Termo, P00OS2_A1347Glosario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV41GXV1 ;
      private int AV30Glosario_AreaTrabalhoCod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1346Glosario_AreaTrabalhoCod ;
      private int A1347Glosario_Codigo ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool BRKOS2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFGlosario_Termo ;
      private String AV11TFGlosario_Termo_Sel ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV32Glosario_Termo1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV35Glosario_Termo2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV38Glosario_Termo3 ;
      private String lV32Glosario_Termo1 ;
      private String lV35Glosario_Termo2 ;
      private String lV38Glosario_Termo3 ;
      private String lV10TFGlosario_Termo ;
      private String A858Glosario_Termo ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OS2_A1346Glosario_AreaTrabalhoCod ;
      private String[] P00OS2_A858Glosario_Termo ;
      private int[] P00OS2_A1347Glosario_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getpromptglosariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OS2( IGxContext context ,
                                             String AV31DynamicFiltersSelector1 ,
                                             String AV32Glosario_Termo1 ,
                                             bool AV33DynamicFiltersEnabled2 ,
                                             String AV34DynamicFiltersSelector2 ,
                                             String AV35Glosario_Termo2 ,
                                             bool AV36DynamicFiltersEnabled3 ,
                                             String AV37DynamicFiltersSelector3 ,
                                             String AV38Glosario_Termo3 ,
                                             String AV11TFGlosario_Termo_Sel ,
                                             String AV10TFGlosario_Termo ,
                                             String A858Glosario_Termo ,
                                             int A1346Glosario_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Glosario_AreaTrabalhoCod], [Glosario_Termo], [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Glosario_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Glosario_Termo1)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV32Glosario_Termo1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV33DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Glosario_Termo2)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV35Glosario_Termo2)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "GLOSARIO_TERMO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Glosario_Termo3)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like '%' + @lV38Glosario_Termo3)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFGlosario_Termo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFGlosario_Termo)) ) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] like @lV10TFGlosario_Termo)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFGlosario_Termo_Sel)) )
         {
            sWhereString = sWhereString + " and ([Glosario_Termo] = @AV11TFGlosario_Termo_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Glosario_Termo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OS2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OS2 ;
          prmP00OS2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV32Glosario_Termo1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV35Glosario_Termo2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV38Glosario_Termo3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@lV10TFGlosario_Termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV11TFGlosario_Termo_Sel",SqlDbType.VarChar,255,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OS2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptglosariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptglosariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptglosariofilterdata") )
          {
             return  ;
          }
          getpromptglosariofilterdata worker = new getpromptglosariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
