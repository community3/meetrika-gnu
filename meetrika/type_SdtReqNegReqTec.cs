/*
               File: type_SdtReqNegReqTec
        Description: Req Neg Req Tec
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:16.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ReqNegReqTec" )]
   [XmlType(TypeName =  "ReqNegReqTec" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtReqNegReqTec : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtReqNegReqTec( )
      {
         /* Constructor for serialization */
         gxTv_SdtReqNegReqTec_Mode = "";
      }

      public SdtReqNegReqTec( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( long AV1895SolicServicoReqNeg_Codigo ,
                        int AV1919Requisito_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(long)AV1895SolicServicoReqNeg_Codigo,(int)AV1919Requisito_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"SolicServicoReqNeg_Codigo", typeof(long)}, new Object[]{"Requisito_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ReqNegReqTec");
         metadata.Set("BT", "ReqNegReqTec");
         metadata.Set("PK", "[ \"SolicServicoReqNeg_Codigo\",\"Requisito_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Requisito_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"SolicServicoReqNeg_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Reqnegreqtec_propostacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Reqnegreqtec_oscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Requisito_ordem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Reqnegreqtec_propostacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Reqnegreqtec_oscod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtReqNegReqTec deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtReqNegReqTec)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtReqNegReqTec obj ;
         obj = this;
         obj.gxTpr_Solicservicoreqneg_codigo = deserialized.gxTpr_Solicservicoreqneg_codigo;
         obj.gxTpr_Requisito_codigo = deserialized.gxTpr_Requisito_codigo;
         obj.gxTpr_Requisito_ordem = deserialized.gxTpr_Requisito_ordem;
         obj.gxTpr_Reqnegreqtec_propostacod = deserialized.gxTpr_Reqnegreqtec_propostacod;
         obj.gxTpr_Reqnegreqtec_oscod = deserialized.gxTpr_Reqnegreqtec_oscod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Solicservicoreqneg_codigo_Z = deserialized.gxTpr_Solicservicoreqneg_codigo_Z;
         obj.gxTpr_Requisito_codigo_Z = deserialized.gxTpr_Requisito_codigo_Z;
         obj.gxTpr_Requisito_ordem_Z = deserialized.gxTpr_Requisito_ordem_Z;
         obj.gxTpr_Reqnegreqtec_propostacod_Z = deserialized.gxTpr_Reqnegreqtec_propostacod_Z;
         obj.gxTpr_Reqnegreqtec_oscod_Z = deserialized.gxTpr_Reqnegreqtec_oscod_Z;
         obj.gxTpr_Requisito_ordem_N = deserialized.gxTpr_Requisito_ordem_N;
         obj.gxTpr_Reqnegreqtec_propostacod_N = deserialized.gxTpr_Reqnegreqtec_propostacod_N;
         obj.gxTpr_Reqnegreqtec_oscod_N = deserialized.gxTpr_Reqnegreqtec_oscod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Codigo") )
               {
                  gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Codigo") )
               {
                  gxTv_SdtReqNegReqTec_Requisito_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem") )
               {
                  gxTv_SdtReqNegReqTec_Requisito_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_PropostaCod") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_OSCod") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtReqNegReqTec_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtReqNegReqTec_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Codigo_Z") )
               {
                  gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Codigo_Z") )
               {
                  gxTv_SdtReqNegReqTec_Requisito_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem_Z") )
               {
                  gxTv_SdtReqNegReqTec_Requisito_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_PropostaCod_Z") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_OSCod_Z") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Requisito_Ordem_N") )
               {
                  gxTv_SdtReqNegReqTec_Requisito_ordem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_PropostaCod_N") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReqNegReqTec_OSCod_N") )
               {
                  gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ReqNegReqTec";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SolicServicoReqNeg_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Requisito_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Requisito_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Requisito_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Requisito_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ReqNegReqTec_PropostaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ReqNegReqTec_OSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtReqNegReqTec_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Requisito_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Requisito_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Requisito_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Requisito_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ReqNegReqTec_PropostaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ReqNegReqTec_OSCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Requisito_Ordem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Requisito_ordem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ReqNegReqTec_PropostaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ReqNegReqTec_OSCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SolicServicoReqNeg_Codigo", gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo, false);
         AddObjectProperty("Requisito_Codigo", gxTv_SdtReqNegReqTec_Requisito_codigo, false);
         AddObjectProperty("Requisito_Ordem", gxTv_SdtReqNegReqTec_Requisito_ordem, false);
         AddObjectProperty("ReqNegReqTec_PropostaCod", gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod, false);
         AddObjectProperty("ReqNegReqTec_OSCod", gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtReqNegReqTec_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtReqNegReqTec_Initialized, false);
            AddObjectProperty("SolicServicoReqNeg_Codigo_Z", gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z, false);
            AddObjectProperty("Requisito_Codigo_Z", gxTv_SdtReqNegReqTec_Requisito_codigo_Z, false);
            AddObjectProperty("Requisito_Ordem_Z", gxTv_SdtReqNegReqTec_Requisito_ordem_Z, false);
            AddObjectProperty("ReqNegReqTec_PropostaCod_Z", gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z, false);
            AddObjectProperty("ReqNegReqTec_OSCod_Z", gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z, false);
            AddObjectProperty("Requisito_Ordem_N", gxTv_SdtReqNegReqTec_Requisito_ordem_N, false);
            AddObjectProperty("ReqNegReqTec_PropostaCod_N", gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N, false);
            AddObjectProperty("ReqNegReqTec_OSCod_N", gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Codigo" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Codigo"   )]
      public long gxTpr_Solicservicoreqneg_codigo
      {
         get {
            return gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo ;
         }

         set {
            if ( gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo != value )
            {
               gxTv_SdtReqNegReqTec_Mode = "INS";
               this.gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Requisito_codigo_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Requisito_ordem_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z_SetNull( );
            }
            gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo = (long)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Codigo" )]
      [  XmlElement( ElementName = "Requisito_Codigo"   )]
      public int gxTpr_Requisito_codigo
      {
         get {
            return gxTv_SdtReqNegReqTec_Requisito_codigo ;
         }

         set {
            if ( gxTv_SdtReqNegReqTec_Requisito_codigo != value )
            {
               gxTv_SdtReqNegReqTec_Mode = "INS";
               this.gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Requisito_codigo_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Requisito_ordem_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z_SetNull( );
               this.gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z_SetNull( );
            }
            gxTv_SdtReqNegReqTec_Requisito_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Requisito_Ordem" )]
      [  XmlElement( ElementName = "Requisito_Ordem"   )]
      public short gxTpr_Requisito_ordem
      {
         get {
            return gxTv_SdtReqNegReqTec_Requisito_ordem ;
         }

         set {
            gxTv_SdtReqNegReqTec_Requisito_ordem_N = 0;
            gxTv_SdtReqNegReqTec_Requisito_ordem = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Requisito_ordem_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Requisito_ordem_N = 1;
         gxTv_SdtReqNegReqTec_Requisito_ordem = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Requisito_ordem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_PropostaCod" )]
      [  XmlElement( ElementName = "ReqNegReqTec_PropostaCod"   )]
      public int gxTpr_Reqnegreqtec_propostacod
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N = 0;
            gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod = (int)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N = 1;
         gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_OSCod" )]
      [  XmlElement( ElementName = "ReqNegReqTec_OSCod"   )]
      public int gxTpr_Reqnegreqtec_oscod
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N = 0;
            gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod = (int)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N = 1;
         gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtReqNegReqTec_Mode ;
         }

         set {
            gxTv_SdtReqNegReqTec_Mode = (String)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Mode_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Mode = "";
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtReqNegReqTec_Initialized ;
         }

         set {
            gxTv_SdtReqNegReqTec_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Initialized_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Codigo_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Codigo_Z"   )]
      public long gxTpr_Solicservicoreqneg_codigo_Z
      {
         get {
            return gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z ;
         }

         set {
            gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z = (long)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Codigo_Z" )]
      [  XmlElement( ElementName = "Requisito_Codigo_Z"   )]
      public int gxTpr_Requisito_codigo_Z
      {
         get {
            return gxTv_SdtReqNegReqTec_Requisito_codigo_Z ;
         }

         set {
            gxTv_SdtReqNegReqTec_Requisito_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Requisito_codigo_Z_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Requisito_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Requisito_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ordem_Z" )]
      [  XmlElement( ElementName = "Requisito_Ordem_Z"   )]
      public short gxTpr_Requisito_ordem_Z
      {
         get {
            return gxTv_SdtReqNegReqTec_Requisito_ordem_Z ;
         }

         set {
            gxTv_SdtReqNegReqTec_Requisito_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Requisito_ordem_Z_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Requisito_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Requisito_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_PropostaCod_Z" )]
      [  XmlElement( ElementName = "ReqNegReqTec_PropostaCod_Z"   )]
      public int gxTpr_Reqnegreqtec_propostacod_Z
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_OSCod_Z" )]
      [  XmlElement( ElementName = "ReqNegReqTec_OSCod_Z"   )]
      public int gxTpr_Reqnegreqtec_oscod_Z
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Requisito_Ordem_N" )]
      [  XmlElement( ElementName = "Requisito_Ordem_N"   )]
      public short gxTpr_Requisito_ordem_N
      {
         get {
            return gxTv_SdtReqNegReqTec_Requisito_ordem_N ;
         }

         set {
            gxTv_SdtReqNegReqTec_Requisito_ordem_N = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Requisito_ordem_N_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Requisito_ordem_N = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Requisito_ordem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_PropostaCod_N" )]
      [  XmlElement( ElementName = "ReqNegReqTec_PropostaCod_N"   )]
      public short gxTpr_Reqnegreqtec_propostacod_N
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReqNegReqTec_OSCod_N" )]
      [  XmlElement( ElementName = "ReqNegReqTec_OSCod_N"   )]
      public short gxTpr_Reqnegreqtec_oscod_N
      {
         get {
            return gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N ;
         }

         set {
            gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N = (short)(value);
         }

      }

      public void gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N_SetNull( )
      {
         gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N = 0;
         return  ;
      }

      public bool gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtReqNegReqTec_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "reqnegreqtec", "GeneXus.Programs.reqnegreqtec_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtReqNegReqTec_Requisito_ordem ;
      private short gxTv_SdtReqNegReqTec_Initialized ;
      private short gxTv_SdtReqNegReqTec_Requisito_ordem_Z ;
      private short gxTv_SdtReqNegReqTec_Requisito_ordem_N ;
      private short gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_N ;
      private short gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtReqNegReqTec_Requisito_codigo ;
      private int gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod ;
      private int gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod ;
      private int gxTv_SdtReqNegReqTec_Requisito_codigo_Z ;
      private int gxTv_SdtReqNegReqTec_Reqnegreqtec_propostacod_Z ;
      private int gxTv_SdtReqNegReqTec_Reqnegreqtec_oscod_Z ;
      private long gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo ;
      private long gxTv_SdtReqNegReqTec_Solicservicoreqneg_codigo_Z ;
      private String gxTv_SdtReqNegReqTec_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ReqNegReqTec", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtReqNegReqTec_RESTInterface : GxGenericCollectionItem<SdtReqNegReqTec>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtReqNegReqTec_RESTInterface( ) : base()
      {
      }

      public SdtReqNegReqTec_RESTInterface( SdtReqNegReqTec psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SolicServicoReqNeg_Codigo" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_codigo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Solicservicoreqneg_codigo), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_codigo = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Requisito_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Requisito_codigo
      {
         get {
            return sdt.gxTpr_Requisito_codigo ;
         }

         set {
            sdt.gxTpr_Requisito_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Requisito_Ordem" , Order = 2 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Requisito_ordem
      {
         get {
            return sdt.gxTpr_Requisito_ordem ;
         }

         set {
            sdt.gxTpr_Requisito_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ReqNegReqTec_PropostaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Reqnegreqtec_propostacod
      {
         get {
            return sdt.gxTpr_Reqnegreqtec_propostacod ;
         }

         set {
            sdt.gxTpr_Reqnegreqtec_propostacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ReqNegReqTec_OSCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Reqnegreqtec_oscod
      {
         get {
            return sdt.gxTpr_Reqnegreqtec_oscod ;
         }

         set {
            sdt.gxTpr_Reqnegreqtec_oscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtReqNegReqTec sdt
      {
         get {
            return (SdtReqNegReqTec)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtReqNegReqTec() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
