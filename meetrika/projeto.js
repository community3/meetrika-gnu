/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:21:8.70
*/
gx.evt.autoSkip = false;
gx.define('projeto', false, function () {
   this.ServerClass =  "projeto" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A740Projeto_SistemaCod=gx.fn.getIntegerValue("PROJETO_SISTEMACOD",'.') ;
      this.AV7Projeto_Codigo=gx.fn.getIntegerValue("vPROJETO_CODIGO",'.') ;
      this.AV12Insert_Projeto_TipoProjetoCod=gx.fn.getIntegerValue("vINSERT_PROJETO_TIPOPROJETOCOD",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A651Projeto_TipoContagem=gx.fn.getControlValue("PROJETO_TIPOCONTAGEM") ;
      this.A652Projeto_TecnicaContagem=gx.fn.getControlValue("PROJETO_TECNICACONTAGEM") ;
      this.A1540Projeto_Introducao=gx.fn.getControlValue("PROJETO_INTRODUCAO") ;
      this.A1541Projeto_Previsao=gx.fn.getDateValue("PROJETO_PREVISAO") ;
      this.A1542Projeto_GerenteCod=gx.fn.getIntegerValue("PROJETO_GERENTECOD",'.') ;
      this.A1543Projeto_ServicoCod=gx.fn.getIntegerValue("PROJETO_SERVICOCOD",'.') ;
      this.A655Projeto_Custo=gx.fn.getDecimalValue("PROJETO_CUSTO",'.',',') ;
      this.A656Projeto_Prazo=gx.fn.getIntegerValue("PROJETO_PRAZO",'.') ;
      this.A657Projeto_Esforco=gx.fn.getIntegerValue("PROJETO_ESFORCO",'.') ;
      this.AV19Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
      this.AV8WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
   };
   this.Valid_Projeto_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Projeto_codigo",["gx.O.A648Projeto_Codigo", "gx.num.urlDecimal(gx.O.A655Projeto_Custo,\'.\',\',\')", "gx.O.A656Projeto_Prazo", "gx.O.A657Projeto_Esforco", "gx.O.A740Projeto_SistemaCod"],["A655Projeto_Custo", "A656Projeto_Prazo", "A657Projeto_Esforco", "A740Projeto_SistemaCod"]);
      return true;
   }
   this.Valid_Projeto_tipoprojetocod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Projeto_tipoprojetocod",["gx.O.A983Projeto_TipoProjetoCod"],[]);
      return true;
   }
   this.e112986_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_projetofatores.aspx",[this.AV8WWPContext.AreaTrabalho_Codigo, this.AV7Projeto_Codigo, "E", ""]), ["AV7Projeto_Codigo", "", "A985Projeto_FatorEscala"]);
      this.refreshOutputs([{av:'A985Projeto_FatorEscala',fld:'PROJETO_FATORESCALA',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]);
   };
   this.e122986_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_projetofatores.aspx",[this.AV8WWPContext.AreaTrabalho_Codigo, this.AV7Projeto_Codigo, "M", ""]), ["AV7Projeto_Codigo", "", "A989Projeto_FatorMultiplicador"]);
      this.refreshOutputs([{av:'A989Projeto_FatorMultiplicador',fld:'PROJETO_FATORMULTIPLICADOR',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]);
   };
   this.e14292_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e152986_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e162986_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,20,22,29,31,40,42,45,47,49,51,54,58,60,64,66,68,70,75,83];
   this.GXLastCtrlId =83;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Projeto", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKPROJETO_SIGLA", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_SIGLA",gxz:"Z650Projeto_Sigla",gxold:"O650Projeto_Sigla",gxvar:"A650Projeto_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A650Projeto_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z650Projeto_Sigla=Value},v2c:function(){gx.fn.setControlValue("PROJETO_SIGLA",gx.O.A650Projeto_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A650Projeto_Sigla=this.val()},val:function(){return gx.fn.getControlValue("PROJETO_SIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 18 , function() {
   });
   GXValidFnc[20]={fld:"TEXTBLOCKPROJETO_NOME", format:0,grid:0};
   GXValidFnc[22]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_NOME",gxz:"Z649Projeto_Nome",gxold:"O649Projeto_Nome",gxvar:"A649Projeto_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A649Projeto_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z649Projeto_Nome=Value},v2c:function(){gx.fn.setControlValue("PROJETO_NOME",gx.O.A649Projeto_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A649Projeto_Nome=this.val()},val:function(){return gx.fn.getControlValue("PROJETO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 22 , function() {
   });
   GXValidFnc[29]={fld:"TEXTBLOCKPROJETO_TIPOPROJETOCOD", format:0,grid:0};
   GXValidFnc[31]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Projeto_tipoprojetocod,isvalid:null,rgrid:[],fld:"PROJETO_TIPOPROJETOCOD",gxz:"Z983Projeto_TipoProjetoCod",gxold:"O983Projeto_TipoProjetoCod",gxvar:"A983Projeto_TipoProjetoCod",ucs:[],op:[],ip:[31],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A983Projeto_TipoProjetoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z983Projeto_TipoProjetoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("PROJETO_TIPOPROJETOCOD",gx.O.A983Projeto_TipoProjetoCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A983Projeto_TipoProjetoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("PROJETO_TIPOPROJETOCOD",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV12Insert_Projeto_TipoProjetoCod))}};
   this.declareDomainHdlr( 31 , function() {
   });
   GXValidFnc[40]={fld:"TEXTBLOCKPROJETO_ESCOPO", format:0,grid:0};
   GXValidFnc[42]={lvl:0,type:"vchar",len:2097152,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_ESCOPO",gxz:"Z653Projeto_Escopo",gxold:"O653Projeto_Escopo",gxvar:"A653Projeto_Escopo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A653Projeto_Escopo=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z653Projeto_Escopo=Value},v2c:function(){gx.fn.setControlValue("PROJETO_ESCOPO",gx.O.A653Projeto_Escopo,0)},c2v:function(){if(this.val()!==undefined)gx.O.A653Projeto_Escopo=this.val()},val:function(){return gx.fn.getControlValue("PROJETO_ESCOPO")},nac:gx.falseFn};
   GXValidFnc[45]={fld:"TEXTBLOCKPROJETO_STATUS", format:0,grid:0};
   GXValidFnc[47]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_STATUS",gxz:"Z658Projeto_Status",gxold:"O658Projeto_Status",gxvar:"A658Projeto_Status",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A658Projeto_Status=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z658Projeto_Status=Value},v2c:function(){gx.fn.setComboBoxValue("PROJETO_STATUS",gx.O.A658Projeto_Status);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A658Projeto_Status=this.val()},val:function(){return gx.fn.getControlValue("PROJETO_STATUS")},nac:gx.falseFn};
   this.declareDomainHdlr( 47 , function() {
   });
   GXValidFnc[49]={fld:"TEXTBLOCKPROJETO_FATORESCALA", format:0,grid:0};
   GXValidFnc[51]={fld:"TABLEMERGEDPROJETO_FATORESCALA",grid:0};
   GXValidFnc[54]={lvl:0,type:"decimal",len:6,dec:2,sign:false,pic:"ZZ9.99",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_FATORESCALA",gxz:"Z985Projeto_FatorEscala",gxold:"O985Projeto_FatorEscala",gxvar:"A985Projeto_FatorEscala",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A985Projeto_FatorEscala=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z985Projeto_FatorEscala=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("PROJETO_FATORESCALA",gx.O.A985Projeto_FatorEscala,2,',')},c2v:function(){if(this.val()!==undefined)gx.O.A985Projeto_FatorEscala=this.val()},val:function(){return gx.fn.getDecimalValue("PROJETO_FATORESCALA",'.',',')},nac:gx.falseFn};
   GXValidFnc[58]={fld:"TEXTBLOCKPROJETO_FATORMULTIPLICADOR", format:0,grid:0};
   GXValidFnc[60]={lvl:0,type:"decimal",len:6,dec:2,sign:false,pic:"ZZ9.99",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_FATORMULTIPLICADOR",gxz:"Z989Projeto_FatorMultiplicador",gxold:"O989Projeto_FatorMultiplicador",gxvar:"A989Projeto_FatorMultiplicador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A989Projeto_FatorMultiplicador=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z989Projeto_FatorMultiplicador=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("PROJETO_FATORMULTIPLICADOR",gx.O.A989Projeto_FatorMultiplicador,2,',')},c2v:function(){if(this.val()!==undefined)gx.O.A989Projeto_FatorMultiplicador=this.val()},val:function(){return gx.fn.getDecimalValue("PROJETO_FATORMULTIPLICADOR",'.',',')},nac:gx.falseFn};
   GXValidFnc[64]={fld:"TEXTBLOCKPROJETO_CONSTACOCOMO", format:0,grid:0};
   GXValidFnc[66]={lvl:0,type:"decimal",len:6,dec:2,sign:false,pic:"ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_CONSTACOCOMO",gxz:"Z990Projeto_ConstACocomo",gxold:"O990Projeto_ConstACocomo",gxvar:"A990Projeto_ConstACocomo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A990Projeto_ConstACocomo=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z990Projeto_ConstACocomo=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("PROJETO_CONSTACOCOMO",gx.O.A990Projeto_ConstACocomo,2,',')},c2v:function(){if(this.val()!==undefined)gx.O.A990Projeto_ConstACocomo=this.val()},val:function(){return gx.fn.getDecimalValue("PROJETO_CONSTACOCOMO",'.',',')},nac:gx.falseFn};
   GXValidFnc[68]={fld:"TEXTBLOCKPROJETO_INCREMENTAL", format:0,grid:0};
   GXValidFnc[70]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"PROJETO_INCREMENTAL",gxz:"Z1232Projeto_Incremental",gxold:"O1232Projeto_Incremental",gxvar:"A1232Projeto_Incremental",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A1232Projeto_Incremental=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1232Projeto_Incremental=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("PROJETO_INCREMENTAL",gx.O.A1232Projeto_Incremental,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1232Projeto_Incremental=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("PROJETO_INCREMENTAL")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 70 , function() {
   });
   GXValidFnc[75]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[83]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Projeto_codigo,isvalid:null,rgrid:[],fld:"PROJETO_CODIGO",gxz:"Z648Projeto_Codigo",gxold:"O648Projeto_Codigo",gxvar:"A648Projeto_Codigo",ucs:[],op:[],ip:[83],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A648Projeto_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z648Projeto_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("PROJETO_CODIGO",gx.O.A648Projeto_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A648Projeto_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("PROJETO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 83 , function() {
   });
   this.A650Projeto_Sigla = "" ;
   this.Z650Projeto_Sigla = "" ;
   this.O650Projeto_Sigla = "" ;
   this.A649Projeto_Nome = "" ;
   this.Z649Projeto_Nome = "" ;
   this.O649Projeto_Nome = "" ;
   this.A983Projeto_TipoProjetoCod = 0 ;
   this.Z983Projeto_TipoProjetoCod = 0 ;
   this.O983Projeto_TipoProjetoCod = 0 ;
   this.A653Projeto_Escopo = "" ;
   this.Z653Projeto_Escopo = "" ;
   this.O653Projeto_Escopo = "" ;
   this.A658Projeto_Status = "" ;
   this.Z658Projeto_Status = "" ;
   this.O658Projeto_Status = "" ;
   this.A985Projeto_FatorEscala = 0 ;
   this.Z985Projeto_FatorEscala = 0 ;
   this.O985Projeto_FatorEscala = 0 ;
   this.A989Projeto_FatorMultiplicador = 0 ;
   this.Z989Projeto_FatorMultiplicador = 0 ;
   this.O989Projeto_FatorMultiplicador = 0 ;
   this.A990Projeto_ConstACocomo = 0 ;
   this.Z990Projeto_ConstACocomo = 0 ;
   this.O990Projeto_ConstACocomo = 0 ;
   this.A1232Projeto_Incremental = false ;
   this.Z1232Projeto_Incremental = false ;
   this.O1232Projeto_Incremental = false ;
   this.A648Projeto_Codigo = 0 ;
   this.Z648Projeto_Codigo = 0 ;
   this.O648Projeto_Codigo = 0 ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV20GXV1 = 0 ;
   this.AV12Insert_Projeto_TipoProjetoCod = 0 ;
   this.AV16FatorEscala = 0 ;
   this.AV15FatorMultiplicador = 0 ;
   this.AV13TrnContextAtt = {} ;
   this.AV7Projeto_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A648Projeto_Codigo = 0 ;
   this.A983Projeto_TipoProjetoCod = 0 ;
   this.Gx_BScreen = 0 ;
   this.AV19Pgmname = "" ;
   this.A740Projeto_SistemaCod = 0 ;
   this.A649Projeto_Nome = "" ;
   this.A650Projeto_Sigla = "" ;
   this.A651Projeto_TipoContagem = "" ;
   this.A652Projeto_TecnicaContagem = "" ;
   this.A1540Projeto_Introducao = "" ;
   this.A653Projeto_Escopo = "" ;
   this.A1541Projeto_Previsao = gx.date.nullDate() ;
   this.A1542Projeto_GerenteCod = 0 ;
   this.A1543Projeto_ServicoCod = 0 ;
   this.A655Projeto_Custo = 0 ;
   this.A656Projeto_Prazo = 0 ;
   this.A657Projeto_Esforco = 0 ;
   this.A658Projeto_Status = "" ;
   this.A985Projeto_FatorEscala = 0 ;
   this.A989Projeto_FatorMultiplicador = 0 ;
   this.A990Projeto_ConstACocomo = 0 ;
   this.A1232Projeto_Incremental = false ;
   this.Gx_mode = "" ;
   this.Events = {"e14292_client": ["AFTER TRN", true] ,"e152986_client": ["ENTER", true] ,"e162986_client": ["CANCEL", true] ,"e112986_client": ["'DOFATORESCALA'", false] ,"e122986_client": ["'DOFATORMULTIPLICADOR'", false]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[]];
   this.EvtParms["'DOFATORESCALA'"] = [[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'A985Projeto_FatorEscala',fld:'PROJETO_FATORESCALA',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["'DOFATORMULTIPLICADOR'"] = [[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'A989Projeto_FatorMultiplicador',fld:'PROJETO_FATORMULTIPLICADOR',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("A740Projeto_SistemaCod", "PROJETO_SISTEMACOD", 0, "int");
   this.setVCMap("AV7Projeto_Codigo", "vPROJETO_CODIGO", 0, "int");
   this.setVCMap("AV12Insert_Projeto_TipoProjetoCod", "vINSERT_PROJETO_TIPOPROJETOCOD", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A651Projeto_TipoContagem", "PROJETO_TIPOCONTAGEM", 0, "char");
   this.setVCMap("A652Projeto_TecnicaContagem", "PROJETO_TECNICACONTAGEM", 0, "char");
   this.setVCMap("A1540Projeto_Introducao", "PROJETO_INTRODUCAO", 0, "vchar");
   this.setVCMap("A1541Projeto_Previsao", "PROJETO_PREVISAO", 0, "date");
   this.setVCMap("A1542Projeto_GerenteCod", "PROJETO_GERENTECOD", 0, "int");
   this.setVCMap("A1543Projeto_ServicoCod", "PROJETO_SERVICOCOD", 0, "int");
   this.setVCMap("A655Projeto_Custo", "PROJETO_CUSTO", 0, "decimal");
   this.setVCMap("A656Projeto_Prazo", "PROJETO_PRAZO", 0, "int");
   this.setVCMap("A657Projeto_Esforco", "PROJETO_ESFORCO", 0, "int");
   this.setVCMap("AV19Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.setVCMap("AV8WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(projeto);
