/*
               File: GetWWPadroesArtefatosFilterData
        Description: Get WWPadroes Artefatos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:52.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwpadroesartefatosfilterdata : GXProcedure
   {
      public getwwpadroesartefatosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwpadroesartefatosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwpadroesartefatosfilterdata objgetwwpadroesartefatosfilterdata;
         objgetwwpadroesartefatosfilterdata = new getwwpadroesartefatosfilterdata();
         objgetwwpadroesartefatosfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwpadroesartefatosfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwpadroesartefatosfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwpadroesartefatosfilterdata.AV20OptionsJson = "" ;
         objgetwwpadroesartefatosfilterdata.AV23OptionsDescJson = "" ;
         objgetwwpadroesartefatosfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwpadroesartefatosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwpadroesartefatosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwpadroesartefatosfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwpadroesartefatosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_PADROESARTEFATOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADPADROESARTEFATOS_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_METODOLOGIAFASES_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIAFASES_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWPadroesArtefatosGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWPadroesArtefatosGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWPadroesArtefatosGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFPADROESARTEFATOS_DESCRICAO") == 0 )
            {
               AV10TFPadroesArtefatos_Descricao = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFPADROESARTEFATOS_DESCRICAO_SEL") == 0 )
            {
               AV11TFPadroesArtefatos_Descricao_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME") == 0 )
            {
               AV12TFMetodologiaFases_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIAFASES_NOME_SEL") == 0 )
            {
               AV13TFMetodologiaFases_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34PadroesArtefatos_Descricao1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV35MetodologiaFases_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV39PadroesArtefatos_Descricao2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV40MetodologiaFases_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPADROESARTEFATOS_DESCRICAOOPTIONS' Routine */
         AV10TFPadroesArtefatos_Descricao = AV14SearchTxt;
         AV11TFPadroesArtefatos_Descricao_Sel = "";
         AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = AV34PadroesArtefatos_Descricao1;
         AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = AV35MetodologiaFases_Nome1;
         AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 = AV38DynamicFiltersOperator2;
         AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = AV39PadroesArtefatos_Descricao2;
         AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = AV40MetodologiaFases_Nome2;
         AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = AV10TFPadroesArtefatos_Descricao;
         AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel = AV11TFPadroesArtefatos_Descricao_Sel;
         AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = AV12TFMetodologiaFases_Nome;
         AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel = AV13TFMetodologiaFases_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 ,
                                              AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 ,
                                              AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ,
                                              AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ,
                                              AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 ,
                                              AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 ,
                                              AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 ,
                                              AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ,
                                              AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ,
                                              AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel ,
                                              AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ,
                                              AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel ,
                                              AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ,
                                              A151PadroesArtefatos_Descricao ,
                                              A148MetodologiaFases_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1), "%", "");
         lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1), "%", "");
         lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1), 50, "%");
         lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1), 50, "%");
         lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2), "%", "");
         lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2), "%", "");
         lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2), 50, "%");
         lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2), 50, "%");
         lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = StringUtil.Concat( StringUtil.RTrim( AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao), "%", "");
         lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome), 50, "%");
         /* Using cursor P00L02 */
         pr_default.execute(0, new Object[] {lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1, lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1, lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1, lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1, lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2, lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2, lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2, lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2, lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao, AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel, lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome, AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKL02 = false;
            A147MetodologiaFases_Codigo = P00L02_A147MetodologiaFases_Codigo[0];
            A151PadroesArtefatos_Descricao = P00L02_A151PadroesArtefatos_Descricao[0];
            A148MetodologiaFases_Nome = P00L02_A148MetodologiaFases_Nome[0];
            A150PadroesArtefatos_Codigo = P00L02_A150PadroesArtefatos_Codigo[0];
            A148MetodologiaFases_Nome = P00L02_A148MetodologiaFases_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00L02_A151PadroesArtefatos_Descricao[0], A151PadroesArtefatos_Descricao) == 0 ) )
            {
               BRKL02 = false;
               A150PadroesArtefatos_Codigo = P00L02_A150PadroesArtefatos_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKL02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A151PadroesArtefatos_Descricao)) )
            {
               AV18Option = A151PadroesArtefatos_Descricao;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL02 )
            {
               BRKL02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMETODOLOGIAFASES_NOMEOPTIONS' Routine */
         AV12TFMetodologiaFases_Nome = AV14SearchTxt;
         AV13TFMetodologiaFases_Nome_Sel = "";
         AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 = AV33DynamicFiltersOperator1;
         AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = AV34PadroesArtefatos_Descricao1;
         AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = AV35MetodologiaFases_Nome1;
         AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 = AV38DynamicFiltersOperator2;
         AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = AV39PadroesArtefatos_Descricao2;
         AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = AV40MetodologiaFases_Nome2;
         AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = AV10TFPadroesArtefatos_Descricao;
         AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel = AV11TFPadroesArtefatos_Descricao_Sel;
         AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = AV12TFMetodologiaFases_Nome;
         AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel = AV13TFMetodologiaFases_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 ,
                                              AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 ,
                                              AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ,
                                              AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ,
                                              AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 ,
                                              AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 ,
                                              AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 ,
                                              AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ,
                                              AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ,
                                              AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel ,
                                              AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ,
                                              AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel ,
                                              AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ,
                                              A151PadroesArtefatos_Descricao ,
                                              A148MetodologiaFases_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1), "%", "");
         lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1), "%", "");
         lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1), 50, "%");
         lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1), 50, "%");
         lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2), "%", "");
         lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2), "%", "");
         lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2), 50, "%");
         lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = StringUtil.PadR( StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2), 50, "%");
         lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = StringUtil.Concat( StringUtil.RTrim( AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao), "%", "");
         lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome), 50, "%");
         /* Using cursor P00L03 */
         pr_default.execute(1, new Object[] {lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1, lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1, lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1, lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1, lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2, lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2, lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2, lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2, lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao, AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel, lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome, AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKL04 = false;
            A147MetodologiaFases_Codigo = P00L03_A147MetodologiaFases_Codigo[0];
            A148MetodologiaFases_Nome = P00L03_A148MetodologiaFases_Nome[0];
            A151PadroesArtefatos_Descricao = P00L03_A151PadroesArtefatos_Descricao[0];
            A150PadroesArtefatos_Codigo = P00L03_A150PadroesArtefatos_Codigo[0];
            A148MetodologiaFases_Nome = P00L03_A148MetodologiaFases_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00L03_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
            {
               BRKL04 = false;
               A150PadroesArtefatos_Codigo = P00L03_A150PadroesArtefatos_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKL04 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A148MetodologiaFases_Nome)) )
            {
               AV18Option = A148MetodologiaFases_Nome;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV19Options.Item(AV17InsertIndex)), AV18Option) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL04 )
            {
               BRKL04 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFPadroesArtefatos_Descricao = "";
         AV11TFPadroesArtefatos_Descricao_Sel = "";
         AV12TFMetodologiaFases_Nome = "";
         AV13TFMetodologiaFases_Nome_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34PadroesArtefatos_Descricao1 = "";
         AV35MetodologiaFases_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV39PadroesArtefatos_Descricao2 = "";
         AV40MetodologiaFases_Nome2 = "";
         AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 = "";
         AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = "";
         AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = "";
         AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 = "";
         AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = "";
         AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = "";
         AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = "";
         AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel = "";
         AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = "";
         AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel = "";
         scmdbuf = "";
         lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 = "";
         lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 = "";
         lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 = "";
         lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 = "";
         lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao = "";
         lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome = "";
         A151PadroesArtefatos_Descricao = "";
         A148MetodologiaFases_Nome = "";
         P00L02_A147MetodologiaFases_Codigo = new int[1] ;
         P00L02_A151PadroesArtefatos_Descricao = new String[] {""} ;
         P00L02_A148MetodologiaFases_Nome = new String[] {""} ;
         P00L02_A150PadroesArtefatos_Codigo = new int[1] ;
         AV18Option = "";
         P00L03_A147MetodologiaFases_Codigo = new int[1] ;
         P00L03_A148MetodologiaFases_Nome = new String[] {""} ;
         P00L03_A151PadroesArtefatos_Descricao = new String[] {""} ;
         P00L03_A150PadroesArtefatos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwpadroesartefatosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00L02_A147MetodologiaFases_Codigo, P00L02_A151PadroesArtefatos_Descricao, P00L02_A148MetodologiaFases_Nome, P00L02_A150PadroesArtefatos_Codigo
               }
               , new Object[] {
               P00L03_A147MetodologiaFases_Codigo, P00L03_A148MetodologiaFases_Nome, P00L03_A151PadroesArtefatos_Descricao, P00L03_A150PadroesArtefatos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV38DynamicFiltersOperator2 ;
      private short AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 ;
      private short AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 ;
      private int AV43GXV1 ;
      private int A147MetodologiaFases_Codigo ;
      private int A150PadroesArtefatos_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV12TFMetodologiaFases_Nome ;
      private String AV13TFMetodologiaFases_Nome_Sel ;
      private String AV35MetodologiaFases_Nome1 ;
      private String AV40MetodologiaFases_Nome2 ;
      private String AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ;
      private String AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ;
      private String AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ;
      private String AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel ;
      private String scmdbuf ;
      private String lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ;
      private String lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ;
      private String lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ;
      private String A148MetodologiaFases_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 ;
      private bool BRKL02 ;
      private bool BRKL04 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFPadroesArtefatos_Descricao ;
      private String AV11TFPadroesArtefatos_Descricao_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34PadroesArtefatos_Descricao1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV39PadroesArtefatos_Descricao2 ;
      private String AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 ;
      private String AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ;
      private String AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 ;
      private String AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ;
      private String AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ;
      private String AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel ;
      private String lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ;
      private String lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ;
      private String lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ;
      private String A151PadroesArtefatos_Descricao ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00L02_A147MetodologiaFases_Codigo ;
      private String[] P00L02_A151PadroesArtefatos_Descricao ;
      private String[] P00L02_A148MetodologiaFases_Nome ;
      private int[] P00L02_A150PadroesArtefatos_Codigo ;
      private int[] P00L03_A147MetodologiaFases_Codigo ;
      private String[] P00L03_A148MetodologiaFases_Nome ;
      private String[] P00L03_A151PadroesArtefatos_Descricao ;
      private int[] P00L03_A150PadroesArtefatos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwpadroesartefatosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00L02( IGxContext context ,
                                             String AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 ,
                                             short AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 ,
                                             String AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ,
                                             String AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ,
                                             bool AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 ,
                                             String AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 ,
                                             short AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 ,
                                             String AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ,
                                             String AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ,
                                             String AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel ,
                                             String AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ,
                                             String AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel ,
                                             String AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ,
                                             String A151PadroesArtefatos_Descricao ,
                                             String A148MetodologiaFases_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[MetodologiaFases_Codigo], T1.[PadroesArtefatos_Descricao], T2.[MetodologiaFases_Nome], T1.[PadroesArtefatos_Codigo] FROM ([PadroesArtefatos] T1 WITH (NOLOCK) INNER JOIN [MetodologiaFases] T2 WITH (NOLOCK) ON T2.[MetodologiaFases_Codigo] = T1.[MetodologiaFases_Codigo])";
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] = @AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] = @AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] = @AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] = @AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[PadroesArtefatos_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00L03( IGxContext context ,
                                             String AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1 ,
                                             short AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 ,
                                             String AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1 ,
                                             String AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1 ,
                                             bool AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 ,
                                             String AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2 ,
                                             short AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 ,
                                             String AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2 ,
                                             String AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2 ,
                                             String AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel ,
                                             String AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao ,
                                             String AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel ,
                                             String AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome ,
                                             String A151PadroesArtefatos_Descricao ,
                                             String A148MetodologiaFases_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[MetodologiaFases_Codigo], T2.[MetodologiaFases_Nome], T1.[PadroesArtefatos_Descricao], T1.[PadroesArtefatos_Codigo] FROM ([PadroesArtefatos] T1 WITH (NOLOCK) INNER JOIN [MetodologiaFases] T2 WITH (NOLOCK) ON T2.[MetodologiaFases_Codigo] = T1.[MetodologiaFases_Codigo])";
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWPadroesArtefatosDS_1_Dynamicfiltersselector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV46WWPadroesArtefatosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV49WWPadroesArtefatosDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWPadroesArtefatosDS_6_Dynamicfiltersselector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV51WWPadroesArtefatosDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] = @AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] = @AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] = @AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] = @AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[MetodologiaFases_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00L02(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] );
               case 1 :
                     return conditional_P00L03(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00L02 ;
          prmP00L02 = new Object[] {
          new Object[] {"@lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00L03 ;
          prmP00L03 = new Object[] {
          new Object[] {"@lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV47WWPadroesArtefatosDS_3_Padroesartefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48WWPadroesArtefatosDS_4_Metodologiafases_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52WWPadroesArtefatosDS_8_Padroesartefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWPadroesArtefatosDS_9_Metodologiafases_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWPadroesArtefatosDS_10_Tfpadroesartefatos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV55WWPadroesArtefatosDS_11_Tfpadroesartefatos_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWPadroesArtefatosDS_12_Tfmetodologiafases_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWPadroesArtefatosDS_13_Tfmetodologiafases_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00L02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L02,100,0,true,false )
             ,new CursorDef("P00L03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L03,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwpadroesartefatosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwpadroesartefatosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwpadroesartefatosfilterdata") )
          {
             return  ;
          }
          getwwpadroesartefatosfilterdata worker = new getwwpadroesartefatosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
