/*
               File: REL_CompromissosAssumidos
        Description: Compromissos Assumidos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:4:51.37
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_compromissosassumidos : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV37Linha5 = NumberUtil.Val( GetNextPar( ), ".");
                  AV51TodasAsAreas = (bool)(BooleanUtil.Val(GetNextPar( )));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_compromissosassumidos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_compromissosassumidos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           decimal aP1_Linha5 ,
                           bool aP2_TodasAsAreas )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV37Linha5 = aP1_Linha5;
         this.AV51TodasAsAreas = aP2_TodasAsAreas;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.AV8AreaTrabalho_Codigo;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 decimal aP1_Linha5 ,
                                 bool aP2_TodasAsAreas )
      {
         arel_compromissosassumidos objarel_compromissosassumidos;
         objarel_compromissosassumidos = new arel_compromissosassumidos();
         objarel_compromissosassumidos.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objarel_compromissosassumidos.AV37Linha5 = aP1_Linha5;
         objarel_compromissosassumidos.AV51TodasAsAreas = aP2_TodasAsAreas;
         objarel_compromissosassumidos.context.SetSubmitInitialConfig(context);
         objarel_compromissosassumidos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_compromissosassumidos);
         aP0_AreaTrabalho_Codigo=this.AV8AreaTrabalho_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_compromissosassumidos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 2;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 256, 11909, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*2));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV30Hoje = DateTimeUtil.ServerDate( context, "DEFAULT");
            AV40Linha8 = "N�o";
            /* Using cursor P00DN2 */
            pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A29Contratante_Codigo = P00DN2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00DN2_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = P00DN2_A335Contratante_PessoaCod[0];
               A5AreaTrabalho_Codigo = P00DN2_A5AreaTrabalho_Codigo[0];
               A1125Contratante_LogoNomeArq = P00DN2_A1125Contratante_LogoNomeArq[0];
               n1125Contratante_LogoNomeArq = P00DN2_n1125Contratante_LogoNomeArq[0];
               A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
               A1126Contratante_LogoTipoArq = P00DN2_A1126Contratante_LogoTipoArq[0];
               n1126Contratante_LogoTipoArq = P00DN2_n1126Contratante_LogoTipoArq[0];
               A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
               A1805Contratante_AtivoCirculante = P00DN2_A1805Contratante_AtivoCirculante[0];
               n1805Contratante_AtivoCirculante = P00DN2_n1805Contratante_AtivoCirculante[0];
               A1806Contratante_PassivoCirculante = P00DN2_A1806Contratante_PassivoCirculante[0];
               n1806Contratante_PassivoCirculante = P00DN2_n1806Contratante_PassivoCirculante[0];
               A1807Contratante_PatrimonioLiquido = P00DN2_A1807Contratante_PatrimonioLiquido[0];
               n1807Contratante_PatrimonioLiquido = P00DN2_n1807Contratante_PatrimonioLiquido[0];
               A1808Contratante_ReceitaBruta = P00DN2_A1808Contratante_ReceitaBruta[0];
               n1808Contratante_ReceitaBruta = P00DN2_n1808Contratante_ReceitaBruta[0];
               A12Contratante_CNPJ = P00DN2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00DN2_n12Contratante_CNPJ[0];
               A10Contratante_NomeFantasia = P00DN2_A10Contratante_NomeFantasia[0];
               A1124Contratante_LogoArquivo = P00DN2_A1124Contratante_LogoArquivo[0];
               n1124Contratante_LogoArquivo = P00DN2_n1124Contratante_LogoArquivo[0];
               A335Contratante_PessoaCod = P00DN2_A335Contratante_PessoaCod[0];
               A1125Contratante_LogoNomeArq = P00DN2_A1125Contratante_LogoNomeArq[0];
               n1125Contratante_LogoNomeArq = P00DN2_n1125Contratante_LogoNomeArq[0];
               A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
               A1126Contratante_LogoTipoArq = P00DN2_A1126Contratante_LogoTipoArq[0];
               n1126Contratante_LogoTipoArq = P00DN2_n1126Contratante_LogoTipoArq[0];
               A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
               A1805Contratante_AtivoCirculante = P00DN2_A1805Contratante_AtivoCirculante[0];
               n1805Contratante_AtivoCirculante = P00DN2_n1805Contratante_AtivoCirculante[0];
               A1806Contratante_PassivoCirculante = P00DN2_A1806Contratante_PassivoCirculante[0];
               n1806Contratante_PassivoCirculante = P00DN2_n1806Contratante_PassivoCirculante[0];
               A1807Contratante_PatrimonioLiquido = P00DN2_A1807Contratante_PatrimonioLiquido[0];
               n1807Contratante_PatrimonioLiquido = P00DN2_n1807Contratante_PatrimonioLiquido[0];
               A1808Contratante_ReceitaBruta = P00DN2_A1808Contratante_ReceitaBruta[0];
               n1808Contratante_ReceitaBruta = P00DN2_n1808Contratante_ReceitaBruta[0];
               A10Contratante_NomeFantasia = P00DN2_A10Contratante_NomeFantasia[0];
               A1124Contratante_LogoArquivo = P00DN2_A1124Contratante_LogoArquivo[0];
               n1124Contratante_LogoArquivo = P00DN2_n1124Contratante_LogoArquivo[0];
               A12Contratante_CNPJ = P00DN2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = P00DN2_n12Contratante_CNPJ[0];
               AV35Linha3 = A1805Contratante_AtivoCirculante;
               AV36Linha4 = A1806Contratante_PassivoCirculante;
               AV39Linha7 = A1807Contratante_PatrimonioLiquido;
               AV41Linha9 = A1808Contratante_ReceitaBruta;
               AV24Contratante_Logo = A1124Contratante_LogoArquivo;
               A40000Contratante_Logo_GXI = GeneXus.Utils.GXDbFile.GetUriFromFile( A1125Contratante_LogoNomeArq, A1126Contratante_LogoTipoArq);
               AV49Contratante_CNPJ = (long)(NumberUtil.Val( A12Contratante_CNPJ, "."));
               AV50ContratanteInfo = "Contratante: " + StringUtil.Trim( A10Contratante_NomeFantasia) + "  CNPJ: " + context.localUtil.Format( (decimal)(AV49Contratante_CNPJ), "99.999.999/9999-99");
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV51TodasAsAreas ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV8AreaTrabalho_Codigo ,
                                                 A83Contrato_DataVigenciaTermino ,
                                                 A843Contrato_DataFimTA ,
                                                 AV30Hoje ,
                                                 A92Contrato_Ativo ,
                                                 A43Contratada_Ativo },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00DN7 */
            pr_default.execute(1, new Object[] {AV30Hoje, AV30Hoje, AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A39Contratada_Codigo = P00DN7_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00DN7_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00DN7_n74Contrato_Codigo[0];
               A43Contratada_Ativo = P00DN7_A43Contratada_Ativo[0];
               A92Contrato_Ativo = P00DN7_A92Contrato_Ativo[0];
               A83Contrato_DataVigenciaTermino = P00DN7_A83Contrato_DataVigenciaTermino[0];
               A52Contratada_AreaTrabalhoCod = P00DN7_A52Contratada_AreaTrabalhoCod[0];
               A82Contrato_DataVigenciaInicio = P00DN7_A82Contrato_DataVigenciaInicio[0];
               A53Contratada_AreaTrabalhoDes = P00DN7_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = P00DN7_n53Contratada_AreaTrabalhoDes[0];
               A843Contrato_DataFimTA = P00DN7_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = P00DN7_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = P00DN7_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00DN7_n842Contrato_DataInicioTA[0];
               A43Contratada_Ativo = P00DN7_A43Contratada_Ativo[0];
               A52Contratada_AreaTrabalhoCod = P00DN7_A52Contratada_AreaTrabalhoCod[0];
               A53Contratada_AreaTrabalhoDes = P00DN7_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = P00DN7_n53Contratada_AreaTrabalhoDes[0];
               A843Contrato_DataFimTA = P00DN7_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = P00DN7_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = P00DN7_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00DN7_n842Contrato_DataInicioTA[0];
               if ( ( A82Contrato_DataVigenciaInicio > A842Contrato_DataInicioTA ) && (DateTime.MinValue==AV47Vigencia) )
               {
                  AV47Vigencia = A82Contrato_DataVigenciaInicio;
               }
               else if ( ( A842Contrato_DataInicioTA > A82Contrato_DataVigenciaInicio ) && (DateTime.MinValue==AV47Vigencia) )
               {
                  AV47Vigencia = A842Contrato_DataInicioTA;
               }
               else if ( ( A82Contrato_DataVigenciaInicio > A842Contrato_DataInicioTA ) && ( AV47Vigencia > A82Contrato_DataVigenciaInicio ) )
               {
                  AV47Vigencia = A82Contrato_DataVigenciaInicio;
               }
               else if ( ( A842Contrato_DataInicioTA > A82Contrato_DataVigenciaInicio ) && ( AV47Vigencia > A842Contrato_DataInicioTA ) )
               {
                  AV47Vigencia = A842Contrato_DataInicioTA;
               }
               if ( ( A83Contrato_DataVigenciaTermino > A843Contrato_DataFimTA ) && ( AV48Vigencia_To < A83Contrato_DataVigenciaTermino ) )
               {
                  AV48Vigencia_To = A83Contrato_DataVigenciaTermino;
               }
               else if ( ( A843Contrato_DataFimTA > A83Contrato_DataVigenciaTermino ) && ( AV48Vigencia_To < A843Contrato_DataFimTA ) )
               {
                  AV48Vigencia_To = A843Contrato_DataFimTA;
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            AV29HeaderPeriodo = "Periodo de Execu�� Contratual: " + context.localUtil.DToC( AV47Vigencia, 2, "/") + " a " + context.localUtil.DToC( AV48Vigencia_To, 2, "/");
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV51TodasAsAreas ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV8AreaTrabalho_Codigo ,
                                                 A83Contrato_DataVigenciaTermino ,
                                                 A843Contrato_DataFimTA ,
                                                 AV30Hoje ,
                                                 A92Contrato_Ativo ,
                                                 A43Contratada_Ativo },
                                                 new int[] {
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00DN12 */
            pr_default.execute(2, new Object[] {AV30Hoje, AV30Hoje, AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A74Contrato_Codigo = P00DN12_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00DN12_n74Contrato_Codigo[0];
               A43Contratada_Ativo = P00DN12_A43Contratada_Ativo[0];
               A92Contrato_Ativo = P00DN12_A92Contrato_Ativo[0];
               A83Contrato_DataVigenciaTermino = P00DN12_A83Contrato_DataVigenciaTermino[0];
               A52Contratada_AreaTrabalhoCod = P00DN12_A52Contratada_AreaTrabalhoCod[0];
               A82Contrato_DataVigenciaInicio = P00DN12_A82Contrato_DataVigenciaInicio[0];
               A85Contrato_DataAssinatura = P00DN12_A85Contrato_DataAssinatura[0];
               A116Contrato_ValorUnidadeContratacao = P00DN12_A116Contrato_ValorUnidadeContratacao[0];
               A81Contrato_Quantidade = P00DN12_A81Contrato_Quantidade[0];
               A39Contratada_Codigo = P00DN12_A39Contratada_Codigo[0];
               A53Contratada_AreaTrabalhoDes = P00DN12_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = P00DN12_n53Contratada_AreaTrabalhoDes[0];
               A438Contratada_Sigla = P00DN12_A438Contratada_Sigla[0];
               A843Contrato_DataFimTA = P00DN12_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = P00DN12_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = P00DN12_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00DN12_n842Contrato_DataInicioTA[0];
               A843Contrato_DataFimTA = P00DN12_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = P00DN12_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = P00DN12_A842Contrato_DataInicioTA[0];
               n842Contrato_DataInicioTA = P00DN12_n842Contrato_DataInicioTA[0];
               A43Contratada_Ativo = P00DN12_A43Contratada_Ativo[0];
               A52Contratada_AreaTrabalhoCod = P00DN12_A52Contratada_AreaTrabalhoCod[0];
               A438Contratada_Sigla = P00DN12_A438Contratada_Sigla[0];
               A53Contratada_AreaTrabalhoDes = P00DN12_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = P00DN12_n53Contratada_AreaTrabalhoDes[0];
               AV28Empresa = A438Contratada_Sigla;
               AV9Coluna1 = A82Contrato_DataVigenciaInicio;
               AV14Coluna2 = A85Contrato_DataAssinatura;
               if ( A843Contrato_DataFimTA > A83Contrato_DataVigenciaTermino )
               {
                  AV15Coluna3 = A843Contrato_DataFimTA;
               }
               else
               {
                  AV15Coluna3 = A83Contrato_DataVigenciaTermino;
               }
               AV16Coluna4 = (decimal)(DateTimeUtil.DDiff(AV15Coluna3,AV30Hoje));
               AV17Coluna5 = A116Contrato_ValorUnidadeContratacao;
               AV18Coluna6 = (decimal)(A81Contrato_Quantidade);
               AV11Coluna11 = (decimal)(AV17Coluna5*AV18Coluna6);
               AV47Vigencia = DateTime.MinValue;
               AV48Vigencia_To = DateTime.MinValue;
               if ( ( A82Contrato_DataVigenciaInicio > A842Contrato_DataInicioTA ) && (DateTime.MinValue==AV47Vigencia) )
               {
                  AV47Vigencia = A82Contrato_DataVigenciaInicio;
               }
               else if ( ( A842Contrato_DataInicioTA > A82Contrato_DataVigenciaInicio ) && (DateTime.MinValue==AV47Vigencia) )
               {
                  AV47Vigencia = A842Contrato_DataInicioTA;
               }
               else if ( ( A82Contrato_DataVigenciaInicio > A842Contrato_DataInicioTA ) && ( AV47Vigencia > A82Contrato_DataVigenciaInicio ) )
               {
                  AV47Vigencia = A82Contrato_DataVigenciaInicio;
               }
               else if ( ( A842Contrato_DataInicioTA > A82Contrato_DataVigenciaInicio ) && ( AV47Vigencia > A842Contrato_DataInicioTA ) )
               {
                  AV47Vigencia = A842Contrato_DataInicioTA;
               }
               if ( ( A83Contrato_DataVigenciaTermino > A843Contrato_DataFimTA ) && ( AV48Vigencia_To < A83Contrato_DataVigenciaTermino ) )
               {
                  AV48Vigencia_To = A83Contrato_DataVigenciaTermino;
               }
               else if ( ( A843Contrato_DataFimTA > A83Contrato_DataVigenciaTermino ) && ( AV48Vigencia_To < A843Contrato_DataFimTA ) )
               {
                  AV48Vigencia_To = A843Contrato_DataFimTA;
               }
               AV22Contratada_Codigo = A39Contratada_Codigo;
               /* Execute user subroutine: 'EXECUTADO' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  this.cleanup();
                  if (true) return;
               }
               if ( ( AV18Coluna6 > Convert.ToDecimal( 0 )) )
               {
                  AV20Coluna8 = (decimal)((AV19Coluna7/ (decimal)(AV18Coluna6))*100);
               }
               AV21Coluna9 = (decimal)(AV18Coluna6-AV19Coluna7);
               AV44Meses = (decimal)((DateTimeUtil.DDiff(AV15Coluna3,AV30Hoje))/ (decimal)(30));
               AV10Coluna10 = (decimal)(AV21Coluna9/ (decimal)(AV44Meses));
               AV13Coluna13 = (decimal)(AV11Coluna11-AV12Coluna12);
               AV46Total = (decimal)(AV46Total+AV13Coluna13);
               HDN0( false, 17) ;
               getPrinter().GxDrawRect(708, Gx_line+0, 783, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(783, Gx_line+0, 858, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(517, Gx_line+0, 592, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(592, Gx_line+0, 667, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(1008, Gx_line+0, 1083, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(858, Gx_line+0, 933, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(933, Gx_line+0, 1008, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(667, Gx_line+0, 708, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(142, Gx_line+0, 217, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(367, Gx_line+0, 442, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(442, Gx_line+0, 517, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(217, Gx_line+0, 292, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(292, Gx_line+0, 367, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(0, Gx_line+0, 143, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV18Coluna6, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 517, Gx_line+1, 589, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV19Coluna7, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 592, Gx_line+1, 664, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV21Coluna9, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 708, Gx_line+1, 780, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV10Coluna10, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 783, Gx_line+1, 855, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV11Coluna11, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 858, Gx_line+1, 930, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV12Coluna12, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 933, Gx_line+1, 1005, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV13Coluna13, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1008, Gx_line+1, 1080, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV28Empresa, "@!")), 2, Gx_line+1, 142, Gx_line+15, 0, 0, 0, 2) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV9Coluna1, "99/99/99"), 142, Gx_line+0, 214, Gx_line+15, 1, 0, 0, 2) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV14Coluna2, "99/99/99"), 217, Gx_line+0, 289, Gx_line+15, 1, 0, 0, 2) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV15Coluna3, "99/99/99"), 292, Gx_line+0, 364, Gx_line+15, 1, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV16Coluna4, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 367, Gx_line+0, 439, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV17Coluna5, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 442, Gx_line+0, 514, Gx_line+15, 2, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV20Coluna8, "ZZ9.99")), 668, Gx_line+0, 707, Gx_line+15, 2+256, 0, 0, 2) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+17);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            AV34Linha2 = (decimal)(AV46Total/ (decimal)(12));
            if ( ( ( AV35Linha3 - AV36Linha4 ) > Convert.ToDecimal( 0 )) )
            {
               AV38Linha6 = (decimal)((AV37Linha5/ (decimal)((AV35Linha3-AV36Linha4)))*100);
            }
            if ( ( AV39Linha7 > Convert.ToDecimal( 0 )) )
            {
               AV33Linha11 = (decimal)((AV37Linha5/ (decimal)(AV39Linha7))*100);
            }
            if ( ( AV41Linha9 > Convert.ToDecimal( 0 )) )
            {
               AV32Linha10 = (decimal)(((AV41Linha9-AV46Total)/ (decimal)(AV41Linha9))*100);
            }
            HDN0( false, 232) ;
            getPrinter().GxDrawRect(975, Gx_line+0, 1083, Gx_line+17, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxDrawRect(0, Gx_line+0, 1083, Gx_line+228, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Valor Total", 908, Gx_line+2, 962, Gx_line+16, 1+256, 0, 0, 2) ;
            getPrinter().GxDrawText("Valor Total dos Compromissos Assumidos (vigentes) (A)", 667, Gx_line+33, 967, Gx_line+47, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("1/12 Avos (B)", 667, Gx_line+50, 967, Gx_line+64, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Valor Total do Ativo Circulante (C)", 667, Gx_line+67, 967, Gx_line+81, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Valor Total do Passivo Circulante (D)", 667, Gx_line+83, 967, Gx_line+97, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Valor Estimado da Contrata��o (E)", 667, Gx_line+100, 967, Gx_line+114, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Circulante L�quido (CCL) ou Capital de Giro (F)", 667, Gx_line+117, 967, Gx_line+131, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Item 10.6 al�nea 'c'", 367, Gx_line+117, 667, Gx_line+131, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Valor do Patrim�nio L�quido (G)", 667, Gx_line+133, 967, Gx_line+147, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Item 10.6 al�nea 'e'", 367, Gx_line+150, 667, Gx_line+164, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("1/12 Avos > Patrim�nio Liqu�do", 667, Gx_line+150, 967, Gx_line+164, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Item 10.6 al�nea 'e-2'", 367, Gx_line+183, 667, Gx_line+197, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("(H) > 10% ou < 10%", 667, Gx_line+183, 967, Gx_line+197, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Item 10.6 al�nea 'd'", 367, Gx_line+200, 667, Gx_line+214, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("(E) / (G)", 667, Gx_line+200, 967, Gx_line+214, 2, 0, 0, 1) ;
            getPrinter().GxDrawText("Receita Bruta (H )", 667, Gx_line+167, 967, Gx_line+181, 2, 0, 0, 1) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV46Total, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 983, Gx_line+1, 1080, Gx_line+16, 2, 0, 0, 2) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV38Linha6, "ZZ9.99 %")), 1000, Gx_line+117, 1081, Gx_line+132, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV32Linha10, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+183, 1081, Gx_line+198, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV41Linha9, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+167, 1081, Gx_line+182, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV39Linha7, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+133, 1081, Gx_line+148, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV37Linha5, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+100, 1081, Gx_line+115, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV36Linha4, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+83, 1081, Gx_line+98, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV35Linha3, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+67, 1081, Gx_line+82, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV34Linha2, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+50, 1081, Gx_line+65, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV33Linha11, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 1000, Gx_line+200, 1081, Gx_line+215, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV40Linha8, "")), 1033, Gx_line+150, 1081, Gx_line+165, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV46Total, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 984, Gx_line+33, 1081, Gx_line+48, 2, 0, 0, 2) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+232);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HDN0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'EXECUTADO' Routine */
         AV19Coluna7 = 0;
         AV12Coluna12 = 0;
         /* Using cursor P00DN13 */
         pr_default.execute(3, new Object[] {AV22Contratada_Codigo, AV47Vigencia, AV48Vigencia_To});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00DN13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00DN13_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = P00DN13_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00DN13_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00DN13_A471ContagemResultado_DataDmn[0];
            A512ContagemResultado_ValorPF = P00DN13_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00DN13_n512ContagemResultado_ValorPF[0];
            A456ContagemResultado_Codigo = P00DN13_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV19Coluna7 = (decimal)(AV19Coluna7+A574ContagemResultado_PFFinal);
            AV12Coluna12 = (decimal)(AV12Coluna12+((A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF)));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void HDN0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawRect(142, Gx_line+150, 1084, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(1008, Gx_line+150, 1083, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(933, Gx_line+150, 1008, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(858, Gx_line+150, 933, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(783, Gx_line+150, 858, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(708, Gx_line+150, 783, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(592, Gx_line+150, 667, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(517, Gx_line+150, 592, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(442, Gx_line+150, 517, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(367, Gx_line+150, 442, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(292, Gx_line+150, 367, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(217, Gx_line+150, 292, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(142, Gx_line+150, 217, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(667, Gx_line+150, 708, Gx_line+233, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(858, Gx_line+117, 858, Gx_line+150, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawRect(517, Gx_line+117, 1084, Gx_line+150, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV29HeaderPeriodo, "")), 280, Gx_line+46, 802, Gx_line+71, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rio de Compromissos Assumidos", 295, Gx_line+17, 787, Gx_line+45, 1, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV24Contratante_Logo, 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("�rg�o / Empresa", 25, Gx_line+184, 122, Gx_line+198, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Unidades", 658, Gx_line+126, 716, Gx_line+140, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Valor", 950, Gx_line+126, 983, Gx_line+140, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Previsto Vig�ncia", 1021, Gx_line+168, 1069, Gx_line+215, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("%", 683, Gx_line+184, 692, Gx_line+198, 1+256, 0, 0, 1) ;
               getPrinter().GxDrawText("Executado", 940, Gx_line+184, 1001, Gx_line+198, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Contratado", 868, Gx_line+184, 923, Gx_line+198, 1+256, 0, 0, 1) ;
               getPrinter().GxDrawText("Saldo dispon�vel por m�s at� final da vig�ncia", 787, Gx_line+150, 854, Gx_line+233, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Saldo dispon�vel", 715, Gx_line+168, 775, Gx_line+215, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Executado", 599, Gx_line+184, 659, Gx_line+198, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Contratado", 524, Gx_line+184, 585, Gx_line+198, 1, 0, 0, 1) ;
               getPrinter().GxDrawText("Valor Unit�rio Contratada", 446, Gx_line+166, 513, Gx_line+216, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Tempo Vig�ncia", 371, Gx_line+175, 438, Gx_line+208, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Data da Assinatura", 221, Gx_line+175, 288, Gx_line+208, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Data do Contrato", 146, Gx_line+175, 213, Gx_line+208, 1+16, 0, 0, 1) ;
               getPrinter().GxDrawText("Vig�ncia", 303, Gx_line+184, 355, Gx_line+198, 1, 0, 0, 1) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV50ContratanteInfo, "")), 228, Gx_line+83, 854, Gx_line+101, 1+256, 0, 0, 1) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+233);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV30Hoje = DateTime.MinValue;
         AV40Linha8 = "";
         scmdbuf = "";
         P00DN2_A29Contratante_Codigo = new int[1] ;
         P00DN2_n29Contratante_Codigo = new bool[] {false} ;
         P00DN2_A335Contratante_PessoaCod = new int[1] ;
         P00DN2_A5AreaTrabalho_Codigo = new int[1] ;
         P00DN2_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00DN2_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00DN2_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00DN2_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00DN2_A1805Contratante_AtivoCirculante = new decimal[1] ;
         P00DN2_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         P00DN2_A1806Contratante_PassivoCirculante = new decimal[1] ;
         P00DN2_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         P00DN2_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         P00DN2_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         P00DN2_A1808Contratante_ReceitaBruta = new decimal[1] ;
         P00DN2_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         P00DN2_A12Contratante_CNPJ = new String[] {""} ;
         P00DN2_n12Contratante_CNPJ = new bool[] {false} ;
         P00DN2_A10Contratante_NomeFantasia = new String[] {""} ;
         P00DN2_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00DN2_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1125Contratante_LogoNomeArq = "";
         A1124Contratante_LogoArquivo_Filename = "";
         A1126Contratante_LogoTipoArq = "";
         A1124Contratante_LogoArquivo_Filetype = "";
         A12Contratante_CNPJ = "";
         A10Contratante_NomeFantasia = "";
         A1124Contratante_LogoArquivo = "";
         AV24Contratante_Logo = "";
         A40000Contratante_Logo_GXI = "";
         AV50ContratanteInfo = "";
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         P00DN7_A39Contratada_Codigo = new int[1] ;
         P00DN7_A74Contrato_Codigo = new int[1] ;
         P00DN7_n74Contrato_Codigo = new bool[] {false} ;
         P00DN7_A43Contratada_Ativo = new bool[] {false} ;
         P00DN7_A92Contrato_Ativo = new bool[] {false} ;
         P00DN7_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00DN7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00DN7_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00DN7_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00DN7_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00DN7_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00DN7_n843Contrato_DataFimTA = new bool[] {false} ;
         P00DN7_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00DN7_n842Contrato_DataInicioTA = new bool[] {false} ;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A53Contratada_AreaTrabalhoDes = "";
         A842Contrato_DataInicioTA = DateTime.MinValue;
         AV47Vigencia = DateTime.MinValue;
         AV48Vigencia_To = DateTime.MinValue;
         AV29HeaderPeriodo = "";
         P00DN12_A74Contrato_Codigo = new int[1] ;
         P00DN12_n74Contrato_Codigo = new bool[] {false} ;
         P00DN12_A43Contratada_Ativo = new bool[] {false} ;
         P00DN12_A92Contrato_Ativo = new bool[] {false} ;
         P00DN12_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00DN12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00DN12_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00DN12_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00DN12_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00DN12_A81Contrato_Quantidade = new int[1] ;
         P00DN12_A39Contratada_Codigo = new int[1] ;
         P00DN12_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00DN12_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00DN12_A438Contratada_Sigla = new String[] {""} ;
         P00DN12_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         P00DN12_n843Contrato_DataFimTA = new bool[] {false} ;
         P00DN12_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         P00DN12_n842Contrato_DataInicioTA = new bool[] {false} ;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A438Contratada_Sigla = "";
         AV28Empresa = "";
         AV9Coluna1 = DateTime.MinValue;
         AV14Coluna2 = DateTime.MinValue;
         AV15Coluna3 = DateTime.MinValue;
         P00DN13_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00DN13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00DN13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00DN13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00DN13_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00DN13_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00DN13_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00DN13_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         AV24Contratante_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_compromissosassumidos__default(),
            new Object[][] {
                new Object[] {
               P00DN2_A29Contratante_Codigo, P00DN2_n29Contratante_Codigo, P00DN2_A335Contratante_PessoaCod, P00DN2_A5AreaTrabalho_Codigo, P00DN2_A1125Contratante_LogoNomeArq, P00DN2_n1125Contratante_LogoNomeArq, P00DN2_A1126Contratante_LogoTipoArq, P00DN2_n1126Contratante_LogoTipoArq, P00DN2_A1805Contratante_AtivoCirculante, P00DN2_n1805Contratante_AtivoCirculante,
               P00DN2_A1806Contratante_PassivoCirculante, P00DN2_n1806Contratante_PassivoCirculante, P00DN2_A1807Contratante_PatrimonioLiquido, P00DN2_n1807Contratante_PatrimonioLiquido, P00DN2_A1808Contratante_ReceitaBruta, P00DN2_n1808Contratante_ReceitaBruta, P00DN2_A12Contratante_CNPJ, P00DN2_n12Contratante_CNPJ, P00DN2_A10Contratante_NomeFantasia, P00DN2_A1124Contratante_LogoArquivo,
               P00DN2_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               P00DN7_A39Contratada_Codigo, P00DN7_A74Contrato_Codigo, P00DN7_A43Contratada_Ativo, P00DN7_A92Contrato_Ativo, P00DN7_A83Contrato_DataVigenciaTermino, P00DN7_A52Contratada_AreaTrabalhoCod, P00DN7_A82Contrato_DataVigenciaInicio, P00DN7_A53Contratada_AreaTrabalhoDes, P00DN7_n53Contratada_AreaTrabalhoDes, P00DN7_A843Contrato_DataFimTA,
               P00DN7_n843Contrato_DataFimTA, P00DN7_A842Contrato_DataInicioTA, P00DN7_n842Contrato_DataInicioTA
               }
               , new Object[] {
               P00DN12_A74Contrato_Codigo, P00DN12_A43Contratada_Ativo, P00DN12_A92Contrato_Ativo, P00DN12_A83Contrato_DataVigenciaTermino, P00DN12_A52Contratada_AreaTrabalhoCod, P00DN12_A82Contrato_DataVigenciaInicio, P00DN12_A85Contrato_DataAssinatura, P00DN12_A116Contrato_ValorUnidadeContratacao, P00DN12_A81Contrato_Quantidade, P00DN12_A39Contratada_Codigo,
               P00DN12_A53Contratada_AreaTrabalhoDes, P00DN12_n53Contratada_AreaTrabalhoDes, P00DN12_A438Contratada_Sigla, P00DN12_A843Contrato_DataFimTA, P00DN12_n843Contrato_DataFimTA, P00DN12_A842Contrato_DataInicioTA, P00DN12_n842Contrato_DataInicioTA
               }
               , new Object[] {
               P00DN13_A490ContagemResultado_ContratadaCod, P00DN13_n490ContagemResultado_ContratadaCod, P00DN13_A484ContagemResultado_StatusDmn, P00DN13_n484ContagemResultado_StatusDmn, P00DN13_A471ContagemResultado_DataDmn, P00DN13_A512ContagemResultado_ValorPF, P00DN13_n512ContagemResultado_ValorPF, P00DN13_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private int AV8AreaTrabalho_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A81Contrato_Quantidade ;
      private int AV22Contratada_Codigo ;
      private int Gx_OldLine ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV49Contratante_CNPJ ;
      private decimal AV37Linha5 ;
      private decimal A1805Contratante_AtivoCirculante ;
      private decimal A1806Contratante_PassivoCirculante ;
      private decimal A1807Contratante_PatrimonioLiquido ;
      private decimal A1808Contratante_ReceitaBruta ;
      private decimal AV35Linha3 ;
      private decimal AV36Linha4 ;
      private decimal AV39Linha7 ;
      private decimal AV41Linha9 ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV16Coluna4 ;
      private decimal AV17Coluna5 ;
      private decimal AV18Coluna6 ;
      private decimal AV11Coluna11 ;
      private decimal AV20Coluna8 ;
      private decimal AV19Coluna7 ;
      private decimal AV21Coluna9 ;
      private decimal AV44Meses ;
      private decimal AV10Coluna10 ;
      private decimal AV13Coluna13 ;
      private decimal AV12Coluna12 ;
      private decimal AV46Total ;
      private decimal AV34Linha2 ;
      private decimal AV38Linha6 ;
      private decimal AV33Linha11 ;
      private decimal AV32Linha10 ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV40Linha8 ;
      private String scmdbuf ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1124Contratante_LogoArquivo_Filename ;
      private String A1126Contratante_LogoTipoArq ;
      private String A1124Contratante_LogoArquivo_Filetype ;
      private String A10Contratante_NomeFantasia ;
      private String AV50ContratanteInfo ;
      private String AV29HeaderPeriodo ;
      private String A438Contratada_Sigla ;
      private String AV28Empresa ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime AV30Hoje ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime AV47Vigencia ;
      private DateTime AV48Vigencia_To ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime AV9Coluna1 ;
      private DateTime AV14Coluna2 ;
      private DateTime AV15Coluna3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool AV51TodasAsAreas ;
      private bool n29Contratante_Codigo ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n1805Contratante_AtivoCirculante ;
      private bool n1806Contratante_PassivoCirculante ;
      private bool n1807Contratante_PatrimonioLiquido ;
      private bool n1808Contratante_ReceitaBruta ;
      private bool n12Contratante_CNPJ ;
      private bool n1124Contratante_LogoArquivo ;
      private bool A92Contrato_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool n74Contrato_Codigo ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n843Contrato_DataFimTA ;
      private bool n842Contrato_DataInicioTA ;
      private bool returnInSub ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n512ContagemResultado_ValorPF ;
      private String A12Contratante_CNPJ ;
      private String A40000Contratante_Logo_GXI ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String AV24Contratante_Logo ;
      private String Contratante_logo ;
      private String A1124Contratante_LogoArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00DN2_A29Contratante_Codigo ;
      private bool[] P00DN2_n29Contratante_Codigo ;
      private int[] P00DN2_A335Contratante_PessoaCod ;
      private int[] P00DN2_A5AreaTrabalho_Codigo ;
      private String[] P00DN2_A1125Contratante_LogoNomeArq ;
      private bool[] P00DN2_n1125Contratante_LogoNomeArq ;
      private String[] P00DN2_A1126Contratante_LogoTipoArq ;
      private bool[] P00DN2_n1126Contratante_LogoTipoArq ;
      private decimal[] P00DN2_A1805Contratante_AtivoCirculante ;
      private bool[] P00DN2_n1805Contratante_AtivoCirculante ;
      private decimal[] P00DN2_A1806Contratante_PassivoCirculante ;
      private bool[] P00DN2_n1806Contratante_PassivoCirculante ;
      private decimal[] P00DN2_A1807Contratante_PatrimonioLiquido ;
      private bool[] P00DN2_n1807Contratante_PatrimonioLiquido ;
      private decimal[] P00DN2_A1808Contratante_ReceitaBruta ;
      private bool[] P00DN2_n1808Contratante_ReceitaBruta ;
      private String[] P00DN2_A12Contratante_CNPJ ;
      private bool[] P00DN2_n12Contratante_CNPJ ;
      private String[] P00DN2_A10Contratante_NomeFantasia ;
      private String[] P00DN2_A1124Contratante_LogoArquivo ;
      private bool[] P00DN2_n1124Contratante_LogoArquivo ;
      private int[] P00DN7_A39Contratada_Codigo ;
      private int[] P00DN7_A74Contrato_Codigo ;
      private bool[] P00DN7_n74Contrato_Codigo ;
      private bool[] P00DN7_A43Contratada_Ativo ;
      private bool[] P00DN7_A92Contrato_Ativo ;
      private DateTime[] P00DN7_A83Contrato_DataVigenciaTermino ;
      private int[] P00DN7_A52Contratada_AreaTrabalhoCod ;
      private DateTime[] P00DN7_A82Contrato_DataVigenciaInicio ;
      private String[] P00DN7_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00DN7_n53Contratada_AreaTrabalhoDes ;
      private DateTime[] P00DN7_A843Contrato_DataFimTA ;
      private bool[] P00DN7_n843Contrato_DataFimTA ;
      private DateTime[] P00DN7_A842Contrato_DataInicioTA ;
      private bool[] P00DN7_n842Contrato_DataInicioTA ;
      private int[] P00DN12_A74Contrato_Codigo ;
      private bool[] P00DN12_n74Contrato_Codigo ;
      private bool[] P00DN12_A43Contratada_Ativo ;
      private bool[] P00DN12_A92Contrato_Ativo ;
      private DateTime[] P00DN12_A83Contrato_DataVigenciaTermino ;
      private int[] P00DN12_A52Contratada_AreaTrabalhoCod ;
      private DateTime[] P00DN12_A82Contrato_DataVigenciaInicio ;
      private DateTime[] P00DN12_A85Contrato_DataAssinatura ;
      private decimal[] P00DN12_A116Contrato_ValorUnidadeContratacao ;
      private int[] P00DN12_A81Contrato_Quantidade ;
      private int[] P00DN12_A39Contratada_Codigo ;
      private String[] P00DN12_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00DN12_n53Contratada_AreaTrabalhoDes ;
      private String[] P00DN12_A438Contratada_Sigla ;
      private DateTime[] P00DN12_A843Contrato_DataFimTA ;
      private bool[] P00DN12_n843Contrato_DataFimTA ;
      private DateTime[] P00DN12_A842Contrato_DataInicioTA ;
      private bool[] P00DN12_n842Contrato_DataInicioTA ;
      private int[] P00DN13_A490ContagemResultado_ContratadaCod ;
      private bool[] P00DN13_n490ContagemResultado_ContratadaCod ;
      private String[] P00DN13_A484ContagemResultado_StatusDmn ;
      private bool[] P00DN13_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00DN13_A471ContagemResultado_DataDmn ;
      private decimal[] P00DN13_A512ContagemResultado_ValorPF ;
      private bool[] P00DN13_n512ContagemResultado_ValorPF ;
      private int[] P00DN13_A456ContagemResultado_Codigo ;
   }

   public class arel_compromissosassumidos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00DN7( IGxContext context ,
                                             bool AV51TodasAsAreas ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV8AreaTrabalho_Codigo ,
                                             DateTime A83Contrato_DataVigenciaTermino ,
                                             DateTime A843Contrato_DataFimTA ,
                                             DateTime AV30Hoje ,
                                             bool A92Contrato_Ativo ,
                                             bool A43Contratada_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [3] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_Codigo], T1.[Contrato_Codigo], T2.[Contratada_Ativo], T1.[Contrato_Ativo], T1.[Contrato_DataVigenciaTermino], T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contrato_DataVigenciaInicio], T3.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, COALESCE( T5.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (((([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T4 ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataInicio], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T5 ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE (( T1.[Contrato_DataVigenciaTermino] > COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) and T1.[Contrato_DataVigenciaTermino] >= @AV30Hoje) or ( COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) > T1.[Contrato_DataVigenciaTermino] and COALESCE( T4.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV30Hoje))";
         scmdbuf = scmdbuf + " and (T1.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T2.[Contratada_Ativo] = 1)";
         if ( ! AV51TodasAsAreas )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV51TodasAsAreas )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[AreaTrabalho_Descricao]";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00DN12( IGxContext context ,
                                              bool AV51TodasAsAreas ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV8AreaTrabalho_Codigo ,
                                              DateTime A83Contrato_DataVigenciaTermino ,
                                              DateTime A843Contrato_DataFimTA ,
                                              DateTime AV30Hoje ,
                                              bool A92Contrato_Ativo ,
                                              bool A43Contratada_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T4.[Contratada_Ativo], T1.[Contrato_Ativo], T1.[Contrato_DataVigenciaTermino], T4.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contrato_DataVigenciaInicio], T1.[Contrato_DataAssinatura], T1.[Contrato_ValorUnidadeContratacao], T1.[Contrato_Quantidade], T1.[Contratada_Codigo], T5.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T4.[Contratada_Sigla], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, COALESCE( T3.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataInicio], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T4.[Contratada_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (( T1.[Contrato_DataVigenciaTermino] > COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) and T1.[Contrato_DataVigenciaTermino] >= @AV30Hoje) or ( COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) > T1.[Contrato_DataVigenciaTermino] and COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV30Hoje))";
         scmdbuf = scmdbuf + " and (T1.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T4.[Contratada_Ativo] = 1)";
         if ( ! AV51TodasAsAreas )
         {
            sWhereString = sWhereString + " and (T4.[Contratada_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV51TodasAsAreas )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[AreaTrabalho_Descricao]";
         }
         else if ( AV51TodasAsAreas )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[Contratada_Sigla], T5.[AreaTrabalho_Descricao]";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00DN7(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (bool)dynConstraints[6] , (bool)dynConstraints[7] );
               case 2 :
                     return conditional_P00DN12(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (bool)dynConstraints[6] , (bool)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DN2 ;
          prmP00DN2 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DN13 ;
          prmP00DN13 = new Object[] {
          new Object[] {"@AV22Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47Vigencia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48Vigencia_To",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00DN7 ;
          prmP00DN7 = new Object[] {
          new Object[] {"@AV30Hoje",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30Hoje",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DN12 ;
          prmP00DN12 = new Object[] {
          new Object[] {"@AV30Hoje",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30Hoje",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DN2", "SELECT TOP 1 T1.[Contratante_Codigo], T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T2.[Contratante_LogoNomeArq], T2.[Contratante_LogoTipoArq], T2.[Contratante_AtivoCirculante], T2.[Contratante_PassivoCirculante], T2.[Contratante_PatrimonioLiquido], T2.[Contratante_ReceitaBruta], T3.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T2.[Contratante_LogoArquivo] FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[AreaTrabalho_Codigo] = @AV8AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DN2,1,0,false,true )
             ,new CursorDef("P00DN7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DN7,100,0,false,false )
             ,new CursorDef("P00DN12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DN12,100,0,true,false )
             ,new CursorDef("P00DN13", "SELECT [ContagemResultado_ContratadaCod], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_ContratadaCod] = @AV22Contratada_Codigo and [ContagemResultado_DataDmn] >= @AV47Vigencia) AND (Not [ContagemResultado_StatusDmn] = 'X') AND ([ContagemResultado_DataDmn] <= @AV48Vigencia_To) ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_DataDmn] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DN13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 100) ;
                ((String[]) buf[19])[0] = rslt.getBLOBFile(12, rslt.getString(5, 10), rslt.getString(4, 50)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(11);
                ((String[]) buf[12])[0] = rslt.getString(12, 15) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                return;
       }
    }

 }

}
