/*
               File: GetContratoContratoNotaEmpenhoWCFilterData
        Description: Get Contrato Contrato Nota Empenho WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:33.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratonotaempenhowcfilterdata : GXProcedure
   {
      public getcontratocontratonotaempenhowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratonotaempenhowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
         return AV40OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratonotaempenhowcfilterdata objgetcontratocontratonotaempenhowcfilterdata;
         objgetcontratocontratonotaempenhowcfilterdata = new getcontratocontratonotaempenhowcfilterdata();
         objgetcontratocontratonotaempenhowcfilterdata.AV31DDOName = aP0_DDOName;
         objgetcontratocontratonotaempenhowcfilterdata.AV29SearchTxt = aP1_SearchTxt;
         objgetcontratocontratonotaempenhowcfilterdata.AV30SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratonotaempenhowcfilterdata.AV35OptionsJson = "" ;
         objgetcontratocontratonotaempenhowcfilterdata.AV38OptionsDescJson = "" ;
         objgetcontratocontratonotaempenhowcfilterdata.AV40OptionIndexesJson = "" ;
         objgetcontratocontratonotaempenhowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratonotaempenhowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratonotaempenhowcfilterdata);
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratonotaempenhowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV34Options = (IGxCollection)(new GxSimpleCollection());
         AV37OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV39OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_SALDOCONTRATO_UNIDADEMEDICAO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSALDOCONTRATO_UNIDADEMEDICAO_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_NOTAEMPENHO_ITENTIFICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV35OptionsJson = AV34Options.ToJSonString(false);
         AV38OptionsDescJson = AV37OptionsDesc.ToJSonString(false);
         AV40OptionIndexesJson = AV39OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV42Session.Get("ContratoContratoNotaEmpenhoWCGridState"), "") == 0 )
         {
            AV44GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoNotaEmpenhoWCGridState"), "");
         }
         else
         {
            AV44GridState.FromXml(AV42Session.Get("ContratoContratoNotaEmpenhoWCGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV44GridState.gxTpr_Filtervalues.Count )
         {
            AV45GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV44GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_CODIGO") == 0 )
            {
               AV10TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
               AV11TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CODIGO") == 0 )
            {
               AV12TFSaldoContrato_Codigo = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
               AV13TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA") == 0 )
            {
               AV14TFSaldoContrato_UnidadeMedicao_Sigla = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_SIGLA_SEL") == 0 )
            {
               AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR") == 0 )
            {
               AV16TFNotaEmpenho_Itentificador = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR_SEL") == 0 )
            {
               AV17TFNotaEmpenho_Itentificador_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_DEMISSAO") == 0 )
            {
               AV18TFNotaEmpenho_DEmissao = context.localUtil.CToT( AV45GridStateFilterValue.gxTpr_Value, 2);
               AV19TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( AV45GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_QTD") == 0 )
            {
               AV20TFNotaEmpenho_Qtd = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, ".");
               AV21TFNotaEmpenho_Qtd_To = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_VALOR") == 0 )
            {
               AV22TFNotaEmpenho_Valor = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, ".");
               AV23TFNotaEmpenho_Valor_To = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOANT") == 0 )
            {
               AV24TFNotaEmpenho_SaldoAnt = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, ".");
               AV25TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOPOS") == 0 )
            {
               AV26TFNotaEmpenho_SaldoPos = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, ".");
               AV27TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ATIVO_SEL") == 0 )
            {
               AV28TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV47Contrato_Codigo = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSALDOCONTRATO_UNIDADEMEDICAO_SIGLAOPTIONS' Routine */
         AV14TFSaldoContrato_UnidadeMedicao_Sigla = AV29SearchTxt;
         AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFNotaEmpenho_Codigo ,
                                              AV11TFNotaEmpenho_Codigo_To ,
                                              AV12TFSaldoContrato_Codigo ,
                                              AV13TFSaldoContrato_Codigo_To ,
                                              AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                              AV14TFSaldoContrato_UnidadeMedicao_Sigla ,
                                              AV17TFNotaEmpenho_Itentificador_Sel ,
                                              AV16TFNotaEmpenho_Itentificador ,
                                              AV18TFNotaEmpenho_DEmissao ,
                                              AV19TFNotaEmpenho_DEmissao_To ,
                                              AV20TFNotaEmpenho_Qtd ,
                                              AV21TFNotaEmpenho_Qtd_To ,
                                              AV22TFNotaEmpenho_Valor ,
                                              AV23TFNotaEmpenho_Valor_To ,
                                              AV24TFNotaEmpenho_SaldoAnt ,
                                              AV25TFNotaEmpenho_SaldoAnt_To ,
                                              AV26TFNotaEmpenho_SaldoPos ,
                                              AV27TFNotaEmpenho_SaldoPos_To ,
                                              AV28TFNotaEmpenho_Ativo_Sel ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1561SaldoContrato_Codigo ,
                                              A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1566NotaEmpenho_Valor ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo ,
                                              AV47Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV14TFSaldoContrato_UnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Sigla), 15, "%");
         lV16TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV16TFNotaEmpenho_Itentificador), 15, "%");
         /* Using cursor P00IY2 */
         pr_default.execute(0, new Object[] {AV47Contrato_Codigo, AV10TFNotaEmpenho_Codigo, AV11TFNotaEmpenho_Codigo_To, AV12TFSaldoContrato_Codigo, AV13TFSaldoContrato_Codigo_To, lV14TFSaldoContrato_UnidadeMedicao_Sigla, AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel, lV16TFNotaEmpenho_Itentificador, AV17TFNotaEmpenho_Itentificador_Sel, AV18TFNotaEmpenho_DEmissao, AV19TFNotaEmpenho_DEmissao_To, AV20TFNotaEmpenho_Qtd, AV21TFNotaEmpenho_Qtd_To, AV22TFNotaEmpenho_Valor, AV23TFNotaEmpenho_Valor_To, AV24TFNotaEmpenho_SaldoAnt, AV25TFNotaEmpenho_SaldoAnt_To, AV26TFNotaEmpenho_SaldoPos, AV27TFNotaEmpenho_SaldoPos_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKIY2 = false;
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00IY2_A74Contrato_Codigo[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = P00IY2_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            A1570NotaEmpenho_Ativo = P00IY2_A1570NotaEmpenho_Ativo[0];
            n1570NotaEmpenho_Ativo = P00IY2_n1570NotaEmpenho_Ativo[0];
            A1569NotaEmpenho_SaldoPos = P00IY2_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = P00IY2_n1569NotaEmpenho_SaldoPos[0];
            A1568NotaEmpenho_SaldoAnt = P00IY2_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = P00IY2_n1568NotaEmpenho_SaldoAnt[0];
            A1566NotaEmpenho_Valor = P00IY2_A1566NotaEmpenho_Valor[0];
            n1566NotaEmpenho_Valor = P00IY2_n1566NotaEmpenho_Valor[0];
            A1567NotaEmpenho_Qtd = P00IY2_A1567NotaEmpenho_Qtd[0];
            n1567NotaEmpenho_Qtd = P00IY2_n1567NotaEmpenho_Qtd[0];
            A1565NotaEmpenho_DEmissao = P00IY2_A1565NotaEmpenho_DEmissao[0];
            n1565NotaEmpenho_DEmissao = P00IY2_n1565NotaEmpenho_DEmissao[0];
            A1564NotaEmpenho_Itentificador = P00IY2_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = P00IY2_n1564NotaEmpenho_Itentificador[0];
            A1561SaldoContrato_Codigo = P00IY2_A1561SaldoContrato_Codigo[0];
            A1560NotaEmpenho_Codigo = P00IY2_A1560NotaEmpenho_Codigo[0];
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00IY2_A74Contrato_Codigo[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = P00IY2_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            AV41count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00IY2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla[0], A1785SaldoContrato_UnidadeMedicao_Sigla) == 0 ) )
            {
               BRKIY2 = false;
               A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
               A1561SaldoContrato_Codigo = P00IY2_A1561SaldoContrato_Codigo[0];
               A1560NotaEmpenho_Codigo = P00IY2_A1560NotaEmpenho_Codigo[0];
               A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
               AV41count = (long)(AV41count+1);
               BRKIY2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1785SaldoContrato_UnidadeMedicao_Sigla)) )
            {
               AV33Option = A1785SaldoContrato_UnidadeMedicao_Sigla;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            A74Contrato_Codigo = W74Contrato_Codigo;
            if ( ! BRKIY2 )
            {
               BRKIY2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' Routine */
         AV16TFNotaEmpenho_Itentificador = AV29SearchTxt;
         AV17TFNotaEmpenho_Itentificador_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV10TFNotaEmpenho_Codigo ,
                                              AV11TFNotaEmpenho_Codigo_To ,
                                              AV12TFSaldoContrato_Codigo ,
                                              AV13TFSaldoContrato_Codigo_To ,
                                              AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                              AV14TFSaldoContrato_UnidadeMedicao_Sigla ,
                                              AV17TFNotaEmpenho_Itentificador_Sel ,
                                              AV16TFNotaEmpenho_Itentificador ,
                                              AV18TFNotaEmpenho_DEmissao ,
                                              AV19TFNotaEmpenho_DEmissao_To ,
                                              AV20TFNotaEmpenho_Qtd ,
                                              AV21TFNotaEmpenho_Qtd_To ,
                                              AV22TFNotaEmpenho_Valor ,
                                              AV23TFNotaEmpenho_Valor_To ,
                                              AV24TFNotaEmpenho_SaldoAnt ,
                                              AV25TFNotaEmpenho_SaldoAnt_To ,
                                              AV26TFNotaEmpenho_SaldoPos ,
                                              AV27TFNotaEmpenho_SaldoPos_To ,
                                              AV28TFNotaEmpenho_Ativo_Sel ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1561SaldoContrato_Codigo ,
                                              A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1566NotaEmpenho_Valor ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo ,
                                              AV47Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV14TFSaldoContrato_UnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Sigla), 15, "%");
         lV16TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV16TFNotaEmpenho_Itentificador), 15, "%");
         /* Using cursor P00IY3 */
         pr_default.execute(1, new Object[] {AV47Contrato_Codigo, AV10TFNotaEmpenho_Codigo, AV11TFNotaEmpenho_Codigo_To, AV12TFSaldoContrato_Codigo, AV13TFSaldoContrato_Codigo_To, lV14TFSaldoContrato_UnidadeMedicao_Sigla, AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel, lV16TFNotaEmpenho_Itentificador, AV17TFNotaEmpenho_Itentificador_Sel, AV18TFNotaEmpenho_DEmissao, AV19TFNotaEmpenho_DEmissao_To, AV20TFNotaEmpenho_Qtd, AV21TFNotaEmpenho_Qtd_To, AV22TFNotaEmpenho_Valor, AV23TFNotaEmpenho_Valor_To, AV24TFNotaEmpenho_SaldoAnt, AV25TFNotaEmpenho_SaldoAnt_To, AV26TFNotaEmpenho_SaldoPos, AV27TFNotaEmpenho_SaldoPos_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKIY4 = false;
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00IY3_A74Contrato_Codigo[0];
            A1564NotaEmpenho_Itentificador = P00IY3_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = P00IY3_n1564NotaEmpenho_Itentificador[0];
            A1570NotaEmpenho_Ativo = P00IY3_A1570NotaEmpenho_Ativo[0];
            n1570NotaEmpenho_Ativo = P00IY3_n1570NotaEmpenho_Ativo[0];
            A1569NotaEmpenho_SaldoPos = P00IY3_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = P00IY3_n1569NotaEmpenho_SaldoPos[0];
            A1568NotaEmpenho_SaldoAnt = P00IY3_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = P00IY3_n1568NotaEmpenho_SaldoAnt[0];
            A1566NotaEmpenho_Valor = P00IY3_A1566NotaEmpenho_Valor[0];
            n1566NotaEmpenho_Valor = P00IY3_n1566NotaEmpenho_Valor[0];
            A1567NotaEmpenho_Qtd = P00IY3_A1567NotaEmpenho_Qtd[0];
            n1567NotaEmpenho_Qtd = P00IY3_n1567NotaEmpenho_Qtd[0];
            A1565NotaEmpenho_DEmissao = P00IY3_A1565NotaEmpenho_DEmissao[0];
            n1565NotaEmpenho_DEmissao = P00IY3_n1565NotaEmpenho_DEmissao[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = P00IY3_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = P00IY3_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            A1561SaldoContrato_Codigo = P00IY3_A1561SaldoContrato_Codigo[0];
            A1560NotaEmpenho_Codigo = P00IY3_A1560NotaEmpenho_Codigo[0];
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00IY3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00IY3_A74Contrato_Codigo[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = P00IY3_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = P00IY3_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            AV41count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00IY3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00IY3_A1564NotaEmpenho_Itentificador[0], A1564NotaEmpenho_Itentificador) == 0 ) )
            {
               BRKIY4 = false;
               A1561SaldoContrato_Codigo = P00IY3_A1561SaldoContrato_Codigo[0];
               A1560NotaEmpenho_Codigo = P00IY3_A1560NotaEmpenho_Codigo[0];
               AV41count = (long)(AV41count+1);
               BRKIY4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) )
            {
               AV33Option = A1564NotaEmpenho_Itentificador;
               AV34Options.Add(AV33Option, 0);
               AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV34Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            A74Contrato_Codigo = W74Contrato_Codigo;
            if ( ! BRKIY4 )
            {
               BRKIY4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV34Options = new GxSimpleCollection();
         AV37OptionsDesc = new GxSimpleCollection();
         AV39OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV42Session = context.GetSession();
         AV44GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV45GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFSaldoContrato_UnidadeMedicao_Sigla = "";
         AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel = "";
         AV16TFNotaEmpenho_Itentificador = "";
         AV17TFNotaEmpenho_Itentificador_Sel = "";
         AV18TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV19TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV14TFSaldoContrato_UnidadeMedicao_Sigla = "";
         lV16TFNotaEmpenho_Itentificador = "";
         A1785SaldoContrato_UnidadeMedicao_Sigla = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00IY2_A74Contrato_Codigo = new int[1] ;
         P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         P00IY2_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         P00IY2_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00IY2_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00IY2_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         P00IY2_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         P00IY2_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         P00IY2_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         P00IY2_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00IY2_n1566NotaEmpenho_Valor = new bool[] {false} ;
         P00IY2_A1567NotaEmpenho_Qtd = new decimal[1] ;
         P00IY2_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         P00IY2_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         P00IY2_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         P00IY2_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         P00IY2_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         P00IY2_A1561SaldoContrato_Codigo = new int[1] ;
         P00IY2_A1560NotaEmpenho_Codigo = new int[1] ;
         AV33Option = "";
         P00IY3_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00IY3_A74Contrato_Codigo = new int[1] ;
         P00IY3_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         P00IY3_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         P00IY3_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00IY3_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00IY3_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         P00IY3_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         P00IY3_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         P00IY3_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         P00IY3_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00IY3_n1566NotaEmpenho_Valor = new bool[] {false} ;
         P00IY3_A1567NotaEmpenho_Qtd = new decimal[1] ;
         P00IY3_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         P00IY3_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         P00IY3_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         P00IY3_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         P00IY3_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         P00IY3_A1561SaldoContrato_Codigo = new int[1] ;
         P00IY3_A1560NotaEmpenho_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratonotaempenhowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo, P00IY2_A74Contrato_Codigo, P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla, P00IY2_n1785SaldoContrato_UnidadeMedicao_Sigla, P00IY2_A1570NotaEmpenho_Ativo, P00IY2_n1570NotaEmpenho_Ativo, P00IY2_A1569NotaEmpenho_SaldoPos, P00IY2_n1569NotaEmpenho_SaldoPos, P00IY2_A1568NotaEmpenho_SaldoAnt, P00IY2_n1568NotaEmpenho_SaldoAnt,
               P00IY2_A1566NotaEmpenho_Valor, P00IY2_n1566NotaEmpenho_Valor, P00IY2_A1567NotaEmpenho_Qtd, P00IY2_n1567NotaEmpenho_Qtd, P00IY2_A1565NotaEmpenho_DEmissao, P00IY2_n1565NotaEmpenho_DEmissao, P00IY2_A1564NotaEmpenho_Itentificador, P00IY2_n1564NotaEmpenho_Itentificador, P00IY2_A1561SaldoContrato_Codigo, P00IY2_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               P00IY3_A1783SaldoContrato_UnidadeMedicao_Codigo, P00IY3_A74Contrato_Codigo, P00IY3_A1564NotaEmpenho_Itentificador, P00IY3_n1564NotaEmpenho_Itentificador, P00IY3_A1570NotaEmpenho_Ativo, P00IY3_n1570NotaEmpenho_Ativo, P00IY3_A1569NotaEmpenho_SaldoPos, P00IY3_n1569NotaEmpenho_SaldoPos, P00IY3_A1568NotaEmpenho_SaldoAnt, P00IY3_n1568NotaEmpenho_SaldoAnt,
               P00IY3_A1566NotaEmpenho_Valor, P00IY3_n1566NotaEmpenho_Valor, P00IY3_A1567NotaEmpenho_Qtd, P00IY3_n1567NotaEmpenho_Qtd, P00IY3_A1565NotaEmpenho_DEmissao, P00IY3_n1565NotaEmpenho_DEmissao, P00IY3_A1785SaldoContrato_UnidadeMedicao_Sigla, P00IY3_n1785SaldoContrato_UnidadeMedicao_Sigla, P00IY3_A1561SaldoContrato_Codigo, P00IY3_A1560NotaEmpenho_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV28TFNotaEmpenho_Ativo_Sel ;
      private int AV50GXV1 ;
      private int AV10TFNotaEmpenho_Codigo ;
      private int AV11TFNotaEmpenho_Codigo_To ;
      private int AV12TFSaldoContrato_Codigo ;
      private int AV13TFSaldoContrato_Codigo_To ;
      private int AV47Contrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int W74Contrato_Codigo ;
      private long AV41count ;
      private decimal AV20TFNotaEmpenho_Qtd ;
      private decimal AV21TFNotaEmpenho_Qtd_To ;
      private decimal AV22TFNotaEmpenho_Valor ;
      private decimal AV23TFNotaEmpenho_Valor_To ;
      private decimal AV24TFNotaEmpenho_SaldoAnt ;
      private decimal AV25TFNotaEmpenho_SaldoAnt_To ;
      private decimal AV26TFNotaEmpenho_SaldoPos ;
      private decimal AV27TFNotaEmpenho_SaldoPos_To ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String AV14TFSaldoContrato_UnidadeMedicao_Sigla ;
      private String AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel ;
      private String AV16TFNotaEmpenho_Itentificador ;
      private String AV17TFNotaEmpenho_Itentificador_Sel ;
      private String scmdbuf ;
      private String lV14TFSaldoContrato_UnidadeMedicao_Sigla ;
      private String lV16TFNotaEmpenho_Itentificador ;
      private String A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private String A1564NotaEmpenho_Itentificador ;
      private DateTime AV18TFNotaEmpenho_DEmissao ;
      private DateTime AV19TFNotaEmpenho_DEmissao_To ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private bool returnInSub ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool BRKIY2 ;
      private bool n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool BRKIY4 ;
      private String AV40OptionIndexesJson ;
      private String AV35OptionsJson ;
      private String AV38OptionsDescJson ;
      private String AV31DDOName ;
      private String AV29SearchTxt ;
      private String AV30SearchTxtTo ;
      private String AV33Option ;
      private IGxSession AV42Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00IY2_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P00IY2_A74Contrato_Codigo ;
      private String[] P00IY2_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] P00IY2_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] P00IY2_A1570NotaEmpenho_Ativo ;
      private bool[] P00IY2_n1570NotaEmpenho_Ativo ;
      private decimal[] P00IY2_A1569NotaEmpenho_SaldoPos ;
      private bool[] P00IY2_n1569NotaEmpenho_SaldoPos ;
      private decimal[] P00IY2_A1568NotaEmpenho_SaldoAnt ;
      private bool[] P00IY2_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] P00IY2_A1566NotaEmpenho_Valor ;
      private bool[] P00IY2_n1566NotaEmpenho_Valor ;
      private decimal[] P00IY2_A1567NotaEmpenho_Qtd ;
      private bool[] P00IY2_n1567NotaEmpenho_Qtd ;
      private DateTime[] P00IY2_A1565NotaEmpenho_DEmissao ;
      private bool[] P00IY2_n1565NotaEmpenho_DEmissao ;
      private String[] P00IY2_A1564NotaEmpenho_Itentificador ;
      private bool[] P00IY2_n1564NotaEmpenho_Itentificador ;
      private int[] P00IY2_A1561SaldoContrato_Codigo ;
      private int[] P00IY2_A1560NotaEmpenho_Codigo ;
      private int[] P00IY3_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P00IY3_A74Contrato_Codigo ;
      private String[] P00IY3_A1564NotaEmpenho_Itentificador ;
      private bool[] P00IY3_n1564NotaEmpenho_Itentificador ;
      private bool[] P00IY3_A1570NotaEmpenho_Ativo ;
      private bool[] P00IY3_n1570NotaEmpenho_Ativo ;
      private decimal[] P00IY3_A1569NotaEmpenho_SaldoPos ;
      private bool[] P00IY3_n1569NotaEmpenho_SaldoPos ;
      private decimal[] P00IY3_A1568NotaEmpenho_SaldoAnt ;
      private bool[] P00IY3_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] P00IY3_A1566NotaEmpenho_Valor ;
      private bool[] P00IY3_n1566NotaEmpenho_Valor ;
      private decimal[] P00IY3_A1567NotaEmpenho_Qtd ;
      private bool[] P00IY3_n1567NotaEmpenho_Qtd ;
      private DateTime[] P00IY3_A1565NotaEmpenho_DEmissao ;
      private bool[] P00IY3_n1565NotaEmpenho_DEmissao ;
      private String[] P00IY3_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] P00IY3_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] P00IY3_A1561SaldoContrato_Codigo ;
      private int[] P00IY3_A1560NotaEmpenho_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV44GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV45GridStateFilterValue ;
   }

   public class getcontratocontratonotaempenhowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00IY2( IGxContext context ,
                                             int AV10TFNotaEmpenho_Codigo ,
                                             int AV11TFNotaEmpenho_Codigo_To ,
                                             int AV12TFSaldoContrato_Codigo ,
                                             int AV13TFSaldoContrato_Codigo_To ,
                                             String AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                             String AV14TFSaldoContrato_UnidadeMedicao_Sigla ,
                                             String AV17TFNotaEmpenho_Itentificador_Sel ,
                                             String AV16TFNotaEmpenho_Itentificador ,
                                             DateTime AV18TFNotaEmpenho_DEmissao ,
                                             DateTime AV19TFNotaEmpenho_DEmissao_To ,
                                             decimal AV20TFNotaEmpenho_Qtd ,
                                             decimal AV21TFNotaEmpenho_Qtd_To ,
                                             decimal AV22TFNotaEmpenho_Valor ,
                                             decimal AV23TFNotaEmpenho_Valor_To ,
                                             decimal AV24TFNotaEmpenho_SaldoAnt ,
                                             decimal AV25TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV26TFNotaEmpenho_SaldoPos ,
                                             decimal AV27TFNotaEmpenho_SaldoPos_To ,
                                             short AV28TFNotaEmpenho_Ativo_Sel ,
                                             int A1560NotaEmpenho_Codigo ,
                                             int A1561SaldoContrato_Codigo ,
                                             String A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             int AV47Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [19] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T2.[Contrato_Codigo], T3.[UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla, T1.[NotaEmpenho_Ativo], T1.[NotaEmpenho_SaldoPos], T1.[NotaEmpenho_SaldoAnt], T1.[NotaEmpenho_Valor], T1.[NotaEmpenho_Qtd], T1.[NotaEmpenho_DEmissao], T1.[NotaEmpenho_Itentificador], T1.[SaldoContrato_Codigo], T1.[NotaEmpenho_Codigo] FROM (([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[SaldoContrato_UnidadeMedicao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contrato_Codigo] = @AV47Contrato_Codigo)";
         if ( ! (0==AV10TFNotaEmpenho_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] >= @AV10TFNotaEmpenho_Codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV11TFNotaEmpenho_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] <= @AV11TFNotaEmpenho_Codigo_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV12TFSaldoContrato_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV12TFSaldoContrato_Codigo)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV13TFSaldoContrato_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV13TFSaldoContrato_Codigo_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] like @lV14TFSaldoContrato_UnidadeMedicao_Sigla)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] = @AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFNotaEmpenho_Itentificador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] like @lV16TFNotaEmpenho_Itentificador)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFNotaEmpenho_Itentificador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] = @AV17TFNotaEmpenho_Itentificador_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFNotaEmpenho_DEmissao) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] >= @AV18TFNotaEmpenho_DEmissao)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFNotaEmpenho_DEmissao_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] <= @AV19TFNotaEmpenho_DEmissao_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFNotaEmpenho_Qtd) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] >= @AV20TFNotaEmpenho_Qtd)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFNotaEmpenho_Qtd_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] <= @AV21TFNotaEmpenho_Qtd_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFNotaEmpenho_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] >= @AV22TFNotaEmpenho_Valor)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFNotaEmpenho_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] <= @AV23TFNotaEmpenho_Valor_To)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFNotaEmpenho_SaldoAnt) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] >= @AV24TFNotaEmpenho_SaldoAnt)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFNotaEmpenho_SaldoAnt_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] <= @AV25TFNotaEmpenho_SaldoAnt_To)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFNotaEmpenho_SaldoPos) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] >= @AV26TFNotaEmpenho_SaldoPos)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFNotaEmpenho_SaldoPos_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] <= @AV27TFNotaEmpenho_SaldoPos_To)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV28TFNotaEmpenho_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 1)";
         }
         if ( AV28TFNotaEmpenho_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Codigo], T3.[UnidadeMedicao_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00IY3( IGxContext context ,
                                             int AV10TFNotaEmpenho_Codigo ,
                                             int AV11TFNotaEmpenho_Codigo_To ,
                                             int AV12TFSaldoContrato_Codigo ,
                                             int AV13TFSaldoContrato_Codigo_To ,
                                             String AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel ,
                                             String AV14TFSaldoContrato_UnidadeMedicao_Sigla ,
                                             String AV17TFNotaEmpenho_Itentificador_Sel ,
                                             String AV16TFNotaEmpenho_Itentificador ,
                                             DateTime AV18TFNotaEmpenho_DEmissao ,
                                             DateTime AV19TFNotaEmpenho_DEmissao_To ,
                                             decimal AV20TFNotaEmpenho_Qtd ,
                                             decimal AV21TFNotaEmpenho_Qtd_To ,
                                             decimal AV22TFNotaEmpenho_Valor ,
                                             decimal AV23TFNotaEmpenho_Valor_To ,
                                             decimal AV24TFNotaEmpenho_SaldoAnt ,
                                             decimal AV25TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV26TFNotaEmpenho_SaldoPos ,
                                             decimal AV27TFNotaEmpenho_SaldoPos_To ,
                                             short AV28TFNotaEmpenho_Ativo_Sel ,
                                             int A1560NotaEmpenho_Codigo ,
                                             int A1561SaldoContrato_Codigo ,
                                             String A1785SaldoContrato_UnidadeMedicao_Sigla ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             int AV47Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [19] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T2.[Contrato_Codigo], T1.[NotaEmpenho_Itentificador], T1.[NotaEmpenho_Ativo], T1.[NotaEmpenho_SaldoPos], T1.[NotaEmpenho_SaldoAnt], T1.[NotaEmpenho_Valor], T1.[NotaEmpenho_Qtd], T1.[NotaEmpenho_DEmissao], T3.[UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla, T1.[SaldoContrato_Codigo], T1.[NotaEmpenho_Codigo] FROM (([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[SaldoContrato_UnidadeMedicao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contrato_Codigo] = @AV47Contrato_Codigo)";
         if ( ! (0==AV10TFNotaEmpenho_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] >= @AV10TFNotaEmpenho_Codigo)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV11TFNotaEmpenho_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Codigo] <= @AV11TFNotaEmpenho_Codigo_To)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV12TFSaldoContrato_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] >= @AV12TFSaldoContrato_Codigo)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV13TFSaldoContrato_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Codigo] <= @AV13TFSaldoContrato_Codigo_To)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] like @lV14TFSaldoContrato_UnidadeMedicao_Sigla)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Sigla] = @AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFNotaEmpenho_Itentificador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] like @lV16TFNotaEmpenho_Itentificador)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFNotaEmpenho_Itentificador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Itentificador] = @AV17TFNotaEmpenho_Itentificador_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFNotaEmpenho_DEmissao) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] >= @AV18TFNotaEmpenho_DEmissao)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFNotaEmpenho_DEmissao_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_DEmissao] <= @AV19TFNotaEmpenho_DEmissao_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFNotaEmpenho_Qtd) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] >= @AV20TFNotaEmpenho_Qtd)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFNotaEmpenho_Qtd_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Qtd] <= @AV21TFNotaEmpenho_Qtd_To)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFNotaEmpenho_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] >= @AV22TFNotaEmpenho_Valor)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFNotaEmpenho_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Valor] <= @AV23TFNotaEmpenho_Valor_To)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFNotaEmpenho_SaldoAnt) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] >= @AV24TFNotaEmpenho_SaldoAnt)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFNotaEmpenho_SaldoAnt_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoAnt] <= @AV25TFNotaEmpenho_SaldoAnt_To)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFNotaEmpenho_SaldoPos) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] >= @AV26TFNotaEmpenho_SaldoPos)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFNotaEmpenho_SaldoPos_To) )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_SaldoPos] <= @AV27TFNotaEmpenho_SaldoPos_To)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV28TFNotaEmpenho_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 1)";
         }
         if ( AV28TFNotaEmpenho_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[NotaEmpenho_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Codigo], T1.[NotaEmpenho_Itentificador]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00IY2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (decimal)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (bool)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
               case 1 :
                     return conditional_P00IY3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (decimal)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (bool)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00IY2 ;
          prmP00IY2 = new Object[] {
          new Object[] {"@AV47Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFSaldoContrato_UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV16TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV24TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV26TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV27TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00IY3 ;
          prmP00IY3 = new Object[] {
          new Object[] {"@AV47Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFSaldoContrato_UnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFSaldoContrato_UnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV16TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV24TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV26TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV27TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00IY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IY2,100,0,true,false )
             ,new CursorDef("P00IY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IY3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratonotaempenhowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratonotaempenhowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratonotaempenhowcfilterdata") )
          {
             return  ;
          }
          getcontratocontratonotaempenhowcfilterdata worker = new getcontratocontratonotaempenhowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
