/*
               File: PRC_ReplicarTemposEstimados
        Description: Replicar Tempos Estimados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:31.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_replicartemposestimados : GXProcedure
   {
      public prc_replicartemposestimados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_replicartemposestimados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           int aP1_TmpEstAnl ,
                           int aP2_TmpEstExc ,
                           int aP3_TmpEstCrr )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV13TmpEstAnl = aP1_TmpEstAnl;
         this.AV11TmpEstExc = aP2_TmpEstExc;
         this.AV12TmpEstCrr = aP3_TmpEstCrr;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 int aP1_TmpEstAnl ,
                                 int aP2_TmpEstExc ,
                                 int aP3_TmpEstCrr )
      {
         prc_replicartemposestimados objprc_replicartemposestimados;
         objprc_replicartemposestimados = new prc_replicartemposestimados();
         objprc_replicartemposestimados.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_replicartemposestimados.AV13TmpEstAnl = aP1_TmpEstAnl;
         objprc_replicartemposestimados.AV11TmpEstExc = aP2_TmpEstExc;
         objprc_replicartemposestimados.AV12TmpEstCrr = aP3_TmpEstCrr;
         objprc_replicartemposestimados.context.SetSubmitInitialConfig(context);
         objprc_replicartemposestimados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_replicartemposestimados);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_replicartemposestimados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AV2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00AV2_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P00AV2_A39Contratada_Codigo[0];
            A155Servico_Codigo = P00AV2_A155Servico_Codigo[0];
            A39Contratada_Codigo = P00AV2_A39Contratada_Codigo[0];
            AV9Contratada = A39Contratada_Codigo;
            AV10Servico = A155Servico_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00AV3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00AV3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AV3_n1553ContagemResultado_CntSrvCod[0];
            A1519ContagemResultado_TmpEstAnl = P00AV3_A1519ContagemResultado_TmpEstAnl[0];
            n1519ContagemResultado_TmpEstAnl = P00AV3_n1519ContagemResultado_TmpEstAnl[0];
            A1505ContagemResultado_TmpEstExc = P00AV3_A1505ContagemResultado_TmpEstExc[0];
            n1505ContagemResultado_TmpEstExc = P00AV3_n1505ContagemResultado_TmpEstExc[0];
            A1506ContagemResultado_TmpEstCrr = P00AV3_A1506ContagemResultado_TmpEstCrr[0];
            n1506ContagemResultado_TmpEstCrr = P00AV3_n1506ContagemResultado_TmpEstCrr[0];
            A456ContagemResultado_Codigo = P00AV3_A456ContagemResultado_Codigo[0];
            A1519ContagemResultado_TmpEstAnl = AV13TmpEstAnl;
            n1519ContagemResultado_TmpEstAnl = false;
            A1505ContagemResultado_TmpEstExc = AV11TmpEstExc;
            n1505ContagemResultado_TmpEstExc = false;
            A1506ContagemResultado_TmpEstCrr = AV12TmpEstCrr;
            n1506ContagemResultado_TmpEstCrr = false;
            BatchSize = 500;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00av4");
            /* Using cursor P00AV4 */
            pr_default.addRecord(2, new Object[] {n1519ContagemResultado_TmpEstAnl, A1519ContagemResultado_TmpEstAnl, n1505ContagemResultado_TmpEstExc, A1505ContagemResultado_TmpEstExc, n1506ContagemResultado_TmpEstCrr, A1506ContagemResultado_TmpEstCrr, A456ContagemResultado_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp00av4( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         if ( pr_default.getBatchSize(2) > 0 )
         {
            Executebatchp00av4( ) ;
         }
         pr_default.close(1);
         /* Using cursor P00AV5 */
         pr_default.execute(3, new Object[] {AV9Contratada});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = P00AV5_A69ContratadaUsuario_UsuarioCod[0];
            A66ContratadaUsuario_ContratadaCod = P00AV5_A66ContratadaUsuario_ContratadaCod[0];
            A1518ContratadaUsuario_TmpEstAnl = P00AV5_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = P00AV5_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = P00AV5_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = P00AV5_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = P00AV5_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = P00AV5_n1504ContratadaUsuario_TmpEstCrr[0];
            A1518ContratadaUsuario_TmpEstAnl = AV13TmpEstAnl;
            n1518ContratadaUsuario_TmpEstAnl = false;
            A1503ContratadaUsuario_TmpEstExc = AV11TmpEstExc;
            n1503ContratadaUsuario_TmpEstExc = false;
            A1504ContratadaUsuario_TmpEstCrr = AV12TmpEstCrr;
            n1504ContratadaUsuario_TmpEstCrr = false;
            /* Using cursor P00AV6 */
            pr_default.execute(4, new Object[] {A69ContratadaUsuario_UsuarioCod, AV10Servico});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A828UsuarioServicos_UsuarioCod = P00AV6_A828UsuarioServicos_UsuarioCod[0];
               A829UsuarioServicos_ServicoCod = P00AV6_A829UsuarioServicos_ServicoCod[0];
               A1517UsuarioServicos_TmpEstAnl = P00AV6_A1517UsuarioServicos_TmpEstAnl[0];
               n1517UsuarioServicos_TmpEstAnl = P00AV6_n1517UsuarioServicos_TmpEstAnl[0];
               A1513UsuarioServicos_TmpEstExc = P00AV6_A1513UsuarioServicos_TmpEstExc[0];
               n1513UsuarioServicos_TmpEstExc = P00AV6_n1513UsuarioServicos_TmpEstExc[0];
               A1514UsuarioServicos_TmpEstCrr = P00AV6_A1514UsuarioServicos_TmpEstCrr[0];
               n1514UsuarioServicos_TmpEstCrr = P00AV6_n1514UsuarioServicos_TmpEstCrr[0];
               A1517UsuarioServicos_TmpEstAnl = AV13TmpEstAnl;
               n1517UsuarioServicos_TmpEstAnl = false;
               A1513UsuarioServicos_TmpEstExc = AV11TmpEstExc;
               n1513UsuarioServicos_TmpEstExc = false;
               A1514UsuarioServicos_TmpEstCrr = AV12TmpEstCrr;
               n1514UsuarioServicos_TmpEstCrr = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00AV7 */
               pr_default.execute(5, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
               if (true) break;
               /* Using cursor P00AV8 */
               pr_default.execute(6, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            BatchSize = 100;
            pr_default.initializeBatch( 7, BatchSize, this, "Executebatchp00av9");
            /* Using cursor P00AV9 */
            pr_default.addRecord(7, new Object[] {n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, n1504ContratadaUsuario_TmpEstCrr, A1504ContratadaUsuario_TmpEstCrr, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            if ( pr_default.recordCount(7) == pr_default.getBatchSize(7) )
            {
               Executebatchp00av9( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
            pr_default.readNext(3);
         }
         if ( pr_default.getBatchSize(7) > 0 )
         {
            Executebatchp00av9( ) ;
         }
         pr_default.close(3);
         this.cleanup();
      }

      protected void Executebatchp00av4( )
      {
         /* Using cursor P00AV4 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      protected void Executebatchp00av9( )
      {
         /* Using cursor P00AV9 */
         pr_default.executeBatch(7);
         pr_default.close(7);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ReplicarTemposEstimados");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(7);
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AV2_A74Contrato_Codigo = new int[1] ;
         P00AV2_A160ContratoServicos_Codigo = new int[1] ;
         P00AV2_A39Contratada_Codigo = new int[1] ;
         P00AV2_A155Servico_Codigo = new int[1] ;
         P00AV3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AV3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AV3_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00AV3_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00AV3_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00AV3_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00AV3_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P00AV3_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P00AV3_A456ContagemResultado_Codigo = new int[1] ;
         P00AV4_A1519ContagemResultado_TmpEstAnl = new int[1] ;
         P00AV4_n1519ContagemResultado_TmpEstAnl = new bool[] {false} ;
         P00AV4_A1505ContagemResultado_TmpEstExc = new int[1] ;
         P00AV4_n1505ContagemResultado_TmpEstExc = new bool[] {false} ;
         P00AV4_A1506ContagemResultado_TmpEstCrr = new int[1] ;
         P00AV4_n1506ContagemResultado_TmpEstCrr = new bool[] {false} ;
         P00AV4_A456ContagemResultado_Codigo = new int[1] ;
         P00AV5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00AV5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00AV5_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         P00AV5_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         P00AV5_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         P00AV5_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         P00AV5_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         P00AV5_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         P00AV6_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P00AV6_A829UsuarioServicos_ServicoCod = new int[1] ;
         P00AV6_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         P00AV6_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         P00AV6_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         P00AV6_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         P00AV6_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         P00AV6_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         P00AV9_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         P00AV9_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         P00AV9_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         P00AV9_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         P00AV9_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         P00AV9_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         P00AV9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00AV9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_replicartemposestimados__default(),
            new Object[][] {
                new Object[] {
               P00AV2_A74Contrato_Codigo, P00AV2_A160ContratoServicos_Codigo, P00AV2_A39Contratada_Codigo, P00AV2_A155Servico_Codigo
               }
               , new Object[] {
               P00AV3_A1553ContagemResultado_CntSrvCod, P00AV3_n1553ContagemResultado_CntSrvCod, P00AV3_A1519ContagemResultado_TmpEstAnl, P00AV3_n1519ContagemResultado_TmpEstAnl, P00AV3_A1505ContagemResultado_TmpEstExc, P00AV3_n1505ContagemResultado_TmpEstExc, P00AV3_A1506ContagemResultado_TmpEstCrr, P00AV3_n1506ContagemResultado_TmpEstCrr, P00AV3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00AV5_A69ContratadaUsuario_UsuarioCod, P00AV5_A66ContratadaUsuario_ContratadaCod, P00AV5_A1518ContratadaUsuario_TmpEstAnl, P00AV5_n1518ContratadaUsuario_TmpEstAnl, P00AV5_A1503ContratadaUsuario_TmpEstExc, P00AV5_n1503ContratadaUsuario_TmpEstExc, P00AV5_A1504ContratadaUsuario_TmpEstCrr, P00AV5_n1504ContratadaUsuario_TmpEstCrr
               }
               , new Object[] {
               P00AV6_A828UsuarioServicos_UsuarioCod, P00AV6_A829UsuarioServicos_ServicoCod, P00AV6_A1517UsuarioServicos_TmpEstAnl, P00AV6_n1517UsuarioServicos_TmpEstAnl, P00AV6_A1513UsuarioServicos_TmpEstExc, P00AV6_n1513UsuarioServicos_TmpEstExc, P00AV6_A1514UsuarioServicos_TmpEstCrr, P00AV6_n1514UsuarioServicos_TmpEstCrr
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int AV13TmpEstAnl ;
      private int AV11TmpEstExc ;
      private int AV12TmpEstCrr ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int AV9Contratada ;
      private int AV10Servico ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1519ContagemResultado_TmpEstAnl ;
      private int A1505ContagemResultado_TmpEstExc ;
      private int A1506ContagemResultado_TmpEstCrr ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1518ContratadaUsuario_TmpEstAnl ;
      private int A1503ContratadaUsuario_TmpEstExc ;
      private int A1504ContratadaUsuario_TmpEstCrr ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A1517UsuarioServicos_TmpEstAnl ;
      private int A1513UsuarioServicos_TmpEstExc ;
      private int A1514UsuarioServicos_TmpEstCrr ;
      private String scmdbuf ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1519ContagemResultado_TmpEstAnl ;
      private bool n1505ContagemResultado_TmpEstExc ;
      private bool n1506ContagemResultado_TmpEstCrr ;
      private bool n1518ContratadaUsuario_TmpEstAnl ;
      private bool n1503ContratadaUsuario_TmpEstExc ;
      private bool n1504ContratadaUsuario_TmpEstCrr ;
      private bool n1517UsuarioServicos_TmpEstAnl ;
      private bool n1513UsuarioServicos_TmpEstExc ;
      private bool n1514UsuarioServicos_TmpEstCrr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AV2_A74Contrato_Codigo ;
      private int[] P00AV2_A160ContratoServicos_Codigo ;
      private int[] P00AV2_A39Contratada_Codigo ;
      private int[] P00AV2_A155Servico_Codigo ;
      private int[] P00AV3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AV3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AV3_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00AV3_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00AV3_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00AV3_n1505ContagemResultado_TmpEstExc ;
      private int[] P00AV3_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P00AV3_n1506ContagemResultado_TmpEstCrr ;
      private int[] P00AV3_A456ContagemResultado_Codigo ;
      private int[] P00AV4_A1519ContagemResultado_TmpEstAnl ;
      private bool[] P00AV4_n1519ContagemResultado_TmpEstAnl ;
      private int[] P00AV4_A1505ContagemResultado_TmpEstExc ;
      private bool[] P00AV4_n1505ContagemResultado_TmpEstExc ;
      private int[] P00AV4_A1506ContagemResultado_TmpEstCrr ;
      private bool[] P00AV4_n1506ContagemResultado_TmpEstCrr ;
      private int[] P00AV4_A456ContagemResultado_Codigo ;
      private int[] P00AV5_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00AV5_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00AV5_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] P00AV5_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] P00AV5_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] P00AV5_n1503ContratadaUsuario_TmpEstExc ;
      private int[] P00AV5_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] P00AV5_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] P00AV6_A828UsuarioServicos_UsuarioCod ;
      private int[] P00AV6_A829UsuarioServicos_ServicoCod ;
      private int[] P00AV6_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] P00AV6_n1517UsuarioServicos_TmpEstAnl ;
      private int[] P00AV6_A1513UsuarioServicos_TmpEstExc ;
      private bool[] P00AV6_n1513UsuarioServicos_TmpEstExc ;
      private int[] P00AV6_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] P00AV6_n1514UsuarioServicos_TmpEstCrr ;
      private int[] P00AV9_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] P00AV9_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] P00AV9_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] P00AV9_n1503ContratadaUsuario_TmpEstExc ;
      private int[] P00AV9_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] P00AV9_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] P00AV9_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00AV9_A69ContratadaUsuario_UsuarioCod ;
   }

   public class prc_replicartemposestimados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new BatchUpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AV2 ;
          prmP00AV2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV3 ;
          prmP00AV3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV4 ;
          prmP00AV4 = new Object[] {
          new Object[] {"@ContagemResultado_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV5 ;
          prmP00AV5 = new Object[] {
          new Object[] {"@AV9Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV6 ;
          prmP00AV6 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV7 ;
          prmP00AV7 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV8 ;
          prmP00AV8 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AV9 ;
          prmP00AV9 = new Object[] {
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AV2", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T2.[Contratada_Codigo], T1.[Servico_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AV2,1,0,false,true )
             ,new CursorDef("P00AV3", "SELECT [ContagemResultado_CntSrvCod], [ContagemResultado_TmpEstAnl], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContagemResultado_CntSrvCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AV3,1,0,true,false )
             ,new CursorDef("P00AV4", "UPDATE [ContagemResultado] SET [ContagemResultado_TmpEstAnl]=@ContagemResultado_TmpEstAnl, [ContagemResultado_TmpEstExc]=@ContagemResultado_TmpEstExc, [ContagemResultado_TmpEstCrr]=@ContagemResultado_TmpEstCrr  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AV4)
             ,new CursorDef("P00AV5", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV9Contratada ORDER BY [ContratadaUsuario_ContratadaCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AV5,1,0,true,false )
             ,new CursorDef("P00AV6", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod], [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr] FROM [UsuarioServicos] WITH (UPDLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV10Servico ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AV6,1,0,true,true )
             ,new CursorDef("P00AV7", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl, [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc, [UsuarioServicos_TmpEstCrr]=@UsuarioServicos_TmpEstCrr  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AV7)
             ,new CursorDef("P00AV8", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl, [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc, [UsuarioServicos_TmpEstCrr]=@UsuarioServicos_TmpEstCrr  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AV8)
             ,new CursorDef("P00AV9", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_TmpEstAnl]=@ContratadaUsuario_TmpEstAnl, [ContratadaUsuario_TmpEstExc]=@ContratadaUsuario_TmpEstExc, [ContratadaUsuario_TmpEstCrr]=@ContratadaUsuario_TmpEstCrr  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AV9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
       }
    }

 }

}
