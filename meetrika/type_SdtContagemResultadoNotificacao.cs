/*
               File: type_SdtContagemResultadoNotificacao
        Description: Notificações da Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:27:27.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoNotificacao" )]
   [XmlType(TypeName =  "ContagemResultadoNotificacao" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtContagemResultadoNotificacao_Destinatario ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtContagemResultadoNotificacao_Demanda ))]
   [Serializable]
   public class SdtContagemResultadoNotificacao : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotificacao( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user = "";
         gxTv_SdtContagemResultadoNotificacao_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z = "";
      }

      public SdtContagemResultadoNotificacao( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( long AV1412ContagemResultadoNotificacao_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(long)AV1412ContagemResultadoNotificacao_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoNotificacao_Codigo", typeof(long)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoNotificacao");
         metadata.Set("BT", "ContagemResultadoNotificacao");
         metadata.Set("PK", "[ \"ContagemResultadoNotificacao_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoNotificacao_Codigo\" ]");
         metadata.Set("Levels", "[ \"Demanda\",\"Destinatario\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNotificacao_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] },{ \"FK\":[ \"ContagemResultadoNotificacao_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNotificacao_ReenvioCod-ContagemResultadoNotificacao_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNotificacao_UsuCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Destinatario_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Demanda_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_datahora_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_usucod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_usupescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_usunom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_corpoemail_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_midia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_host_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_user_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_port_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_aut_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_sec_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_logged_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destinatarios_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_demandas_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_reenviocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_datahora_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_areatrabalhocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_usupescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_usunom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_assunto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_corpoemail_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_midia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_observacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_host_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_user_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_port_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_aut_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_sec_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_logged_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_destinatarios_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_demandas_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotificacao_reenviocod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoNotificacao deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoNotificacao)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoNotificacao obj ;
         obj = this;
         obj.gxTpr_Contagemresultadonotificacao_codigo = deserialized.gxTpr_Contagemresultadonotificacao_codigo;
         obj.gxTpr_Contagemresultadonotificacao_datahora = deserialized.gxTpr_Contagemresultadonotificacao_datahora;
         obj.gxTpr_Contagemresultadonotificacao_areatrabalhocod = deserialized.gxTpr_Contagemresultadonotificacao_areatrabalhocod;
         obj.gxTpr_Contagemresultadonotificacao_usucod = deserialized.gxTpr_Contagemresultadonotificacao_usucod;
         obj.gxTpr_Contagemresultadonotificacao_usupescod = deserialized.gxTpr_Contagemresultadonotificacao_usupescod;
         obj.gxTpr_Contagemresultadonotificacao_usunom = deserialized.gxTpr_Contagemresultadonotificacao_usunom;
         obj.gxTpr_Contagemresultadonotificacao_assunto = deserialized.gxTpr_Contagemresultadonotificacao_assunto;
         obj.gxTpr_Contagemresultadonotificacao_corpoemail = deserialized.gxTpr_Contagemresultadonotificacao_corpoemail;
         obj.gxTpr_Contagemresultadonotificacao_midia = deserialized.gxTpr_Contagemresultadonotificacao_midia;
         obj.gxTpr_Contagemresultadonotificacao_observacao = deserialized.gxTpr_Contagemresultadonotificacao_observacao;
         obj.gxTpr_Contagemresultadonotificacao_host = deserialized.gxTpr_Contagemresultadonotificacao_host;
         obj.gxTpr_Contagemresultadonotificacao_user = deserialized.gxTpr_Contagemresultadonotificacao_user;
         obj.gxTpr_Contagemresultadonotificacao_port = deserialized.gxTpr_Contagemresultadonotificacao_port;
         obj.gxTpr_Contagemresultadonotificacao_aut = deserialized.gxTpr_Contagemresultadonotificacao_aut;
         obj.gxTpr_Contagemresultadonotificacao_sec = deserialized.gxTpr_Contagemresultadonotificacao_sec;
         obj.gxTpr_Contagemresultadonotificacao_logged = deserialized.gxTpr_Contagemresultadonotificacao_logged;
         obj.gxTpr_Contagemresultadonotificacao_destinatarios = deserialized.gxTpr_Contagemresultadonotificacao_destinatarios;
         obj.gxTpr_Contagemresultadonotificacao_demandas = deserialized.gxTpr_Contagemresultadonotificacao_demandas;
         obj.gxTpr_Contagemresultadonotificacao_reenviocod = deserialized.gxTpr_Contagemresultadonotificacao_reenviocod;
         obj.gxTpr_Destinatario = deserialized.gxTpr_Destinatario;
         obj.gxTpr_Demanda = deserialized.gxTpr_Demanda;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadonotificacao_codigo_Z = deserialized.gxTpr_Contagemresultadonotificacao_codigo_Z;
         obj.gxTpr_Contagemresultadonotificacao_datahora_Z = deserialized.gxTpr_Contagemresultadonotificacao_datahora_Z;
         obj.gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z = deserialized.gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z;
         obj.gxTpr_Contagemresultadonotificacao_usucod_Z = deserialized.gxTpr_Contagemresultadonotificacao_usucod_Z;
         obj.gxTpr_Contagemresultadonotificacao_usupescod_Z = deserialized.gxTpr_Contagemresultadonotificacao_usupescod_Z;
         obj.gxTpr_Contagemresultadonotificacao_usunom_Z = deserialized.gxTpr_Contagemresultadonotificacao_usunom_Z;
         obj.gxTpr_Contagemresultadonotificacao_corpoemail_Z = deserialized.gxTpr_Contagemresultadonotificacao_corpoemail_Z;
         obj.gxTpr_Contagemresultadonotificacao_midia_Z = deserialized.gxTpr_Contagemresultadonotificacao_midia_Z;
         obj.gxTpr_Contagemresultadonotificacao_host_Z = deserialized.gxTpr_Contagemresultadonotificacao_host_Z;
         obj.gxTpr_Contagemresultadonotificacao_user_Z = deserialized.gxTpr_Contagemresultadonotificacao_user_Z;
         obj.gxTpr_Contagemresultadonotificacao_port_Z = deserialized.gxTpr_Contagemresultadonotificacao_port_Z;
         obj.gxTpr_Contagemresultadonotificacao_aut_Z = deserialized.gxTpr_Contagemresultadonotificacao_aut_Z;
         obj.gxTpr_Contagemresultadonotificacao_sec_Z = deserialized.gxTpr_Contagemresultadonotificacao_sec_Z;
         obj.gxTpr_Contagemresultadonotificacao_logged_Z = deserialized.gxTpr_Contagemresultadonotificacao_logged_Z;
         obj.gxTpr_Contagemresultadonotificacao_destinatarios_Z = deserialized.gxTpr_Contagemresultadonotificacao_destinatarios_Z;
         obj.gxTpr_Contagemresultadonotificacao_demandas_Z = deserialized.gxTpr_Contagemresultadonotificacao_demandas_Z;
         obj.gxTpr_Contagemresultadonotificacao_reenviocod_Z = deserialized.gxTpr_Contagemresultadonotificacao_reenviocod_Z;
         obj.gxTpr_Contagemresultadonotificacao_datahora_N = deserialized.gxTpr_Contagemresultadonotificacao_datahora_N;
         obj.gxTpr_Contagemresultadonotificacao_areatrabalhocod_N = deserialized.gxTpr_Contagemresultadonotificacao_areatrabalhocod_N;
         obj.gxTpr_Contagemresultadonotificacao_usupescod_N = deserialized.gxTpr_Contagemresultadonotificacao_usupescod_N;
         obj.gxTpr_Contagemresultadonotificacao_usunom_N = deserialized.gxTpr_Contagemresultadonotificacao_usunom_N;
         obj.gxTpr_Contagemresultadonotificacao_assunto_N = deserialized.gxTpr_Contagemresultadonotificacao_assunto_N;
         obj.gxTpr_Contagemresultadonotificacao_corpoemail_N = deserialized.gxTpr_Contagemresultadonotificacao_corpoemail_N;
         obj.gxTpr_Contagemresultadonotificacao_midia_N = deserialized.gxTpr_Contagemresultadonotificacao_midia_N;
         obj.gxTpr_Contagemresultadonotificacao_observacao_N = deserialized.gxTpr_Contagemresultadonotificacao_observacao_N;
         obj.gxTpr_Contagemresultadonotificacao_host_N = deserialized.gxTpr_Contagemresultadonotificacao_host_N;
         obj.gxTpr_Contagemresultadonotificacao_user_N = deserialized.gxTpr_Contagemresultadonotificacao_user_N;
         obj.gxTpr_Contagemresultadonotificacao_port_N = deserialized.gxTpr_Contagemresultadonotificacao_port_N;
         obj.gxTpr_Contagemresultadonotificacao_aut_N = deserialized.gxTpr_Contagemresultadonotificacao_aut_N;
         obj.gxTpr_Contagemresultadonotificacao_sec_N = deserialized.gxTpr_Contagemresultadonotificacao_sec_N;
         obj.gxTpr_Contagemresultadonotificacao_logged_N = deserialized.gxTpr_Contagemresultadonotificacao_logged_N;
         obj.gxTpr_Contagemresultadonotificacao_destinatarios_N = deserialized.gxTpr_Contagemresultadonotificacao_destinatarios_N;
         obj.gxTpr_Contagemresultadonotificacao_demandas_N = deserialized.gxTpr_Contagemresultadonotificacao_demandas_N;
         obj.gxTpr_Contagemresultadonotificacao_reenviocod_N = deserialized.gxTpr_Contagemresultadonotificacao_reenviocod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Codigo") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DataHora") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_AreaTrabalhoCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuPesCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuNom") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Assunto") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_CorpoEmail") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Midia") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Observacao") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Host") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_User") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Port") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Aut") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Sec") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Logged") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Destinatarios") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Demandas") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_ReenvioCod") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Destinatario") )
               {
                  if ( gxTv_SdtContagemResultadoNotificacao_Destinatario == null )
                  {
                     gxTv_SdtContagemResultadoNotificacao_Destinatario = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Destinatario", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Destinatario", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtContagemResultadoNotificacao_Destinatario.readxml(oReader, "Destinatario");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Demanda") )
               {
                  if ( gxTv_SdtContagemResultadoNotificacao_Demanda == null )
                  {
                     gxTv_SdtContagemResultadoNotificacao_Demanda = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Demanda", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Demanda", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtContagemResultadoNotificacao_Demanda.readxml(oReader, "Demanda");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DataHora_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuPesCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuNom_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_CorpoEmail_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Midia_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Host_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_User_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Port_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Aut_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Sec_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Logged_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Destinatarios_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Demandas_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_ReenvioCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_DataHora_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuPesCod_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_UsuNom_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Assunto_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_CorpoEmail_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Midia_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Observacao_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Host_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_User_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Port_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Aut_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Sec_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Logged_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Destinatarios_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_Demandas_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotificacao_ReenvioCod_N") )
               {
                  gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoNotificacao";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora) )
         {
            oWriter.WriteStartElement("ContagemResultadoNotificacao_DataHora");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoNotificacao_DataHora", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_UsuCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_UsuPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_UsuNom", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Assunto", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_CorpoEmail", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Midia", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Observacao", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Host", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_User", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Port", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Aut", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Sec", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Logged", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Destinatarios", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_Demandas", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoNotificacao_ReenvioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtContagemResultadoNotificacao_Destinatario != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_Meetrika";
               }
               gxTv_SdtContagemResultadoNotificacao_Destinatario.writexml(oWriter, "Destinatario", sNameSpace1);
            }
            if ( gxTv_SdtContagemResultadoNotificacao_Demanda != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_Meetrika";
               }
               gxTv_SdtContagemResultadoNotificacao_Demanda.writexml(oWriter, "Demanda", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoNotificacao_DataHora_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoNotificacao_DataHora_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_UsuCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_UsuPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_UsuNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_CorpoEmail_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Midia_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Host_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_User_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Port_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Aut_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Sec_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Logged_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Destinatarios_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Demandas_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_ReenvioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_DataHora_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_UsuPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_UsuNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Assunto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_CorpoEmail_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Midia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Observacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Host_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_User_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Port_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Aut_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Sec_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Logged_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Destinatarios_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_Demandas_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoNotificacao_ReenvioCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoNotificacao_Codigo", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo, false);
         datetime_STZ = gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoNotificacao_DataHora", sDateCnv, false);
         AddObjectProperty("ContagemResultadoNotificacao_AreaTrabalhoCod", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod, false);
         AddObjectProperty("ContagemResultadoNotificacao_UsuCod", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod, false);
         AddObjectProperty("ContagemResultadoNotificacao_UsuPesCod", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod, false);
         AddObjectProperty("ContagemResultadoNotificacao_UsuNom", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom, false);
         AddObjectProperty("ContagemResultadoNotificacao_Assunto", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto, false);
         AddObjectProperty("ContagemResultadoNotificacao_CorpoEmail", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail, false);
         AddObjectProperty("ContagemResultadoNotificacao_Midia", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia, false);
         AddObjectProperty("ContagemResultadoNotificacao_Observacao", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao, false);
         AddObjectProperty("ContagemResultadoNotificacao_Host", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host, false);
         AddObjectProperty("ContagemResultadoNotificacao_User", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user, false);
         AddObjectProperty("ContagemResultadoNotificacao_Port", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port, false);
         AddObjectProperty("ContagemResultadoNotificacao_Aut", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut, false);
         AddObjectProperty("ContagemResultadoNotificacao_Sec", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec, false);
         AddObjectProperty("ContagemResultadoNotificacao_Logged", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged, false);
         AddObjectProperty("ContagemResultadoNotificacao_Destinatarios", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios, false);
         AddObjectProperty("ContagemResultadoNotificacao_Demandas", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas, false);
         AddObjectProperty("ContagemResultadoNotificacao_ReenvioCod", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod, false);
         if ( gxTv_SdtContagemResultadoNotificacao_Destinatario != null )
         {
            AddObjectProperty("Destinatario", gxTv_SdtContagemResultadoNotificacao_Destinatario, includeState);
         }
         if ( gxTv_SdtContagemResultadoNotificacao_Demanda != null )
         {
            AddObjectProperty("Demanda", gxTv_SdtContagemResultadoNotificacao_Demanda, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoNotificacao_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoNotificacao_Initialized, false);
            AddObjectProperty("ContagemResultadoNotificacao_Codigo_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoNotificacao_DataHora_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultadoNotificacao_AreaTrabalhoCod_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_UsuCod_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_UsuPesCod_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_UsuNom_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_CorpoEmail_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Midia_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Host_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_User_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Port_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Aut_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Sec_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Logged_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Destinatarios_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_Demandas_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_ReenvioCod_Z", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z, false);
            AddObjectProperty("ContagemResultadoNotificacao_DataHora_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_AreaTrabalhoCod_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_UsuPesCod_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_UsuNom_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Assunto_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_CorpoEmail_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Midia_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Observacao_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Host_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_User_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Port_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Aut_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Sec_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Logged_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Destinatarios_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_Demandas_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N, false);
            AddObjectProperty("ContagemResultadoNotificacao_ReenvioCod_N", gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Codigo"   )]
      public long gxTpr_Contagemresultadonotificacao_codigo
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo != value )
            {
               gxTv_SdtContagemResultadoNotificacao_Mode = "INS";
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z_SetNull( );
               if ( gxTv_SdtContagemResultadoNotificacao_Destinatario != null )
               {
                  GxSilentTrnGridCollection collectionDestinatario = gxTv_SdtContagemResultadoNotificacao_Destinatario ;
                  SdtContagemResultadoNotificacao_Destinatario currItemDestinatario ;
                  short idx = 1 ;
                  while ( idx <= collectionDestinatario.Count )
                  {
                     currItemDestinatario = ((SdtContagemResultadoNotificacao_Destinatario)collectionDestinatario.Item(idx));
                     currItemDestinatario.gxTpr_Mode = "INS";
                     currItemDestinatario.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
               if ( gxTv_SdtContagemResultadoNotificacao_Demanda != null )
               {
                  GxSilentTrnGridCollection collectionDemanda = gxTv_SdtContagemResultadoNotificacao_Demanda ;
                  SdtContagemResultadoNotificacao_Demanda currItemDemanda ;
                  short idx = 1 ;
                  while ( idx <= collectionDemanda.Count )
                  {
                     currItemDemanda = ((SdtContagemResultadoNotificacao_Demanda)collectionDemanda.Item(idx));
                     currItemDemanda.gxTpr_Mode = "INS";
                     currItemDemanda.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DataHora" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DataHora"  , IsNullable=true )]
      public string gxTpr_Contagemresultadonotificacao_datahora_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora).value ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadonotificacao_datahora
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod"   )]
      public int gxTpr_Contagemresultadonotificacao_areatrabalhocod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuCod"   )]
      public int gxTpr_Contagemresultadonotificacao_usucod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod"   )]
      public int gxTpr_Contagemresultadonotificacao_usupescod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuNom" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuNom"   )]
      public String gxTpr_Contagemresultadonotificacao_usunom
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Assunto" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Assunto"   )]
      public String gxTpr_Contagemresultadonotificacao_assunto
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail"   )]
      public String gxTpr_Contagemresultadonotificacao_corpoemail
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Midia" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Midia"   )]
      public String gxTpr_Contagemresultadonotificacao_midia
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Observacao" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Observacao"   )]
      public String gxTpr_Contagemresultadonotificacao_observacao
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Host" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Host"   )]
      public String gxTpr_Contagemresultadonotificacao_host
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_User" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_User"   )]
      public String gxTpr_Contagemresultadonotificacao_user
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Port" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Port"   )]
      public short gxTpr_Contagemresultadonotificacao_port
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Aut" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Aut"   )]
      public bool gxTpr_Contagemresultadonotificacao_aut
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Sec" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Sec"   )]
      public short gxTpr_Contagemresultadonotificacao_sec
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Logged" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Logged"   )]
      public short gxTpr_Contagemresultadonotificacao_logged
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Destinatarios" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Destinatarios"   )]
      public int gxTpr_Contagemresultadonotificacao_destinatarios
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Demandas" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Demandas"   )]
      public int gxTpr_Contagemresultadonotificacao_demandas
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod"   )]
      public long gxTpr_Contagemresultadonotificacao_reenviocod
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N = 0;
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod = (long)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N = 1;
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_IsNull( )
      {
         return false ;
      }

      public class gxTv_SdtContagemResultadoNotificacao_Destinatario_SdtContagemResultadoNotificacao_Destinatario_80compatibility:SdtContagemResultadoNotificacao_Destinatario {}
      [  SoapElement( ElementName = "Destinatario" )]
      [  XmlArray( ElementName = "Destinatario"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtContagemResultadoNotificacao_Destinatario ), ElementName= "ContagemResultadoNotificacao.Destinatario"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Destinatario_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Destinatario == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Destinatario = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Destinatario", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Destinatario", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtContagemResultadoNotificacao_Destinatario ;
         }

         set {
            if ( gxTv_SdtContagemResultadoNotificacao_Destinatario == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Destinatario = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Destinatario", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Destinatario", "GeneXus.Programs");
            }
            gxTv_SdtContagemResultadoNotificacao_Destinatario = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Destinatario
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Destinatario == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Destinatario = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Destinatario", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Destinatario", "GeneXus.Programs");
            }
            return gxTv_SdtContagemResultadoNotificacao_Destinatario ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Destinatario = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Destinatario_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Destinatario = null;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Destinatario_IsNull( )
      {
         if ( gxTv_SdtContagemResultadoNotificacao_Destinatario == null )
         {
            return true ;
         }
         return false ;
      }

      public class gxTv_SdtContagemResultadoNotificacao_Demanda_SdtContagemResultadoNotificacao_Demanda_80compatibility:SdtContagemResultadoNotificacao_Demanda {}
      [  SoapElement( ElementName = "Demanda" )]
      [  XmlArray( ElementName = "Demanda"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtContagemResultadoNotificacao_Demanda ), ElementName= "ContagemResultadoNotificacao.Demanda"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Demanda_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Demanda == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Demanda = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Demanda", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Demanda", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtContagemResultadoNotificacao_Demanda ;
         }

         set {
            if ( gxTv_SdtContagemResultadoNotificacao_Demanda == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Demanda = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Demanda", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Demanda", "GeneXus.Programs");
            }
            gxTv_SdtContagemResultadoNotificacao_Demanda = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Demanda
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Demanda == null )
            {
               gxTv_SdtContagemResultadoNotificacao_Demanda = new GxSilentTrnGridCollection( context, "ContagemResultadoNotificacao.Demanda", "GxEv3Up14_Meetrika", "SdtContagemResultadoNotificacao_Demanda", "GeneXus.Programs");
            }
            return gxTv_SdtContagemResultadoNotificacao_Demanda ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Demanda = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Demanda_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Demanda = null;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Demanda_IsNull( )
      {
         if ( gxTv_SdtContagemResultadoNotificacao_Demanda == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Codigo_Z"   )]
      public long gxTpr_Contagemresultadonotificacao_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z = (long)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DataHora_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DataHora_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadonotificacao_datahora_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadonotificacao_datahora_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuCod_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_usucod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_usupescod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuNom_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_usunom_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_corpoemail_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Midia_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Midia_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_midia_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Host_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Host_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_host_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_User_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_User_Z"   )]
      public String gxTpr_Contagemresultadonotificacao_user_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Port_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Port_Z"   )]
      public short gxTpr_Contagemresultadonotificacao_port_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Aut_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Aut_Z"   )]
      public bool gxTpr_Contagemresultadonotificacao_aut_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Sec_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Sec_Z"   )]
      public short gxTpr_Contagemresultadonotificacao_sec_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Logged_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Logged_Z"   )]
      public short gxTpr_Contagemresultadonotificacao_logged_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Destinatarios_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Destinatarios_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_destinatarios_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Demandas_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Demandas_Z"   )]
      public int gxTpr_Contagemresultadonotificacao_demandas_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod_Z"   )]
      public long gxTpr_Contagemresultadonotificacao_reenviocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z = (long)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_DataHora_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_DataHora_N"   )]
      public short gxTpr_Contagemresultadonotificacao_datahora_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_AreaTrabalhoCod_N"   )]
      public short gxTpr_Contagemresultadonotificacao_areatrabalhocod_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuPesCod_N"   )]
      public short gxTpr_Contagemresultadonotificacao_usupescod_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_UsuNom_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_UsuNom_N"   )]
      public short gxTpr_Contagemresultadonotificacao_usunom_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Assunto_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Assunto_N"   )]
      public short gxTpr_Contagemresultadonotificacao_assunto_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_CorpoEmail_N"   )]
      public short gxTpr_Contagemresultadonotificacao_corpoemail_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Midia_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Midia_N"   )]
      public short gxTpr_Contagemresultadonotificacao_midia_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Observacao_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Observacao_N"   )]
      public short gxTpr_Contagemresultadonotificacao_observacao_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Host_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Host_N"   )]
      public short gxTpr_Contagemresultadonotificacao_host_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_User_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_User_N"   )]
      public short gxTpr_Contagemresultadonotificacao_user_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Port_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Port_N"   )]
      public short gxTpr_Contagemresultadonotificacao_port_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Aut_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Aut_N"   )]
      public short gxTpr_Contagemresultadonotificacao_aut_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Sec_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Sec_N"   )]
      public short gxTpr_Contagemresultadonotificacao_sec_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Logged_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Logged_N"   )]
      public short gxTpr_Contagemresultadonotificacao_logged_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Destinatarios_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Destinatarios_N"   )]
      public short gxTpr_Contagemresultadonotificacao_destinatarios_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_Demandas_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_Demandas_N"   )]
      public short gxTpr_Contagemresultadonotificacao_demandas_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotificacao_ReenvioCod_N"   )]
      public short gxTpr_Contagemresultadonotificacao_reenviocod_N
      {
         get {
            return gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user = "";
         gxTv_SdtContagemResultadoNotificacao_Mode = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z = "";
         gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadonotificacao", "GeneXus.Programs.contagemresultadonotificacao_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged ;
      private short gxTv_SdtContagemResultadoNotificacao_Initialized ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_Z ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_Z ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_Z ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_port_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_sec_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_logged_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_N ;
      private short gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_areatrabalhocod_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usucod_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usupescod_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_destinatarios_Z ;
      private int gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_demandas_Z ;
      private long gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo ;
      private long gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod ;
      private long gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_codigo_Z ;
      private long gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_reenviocod_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user ;
      private String gxTv_SdtContagemResultadoNotificacao_Mode ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_usunom_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_midia_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_host_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_user_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora ;
      private DateTime gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_datahora_Z ;
      private DateTime datetime_STZ ;
      private bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut ;
      private bool gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_aut_Z ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_assunto ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_observacao ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail ;
      private String gxTv_SdtContagemResultadoNotificacao_Contagemresultadonotificacao_corpoemail_Z ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtContagemResultadoNotificacao_Destinatario ))]
      private GxSilentTrnGridCollection gxTv_SdtContagemResultadoNotificacao_Destinatario=null ;
      [ObjectCollection(ItemType=typeof( SdtContagemResultadoNotificacao_Demanda ))]
      private GxSilentTrnGridCollection gxTv_SdtContagemResultadoNotificacao_Demanda=null ;
   }

   [DataContract(Name = @"ContagemResultadoNotificacao", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContagemResultadoNotificacao_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoNotificacao>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotificacao_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoNotificacao_RESTInterface( SdtContagemResultadoNotificacao psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoNotificacao_Codigo" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_codigo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultadonotificacao_codigo), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_codigo = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_DataHora" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_datahora
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadonotificacao_datahora) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_datahora = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_AreaTrabalhoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_UsuCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_usucod
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_usucod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_usucod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_UsuPesCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_usupescod
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_usupescod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_usupescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_UsuNom" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_usunom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonotificacao_usunom) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_usunom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Assunto" , Order = 6 )]
      public String gxTpr_Contagemresultadonotificacao_assunto
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_assunto ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_assunto = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_CorpoEmail" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_corpoemail
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_corpoemail ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_corpoemail = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Midia" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_midia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonotificacao_midia) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_midia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Observacao" , Order = 9 )]
      public String gxTpr_Contagemresultadonotificacao_observacao
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_observacao ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_observacao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Host" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_host
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonotificacao_host) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_host = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_User" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_user
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonotificacao_user) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_user = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Port" , Order = 12 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultadonotificacao_port
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_port ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_port = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Aut" , Order = 13 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultadonotificacao_aut
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_aut ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_aut = value;
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Sec" , Order = 14 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultadonotificacao_sec
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_sec ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_sec = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Logged" , Order = 15 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultadonotificacao_logged
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_logged ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_logged = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Destinatarios" , Order = 16 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_destinatarios
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_destinatarios ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_destinatarios = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_Demandas" , Order = 17 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonotificacao_demandas
      {
         get {
            return sdt.gxTpr_Contagemresultadonotificacao_demandas ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_demandas = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNotificacao_ReenvioCod" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonotificacao_reenviocod
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultadonotificacao_reenviocod), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotificacao_reenviocod = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Destinatario" , Order = 19 )]
      public GxGenericCollection<SdtContagemResultadoNotificacao_Destinatario_RESTInterface> gxTpr_Destinatario
      {
         get {
            return new GxGenericCollection<SdtContagemResultadoNotificacao_Destinatario_RESTInterface>(sdt.gxTpr_Destinatario) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Destinatario);
         }

      }

      [DataMember( Name = "Demanda" , Order = 20 )]
      public GxGenericCollection<SdtContagemResultadoNotificacao_Demanda_RESTInterface> gxTpr_Demanda
      {
         get {
            return new GxGenericCollection<SdtContagemResultadoNotificacao_Demanda_RESTInterface>(sdt.gxTpr_Demanda) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Demanda);
         }

      }

      public SdtContagemResultadoNotificacao sdt
      {
         get {
            return (SdtContagemResultadoNotificacao)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoNotificacao() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 57 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
