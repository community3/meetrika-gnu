/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 22:57:40.91
*/
gx.evt.autoSkip = false;
gx.define('wclogresponsavel', true, function (CmpContext) {
   this.ServerClass =  "wclogresponsavel" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7LogResponsavel_DemandaCod=gx.fn.getIntegerValue("vLOGRESPONSAVEL_DEMANDACOD",'.') ;
      this.AV57Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV30TFLogResponsavel_Acao_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_ACAO_SELS") ;
      this.AV34TFLogResponsavel_Status_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_STATUS_SELS") ;
      this.AV38TFLogResponsavel_NovoStatus_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_NOVOSTATUS_SELS") ;
      this.A1Usuario_Codigo=gx.fn.getIntegerValue("USUARIO_CODIGO",'.') ;
      this.A58Usuario_PessoaNom=gx.fn.getControlValue("USUARIO_PESSOANOM") ;
      this.A896LogResponsavel_Owner=gx.fn.getIntegerValue("LOGRESPONSAVEL_OWNER",'.') ;
      this.A891LogResponsavel_UsuarioCod=gx.fn.getIntegerValue("LOGRESPONSAVEL_USUARIOCOD",'.') ;
      this.A493ContagemResultado_DemandaFM=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDAFM") ;
      this.AV52UltimaAcao=gx.fn.getIntegerValue("vULTIMAACAO",'.') ;
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV54String=gx.fn.getControlValue("vSTRING") ;
      this.A1797LogResponsavel_Codigo=gx.fn.getIntegerValue("LOGRESPONSAVEL_CODIGO",'.') ;
      this.A1228ContratadaUsuario_AreaTrabalhoCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_AREATRABALHOCOD",'.') ;
      this.A69ContratadaUsuario_UsuarioCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_USUARIOCOD",'.') ;
      this.A2080AreaTrabalho_SelUsrPrestadora=gx.fn.getControlValue("AREATRABALHO_SELUSRPRESTADORA") ;
      this.A66ContratadaUsuario_ContratadaCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_CONTRATADACOD",'.') ;
      this.A438Contratada_Sigla=gx.fn.getControlValue("CONTRATADA_SIGLA") ;
      this.AV51OS=gx.fn.getControlValue("vOS") ;
   };
   this.Valid_Logresponsavel_demandacod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("LOGRESPONSAVEL_DEMANDACOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_datahora=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_DATAHORA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV23TFLogResponsavel_DataHora)==0) || new gx.date.gxdate( this.AV23TFLogResponsavel_DataHora ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Data Hora fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_datahora_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_DATAHORA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV24TFLogResponsavel_DataHora_To)==0) || new gx.date.gxdate( this.AV24TFLogResponsavel_DataHora_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Data Hora_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_datahoraauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV25DDO_LogResponsavel_DataHoraAuxDate)==0) || new gx.date.gxdate( this.AV25DDO_LogResponsavel_DataHoraAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Data Hora Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_datahoraauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV26DDO_LogResponsavel_DataHoraAuxDateTo)==0) || new gx.date.gxdate( this.AV26DDO_LogResponsavel_DataHoraAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Data Hora Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_prazo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_PRAZO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV41TFLogResponsavel_Prazo)==0) || new gx.date.gxdate( this.AV41TFLogResponsavel_Prazo ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Prazo fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_prazo_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_PRAZO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV42TFLogResponsavel_Prazo_To)==0) || new gx.date.gxdate( this.AV42TFLogResponsavel_Prazo_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Prazo_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_prazoauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV43DDO_LogResponsavel_PrazoAuxDate)==0) || new gx.date.gxdate( this.AV43DDO_LogResponsavel_PrazoAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Prazo Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_prazoauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV44DDO_LogResponsavel_PrazoAuxDateTo)==0) || new gx.date.gxdate( this.AV44DDO_LogResponsavel_PrazoAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Prazo Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e11fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e12fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16fy2_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_OBSERVACAO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17fy2_client=function()
   {
      this.executeServerEvent("'DOPRAZOS'", false, null, false, false);
   };
   this.e18fy2_client=function()
   {
      this.executeServerEvent("'DOCANCELARACAO'", true, null, false, false);
   };
   this.e22fy2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e23fy2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,10,11,12,13,14,15,16,17,19,22,23,24,25,26,27,28,29,30,31,32,33,34,35,37,39,41,43,45,47];
   this.GXLastCtrlId =47;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",9,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wclogresponsavel",[],false,1,false,true,0,true,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(893,10,"LOGRESPONSAVEL_DATAHORA","","","LogResponsavel_DataHora","dtime",0,"px",14,14,"right",null,[],893,"LogResponsavel_DataHora",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addComboBox("Emissor",11,"vEMISSOR","Emissor","Emissor","int",null,0,true,false,0,"px","");
   GridContainer.addComboBox(894,12,"LOGRESPONSAVEL_ACAO","","LogResponsavel_Acao","char",null,0,true,false,0,"px","");
   GridContainer.addComboBox("Destino",13,"vDESTINO","Destino","Destino","int",null,0,true,false,0,"px","");
   GridContainer.addComboBox(1130,14,"LOGRESPONSAVEL_STATUS","","LogResponsavel_Status","char",null,0,true,false,0,"px","");
   GridContainer.addComboBox(1234,15,"LOGRESPONSAVEL_NOVOSTATUS","","LogResponsavel_NovoStatus","char",null,0,true,false,0,"px","");
   GridContainer.addSingleLineEdit(1177,16,"LOGRESPONSAVEL_PRAZO","","","LogResponsavel_Prazo","dtime",0,"px",14,14,"right",null,[],1177,"LogResponsavel_Prazo",true,5,false,false,"BootstrapAttributeNoWrap",1,"");
   GridContainer.addSingleLineEdit(1131,17,"LOGRESPONSAVEL_OBSERVACAO","","","LogResponsavel_Observacao","vchar",0,"px",2097152,80,"left",null,[],1131,"LogResponsavel_Observacao",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.DDO_LOGRESPONSAVEL_DATAHORAContainer = gx.uc.getNew(this, 36, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_DATAHORAContainer", "Ddo_logresponsavel_datahora");
   var DDO_LOGRESPONSAVEL_DATAHORAContainer = this.DDO_LOGRESPONSAVEL_DATAHORAContainer;
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addV2CFunction('AV22LogResponsavel_DataHoraTitleFilterData', "vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV22LogResponsavel_DataHoraTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA",UC.ParentObject.AV22LogResponsavel_DataHoraTitleFilterData); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addEventHandler("OnOptionClicked", this.e11fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_DATAHORAContainer);
   this.DDO_LOGRESPONSAVEL_ACAOContainer = gx.uc.getNew(this, 38, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_ACAOContainer", "Ddo_logresponsavel_acao");
   var DDO_LOGRESPONSAVEL_ACAOContainer = this.DDO_LOGRESPONSAVEL_ACAOContainer;
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "A:Atribui,B:StandBy,C:Captura,R:Rejeita,L:Libera,E:Encaminha,I:Importa,S:Solicita,D:Divergência,V:Resolvida,H:Homologa,Q:Liquida,P:Fatura,O:Aceite,N:Não Acata,M:Automática,F:Cumprido,T:Acata,X:Cancela,NO:Nota,U:Altera,UN:Rascunho,EA:Em Análise,RN:Reunião,BK:Desfaz,RE:Reinicio,PR:Prioridade", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_ACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_ACAOContainer.addV2CFunction('AV28LogResponsavel_AcaoTitleFilterData', "vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_ACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV28LogResponsavel_AcaoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_ACAOTITLEFILTERDATA",UC.ParentObject.AV28LogResponsavel_AcaoTitleFilterData); });
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_ACAOContainer.addEventHandler("OnOptionClicked", this.e12fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_ACAOContainer);
   this.DDO_LOGRESPONSAVEL_STATUSContainer = gx.uc.getNew(this, 40, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_STATUSContainer", "Ddo_logresponsavel_status");
   var DDO_LOGRESPONSAVEL_STATUSContainer = this.DDO_LOGRESPONSAVEL_STATUSContainer;
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "B:Stand by,S:Solicitada,E:Em Análise,A:Em execução,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:Não Faturada,J:Planejamento,I:Análise Planejamento,T:Validacao Técnica,Q:Validacao Qualidade,G:Em Homologação,M:Validação Mensuração,U:Rascunho", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_STATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_STATUSContainer.addV2CFunction('AV32LogResponsavel_StatusTitleFilterData', "vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_STATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV32LogResponsavel_StatusTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_STATUSTITLEFILTERDATA",UC.ParentObject.AV32LogResponsavel_StatusTitleFilterData); });
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_STATUSContainer.addEventHandler("OnOptionClicked", this.e13fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_STATUSContainer);
   this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer = gx.uc.getNew(this, 42, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_NOVOSTATUSContainer", "Ddo_logresponsavel_novostatus");
   var DDO_LOGRESPONSAVEL_NOVOSTATUSContainer = this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer;
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "B:Stand by,S:Solicitada,E:Em Análise,A:Em execução,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:Não Faturada,J:Planejamento,I:Análise Planejamento,T:Validacao Técnica,Q:Validacao Qualidade,G:Em Homologação,M:Validação Mensuração,U:Rascunho", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addV2CFunction('AV36LogResponsavel_NovoStatusTitleFilterData', "vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV36LogResponsavel_NovoStatusTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA",UC.ParentObject.AV36LogResponsavel_NovoStatusTitleFilterData); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addEventHandler("OnOptionClicked", this.e14fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_NOVOSTATUSContainer);
   this.DDO_LOGRESPONSAVEL_PRAZOContainer = gx.uc.getNew(this, 44, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_PRAZOContainer", "Ddo_logresponsavel_prazo");
   var DDO_LOGRESPONSAVEL_PRAZOContainer = this.DDO_LOGRESPONSAVEL_PRAZOContainer;
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_PRAZOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.addV2CFunction('AV40LogResponsavel_PrazoTitleFilterData', "vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_PRAZOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV40LogResponsavel_PrazoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA",UC.ParentObject.AV40LogResponsavel_PrazoTitleFilterData); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.addEventHandler("OnOptionClicked", this.e15fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_PRAZOContainer);
   this.DDO_LOGRESPONSAVEL_OBSERVACAOContainer = gx.uc.getNew(this, 46, 23, "BootstrapDropDownOptions", this.CmpContext + "DDO_LOGRESPONSAVEL_OBSERVACAOContainer", "Ddo_logresponsavel_observacao");
   var DDO_LOGRESPONSAVEL_OBSERVACAOContainer = this.DDO_LOGRESPONSAVEL_OBSERVACAOContainer;
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("DataListProc", "Datalistproc", "GetWCLogResponsavelFilterData", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.addV2CFunction('AV50DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV50DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV50DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.addV2CFunction('AV46LogResponsavel_ObservacaoTitleFilterData', "vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV46LogResponsavel_ObservacaoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA",UC.ParentObject.AV46LogResponsavel_ObservacaoTitleFilterData); });
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_OBSERVACAOContainer.addEventHandler("OnOptionClicked", this.e16fy2_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_OBSERVACAOContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[10]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_DATAHORA",gxz:"Z893LogResponsavel_DataHora",gxold:"O893LogResponsavel_DataHora",gxvar:"A893LogResponsavel_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A893LogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z893LogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_DATAHORA",row || gx.fn.currentGridRowImpl(9),gx.O.A893LogResponsavel_DataHora,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A893LogResponsavel_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("LOGRESPONSAVEL_DATAHORA",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[11]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vEMISSOR",gxz:"ZV16Emissor",gxold:"OV16Emissor",gxvar:"AV16Emissor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV16Emissor=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16Emissor=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("vEMISSOR",row || gx.fn.currentGridRowImpl(9),gx.O.AV16Emissor)},c2v:function(){if(this.val()!==undefined)gx.O.AV16Emissor=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vEMISSOR",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[12]={lvl:2,type:"char",len:20,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_ACAO",gxz:"Z894LogResponsavel_Acao",gxold:"O894LogResponsavel_Acao",gxvar:"A894LogResponsavel_Acao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A894LogResponsavel_Acao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z894LogResponsavel_Acao=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_ACAO",row || gx.fn.currentGridRowImpl(9),gx.O.A894LogResponsavel_Acao);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A894LogResponsavel_Acao=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_ACAO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[13]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDESTINO",gxz:"ZV17Destino",gxold:"OV17Destino",gxvar:"AV17Destino",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV17Destino=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Destino=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("vDESTINO",row || gx.fn.currentGridRowImpl(9),gx.O.AV17Destino)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Destino=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vDESTINO",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[14]={lvl:2,type:"char",len:1,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_STATUS",gxz:"Z1130LogResponsavel_Status",gxold:"O1130LogResponsavel_Status",gxvar:"A1130LogResponsavel_Status",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1130LogResponsavel_Status=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1130LogResponsavel_Status=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_STATUS",row || gx.fn.currentGridRowImpl(9),gx.O.A1130LogResponsavel_Status);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1130LogResponsavel_Status=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_STATUS",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[15]={lvl:2,type:"char",len:1,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_NOVOSTATUS",gxz:"Z1234LogResponsavel_NovoStatus",gxold:"O1234LogResponsavel_NovoStatus",gxvar:"A1234LogResponsavel_NovoStatus",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1234LogResponsavel_NovoStatus=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1234LogResponsavel_NovoStatus=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_NOVOSTATUS",row || gx.fn.currentGridRowImpl(9),gx.O.A1234LogResponsavel_NovoStatus);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1234LogResponsavel_NovoStatus=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_NOVOSTATUS",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_PRAZO",gxz:"Z1177LogResponsavel_Prazo",gxold:"O1177LogResponsavel_Prazo",gxvar:"A1177LogResponsavel_Prazo",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_PRAZO",row || gx.fn.currentGridRowImpl(9),gx.O.A1177LogResponsavel_Prazo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("LOGRESPONSAVEL_PRAZO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[17]={lvl:2,type:"vchar",len:2097152,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_OBSERVACAO",gxz:"Z1131LogResponsavel_Observacao",gxold:"O1131LogResponsavel_Observacao",gxvar:"A1131LogResponsavel_Observacao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A1131LogResponsavel_Observacao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1131LogResponsavel_Observacao=Value},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_OBSERVACAO",row || gx.fn.currentGridRowImpl(9),gx.O.A1131LogResponsavel_Observacao,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1131LogResponsavel_Observacao=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_OBSERVACAO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[19]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[22]={fld:"CANCELARACAO",grid:0};
   GXValidFnc[23]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Logresponsavel_demandacod,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_DEMANDACOD",gxz:"Z892LogResponsavel_DemandaCod",gxold:"O892LogResponsavel_DemandaCod",gxvar:"A892LogResponsavel_DemandaCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("LOGRESPONSAVEL_DEMANDACOD",gx.O.A892LogResponsavel_DemandaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("LOGRESPONSAVEL_DEMANDACOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 23 , function() {
   });
   GXValidFnc[24]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_datahora,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_DATAHORA",gxz:"ZV23TFLogResponsavel_DataHora",gxold:"OV23TFLogResponsavel_DataHora",gxvar:"AV23TFLogResponsavel_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[24],ip:[24],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_DATAHORA",gx.O.AV23TFLogResponsavel_DataHora,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_DATAHORA")},nac:gx.falseFn};
   GXValidFnc[25]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_datahora_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_DATAHORA_TO",gxz:"ZV24TFLogResponsavel_DataHora_To",gxold:"OV24TFLogResponsavel_DataHora_To",gxvar:"AV24TFLogResponsavel_DataHora_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[25],ip:[25],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_DATAHORA_TO",gx.O.AV24TFLogResponsavel_DataHora_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_DATAHORA_TO")},nac:gx.falseFn};
   GXValidFnc[26]={fld:"DDO_LOGRESPONSAVEL_DATAHORAAUXDATES",grid:0};
   GXValidFnc[27]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_datahoraauxdate,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE",gxz:"ZV25DDO_LogResponsavel_DataHoraAuxDate",gxold:"OV25DDO_LogResponsavel_DataHoraAuxDate",gxvar:"AV25DDO_LogResponsavel_DataHoraAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[27],ip:[27],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE",gx.O.AV25DDO_LogResponsavel_DataHoraAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[28]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_datahoraauxdateto,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO",gxz:"ZV26DDO_LogResponsavel_DataHoraAuxDateTo",gxold:"OV26DDO_LogResponsavel_DataHoraAuxDateTo",gxvar:"AV26DDO_LogResponsavel_DataHoraAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[28],ip:[28],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV26DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV26DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO",gx.O.AV26DDO_LogResponsavel_DataHoraAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV26DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[29]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_prazo,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_PRAZO",gxz:"ZV41TFLogResponsavel_Prazo",gxold:"OV41TFLogResponsavel_Prazo",gxvar:"AV41TFLogResponsavel_Prazo",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[29],ip:[29],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV41TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV41TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_PRAZO",gx.O.AV41TFLogResponsavel_Prazo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV41TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_PRAZO")},nac:gx.falseFn};
   GXValidFnc[30]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_prazo_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_PRAZO_TO",gxz:"ZV42TFLogResponsavel_Prazo_To",gxold:"OV42TFLogResponsavel_Prazo_To",gxvar:"AV42TFLogResponsavel_Prazo_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[30],ip:[30],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV42TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_PRAZO_TO",gx.O.AV42TFLogResponsavel_Prazo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_PRAZO_TO")},nac:gx.falseFn};
   GXValidFnc[31]={fld:"DDO_LOGRESPONSAVEL_PRAZOAUXDATES",grid:0};
   GXValidFnc[32]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_prazoauxdate,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOAUXDATE",gxz:"ZV43DDO_LogResponsavel_PrazoAuxDate",gxold:"OV43DDO_LogResponsavel_PrazoAuxDate",gxvar:"AV43DDO_LogResponsavel_PrazoAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[32],ip:[32],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV43DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE",gx.O.AV43DDO_LogResponsavel_PrazoAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[33]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_prazoauxdateto,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO",gxz:"ZV44DDO_LogResponsavel_PrazoAuxDateTo",gxold:"OV44DDO_LogResponsavel_PrazoAuxDateTo",gxvar:"AV44DDO_LogResponsavel_PrazoAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[33],ip:[33],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV44DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO",gx.O.AV44DDO_LogResponsavel_PrazoAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[34]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_OBSERVACAO",gxz:"ZV47TFLogResponsavel_Observacao",gxold:"OV47TFLogResponsavel_Observacao",gxvar:"AV47TFLogResponsavel_Observacao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV47TFLogResponsavel_Observacao=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV47TFLogResponsavel_Observacao=Value},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_OBSERVACAO",gx.O.AV47TFLogResponsavel_Observacao,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV47TFLogResponsavel_Observacao=this.val()},val:function(){return gx.fn.getControlValue("vTFLOGRESPONSAVEL_OBSERVACAO")},nac:gx.falseFn};
   GXValidFnc[35]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFLOGRESPONSAVEL_OBSERVACAO_SEL",gxz:"ZV48TFLogResponsavel_Observacao_Sel",gxold:"OV48TFLogResponsavel_Observacao_Sel",gxvar:"AV48TFLogResponsavel_Observacao_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV48TFLogResponsavel_Observacao_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV48TFLogResponsavel_Observacao_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_OBSERVACAO_SEL",gx.O.AV48TFLogResponsavel_Observacao_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV48TFLogResponsavel_Observacao_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFLOGRESPONSAVEL_OBSERVACAO_SEL")},nac:gx.falseFn};
   GXValidFnc[37]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE",gxz:"ZV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace",gxold:"OV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace",gxvar:"AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE",gx.O.AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[39]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE",gxz:"ZV31ddo_LogResponsavel_AcaoTitleControlIdToReplace",gxold:"OV31ddo_LogResponsavel_AcaoTitleControlIdToReplace",gxvar:"AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31ddo_LogResponsavel_AcaoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE",gx.O.AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[41]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE",gxz:"ZV35ddo_LogResponsavel_StatusTitleControlIdToReplace",gxold:"OV35ddo_LogResponsavel_StatusTitleControlIdToReplace",gxvar:"AV35ddo_LogResponsavel_StatusTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35ddo_LogResponsavel_StatusTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV35ddo_LogResponsavel_StatusTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE",gx.O.AV35ddo_LogResponsavel_StatusTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35ddo_LogResponsavel_StatusTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[43]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE",gxz:"ZV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",gxold:"OV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",gxvar:"AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE",gx.O.AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[45]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE",gxz:"ZV45ddo_LogResponsavel_PrazoTitleControlIdToReplace",gxold:"OV45ddo_LogResponsavel_PrazoTitleControlIdToReplace",gxvar:"AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV45ddo_LogResponsavel_PrazoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE",gx.O.AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[47]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE",gxz:"ZV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace",gxold:"OV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace",gxvar:"AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE",gx.O.AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.Z893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.O893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.ZV16Emissor = 0 ;
   this.OV16Emissor = 0 ;
   this.Z894LogResponsavel_Acao = "" ;
   this.O894LogResponsavel_Acao = "" ;
   this.ZV17Destino = 0 ;
   this.OV17Destino = 0 ;
   this.Z1130LogResponsavel_Status = "" ;
   this.O1130LogResponsavel_Status = "" ;
   this.Z1234LogResponsavel_NovoStatus = "" ;
   this.O1234LogResponsavel_NovoStatus = "" ;
   this.Z1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.O1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.Z1131LogResponsavel_Observacao = "" ;
   this.O1131LogResponsavel_Observacao = "" ;
   this.A892LogResponsavel_DemandaCod = 0 ;
   this.Z892LogResponsavel_DemandaCod = 0 ;
   this.O892LogResponsavel_DemandaCod = 0 ;
   this.AV23TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.ZV23TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.OV23TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV24TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.ZV24TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.OV24TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.AV25DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.ZV25DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.OV25DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV26DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.ZV26DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.OV26DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV41TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.ZV41TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.OV41TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.AV42TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.ZV42TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.OV42TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.AV43DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.ZV43DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.OV43DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.AV44DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.ZV44DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.OV44DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.AV47TFLogResponsavel_Observacao = "" ;
   this.ZV47TFLogResponsavel_Observacao = "" ;
   this.OV47TFLogResponsavel_Observacao = "" ;
   this.AV48TFLogResponsavel_Observacao_Sel = "" ;
   this.ZV48TFLogResponsavel_Observacao_Sel = "" ;
   this.OV48TFLogResponsavel_Observacao_Sel = "" ;
   this.AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.ZV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.OV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.ZV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.OV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.ZV35ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.OV35ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.ZV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.OV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.ZV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.OV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = "" ;
   this.ZV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = "" ;
   this.OV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = "" ;
   this.A892LogResponsavel_DemandaCod = 0 ;
   this.AV23TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV24TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.AV25DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV26DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV41TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.AV42TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.AV43DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.AV44DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.AV47TFLogResponsavel_Observacao = "" ;
   this.AV48TFLogResponsavel_Observacao_Sel = "" ;
   this.AV50DDO_TitleSettingsIcons = {} ;
   this.AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.AV35ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace = "" ;
   this.AV7LogResponsavel_DemandaCod = 0 ;
   this.A896LogResponsavel_Owner = 0 ;
   this.A891LogResponsavel_UsuarioCod = 0 ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A1797LogResponsavel_Codigo = 0 ;
   this.A893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV16Emissor = 0 ;
   this.A894LogResponsavel_Acao = "" ;
   this.AV17Destino = 0 ;
   this.A1130LogResponsavel_Status = "" ;
   this.A1234LogResponsavel_NovoStatus = "" ;
   this.A1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.A1131LogResponsavel_Observacao = "" ;
   this.A58Usuario_PessoaNom = "" ;
   this.A1Usuario_Codigo = 0 ;
   this.A57Usuario_PessoaCod = 0 ;
   this.A69ContratadaUsuario_UsuarioCod = 0 ;
   this.A1228ContratadaUsuario_AreaTrabalhoCod = 0 ;
   this.A66ContratadaUsuario_ContratadaCod = 0 ;
   this.A2080AreaTrabalho_SelUsrPrestadora = false ;
   this.A438Contratada_Sigla = "" ;
   this.AV57Pgmname = "" ;
   this.AV30TFLogResponsavel_Acao_Sels = [ ] ;
   this.AV34TFLogResponsavel_Status_Sels = [ ] ;
   this.AV38TFLogResponsavel_NovoStatus_Sels = [ ] ;
   this.AV52UltimaAcao = 0 ;
   this.AV6WWPContext = {} ;
   this.AV54String = "" ;
   this.AV51OS = "" ;
   this.Events = {"e11fy2_client": ["DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED", true] ,"e12fy2_client": ["DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED", true] ,"e13fy2_client": ["DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED", true] ,"e14fy2_client": ["DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED", true] ,"e15fy2_client": ["DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED", true] ,"e16fy2_client": ["DDO_LOGRESPONSAVEL_OBSERVACAO.ONOPTIONCLICKED", true] ,"e17fy2_client": ["'DOPRAZOS'", true] ,"e18fy2_client": ["'DOCANCELARACAO'", true] ,"e22fy2_client": ["ENTER", true] ,"e23fy2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_OBSERVACAO","Title")',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{ctrl:'GRID',prop:'Cellpadding'},{ctrl:'GRID',prop:'Cellspacing'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format',hsh:true},{av:'gx.fn.getCtrlProperty("UNNAMEDTABLE2","Height")',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.FilteredText_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredText_get'},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.FilteredTextTo_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredTextTo_get'}],[{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_ACAOContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_ACAOContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'SelectedValue_get'}],[{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_STATUSContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_STATUSContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'SelectedValue_get'}],[{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'SelectedValue_get'}],[{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.FilteredText_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredText_get'},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.FilteredTextTo_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredTextTo_get'}],[{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_OBSERVACAO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_LOGRESPONSAVEL_OBSERVACAOContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_OBSERVACAOContainer.FilteredText_get',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'FilteredText_get'},{av:'this.DDO_LOGRESPONSAVEL_OBSERVACAOContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_OBSERVACAO',prop:'SelectedValue_get'}],[{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''}],[{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'AV51OS',fld:'vOS',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["'DOPRAZOS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["'DOCANCELARACAO'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV51OS',fld:'vOS',pic:'',nv:''}],[]];
   this.EvtParms["GRID_FIRSTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_OBSERVACAO","Title")',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{ctrl:'GRID',prop:'Cellpadding'},{ctrl:'GRID',prop:'Cellspacing'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format',hsh:true},{av:'gx.fn.getCtrlProperty("UNNAMEDTABLE2","Height")',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_PREVPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_OBSERVACAO","Title")',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{ctrl:'GRID',prop:'Cellpadding'},{ctrl:'GRID',prop:'Cellspacing'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format',hsh:true},{av:'gx.fn.getCtrlProperty("UNNAMEDTABLE2","Height")',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_NEXTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_OBSERVACAO","Title")',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{ctrl:'GRID',prop:'Cellpadding'},{ctrl:'GRID',prop:'Cellspacing'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format',hsh:true},{av:'gx.fn.getCtrlProperty("UNNAMEDTABLE2","Height")',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_LASTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV52UltimaAcao',fld:'vULTIMAACAO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A2080AreaTrabalho_SelUsrPrestadora',fld:'AREATRABALHO_SELUSRPRESTADORA',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV27ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LogResponsavel_ObservacaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_OBSERVACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV23TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV24TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV30TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV34TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV41TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV42TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLogResponsavel_Observacao',fld:'vTFLOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'AV48TFLogResponsavel_Observacao_Sel',fld:'vTFLOGRESPONSAVEL_OBSERVACAO_SEL',pic:'',nv:''},{av:'AV7LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV22LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV28LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV36LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV40LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46LogResponsavel_ObservacaoTitleFilterData',fld:'vLOGRESPONSAVEL_OBSERVACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_OBSERVACAO","Title")',ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Title'},{av:'gx.fn.getCtrlProperty("CANCELARACAO","Visible")',ctrl:'CANCELARACAO',prop:'Visible'},{ctrl:'GRID',prop:'Cellpadding'},{ctrl:'GRID',prop:'Cellspacing'},{ctrl:'LOGRESPONSAVEL_OBSERVACAO',prop:'Format',hsh:true},{av:'gx.fn.getCtrlProperty("UNNAMEDTABLE2","Height")',ctrl:'UNNAMEDTABLE2',prop:'Height'},{av:'AV54String',fld:'vSTRING',pic:'',nv:''},{av:'AV16Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV17Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("AV7LogResponsavel_DemandaCod", "vLOGRESPONSAVEL_DEMANDACOD", 0, "int");
   this.setVCMap("AV57Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV30TFLogResponsavel_Acao_Sels", "vTFLOGRESPONSAVEL_ACAO_SELS", 0, "Collchar");
   this.setVCMap("AV34TFLogResponsavel_Status_Sels", "vTFLOGRESPONSAVEL_STATUS_SELS", 0, "Collchar");
   this.setVCMap("AV38TFLogResponsavel_NovoStatus_Sels", "vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", 0, "Collchar");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A896LogResponsavel_Owner", "LOGRESPONSAVEL_OWNER", 0, "int");
   this.setVCMap("A891LogResponsavel_UsuarioCod", "LOGRESPONSAVEL_USUARIOCOD", 0, "int");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV52UltimaAcao", "vULTIMAACAO", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV54String", "vSTRING", 0, "char");
   this.setVCMap("A1797LogResponsavel_Codigo", "LOGRESPONSAVEL_CODIGO", 0, "int");
   this.setVCMap("A1228ContratadaUsuario_AreaTrabalhoCod", "CONTRATADAUSUARIO_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A2080AreaTrabalho_SelUsrPrestadora", "AREATRABALHO_SELUSRPRESTADORA", 0, "boolean");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A438Contratada_Sigla", "CONTRATADA_SIGLA", 0, "char");
   this.setVCMap("AV51OS", "vOS", 0, "svchar");
   this.setVCMap("AV7LogResponsavel_DemandaCod", "vLOGRESPONSAVEL_DEMANDACOD", 0, "int");
   this.setVCMap("AV57Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV30TFLogResponsavel_Acao_Sels", "vTFLOGRESPONSAVEL_ACAO_SELS", 0, "Collchar");
   this.setVCMap("AV34TFLogResponsavel_Status_Sels", "vTFLOGRESPONSAVEL_STATUS_SELS", 0, "Collchar");
   this.setVCMap("AV38TFLogResponsavel_NovoStatus_Sels", "vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", 0, "Collchar");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A896LogResponsavel_Owner", "LOGRESPONSAVEL_OWNER", 0, "int");
   this.setVCMap("A891LogResponsavel_UsuarioCod", "LOGRESPONSAVEL_USUARIOCOD", 0, "int");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV52UltimaAcao", "vULTIMAACAO", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV54String", "vSTRING", 0, "char");
   this.setVCMap("A1797LogResponsavel_Codigo", "LOGRESPONSAVEL_CODIGO", 0, "int");
   this.setVCMap("A1228ContratadaUsuario_AreaTrabalhoCod", "CONTRATADAUSUARIO_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A2080AreaTrabalho_SelUsrPrestadora", "AREATRABALHO_SELUSRPRESTADORA", 0, "boolean");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A438Contratada_Sigla", "CONTRATADA_SIGLA", 0, "char");
   GridContainer.addRefreshingVar(this.GXValidFnc[24]);
   GridContainer.addRefreshingVar(this.GXValidFnc[25]);
   GridContainer.addRefreshingVar(this.GXValidFnc[29]);
   GridContainer.addRefreshingVar(this.GXValidFnc[30]);
   GridContainer.addRefreshingVar(this.GXValidFnc[34]);
   GridContainer.addRefreshingVar(this.GXValidFnc[35]);
   GridContainer.addRefreshingVar({rfrVar:"AV7LogResponsavel_DemandaCod"});
   GridContainer.addRefreshingVar(this.GXValidFnc[37]);
   GridContainer.addRefreshingVar(this.GXValidFnc[39]);
   GridContainer.addRefreshingVar(this.GXValidFnc[41]);
   GridContainer.addRefreshingVar(this.GXValidFnc[43]);
   GridContainer.addRefreshingVar(this.GXValidFnc[45]);
   GridContainer.addRefreshingVar(this.GXValidFnc[47]);
   GridContainer.addRefreshingVar({rfrVar:"AV57Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV30TFLogResponsavel_Acao_Sels"});
   GridContainer.addRefreshingVar({rfrVar:"AV34TFLogResponsavel_Status_Sels"});
   GridContainer.addRefreshingVar({rfrVar:"AV38TFLogResponsavel_NovoStatus_Sels"});
   GridContainer.addRefreshingVar({rfrVar:"A1Usuario_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A58Usuario_PessoaNom"});
   GridContainer.addRefreshingVar({rfrVar:"A896LogResponsavel_Owner"});
   GridContainer.addRefreshingVar({rfrVar:"A891LogResponsavel_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"A493ContagemResultado_DemandaFM"});
   GridContainer.addRefreshingVar({rfrVar:"AV52UltimaAcao"});
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"AV54String"});
   GridContainer.addRefreshingVar({rfrVar:"A894LogResponsavel_Acao", rfrProp:"Value", gxAttId:"894"});
   GridContainer.addRefreshingVar({rfrVar:"A1797LogResponsavel_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A1228ContratadaUsuario_AreaTrabalhoCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV17Destino", rfrProp:"Value", gxAttId:"Destino"});
   GridContainer.addRefreshingVar({rfrVar:"A69ContratadaUsuario_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV16Emissor", rfrProp:"Value", gxAttId:"Emissor"});
   GridContainer.addRefreshingVar({rfrVar:"A2080AreaTrabalho_SelUsrPrestadora"});
   GridContainer.addRefreshingVar({rfrVar:"A66ContratadaUsuario_ContratadaCod"});
   GridContainer.addRefreshingVar({rfrVar:"A438Contratada_Sigla"});
   this.InitStandaloneVars( );
});
