/*
               File: ServicoProcessoServico
        Description: Servico Processo Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:48.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoprocessoservico : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoprocessoservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoprocessoservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbServico_UORespExclusiva = new GXCombobox();
         cmbServico_Terceriza = new GXCombobox();
         cmbServico_Tela = new GXCombobox();
         cmbServico_Atende = new GXCombobox();
         cmbServico_ObrigaValores = new GXCombobox();
         cmbServico_ObjetoControle = new GXCombobox();
         cmbServico_TipoHierarquia = new GXCombobox();
         cmbServico_Ativo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A155Servico_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PALO2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ServicoProcessoServico";
               context.Gx_err = 0;
               GXt_int1 = A640Servico_Vinculados;
               new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A640Servico_Vinculados = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
               GXt_int2 = A1551Servico_Responsavel;
               new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1551Servico_Responsavel = GXt_int2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
               WSLO2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Processo Servico") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117174843");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoprocessoservico.aspx") + "?" + UrlEncode("" +A155Servico_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VINCULADOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ServicoProcessoServico";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicoprocessoservico:[SendSecurityCheck value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormLO2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoprocessoservico.js", "?20203117174852");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoProcessoServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Processo Servico" ;
      }

      protected void WBLO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoprocessoservico.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_LO2( true) ;
         }
         else
         {
            wb_table1_2_LO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoGrupo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoGrupo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoGrupo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Vinculado_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Vinculado_Jsonclick, 0, "Attribute", "", "", "", edtServico_Vinculado_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
         }
         wbLoad = true;
      }

      protected void STARTLO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Processo Servico", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPLO0( ) ;
            }
         }
      }

      protected void WSLO2( )
      {
         STARTLO2( ) ;
         EVTLO2( ) ;
      }

      protected void EVTLO2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11LO2 */
                                    E11LO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12LO2 */
                                    E12LO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13LO2 */
                                    E13LO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14LO2 */
                                    E14LO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormLO2( ) ;
            }
         }
      }

      protected void PALO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbServico_UORespExclusiva.Name = "SERVICO_UORESPEXCLUSIVA";
            cmbServico_UORespExclusiva.WebTags = "";
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( false), "Herdada pelas sub-unidades", 0);
            cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( true), "Exclusiva", 0);
            if ( cmbServico_UORespExclusiva.ItemCount > 0 )
            {
               A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
               n1077Servico_UORespExclusiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
            }
            cmbServico_Terceriza.Name = "SERVICO_TERCERIZA";
            cmbServico_Terceriza.WebTags = "";
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "Desconhecido", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbServico_Terceriza.ItemCount > 0 )
            {
               A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
               n889Servico_Terceriza = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
            }
            cmbServico_Tela.Name = "SERVICO_TELA";
            cmbServico_Tela.WebTags = "";
            cmbServico_Tela.addItem("", "(Nenhuma)", 0);
            cmbServico_Tela.addItem("CNT", "Contagem", 0);
            cmbServico_Tela.addItem("CHK", "Check List", 0);
            if ( cmbServico_Tela.ItemCount > 0 )
            {
               A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
               n1061Servico_Tela = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
            }
            cmbServico_Atende.Name = "SERVICO_ATENDE";
            cmbServico_Atende.WebTags = "";
            cmbServico_Atende.addItem("S", "Software", 0);
            cmbServico_Atende.addItem("H", "Hardware", 0);
            if ( cmbServico_Atende.ItemCount > 0 )
            {
               A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
               n1072Servico_Atende = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
            }
            cmbServico_ObrigaValores.Name = "SERVICO_OBRIGAVALORES";
            cmbServico_ObrigaValores.WebTags = "";
            cmbServico_ObrigaValores.addItem("", "Nenhum", 0);
            cmbServico_ObrigaValores.addItem("L", "Valor L�quido", 0);
            cmbServico_ObrigaValores.addItem("B", "Valor Bruto", 0);
            cmbServico_ObrigaValores.addItem("A", "Ambos", 0);
            if ( cmbServico_ObrigaValores.ItemCount > 0 )
            {
               A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
               n1429Servico_ObrigaValores = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
            }
            cmbServico_ObjetoControle.Name = "SERVICO_OBJETOCONTROLE";
            cmbServico_ObjetoControle.WebTags = "";
            cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
            cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
            cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
            if ( cmbServico_ObjetoControle.ItemCount > 0 )
            {
               A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            }
            cmbServico_TipoHierarquia.Name = "SERVICO_TIPOHIERARQUIA";
            cmbServico_TipoHierarquia.WebTags = "";
            cmbServico_TipoHierarquia.addItem("1", "Prim�rio", 0);
            cmbServico_TipoHierarquia.addItem("2", "Secund�rio", 0);
            if ( cmbServico_TipoHierarquia.ItemCount > 0 )
            {
               A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbServico_TipoHierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0))), "."));
               n1530Servico_TipoHierarquia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
            }
            cmbServico_Ativo.Name = "SERVICO_ATIVO";
            cmbServico_Ativo.WebTags = "";
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbServico_Ativo.ItemCount > 0 )
            {
               A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbServico_UORespExclusiva.ItemCount > 0 )
         {
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
         }
         if ( cmbServico_Terceriza.ItemCount > 0 )
         {
            A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
         }
         if ( cmbServico_Tela.ItemCount > 0 )
         {
            A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
         }
         if ( cmbServico_Atende.ItemCount > 0 )
         {
            A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
         }
         if ( cmbServico_ObrigaValores.ItemCount > 0 )
         {
            A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
         }
         if ( cmbServico_ObjetoControle.ItemCount > 0 )
         {
            A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
         }
         if ( cmbServico_TipoHierarquia.ItemCount > 0 )
         {
            A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cmbServico_TipoHierarquia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0))), "."));
            n1530Servico_TipoHierarquia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
         }
         if ( cmbServico_Ativo.ItemCount > 0 )
         {
            A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ServicoProcessoServico";
         context.Gx_err = 0;
      }

      protected void RFLO2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00LO2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A631Servico_Vinculado = H00LO2_A631Servico_Vinculado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
               n631Servico_Vinculado = H00LO2_n631Servico_Vinculado[0];
               A157ServicoGrupo_Codigo = H00LO2_A157ServicoGrupo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
               A632Servico_Ativo = H00LO2_A632Servico_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
               A1546Servico_Posterior = H00LO2_A1546Servico_Posterior[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
               n1546Servico_Posterior = H00LO2_n1546Servico_Posterior[0];
               A1545Servico_Anterior = H00LO2_A1545Servico_Anterior[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
               n1545Servico_Anterior = H00LO2_n1545Servico_Anterior[0];
               A1536Servico_PercCnc = H00LO2_A1536Servico_PercCnc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
               n1536Servico_PercCnc = H00LO2_n1536Servico_PercCnc[0];
               A1535Servico_PercPgm = H00LO2_A1535Servico_PercPgm[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
               n1535Servico_PercPgm = H00LO2_n1535Servico_PercPgm[0];
               A1534Servico_PercTmp = H00LO2_A1534Servico_PercTmp[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
               n1534Servico_PercTmp = H00LO2_n1534Servico_PercTmp[0];
               A1530Servico_TipoHierarquia = H00LO2_A1530Servico_TipoHierarquia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
               n1530Servico_TipoHierarquia = H00LO2_n1530Servico_TipoHierarquia[0];
               A1436Servico_ObjetoControle = H00LO2_A1436Servico_ObjetoControle[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
               A1429Servico_ObrigaValores = H00LO2_A1429Servico_ObrigaValores[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
               n1429Servico_ObrigaValores = H00LO2_n1429Servico_ObrigaValores[0];
               A1072Servico_Atende = H00LO2_A1072Servico_Atende[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
               n1072Servico_Atende = H00LO2_n1072Servico_Atende[0];
               A1061Servico_Tela = H00LO2_A1061Servico_Tela[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
               n1061Servico_Tela = H00LO2_n1061Servico_Tela[0];
               A889Servico_Terceriza = H00LO2_A889Servico_Terceriza[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
               n889Servico_Terceriza = H00LO2_n889Servico_Terceriza[0];
               A641Servico_VincDesc = H00LO2_A641Servico_VincDesc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A641Servico_VincDesc", A641Servico_VincDesc);
               n641Servico_VincDesc = H00LO2_n641Servico_VincDesc[0];
               A1077Servico_UORespExclusiva = H00LO2_A1077Servico_UORespExclusiva[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
               n1077Servico_UORespExclusiva = H00LO2_n1077Servico_UORespExclusiva[0];
               A633Servico_UO = H00LO2_A633Servico_UO[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
               n633Servico_UO = H00LO2_n633Servico_UO[0];
               A605Servico_Sigla = H00LO2_A605Servico_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A605Servico_Sigla", A605Servico_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
               A156Servico_Descricao = H00LO2_A156Servico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A156Servico_Descricao", A156Servico_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
               n156Servico_Descricao = H00LO2_n156Servico_Descricao[0];
               A608Servico_Nome = H00LO2_A608Servico_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
               A641Servico_VincDesc = H00LO2_A641Servico_VincDesc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A641Servico_VincDesc", A641Servico_VincDesc);
               n641Servico_VincDesc = H00LO2_n641Servico_VincDesc[0];
               /* Execute user event: E12LO2 */
               E12LO2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBLO0( ) ;
         }
      }

      protected void STRUPLO0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ServicoProcessoServico";
         context.Gx_err = 0;
         GXt_int1 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11LO2 */
         E11LO2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A608Servico_Nome", A608Servico_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!"))));
            A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
            n156Servico_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A156Servico_Descricao", A156Servico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_DESCRICAO", GetSecureSignedToken( sPrefix, A156Servico_Descricao));
            A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A605Servico_Sigla", A605Servico_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!"))));
            A633Servico_UO = (int)(context.localUtil.CToN( cgiGet( edtServico_UO_Internalname), ",", "."));
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9")));
            cmbServico_UORespExclusiva.CurrentValue = cgiGet( cmbServico_UORespExclusiva_Internalname);
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cgiGet( cmbServico_UORespExclusiva_Internalname));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_UORESPEXCLUSIVA", GetSecureSignedToken( sPrefix, A1077Servico_UORespExclusiva));
            A640Servico_Vinculados = (short)(context.localUtil.CToN( cgiGet( edtServico_Vinculados_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9")));
            A641Servico_VincDesc = cgiGet( edtServico_VincDesc_Internalname);
            n641Servico_VincDesc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A641Servico_VincDesc", A641Servico_VincDesc);
            cmbServico_Terceriza.CurrentValue = cgiGet( cmbServico_Terceriza_Internalname);
            A889Servico_Terceriza = StringUtil.StrToBool( cgiGet( cmbServico_Terceriza_Internalname));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A889Servico_Terceriza", A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TERCERIZA", GetSecureSignedToken( sPrefix, A889Servico_Terceriza));
            cmbServico_Tela.CurrentValue = cgiGet( cmbServico_Tela_Internalname);
            A1061Servico_Tela = cgiGet( cmbServico_Tela_Internalname);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1061Servico_Tela", A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1061Servico_Tela, ""))));
            cmbServico_Atende.CurrentValue = cgiGet( cmbServico_Atende_Internalname);
            A1072Servico_Atende = cgiGet( cmbServico_Atende_Internalname);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1072Servico_Atende", A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATENDE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1072Servico_Atende, ""))));
            cmbServico_ObrigaValores.CurrentValue = cgiGet( cmbServico_ObrigaValores_Internalname);
            A1429Servico_ObrigaValores = cgiGet( cmbServico_ObrigaValores_Internalname);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBRIGAVALORES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1429Servico_ObrigaValores, ""))));
            cmbServico_ObjetoControle.CurrentValue = cgiGet( cmbServico_ObjetoControle_Internalname);
            A1436Servico_ObjetoControle = cgiGet( cmbServico_ObjetoControle_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_OBJETOCONTROLE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1436Servico_ObjetoControle, ""))));
            cmbServico_TipoHierarquia.CurrentValue = cgiGet( cmbServico_TipoHierarquia_Internalname);
            A1530Servico_TipoHierarquia = (short)(NumberUtil.Val( cgiGet( cmbServico_TipoHierarquia_Internalname), "."));
            n1530Servico_TipoHierarquia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_TIPOHIERARQUIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9")));
            A1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", "."));
            n1534Servico_PercTmp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCTMP", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")));
            A1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", "."));
            n1535Servico_PercPgm = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCPGM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")));
            A1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", "."));
            n1536Servico_PercCnc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_PERCCNC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")));
            A1545Servico_Anterior = (int)(context.localUtil.CToN( cgiGet( edtServico_Anterior_Internalname), ",", "."));
            n1545Servico_Anterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ANTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9")));
            A1546Servico_Posterior = (int)(context.localUtil.CToN( cgiGet( edtServico_Posterior_Internalname), ",", "."));
            n1546Servico_Posterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_POSTERIOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9")));
            A1551Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtServico_Responsavel_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9")));
            cmbServico_Ativo.CurrentValue = cgiGet( cmbServico_Ativo_Internalname);
            A632Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbServico_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A632Servico_Ativo", A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_ATIVO", GetSecureSignedToken( sPrefix, A632Servico_Ativo));
            A157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoGrupo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOGRUPO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A157ServicoGrupo_Codigo), "ZZZZZ9")));
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA155Servico_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ServicoProcessoServico";
            A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( edtServico_Vinculado_Internalname), ",", "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICO_VINCULADO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("servicoprocessoservico:[SecurityCheckFailed value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11LO2 */
         E11LO2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11LO2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12LO2( )
      {
         /* Load Routine */
         edtServico_Descricao_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Descricao_Internalname, "Link", edtServico_Descricao_Link);
         edtServico_VincDesc_Link = formatLink("viewservico.aspx") + "?" + UrlEncode("" +A631Servico_Vinculado) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_VincDesc_Internalname, "Link", edtServico_VincDesc_Link);
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         edtServicoGrupo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoGrupo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoGrupo_Codigo_Visible), 5, 0)));
         edtServico_Vinculado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Vinculado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Vinculado_Visible), 5, 0)));
      }

      protected void E13LO2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV12ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14LO2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A155Servico_Codigo) + "," + UrlEncode("" +AV12ServicoGrupo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Servico";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Servico_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_LO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableData", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_LO2( true) ;
         }
         else
         {
            wb_table2_5_LO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_LO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_108_LO2( true) ;
         }
         else
         {
            wb_table3_108_LO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_108_LO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LO2e( true) ;
         }
         else
         {
            wb_table1_2_LO2e( false) ;
         }
      }

      protected void wb_table3_108_LO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_108_LO2e( true) ;
         }
         else
         {
            wb_table3_108_LO2e( false) ;
         }
      }

      protected void wb_table2_5_LO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table100x100", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Nome_Internalname, StringUtil.RTrim( A608Servico_Nome), StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_descricao_Internalname, "Descri��o de Servi�o", "", "", lblTextblockservico_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_Descricao_Internalname, A156Servico_Descricao, edtServico_Descricao_Link, "", 0, 1, 0, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_sigla_Internalname, "Sigla", "", "", lblTextblockservico_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Sigla_Internalname, StringUtil.RTrim( A605Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uo_Internalname, "Unidade Organizacional", "", "", lblTextblockservico_uo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_UO_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A633Servico_UO), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_UO_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uorespexclusiva_Internalname, "Responsabilidade", "", "", lblTextblockservico_uorespexclusiva_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_UORespExclusiva, cmbServico_UORespExclusiva_Internalname, StringUtil.BoolToStr( A1077Servico_UORespExclusiva), 1, cmbServico_UORespExclusiva_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_UORespExclusiva.CurrentValue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_UORespExclusiva_Internalname, "Values", (String)(cmbServico_UORespExclusiva.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vinculados_Internalname, "Servico_Vinculados", "", "", lblTextblockservico_vinculados_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Vinculados_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A640Servico_Vinculados), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Vinculados_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vincdesc_Internalname, "Descri��o", "", "", lblTextblockservico_vincdesc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_VincDesc_Internalname, A641Servico_VincDesc, edtServico_VincDesc_Link, "", 0, 1, 0, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_terceriza_Internalname, "Pode ser tercerizado?", "", "", lblTextblockservico_terceriza_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Terceriza, cmbServico_Terceriza_Internalname, StringUtil.BoolToStr( A889Servico_Terceriza), 1, cmbServico_Terceriza_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_Terceriza.CurrentValue = StringUtil.BoolToStr( A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Terceriza_Internalname, "Values", (String)(cmbServico_Terceriza.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_tela_Internalname, "Tela", "", "", lblTextblockservico_tela_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Tela, cmbServico_Tela_Internalname, StringUtil.RTrim( A1061Servico_Tela), 1, cmbServico_Tela_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_Tela.CurrentValue = StringUtil.RTrim( A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Tela_Internalname, "Values", (String)(cmbServico_Tela.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_atende_Internalname, "Atende", "", "", lblTextblockservico_atende_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Atende, cmbServico_Atende_Internalname, StringUtil.RTrim( A1072Servico_Atende), 1, cmbServico_Atende_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_Atende.CurrentValue = StringUtil.RTrim( A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Atende_Internalname, "Values", (String)(cmbServico_Atende.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_obrigavalores_Internalname, "Valores", "", "", lblTextblockservico_obrigavalores_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObrigaValores, cmbServico_ObrigaValores_Internalname, StringUtil.RTrim( A1429Servico_ObrigaValores), 1, cmbServico_ObrigaValores_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_ObrigaValores.CurrentValue = StringUtil.RTrim( A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObrigaValores_Internalname, "Values", (String)(cmbServico_ObrigaValores.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_objetocontrole_Internalname, "Controle", "", "", lblTextblockservico_objetocontrole_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObjetoControle, cmbServico_ObjetoControle_Internalname, StringUtil.RTrim( A1436Servico_ObjetoControle), 1, cmbServico_ObjetoControle_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_ObjetoControle.CurrentValue = StringUtil.RTrim( A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_ObjetoControle_Internalname, "Values", (String)(cmbServico_ObjetoControle.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_tipohierarquia_Internalname, "de Hierarquia", "", "", lblTextblockservico_tipohierarquia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_TipoHierarquia, cmbServico_TipoHierarquia_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)), 1, cmbServico_TipoHierarquia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_TipoHierarquia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_TipoHierarquia_Internalname, "Values", (String)(cmbServico_TipoHierarquia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perctmp_Internalname, "de Tempo", "", "", lblTextblockservico_perctmp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercTmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercTmp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percpgm_Internalname, "de Pagamento", "", "", lblTextblockservico_percpgm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercPgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercPgm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perccnc_Internalname, "de Cancelamento", "", "", lblTextblockservico_perccnc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_anterior_Internalname, "Anterior", "", "", lblTextblockservico_anterior_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Anterior_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1545Servico_Anterior), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1545Servico_Anterior), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Anterior_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_posterior_Internalname, "Posterior", "", "", lblTextblockservico_posterior_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Posterior_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1546Servico_Posterior), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1546Servico_Posterior), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Posterior_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_responsavel_Internalname, "da Contratante", "", "", lblTextblockservico_responsavel_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1551Servico_Responsavel), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_ativo_Internalname, "Ativo?", "", "", lblTextblockservico_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Ativo, cmbServico_Ativo_Internalname, StringUtil.BoolToStr( A632Servico_Ativo), 1, cmbServico_Ativo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoProcessoServico.htm");
            cmbServico_Ativo.CurrentValue = StringUtil.BoolToStr( A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServico_Ativo_Internalname, "Values", (String)(cmbServico_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_LO2e( true) ;
         }
         else
         {
            wb_table2_5_LO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A155Servico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALO2( ) ;
         WSLO2( ) ;
         WELO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA155Servico_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PALO2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoprocessoservico");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PALO2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A155Servico_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         wcpOA155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA155Servico_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A155Servico_Codigo != wcpOA155Servico_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA155Servico_Codigo = A155Servico_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA155Servico_Codigo = cgiGet( sPrefix+"A155Servico_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA155Servico_Codigo) > 0 )
         {
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA155Servico_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         else
         {
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A155Servico_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PALO2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSLO2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSLO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A155Servico_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA155Servico_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A155Servico_Codigo_CTRL", StringUtil.RTrim( sCtrlA155Servico_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WELO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117175041");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicoprocessoservico.js", "?20203117175041");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservico_nome_Internalname = sPrefix+"TEXTBLOCKSERVICO_NOME";
         edtServico_Nome_Internalname = sPrefix+"SERVICO_NOME";
         lblTextblockservico_descricao_Internalname = sPrefix+"TEXTBLOCKSERVICO_DESCRICAO";
         edtServico_Descricao_Internalname = sPrefix+"SERVICO_DESCRICAO";
         lblTextblockservico_sigla_Internalname = sPrefix+"TEXTBLOCKSERVICO_SIGLA";
         edtServico_Sigla_Internalname = sPrefix+"SERVICO_SIGLA";
         lblTextblockservico_uo_Internalname = sPrefix+"TEXTBLOCKSERVICO_UO";
         edtServico_UO_Internalname = sPrefix+"SERVICO_UO";
         lblTextblockservico_uorespexclusiva_Internalname = sPrefix+"TEXTBLOCKSERVICO_UORESPEXCLUSIVA";
         cmbServico_UORespExclusiva_Internalname = sPrefix+"SERVICO_UORESPEXCLUSIVA";
         lblTextblockservico_vinculados_Internalname = sPrefix+"TEXTBLOCKSERVICO_VINCULADOS";
         edtServico_Vinculados_Internalname = sPrefix+"SERVICO_VINCULADOS";
         lblTextblockservico_vincdesc_Internalname = sPrefix+"TEXTBLOCKSERVICO_VINCDESC";
         edtServico_VincDesc_Internalname = sPrefix+"SERVICO_VINCDESC";
         lblTextblockservico_terceriza_Internalname = sPrefix+"TEXTBLOCKSERVICO_TERCERIZA";
         cmbServico_Terceriza_Internalname = sPrefix+"SERVICO_TERCERIZA";
         lblTextblockservico_tela_Internalname = sPrefix+"TEXTBLOCKSERVICO_TELA";
         cmbServico_Tela_Internalname = sPrefix+"SERVICO_TELA";
         lblTextblockservico_atende_Internalname = sPrefix+"TEXTBLOCKSERVICO_ATENDE";
         cmbServico_Atende_Internalname = sPrefix+"SERVICO_ATENDE";
         lblTextblockservico_obrigavalores_Internalname = sPrefix+"TEXTBLOCKSERVICO_OBRIGAVALORES";
         cmbServico_ObrigaValores_Internalname = sPrefix+"SERVICO_OBRIGAVALORES";
         lblTextblockservico_objetocontrole_Internalname = sPrefix+"TEXTBLOCKSERVICO_OBJETOCONTROLE";
         cmbServico_ObjetoControle_Internalname = sPrefix+"SERVICO_OBJETOCONTROLE";
         lblTextblockservico_tipohierarquia_Internalname = sPrefix+"TEXTBLOCKSERVICO_TIPOHIERARQUIA";
         cmbServico_TipoHierarquia_Internalname = sPrefix+"SERVICO_TIPOHIERARQUIA";
         lblTextblockservico_perctmp_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCTMP";
         edtServico_PercTmp_Internalname = sPrefix+"SERVICO_PERCTMP";
         lblTextblockservico_percpgm_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCPGM";
         edtServico_PercPgm_Internalname = sPrefix+"SERVICO_PERCPGM";
         lblTextblockservico_perccnc_Internalname = sPrefix+"TEXTBLOCKSERVICO_PERCCNC";
         edtServico_PercCnc_Internalname = sPrefix+"SERVICO_PERCCNC";
         lblTextblockservico_anterior_Internalname = sPrefix+"TEXTBLOCKSERVICO_ANTERIOR";
         edtServico_Anterior_Internalname = sPrefix+"SERVICO_ANTERIOR";
         lblTextblockservico_posterior_Internalname = sPrefix+"TEXTBLOCKSERVICO_POSTERIOR";
         edtServico_Posterior_Internalname = sPrefix+"SERVICO_POSTERIOR";
         lblTextblockservico_responsavel_Internalname = sPrefix+"TEXTBLOCKSERVICO_RESPONSAVEL";
         edtServico_Responsavel_Internalname = sPrefix+"SERVICO_RESPONSAVEL";
         lblTextblockservico_ativo_Internalname = sPrefix+"TEXTBLOCKSERVICO_ATIVO";
         cmbServico_Ativo_Internalname = sPrefix+"SERVICO_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         edtServicoGrupo_Codigo_Internalname = sPrefix+"SERVICOGRUPO_CODIGO";
         edtServico_Vinculado_Internalname = sPrefix+"SERVICO_VINCULADO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbServico_Ativo_Jsonclick = "";
         edtServico_Responsavel_Jsonclick = "";
         edtServico_Posterior_Jsonclick = "";
         edtServico_Anterior_Jsonclick = "";
         edtServico_PercCnc_Jsonclick = "";
         edtServico_PercPgm_Jsonclick = "";
         edtServico_PercTmp_Jsonclick = "";
         cmbServico_TipoHierarquia_Jsonclick = "";
         cmbServico_ObjetoControle_Jsonclick = "";
         cmbServico_ObrigaValores_Jsonclick = "";
         cmbServico_Atende_Jsonclick = "";
         cmbServico_Tela_Jsonclick = "";
         cmbServico_Terceriza_Jsonclick = "";
         edtServico_Vinculados_Jsonclick = "";
         cmbServico_UORespExclusiva_Jsonclick = "";
         edtServico_UO_Jsonclick = "";
         edtServico_Sigla_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServico_VincDesc_Link = "";
         edtServico_Descricao_Link = "";
         edtServico_Vinculado_Jsonclick = "";
         edtServico_Vinculado_Visible = 1;
         edtServicoGrupo_Codigo_Jsonclick = "";
         edtServicoGrupo_Codigo_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Visible = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13LO2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14LO2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A608Servico_Nome = "";
         A156Servico_Descricao = "";
         A605Servico_Sigla = "";
         A1061Servico_Tela = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A1436Servico_ObjetoControle = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00LO2_A631Servico_Vinculado = new int[1] ;
         H00LO2_n631Servico_Vinculado = new bool[] {false} ;
         H00LO2_A157ServicoGrupo_Codigo = new int[1] ;
         H00LO2_A632Servico_Ativo = new bool[] {false} ;
         H00LO2_A1546Servico_Posterior = new int[1] ;
         H00LO2_n1546Servico_Posterior = new bool[] {false} ;
         H00LO2_A1545Servico_Anterior = new int[1] ;
         H00LO2_n1545Servico_Anterior = new bool[] {false} ;
         H00LO2_A1536Servico_PercCnc = new short[1] ;
         H00LO2_n1536Servico_PercCnc = new bool[] {false} ;
         H00LO2_A1535Servico_PercPgm = new short[1] ;
         H00LO2_n1535Servico_PercPgm = new bool[] {false} ;
         H00LO2_A1534Servico_PercTmp = new short[1] ;
         H00LO2_n1534Servico_PercTmp = new bool[] {false} ;
         H00LO2_A1530Servico_TipoHierarquia = new short[1] ;
         H00LO2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00LO2_A1436Servico_ObjetoControle = new String[] {""} ;
         H00LO2_A1429Servico_ObrigaValores = new String[] {""} ;
         H00LO2_n1429Servico_ObrigaValores = new bool[] {false} ;
         H00LO2_A1072Servico_Atende = new String[] {""} ;
         H00LO2_n1072Servico_Atende = new bool[] {false} ;
         H00LO2_A1061Servico_Tela = new String[] {""} ;
         H00LO2_n1061Servico_Tela = new bool[] {false} ;
         H00LO2_A889Servico_Terceriza = new bool[] {false} ;
         H00LO2_n889Servico_Terceriza = new bool[] {false} ;
         H00LO2_A641Servico_VincDesc = new String[] {""} ;
         H00LO2_n641Servico_VincDesc = new bool[] {false} ;
         H00LO2_A1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LO2_n1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LO2_A633Servico_UO = new int[1] ;
         H00LO2_n633Servico_UO = new bool[] {false} ;
         H00LO2_A605Servico_Sigla = new String[] {""} ;
         H00LO2_A156Servico_Descricao = new String[] {""} ;
         H00LO2_n156Servico_Descricao = new bool[] {false} ;
         H00LO2_A608Servico_Nome = new String[] {""} ;
         H00LO2_A155Servico_Codigo = new int[1] ;
         A641Servico_VincDesc = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         lblTextblockservico_descricao_Jsonclick = "";
         lblTextblockservico_sigla_Jsonclick = "";
         lblTextblockservico_uo_Jsonclick = "";
         lblTextblockservico_uorespexclusiva_Jsonclick = "";
         lblTextblockservico_vinculados_Jsonclick = "";
         lblTextblockservico_vincdesc_Jsonclick = "";
         lblTextblockservico_terceriza_Jsonclick = "";
         lblTextblockservico_tela_Jsonclick = "";
         lblTextblockservico_atende_Jsonclick = "";
         lblTextblockservico_obrigavalores_Jsonclick = "";
         lblTextblockservico_objetocontrole_Jsonclick = "";
         lblTextblockservico_tipohierarquia_Jsonclick = "";
         lblTextblockservico_perctmp_Jsonclick = "";
         lblTextblockservico_percpgm_Jsonclick = "";
         lblTextblockservico_perccnc_Jsonclick = "";
         lblTextblockservico_anterior_Jsonclick = "";
         lblTextblockservico_posterior_Jsonclick = "";
         lblTextblockservico_responsavel_Jsonclick = "";
         lblTextblockservico_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA155Servico_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoprocessoservico__default(),
            new Object[][] {
                new Object[] {
               H00LO2_A631Servico_Vinculado, H00LO2_n631Servico_Vinculado, H00LO2_A157ServicoGrupo_Codigo, H00LO2_A632Servico_Ativo, H00LO2_A1546Servico_Posterior, H00LO2_n1546Servico_Posterior, H00LO2_A1545Servico_Anterior, H00LO2_n1545Servico_Anterior, H00LO2_A1536Servico_PercCnc, H00LO2_n1536Servico_PercCnc,
               H00LO2_A1535Servico_PercPgm, H00LO2_n1535Servico_PercPgm, H00LO2_A1534Servico_PercTmp, H00LO2_n1534Servico_PercTmp, H00LO2_A1530Servico_TipoHierarquia, H00LO2_n1530Servico_TipoHierarquia, H00LO2_A1436Servico_ObjetoControle, H00LO2_A1429Servico_ObrigaValores, H00LO2_n1429Servico_ObrigaValores, H00LO2_A1072Servico_Atende,
               H00LO2_n1072Servico_Atende, H00LO2_A1061Servico_Tela, H00LO2_n1061Servico_Tela, H00LO2_A889Servico_Terceriza, H00LO2_n889Servico_Terceriza, H00LO2_A641Servico_VincDesc, H00LO2_n641Servico_VincDesc, H00LO2_A1077Servico_UORespExclusiva, H00LO2_n1077Servico_UORespExclusiva, H00LO2_A633Servico_UO,
               H00LO2_n633Servico_UO, H00LO2_A605Servico_Sigla, H00LO2_A156Servico_Descricao, H00LO2_n156Servico_Descricao, H00LO2_A608Servico_Nome, H00LO2_A155Servico_Codigo
               }
            }
         );
         AV15Pgmname = "ServicoProcessoServico";
         /* GeneXus formulas. */
         AV15Pgmname = "ServicoProcessoServico";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A640Servico_Vinculados ;
      private short A1530Servico_TipoHierarquia ;
      private short A1534Servico_PercTmp ;
      private short A1535Servico_PercPgm ;
      private short A1536Servico_PercCnc ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GXt_int1 ;
      private short nGXWrapped ;
      private int A155Servico_Codigo ;
      private int wcpOA155Servico_Codigo ;
      private int A1551Servico_Responsavel ;
      private int AV12ServicoGrupo_Codigo ;
      private int A633Servico_UO ;
      private int A1545Servico_Anterior ;
      private int A1546Servico_Posterior ;
      private int A157ServicoGrupo_Codigo ;
      private int A631Servico_Vinculado ;
      private int edtServico_Codigo_Visible ;
      private int edtServicoGrupo_Codigo_Visible ;
      private int edtServico_Vinculado_Visible ;
      private int GXt_int2 ;
      private int AV7Servico_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private String A1061Servico_Tela ;
      private String A1072Servico_Atende ;
      private String A1429Servico_ObrigaValores ;
      private String A1436Servico_ObjetoControle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServicoGrupo_Codigo_Internalname ;
      private String edtServicoGrupo_Codigo_Jsonclick ;
      private String edtServico_Vinculado_Internalname ;
      private String edtServico_Vinculado_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtServico_Nome_Internalname ;
      private String edtServico_Descricao_Internalname ;
      private String edtServico_Sigla_Internalname ;
      private String edtServico_UO_Internalname ;
      private String cmbServico_UORespExclusiva_Internalname ;
      private String edtServico_Vinculados_Internalname ;
      private String edtServico_VincDesc_Internalname ;
      private String cmbServico_Terceriza_Internalname ;
      private String cmbServico_Tela_Internalname ;
      private String cmbServico_Atende_Internalname ;
      private String cmbServico_ObrigaValores_Internalname ;
      private String cmbServico_ObjetoControle_Internalname ;
      private String cmbServico_TipoHierarquia_Internalname ;
      private String edtServico_PercTmp_Internalname ;
      private String edtServico_PercPgm_Internalname ;
      private String edtServico_PercCnc_Internalname ;
      private String edtServico_Anterior_Internalname ;
      private String edtServico_Posterior_Internalname ;
      private String edtServico_Responsavel_Internalname ;
      private String cmbServico_Ativo_Internalname ;
      private String hsh ;
      private String edtServico_Descricao_Link ;
      private String edtServico_VincDesc_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String lblTextblockservico_descricao_Internalname ;
      private String lblTextblockservico_descricao_Jsonclick ;
      private String lblTextblockservico_sigla_Internalname ;
      private String lblTextblockservico_sigla_Jsonclick ;
      private String edtServico_Sigla_Jsonclick ;
      private String lblTextblockservico_uo_Internalname ;
      private String lblTextblockservico_uo_Jsonclick ;
      private String edtServico_UO_Jsonclick ;
      private String lblTextblockservico_uorespexclusiva_Internalname ;
      private String lblTextblockservico_uorespexclusiva_Jsonclick ;
      private String cmbServico_UORespExclusiva_Jsonclick ;
      private String lblTextblockservico_vinculados_Internalname ;
      private String lblTextblockservico_vinculados_Jsonclick ;
      private String edtServico_Vinculados_Jsonclick ;
      private String lblTextblockservico_vincdesc_Internalname ;
      private String lblTextblockservico_vincdesc_Jsonclick ;
      private String lblTextblockservico_terceriza_Internalname ;
      private String lblTextblockservico_terceriza_Jsonclick ;
      private String cmbServico_Terceriza_Jsonclick ;
      private String lblTextblockservico_tela_Internalname ;
      private String lblTextblockservico_tela_Jsonclick ;
      private String cmbServico_Tela_Jsonclick ;
      private String lblTextblockservico_atende_Internalname ;
      private String lblTextblockservico_atende_Jsonclick ;
      private String cmbServico_Atende_Jsonclick ;
      private String lblTextblockservico_obrigavalores_Internalname ;
      private String lblTextblockservico_obrigavalores_Jsonclick ;
      private String cmbServico_ObrigaValores_Jsonclick ;
      private String lblTextblockservico_objetocontrole_Internalname ;
      private String lblTextblockservico_objetocontrole_Jsonclick ;
      private String cmbServico_ObjetoControle_Jsonclick ;
      private String lblTextblockservico_tipohierarquia_Internalname ;
      private String lblTextblockservico_tipohierarquia_Jsonclick ;
      private String cmbServico_TipoHierarquia_Jsonclick ;
      private String lblTextblockservico_perctmp_Internalname ;
      private String lblTextblockservico_perctmp_Jsonclick ;
      private String edtServico_PercTmp_Jsonclick ;
      private String lblTextblockservico_percpgm_Internalname ;
      private String lblTextblockservico_percpgm_Jsonclick ;
      private String edtServico_PercPgm_Jsonclick ;
      private String lblTextblockservico_perccnc_Internalname ;
      private String lblTextblockservico_perccnc_Jsonclick ;
      private String edtServico_PercCnc_Jsonclick ;
      private String lblTextblockservico_anterior_Internalname ;
      private String lblTextblockservico_anterior_Jsonclick ;
      private String edtServico_Anterior_Jsonclick ;
      private String lblTextblockservico_posterior_Internalname ;
      private String lblTextblockservico_posterior_Jsonclick ;
      private String edtServico_Posterior_Jsonclick ;
      private String lblTextblockservico_responsavel_Internalname ;
      private String lblTextblockservico_responsavel_Jsonclick ;
      private String edtServico_Responsavel_Jsonclick ;
      private String lblTextblockservico_ativo_Internalname ;
      private String lblTextblockservico_ativo_Jsonclick ;
      private String cmbServico_Ativo_Jsonclick ;
      private String sCtrlA155Servico_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1077Servico_UORespExclusiva ;
      private bool A889Servico_Terceriza ;
      private bool A632Servico_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n631Servico_Vinculado ;
      private bool n1546Servico_Posterior ;
      private bool n1545Servico_Anterior ;
      private bool n1536Servico_PercCnc ;
      private bool n1535Servico_PercPgm ;
      private bool n1534Servico_PercTmp ;
      private bool n641Servico_VincDesc ;
      private bool n633Servico_UO ;
      private bool n156Servico_Descricao ;
      private bool returnInSub ;
      private String A156Servico_Descricao ;
      private String A641Servico_VincDesc ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbServico_UORespExclusiva ;
      private GXCombobox cmbServico_Terceriza ;
      private GXCombobox cmbServico_Tela ;
      private GXCombobox cmbServico_Atende ;
      private GXCombobox cmbServico_ObrigaValores ;
      private GXCombobox cmbServico_ObjetoControle ;
      private GXCombobox cmbServico_TipoHierarquia ;
      private GXCombobox cmbServico_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00LO2_A631Servico_Vinculado ;
      private bool[] H00LO2_n631Servico_Vinculado ;
      private int[] H00LO2_A157ServicoGrupo_Codigo ;
      private bool[] H00LO2_A632Servico_Ativo ;
      private int[] H00LO2_A1546Servico_Posterior ;
      private bool[] H00LO2_n1546Servico_Posterior ;
      private int[] H00LO2_A1545Servico_Anterior ;
      private bool[] H00LO2_n1545Servico_Anterior ;
      private short[] H00LO2_A1536Servico_PercCnc ;
      private bool[] H00LO2_n1536Servico_PercCnc ;
      private short[] H00LO2_A1535Servico_PercPgm ;
      private bool[] H00LO2_n1535Servico_PercPgm ;
      private short[] H00LO2_A1534Servico_PercTmp ;
      private bool[] H00LO2_n1534Servico_PercTmp ;
      private short[] H00LO2_A1530Servico_TipoHierarquia ;
      private bool[] H00LO2_n1530Servico_TipoHierarquia ;
      private String[] H00LO2_A1436Servico_ObjetoControle ;
      private String[] H00LO2_A1429Servico_ObrigaValores ;
      private bool[] H00LO2_n1429Servico_ObrigaValores ;
      private String[] H00LO2_A1072Servico_Atende ;
      private bool[] H00LO2_n1072Servico_Atende ;
      private String[] H00LO2_A1061Servico_Tela ;
      private bool[] H00LO2_n1061Servico_Tela ;
      private bool[] H00LO2_A889Servico_Terceriza ;
      private bool[] H00LO2_n889Servico_Terceriza ;
      private String[] H00LO2_A641Servico_VincDesc ;
      private bool[] H00LO2_n641Servico_VincDesc ;
      private bool[] H00LO2_A1077Servico_UORespExclusiva ;
      private bool[] H00LO2_n1077Servico_UORespExclusiva ;
      private int[] H00LO2_A633Servico_UO ;
      private bool[] H00LO2_n633Servico_UO ;
      private String[] H00LO2_A605Servico_Sigla ;
      private String[] H00LO2_A156Servico_Descricao ;
      private bool[] H00LO2_n156Servico_Descricao ;
      private String[] H00LO2_A608Servico_Nome ;
      private int[] H00LO2_A155Servico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class servicoprocessoservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LO2 ;
          prmH00LO2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LO2", "SELECT T1.[Servico_Vinculado] AS Servico_Vinculado, T1.[ServicoGrupo_Codigo], T1.[Servico_Ativo], T1.[Servico_Posterior], T1.[Servico_Anterior], T1.[Servico_PercCnc], T1.[Servico_PercPgm], T1.[Servico_PercTmp], T1.[Servico_TipoHierarquia], T1.[Servico_ObjetoControle], T1.[Servico_ObrigaValores], T1.[Servico_Atende], T1.[Servico_Tela], T1.[Servico_Terceriza], T2.[Servico_Descricao] AS Servico_VincDesc, T1.[Servico_UORespExclusiva], T1.[Servico_UO], T1.[Servico_Sigla], T1.[Servico_Descricao], T1.[Servico_Nome], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Vinculado]) WHERE T1.[Servico_Codigo] = @Servico_Codigo ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LO2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 3) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 3) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((bool[]) buf[23])[0] = rslt.getBool(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((bool[]) buf[27])[0] = rslt.getBool(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((String[]) buf[31])[0] = rslt.getString(18, 15) ;
                ((String[]) buf[32])[0] = rslt.getLongVarchar(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((String[]) buf[34])[0] = rslt.getString(20, 50) ;
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
