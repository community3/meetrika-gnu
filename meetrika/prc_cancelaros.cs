/*
               File: PRC_CancelarOS
        Description: Cancelar OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:3.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_cancelaros : GXProcedure
   {
      public prc_cancelaros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_cancelaros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_UserId ,
                           String aP2_Observacao ,
                           int aP3_NaoConformidade_Codigo )
      {
         this.AV22ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11UserId = aP1_UserId;
         this.AV10Observacao = aP2_Observacao;
         this.AV19NaoConformidade_Codigo = aP3_NaoConformidade_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_UserId ,
                                 String aP2_Observacao ,
                                 int aP3_NaoConformidade_Codigo )
      {
         prc_cancelaros objprc_cancelaros;
         objprc_cancelaros = new prc_cancelaros();
         objprc_cancelaros.AV22ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_cancelaros.AV11UserId = aP1_UserId;
         objprc_cancelaros.AV10Observacao = aP2_Observacao;
         objprc_cancelaros.AV19NaoConformidade_Codigo = aP3_NaoConformidade_Codigo;
         objprc_cancelaros.context.SetSubmitInitialConfig(context);
         objprc_cancelaros.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_cancelaros);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_cancelaros)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Codigos.FromXml(AV21websession.Get("Codigos"), "Collection");
         AV21websession.Remove("Codigos");
         if ( (0==AV20Codigos.IndexOf(AV22ContagemResultado_Codigo)) )
         {
            AV20Codigos.Add(AV22ContagemResultado_Codigo, 0);
         }
         AV25GXV1 = 1;
         while ( AV25GXV1 <= AV20Codigos.Count )
         {
            AV8Codigo = (int)(AV20Codigos.GetNumeric(AV25GXV1));
            /* Using cursor P008V2 */
            pr_default.execute(0, new Object[] {AV8Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P008V2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008V2_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P008V2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008V2_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P008V2_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008V2_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P008V2_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008V2_n912ContagemResultado_HoraEntrega[0];
               A602ContagemResultado_OSVinculada = P008V2_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P008V2_n602ContagemResultado_OSVinculada[0];
               A1539ContratoServicos_PercCnc = P008V2_A1539ContratoServicos_PercCnc[0];
               n1539ContratoServicos_PercCnc = P008V2_n1539ContratoServicos_PercCnc[0];
               A1854ContagemResultado_VlrCnc = P008V2_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = P008V2_n1854ContagemResultado_VlrCnc[0];
               A468ContagemResultado_NaoCnfDmnCod = P008V2_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = P008V2_n468ContagemResultado_NaoCnfDmnCod[0];
               A456ContagemResultado_Codigo = P008V2_A456ContagemResultado_Codigo[0];
               A1539ContratoServicos_PercCnc = P008V2_A1539ContratoServicos_PercCnc[0];
               n1539ContratoServicos_PercCnc = P008V2_n1539ContratoServicos_PercCnc[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               A574ContagemResultado_PFFinal = GXt_decimal1;
               AV9StatusDemanda = A484ContagemResultado_StatusDmn;
               AV12PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
               AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV12PrazoEntrega = DateTimeUtil.TAdd( AV12PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               AV17OsVinculada = A602ContagemResultado_OSVinculada;
               AV18StatusRemunerado = "EABD";
               if ( StringUtil.StringSearch( AV18StatusRemunerado, A484ContagemResultado_StatusDmn, 1) > 0 )
               {
                  A1854ContagemResultado_VlrCnc = (decimal)(A574ContagemResultado_PFFinal*(A1539ContratoServicos_PercCnc/ (decimal)(100)));
                  n1854ContagemResultado_VlrCnc = false;
               }
               if ( AV19NaoConformidade_Codigo > 0 )
               {
                  A468ContagemResultado_NaoCnfDmnCod = AV19NaoConformidade_Codigo;
                  n468ContagemResultado_NaoCnfDmnCod = false;
               }
               A484ContagemResultado_StatusDmn = "X";
               n484ContagemResultado_StatusDmn = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008V3 */
               pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, A456ContagemResultado_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P008V4 */
               pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, A456ContagemResultado_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV17OsVinculada > 0 )
            {
               new prc_upddpnhmlg(context ).execute( ref  AV17OsVinculada) ;
            }
            new prc_inslogresponsavel(context ).execute( ref  AV8Codigo,  0,  "X",  "D",  AV11UserId,  0,  AV9StatusDemanda,  "X",  AV10Observacao,  AV12PrazoEntrega,  true) ;
            new prc_disparoservicovinculado(context ).execute(  AV8Codigo,  AV11UserId) ;
            AV25GXV1 = (int)(AV25GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_CancelarOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Codigos = new GxSimpleCollection();
         AV21websession = context.GetSession();
         scmdbuf = "";
         P008V2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008V2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008V2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008V2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008V2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008V2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008V2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008V2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008V2_A602ContagemResultado_OSVinculada = new int[1] ;
         P008V2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P008V2_A1539ContratoServicos_PercCnc = new short[1] ;
         P008V2_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         P008V2_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P008V2_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P008V2_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P008V2_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P008V2_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV9StatusDemanda = "";
         AV12PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV18StatusRemunerado = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_cancelaros__default(),
            new Object[][] {
                new Object[] {
               P008V2_A1553ContagemResultado_CntSrvCod, P008V2_n1553ContagemResultado_CntSrvCod, P008V2_A484ContagemResultado_StatusDmn, P008V2_n484ContagemResultado_StatusDmn, P008V2_A472ContagemResultado_DataEntrega, P008V2_n472ContagemResultado_DataEntrega, P008V2_A912ContagemResultado_HoraEntrega, P008V2_n912ContagemResultado_HoraEntrega, P008V2_A602ContagemResultado_OSVinculada, P008V2_n602ContagemResultado_OSVinculada,
               P008V2_A1539ContratoServicos_PercCnc, P008V2_n1539ContratoServicos_PercCnc, P008V2_A1854ContagemResultado_VlrCnc, P008V2_n1854ContagemResultado_VlrCnc, P008V2_A468ContagemResultado_NaoCnfDmnCod, P008V2_n468ContagemResultado_NaoCnfDmnCod, P008V2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1539ContratoServicos_PercCnc ;
      private int AV22ContagemResultado_Codigo ;
      private int AV11UserId ;
      private int AV19NaoConformidade_Codigo ;
      private int AV25GXV1 ;
      private int AV8Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV17OsVinculada ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV9StatusDemanda ;
      private String AV18StatusRemunerado ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV12PrazoEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private String AV10Observacao ;
      private IGxSession AV21websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008V2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008V2_n1553ContagemResultado_CntSrvCod ;
      private String[] P008V2_A484ContagemResultado_StatusDmn ;
      private bool[] P008V2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P008V2_A472ContagemResultado_DataEntrega ;
      private bool[] P008V2_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008V2_A912ContagemResultado_HoraEntrega ;
      private bool[] P008V2_n912ContagemResultado_HoraEntrega ;
      private int[] P008V2_A602ContagemResultado_OSVinculada ;
      private bool[] P008V2_n602ContagemResultado_OSVinculada ;
      private short[] P008V2_A1539ContratoServicos_PercCnc ;
      private bool[] P008V2_n1539ContratoServicos_PercCnc ;
      private decimal[] P008V2_A1854ContagemResultado_VlrCnc ;
      private bool[] P008V2_n1854ContagemResultado_VlrCnc ;
      private int[] P008V2_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P008V2_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P008V2_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Codigos ;
   }

   public class prc_cancelaros__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008V2 ;
          prmP008V2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008V3 ;
          prmP008V3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008V4 ;
          prmP008V4 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008V2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_OSVinculada], T2.[ContratoServicos_PercCnc], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008V2,1,0,true,true )
             ,new CursorDef("P008V3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_VlrCnc]=@ContagemResultado_VlrCnc, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008V3)
             ,new CursorDef("P008V4", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_VlrCnc]=@ContagemResultado_VlrCnc, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008V4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
       }
    }

 }

}
