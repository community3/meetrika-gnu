/*
               File: WWVariaveisCocomo
        Description:  Variaveis Cocomo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:54:53.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwvariaveiscocomo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwvariaveiscocomo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwvariaveiscocomo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavVariavelcocomo_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavVariavelcocomo_tipo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavVariavelcocomo_tipo3 = new GXCombobox();
         cmbVariavelCocomo_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_77 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_77_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_77_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV32VariavelCocomo_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32VariavelCocomo_Sigla1", AV32VariavelCocomo_Sigla1);
               AV35VariavelCocomo_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV33VariavelCocomo_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33VariavelCocomo_Sigla2", AV33VariavelCocomo_Sigla2);
               AV36VariavelCocomo_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV34VariavelCocomo_Sigla3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34VariavelCocomo_Sigla3", AV34VariavelCocomo_Sigla3);
               AV37VariavelCocomo_Tipo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV41TFVariavelCocomo_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFVariavelCocomo_Sigla", AV41TFVariavelCocomo_Sigla);
               AV42TFVariavelCocomo_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFVariavelCocomo_Sigla_Sel", AV42TFVariavelCocomo_Sigla_Sel);
               AV49TFVariavelCocomo_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
               AV50TFVariavelCocomo_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
               AV55TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
               AV56TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
               AV59TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
               AV60TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
               AV63TFVariavelCocomo_Baixo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
               AV64TFVariavelCocomo_Baixo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
               AV67TFVariavelCocomo_Nominal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
               AV68TFVariavelCocomo_Nominal_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
               AV71TFVariavelCocomo_Alto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
               AV72TFVariavelCocomo_Alto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
               AV75TFVariavelCocomo_MuitoAlto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
               AV76TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
               AV79TFVariavelCocomo_ExtraAlto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
               AV80TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
               AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
               AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace);
               AV53ddo_VariavelCocomo_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_DataTitleControlIdToReplace", AV53ddo_VariavelCocomo_DataTitleControlIdToReplace);
               AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
               AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
               AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
               AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace);
               AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace);
               AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
               AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
               AV31VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV46TFVariavelCocomo_Tipo_Sels);
               AV118Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A961VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               A962VariavelCocomo_Sigla = GetNextPar( );
               A964VariavelCocomo_Tipo = GetNextPar( );
               A992VariavelCocomo_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAGK2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTGK2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118545477");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwvariaveiscocomo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_SIGLA1", StringUtil.RTrim( AV32VariavelCocomo_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_TIPO1", StringUtil.RTrim( AV35VariavelCocomo_Tipo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_SIGLA2", StringUtil.RTrim( AV33VariavelCocomo_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_TIPO2", StringUtil.RTrim( AV36VariavelCocomo_Tipo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_SIGLA3", StringUtil.RTrim( AV34VariavelCocomo_Sigla3));
         GxWebStd.gx_hidden_field( context, "GXH_vVARIAVELCOCOMO_TIPO3", StringUtil.RTrim( AV37VariavelCocomo_Tipo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_SIGLA", StringUtil.RTrim( AV41TFVariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL", StringUtil.RTrim( AV42TFVariavelCocomo_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_DATA", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_DATA_TO", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO", StringUtil.LTrim( StringUtil.NToC( AV55TFVariavelCocomo_ExtraBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO", StringUtil.LTrim( StringUtil.NToC( AV59TFVariavelCocomo_MuitoBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_BAIXO", StringUtil.LTrim( StringUtil.NToC( AV63TFVariavelCocomo_Baixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_BAIXO_TO", StringUtil.LTrim( StringUtil.NToC( AV64TFVariavelCocomo_Baixo_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOMINAL", StringUtil.LTrim( StringUtil.NToC( AV67TFVariavelCocomo_Nominal, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO", StringUtil.LTrim( StringUtil.NToC( AV68TFVariavelCocomo_Nominal_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_ALTO", StringUtil.LTrim( StringUtil.NToC( AV71TFVariavelCocomo_Alto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_ALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV72TFVariavelCocomo_Alto_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOALTO", StringUtil.LTrim( StringUtil.NToC( AV75TFVariavelCocomo_MuitoAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRAALTO", StringUtil.LTrim( StringUtil.NToC( AV79TFVariavelCocomo_ExtraAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO", StringUtil.LTrim( StringUtil.NToC( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_77", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_77), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_SIGLATITLEFILTERDATA", AV40VariavelCocomo_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_SIGLATITLEFILTERDATA", AV40VariavelCocomo_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_TIPOTITLEFILTERDATA", AV44VariavelCocomo_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_TIPOTITLEFILTERDATA", AV44VariavelCocomo_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_DATATITLEFILTERDATA", AV48VariavelCocomo_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_DATATITLEFILTERDATA", AV48VariavelCocomo_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA", AV54VariavelCocomo_ExtraBaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA", AV54VariavelCocomo_ExtraBaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA", AV58VariavelCocomo_MuitoBaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA", AV58VariavelCocomo_MuitoBaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA", AV62VariavelCocomo_BaixoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA", AV62VariavelCocomo_BaixoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA", AV66VariavelCocomo_NominalTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA", AV66VariavelCocomo_NominalTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_ALTOTITLEFILTERDATA", AV70VariavelCocomo_AltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_ALTOTITLEFILTERDATA", AV70VariavelCocomo_AltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA", AV74VariavelCocomo_MuitoAltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA", AV74VariavelCocomo_MuitoAltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA", AV78VariavelCocomo_ExtraAltoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA", AV78VariavelCocomo_ExtraAltoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFVARIAVELCOCOMO_TIPO_SELS", AV46TFVariavelCocomo_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFVARIAVELCOCOMO_TIPO_SELS", AV46TFVariavelCocomo_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV118Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A992VariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Caption", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Cls", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Datalisttype", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_variavelcocomo_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Caption", StringUtil.RTrim( Ddo_variavelcocomo_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Cls", StringUtil.RTrim( Ddo_variavelcocomo_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortasc", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Sortdsc", StringUtil.RTrim( Ddo_variavelcocomo_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_extrabaixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_muitobaixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_baixo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Caption", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Cls", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_nominal_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_alto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_alto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_alto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_alto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_alto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_alto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_alto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_alto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_alto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_alto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_muitoalto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Caption", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Tooltip", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Cls", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_set", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_set", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortasc", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortdsc", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includefilter", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filtertype", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filterisrange", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Includedatalist", StringUtil.BoolToStr( Ddo_variavelcocomo_extraalto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Cleanfilter", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterfrom", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterto", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Searchbuttontext", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_variavelcocomo_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_variavelcocomo_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_extrabaixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_muitobaixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_baixo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_nominal_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_alto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_alto_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_muitoalto_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Activeeventkey", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_get", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_get", StringUtil.RTrim( Ddo_variavelcocomo_extraalto_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEGK2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTGK2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwvariaveiscocomo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWVariaveisCocomo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Variaveis Cocomo" ;
      }

      protected void WBGK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_GK2( true) ;
         }
         else
         {
            wb_table1_2_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(92, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_sigla_Internalname, StringUtil.RTrim( AV41TFVariavelCocomo_Sigla), StringUtil.RTrim( context.localUtil.Format( AV41TFVariavelCocomo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_sigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_sigla_sel_Internalname, StringUtil.RTrim( AV42TFVariavelCocomo_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV42TFVariavelCocomo_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_sigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_77_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfvariavelcocomo_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_data_Internalname, context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"), context.localUtil.Format( AV49TFVariavelCocomo_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavTfvariavelcocomo_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfvariavelcocomo_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_77_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfvariavelcocomo_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_data_to_Internalname, context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"), context.localUtil.Format( AV50TFVariavelCocomo_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavTfvariavelcocomo_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfvariavelcocomo_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_variavelcocomo_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_77_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_variavelcocomo_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_variavelcocomo_dataauxdate_Internalname, context.localUtil.Format(AV51DDO_VariavelCocomo_DataAuxDate, "99/99/99"), context.localUtil.Format( AV51DDO_VariavelCocomo_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_variavelcocomo_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_variavelcocomo_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_77_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_variavelcocomo_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_variavelcocomo_dataauxdateto_Internalname, context.localUtil.Format(AV52DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV52DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_variavelcocomo_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_variavelcocomo_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extrabaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV55TFVariavelCocomo_ExtraBaixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV55TFVariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extrabaixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extrabaixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extrabaixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV56TFVariavelCocomo_ExtraBaixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV56TFVariavelCocomo_ExtraBaixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extrabaixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extrabaixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitobaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFVariavelCocomo_MuitoBaixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFVariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitobaixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitobaixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitobaixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFVariavelCocomo_MuitoBaixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFVariavelCocomo_MuitoBaixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitobaixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitobaixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_baixo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV63TFVariavelCocomo_Baixo, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV63TFVariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_baixo_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_baixo_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_baixo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV64TFVariavelCocomo_Baixo_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV64TFVariavelCocomo_Baixo_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_baixo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_baixo_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nominal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV67TFVariavelCocomo_Nominal, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV67TFVariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nominal_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nominal_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_nominal_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV68TFVariavelCocomo_Nominal_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV68TFVariavelCocomo_Nominal_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_nominal_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_nominal_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_alto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV71TFVariavelCocomo_Alto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV71TFVariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_alto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_alto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_alto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV72TFVariavelCocomo_Alto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV72TFVariavelCocomo_Alto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_alto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_alto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitoalto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV75TFVariavelCocomo_MuitoAlto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV75TFVariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitoalto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitoalto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_muitoalto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV76TFVariavelCocomo_MuitoAlto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV76TFVariavelCocomo_MuitoAlto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_muitoalto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_muitoalto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extraalto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV79TFVariavelCocomo_ExtraAlto, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV79TFVariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extraalto_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extraalto_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfvariavelcocomo_extraalto_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV80TFVariavelCocomo_ExtraAlto_To, 18, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV80TFVariavelCocomo_ExtraAlto_To, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfvariavelcocomo_extraalto_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfvariavelcocomo_extraalto_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_EXTRABAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_MUITOBAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_BAIXOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_NOMINALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_ALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_MUITOALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_VARIAVELCOCOMO_EXTRAALTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWVariaveisCocomo.htm");
         }
         wbLoad = true;
      }

      protected void STARTGK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Variaveis Cocomo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGK0( ) ;
      }

      protected void WSGK2( )
      {
         STARTGK2( ) ;
         EVTGK2( ) ;
      }

      protected void EVTGK2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11GK2 */
                              E11GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12GK2 */
                              E12GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13GK2 */
                              E13GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_EXTRABAIXO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14GK2 */
                              E14GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_MUITOBAIXO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15GK2 */
                              E15GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_BAIXO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16GK2 */
                              E16GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_NOMINAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17GK2 */
                              E17GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_ALTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18GK2 */
                              E18GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_MUITOALTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19GK2 */
                              E19GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_VARIAVELCOCOMO_EXTRAALTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20GK2 */
                              E20GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21GK2 */
                              E21GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22GK2 */
                              E22GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23GK2 */
                              E23GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24GK2 */
                              E24GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25GK2 */
                              E25GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26GK2 */
                              E26GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27GK2 */
                              E27GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28GK2 */
                              E28GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29GK2 */
                              E29GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E30GK2 */
                              E30GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E31GK2 */
                              E31GK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
                              AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
                              AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
                              AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
                              AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
                              AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
                              AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
                              AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
                              AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
                              AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
                              AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
                              AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
                              AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
                              AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
                              AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
                              AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
                              AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
                              AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
                              AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
                              AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
                              AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
                              AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
                              AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
                              AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
                              AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
                              AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
                              AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
                              AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
                              AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
                              AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
                              AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_77_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
                              SubsflControlProps_772( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV116Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV117Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A962VariavelCocomo_Sigla = StringUtil.Upper( cgiGet( edtVariavelCocomo_Sigla_Internalname));
                              cmbVariavelCocomo_Tipo.Name = cmbVariavelCocomo_Tipo_Internalname;
                              cmbVariavelCocomo_Tipo.CurrentValue = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
                              A964VariavelCocomo_Tipo = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
                              A966VariavelCocomo_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtVariavelCocomo_Data_Internalname), 0));
                              n966VariavelCocomo_Data = false;
                              A986VariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".");
                              n986VariavelCocomo_ExtraBaixo = false;
                              A967VariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".");
                              n967VariavelCocomo_MuitoBaixo = false;
                              A968VariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".");
                              n968VariavelCocomo_Baixo = false;
                              A969VariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".");
                              n969VariavelCocomo_Nominal = false;
                              A970VariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".");
                              n970VariavelCocomo_Alto = false;
                              A971VariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".");
                              n971VariavelCocomo_MuitoAlto = false;
                              A972VariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".");
                              n972VariavelCocomo_ExtraAlto = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32GK2 */
                                    E32GK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E33GK2 */
                                    E33GK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E34GK2 */
                                    E34GK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_sigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA1"), AV32VariavelCocomo_Sigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_tipo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO1"), AV35VariavelCocomo_Tipo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_sigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA2"), AV33VariavelCocomo_Sigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_tipo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO2"), AV36VariavelCocomo_Tipo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_sigla3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA3"), AV34VariavelCocomo_Sigla3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Variavelcocomo_tipo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO3"), AV37VariavelCocomo_Tipo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_sigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA"), AV41TFVariavelCocomo_Sigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_sigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL"), AV42TFVariavelCocomo_Sigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA"), 0) != AV49TFVariavelCocomo_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA_TO"), 0) != AV50TFVariavelCocomo_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_extrabaixo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO"), ",", ".") != AV55TFVariavelCocomo_ExtraBaixo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_extrabaixo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO"), ",", ".") != AV56TFVariavelCocomo_ExtraBaixo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_muitobaixo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO"), ",", ".") != AV59TFVariavelCocomo_MuitoBaixo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_muitobaixo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO"), ",", ".") != AV60TFVariavelCocomo_MuitoBaixo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_baixo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO"), ",", ".") != AV63TFVariavelCocomo_Baixo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_baixo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO_TO"), ",", ".") != AV64TFVariavelCocomo_Baixo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_nominal Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL"), ",", ".") != AV67TFVariavelCocomo_Nominal )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_nominal_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO"), ",", ".") != AV68TFVariavelCocomo_Nominal_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_alto Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO"), ",", ".") != AV71TFVariavelCocomo_Alto )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_alto_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO_TO"), ",", ".") != AV72TFVariavelCocomo_Alto_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_muitoalto Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO"), ",", ".") != AV75TFVariavelCocomo_MuitoAlto )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_muitoalto_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO"), ",", ".") != AV76TFVariavelCocomo_MuitoAlto_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_extraalto Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO"), ",", ".") != AV79TFVariavelCocomo_ExtraAlto )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfvariavelcocomo_extraalto_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO"), ",", ".") != AV80TFVariavelCocomo_ExtraAlto_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAGK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("VARIAVELCOCOMO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("VARIAVELCOCOMO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavVariavelcocomo_tipo1.Name = "vVARIAVELCOCOMO_TIPO1";
            cmbavVariavelcocomo_tipo1.WebTags = "";
            cmbavVariavelcocomo_tipo1.addItem("", "Todos", 0);
            cmbavVariavelcocomo_tipo1.addItem("E", "Escala", 0);
            cmbavVariavelcocomo_tipo1.addItem("M", "Multiplicador", 0);
            if ( cmbavVariavelcocomo_tipo1.ItemCount > 0 )
            {
               AV35VariavelCocomo_Tipo1 = cmbavVariavelcocomo_tipo1.getValidValue(AV35VariavelCocomo_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("VARIAVELCOCOMO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("VARIAVELCOCOMO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavVariavelcocomo_tipo2.Name = "vVARIAVELCOCOMO_TIPO2";
            cmbavVariavelcocomo_tipo2.WebTags = "";
            cmbavVariavelcocomo_tipo2.addItem("", "Todos", 0);
            cmbavVariavelcocomo_tipo2.addItem("E", "Escala", 0);
            cmbavVariavelcocomo_tipo2.addItem("M", "Multiplicador", 0);
            if ( cmbavVariavelcocomo_tipo2.ItemCount > 0 )
            {
               AV36VariavelCocomo_Tipo2 = cmbavVariavelcocomo_tipo2.getValidValue(AV36VariavelCocomo_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("VARIAVELCOCOMO_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector3.addItem("VARIAVELCOCOMO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavVariavelcocomo_tipo3.Name = "vVARIAVELCOCOMO_TIPO3";
            cmbavVariavelcocomo_tipo3.WebTags = "";
            cmbavVariavelcocomo_tipo3.addItem("", "Todos", 0);
            cmbavVariavelcocomo_tipo3.addItem("E", "Escala", 0);
            cmbavVariavelcocomo_tipo3.addItem("M", "Multiplicador", 0);
            if ( cmbavVariavelcocomo_tipo3.ItemCount > 0 )
            {
               AV37VariavelCocomo_Tipo3 = cmbavVariavelcocomo_tipo3.getValidValue(AV37VariavelCocomo_Tipo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
            }
            GXCCtl = "VARIAVELCOCOMO_TIPO_" + sGXsfl_77_idx;
            cmbVariavelCocomo_Tipo.Name = GXCCtl;
            cmbVariavelCocomo_Tipo.WebTags = "";
            cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
            cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
            cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
            if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
            {
               A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_772( ) ;
         while ( nGXsfl_77_idx <= nRC_GXsfl_77 )
         {
            sendrow_772( ) ;
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV32VariavelCocomo_Sigla1 ,
                                       String AV35VariavelCocomo_Tipo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV33VariavelCocomo_Sigla2 ,
                                       String AV36VariavelCocomo_Tipo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV34VariavelCocomo_Sigla3 ,
                                       String AV37VariavelCocomo_Tipo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV41TFVariavelCocomo_Sigla ,
                                       String AV42TFVariavelCocomo_Sigla_Sel ,
                                       DateTime AV49TFVariavelCocomo_Data ,
                                       DateTime AV50TFVariavelCocomo_Data_To ,
                                       decimal AV55TFVariavelCocomo_ExtraBaixo ,
                                       decimal AV56TFVariavelCocomo_ExtraBaixo_To ,
                                       decimal AV59TFVariavelCocomo_MuitoBaixo ,
                                       decimal AV60TFVariavelCocomo_MuitoBaixo_To ,
                                       decimal AV63TFVariavelCocomo_Baixo ,
                                       decimal AV64TFVariavelCocomo_Baixo_To ,
                                       decimal AV67TFVariavelCocomo_Nominal ,
                                       decimal AV68TFVariavelCocomo_Nominal_To ,
                                       decimal AV71TFVariavelCocomo_Alto ,
                                       decimal AV72TFVariavelCocomo_Alto_To ,
                                       decimal AV75TFVariavelCocomo_MuitoAlto ,
                                       decimal AV76TFVariavelCocomo_MuitoAlto_To ,
                                       decimal AV79TFVariavelCocomo_ExtraAlto ,
                                       decimal AV80TFVariavelCocomo_ExtraAlto_To ,
                                       String AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace ,
                                       String AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace ,
                                       String AV53ddo_VariavelCocomo_DataTitleControlIdToReplace ,
                                       String AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace ,
                                       String AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace ,
                                       String AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace ,
                                       String AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace ,
                                       String AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace ,
                                       String AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace ,
                                       String AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace ,
                                       int AV31VariavelCocomo_AreaTrabalhoCod ,
                                       IGxCollection AV46TFVariavelCocomo_Tipo_Sels ,
                                       String AV118Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A961VariavelCocomo_AreaTrabalhoCod ,
                                       String A962VariavelCocomo_Sigla ,
                                       String A964VariavelCocomo_Tipo ,
                                       short A992VariavelCocomo_Sequencial )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGK2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_SIGLA", StringUtil.RTrim( A962VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A964VariavelCocomo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_TIPO", StringUtil.RTrim( A964VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_DATA", GetSecureSignedToken( "", A966VariavelCocomo_Data));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_DATA", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRABAIXO", GetSecureSignedToken( "", context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_EXTRABAIXO", StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOBAIXO", GetSecureSignedToken( "", context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_MUITOBAIXO", StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_BAIXO", GetSecureSignedToken( "", context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_BAIXO", StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOMINAL", GetSecureSignedToken( "", context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_NOMINAL", StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_ALTO", GetSecureSignedToken( "", context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_ALTO", StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOALTO", GetSecureSignedToken( "", context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_MUITOALTO", StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 12, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRAALTO", GetSecureSignedToken( "", context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_EXTRAALTO", StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 12, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavVariavelcocomo_tipo1.ItemCount > 0 )
         {
            AV35VariavelCocomo_Tipo1 = cmbavVariavelcocomo_tipo1.getValidValue(AV35VariavelCocomo_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavVariavelcocomo_tipo2.ItemCount > 0 )
         {
            AV36VariavelCocomo_Tipo2 = cmbavVariavelcocomo_tipo2.getValidValue(AV36VariavelCocomo_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavVariavelcocomo_tipo3.ItemCount > 0 )
         {
            AV37VariavelCocomo_Tipo3 = cmbavVariavelcocomo_tipo3.getValidValue(AV37VariavelCocomo_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV118Pgmname = "WWVariaveisCocomo";
         context.Gx_err = 0;
      }

      protected void RFGK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 77;
         /* Execute user event: E33GK2 */
         E33GK2 ();
         nGXsfl_77_idx = 1;
         sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
         SubsflControlProps_772( ) ;
         nGXsfl_77_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_772( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A964VariavelCocomo_Tipo ,
                                                 AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                                 AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                                 AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                                 AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                                 AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                                 AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                                 AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                                 AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                                 AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                                 AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                                 AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                                 AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                                 AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                                 AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                                 AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels.Count ,
                                                 AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                                 AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                                 AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                                 AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                                 AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                                 AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                                 AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                                 AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                                 AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                                 AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                                 AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                                 AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                                 AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                                 AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                                 AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                                 AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                                 A962VariavelCocomo_Sigla ,
                                                 A966VariavelCocomo_Data ,
                                                 A986VariavelCocomo_ExtraBaixo ,
                                                 A967VariavelCocomo_MuitoBaixo ,
                                                 A968VariavelCocomo_Baixo ,
                                                 A969VariavelCocomo_Nominal ,
                                                 A970VariavelCocomo_Alto ,
                                                 A971VariavelCocomo_MuitoAlto ,
                                                 A972VariavelCocomo_ExtraAlto ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A961VariavelCocomo_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1), 15, "%");
            lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2), 15, "%");
            lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3), 15, "%");
            lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = StringUtil.PadR( StringUtil.RTrim( AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla), 15, "%");
            /* Using cursor H00GK2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1, AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1, lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2, AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2, lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3, AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3, lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla, AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel, AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data, AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to, AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo, AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to, AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo, AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to, AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo, AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to, AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal, AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to, AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto, AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to, AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto, AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to, AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto, AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_77_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A992VariavelCocomo_Sequencial = H00GK2_A992VariavelCocomo_Sequencial[0];
               A961VariavelCocomo_AreaTrabalhoCod = H00GK2_A961VariavelCocomo_AreaTrabalhoCod[0];
               A972VariavelCocomo_ExtraAlto = H00GK2_A972VariavelCocomo_ExtraAlto[0];
               n972VariavelCocomo_ExtraAlto = H00GK2_n972VariavelCocomo_ExtraAlto[0];
               A971VariavelCocomo_MuitoAlto = H00GK2_A971VariavelCocomo_MuitoAlto[0];
               n971VariavelCocomo_MuitoAlto = H00GK2_n971VariavelCocomo_MuitoAlto[0];
               A970VariavelCocomo_Alto = H00GK2_A970VariavelCocomo_Alto[0];
               n970VariavelCocomo_Alto = H00GK2_n970VariavelCocomo_Alto[0];
               A969VariavelCocomo_Nominal = H00GK2_A969VariavelCocomo_Nominal[0];
               n969VariavelCocomo_Nominal = H00GK2_n969VariavelCocomo_Nominal[0];
               A968VariavelCocomo_Baixo = H00GK2_A968VariavelCocomo_Baixo[0];
               n968VariavelCocomo_Baixo = H00GK2_n968VariavelCocomo_Baixo[0];
               A967VariavelCocomo_MuitoBaixo = H00GK2_A967VariavelCocomo_MuitoBaixo[0];
               n967VariavelCocomo_MuitoBaixo = H00GK2_n967VariavelCocomo_MuitoBaixo[0];
               A986VariavelCocomo_ExtraBaixo = H00GK2_A986VariavelCocomo_ExtraBaixo[0];
               n986VariavelCocomo_ExtraBaixo = H00GK2_n986VariavelCocomo_ExtraBaixo[0];
               A966VariavelCocomo_Data = H00GK2_A966VariavelCocomo_Data[0];
               n966VariavelCocomo_Data = H00GK2_n966VariavelCocomo_Data[0];
               A964VariavelCocomo_Tipo = H00GK2_A964VariavelCocomo_Tipo[0];
               A962VariavelCocomo_Sigla = H00GK2_A962VariavelCocomo_Sigla[0];
               /* Execute user event: E34GK2 */
               E34GK2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 77;
            WBGK0( ) ;
         }
         nGXsfl_77_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A964VariavelCocomo_Tipo ,
                                              AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                              AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                              AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                              AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                              AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                              AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                              AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                              AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                              AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                              AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                              AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                              AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                              AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                              AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                              AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels.Count ,
                                              AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                              AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                              AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                              AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                              AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                              AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                              AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                              AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                              AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                              AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                              AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                              AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                              AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                              AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                              AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                              AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                              A962VariavelCocomo_Sigla ,
                                              A966VariavelCocomo_Data ,
                                              A986VariavelCocomo_ExtraBaixo ,
                                              A967VariavelCocomo_MuitoBaixo ,
                                              A968VariavelCocomo_Baixo ,
                                              A969VariavelCocomo_Nominal ,
                                              A970VariavelCocomo_Alto ,
                                              A971VariavelCocomo_MuitoAlto ,
                                              A972VariavelCocomo_ExtraAlto ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A961VariavelCocomo_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1), 15, "%");
         lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2), 15, "%");
         lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = StringUtil.PadR( StringUtil.RTrim( AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3), 15, "%");
         lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = StringUtil.PadR( StringUtil.RTrim( AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla), 15, "%");
         /* Using cursor H00GK3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1, AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1, lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2, AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2, lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3, AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3, lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla, AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel, AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data, AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to, AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo, AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to, AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo, AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to, AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo, AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to, AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal, AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to, AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto, AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to, AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto, AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to, AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto, AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to});
         GRID_nRecordCount = H00GK3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGK0( )
      {
         /* Before Start, stand alone formulas. */
         AV118Pgmname = "WWVariaveisCocomo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E32GK2 */
         E32GK2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV82DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_SIGLATITLEFILTERDATA"), AV40VariavelCocomo_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_TIPOTITLEFILTERDATA"), AV44VariavelCocomo_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_DATATITLEFILTERDATA"), AV48VariavelCocomo_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA"), AV54VariavelCocomo_ExtraBaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA"), AV58VariavelCocomo_MuitoBaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA"), AV62VariavelCocomo_BaixoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA"), AV66VariavelCocomo_NominalTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_ALTOTITLEFILTERDATA"), AV70VariavelCocomo_AltoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA"), AV74VariavelCocomo_MuitoAltoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA"), AV78VariavelCocomo_ExtraAltoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavVariavelcocomo_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavVariavelcocomo_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVARIAVELCOCOMO_AREATRABALHOCOD");
               GX_FocusControl = edtavVariavelcocomo_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31VariavelCocomo_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV31VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavVariavelcocomo_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV32VariavelCocomo_Sigla1 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32VariavelCocomo_Sigla1", AV32VariavelCocomo_Sigla1);
            cmbavVariavelcocomo_tipo1.Name = cmbavVariavelcocomo_tipo1_Internalname;
            cmbavVariavelcocomo_tipo1.CurrentValue = cgiGet( cmbavVariavelcocomo_tipo1_Internalname);
            AV35VariavelCocomo_Tipo1 = cgiGet( cmbavVariavelcocomo_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV33VariavelCocomo_Sigla2 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33VariavelCocomo_Sigla2", AV33VariavelCocomo_Sigla2);
            cmbavVariavelcocomo_tipo2.Name = cmbavVariavelcocomo_tipo2_Internalname;
            cmbavVariavelcocomo_tipo2.CurrentValue = cgiGet( cmbavVariavelcocomo_tipo2_Internalname);
            AV36VariavelCocomo_Tipo2 = cgiGet( cmbavVariavelcocomo_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV34VariavelCocomo_Sigla3 = StringUtil.Upper( cgiGet( edtavVariavelcocomo_sigla3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34VariavelCocomo_Sigla3", AV34VariavelCocomo_Sigla3);
            cmbavVariavelcocomo_tipo3.Name = cmbavVariavelcocomo_tipo3_Internalname;
            cmbavVariavelcocomo_tipo3.CurrentValue = cgiGet( cmbavVariavelcocomo_tipo3_Internalname);
            AV37VariavelCocomo_Tipo3 = cgiGet( cmbavVariavelcocomo_tipo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV41TFVariavelCocomo_Sigla = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFVariavelCocomo_Sigla", AV41TFVariavelCocomo_Sigla);
            AV42TFVariavelCocomo_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfvariavelcocomo_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFVariavelCocomo_Sigla_Sel", AV42TFVariavelCocomo_Sigla_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfvariavelcocomo_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFVariavel Cocomo_Data"}), 1, "vTFVARIAVELCOCOMO_DATA");
               GX_FocusControl = edtavTfvariavelcocomo_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFVariavelCocomo_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
            }
            else
            {
               AV49TFVariavelCocomo_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfvariavelcocomo_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfvariavelcocomo_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFVariavel Cocomo_Data_To"}), 1, "vTFVARIAVELCOCOMO_DATA_TO");
               GX_FocusControl = edtavTfvariavelcocomo_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFVariavelCocomo_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
            }
            else
            {
               AV50TFVariavelCocomo_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfvariavelcocomo_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_variavelcocomo_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Variavel Cocomo_Data Aux Date"}), 1, "vDDO_VARIAVELCOCOMO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_variavelcocomo_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_VariavelCocomo_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_VariavelCocomo_DataAuxDate", context.localUtil.Format(AV51DDO_VariavelCocomo_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV51DDO_VariavelCocomo_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_variavelcocomo_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_VariavelCocomo_DataAuxDate", context.localUtil.Format(AV51DDO_VariavelCocomo_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_variavelcocomo_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Variavel Cocomo_Data Aux Date To"}), 1, "vDDO_VARIAVELCOCOMO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_variavelcocomo_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52DDO_VariavelCocomo_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_VariavelCocomo_DataAuxDateTo", context.localUtil.Format(AV52DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV52DDO_VariavelCocomo_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_variavelcocomo_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_VariavelCocomo_DataAuxDateTo", context.localUtil.Format(AV52DDO_VariavelCocomo_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRABAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_extrabaixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFVariavelCocomo_ExtraBaixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
            }
            else
            {
               AV55TFVariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRABAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_extrabaixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFVariavelCocomo_ExtraBaixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            }
            else
            {
               AV56TFVariavelCocomo_ExtraBaixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extrabaixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOBAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_muitobaixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFVariavelCocomo_MuitoBaixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
            }
            else
            {
               AV59TFVariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOBAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_muitobaixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFVariavelCocomo_MuitoBaixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            }
            else
            {
               AV60TFVariavelCocomo_MuitoBaixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitobaixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_BAIXO");
               GX_FocusControl = edtavTfvariavelcocomo_baixo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFVariavelCocomo_Baixo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
            }
            else
            {
               AV63TFVariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_BAIXO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_baixo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFVariavelCocomo_Baixo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
            }
            else
            {
               AV64TFVariavelCocomo_Baixo_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_baixo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_NOMINAL");
               GX_FocusControl = edtavTfvariavelcocomo_nominal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFVariavelCocomo_Nominal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
            }
            else
            {
               AV67TFVariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_NOMINAL_TO");
               GX_FocusControl = edtavTfvariavelcocomo_nominal_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFVariavelCocomo_Nominal_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
            }
            else
            {
               AV68TFVariavelCocomo_Nominal_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_nominal_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_ALTO");
               GX_FocusControl = edtavTfvariavelcocomo_alto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFVariavelCocomo_Alto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
            }
            else
            {
               AV71TFVariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_ALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_alto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFVariavelCocomo_Alto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
            }
            else
            {
               AV72TFVariavelCocomo_Alto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_alto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOALTO");
               GX_FocusControl = edtavTfvariavelcocomo_muitoalto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFVariavelCocomo_MuitoAlto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
            }
            else
            {
               AV75TFVariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_MUITOALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_muitoalto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFVariavelCocomo_MuitoAlto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            }
            else
            {
               AV76TFVariavelCocomo_MuitoAlto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_muitoalto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRAALTO");
               GX_FocusControl = edtavTfvariavelcocomo_extraalto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV79TFVariavelCocomo_ExtraAlto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
            }
            else
            {
               AV79TFVariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".") > 999999999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFVARIAVELCOCOMO_EXTRAALTO_TO");
               GX_FocusControl = edtavTfvariavelcocomo_extraalto_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFVariavelCocomo_ExtraAlto_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            }
            else
            {
               AV80TFVariavelCocomo_ExtraAlto_To = context.localUtil.CToN( cgiGet( edtavTfvariavelcocomo_extraalto_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            }
            AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
            AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace);
            AV53ddo_VariavelCocomo_DataTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_DataTitleControlIdToReplace", AV53ddo_VariavelCocomo_DataTitleControlIdToReplace);
            AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
            AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
            AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
            AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace);
            AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace);
            AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
            AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = cgiGet( edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_77 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_77"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_variavelcocomo_sigla_Caption = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Caption");
            Ddo_variavelcocomo_sigla_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Tooltip");
            Ddo_variavelcocomo_sigla_Cls = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Cls");
            Ddo_variavelcocomo_sigla_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_set");
            Ddo_variavelcocomo_sigla_Selectedvalue_set = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_set");
            Ddo_variavelcocomo_sigla_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Dropdownoptionstype");
            Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includesortasc"));
            Ddo_variavelcocomo_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includesortdsc"));
            Ddo_variavelcocomo_sigla_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortedstatus");
            Ddo_variavelcocomo_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includefilter"));
            Ddo_variavelcocomo_sigla_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filtertype");
            Ddo_variavelcocomo_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filterisrange"));
            Ddo_variavelcocomo_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Includedatalist"));
            Ddo_variavelcocomo_sigla_Datalisttype = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalisttype");
            Ddo_variavelcocomo_sigla_Datalistproc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalistproc");
            Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_variavelcocomo_sigla_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortasc");
            Ddo_variavelcocomo_sigla_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Sortdsc");
            Ddo_variavelcocomo_sigla_Loadingdata = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Loadingdata");
            Ddo_variavelcocomo_sigla_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Cleanfilter");
            Ddo_variavelcocomo_sigla_Noresultsfound = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Noresultsfound");
            Ddo_variavelcocomo_sigla_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Searchbuttontext");
            Ddo_variavelcocomo_tipo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Caption");
            Ddo_variavelcocomo_tipo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Tooltip");
            Ddo_variavelcocomo_tipo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Cls");
            Ddo_variavelcocomo_tipo_Selectedvalue_set = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_set");
            Ddo_variavelcocomo_tipo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Dropdownoptionstype");
            Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includesortasc"));
            Ddo_variavelcocomo_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includesortdsc"));
            Ddo_variavelcocomo_tipo_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortedstatus");
            Ddo_variavelcocomo_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includefilter"));
            Ddo_variavelcocomo_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Includedatalist"));
            Ddo_variavelcocomo_tipo_Datalisttype = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Datalisttype");
            Ddo_variavelcocomo_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Allowmultipleselection"));
            Ddo_variavelcocomo_tipo_Datalistfixedvalues = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Datalistfixedvalues");
            Ddo_variavelcocomo_tipo_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortasc");
            Ddo_variavelcocomo_tipo_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Sortdsc");
            Ddo_variavelcocomo_tipo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Cleanfilter");
            Ddo_variavelcocomo_tipo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Searchbuttontext");
            Ddo_variavelcocomo_data_Caption = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Caption");
            Ddo_variavelcocomo_data_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Tooltip");
            Ddo_variavelcocomo_data_Cls = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Cls");
            Ddo_variavelcocomo_data_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtext_set");
            Ddo_variavelcocomo_data_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_set");
            Ddo_variavelcocomo_data_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Dropdownoptionstype");
            Ddo_variavelcocomo_data_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includesortasc"));
            Ddo_variavelcocomo_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includesortdsc"));
            Ddo_variavelcocomo_data_Sortedstatus = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortedstatus");
            Ddo_variavelcocomo_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includefilter"));
            Ddo_variavelcocomo_data_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filtertype");
            Ddo_variavelcocomo_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filterisrange"));
            Ddo_variavelcocomo_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_DATA_Includedatalist"));
            Ddo_variavelcocomo_data_Sortasc = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortasc");
            Ddo_variavelcocomo_data_Sortdsc = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Sortdsc");
            Ddo_variavelcocomo_data_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Cleanfilter");
            Ddo_variavelcocomo_data_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Rangefilterfrom");
            Ddo_variavelcocomo_data_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Rangefilterto");
            Ddo_variavelcocomo_data_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Searchbuttontext");
            Ddo_variavelcocomo_extrabaixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Caption");
            Ddo_variavelcocomo_extrabaixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Tooltip");
            Ddo_variavelcocomo_extrabaixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cls");
            Ddo_variavelcocomo_extrabaixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_set");
            Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_extrabaixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortasc"));
            Ddo_variavelcocomo_extrabaixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includesortdsc"));
            Ddo_variavelcocomo_extrabaixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includefilter"));
            Ddo_variavelcocomo_extrabaixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filtertype");
            Ddo_variavelcocomo_extrabaixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filterisrange"));
            Ddo_variavelcocomo_extrabaixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Includedatalist"));
            Ddo_variavelcocomo_extrabaixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Cleanfilter");
            Ddo_variavelcocomo_extrabaixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_extrabaixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Rangefilterto");
            Ddo_variavelcocomo_extrabaixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Searchbuttontext");
            Ddo_variavelcocomo_muitobaixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Caption");
            Ddo_variavelcocomo_muitobaixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Tooltip");
            Ddo_variavelcocomo_muitobaixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cls");
            Ddo_variavelcocomo_muitobaixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_set");
            Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_muitobaixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortasc"));
            Ddo_variavelcocomo_muitobaixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includesortdsc"));
            Ddo_variavelcocomo_muitobaixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includefilter"));
            Ddo_variavelcocomo_muitobaixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filtertype");
            Ddo_variavelcocomo_muitobaixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filterisrange"));
            Ddo_variavelcocomo_muitobaixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Includedatalist"));
            Ddo_variavelcocomo_muitobaixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Cleanfilter");
            Ddo_variavelcocomo_muitobaixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_muitobaixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Rangefilterto");
            Ddo_variavelcocomo_muitobaixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Searchbuttontext");
            Ddo_variavelcocomo_baixo_Caption = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Caption");
            Ddo_variavelcocomo_baixo_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Tooltip");
            Ddo_variavelcocomo_baixo_Cls = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Cls");
            Ddo_variavelcocomo_baixo_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_set");
            Ddo_variavelcocomo_baixo_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_set");
            Ddo_variavelcocomo_baixo_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Dropdownoptionstype");
            Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_baixo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includesortasc"));
            Ddo_variavelcocomo_baixo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includesortdsc"));
            Ddo_variavelcocomo_baixo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includefilter"));
            Ddo_variavelcocomo_baixo_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filtertype");
            Ddo_variavelcocomo_baixo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filterisrange"));
            Ddo_variavelcocomo_baixo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Includedatalist"));
            Ddo_variavelcocomo_baixo_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Cleanfilter");
            Ddo_variavelcocomo_baixo_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterfrom");
            Ddo_variavelcocomo_baixo_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Rangefilterto");
            Ddo_variavelcocomo_baixo_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Searchbuttontext");
            Ddo_variavelcocomo_nominal_Caption = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Caption");
            Ddo_variavelcocomo_nominal_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Tooltip");
            Ddo_variavelcocomo_nominal_Cls = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Cls");
            Ddo_variavelcocomo_nominal_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_set");
            Ddo_variavelcocomo_nominal_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_set");
            Ddo_variavelcocomo_nominal_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Dropdownoptionstype");
            Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_nominal_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includesortasc"));
            Ddo_variavelcocomo_nominal_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includesortdsc"));
            Ddo_variavelcocomo_nominal_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includefilter"));
            Ddo_variavelcocomo_nominal_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filtertype");
            Ddo_variavelcocomo_nominal_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filterisrange"));
            Ddo_variavelcocomo_nominal_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Includedatalist"));
            Ddo_variavelcocomo_nominal_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Cleanfilter");
            Ddo_variavelcocomo_nominal_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterfrom");
            Ddo_variavelcocomo_nominal_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Rangefilterto");
            Ddo_variavelcocomo_nominal_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Searchbuttontext");
            Ddo_variavelcocomo_alto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Caption");
            Ddo_variavelcocomo_alto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Tooltip");
            Ddo_variavelcocomo_alto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Cls");
            Ddo_variavelcocomo_alto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_set");
            Ddo_variavelcocomo_alto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_set");
            Ddo_variavelcocomo_alto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_alto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includesortasc"));
            Ddo_variavelcocomo_alto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includesortdsc"));
            Ddo_variavelcocomo_alto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includefilter"));
            Ddo_variavelcocomo_alto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filtertype");
            Ddo_variavelcocomo_alto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filterisrange"));
            Ddo_variavelcocomo_alto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Includedatalist"));
            Ddo_variavelcocomo_alto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Cleanfilter");
            Ddo_variavelcocomo_alto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Rangefilterfrom");
            Ddo_variavelcocomo_alto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Rangefilterto");
            Ddo_variavelcocomo_alto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Searchbuttontext");
            Ddo_variavelcocomo_muitoalto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Caption");
            Ddo_variavelcocomo_muitoalto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Tooltip");
            Ddo_variavelcocomo_muitoalto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Cls");
            Ddo_variavelcocomo_muitoalto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_set");
            Ddo_variavelcocomo_muitoalto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_set");
            Ddo_variavelcocomo_muitoalto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_muitoalto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortasc"));
            Ddo_variavelcocomo_muitoalto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includesortdsc"));
            Ddo_variavelcocomo_muitoalto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includefilter"));
            Ddo_variavelcocomo_muitoalto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filtertype");
            Ddo_variavelcocomo_muitoalto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filterisrange"));
            Ddo_variavelcocomo_muitoalto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Includedatalist"));
            Ddo_variavelcocomo_muitoalto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Cleanfilter");
            Ddo_variavelcocomo_muitoalto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterfrom");
            Ddo_variavelcocomo_muitoalto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Rangefilterto");
            Ddo_variavelcocomo_muitoalto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Searchbuttontext");
            Ddo_variavelcocomo_extraalto_Caption = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Caption");
            Ddo_variavelcocomo_extraalto_Tooltip = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Tooltip");
            Ddo_variavelcocomo_extraalto_Cls = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Cls");
            Ddo_variavelcocomo_extraalto_Filteredtext_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_set");
            Ddo_variavelcocomo_extraalto_Filteredtextto_set = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_set");
            Ddo_variavelcocomo_extraalto_Dropdownoptionstype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Dropdownoptionstype");
            Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Titlecontrolidtoreplace");
            Ddo_variavelcocomo_extraalto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortasc"));
            Ddo_variavelcocomo_extraalto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includesortdsc"));
            Ddo_variavelcocomo_extraalto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includefilter"));
            Ddo_variavelcocomo_extraalto_Filtertype = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filtertype");
            Ddo_variavelcocomo_extraalto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filterisrange"));
            Ddo_variavelcocomo_extraalto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Includedatalist"));
            Ddo_variavelcocomo_extraalto_Cleanfilter = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Cleanfilter");
            Ddo_variavelcocomo_extraalto_Rangefilterfrom = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterfrom");
            Ddo_variavelcocomo_extraalto_Rangefilterto = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Rangefilterto");
            Ddo_variavelcocomo_extraalto_Searchbuttontext = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Searchbuttontext");
            Ddo_variavelcocomo_sigla_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Activeeventkey");
            Ddo_variavelcocomo_sigla_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Filteredtext_get");
            Ddo_variavelcocomo_sigla_Selectedvalue_get = cgiGet( "DDO_VARIAVELCOCOMO_SIGLA_Selectedvalue_get");
            Ddo_variavelcocomo_tipo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Activeeventkey");
            Ddo_variavelcocomo_tipo_Selectedvalue_get = cgiGet( "DDO_VARIAVELCOCOMO_TIPO_Selectedvalue_get");
            Ddo_variavelcocomo_data_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Activeeventkey");
            Ddo_variavelcocomo_data_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtext_get");
            Ddo_variavelcocomo_data_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_DATA_Filteredtextto_get");
            Ddo_variavelcocomo_extrabaixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Activeeventkey");
            Ddo_variavelcocomo_extrabaixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtext_get");
            Ddo_variavelcocomo_extrabaixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRABAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_muitobaixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Activeeventkey");
            Ddo_variavelcocomo_muitobaixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtext_get");
            Ddo_variavelcocomo_muitobaixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOBAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_baixo_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Activeeventkey");
            Ddo_variavelcocomo_baixo_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtext_get");
            Ddo_variavelcocomo_baixo_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_BAIXO_Filteredtextto_get");
            Ddo_variavelcocomo_nominal_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Activeeventkey");
            Ddo_variavelcocomo_nominal_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtext_get");
            Ddo_variavelcocomo_nominal_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_NOMINAL_Filteredtextto_get");
            Ddo_variavelcocomo_alto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Activeeventkey");
            Ddo_variavelcocomo_alto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtext_get");
            Ddo_variavelcocomo_alto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_ALTO_Filteredtextto_get");
            Ddo_variavelcocomo_muitoalto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Activeeventkey");
            Ddo_variavelcocomo_muitoalto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtext_get");
            Ddo_variavelcocomo_muitoalto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_MUITOALTO_Filteredtextto_get");
            Ddo_variavelcocomo_extraalto_Activeeventkey = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Activeeventkey");
            Ddo_variavelcocomo_extraalto_Filteredtext_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtext_get");
            Ddo_variavelcocomo_extraalto_Filteredtextto_get = cgiGet( "DDO_VARIAVELCOCOMO_EXTRAALTO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA1"), AV32VariavelCocomo_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO1"), AV35VariavelCocomo_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA2"), AV33VariavelCocomo_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO2"), AV36VariavelCocomo_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_SIGLA3"), AV34VariavelCocomo_Sigla3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vVARIAVELCOCOMO_TIPO3"), AV37VariavelCocomo_Tipo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA"), AV41TFVariavelCocomo_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFVARIAVELCOCOMO_SIGLA_SEL"), AV42TFVariavelCocomo_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA"), 0) != AV49TFVariavelCocomo_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFVARIAVELCOCOMO_DATA_TO"), 0) != AV50TFVariavelCocomo_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO"), ",", ".") != AV55TFVariavelCocomo_ExtraBaixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRABAIXO_TO"), ",", ".") != AV56TFVariavelCocomo_ExtraBaixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO"), ",", ".") != AV59TFVariavelCocomo_MuitoBaixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOBAIXO_TO"), ",", ".") != AV60TFVariavelCocomo_MuitoBaixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO"), ",", ".") != AV63TFVariavelCocomo_Baixo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_BAIXO_TO"), ",", ".") != AV64TFVariavelCocomo_Baixo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL"), ",", ".") != AV67TFVariavelCocomo_Nominal )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_NOMINAL_TO"), ",", ".") != AV68TFVariavelCocomo_Nominal_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO"), ",", ".") != AV71TFVariavelCocomo_Alto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_ALTO_TO"), ",", ".") != AV72TFVariavelCocomo_Alto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO"), ",", ".") != AV75TFVariavelCocomo_MuitoAlto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_MUITOALTO_TO"), ",", ".") != AV76TFVariavelCocomo_MuitoAlto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO"), ",", ".") != AV79TFVariavelCocomo_ExtraAlto )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFVARIAVELCOCOMO_EXTRAALTO_TO"), ",", ".") != AV80TFVariavelCocomo_ExtraAlto_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E32GK2 */
         E32GK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E32GK2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV35VariavelCocomo_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
         AV15DynamicFiltersSelector1 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36VariavelCocomo_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
         AV19DynamicFiltersSelector2 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37VariavelCocomo_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
         AV23DynamicFiltersSelector3 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfvariavelcocomo_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_sigla_Visible), 5, 0)));
         edtavTfvariavelcocomo_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_sigla_sel_Visible), 5, 0)));
         edtavTfvariavelcocomo_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_data_Visible), 5, 0)));
         edtavTfvariavelcocomo_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_data_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_extrabaixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extrabaixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extrabaixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_extrabaixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extrabaixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extrabaixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitobaixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitobaixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitobaixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitobaixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitobaixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitobaixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_baixo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_baixo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_baixo_Visible), 5, 0)));
         edtavTfvariavelcocomo_baixo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_baixo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_baixo_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_nominal_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nominal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nominal_Visible), 5, 0)));
         edtavTfvariavelcocomo_nominal_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_nominal_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_nominal_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_alto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_alto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_alto_Visible), 5, 0)));
         edtavTfvariavelcocomo_alto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_alto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_alto_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitoalto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitoalto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitoalto_Visible), 5, 0)));
         edtavTfvariavelcocomo_muitoalto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_muitoalto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_muitoalto_to_Visible), 5, 0)));
         edtavTfvariavelcocomo_extraalto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extraalto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extraalto_Visible), 5, 0)));
         edtavTfvariavelcocomo_extraalto_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfvariavelcocomo_extraalto_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfvariavelcocomo_extraalto_to_Visible), 5, 0)));
         Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace);
         AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace = Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace", AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace);
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace);
         AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace = Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace", AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_data_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_data_Titlecontrolidtoreplace);
         AV53ddo_VariavelCocomo_DataTitleControlIdToReplace = Ddo_variavelcocomo_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_VariavelCocomo_DataTitleControlIdToReplace", AV53ddo_VariavelCocomo_DataTitleControlIdToReplace);
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_ExtraBaixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace);
         AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace", AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_MuitoBaixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace);
         AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace", AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Baixo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace);
         AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace = Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace", AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Nominal";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace);
         AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace = Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace", AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace);
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_Alto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_alto_Titlecontrolidtoreplace);
         AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace = Ddo_variavelcocomo_alto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace", AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_MuitoAlto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace);
         AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace", AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = subGrid_Internalname+"_VariavelCocomo_ExtraAlto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "TitleControlIdToReplace", Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace);
         AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace", AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace);
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Variaveis Cocomo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Data", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV82DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV82DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E33GK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV40VariavelCocomo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44VariavelCocomo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48VariavelCocomo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54VariavelCocomo_ExtraBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58VariavelCocomo_MuitoBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62VariavelCocomo_BaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66VariavelCocomo_NominalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70VariavelCocomo_AltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74VariavelCocomo_MuitoAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78VariavelCocomo_ExtraAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtVariavelCocomo_Sigla_Titleformat = 2;
         edtVariavelCocomo_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Title", edtVariavelCocomo_Sigla_Title);
         cmbVariavelCocomo_Tipo_Titleformat = 2;
         cmbVariavelCocomo_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Title", cmbVariavelCocomo_Tipo.Title.Text);
         edtVariavelCocomo_Data_Titleformat = 2;
         edtVariavelCocomo_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Data_Internalname, "Title", edtVariavelCocomo_Data_Title);
         edtVariavelCocomo_ExtraBaixo_Titleformat = 2;
         edtVariavelCocomo_ExtraBaixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Extra Baixo", AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraBaixo_Internalname, "Title", edtVariavelCocomo_ExtraBaixo_Title);
         edtVariavelCocomo_MuitoBaixo_Titleformat = 2;
         edtVariavelCocomo_MuitoBaixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Muito Baixo", AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoBaixo_Internalname, "Title", edtVariavelCocomo_MuitoBaixo_Title);
         edtVariavelCocomo_Baixo_Titleformat = 2;
         edtVariavelCocomo_Baixo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Baixo", AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Baixo_Internalname, "Title", edtVariavelCocomo_Baixo_Title);
         edtVariavelCocomo_Nominal_Titleformat = 2;
         edtVariavelCocomo_Nominal_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nominal", AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Nominal_Internalname, "Title", edtVariavelCocomo_Nominal_Title);
         edtVariavelCocomo_Alto_Titleformat = 2;
         edtVariavelCocomo_Alto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Alto", AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Alto_Internalname, "Title", edtVariavelCocomo_Alto_Title);
         edtVariavelCocomo_MuitoAlto_Titleformat = 2;
         edtVariavelCocomo_MuitoAlto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Muito Alto", AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoAlto_Internalname, "Title", edtVariavelCocomo_MuitoAlto_Title);
         edtVariavelCocomo_ExtraAlto_Titleformat = 2;
         edtVariavelCocomo_ExtraAlto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Extra Alto", AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraAlto_Internalname, "Title", edtVariavelCocomo_ExtraAlto_Title);
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod = AV31VariavelCocomo_AreaTrabalhoCod;
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = AV32VariavelCocomo_Sigla1;
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = AV35VariavelCocomo_Tipo1;
         AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = AV33VariavelCocomo_Sigla2;
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = AV36VariavelCocomo_Tipo2;
         AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = AV34VariavelCocomo_Sigla3;
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = AV37VariavelCocomo_Tipo3;
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = AV41TFVariavelCocomo_Sigla;
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = AV42TFVariavelCocomo_Sigla_Sel;
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = AV46TFVariavelCocomo_Tipo_Sels;
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = AV49TFVariavelCocomo_Data;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = AV50TFVariavelCocomo_Data_To;
         AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo = AV55TFVariavelCocomo_ExtraBaixo;
         AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to = AV56TFVariavelCocomo_ExtraBaixo_To;
         AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo = AV59TFVariavelCocomo_MuitoBaixo;
         AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to = AV60TFVariavelCocomo_MuitoBaixo_To;
         AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo = AV63TFVariavelCocomo_Baixo;
         AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to = AV64TFVariavelCocomo_Baixo_To;
         AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal = AV67TFVariavelCocomo_Nominal;
         AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to = AV68TFVariavelCocomo_Nominal_To;
         AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto = AV71TFVariavelCocomo_Alto;
         AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to = AV72TFVariavelCocomo_Alto_To;
         AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto = AV75TFVariavelCocomo_MuitoAlto;
         AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to = AV76TFVariavelCocomo_MuitoAlto_To;
         AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto = AV79TFVariavelCocomo_ExtraAlto;
         AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to = AV80TFVariavelCocomo_ExtraAlto_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40VariavelCocomo_SiglaTitleFilterData", AV40VariavelCocomo_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44VariavelCocomo_TipoTitleFilterData", AV44VariavelCocomo_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48VariavelCocomo_DataTitleFilterData", AV48VariavelCocomo_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54VariavelCocomo_ExtraBaixoTitleFilterData", AV54VariavelCocomo_ExtraBaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58VariavelCocomo_MuitoBaixoTitleFilterData", AV58VariavelCocomo_MuitoBaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62VariavelCocomo_BaixoTitleFilterData", AV62VariavelCocomo_BaixoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66VariavelCocomo_NominalTitleFilterData", AV66VariavelCocomo_NominalTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70VariavelCocomo_AltoTitleFilterData", AV70VariavelCocomo_AltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74VariavelCocomo_MuitoAltoTitleFilterData", AV74VariavelCocomo_MuitoAltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78VariavelCocomo_ExtraAltoTitleFilterData", AV78VariavelCocomo_ExtraAltoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11GK2( )
      {
         /* Ddo_variavelcocomo_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFVariavelCocomo_Sigla = Ddo_variavelcocomo_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFVariavelCocomo_Sigla", AV41TFVariavelCocomo_Sigla);
            AV42TFVariavelCocomo_Sigla_Sel = Ddo_variavelcocomo_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFVariavelCocomo_Sigla_Sel", AV42TFVariavelCocomo_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E12GK2( )
      {
         /* Ddo_variavelcocomo_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFVariavelCocomo_Tipo_SelsJson = Ddo_variavelcocomo_tipo_Selectedvalue_get;
            AV46TFVariavelCocomo_Tipo_Sels.FromJSonString(AV45TFVariavelCocomo_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46TFVariavelCocomo_Tipo_Sels", AV46TFVariavelCocomo_Tipo_Sels);
      }

      protected void E13GK2( )
      {
         /* Ddo_variavelcocomo_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_variavelcocomo_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_variavelcocomo_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFVariavelCocomo_Data = context.localUtil.CToD( Ddo_variavelcocomo_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
            AV50TFVariavelCocomo_Data_To = context.localUtil.CToD( Ddo_variavelcocomo_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14GK2( )
      {
         /* Ddo_variavelcocomo_extrabaixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_extrabaixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( Ddo_variavelcocomo_extrabaixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
            AV56TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( Ddo_variavelcocomo_extrabaixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15GK2( )
      {
         /* Ddo_variavelcocomo_muitobaixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitobaixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( Ddo_variavelcocomo_muitobaixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
            AV60TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( Ddo_variavelcocomo_muitobaixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16GK2( )
      {
         /* Ddo_variavelcocomo_baixo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_baixo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFVariavelCocomo_Baixo = NumberUtil.Val( Ddo_variavelcocomo_baixo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
            AV64TFVariavelCocomo_Baixo_To = NumberUtil.Val( Ddo_variavelcocomo_baixo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E17GK2( )
      {
         /* Ddo_variavelcocomo_nominal_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_nominal_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFVariavelCocomo_Nominal = NumberUtil.Val( Ddo_variavelcocomo_nominal_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
            AV68TFVariavelCocomo_Nominal_To = NumberUtil.Val( Ddo_variavelcocomo_nominal_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18GK2( )
      {
         /* Ddo_variavelcocomo_alto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_alto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFVariavelCocomo_Alto = NumberUtil.Val( Ddo_variavelcocomo_alto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
            AV72TFVariavelCocomo_Alto_To = NumberUtil.Val( Ddo_variavelcocomo_alto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E19GK2( )
      {
         /* Ddo_variavelcocomo_muitoalto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_muitoalto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFVariavelCocomo_MuitoAlto = NumberUtil.Val( Ddo_variavelcocomo_muitoalto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
            AV76TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( Ddo_variavelcocomo_muitoalto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E20GK2( )
      {
         /* Ddo_variavelcocomo_extraalto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_variavelcocomo_extraalto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFVariavelCocomo_ExtraAlto = NumberUtil.Val( Ddo_variavelcocomo_extraalto_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
            AV80TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( Ddo_variavelcocomo_extraalto_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
            subgrid_firstpage( ) ;
         }
      }

      private void E34GK2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV116Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A961VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A962VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(A964VariavelCocomo_Tipo)) + "," + UrlEncode("" +A992VariavelCocomo_Sequencial);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV117Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A961VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A962VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(A964VariavelCocomo_Tipo)) + "," + UrlEncode("" +A992VariavelCocomo_Sequencial);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 77;
         }
         sendrow_772( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_77_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(77, GridRow);
         }
      }

      protected void E21GK2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E27GK2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E22GK2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo1.CurrentValue = StringUtil.RTrim( AV35VariavelCocomo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Values", cmbavVariavelcocomo_tipo1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo2.CurrentValue = StringUtil.RTrim( AV36VariavelCocomo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Values", cmbavVariavelcocomo_tipo2.ToJavascriptSource());
         cmbavVariavelcocomo_tipo3.CurrentValue = StringUtil.RTrim( AV37VariavelCocomo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Values", cmbavVariavelcocomo_tipo3.ToJavascriptSource());
      }

      protected void E28GK2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29GK2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E23GK2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo1.CurrentValue = StringUtil.RTrim( AV35VariavelCocomo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Values", cmbavVariavelcocomo_tipo1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo2.CurrentValue = StringUtil.RTrim( AV36VariavelCocomo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Values", cmbavVariavelcocomo_tipo2.ToJavascriptSource());
         cmbavVariavelcocomo_tipo3.CurrentValue = StringUtil.RTrim( AV37VariavelCocomo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Values", cmbavVariavelcocomo_tipo3.ToJavascriptSource());
      }

      protected void E30GK2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24GK2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV32VariavelCocomo_Sigla1, AV35VariavelCocomo_Tipo1, AV19DynamicFiltersSelector2, AV33VariavelCocomo_Sigla2, AV36VariavelCocomo_Tipo2, AV23DynamicFiltersSelector3, AV34VariavelCocomo_Sigla3, AV37VariavelCocomo_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFVariavelCocomo_Sigla, AV42TFVariavelCocomo_Sigla_Sel, AV49TFVariavelCocomo_Data, AV50TFVariavelCocomo_Data_To, AV55TFVariavelCocomo_ExtraBaixo, AV56TFVariavelCocomo_ExtraBaixo_To, AV59TFVariavelCocomo_MuitoBaixo, AV60TFVariavelCocomo_MuitoBaixo_To, AV63TFVariavelCocomo_Baixo, AV64TFVariavelCocomo_Baixo_To, AV67TFVariavelCocomo_Nominal, AV68TFVariavelCocomo_Nominal_To, AV71TFVariavelCocomo_Alto, AV72TFVariavelCocomo_Alto_To, AV75TFVariavelCocomo_MuitoAlto, AV76TFVariavelCocomo_MuitoAlto_To, AV79TFVariavelCocomo_ExtraAlto, AV80TFVariavelCocomo_ExtraAlto_To, AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace, AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace, AV53ddo_VariavelCocomo_DataTitleControlIdToReplace, AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace, AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace, AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace, AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace, AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace, AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace, AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace, AV31VariavelCocomo_AreaTrabalhoCod, AV46TFVariavelCocomo_Tipo_Sels, AV118Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo1.CurrentValue = StringUtil.RTrim( AV35VariavelCocomo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Values", cmbavVariavelcocomo_tipo1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo2.CurrentValue = StringUtil.RTrim( AV36VariavelCocomo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Values", cmbavVariavelcocomo_tipo2.ToJavascriptSource());
         cmbavVariavelcocomo_tipo3.CurrentValue = StringUtil.RTrim( AV37VariavelCocomo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Values", cmbavVariavelcocomo_tipo3.ToJavascriptSource());
      }

      protected void E31GK2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25GK2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46TFVariavelCocomo_Tipo_Sels", AV46TFVariavelCocomo_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavVariavelcocomo_tipo1.CurrentValue = StringUtil.RTrim( AV35VariavelCocomo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Values", cmbavVariavelcocomo_tipo1.ToJavascriptSource());
         cmbavVariavelcocomo_tipo2.CurrentValue = StringUtil.RTrim( AV36VariavelCocomo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Values", cmbavVariavelcocomo_tipo2.ToJavascriptSource());
         cmbavVariavelcocomo_tipo3.CurrentValue = StringUtil.RTrim( AV37VariavelCocomo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Values", cmbavVariavelcocomo_tipo3.ToJavascriptSource());
      }

      protected void E26GK2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_variavelcocomo_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
         Ddo_variavelcocomo_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
         Ddo_variavelcocomo_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_variavelcocomo_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SortedStatus", Ddo_variavelcocomo_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_variavelcocomo_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SortedStatus", Ddo_variavelcocomo_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_variavelcocomo_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "SortedStatus", Ddo_variavelcocomo_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavVariavelcocomo_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla1_Visible), 5, 0)));
         cmbavVariavelcocomo_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_SIGLA") == 0 )
         {
            edtavVariavelcocomo_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_TIPO") == 0 )
         {
            cmbavVariavelcocomo_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavVariavelcocomo_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla2_Visible), 5, 0)));
         cmbavVariavelcocomo_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_SIGLA") == 0 )
         {
            edtavVariavelcocomo_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_TIPO") == 0 )
         {
            cmbavVariavelcocomo_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavVariavelcocomo_sigla3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla3_Visible), 5, 0)));
         cmbavVariavelcocomo_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_SIGLA") == 0 )
         {
            edtavVariavelcocomo_sigla3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVariavelcocomo_sigla3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVariavelcocomo_sigla3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_TIPO") == 0 )
         {
            cmbavVariavelcocomo_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavVariavelcocomo_tipo3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV33VariavelCocomo_Sigla2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33VariavelCocomo_Sigla2", AV33VariavelCocomo_Sigla2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV34VariavelCocomo_Sigla3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34VariavelCocomo_Sigla3", AV34VariavelCocomo_Sigla3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31VariavelCocomo_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         AV41TFVariavelCocomo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFVariavelCocomo_Sigla", AV41TFVariavelCocomo_Sigla);
         Ddo_variavelcocomo_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "FilteredText_set", Ddo_variavelcocomo_sigla_Filteredtext_set);
         AV42TFVariavelCocomo_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFVariavelCocomo_Sigla_Sel", AV42TFVariavelCocomo_Sigla_Sel);
         Ddo_variavelcocomo_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SelectedValue_set", Ddo_variavelcocomo_sigla_Selectedvalue_set);
         AV46TFVariavelCocomo_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_variavelcocomo_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SelectedValue_set", Ddo_variavelcocomo_tipo_Selectedvalue_set);
         AV49TFVariavelCocomo_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
         Ddo_variavelcocomo_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredText_set", Ddo_variavelcocomo_data_Filteredtext_set);
         AV50TFVariavelCocomo_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
         Ddo_variavelcocomo_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_data_Filteredtextto_set);
         AV55TFVariavelCocomo_ExtraBaixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
         Ddo_variavelcocomo_extrabaixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_extrabaixo_Filteredtext_set);
         AV56TFVariavelCocomo_ExtraBaixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extrabaixo_Filteredtextto_set);
         AV59TFVariavelCocomo_MuitoBaixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
         Ddo_variavelcocomo_muitobaixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitobaixo_Filteredtext_set);
         AV60TFVariavelCocomo_MuitoBaixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitobaixo_Filteredtextto_set);
         AV63TFVariavelCocomo_Baixo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
         Ddo_variavelcocomo_baixo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_baixo_Filteredtext_set);
         AV64TFVariavelCocomo_Baixo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
         Ddo_variavelcocomo_baixo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_baixo_Filteredtextto_set);
         AV67TFVariavelCocomo_Nominal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
         Ddo_variavelcocomo_nominal_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredText_set", Ddo_variavelcocomo_nominal_Filteredtext_set);
         AV68TFVariavelCocomo_Nominal_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
         Ddo_variavelcocomo_nominal_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_nominal_Filteredtextto_set);
         AV71TFVariavelCocomo_Alto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
         Ddo_variavelcocomo_alto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredText_set", Ddo_variavelcocomo_alto_Filteredtext_set);
         AV72TFVariavelCocomo_Alto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
         Ddo_variavelcocomo_alto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_alto_Filteredtextto_set);
         AV75TFVariavelCocomo_MuitoAlto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
         Ddo_variavelcocomo_muitoalto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitoalto_Filteredtext_set);
         AV76TFVariavelCocomo_MuitoAlto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
         Ddo_variavelcocomo_muitoalto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitoalto_Filteredtextto_set);
         AV79TFVariavelCocomo_ExtraAlto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
         Ddo_variavelcocomo_extraalto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_extraalto_Filteredtext_set);
         AV80TFVariavelCocomo_ExtraAlto_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
         Ddo_variavelcocomo_extraalto_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extraalto_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "VARIAVELCOCOMO_SIGLA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV32VariavelCocomo_Sigla1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32VariavelCocomo_Sigla1", AV32VariavelCocomo_Sigla1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV118Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV118Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV118Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV119GXV1 = 1;
         while ( AV119GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV119GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "VARIAVELCOCOMO_AREATRABALHOCOD") == 0 )
            {
               AV31VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA") == 0 )
            {
               AV41TFVariavelCocomo_Sigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFVariavelCocomo_Sigla", AV41TFVariavelCocomo_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFVariavelCocomo_Sigla)) )
               {
                  Ddo_variavelcocomo_sigla_Filteredtext_set = AV41TFVariavelCocomo_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "FilteredText_set", Ddo_variavelcocomo_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA_SEL") == 0 )
            {
               AV42TFVariavelCocomo_Sigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFVariavelCocomo_Sigla_Sel", AV42TFVariavelCocomo_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFVariavelCocomo_Sigla_Sel)) )
               {
                  Ddo_variavelcocomo_sigla_Selectedvalue_set = AV42TFVariavelCocomo_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_sigla_Internalname, "SelectedValue_set", Ddo_variavelcocomo_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_TIPO_SEL") == 0 )
            {
               AV45TFVariavelCocomo_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV46TFVariavelCocomo_Tipo_Sels.FromJSonString(AV45TFVariavelCocomo_Tipo_SelsJson);
               if ( ! ( AV46TFVariavelCocomo_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_variavelcocomo_tipo_Selectedvalue_set = AV45TFVariavelCocomo_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_tipo_Internalname, "SelectedValue_set", Ddo_variavelcocomo_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_DATA") == 0 )
            {
               AV49TFVariavelCocomo_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFVariavelCocomo_Data", context.localUtil.Format(AV49TFVariavelCocomo_Data, "99/99/99"));
               AV50TFVariavelCocomo_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFVariavelCocomo_Data_To", context.localUtil.Format(AV50TFVariavelCocomo_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV49TFVariavelCocomo_Data) )
               {
                  Ddo_variavelcocomo_data_Filteredtext_set = context.localUtil.DToC( AV49TFVariavelCocomo_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredText_set", Ddo_variavelcocomo_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV50TFVariavelCocomo_Data_To) )
               {
                  Ddo_variavelcocomo_data_Filteredtextto_set = context.localUtil.DToC( AV50TFVariavelCocomo_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_data_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRABAIXO") == 0 )
            {
               AV55TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFVariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2)));
               AV56TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFVariavelCocomo_ExtraBaixo_To", StringUtil.LTrim( StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV55TFVariavelCocomo_ExtraBaixo) )
               {
                  Ddo_variavelcocomo_extrabaixo_Filteredtext_set = StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_extrabaixo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV56TFVariavelCocomo_ExtraBaixo_To) )
               {
                  Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extrabaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extrabaixo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOBAIXO") == 0 )
            {
               AV59TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFVariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2)));
               AV60TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFVariavelCocomo_MuitoBaixo_To", StringUtil.LTrim( StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV59TFVariavelCocomo_MuitoBaixo) )
               {
                  Ddo_variavelcocomo_muitobaixo_Filteredtext_set = StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitobaixo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV60TFVariavelCocomo_MuitoBaixo_To) )
               {
                  Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitobaixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitobaixo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_BAIXO") == 0 )
            {
               AV63TFVariavelCocomo_Baixo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFVariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2)));
               AV64TFVariavelCocomo_Baixo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFVariavelCocomo_Baixo_To", StringUtil.LTrim( StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV63TFVariavelCocomo_Baixo) )
               {
                  Ddo_variavelcocomo_baixo_Filteredtext_set = StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredText_set", Ddo_variavelcocomo_baixo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV64TFVariavelCocomo_Baixo_To) )
               {
                  Ddo_variavelcocomo_baixo_Filteredtextto_set = StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_baixo_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_baixo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_NOMINAL") == 0 )
            {
               AV67TFVariavelCocomo_Nominal = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFVariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2)));
               AV68TFVariavelCocomo_Nominal_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFVariavelCocomo_Nominal_To", StringUtil.LTrim( StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV67TFVariavelCocomo_Nominal) )
               {
                  Ddo_variavelcocomo_nominal_Filteredtext_set = StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredText_set", Ddo_variavelcocomo_nominal_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV68TFVariavelCocomo_Nominal_To) )
               {
                  Ddo_variavelcocomo_nominal_Filteredtextto_set = StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_nominal_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_nominal_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_ALTO") == 0 )
            {
               AV71TFVariavelCocomo_Alto = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFVariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2)));
               AV72TFVariavelCocomo_Alto_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFVariavelCocomo_Alto_To", StringUtil.LTrim( StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV71TFVariavelCocomo_Alto) )
               {
                  Ddo_variavelcocomo_alto_Filteredtext_set = StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredText_set", Ddo_variavelcocomo_alto_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV72TFVariavelCocomo_Alto_To) )
               {
                  Ddo_variavelcocomo_alto_Filteredtextto_set = StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_alto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_alto_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOALTO") == 0 )
            {
               AV75TFVariavelCocomo_MuitoAlto = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFVariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2)));
               AV76TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFVariavelCocomo_MuitoAlto_To", StringUtil.LTrim( StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV75TFVariavelCocomo_MuitoAlto) )
               {
                  Ddo_variavelcocomo_muitoalto_Filteredtext_set = StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_muitoalto_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV76TFVariavelCocomo_MuitoAlto_To) )
               {
                  Ddo_variavelcocomo_muitoalto_Filteredtextto_set = StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_muitoalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_muitoalto_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRAALTO") == 0 )
            {
               AV79TFVariavelCocomo_ExtraAlto = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFVariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2)));
               AV80TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFVariavelCocomo_ExtraAlto_To", StringUtil.LTrim( StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2)));
               if ( ! (Convert.ToDecimal(0)==AV79TFVariavelCocomo_ExtraAlto) )
               {
                  Ddo_variavelcocomo_extraalto_Filteredtext_set = StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredText_set", Ddo_variavelcocomo_extraalto_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV80TFVariavelCocomo_ExtraAlto_To) )
               {
                  Ddo_variavelcocomo_extraalto_Filteredtextto_set = StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_variavelcocomo_extraalto_Internalname, "FilteredTextTo_set", Ddo_variavelcocomo_extraalto_Filteredtextto_set);
               }
            }
            AV119GXV1 = (int)(AV119GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_SIGLA") == 0 )
            {
               AV32VariavelCocomo_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32VariavelCocomo_Sigla1", AV32VariavelCocomo_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_TIPO") == 0 )
            {
               AV35VariavelCocomo_Tipo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35VariavelCocomo_Tipo1", AV35VariavelCocomo_Tipo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_SIGLA") == 0 )
               {
                  AV33VariavelCocomo_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33VariavelCocomo_Sigla2", AV33VariavelCocomo_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_TIPO") == 0 )
               {
                  AV36VariavelCocomo_Tipo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36VariavelCocomo_Tipo2", AV36VariavelCocomo_Tipo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_SIGLA") == 0 )
                  {
                     AV34VariavelCocomo_Sigla3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34VariavelCocomo_Sigla3", AV34VariavelCocomo_Sigla3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_TIPO") == 0 )
                  {
                     AV37VariavelCocomo_Tipo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37VariavelCocomo_Tipo3", AV37VariavelCocomo_Tipo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV118Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV31VariavelCocomo_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "VARIAVELCOCOMO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFVariavelCocomo_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFVariavelCocomo_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFVariavelCocomo_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFVariavelCocomo_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV46TFVariavelCocomo_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFVariavelCocomo_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV49TFVariavelCocomo_Data) && (DateTime.MinValue==AV50TFVariavelCocomo_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV49TFVariavelCocomo_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV50TFVariavelCocomo_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV55TFVariavelCocomo_ExtraBaixo) && (Convert.ToDecimal(0)==AV56TFVariavelCocomo_ExtraBaixo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_EXTRABAIXO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV55TFVariavelCocomo_ExtraBaixo, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV56TFVariavelCocomo_ExtraBaixo_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV59TFVariavelCocomo_MuitoBaixo) && (Convert.ToDecimal(0)==AV60TFVariavelCocomo_MuitoBaixo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_MUITOBAIXO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV59TFVariavelCocomo_MuitoBaixo, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV60TFVariavelCocomo_MuitoBaixo_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV63TFVariavelCocomo_Baixo) && (Convert.ToDecimal(0)==AV64TFVariavelCocomo_Baixo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_BAIXO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV63TFVariavelCocomo_Baixo, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV64TFVariavelCocomo_Baixo_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV67TFVariavelCocomo_Nominal) && (Convert.ToDecimal(0)==AV68TFVariavelCocomo_Nominal_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_NOMINAL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV67TFVariavelCocomo_Nominal, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV68TFVariavelCocomo_Nominal_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV71TFVariavelCocomo_Alto) && (Convert.ToDecimal(0)==AV72TFVariavelCocomo_Alto_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_ALTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV71TFVariavelCocomo_Alto, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV72TFVariavelCocomo_Alto_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV75TFVariavelCocomo_MuitoAlto) && (Convert.ToDecimal(0)==AV76TFVariavelCocomo_MuitoAlto_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_MUITOALTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV75TFVariavelCocomo_MuitoAlto, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV76TFVariavelCocomo_MuitoAlto_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV79TFVariavelCocomo_ExtraAlto) && (Convert.ToDecimal(0)==AV80TFVariavelCocomo_ExtraAlto_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFVARIAVELCOCOMO_EXTRAALTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV79TFVariavelCocomo_ExtraAlto, 12, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV80TFVariavelCocomo_ExtraAlto_To, 12, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV118Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32VariavelCocomo_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32VariavelCocomo_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "VARIAVELCOCOMO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35VariavelCocomo_Tipo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35VariavelCocomo_Tipo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33VariavelCocomo_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33VariavelCocomo_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "VARIAVELCOCOMO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36VariavelCocomo_Tipo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36VariavelCocomo_Tipo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34VariavelCocomo_Sigla3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34VariavelCocomo_Sigla3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "VARIAVELCOCOMO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37VariavelCocomo_Tipo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37VariavelCocomo_Tipo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV118Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "VariaveisCocomo";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_GK2( true) ;
         }
         else
         {
            wb_table2_8_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_GK2( true) ;
         }
         else
         {
            wb_table3_74_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GK2e( true) ;
         }
         else
         {
            wb_table1_2_GK2e( false) ;
         }
      }

      protected void wb_table3_74_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"77\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbVariavelCocomo_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbVariavelCocomo_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbVariavelCocomo_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_ExtraBaixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_ExtraBaixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_ExtraBaixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_MuitoBaixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_MuitoBaixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_MuitoBaixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Baixo_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Baixo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Baixo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Nominal_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Nominal_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Nominal_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_Alto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_Alto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_Alto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_MuitoAlto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_MuitoAlto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_MuitoAlto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtVariavelCocomo_ExtraAlto_Titleformat == 0 )
               {
                  context.SendWebValue( edtVariavelCocomo_ExtraAlto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtVariavelCocomo_ExtraAlto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A962VariavelCocomo_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A964VariavelCocomo_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbVariavelCocomo_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbVariavelCocomo_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_ExtraBaixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_ExtraBaixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_MuitoBaixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_MuitoBaixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Baixo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Baixo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Nominal_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Nominal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_Alto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_Alto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_MuitoAlto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_MuitoAlto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtVariavelCocomo_ExtraAlto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtVariavelCocomo_ExtraAlto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 77 )
         {
            wbEnd = 0;
            nRC_GXsfl_77 = (short)(nGXsfl_77_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_GK2e( true) ;
         }
         else
         {
            wb_table3_74_GK2e( false) ;
         }
      }

      protected void wb_table2_8_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblVariaveiscocomotitle_Internalname, "Vari�veis Cocomo", "", "", lblVariaveiscocomotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_13_GK2( true) ;
         }
         else
         {
            wb_table4_13_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_23_GK2( true) ;
         }
         else
         {
            wb_table5_23_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table5_23_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_GK2e( true) ;
         }
         else
         {
            wb_table2_8_GK2e( false) ;
         }
      }

      protected void wb_table5_23_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextvariavelcocomo_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextvariavelcocomo_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_32_GK2( true) ;
         }
         else
         {
            wb_table6_32_GK2( false) ;
         }
         return  ;
      }

      protected void wb_table6_32_GK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_GK2e( true) ;
         }
         else
         {
            wb_table5_23_GK2e( false) ;
         }
      }

      protected void wb_table6_32_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_sigla1_Internalname, StringUtil.RTrim( AV32VariavelCocomo_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV32VariavelCocomo_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_sigla1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWVariaveisCocomo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavVariavelcocomo_tipo1, cmbavVariavelcocomo_tipo1_Internalname, StringUtil.RTrim( AV35VariavelCocomo_Tipo1), 1, cmbavVariavelcocomo_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavVariavelcocomo_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavVariavelcocomo_tipo1.CurrentValue = StringUtil.RTrim( AV35VariavelCocomo_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo1_Internalname, "Values", (String)(cmbavVariavelcocomo_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_sigla2_Internalname, StringUtil.RTrim( AV33VariavelCocomo_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV33VariavelCocomo_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_sigla2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWVariaveisCocomo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavVariavelcocomo_tipo2, cmbavVariavelcocomo_tipo2_Internalname, StringUtil.RTrim( AV36VariavelCocomo_Tipo2), 1, cmbavVariavelcocomo_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavVariavelcocomo_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavVariavelcocomo_tipo2.CurrentValue = StringUtil.RTrim( AV36VariavelCocomo_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo2_Internalname, "Values", (String)(cmbavVariavelcocomo_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVariavelcocomo_sigla3_Internalname, StringUtil.RTrim( AV34VariavelCocomo_Sigla3), StringUtil.RTrim( context.localUtil.Format( AV34VariavelCocomo_Sigla3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVariavelcocomo_sigla3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavVariavelcocomo_sigla3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWVariaveisCocomo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavVariavelcocomo_tipo3, cmbavVariavelcocomo_tipo3_Internalname, StringUtil.RTrim( AV37VariavelCocomo_Tipo3), 1, cmbavVariavelcocomo_tipo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavVariavelcocomo_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WWVariaveisCocomo.htm");
            cmbavVariavelcocomo_tipo3.CurrentValue = StringUtil.RTrim( AV37VariavelCocomo_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavVariavelcocomo_tipo3_Internalname, "Values", (String)(cmbavVariavelcocomo_tipo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_32_GK2e( true) ;
         }
         else
         {
            wb_table6_32_GK2e( false) ;
         }
      }

      protected void wb_table4_13_GK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_GK2e( true) ;
         }
         else
         {
            wb_table4_13_GK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGK2( ) ;
         WSGK2( ) ;
         WEGK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118551546");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwvariaveiscocomo.js", "?20203118551546");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_idx;
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA_"+sGXsfl_77_idx;
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO_"+sGXsfl_77_idx;
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA_"+sGXsfl_77_idx;
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO_"+sGXsfl_77_idx;
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO_"+sGXsfl_77_idx;
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO_"+sGXsfl_77_idx;
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL_"+sGXsfl_77_idx;
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO_"+sGXsfl_77_idx;
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO_"+sGXsfl_77_idx;
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO_"+sGXsfl_77_idx;
      }

      protected void SubsflControlProps_fel_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA_"+sGXsfl_77_fel_idx;
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO_"+sGXsfl_77_fel_idx;
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO_"+sGXsfl_77_fel_idx;
      }

      protected void sendrow_772( )
      {
         SubsflControlProps_772( ) ;
         WBGK0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_77_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_77_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_77_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV116Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV116Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV117Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV117Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Sigla_Internalname,StringUtil.RTrim( A962VariavelCocomo_Sigla),StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_77_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "VARIAVELCOCOMO_TIPO_" + sGXsfl_77_idx;
               cmbVariavelCocomo_Tipo.Name = GXCCtl;
               cmbVariavelCocomo_Tipo.WebTags = "";
               cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
               cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
               cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
               if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
               {
                  A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbVariavelCocomo_Tipo,(String)cmbVariavelCocomo_Tipo_Internalname,StringUtil.RTrim( A964VariavelCocomo_Tipo),(short)1,(String)cmbVariavelCocomo_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbVariavelCocomo_Tipo.CurrentValue = StringUtil.RTrim( A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Values", (String)(cmbVariavelCocomo_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Data_Internalname,context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"),context.localUtil.Format( A966VariavelCocomo_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_ExtraBaixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ",", "")),context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_ExtraBaixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_MuitoBaixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ",", "")),context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_MuitoBaixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Baixo_Internalname,StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ",", "")),context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Baixo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Nominal_Internalname,StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ",", "")),context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Nominal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_Alto_Internalname,StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ",", "")),context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_Alto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_MuitoAlto_Internalname,StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ",", "")),context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_MuitoAlto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtVariavelCocomo_ExtraAlto_Internalname,StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ",", "")),context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtVariavelCocomo_ExtraAlto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_SIGLA"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_TIPO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, StringUtil.RTrim( context.localUtil.Format( A964VariavelCocomo_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_DATA"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, A966VariavelCocomo_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRABAIXO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOBAIXO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_BAIXO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOMINAL"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_ALTO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_MUITOALTO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_EXTRAALTO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         /* End function sendrow_772 */
      }

      protected void init_default_properties( )
      {
         lblVariaveiscocomotitle_Internalname = "VARIAVEISCOCOMOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextvariavelcocomo_areatrabalhocod_Internalname = "FILTERTEXTVARIAVELCOCOMO_AREATRABALHOCOD";
         edtavVariavelcocomo_areatrabalhocod_Internalname = "vVARIAVELCOCOMO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavVariavelcocomo_sigla1_Internalname = "vVARIAVELCOCOMO_SIGLA1";
         cmbavVariavelcocomo_tipo1_Internalname = "vVARIAVELCOCOMO_TIPO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavVariavelcocomo_sigla2_Internalname = "vVARIAVELCOCOMO_SIGLA2";
         cmbavVariavelcocomo_tipo2_Internalname = "vVARIAVELCOCOMO_TIPO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavVariavelcocomo_sigla3_Internalname = "vVARIAVELCOCOMO_SIGLA3";
         cmbavVariavelcocomo_tipo3_Internalname = "vVARIAVELCOCOMO_TIPO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA";
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO";
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA";
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO";
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO";
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO";
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL";
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO";
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO";
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfvariavelcocomo_sigla_Internalname = "vTFVARIAVELCOCOMO_SIGLA";
         edtavTfvariavelcocomo_sigla_sel_Internalname = "vTFVARIAVELCOCOMO_SIGLA_SEL";
         edtavTfvariavelcocomo_data_Internalname = "vTFVARIAVELCOCOMO_DATA";
         edtavTfvariavelcocomo_data_to_Internalname = "vTFVARIAVELCOCOMO_DATA_TO";
         edtavDdo_variavelcocomo_dataauxdate_Internalname = "vDDO_VARIAVELCOCOMO_DATAAUXDATE";
         edtavDdo_variavelcocomo_dataauxdateto_Internalname = "vDDO_VARIAVELCOCOMO_DATAAUXDATETO";
         divDdo_variavelcocomo_dataauxdates_Internalname = "DDO_VARIAVELCOCOMO_DATAAUXDATES";
         edtavTfvariavelcocomo_extrabaixo_Internalname = "vTFVARIAVELCOCOMO_EXTRABAIXO";
         edtavTfvariavelcocomo_extrabaixo_to_Internalname = "vTFVARIAVELCOCOMO_EXTRABAIXO_TO";
         edtavTfvariavelcocomo_muitobaixo_Internalname = "vTFVARIAVELCOCOMO_MUITOBAIXO";
         edtavTfvariavelcocomo_muitobaixo_to_Internalname = "vTFVARIAVELCOCOMO_MUITOBAIXO_TO";
         edtavTfvariavelcocomo_baixo_Internalname = "vTFVARIAVELCOCOMO_BAIXO";
         edtavTfvariavelcocomo_baixo_to_Internalname = "vTFVARIAVELCOCOMO_BAIXO_TO";
         edtavTfvariavelcocomo_nominal_Internalname = "vTFVARIAVELCOCOMO_NOMINAL";
         edtavTfvariavelcocomo_nominal_to_Internalname = "vTFVARIAVELCOCOMO_NOMINAL_TO";
         edtavTfvariavelcocomo_alto_Internalname = "vTFVARIAVELCOCOMO_ALTO";
         edtavTfvariavelcocomo_alto_to_Internalname = "vTFVARIAVELCOCOMO_ALTO_TO";
         edtavTfvariavelcocomo_muitoalto_Internalname = "vTFVARIAVELCOCOMO_MUITOALTO";
         edtavTfvariavelcocomo_muitoalto_to_Internalname = "vTFVARIAVELCOCOMO_MUITOALTO_TO";
         edtavTfvariavelcocomo_extraalto_Internalname = "vTFVARIAVELCOCOMO_EXTRAALTO";
         edtavTfvariavelcocomo_extraalto_to_Internalname = "vTFVARIAVELCOCOMO_EXTRAALTO_TO";
         Ddo_variavelcocomo_sigla_Internalname = "DDO_VARIAVELCOCOMO_SIGLA";
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_tipo_Internalname = "DDO_VARIAVELCOCOMO_TIPO";
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_data_Internalname = "DDO_VARIAVELCOCOMO_DATA";
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_extrabaixo_Internalname = "DDO_VARIAVELCOCOMO_EXTRABAIXO";
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_muitobaixo_Internalname = "DDO_VARIAVELCOCOMO_MUITOBAIXO";
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_baixo_Internalname = "DDO_VARIAVELCOCOMO_BAIXO";
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_nominal_Internalname = "DDO_VARIAVELCOCOMO_NOMINAL";
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_alto_Internalname = "DDO_VARIAVELCOCOMO_ALTO";
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_muitoalto_Internalname = "DDO_VARIAVELCOCOMO_MUITOALTO";
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE";
         Ddo_variavelcocomo_extraalto_Internalname = "DDO_VARIAVELCOCOMO_EXTRAALTO";
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname = "vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtVariavelCocomo_ExtraAlto_Jsonclick = "";
         edtVariavelCocomo_MuitoAlto_Jsonclick = "";
         edtVariavelCocomo_Alto_Jsonclick = "";
         edtVariavelCocomo_Nominal_Jsonclick = "";
         edtVariavelCocomo_Baixo_Jsonclick = "";
         edtVariavelCocomo_MuitoBaixo_Jsonclick = "";
         edtVariavelCocomo_ExtraBaixo_Jsonclick = "";
         edtVariavelCocomo_Data_Jsonclick = "";
         cmbVariavelCocomo_Tipo_Jsonclick = "";
         edtVariavelCocomo_Sigla_Jsonclick = "";
         imgInsert_Visible = 1;
         cmbavVariavelcocomo_tipo3_Jsonclick = "";
         edtavVariavelcocomo_sigla3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavVariavelcocomo_tipo2_Jsonclick = "";
         edtavVariavelcocomo_sigla2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavVariavelcocomo_tipo1_Jsonclick = "";
         edtavVariavelcocomo_sigla1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavVariavelcocomo_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtVariavelCocomo_ExtraAlto_Titleformat = 0;
         edtVariavelCocomo_MuitoAlto_Titleformat = 0;
         edtVariavelCocomo_Alto_Titleformat = 0;
         edtVariavelCocomo_Nominal_Titleformat = 0;
         edtVariavelCocomo_Baixo_Titleformat = 0;
         edtVariavelCocomo_MuitoBaixo_Titleformat = 0;
         edtVariavelCocomo_ExtraBaixo_Titleformat = 0;
         edtVariavelCocomo_Data_Titleformat = 0;
         cmbVariavelCocomo_Tipo_Titleformat = 0;
         edtVariavelCocomo_Sigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavVariavelcocomo_tipo3.Visible = 1;
         edtavVariavelcocomo_sigla3_Visible = 1;
         cmbavVariavelcocomo_tipo2.Visible = 1;
         edtavVariavelcocomo_sigla2_Visible = 1;
         cmbavVariavelcocomo_tipo1.Visible = 1;
         edtavVariavelcocomo_sigla1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtVariavelCocomo_ExtraAlto_Title = "Extra Alto";
         edtVariavelCocomo_MuitoAlto_Title = "Muito Alto";
         edtVariavelCocomo_Alto_Title = "Alto";
         edtVariavelCocomo_Nominal_Title = "Nominal";
         edtVariavelCocomo_Baixo_Title = "Baixo";
         edtVariavelCocomo_MuitoBaixo_Title = "Muito Baixo";
         edtVariavelCocomo_ExtraBaixo_Title = "Extra Baixo";
         edtVariavelCocomo_Data_Title = "Data";
         cmbVariavelCocomo_Tipo.Title.Text = "Tipo";
         edtVariavelCocomo_Sigla_Title = "Sigla";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible = 1;
         edtavTfvariavelcocomo_extraalto_to_Jsonclick = "";
         edtavTfvariavelcocomo_extraalto_to_Visible = 1;
         edtavTfvariavelcocomo_extraalto_Jsonclick = "";
         edtavTfvariavelcocomo_extraalto_Visible = 1;
         edtavTfvariavelcocomo_muitoalto_to_Jsonclick = "";
         edtavTfvariavelcocomo_muitoalto_to_Visible = 1;
         edtavTfvariavelcocomo_muitoalto_Jsonclick = "";
         edtavTfvariavelcocomo_muitoalto_Visible = 1;
         edtavTfvariavelcocomo_alto_to_Jsonclick = "";
         edtavTfvariavelcocomo_alto_to_Visible = 1;
         edtavTfvariavelcocomo_alto_Jsonclick = "";
         edtavTfvariavelcocomo_alto_Visible = 1;
         edtavTfvariavelcocomo_nominal_to_Jsonclick = "";
         edtavTfvariavelcocomo_nominal_to_Visible = 1;
         edtavTfvariavelcocomo_nominal_Jsonclick = "";
         edtavTfvariavelcocomo_nominal_Visible = 1;
         edtavTfvariavelcocomo_baixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_baixo_to_Visible = 1;
         edtavTfvariavelcocomo_baixo_Jsonclick = "";
         edtavTfvariavelcocomo_baixo_Visible = 1;
         edtavTfvariavelcocomo_muitobaixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_muitobaixo_to_Visible = 1;
         edtavTfvariavelcocomo_muitobaixo_Jsonclick = "";
         edtavTfvariavelcocomo_muitobaixo_Visible = 1;
         edtavTfvariavelcocomo_extrabaixo_to_Jsonclick = "";
         edtavTfvariavelcocomo_extrabaixo_to_Visible = 1;
         edtavTfvariavelcocomo_extrabaixo_Jsonclick = "";
         edtavTfvariavelcocomo_extrabaixo_Visible = 1;
         edtavDdo_variavelcocomo_dataauxdateto_Jsonclick = "";
         edtavDdo_variavelcocomo_dataauxdate_Jsonclick = "";
         edtavTfvariavelcocomo_data_to_Jsonclick = "";
         edtavTfvariavelcocomo_data_to_Visible = 1;
         edtavTfvariavelcocomo_data_Jsonclick = "";
         edtavTfvariavelcocomo_data_Visible = 1;
         edtavTfvariavelcocomo_sigla_sel_Jsonclick = "";
         edtavTfvariavelcocomo_sigla_sel_Visible = 1;
         edtavTfvariavelcocomo_sigla_Jsonclick = "";
         edtavTfvariavelcocomo_sigla_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_variavelcocomo_extraalto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_extraalto_Rangefilterto = "At�";
         Ddo_variavelcocomo_extraalto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_extraalto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_extraalto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extraalto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Filtertype = "Numeric";
         Ddo_variavelcocomo_extraalto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extraalto_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extraalto_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_extraalto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_extraalto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_extraalto_Tooltip = "Op��es";
         Ddo_variavelcocomo_extraalto_Caption = "";
         Ddo_variavelcocomo_muitoalto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_muitoalto_Rangefilterto = "At�";
         Ddo_variavelcocomo_muitoalto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_muitoalto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_muitoalto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitoalto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Filtertype = "Numeric";
         Ddo_variavelcocomo_muitoalto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitoalto_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitoalto_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_muitoalto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_muitoalto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_muitoalto_Tooltip = "Op��es";
         Ddo_variavelcocomo_muitoalto_Caption = "";
         Ddo_variavelcocomo_alto_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_alto_Rangefilterto = "At�";
         Ddo_variavelcocomo_alto_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_alto_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_alto_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_alto_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Filtertype = "Numeric";
         Ddo_variavelcocomo_alto_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_alto_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_alto_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_alto_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_alto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_alto_Cls = "ColumnSettings";
         Ddo_variavelcocomo_alto_Tooltip = "Op��es";
         Ddo_variavelcocomo_alto_Caption = "";
         Ddo_variavelcocomo_nominal_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_nominal_Rangefilterto = "At�";
         Ddo_variavelcocomo_nominal_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_nominal_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_nominal_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_nominal_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Filtertype = "Numeric";
         Ddo_variavelcocomo_nominal_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_nominal_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_nominal_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_nominal_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_nominal_Cls = "ColumnSettings";
         Ddo_variavelcocomo_nominal_Tooltip = "Op��es";
         Ddo_variavelcocomo_nominal_Caption = "";
         Ddo_variavelcocomo_baixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_baixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_baixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_baixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_baixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_baixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_baixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_baixo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_baixo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_baixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_baixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_baixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_baixo_Caption = "";
         Ddo_variavelcocomo_muitobaixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_muitobaixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_muitobaixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_muitobaixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_muitobaixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitobaixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_muitobaixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_muitobaixo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitobaixo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_muitobaixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_muitobaixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_muitobaixo_Caption = "";
         Ddo_variavelcocomo_extrabaixo_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_extrabaixo_Rangefilterto = "At�";
         Ddo_variavelcocomo_extrabaixo_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_extrabaixo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_extrabaixo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extrabaixo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Filtertype = "Numeric";
         Ddo_variavelcocomo_extrabaixo_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_extrabaixo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extrabaixo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_extrabaixo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_extrabaixo_Tooltip = "Op��es";
         Ddo_variavelcocomo_extrabaixo_Caption = "";
         Ddo_variavelcocomo_data_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_data_Rangefilterto = "At�";
         Ddo_variavelcocomo_data_Rangefilterfrom = "Desde";
         Ddo_variavelcocomo_data_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_data_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_data_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Filtertype = "Date";
         Ddo_variavelcocomo_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_data_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_data_Cls = "ColumnSettings";
         Ddo_variavelcocomo_data_Tooltip = "Op��es";
         Ddo_variavelcocomo_data_Caption = "";
         Ddo_variavelcocomo_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_variavelcocomo_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_tipo_Datalistfixedvalues = "E:Escala,M:Multiplicador";
         Ddo_variavelcocomo_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Datalisttype = "FixedValues";
         Ddo_variavelcocomo_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_tipo_Cls = "ColumnSettings";
         Ddo_variavelcocomo_tipo_Tooltip = "Op��es";
         Ddo_variavelcocomo_tipo_Caption = "";
         Ddo_variavelcocomo_sigla_Searchbuttontext = "Pesquisar";
         Ddo_variavelcocomo_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_variavelcocomo_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_variavelcocomo_sigla_Loadingdata = "Carregando dados...";
         Ddo_variavelcocomo_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_variavelcocomo_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_variavelcocomo_sigla_Datalistproc = "GetWWVariaveisCocomoFilterData";
         Ddo_variavelcocomo_sigla_Datalisttype = "Dynamic";
         Ddo_variavelcocomo_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_variavelcocomo_sigla_Filtertype = "Character";
         Ddo_variavelcocomo_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace = "";
         Ddo_variavelcocomo_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_variavelcocomo_sigla_Cls = "ColumnSettings";
         Ddo_variavelcocomo_sigla_Tooltip = "Op��es";
         Ddo_variavelcocomo_sigla_Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Variaveis Cocomo";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV44VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV70VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_SIGLA.ONOPTIONCLICKED","{handler:'E11GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_sigla_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_sigla_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_sigla_Selectedvalue_get',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_TIPO.ONOPTIONCLICKED","{handler:'E12GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_tipo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_tipo_Selectedvalue_get',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_DATA.ONOPTIONCLICKED","{handler:'E13GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_data_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_data_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_data_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_variavelcocomo_data_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'SortedStatus'},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'Ddo_variavelcocomo_sigla_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SortedStatus'},{av:'Ddo_variavelcocomo_tipo_Sortedstatus',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_EXTRABAIXO.ONOPTIONCLICKED","{handler:'E14GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_extrabaixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_MUITOBAIXO.ONOPTIONCLICKED","{handler:'E15GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_muitobaixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_BAIXO.ONOPTIONCLICKED","{handler:'E16GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_baixo_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_baixo_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_baixo_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_NOMINAL.ONOPTIONCLICKED","{handler:'E17GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_nominal_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_nominal_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_nominal_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_ALTO.ONOPTIONCLICKED","{handler:'E18GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_alto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_alto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_alto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_MUITOALTO.ONOPTIONCLICKED","{handler:'E19GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_muitoalto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_muitoalto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_muitoalto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_VARIAVELCOCOMO_EXTRAALTO.ONOPTIONCLICKED","{handler:'E20GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'Ddo_variavelcocomo_extraalto_Activeeventkey',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'ActiveEventKey'},{av:'Ddo_variavelcocomo_extraalto_Filteredtext_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredText_get'},{av:'Ddo_variavelcocomo_extraalto_Filteredtextto_get',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E34GK2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E21GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E27GK2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E22GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'edtavVariavelcocomo_sigla2_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA2',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo2'},{av:'edtavVariavelcocomo_sigla3_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA3',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo3'},{av:'edtavVariavelcocomo_sigla1_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA1',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E28GK2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_sigla1_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA1',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E29GK2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E23GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'edtavVariavelcocomo_sigla2_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA2',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo2'},{av:'edtavVariavelcocomo_sigla3_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA3',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo3'},{av:'edtavVariavelcocomo_sigla1_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA1',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E30GK2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_sigla2_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA2',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E24GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'edtavVariavelcocomo_sigla2_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA2',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo2'},{av:'edtavVariavelcocomo_sigla3_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA3',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo3'},{av:'edtavVariavelcocomo_sigla1_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA1',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E31GK2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavVariavelcocomo_sigla3_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA3',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E25GK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_sigla_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'FilteredText_set'},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_variavelcocomo_sigla_Selectedvalue_set',ctrl:'DDO_VARIAVELCOCOMO_SIGLA',prop:'SelectedValue_set'},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_variavelcocomo_tipo_Selectedvalue_set',ctrl:'DDO_VARIAVELCOCOMO_TIPO',prop:'SelectedValue_set'},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'Ddo_variavelcocomo_data_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredText_set'},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'Ddo_variavelcocomo_data_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_DATA',prop:'FilteredTextTo_set'},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredText_set'},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extrabaixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRABAIXO',prop:'FilteredTextTo_set'},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredText_set'},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitobaixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOBAIXO',prop:'FilteredTextTo_set'},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_baixo_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredText_set'},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_baixo_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_BAIXO',prop:'FilteredTextTo_set'},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_nominal_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredText_set'},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_nominal_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_NOMINAL',prop:'FilteredTextTo_set'},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_alto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredText_set'},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_alto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_ALTO',prop:'FilteredTextTo_set'},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitoalto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredText_set'},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_muitoalto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_MUITOALTO',prop:'FilteredTextTo_set'},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extraalto_Filteredtext_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredText_set'},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_variavelcocomo_extraalto_Filteredtextto_set',ctrl:'DDO_VARIAVELCOCOMO_EXTRAALTO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavVariavelcocomo_sigla1_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA1',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'edtavVariavelcocomo_sigla2_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA2',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo2'},{av:'edtavVariavelcocomo_sigla3_Visible',ctrl:'vVARIAVELCOCOMO_SIGLA3',prop:'Visible'},{av:'cmbavVariavelcocomo_tipo3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E26GK2',iparms:[{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV44VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV70VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV44VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV70VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV44VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV70VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A961VariavelCocomo_AreaTrabalhoCod',fld:'VARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A962VariavelCocomo_Sigla',fld:'VARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'A964VariavelCocomo_Tipo',fld:'VARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'A992VariavelCocomo_Sequencial',fld:'VARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_VariavelCocomo_DataTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRABAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOBAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_BAIXOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_NOMINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_ALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_MUITOALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace',fld:'vDDO_VARIAVELCOCOMO_EXTRAALTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32VariavelCocomo_Sigla1',fld:'vVARIAVELCOCOMO_SIGLA1',pic:'@!',nv:''},{av:'AV35VariavelCocomo_Tipo1',fld:'vVARIAVELCOCOMO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33VariavelCocomo_Sigla2',fld:'vVARIAVELCOCOMO_SIGLA2',pic:'@!',nv:''},{av:'AV36VariavelCocomo_Tipo2',fld:'vVARIAVELCOCOMO_TIPO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34VariavelCocomo_Sigla3',fld:'vVARIAVELCOCOMO_SIGLA3',pic:'@!',nv:''},{av:'AV37VariavelCocomo_Tipo3',fld:'vVARIAVELCOCOMO_TIPO3',pic:'',nv:''},{av:'AV41TFVariavelCocomo_Sigla',fld:'vTFVARIAVELCOCOMO_SIGLA',pic:'@!',nv:''},{av:'AV42TFVariavelCocomo_Sigla_Sel',fld:'vTFVARIAVELCOCOMO_SIGLA_SEL',pic:'@!',nv:''},{av:'AV46TFVariavelCocomo_Tipo_Sels',fld:'vTFVARIAVELCOCOMO_TIPO_SELS',pic:'',nv:null},{av:'AV49TFVariavelCocomo_Data',fld:'vTFVARIAVELCOCOMO_DATA',pic:'',nv:''},{av:'AV50TFVariavelCocomo_Data_To',fld:'vTFVARIAVELCOCOMO_DATA_TO',pic:'',nv:''},{av:'AV55TFVariavelCocomo_ExtraBaixo',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56TFVariavelCocomo_ExtraBaixo_To',fld:'vTFVARIAVELCOCOMO_EXTRABAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59TFVariavelCocomo_MuitoBaixo',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFVariavelCocomo_MuitoBaixo_To',fld:'vTFVARIAVELCOCOMO_MUITOBAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63TFVariavelCocomo_Baixo',fld:'vTFVARIAVELCOCOMO_BAIXO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFVariavelCocomo_Baixo_To',fld:'vTFVARIAVELCOCOMO_BAIXO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67TFVariavelCocomo_Nominal',fld:'vTFVARIAVELCOCOMO_NOMINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV68TFVariavelCocomo_Nominal_To',fld:'vTFVARIAVELCOCOMO_NOMINAL_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71TFVariavelCocomo_Alto',fld:'vTFVARIAVELCOCOMO_ALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV72TFVariavelCocomo_Alto_To',fld:'vTFVARIAVELCOCOMO_ALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV75TFVariavelCocomo_MuitoAlto',fld:'vTFVARIAVELCOCOMO_MUITOALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV76TFVariavelCocomo_MuitoAlto_To',fld:'vTFVARIAVELCOCOMO_MUITOALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79TFVariavelCocomo_ExtraAlto',fld:'vTFVARIAVELCOCOMO_EXTRAALTO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80TFVariavelCocomo_ExtraAlto_To',fld:'vTFVARIAVELCOCOMO_EXTRAALTO_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV118Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40VariavelCocomo_SiglaTitleFilterData',fld:'vVARIAVELCOCOMO_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV44VariavelCocomo_TipoTitleFilterData',fld:'vVARIAVELCOCOMO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48VariavelCocomo_DataTitleFilterData',fld:'vVARIAVELCOCOMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54VariavelCocomo_ExtraBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRABAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58VariavelCocomo_MuitoBaixoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOBAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62VariavelCocomo_BaixoTitleFilterData',fld:'vVARIAVELCOCOMO_BAIXOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66VariavelCocomo_NominalTitleFilterData',fld:'vVARIAVELCOCOMO_NOMINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV70VariavelCocomo_AltoTitleFilterData',fld:'vVARIAVELCOCOMO_ALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV74VariavelCocomo_MuitoAltoTitleFilterData',fld:'vVARIAVELCOCOMO_MUITOALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78VariavelCocomo_ExtraAltoTitleFilterData',fld:'vVARIAVELCOCOMO_EXTRAALTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtVariavelCocomo_Sigla_Titleformat',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Titleformat'},{av:'edtVariavelCocomo_Sigla_Title',ctrl:'VARIAVELCOCOMO_SIGLA',prop:'Title'},{av:'cmbVariavelCocomo_Tipo'},{av:'edtVariavelCocomo_Data_Titleformat',ctrl:'VARIAVELCOCOMO_DATA',prop:'Titleformat'},{av:'edtVariavelCocomo_Data_Title',ctrl:'VARIAVELCOCOMO_DATA',prop:'Title'},{av:'edtVariavelCocomo_ExtraBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraBaixo_Title',ctrl:'VARIAVELCOCOMO_EXTRABAIXO',prop:'Title'},{av:'edtVariavelCocomo_MuitoBaixo_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoBaixo_Title',ctrl:'VARIAVELCOCOMO_MUITOBAIXO',prop:'Title'},{av:'edtVariavelCocomo_Baixo_Titleformat',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Titleformat'},{av:'edtVariavelCocomo_Baixo_Title',ctrl:'VARIAVELCOCOMO_BAIXO',prop:'Title'},{av:'edtVariavelCocomo_Nominal_Titleformat',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Titleformat'},{av:'edtVariavelCocomo_Nominal_Title',ctrl:'VARIAVELCOCOMO_NOMINAL',prop:'Title'},{av:'edtVariavelCocomo_Alto_Titleformat',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_Alto_Title',ctrl:'VARIAVELCOCOMO_ALTO',prop:'Title'},{av:'edtVariavelCocomo_MuitoAlto_Titleformat',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_MuitoAlto_Title',ctrl:'VARIAVELCOCOMO_MUITOALTO',prop:'Title'},{av:'edtVariavelCocomo_ExtraAlto_Titleformat',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Titleformat'},{av:'edtVariavelCocomo_ExtraAlto_Title',ctrl:'VARIAVELCOCOMO_EXTRAALTO',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_variavelcocomo_sigla_Activeeventkey = "";
         Ddo_variavelcocomo_sigla_Filteredtext_get = "";
         Ddo_variavelcocomo_sigla_Selectedvalue_get = "";
         Ddo_variavelcocomo_tipo_Activeeventkey = "";
         Ddo_variavelcocomo_tipo_Selectedvalue_get = "";
         Ddo_variavelcocomo_data_Activeeventkey = "";
         Ddo_variavelcocomo_data_Filteredtext_get = "";
         Ddo_variavelcocomo_data_Filteredtextto_get = "";
         Ddo_variavelcocomo_extrabaixo_Activeeventkey = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtext_get = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_muitobaixo_Activeeventkey = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtext_get = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_baixo_Activeeventkey = "";
         Ddo_variavelcocomo_baixo_Filteredtext_get = "";
         Ddo_variavelcocomo_baixo_Filteredtextto_get = "";
         Ddo_variavelcocomo_nominal_Activeeventkey = "";
         Ddo_variavelcocomo_nominal_Filteredtext_get = "";
         Ddo_variavelcocomo_nominal_Filteredtextto_get = "";
         Ddo_variavelcocomo_alto_Activeeventkey = "";
         Ddo_variavelcocomo_alto_Filteredtext_get = "";
         Ddo_variavelcocomo_alto_Filteredtextto_get = "";
         Ddo_variavelcocomo_muitoalto_Activeeventkey = "";
         Ddo_variavelcocomo_muitoalto_Filteredtext_get = "";
         Ddo_variavelcocomo_muitoalto_Filteredtextto_get = "";
         Ddo_variavelcocomo_extraalto_Activeeventkey = "";
         Ddo_variavelcocomo_extraalto_Filteredtext_get = "";
         Ddo_variavelcocomo_extraalto_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV32VariavelCocomo_Sigla1 = "";
         AV35VariavelCocomo_Tipo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV33VariavelCocomo_Sigla2 = "";
         AV36VariavelCocomo_Tipo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV34VariavelCocomo_Sigla3 = "";
         AV37VariavelCocomo_Tipo3 = "";
         AV41TFVariavelCocomo_Sigla = "";
         AV42TFVariavelCocomo_Sigla_Sel = "";
         AV49TFVariavelCocomo_Data = DateTime.MinValue;
         AV50TFVariavelCocomo_Data_To = DateTime.MinValue;
         AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace = "";
         AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace = "";
         AV53ddo_VariavelCocomo_DataTitleControlIdToReplace = "";
         AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace = "";
         AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace = "";
         AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace = "";
         AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace = "";
         AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace = "";
         AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace = "";
         AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace = "";
         AV46TFVariavelCocomo_Tipo_Sels = new GxSimpleCollection();
         AV118Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A962VariavelCocomo_Sigla = "";
         A964VariavelCocomo_Tipo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV82DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV40VariavelCocomo_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44VariavelCocomo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48VariavelCocomo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54VariavelCocomo_ExtraBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58VariavelCocomo_MuitoBaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62VariavelCocomo_BaixoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66VariavelCocomo_NominalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70VariavelCocomo_AltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74VariavelCocomo_MuitoAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78VariavelCocomo_ExtraAltoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_variavelcocomo_sigla_Filteredtext_set = "";
         Ddo_variavelcocomo_sigla_Selectedvalue_set = "";
         Ddo_variavelcocomo_sigla_Sortedstatus = "";
         Ddo_variavelcocomo_tipo_Selectedvalue_set = "";
         Ddo_variavelcocomo_tipo_Sortedstatus = "";
         Ddo_variavelcocomo_data_Filteredtext_set = "";
         Ddo_variavelcocomo_data_Filteredtextto_set = "";
         Ddo_variavelcocomo_data_Sortedstatus = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtext_set = "";
         Ddo_variavelcocomo_extrabaixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtext_set = "";
         Ddo_variavelcocomo_muitobaixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_baixo_Filteredtext_set = "";
         Ddo_variavelcocomo_baixo_Filteredtextto_set = "";
         Ddo_variavelcocomo_nominal_Filteredtext_set = "";
         Ddo_variavelcocomo_nominal_Filteredtextto_set = "";
         Ddo_variavelcocomo_alto_Filteredtext_set = "";
         Ddo_variavelcocomo_alto_Filteredtextto_set = "";
         Ddo_variavelcocomo_muitoalto_Filteredtext_set = "";
         Ddo_variavelcocomo_muitoalto_Filteredtextto_set = "";
         Ddo_variavelcocomo_extraalto_Filteredtext_set = "";
         Ddo_variavelcocomo_extraalto_Filteredtextto_set = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV51DDO_VariavelCocomo_DataAuxDate = DateTime.MinValue;
         AV52DDO_VariavelCocomo_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 = "";
         AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = "";
         AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 = "";
         AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 = "";
         AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = "";
         AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 = "";
         AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 = "";
         AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = "";
         AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 = "";
         AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = "";
         AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel = "";
         AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels = new GxSimpleCollection();
         AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data = DateTime.MinValue;
         AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to = DateTime.MinValue;
         AV28Update = "";
         AV116Update_GXI = "";
         AV29Delete = "";
         AV117Delete_GXI = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 = "";
         lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 = "";
         lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 = "";
         lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla = "";
         H00GK2_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GK2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GK2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         H00GK2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         H00GK2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         H00GK2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         H00GK2_A970VariavelCocomo_Alto = new decimal[1] ;
         H00GK2_n970VariavelCocomo_Alto = new bool[] {false} ;
         H00GK2_A969VariavelCocomo_Nominal = new decimal[1] ;
         H00GK2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         H00GK2_A968VariavelCocomo_Baixo = new decimal[1] ;
         H00GK2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         H00GK2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         H00GK2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         H00GK2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         H00GK2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         H00GK2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         H00GK2_n966VariavelCocomo_Data = new bool[] {false} ;
         H00GK2_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GK2_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GK3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV45TFVariavelCocomo_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblVariaveiscocomotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextvariavelcocomo_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwvariaveiscocomo__default(),
            new Object[][] {
                new Object[] {
               H00GK2_A992VariavelCocomo_Sequencial, H00GK2_A961VariavelCocomo_AreaTrabalhoCod, H00GK2_A972VariavelCocomo_ExtraAlto, H00GK2_n972VariavelCocomo_ExtraAlto, H00GK2_A971VariavelCocomo_MuitoAlto, H00GK2_n971VariavelCocomo_MuitoAlto, H00GK2_A970VariavelCocomo_Alto, H00GK2_n970VariavelCocomo_Alto, H00GK2_A969VariavelCocomo_Nominal, H00GK2_n969VariavelCocomo_Nominal,
               H00GK2_A968VariavelCocomo_Baixo, H00GK2_n968VariavelCocomo_Baixo, H00GK2_A967VariavelCocomo_MuitoBaixo, H00GK2_n967VariavelCocomo_MuitoBaixo, H00GK2_A986VariavelCocomo_ExtraBaixo, H00GK2_n986VariavelCocomo_ExtraBaixo, H00GK2_A966VariavelCocomo_Data, H00GK2_n966VariavelCocomo_Data, H00GK2_A964VariavelCocomo_Tipo, H00GK2_A962VariavelCocomo_Sigla
               }
               , new Object[] {
               H00GK3_AGRID_nRecordCount
               }
            }
         );
         AV118Pgmname = "WWVariaveisCocomo";
         /* GeneXus formulas. */
         AV118Pgmname = "WWVariaveisCocomo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_77 ;
      private short nGXsfl_77_idx=1 ;
      private short AV13OrderedBy ;
      private short A992VariavelCocomo_Sequencial ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_77_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtVariavelCocomo_Sigla_Titleformat ;
      private short cmbVariavelCocomo_Tipo_Titleformat ;
      private short edtVariavelCocomo_Data_Titleformat ;
      private short edtVariavelCocomo_ExtraBaixo_Titleformat ;
      private short edtVariavelCocomo_MuitoBaixo_Titleformat ;
      private short edtVariavelCocomo_Baixo_Titleformat ;
      private short edtVariavelCocomo_Nominal_Titleformat ;
      private short edtVariavelCocomo_Alto_Titleformat ;
      private short edtVariavelCocomo_MuitoAlto_Titleformat ;
      private short edtVariavelCocomo_ExtraAlto_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV31VariavelCocomo_AreaTrabalhoCod ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int Ddo_variavelcocomo_sigla_Datalistupdateminimumcharacters ;
      private int edtavTfvariavelcocomo_sigla_Visible ;
      private int edtavTfvariavelcocomo_sigla_sel_Visible ;
      private int edtavTfvariavelcocomo_data_Visible ;
      private int edtavTfvariavelcocomo_data_to_Visible ;
      private int edtavTfvariavelcocomo_extrabaixo_Visible ;
      private int edtavTfvariavelcocomo_extrabaixo_to_Visible ;
      private int edtavTfvariavelcocomo_muitobaixo_Visible ;
      private int edtavTfvariavelcocomo_muitobaixo_to_Visible ;
      private int edtavTfvariavelcocomo_baixo_Visible ;
      private int edtavTfvariavelcocomo_baixo_to_Visible ;
      private int edtavTfvariavelcocomo_nominal_Visible ;
      private int edtavTfvariavelcocomo_nominal_to_Visible ;
      private int edtavTfvariavelcocomo_alto_Visible ;
      private int edtavTfvariavelcocomo_alto_to_Visible ;
      private int edtavTfvariavelcocomo_muitoalto_Visible ;
      private int edtavTfvariavelcocomo_muitoalto_to_Visible ;
      private int edtavTfvariavelcocomo_extraalto_Visible ;
      private int edtavTfvariavelcocomo_extraalto_to_Visible ;
      private int edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Visible ;
      private int AV85WWVariaveisCocomoDS_1_Variavelcocomo_areatrabalhocod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavVariavelcocomo_sigla1_Visible ;
      private int edtavVariavelcocomo_sigla2_Visible ;
      private int edtavVariavelcocomo_sigla3_Visible ;
      private int AV119GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV55TFVariavelCocomo_ExtraBaixo ;
      private decimal AV56TFVariavelCocomo_ExtraBaixo_To ;
      private decimal AV59TFVariavelCocomo_MuitoBaixo ;
      private decimal AV60TFVariavelCocomo_MuitoBaixo_To ;
      private decimal AV63TFVariavelCocomo_Baixo ;
      private decimal AV64TFVariavelCocomo_Baixo_To ;
      private decimal AV67TFVariavelCocomo_Nominal ;
      private decimal AV68TFVariavelCocomo_Nominal_To ;
      private decimal AV71TFVariavelCocomo_Alto ;
      private decimal AV72TFVariavelCocomo_Alto_To ;
      private decimal AV75TFVariavelCocomo_MuitoAlto ;
      private decimal AV76TFVariavelCocomo_MuitoAlto_To ;
      private decimal AV79TFVariavelCocomo_ExtraAlto ;
      private decimal AV80TFVariavelCocomo_ExtraAlto_To ;
      private decimal AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ;
      private decimal AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ;
      private decimal AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ;
      private decimal AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ;
      private decimal AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ;
      private decimal AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ;
      private decimal AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ;
      private decimal AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ;
      private decimal AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ;
      private decimal AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ;
      private decimal AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ;
      private decimal AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ;
      private decimal AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ;
      private decimal AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private String Ddo_variavelcocomo_sigla_Activeeventkey ;
      private String Ddo_variavelcocomo_sigla_Filteredtext_get ;
      private String Ddo_variavelcocomo_sigla_Selectedvalue_get ;
      private String Ddo_variavelcocomo_tipo_Activeeventkey ;
      private String Ddo_variavelcocomo_tipo_Selectedvalue_get ;
      private String Ddo_variavelcocomo_data_Activeeventkey ;
      private String Ddo_variavelcocomo_data_Filteredtext_get ;
      private String Ddo_variavelcocomo_data_Filteredtextto_get ;
      private String Ddo_variavelcocomo_extrabaixo_Activeeventkey ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_muitobaixo_Activeeventkey ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_baixo_Activeeventkey ;
      private String Ddo_variavelcocomo_baixo_Filteredtext_get ;
      private String Ddo_variavelcocomo_baixo_Filteredtextto_get ;
      private String Ddo_variavelcocomo_nominal_Activeeventkey ;
      private String Ddo_variavelcocomo_nominal_Filteredtext_get ;
      private String Ddo_variavelcocomo_nominal_Filteredtextto_get ;
      private String Ddo_variavelcocomo_alto_Activeeventkey ;
      private String Ddo_variavelcocomo_alto_Filteredtext_get ;
      private String Ddo_variavelcocomo_alto_Filteredtextto_get ;
      private String Ddo_variavelcocomo_muitoalto_Activeeventkey ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtext_get ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtextto_get ;
      private String Ddo_variavelcocomo_extraalto_Activeeventkey ;
      private String Ddo_variavelcocomo_extraalto_Filteredtext_get ;
      private String Ddo_variavelcocomo_extraalto_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_77_idx="0001" ;
      private String AV32VariavelCocomo_Sigla1 ;
      private String AV35VariavelCocomo_Tipo1 ;
      private String AV33VariavelCocomo_Sigla2 ;
      private String AV36VariavelCocomo_Tipo2 ;
      private String AV34VariavelCocomo_Sigla3 ;
      private String AV37VariavelCocomo_Tipo3 ;
      private String AV41TFVariavelCocomo_Sigla ;
      private String AV42TFVariavelCocomo_Sigla_Sel ;
      private String AV118Pgmname ;
      private String A962VariavelCocomo_Sigla ;
      private String A964VariavelCocomo_Tipo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_variavelcocomo_sigla_Caption ;
      private String Ddo_variavelcocomo_sigla_Tooltip ;
      private String Ddo_variavelcocomo_sigla_Cls ;
      private String Ddo_variavelcocomo_sigla_Filteredtext_set ;
      private String Ddo_variavelcocomo_sigla_Selectedvalue_set ;
      private String Ddo_variavelcocomo_sigla_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_sigla_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_sigla_Sortedstatus ;
      private String Ddo_variavelcocomo_sigla_Filtertype ;
      private String Ddo_variavelcocomo_sigla_Datalisttype ;
      private String Ddo_variavelcocomo_sigla_Datalistproc ;
      private String Ddo_variavelcocomo_sigla_Sortasc ;
      private String Ddo_variavelcocomo_sigla_Sortdsc ;
      private String Ddo_variavelcocomo_sigla_Loadingdata ;
      private String Ddo_variavelcocomo_sigla_Cleanfilter ;
      private String Ddo_variavelcocomo_sigla_Noresultsfound ;
      private String Ddo_variavelcocomo_sigla_Searchbuttontext ;
      private String Ddo_variavelcocomo_tipo_Caption ;
      private String Ddo_variavelcocomo_tipo_Tooltip ;
      private String Ddo_variavelcocomo_tipo_Cls ;
      private String Ddo_variavelcocomo_tipo_Selectedvalue_set ;
      private String Ddo_variavelcocomo_tipo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_tipo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_tipo_Sortedstatus ;
      private String Ddo_variavelcocomo_tipo_Datalisttype ;
      private String Ddo_variavelcocomo_tipo_Datalistfixedvalues ;
      private String Ddo_variavelcocomo_tipo_Sortasc ;
      private String Ddo_variavelcocomo_tipo_Sortdsc ;
      private String Ddo_variavelcocomo_tipo_Cleanfilter ;
      private String Ddo_variavelcocomo_tipo_Searchbuttontext ;
      private String Ddo_variavelcocomo_data_Caption ;
      private String Ddo_variavelcocomo_data_Tooltip ;
      private String Ddo_variavelcocomo_data_Cls ;
      private String Ddo_variavelcocomo_data_Filteredtext_set ;
      private String Ddo_variavelcocomo_data_Filteredtextto_set ;
      private String Ddo_variavelcocomo_data_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_data_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_data_Sortedstatus ;
      private String Ddo_variavelcocomo_data_Filtertype ;
      private String Ddo_variavelcocomo_data_Sortasc ;
      private String Ddo_variavelcocomo_data_Sortdsc ;
      private String Ddo_variavelcocomo_data_Cleanfilter ;
      private String Ddo_variavelcocomo_data_Rangefilterfrom ;
      private String Ddo_variavelcocomo_data_Rangefilterto ;
      private String Ddo_variavelcocomo_data_Searchbuttontext ;
      private String Ddo_variavelcocomo_extrabaixo_Caption ;
      private String Ddo_variavelcocomo_extrabaixo_Tooltip ;
      private String Ddo_variavelcocomo_extrabaixo_Cls ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_extrabaixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_extrabaixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_extrabaixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_extrabaixo_Filtertype ;
      private String Ddo_variavelcocomo_extrabaixo_Cleanfilter ;
      private String Ddo_variavelcocomo_extrabaixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_extrabaixo_Rangefilterto ;
      private String Ddo_variavelcocomo_extrabaixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_muitobaixo_Caption ;
      private String Ddo_variavelcocomo_muitobaixo_Tooltip ;
      private String Ddo_variavelcocomo_muitobaixo_Cls ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_muitobaixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_muitobaixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_muitobaixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_muitobaixo_Filtertype ;
      private String Ddo_variavelcocomo_muitobaixo_Cleanfilter ;
      private String Ddo_variavelcocomo_muitobaixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_muitobaixo_Rangefilterto ;
      private String Ddo_variavelcocomo_muitobaixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_baixo_Caption ;
      private String Ddo_variavelcocomo_baixo_Tooltip ;
      private String Ddo_variavelcocomo_baixo_Cls ;
      private String Ddo_variavelcocomo_baixo_Filteredtext_set ;
      private String Ddo_variavelcocomo_baixo_Filteredtextto_set ;
      private String Ddo_variavelcocomo_baixo_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_baixo_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_baixo_Filtertype ;
      private String Ddo_variavelcocomo_baixo_Cleanfilter ;
      private String Ddo_variavelcocomo_baixo_Rangefilterfrom ;
      private String Ddo_variavelcocomo_baixo_Rangefilterto ;
      private String Ddo_variavelcocomo_baixo_Searchbuttontext ;
      private String Ddo_variavelcocomo_nominal_Caption ;
      private String Ddo_variavelcocomo_nominal_Tooltip ;
      private String Ddo_variavelcocomo_nominal_Cls ;
      private String Ddo_variavelcocomo_nominal_Filteredtext_set ;
      private String Ddo_variavelcocomo_nominal_Filteredtextto_set ;
      private String Ddo_variavelcocomo_nominal_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_nominal_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_nominal_Filtertype ;
      private String Ddo_variavelcocomo_nominal_Cleanfilter ;
      private String Ddo_variavelcocomo_nominal_Rangefilterfrom ;
      private String Ddo_variavelcocomo_nominal_Rangefilterto ;
      private String Ddo_variavelcocomo_nominal_Searchbuttontext ;
      private String Ddo_variavelcocomo_alto_Caption ;
      private String Ddo_variavelcocomo_alto_Tooltip ;
      private String Ddo_variavelcocomo_alto_Cls ;
      private String Ddo_variavelcocomo_alto_Filteredtext_set ;
      private String Ddo_variavelcocomo_alto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_alto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_alto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_alto_Filtertype ;
      private String Ddo_variavelcocomo_alto_Cleanfilter ;
      private String Ddo_variavelcocomo_alto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_alto_Rangefilterto ;
      private String Ddo_variavelcocomo_alto_Searchbuttontext ;
      private String Ddo_variavelcocomo_muitoalto_Caption ;
      private String Ddo_variavelcocomo_muitoalto_Tooltip ;
      private String Ddo_variavelcocomo_muitoalto_Cls ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtext_set ;
      private String Ddo_variavelcocomo_muitoalto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_muitoalto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_muitoalto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_muitoalto_Filtertype ;
      private String Ddo_variavelcocomo_muitoalto_Cleanfilter ;
      private String Ddo_variavelcocomo_muitoalto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_muitoalto_Rangefilterto ;
      private String Ddo_variavelcocomo_muitoalto_Searchbuttontext ;
      private String Ddo_variavelcocomo_extraalto_Caption ;
      private String Ddo_variavelcocomo_extraalto_Tooltip ;
      private String Ddo_variavelcocomo_extraalto_Cls ;
      private String Ddo_variavelcocomo_extraalto_Filteredtext_set ;
      private String Ddo_variavelcocomo_extraalto_Filteredtextto_set ;
      private String Ddo_variavelcocomo_extraalto_Dropdownoptionstype ;
      private String Ddo_variavelcocomo_extraalto_Titlecontrolidtoreplace ;
      private String Ddo_variavelcocomo_extraalto_Filtertype ;
      private String Ddo_variavelcocomo_extraalto_Cleanfilter ;
      private String Ddo_variavelcocomo_extraalto_Rangefilterfrom ;
      private String Ddo_variavelcocomo_extraalto_Rangefilterto ;
      private String Ddo_variavelcocomo_extraalto_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfvariavelcocomo_sigla_Internalname ;
      private String edtavTfvariavelcocomo_sigla_Jsonclick ;
      private String edtavTfvariavelcocomo_sigla_sel_Internalname ;
      private String edtavTfvariavelcocomo_sigla_sel_Jsonclick ;
      private String edtavTfvariavelcocomo_data_Internalname ;
      private String edtavTfvariavelcocomo_data_Jsonclick ;
      private String edtavTfvariavelcocomo_data_to_Internalname ;
      private String edtavTfvariavelcocomo_data_to_Jsonclick ;
      private String divDdo_variavelcocomo_dataauxdates_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdate_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdate_Jsonclick ;
      private String edtavDdo_variavelcocomo_dataauxdateto_Internalname ;
      private String edtavDdo_variavelcocomo_dataauxdateto_Jsonclick ;
      private String edtavTfvariavelcocomo_extrabaixo_Internalname ;
      private String edtavTfvariavelcocomo_extrabaixo_Jsonclick ;
      private String edtavTfvariavelcocomo_extrabaixo_to_Internalname ;
      private String edtavTfvariavelcocomo_extrabaixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_muitobaixo_Internalname ;
      private String edtavTfvariavelcocomo_muitobaixo_Jsonclick ;
      private String edtavTfvariavelcocomo_muitobaixo_to_Internalname ;
      private String edtavTfvariavelcocomo_muitobaixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_baixo_Internalname ;
      private String edtavTfvariavelcocomo_baixo_Jsonclick ;
      private String edtavTfvariavelcocomo_baixo_to_Internalname ;
      private String edtavTfvariavelcocomo_baixo_to_Jsonclick ;
      private String edtavTfvariavelcocomo_nominal_Internalname ;
      private String edtavTfvariavelcocomo_nominal_Jsonclick ;
      private String edtavTfvariavelcocomo_nominal_to_Internalname ;
      private String edtavTfvariavelcocomo_nominal_to_Jsonclick ;
      private String edtavTfvariavelcocomo_alto_Internalname ;
      private String edtavTfvariavelcocomo_alto_Jsonclick ;
      private String edtavTfvariavelcocomo_alto_to_Internalname ;
      private String edtavTfvariavelcocomo_alto_to_Jsonclick ;
      private String edtavTfvariavelcocomo_muitoalto_Internalname ;
      private String edtavTfvariavelcocomo_muitoalto_Jsonclick ;
      private String edtavTfvariavelcocomo_muitoalto_to_Internalname ;
      private String edtavTfvariavelcocomo_muitoalto_to_Jsonclick ;
      private String edtavTfvariavelcocomo_extraalto_Internalname ;
      private String edtavTfvariavelcocomo_extraalto_Jsonclick ;
      private String edtavTfvariavelcocomo_extraalto_to_Internalname ;
      private String edtavTfvariavelcocomo_extraalto_to_Jsonclick ;
      private String edtavDdo_variavelcocomo_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_extrabaixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_muitobaixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_baixotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_nominaltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_altotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_muitoaltotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_variavelcocomo_extraaltotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ;
      private String AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ;
      private String AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ;
      private String AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ;
      private String AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ;
      private String AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ;
      private String AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ;
      private String AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtVariavelCocomo_Sigla_Internalname ;
      private String cmbVariavelCocomo_Tipo_Internalname ;
      private String edtVariavelCocomo_Data_Internalname ;
      private String edtVariavelCocomo_ExtraBaixo_Internalname ;
      private String edtVariavelCocomo_MuitoBaixo_Internalname ;
      private String edtVariavelCocomo_Baixo_Internalname ;
      private String edtVariavelCocomo_Nominal_Internalname ;
      private String edtVariavelCocomo_Alto_Internalname ;
      private String edtVariavelCocomo_MuitoAlto_Internalname ;
      private String edtVariavelCocomo_ExtraAlto_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ;
      private String lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ;
      private String lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ;
      private String lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavVariavelcocomo_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavVariavelcocomo_sigla1_Internalname ;
      private String cmbavVariavelcocomo_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavVariavelcocomo_sigla2_Internalname ;
      private String cmbavVariavelcocomo_tipo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavVariavelcocomo_sigla3_Internalname ;
      private String cmbavVariavelcocomo_tipo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_variavelcocomo_sigla_Internalname ;
      private String Ddo_variavelcocomo_tipo_Internalname ;
      private String Ddo_variavelcocomo_data_Internalname ;
      private String Ddo_variavelcocomo_extrabaixo_Internalname ;
      private String Ddo_variavelcocomo_muitobaixo_Internalname ;
      private String Ddo_variavelcocomo_baixo_Internalname ;
      private String Ddo_variavelcocomo_nominal_Internalname ;
      private String Ddo_variavelcocomo_alto_Internalname ;
      private String Ddo_variavelcocomo_muitoalto_Internalname ;
      private String Ddo_variavelcocomo_extraalto_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtVariavelCocomo_Sigla_Title ;
      private String edtVariavelCocomo_Data_Title ;
      private String edtVariavelCocomo_ExtraBaixo_Title ;
      private String edtVariavelCocomo_MuitoBaixo_Title ;
      private String edtVariavelCocomo_Baixo_Title ;
      private String edtVariavelCocomo_Nominal_Title ;
      private String edtVariavelCocomo_Alto_Title ;
      private String edtVariavelCocomo_MuitoAlto_Title ;
      private String edtVariavelCocomo_ExtraAlto_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblVariaveiscocomotitle_Internalname ;
      private String lblVariaveiscocomotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextvariavelcocomo_areatrabalhocod_Internalname ;
      private String lblFiltertextvariavelcocomo_areatrabalhocod_Jsonclick ;
      private String edtavVariavelcocomo_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavVariavelcocomo_sigla1_Jsonclick ;
      private String cmbavVariavelcocomo_tipo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavVariavelcocomo_sigla2_Jsonclick ;
      private String cmbavVariavelcocomo_tipo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavVariavelcocomo_sigla3_Jsonclick ;
      private String cmbavVariavelcocomo_tipo3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_77_fel_idx="0001" ;
      private String ROClassString ;
      private String edtVariavelCocomo_Sigla_Jsonclick ;
      private String cmbVariavelCocomo_Tipo_Jsonclick ;
      private String edtVariavelCocomo_Data_Jsonclick ;
      private String edtVariavelCocomo_ExtraBaixo_Jsonclick ;
      private String edtVariavelCocomo_MuitoBaixo_Jsonclick ;
      private String edtVariavelCocomo_Baixo_Jsonclick ;
      private String edtVariavelCocomo_Nominal_Jsonclick ;
      private String edtVariavelCocomo_Alto_Jsonclick ;
      private String edtVariavelCocomo_MuitoAlto_Jsonclick ;
      private String edtVariavelCocomo_ExtraAlto_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV49TFVariavelCocomo_Data ;
      private DateTime AV50TFVariavelCocomo_Data_To ;
      private DateTime AV51DDO_VariavelCocomo_DataAuxDate ;
      private DateTime AV52DDO_VariavelCocomo_DataAuxDateTo ;
      private DateTime AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ;
      private DateTime AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ;
      private DateTime A966VariavelCocomo_Data ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Ddo_variavelcocomo_sigla_Includesortasc ;
      private bool Ddo_variavelcocomo_sigla_Includesortdsc ;
      private bool Ddo_variavelcocomo_sigla_Includefilter ;
      private bool Ddo_variavelcocomo_sigla_Filterisrange ;
      private bool Ddo_variavelcocomo_sigla_Includedatalist ;
      private bool Ddo_variavelcocomo_tipo_Includesortasc ;
      private bool Ddo_variavelcocomo_tipo_Includesortdsc ;
      private bool Ddo_variavelcocomo_tipo_Includefilter ;
      private bool Ddo_variavelcocomo_tipo_Includedatalist ;
      private bool Ddo_variavelcocomo_tipo_Allowmultipleselection ;
      private bool Ddo_variavelcocomo_data_Includesortasc ;
      private bool Ddo_variavelcocomo_data_Includesortdsc ;
      private bool Ddo_variavelcocomo_data_Includefilter ;
      private bool Ddo_variavelcocomo_data_Filterisrange ;
      private bool Ddo_variavelcocomo_data_Includedatalist ;
      private bool Ddo_variavelcocomo_extrabaixo_Includesortasc ;
      private bool Ddo_variavelcocomo_extrabaixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_extrabaixo_Includefilter ;
      private bool Ddo_variavelcocomo_extrabaixo_Filterisrange ;
      private bool Ddo_variavelcocomo_extrabaixo_Includedatalist ;
      private bool Ddo_variavelcocomo_muitobaixo_Includesortasc ;
      private bool Ddo_variavelcocomo_muitobaixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_muitobaixo_Includefilter ;
      private bool Ddo_variavelcocomo_muitobaixo_Filterisrange ;
      private bool Ddo_variavelcocomo_muitobaixo_Includedatalist ;
      private bool Ddo_variavelcocomo_baixo_Includesortasc ;
      private bool Ddo_variavelcocomo_baixo_Includesortdsc ;
      private bool Ddo_variavelcocomo_baixo_Includefilter ;
      private bool Ddo_variavelcocomo_baixo_Filterisrange ;
      private bool Ddo_variavelcocomo_baixo_Includedatalist ;
      private bool Ddo_variavelcocomo_nominal_Includesortasc ;
      private bool Ddo_variavelcocomo_nominal_Includesortdsc ;
      private bool Ddo_variavelcocomo_nominal_Includefilter ;
      private bool Ddo_variavelcocomo_nominal_Filterisrange ;
      private bool Ddo_variavelcocomo_nominal_Includedatalist ;
      private bool Ddo_variavelcocomo_alto_Includesortasc ;
      private bool Ddo_variavelcocomo_alto_Includesortdsc ;
      private bool Ddo_variavelcocomo_alto_Includefilter ;
      private bool Ddo_variavelcocomo_alto_Filterisrange ;
      private bool Ddo_variavelcocomo_alto_Includedatalist ;
      private bool Ddo_variavelcocomo_muitoalto_Includesortasc ;
      private bool Ddo_variavelcocomo_muitoalto_Includesortdsc ;
      private bool Ddo_variavelcocomo_muitoalto_Includefilter ;
      private bool Ddo_variavelcocomo_muitoalto_Filterisrange ;
      private bool Ddo_variavelcocomo_muitoalto_Includedatalist ;
      private bool Ddo_variavelcocomo_extraalto_Includesortasc ;
      private bool Ddo_variavelcocomo_extraalto_Includesortdsc ;
      private bool Ddo_variavelcocomo_extraalto_Includefilter ;
      private bool Ddo_variavelcocomo_extraalto_Filterisrange ;
      private bool Ddo_variavelcocomo_extraalto_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ;
      private bool AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ;
      private bool n966VariavelCocomo_Data ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n970VariavelCocomo_Alto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV45TFVariavelCocomo_Tipo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV43ddo_VariavelCocomo_SiglaTitleControlIdToReplace ;
      private String AV47ddo_VariavelCocomo_TipoTitleControlIdToReplace ;
      private String AV53ddo_VariavelCocomo_DataTitleControlIdToReplace ;
      private String AV57ddo_VariavelCocomo_ExtraBaixoTitleControlIdToReplace ;
      private String AV61ddo_VariavelCocomo_MuitoBaixoTitleControlIdToReplace ;
      private String AV65ddo_VariavelCocomo_BaixoTitleControlIdToReplace ;
      private String AV69ddo_VariavelCocomo_NominalTitleControlIdToReplace ;
      private String AV73ddo_VariavelCocomo_AltoTitleControlIdToReplace ;
      private String AV77ddo_VariavelCocomo_MuitoAltoTitleControlIdToReplace ;
      private String AV81ddo_VariavelCocomo_ExtraAltoTitleControlIdToReplace ;
      private String AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ;
      private String AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ;
      private String AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ;
      private String AV116Update_GXI ;
      private String AV117Delete_GXI ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavVariavelcocomo_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavVariavelcocomo_tipo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavVariavelcocomo_tipo3 ;
      private GXCombobox cmbVariavelCocomo_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00GK2_A992VariavelCocomo_Sequencial ;
      private int[] H00GK2_A961VariavelCocomo_AreaTrabalhoCod ;
      private decimal[] H00GK2_A972VariavelCocomo_ExtraAlto ;
      private bool[] H00GK2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] H00GK2_A971VariavelCocomo_MuitoAlto ;
      private bool[] H00GK2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] H00GK2_A970VariavelCocomo_Alto ;
      private bool[] H00GK2_n970VariavelCocomo_Alto ;
      private decimal[] H00GK2_A969VariavelCocomo_Nominal ;
      private bool[] H00GK2_n969VariavelCocomo_Nominal ;
      private decimal[] H00GK2_A968VariavelCocomo_Baixo ;
      private bool[] H00GK2_n968VariavelCocomo_Baixo ;
      private decimal[] H00GK2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] H00GK2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] H00GK2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] H00GK2_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] H00GK2_A966VariavelCocomo_Data ;
      private bool[] H00GK2_n966VariavelCocomo_Data ;
      private String[] H00GK2_A964VariavelCocomo_Tipo ;
      private String[] H00GK2_A962VariavelCocomo_Sigla ;
      private long[] H00GK3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46TFVariavelCocomo_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40VariavelCocomo_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44VariavelCocomo_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48VariavelCocomo_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54VariavelCocomo_ExtraBaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58VariavelCocomo_MuitoBaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62VariavelCocomo_BaixoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66VariavelCocomo_NominalTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70VariavelCocomo_AltoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74VariavelCocomo_MuitoAltoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78VariavelCocomo_ExtraAltoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV82DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwvariaveiscocomo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GK2( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                             String AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                             String AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                             String AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                             bool AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                             String AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                             String AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                             String AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                             bool AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                             String AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                             String AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                             String AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                             String AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                             String AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                             int AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count ,
                                             DateTime AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                             DateTime AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                             decimal AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                             decimal AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                             decimal AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                             decimal AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                             decimal AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                             decimal AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                             decimal AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                             decimal AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                             decimal AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                             decimal AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                             decimal AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                             decimal AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                             decimal AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                             decimal AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                             String A962VariavelCocomo_Sigla ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [VariavelCocomo_Sequencial], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_Tipo], [VariavelCocomo_Sigla]";
         sFromString = " FROM [VariaveisCocomo] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Sigla]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Tipo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Data]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GK3( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels ,
                                             String AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1 ,
                                             String AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 ,
                                             String AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1 ,
                                             bool AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 ,
                                             String AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2 ,
                                             String AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 ,
                                             String AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2 ,
                                             bool AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 ,
                                             String AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3 ,
                                             String AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 ,
                                             String AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3 ,
                                             String AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel ,
                                             String AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla ,
                                             int AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count ,
                                             DateTime AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data ,
                                             DateTime AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to ,
                                             decimal AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo ,
                                             decimal AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to ,
                                             decimal AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo ,
                                             decimal AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to ,
                                             decimal AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo ,
                                             decimal AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to ,
                                             decimal AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal ,
                                             decimal AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to ,
                                             decimal AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto ,
                                             decimal AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to ,
                                             decimal AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto ,
                                             decimal AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to ,
                                             decimal AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto ,
                                             decimal AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to ,
                                             String A962VariavelCocomo_Sigla ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [VariaveisCocomo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV86WWVariaveisCocomoDS_2_Dynamicfiltersselector1, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV89WWVariaveisCocomoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV90WWVariaveisCocomoDS_6_Dynamicfiltersselector2, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV93WWVariaveisCocomoDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV94WWVariaveisCocomoDS_10_Dynamicfiltersselector3, "VARIAVELCOCOMO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Tipo] = @AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)) ) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99WWVariaveisCocomoDS_15_Tfvariavelcocomo_tipo_sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
         }
         if ( ! (DateTime.MinValue==AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to) )
         {
            sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GK2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] );
               case 1 :
                     return conditional_H00GK3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (short)dynConstraints[41] , (bool)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GK2 ;
          prmH00GK2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GK3 ;
          prmH00GK3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV87WWVariaveisCocomoDS_3_Variavelcocomo_sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV88WWVariaveisCocomoDS_4_Variavelcocomo_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV91WWVariaveisCocomoDS_7_Variavelcocomo_sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV92WWVariaveisCocomoDS_8_Variavelcocomo_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV95WWVariaveisCocomoDS_11_Variavelcocomo_sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV96WWVariaveisCocomoDS_12_Variavelcocomo_tipo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV97WWVariaveisCocomoDS_13_Tfvariavelcocomo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV98WWVariaveisCocomoDS_14_Tfvariavelcocomo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV100WWVariaveisCocomoDS_16_Tfvariavelcocomo_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWVariaveisCocomoDS_17_Tfvariavelcocomo_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWVariaveisCocomoDS_18_Tfvariavelcocomo_extrabaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV103WWVariaveisCocomoDS_19_Tfvariavelcocomo_extrabaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV104WWVariaveisCocomoDS_20_Tfvariavelcocomo_muitobaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV105WWVariaveisCocomoDS_21_Tfvariavelcocomo_muitobaixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV106WWVariaveisCocomoDS_22_Tfvariavelcocomo_baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV107WWVariaveisCocomoDS_23_Tfvariavelcocomo_baixo_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV108WWVariaveisCocomoDS_24_Tfvariavelcocomo_nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV109WWVariaveisCocomoDS_25_Tfvariavelcocomo_nominal_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV110WWVariaveisCocomoDS_26_Tfvariavelcocomo_alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV111WWVariaveisCocomoDS_27_Tfvariavelcocomo_alto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV112WWVariaveisCocomoDS_28_Tfvariavelcocomo_muitoalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV113WWVariaveisCocomoDS_29_Tfvariavelcocomo_muitoalto_to",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV114WWVariaveisCocomoDS_30_Tfvariavelcocomo_extraalto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV115WWVariaveisCocomoDS_31_Tfvariavelcocomo_extraalto_to",SqlDbType.Decimal,12,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GK2,11,0,true,false )
             ,new CursorDef("H00GK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GK3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                return;
       }
    }

 }

}
