/*
               File: type_SdtQueryViewerDragAndDropData
        Description: QueryViewerDragAndDropData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:57.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerDragAndDropData" )]
   [XmlType(TypeName =  "QueryViewerDragAndDropData" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtQueryViewerDragAndDropData : GxUserType
   {
      public SdtQueryViewerDragAndDropData( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerDragAndDropData_Name = "";
         gxTv_SdtQueryViewerDragAndDropData_Axis = "";
      }

      public SdtQueryViewerDragAndDropData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerDragAndDropData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerDragAndDropData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerDragAndDropData obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Axis = deserialized.gxTpr_Axis;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerDragAndDropData_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Axis") )
               {
                  gxTv_SdtQueryViewerDragAndDropData_Axis = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerDragAndDropData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerDragAndDropData_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Axis", StringUtil.RTrim( gxTv_SdtQueryViewerDragAndDropData_Axis));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerDragAndDropData_Name, false);
         AddObjectProperty("Axis", gxTv_SdtQueryViewerDragAndDropData_Axis, false);
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerDragAndDropData_Name ;
         }

         set {
            gxTv_SdtQueryViewerDragAndDropData_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Axis" )]
      [  XmlElement( ElementName = "Axis"   )]
      public String gxTpr_Axis
      {
         get {
            return gxTv_SdtQueryViewerDragAndDropData_Axis ;
         }

         set {
            gxTv_SdtQueryViewerDragAndDropData_Axis = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerDragAndDropData_Name = "";
         gxTv_SdtQueryViewerDragAndDropData_Axis = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerDragAndDropData_Name ;
      protected String gxTv_SdtQueryViewerDragAndDropData_Axis ;
      protected String sTagName ;
   }

   [DataContract(Name = @"QueryViewerDragAndDropData", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtQueryViewerDragAndDropData_RESTInterface : GxGenericCollectionItem<SdtQueryViewerDragAndDropData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerDragAndDropData_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerDragAndDropData_RESTInterface( SdtQueryViewerDragAndDropData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Axis" , Order = 1 )]
      public String gxTpr_Axis
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Axis) ;
         }

         set {
            sdt.gxTpr_Axis = (String)(value);
         }

      }

      public SdtQueryViewerDragAndDropData sdt
      {
         get {
            return (SdtQueryViewerDragAndDropData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerDragAndDropData() ;
         }
      }

   }

}
