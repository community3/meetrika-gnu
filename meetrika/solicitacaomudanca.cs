/*
               File: SolicitacaoMudanca
        Description: Solicita��o de Mudan�a
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:5.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaomudanca : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A993SolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A993SolicitacaoMudanca_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A994SolicitacaoMudanca_Solicitante = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A994SolicitacaoMudanca_Solicitante) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SolicitacaoMudanca_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicita��o de Mudan�a", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacaomudanca( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaomudanca( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_SolicitacaoMudanca_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7SolicitacaoMudanca_Codigo = aP1_SolicitacaoMudanca_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2Z121( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2Z121e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2Z121( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2Z121( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2Z121e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_33_2Z121( true) ;
         }
         return  ;
      }

      protected void wb_table3_33_2Z121e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2Z121e( true) ;
         }
         else
         {
            wb_table1_2_2Z121e( false) ;
         }
      }

      protected void wb_table3_33_2Z121( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_33_2Z121e( true) ;
         }
         else
         {
            wb_table3_33_2Z121e( false) ;
         }
      }

      protected void wb_table2_5_2Z121( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2Z121( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2Z121e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2Z121e( true) ;
         }
         else
         {
            wb_table2_5_2Z121e( false) ;
         }
      }

      protected void wb_table4_13_2Z121( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudanca_data_Internalname, "Data", "", "", lblTextblocksolicitacaomudanca_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtSolicitacaoMudanca_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_Data_Internalname, context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"), context.localUtil.Format( A997SolicitacaoMudanca_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtSolicitacaoMudanca_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_SolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtSolicitacaoMudanca_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSolicitacaoMudanca_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudanca_sistemacod_Internalname, "Sistema", "", "", lblTextblocksolicitacaomudanca_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacaoMudanca_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacaoMudanca_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacaoMudanca_SistemaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname, "Item", "", "", lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudancaitem_funcaoapf_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")), ((edtavSolicitacaomudancaitem_funcaoapf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+"e112z121_client"+"'", "", "", "", "", edtavSolicitacaomudancaitem_funcaoapf_Jsonclick, 7, "BootstrapAttribute", "", "", "", 1, edtavSolicitacaomudancaitem_funcaoapf_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsertitem_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsertitem_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgInsertitem_Jsonclick, "'"+""+"'"+",false,"+"'"+"e122z121_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2Z121e( true) ;
         }
         else
         {
            wb_table4_13_2Z121e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E132Z2 */
         E132Z2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( context.localUtil.VCDate( cgiGet( edtSolicitacaoMudanca_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "SOLICITACAOMUDANCA_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A997SolicitacaoMudanca_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
               }
               else
               {
                  A997SolicitacaoMudanca_Data = context.localUtil.CToD( cgiGet( edtSolicitacaoMudanca_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACAOMUDANCA_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacaoMudanca_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A993SolicitacaoMudanca_SistemaCod = 0;
                  n993SolicitacaoMudanca_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
               }
               else
               {
                  A993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", "."));
                  n993SolicitacaoMudanca_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
               }
               n993SolicitacaoMudanca_SistemaCod = ((0==A993SolicitacaoMudanca_SistemaCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavSolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSOLICITACAOMUDANCAITEM_FUNCAOAPF");
                  AnyError = 1;
                  GX_FocusControl = edtavSolicitacaomudancaitem_funcaoapf_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV14SolicitacaoMudancaItem_FuncaoAPF = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               }
               else
               {
                  AV14SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( edtavSolicitacaomudancaitem_funcaoapf_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               }
               /* Read saved values. */
               Z996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z996SolicitacaoMudanca_Codigo"), ",", "."));
               Z997SolicitacaoMudanca_Data = context.localUtil.CToD( cgiGet( "Z997SolicitacaoMudanca_Data"), 0);
               Z993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z993SolicitacaoMudanca_SistemaCod"), ",", "."));
               n993SolicitacaoMudanca_SistemaCod = ((0==A993SolicitacaoMudanca_SistemaCod) ? true : false);
               Z994SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( "Z994SolicitacaoMudanca_Solicitante"), ",", "."));
               A994SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( "Z994SolicitacaoMudanca_Solicitante"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N993SolicitacaoMudanca_SistemaCod"), ",", "."));
               n993SolicitacaoMudanca_SistemaCod = ((0==A993SolicitacaoMudanca_SistemaCod) ? true : false);
               N994SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( "N994SolicitacaoMudanca_Solicitante"), ",", "."));
               AV7SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSOLICITACAOMUDANCA_CODIGO"), ",", "."));
               A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( "SOLICITACAOMUDANCA_CODIGO"), ",", "."));
               AV11Insert_SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SOLICITACAOMUDANCA_SISTEMACOD"), ",", "."));
               AV12Insert_SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SOLICITACAOMUDANCA_SOLICITANTE"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A994SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( "SOLICITACAOMUDANCA_SOLICITANTE"), ",", "."));
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SolicitacaoMudanca";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("solicitacaomudanca:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("solicitacaomudanca:[SecurityCheckFailed value for]"+"SolicitacaoMudanca_Codigo:"+context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("solicitacaomudanca:[SecurityCheckFailed value for]"+"SolicitacaoMudanca_Solicitante:"+context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A996SolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode121 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode121;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound121 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2Z0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132Z2 */
                           E132Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E142Z2 */
                           E142Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E142Z2 */
            E142Z2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2Z121( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2Z121( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitacaomudancaitem_funcaoapf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacaomudancaitem_funcaoapf_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2Z0( )
      {
         BeforeValidate2Z121( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2Z121( ) ;
            }
            else
            {
               CheckExtendedTable2Z121( ) ;
               CloseExtendedTableCursors2Z121( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2Z0( )
      {
      }

      protected void E132Z2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "SolicitacaoMudanca_SistemaCod") == 0 )
               {
                  AV11Insert_SolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_SolicitacaoMudanca_SistemaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "SolicitacaoMudanca_Solicitante") == 0 )
               {
                  AV12Insert_SolicitacaoMudanca_Solicitante = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_SolicitacaoMudanca_Solicitante), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         imgInsertitem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsertitem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsertitem_Visible), 5, 0)));
      }

      protected void E142Z2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsolicitacaomudanca.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2Z121( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z997SolicitacaoMudanca_Data = T002Z3_A997SolicitacaoMudanca_Data[0];
               Z993SolicitacaoMudanca_SistemaCod = T002Z3_A993SolicitacaoMudanca_SistemaCod[0];
               Z994SolicitacaoMudanca_Solicitante = T002Z3_A994SolicitacaoMudanca_Solicitante[0];
            }
            else
            {
               Z997SolicitacaoMudanca_Data = A997SolicitacaoMudanca_Data;
               Z993SolicitacaoMudanca_SistemaCod = A993SolicitacaoMudanca_SistemaCod;
               Z994SolicitacaoMudanca_Solicitante = A994SolicitacaoMudanca_Solicitante;
            }
         }
         if ( GX_JID == -11 )
         {
            Z996SolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
            Z997SolicitacaoMudanca_Data = A997SolicitacaoMudanca_Data;
            Z993SolicitacaoMudanca_SistemaCod = A993SolicitacaoMudanca_SistemaCod;
            Z994SolicitacaoMudanca_Solicitante = A994SolicitacaoMudanca_Solicitante;
         }
      }

      protected void standaloneNotModal( )
      {
         AV16Pgmname = "SolicitacaoMudanca";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7SolicitacaoMudanca_Codigo) )
         {
            A996SolicitacaoMudanca_Codigo = AV7SolicitacaoMudanca_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_SolicitacaoMudanca_SistemaCod) )
         {
            edtSolicitacaoMudanca_SistemaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_SistemaCod_Enabled), 5, 0)));
         }
         else
         {
            edtSolicitacaoMudanca_SistemaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_SistemaCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_SolicitacaoMudanca_SistemaCod) )
         {
            A993SolicitacaoMudanca_SistemaCod = AV11Insert_SolicitacaoMudanca_SistemaCod;
            n993SolicitacaoMudanca_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_SolicitacaoMudanca_Solicitante) )
         {
            A994SolicitacaoMudanca_Solicitante = AV12Insert_SolicitacaoMudanca_Solicitante;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A994SolicitacaoMudanca_Solicitante) && ( Gx_BScreen == 0 ) )
            {
               A994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A997SolicitacaoMudanca_Data) && ( Gx_BScreen == 0 ) )
         {
            A997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load2Z121( )
      {
         /* Using cursor T002Z6 */
         pr_default.execute(4, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound121 = 1;
            A997SolicitacaoMudanca_Data = T002Z6_A997SolicitacaoMudanca_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
            A993SolicitacaoMudanca_SistemaCod = T002Z6_A993SolicitacaoMudanca_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            n993SolicitacaoMudanca_SistemaCod = T002Z6_n993SolicitacaoMudanca_SistemaCod[0];
            A994SolicitacaoMudanca_Solicitante = T002Z6_A994SolicitacaoMudanca_Solicitante[0];
            ZM2Z121( -11) ;
         }
         pr_default.close(4);
         OnLoadActions2Z121( ) ;
      }

      protected void OnLoadActions2Z121( )
      {
      }

      protected void CheckExtendedTable2Z121( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A997SolicitacaoMudanca_Data) || ( A997SolicitacaoMudanca_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "SOLICITACAOMUDANCA_DATA");
            AnyError = 1;
            GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002Z4 */
         pr_default.execute(2, new Object[] {n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A993SolicitacaoMudanca_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Solicitacao Mudanca_Sistema'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtSolicitacaoMudanca_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         /* Using cursor T002Z5 */
         pr_default.execute(3, new Object[] {A994SolicitacaoMudanca_Solicitante});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacao Mudanca_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2Z121( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A993SolicitacaoMudanca_SistemaCod )
      {
         /* Using cursor T002Z7 */
         pr_default.execute(5, new Object[] {n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A993SolicitacaoMudanca_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Solicitacao Mudanca_Sistema'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtSolicitacaoMudanca_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_13( int A994SolicitacaoMudanca_Solicitante )
      {
         /* Using cursor T002Z8 */
         pr_default.execute(6, new Object[] {A994SolicitacaoMudanca_Solicitante});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacao Mudanca_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2Z121( )
      {
         /* Using cursor T002Z9 */
         pr_default.execute(7, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound121 = 1;
         }
         else
         {
            RcdFound121 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002Z3 */
         pr_default.execute(1, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2Z121( 11) ;
            RcdFound121 = 1;
            A996SolicitacaoMudanca_Codigo = T002Z3_A996SolicitacaoMudanca_Codigo[0];
            A997SolicitacaoMudanca_Data = T002Z3_A997SolicitacaoMudanca_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
            A993SolicitacaoMudanca_SistemaCod = T002Z3_A993SolicitacaoMudanca_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
            n993SolicitacaoMudanca_SistemaCod = T002Z3_n993SolicitacaoMudanca_SistemaCod[0];
            A994SolicitacaoMudanca_Solicitante = T002Z3_A994SolicitacaoMudanca_Solicitante[0];
            Z996SolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
            sMode121 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2Z121( ) ;
            if ( AnyError == 1 )
            {
               RcdFound121 = 0;
               InitializeNonKey2Z121( ) ;
            }
            Gx_mode = sMode121;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound121 = 0;
            InitializeNonKey2Z121( ) ;
            sMode121 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode121;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2Z121( ) ;
         if ( RcdFound121 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound121 = 0;
         /* Using cursor T002Z10 */
         pr_default.execute(8, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002Z10_A996SolicitacaoMudanca_Codigo[0] < A996SolicitacaoMudanca_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002Z10_A996SolicitacaoMudanca_Codigo[0] > A996SolicitacaoMudanca_Codigo ) ) )
            {
               A996SolicitacaoMudanca_Codigo = T002Z10_A996SolicitacaoMudanca_Codigo[0];
               RcdFound121 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound121 = 0;
         /* Using cursor T002Z11 */
         pr_default.execute(9, new Object[] {A996SolicitacaoMudanca_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002Z11_A996SolicitacaoMudanca_Codigo[0] > A996SolicitacaoMudanca_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002Z11_A996SolicitacaoMudanca_Codigo[0] < A996SolicitacaoMudanca_Codigo ) ) )
            {
               A996SolicitacaoMudanca_Codigo = T002Z11_A996SolicitacaoMudanca_Codigo[0];
               RcdFound121 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2Z121( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2Z121( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound121 == 1 )
            {
               if ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo )
               {
                  A996SolicitacaoMudanca_Codigo = Z996SolicitacaoMudanca_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2Z121( ) ;
                  GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2Z121( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2Z121( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A996SolicitacaoMudanca_Codigo != Z996SolicitacaoMudanca_Codigo )
         {
            A996SolicitacaoMudanca_Codigo = Z996SolicitacaoMudanca_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSolicitacaoMudanca_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2Z121( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002Z2 */
            pr_default.execute(0, new Object[] {A996SolicitacaoMudanca_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoMudanca"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z997SolicitacaoMudanca_Data != T002Z2_A997SolicitacaoMudanca_Data[0] ) || ( Z993SolicitacaoMudanca_SistemaCod != T002Z2_A993SolicitacaoMudanca_SistemaCod[0] ) || ( Z994SolicitacaoMudanca_Solicitante != T002Z2_A994SolicitacaoMudanca_Solicitante[0] ) )
            {
               if ( Z997SolicitacaoMudanca_Data != T002Z2_A997SolicitacaoMudanca_Data[0] )
               {
                  GXUtil.WriteLog("solicitacaomudanca:[seudo value changed for attri]"+"SolicitacaoMudanca_Data");
                  GXUtil.WriteLogRaw("Old: ",Z997SolicitacaoMudanca_Data);
                  GXUtil.WriteLogRaw("Current: ",T002Z2_A997SolicitacaoMudanca_Data[0]);
               }
               if ( Z993SolicitacaoMudanca_SistemaCod != T002Z2_A993SolicitacaoMudanca_SistemaCod[0] )
               {
                  GXUtil.WriteLog("solicitacaomudanca:[seudo value changed for attri]"+"SolicitacaoMudanca_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z993SolicitacaoMudanca_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T002Z2_A993SolicitacaoMudanca_SistemaCod[0]);
               }
               if ( Z994SolicitacaoMudanca_Solicitante != T002Z2_A994SolicitacaoMudanca_Solicitante[0] )
               {
                  GXUtil.WriteLog("solicitacaomudanca:[seudo value changed for attri]"+"SolicitacaoMudanca_Solicitante");
                  GXUtil.WriteLogRaw("Old: ",Z994SolicitacaoMudanca_Solicitante);
                  GXUtil.WriteLogRaw("Current: ",T002Z2_A994SolicitacaoMudanca_Solicitante[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacaoMudanca"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2Z121( )
      {
         BeforeValidate2Z121( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Z121( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2Z121( 0) ;
            CheckOptimisticConcurrency2Z121( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Z121( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2Z121( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Z12 */
                     pr_default.execute(10, new Object[] {A997SolicitacaoMudanca_Data, n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod, A994SolicitacaoMudanca_Solicitante});
                     A996SolicitacaoMudanca_Codigo = T002Z12_A996SolicitacaoMudanca_Codigo[0];
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudanca") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2Z0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2Z121( ) ;
            }
            EndLevel2Z121( ) ;
         }
         CloseExtendedTableCursors2Z121( ) ;
      }

      protected void Update2Z121( )
      {
         BeforeValidate2Z121( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Z121( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Z121( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Z121( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2Z121( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Z13 */
                     pr_default.execute(11, new Object[] {A997SolicitacaoMudanca_Data, n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod, A994SolicitacaoMudanca_Solicitante, A996SolicitacaoMudanca_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudanca") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacaoMudanca"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2Z121( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2Z121( ) ;
         }
         CloseExtendedTableCursors2Z121( ) ;
      }

      protected void DeferredUpdate2Z121( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2Z121( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Z121( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2Z121( ) ;
            AfterConfirm2Z121( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2Z121( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002Z14 */
                  pr_default.execute(12, new Object[] {A996SolicitacaoMudanca_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudanca") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode121 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2Z121( ) ;
         Gx_mode = sMode121;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2Z121( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T002Z15 */
            pr_default.execute(13, new Object[] {A996SolicitacaoMudanca_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item da Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel2Z121( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2Z121( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SolicitacaoMudanca");
            if ( AnyError == 0 )
            {
               ConfirmValues2Z0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SolicitacaoMudanca");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2Z121( )
      {
         /* Scan By routine */
         /* Using cursor T002Z16 */
         pr_default.execute(14);
         RcdFound121 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound121 = 1;
            A996SolicitacaoMudanca_Codigo = T002Z16_A996SolicitacaoMudanca_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2Z121( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound121 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound121 = 1;
            A996SolicitacaoMudanca_Codigo = T002Z16_A996SolicitacaoMudanca_Codigo[0];
         }
      }

      protected void ScanEnd2Z121( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm2Z121( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2Z121( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2Z121( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2Z121( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2Z121( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2Z121( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2Z121( )
      {
         edtSolicitacaoMudanca_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_Data_Enabled), 5, 0)));
         edtSolicitacaoMudanca_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacaoMudanca_SistemaCod_Enabled), 5, 0)));
         edtavSolicitacaomudancaitem_funcaoapf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitacaomudancaitem_funcaoapf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacaomudancaitem_funcaoapf_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2Z0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311726635");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaomudanca.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SolicitacaoMudanca_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z996SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z997SolicitacaoMudanca_Data", context.localUtil.DToC( Z997SolicitacaoMudanca_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z994SolicitacaoMudanca_Solicitante), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.NToC( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SOLICITACAOMUDANCA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SOLICITACAOMUDANCA_SOLICITANTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_SolicitacaoMudanca_Solicitante), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_SOLICITANTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SolicitacaoMudanca_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SolicitacaoMudanca";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacaomudanca:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("solicitacaomudanca:[SendSecurityCheck value for]"+"SolicitacaoMudanca_Codigo:"+context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("solicitacaomudanca:[SendSecurityCheck value for]"+"SolicitacaoMudanca_Solicitante:"+context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacaomudanca.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SolicitacaoMudanca_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoMudanca" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicita��o de Mudan�a" ;
      }

      protected void InitializeNonKey2Z121( )
      {
         A993SolicitacaoMudanca_SistemaCod = 0;
         n993SolicitacaoMudanca_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A993SolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0)));
         n993SolicitacaoMudanca_SistemaCod = ((0==A993SolicitacaoMudanca_SistemaCod) ? true : false);
         AV14SolicitacaoMudancaItem_FuncaoAPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         A994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0)));
         A997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
         Z997SolicitacaoMudanca_Data = DateTime.MinValue;
         Z993SolicitacaoMudanca_SistemaCod = 0;
         Z994SolicitacaoMudanca_Solicitante = 0;
      }

      protected void InitAll2Z121( )
      {
         A996SolicitacaoMudanca_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A996SolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0)));
         InitializeNonKey2Z121( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A994SolicitacaoMudanca_Solicitante = i994SolicitacaoMudanca_Solicitante;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A994SolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0)));
         A997SolicitacaoMudanca_Data = i997SolicitacaoMudanca_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A997SolicitacaoMudanca_Data", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311726650");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacaomudanca.js", "?2020311726650");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicitacaomudanca_data_Internalname = "TEXTBLOCKSOLICITACAOMUDANCA_DATA";
         edtSolicitacaoMudanca_Data_Internalname = "SOLICITACAOMUDANCA_DATA";
         lblTextblocksolicitacaomudanca_sistemacod_Internalname = "TEXTBLOCKSOLICITACAOMUDANCA_SISTEMACOD";
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname = "TEXTBLOCKSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtavSolicitacaomudancaitem_funcaoapf_Internalname = "vSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         imgInsertitem_Internalname = "INSERTITEM";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Solicita��o de Mudan�a";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicita��o de Mudan�a";
         imgInsertitem_Visible = 1;
         edtavSolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         edtavSolicitacaomudancaitem_funcaoapf_Enabled = 1;
         edtSolicitacaoMudanca_SistemaCod_Jsonclick = "";
         edtSolicitacaoMudanca_SistemaCod_Enabled = 1;
         edtSolicitacaoMudanca_Data_Jsonclick = "";
         edtSolicitacaoMudanca_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Solicitacaomudanca_sistemacod( int GX_Parm1 )
      {
         A993SolicitacaoMudanca_SistemaCod = GX_Parm1;
         n993SolicitacaoMudanca_SistemaCod = false;
         /* Using cursor T002Z17 */
         pr_default.execute(15, new Object[] {n993SolicitacaoMudanca_SistemaCod, A993SolicitacaoMudanca_SistemaCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A993SolicitacaoMudanca_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Solicitacao Mudanca_Sistema'.", "ForeignKeyNotFound", 1, "SOLICITACAOMUDANCA_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtSolicitacaoMudanca_SistemaCod_Internalname;
            }
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7SolicitacaoMudanca_Codigo',fld:'vSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E142Z2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOINSERTITEM'","{handler:'E122Z121',iparms:[],oparms:[{av:'imgInsertitem_Visible',ctrl:'INSERTITEM',prop:'Visible'},{av:'AV14SolicitacaoMudancaItem_FuncaoAPF',fld:'vSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSOLICITACAOMUDANCAITEM_FUNCAOAPF.CLICK","{handler:'E112Z121',iparms:[],oparms:[{av:'imgInsertitem_Visible',ctrl:'INSERTITEM',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z997SolicitacaoMudanca_Data = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksolicitacaomudanca_data_Jsonclick = "";
         A997SolicitacaoMudanca_Data = DateTime.MinValue;
         lblTextblocksolicitacaomudanca_sistemacod_Jsonclick = "";
         lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         imgInsertitem_Jsonclick = "";
         AV16Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode121 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T002Z6_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z6_A997SolicitacaoMudanca_Data = new DateTime[] {DateTime.MinValue} ;
         T002Z6_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z6_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T002Z6_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         T002Z4_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z4_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T002Z5_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         T002Z7_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z7_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T002Z8_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         T002Z9_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z3_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z3_A997SolicitacaoMudanca_Data = new DateTime[] {DateTime.MinValue} ;
         T002Z3_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z3_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T002Z3_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         T002Z10_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z11_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z2_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z2_A997SolicitacaoMudanca_Data = new DateTime[] {DateTime.MinValue} ;
         T002Z2_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z2_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         T002Z2_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         T002Z12_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z15_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T002Z15_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T002Z16_A996SolicitacaoMudanca_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i997SolicitacaoMudanca_Data = DateTime.MinValue;
         T002Z17_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         T002Z17_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaomudanca__default(),
            new Object[][] {
                new Object[] {
               T002Z2_A996SolicitacaoMudanca_Codigo, T002Z2_A997SolicitacaoMudanca_Data, T002Z2_A993SolicitacaoMudanca_SistemaCod, T002Z2_n993SolicitacaoMudanca_SistemaCod, T002Z2_A994SolicitacaoMudanca_Solicitante
               }
               , new Object[] {
               T002Z3_A996SolicitacaoMudanca_Codigo, T002Z3_A997SolicitacaoMudanca_Data, T002Z3_A993SolicitacaoMudanca_SistemaCod, T002Z3_n993SolicitacaoMudanca_SistemaCod, T002Z3_A994SolicitacaoMudanca_Solicitante
               }
               , new Object[] {
               T002Z4_A993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T002Z5_A994SolicitacaoMudanca_Solicitante
               }
               , new Object[] {
               T002Z6_A996SolicitacaoMudanca_Codigo, T002Z6_A997SolicitacaoMudanca_Data, T002Z6_A993SolicitacaoMudanca_SistemaCod, T002Z6_n993SolicitacaoMudanca_SistemaCod, T002Z6_A994SolicitacaoMudanca_Solicitante
               }
               , new Object[] {
               T002Z7_A993SolicitacaoMudanca_SistemaCod
               }
               , new Object[] {
               T002Z8_A994SolicitacaoMudanca_Solicitante
               }
               , new Object[] {
               T002Z9_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T002Z10_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T002Z11_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T002Z12_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002Z15_A996SolicitacaoMudanca_Codigo, T002Z15_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T002Z16_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               T002Z17_A993SolicitacaoMudanca_SistemaCod
               }
            }
         );
         AV16Pgmname = "SolicitacaoMudanca";
         Z994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
         N994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
         i994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
         A994SolicitacaoMudanca_Solicitante = AV8WWPContext.gxTpr_Userid;
         Z997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         A997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         i997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound121 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7SolicitacaoMudanca_Codigo ;
      private int Z996SolicitacaoMudanca_Codigo ;
      private int Z993SolicitacaoMudanca_SistemaCod ;
      private int Z994SolicitacaoMudanca_Solicitante ;
      private int N993SolicitacaoMudanca_SistemaCod ;
      private int N994SolicitacaoMudanca_Solicitante ;
      private int A993SolicitacaoMudanca_SistemaCod ;
      private int A994SolicitacaoMudanca_Solicitante ;
      private int AV7SolicitacaoMudanca_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtSolicitacaoMudanca_Data_Enabled ;
      private int edtSolicitacaoMudanca_SistemaCod_Enabled ;
      private int AV14SolicitacaoMudancaItem_FuncaoAPF ;
      private int edtavSolicitacaomudancaitem_funcaoapf_Enabled ;
      private int imgInsertitem_Visible ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int AV11Insert_SolicitacaoMudanca_SistemaCod ;
      private int AV12Insert_SolicitacaoMudanca_Solicitante ;
      private int AV17GXV1 ;
      private int i994SolicitacaoMudanca_Solicitante ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSolicitacaoMudanca_Data_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicitacaomudanca_data_Internalname ;
      private String lblTextblocksolicitacaomudanca_data_Jsonclick ;
      private String edtSolicitacaoMudanca_Data_Jsonclick ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Internalname ;
      private String lblTextblocksolicitacaomudanca_sistemacod_Jsonclick ;
      private String edtSolicitacaoMudanca_SistemaCod_Internalname ;
      private String edtSolicitacaoMudanca_SistemaCod_Jsonclick ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Internalname ;
      private String lblTextblocksolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String edtavSolicitacaomudancaitem_funcaoapf_Internalname ;
      private String edtavSolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String imgInsertitem_Internalname ;
      private String imgInsertitem_Jsonclick ;
      private String AV16Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode121 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z997SolicitacaoMudanca_Data ;
      private DateTime A997SolicitacaoMudanca_Data ;
      private DateTime i997SolicitacaoMudanca_Data ;
      private bool entryPointCalled ;
      private bool n993SolicitacaoMudanca_SistemaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T002Z6_A996SolicitacaoMudanca_Codigo ;
      private DateTime[] T002Z6_A997SolicitacaoMudanca_Data ;
      private int[] T002Z6_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z6_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T002Z6_A994SolicitacaoMudanca_Solicitante ;
      private int[] T002Z4_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z4_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T002Z5_A994SolicitacaoMudanca_Solicitante ;
      private int[] T002Z7_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z7_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T002Z8_A994SolicitacaoMudanca_Solicitante ;
      private int[] T002Z9_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z3_A996SolicitacaoMudanca_Codigo ;
      private DateTime[] T002Z3_A997SolicitacaoMudanca_Data ;
      private int[] T002Z3_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z3_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T002Z3_A994SolicitacaoMudanca_Solicitante ;
      private int[] T002Z10_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z11_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z2_A996SolicitacaoMudanca_Codigo ;
      private DateTime[] T002Z2_A997SolicitacaoMudanca_Data ;
      private int[] T002Z2_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z2_n993SolicitacaoMudanca_SistemaCod ;
      private int[] T002Z2_A994SolicitacaoMudanca_Solicitante ;
      private int[] T002Z12_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z15_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z15_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T002Z16_A996SolicitacaoMudanca_Codigo ;
      private int[] T002Z17_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] T002Z17_n993SolicitacaoMudanca_SistemaCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class solicitacaomudanca__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002Z6 ;
          prmT002Z6 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z4 ;
          prmT002Z4 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z5 ;
          prmT002Z5 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z7 ;
          prmT002Z7 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z8 ;
          prmT002Z8 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z9 ;
          prmT002Z9 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z3 ;
          prmT002Z3 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z10 ;
          prmT002Z10 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z11 ;
          prmT002Z11 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z2 ;
          prmT002Z2 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z12 ;
          prmT002Z12 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z13 ;
          prmT002Z13 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z14 ;
          prmT002Z14 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z15 ;
          prmT002Z15 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Z16 ;
          prmT002Z16 = new Object[] {
          } ;
          Object[] prmT002Z17 ;
          prmT002Z17 = new Object[] {
          new Object[] {"@SolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002Z2", "SELECT [SolicitacaoMudanca_Codigo], [SolicitacaoMudanca_Data], [SolicitacaoMudanca_SistemaCod] AS SolicitacaoMudanca_SistemaCod, [SolicitacaoMudanca_Solicitante] AS SolicitacaoMudanca_Solicitante FROM [SolicitacaoMudanca] WITH (UPDLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z2,1,0,true,false )
             ,new CursorDef("T002Z3", "SELECT [SolicitacaoMudanca_Codigo], [SolicitacaoMudanca_Data], [SolicitacaoMudanca_SistemaCod] AS SolicitacaoMudanca_SistemaCod, [SolicitacaoMudanca_Solicitante] AS SolicitacaoMudanca_Solicitante FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z3,1,0,true,false )
             ,new CursorDef("T002Z4", "SELECT [Sistema_Codigo] AS SolicitacaoMudanca_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @SolicitacaoMudanca_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z4,1,0,true,false )
             ,new CursorDef("T002Z5", "SELECT [Usuario_Codigo] AS SolicitacaoMudanca_Solicitante FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @SolicitacaoMudanca_Solicitante ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z5,1,0,true,false )
             ,new CursorDef("T002Z6", "SELECT TM1.[SolicitacaoMudanca_Codigo], TM1.[SolicitacaoMudanca_Data], TM1.[SolicitacaoMudanca_SistemaCod] AS SolicitacaoMudanca_SistemaCod, TM1.[SolicitacaoMudanca_Solicitante] AS SolicitacaoMudanca_Solicitante FROM [SolicitacaoMudanca] TM1 WITH (NOLOCK) WHERE TM1.[SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ORDER BY TM1.[SolicitacaoMudanca_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z6,100,0,true,false )
             ,new CursorDef("T002Z7", "SELECT [Sistema_Codigo] AS SolicitacaoMudanca_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @SolicitacaoMudanca_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z7,1,0,true,false )
             ,new CursorDef("T002Z8", "SELECT [Usuario_Codigo] AS SolicitacaoMudanca_Solicitante FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @SolicitacaoMudanca_Solicitante ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z8,1,0,true,false )
             ,new CursorDef("T002Z9", "SELECT [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z9,1,0,true,false )
             ,new CursorDef("T002Z10", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE ( [SolicitacaoMudanca_Codigo] > @SolicitacaoMudanca_Codigo) ORDER BY [SolicitacaoMudanca_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z10,1,0,true,true )
             ,new CursorDef("T002Z11", "SELECT TOP 1 [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) WHERE ( [SolicitacaoMudanca_Codigo] < @SolicitacaoMudanca_Codigo) ORDER BY [SolicitacaoMudanca_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z11,1,0,true,true )
             ,new CursorDef("T002Z12", "INSERT INTO [SolicitacaoMudanca]([SolicitacaoMudanca_Data], [SolicitacaoMudanca_SistemaCod], [SolicitacaoMudanca_Solicitante]) VALUES(@SolicitacaoMudanca_Data, @SolicitacaoMudanca_SistemaCod, @SolicitacaoMudanca_Solicitante); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002Z12)
             ,new CursorDef("T002Z13", "UPDATE [SolicitacaoMudanca] SET [SolicitacaoMudanca_Data]=@SolicitacaoMudanca_Data, [SolicitacaoMudanca_SistemaCod]=@SolicitacaoMudanca_SistemaCod, [SolicitacaoMudanca_Solicitante]=@SolicitacaoMudanca_Solicitante  WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo", GxErrorMask.GX_NOMASK,prmT002Z13)
             ,new CursorDef("T002Z14", "DELETE FROM [SolicitacaoMudanca]  WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo", GxErrorMask.GX_NOMASK,prmT002Z14)
             ,new CursorDef("T002Z15", "SELECT TOP 1 [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE [SolicitacaoMudanca_Codigo] = @SolicitacaoMudanca_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z15,1,0,true,true )
             ,new CursorDef("T002Z16", "SELECT [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudanca] WITH (NOLOCK) ORDER BY [SolicitacaoMudanca_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z16,100,0,true,false )
             ,new CursorDef("T002Z17", "SELECT [Sistema_Codigo] AS SolicitacaoMudanca_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @SolicitacaoMudanca_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Z17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
