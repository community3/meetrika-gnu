/*
               File: PRC_Acatou
        Description: PRC_Acatou
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:4.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_acatou : GXProcedure
   {
      public prc_acatou( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_acatou( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UserId ,
                           String aP1_Observacao )
      {
         this.AV12UserId = aP0_UserId;
         this.AV9Observacao = aP1_Observacao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_UserId ,
                                 String aP1_Observacao )
      {
         prc_acatou objprc_acatou;
         objprc_acatou = new prc_acatou();
         objprc_acatou.AV12UserId = aP0_UserId;
         objprc_acatou.AV9Observacao = aP1_Observacao;
         objprc_acatou.context.SetSubmitInitialConfig(context);
         objprc_acatou.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_acatou);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_acatou)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Selecionadas.FromXml(AV19WebSession.Get("Codigos"), "Collection");
         AV22GXV1 = 1;
         while ( AV22GXV1 <= AV18Selecionadas.Count )
         {
            AV8Codigo = (int)(AV18Selecionadas.GetNumeric(AV22GXV1));
            /* Using cursor P00903 */
            pr_default.execute(0, new Object[] {AV8Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A456ContagemResultado_Codigo = P00903_A456ContagemResultado_Codigo[0];
               A602ContagemResultado_OSVinculada = P00903_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00903_n602ContagemResultado_OSVinculada[0];
               A682ContagemResultado_PFBFMUltima = P00903_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = P00903_A683ContagemResultado_PFLFMUltima[0];
               A682ContagemResultado_PFBFMUltima = P00903_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = P00903_A683ContagemResultado_PFLFMUltima[0];
               AV13OSVinculada = A602ContagemResultado_OSVinculada;
               AV11PFB = A682ContagemResultado_PFBFMUltima;
               AV10PFL = A683ContagemResultado_PFLFMUltima;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( (0==AV13OSVinculada) )
            {
               /* Using cursor P00905 */
               pr_default.execute(1, new Object[] {AV8Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A602ContagemResultado_OSVinculada = P00905_A602ContagemResultado_OSVinculada[0];
                  n602ContagemResultado_OSVinculada = P00905_n602ContagemResultado_OSVinculada[0];
                  A456ContagemResultado_Codigo = P00905_A456ContagemResultado_Codigo[0];
                  A531ContagemResultado_StatusUltCnt = P00905_A531ContagemResultado_StatusUltCnt[0];
                  n531ContagemResultado_StatusUltCnt = P00905_n531ContagemResultado_StatusUltCnt[0];
                  A531ContagemResultado_StatusUltCnt = P00905_A531ContagemResultado_StatusUltCnt[0];
                  n531ContagemResultado_StatusUltCnt = P00905_n531ContagemResultado_StatusUltCnt[0];
                  AV13OSVinculada = A456ContagemResultado_Codigo;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
            }
            new prc_acata(context ).execute(  AV8Codigo,  AV13OSVinculada,  AV12UserId,  AV11PFB,  AV10PFL,  AV9Observacao) ;
            AV22GXV1 = (int)(AV22GXV1+1);
         }
         AV19WebSession.Remove("Codigos");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Selecionadas = new GxSimpleCollection();
         AV19WebSession = context.GetSession();
         scmdbuf = "";
         P00903_A456ContagemResultado_Codigo = new int[1] ;
         P00903_A602ContagemResultado_OSVinculada = new int[1] ;
         P00903_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00903_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00903_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00905_A602ContagemResultado_OSVinculada = new int[1] ;
         P00905_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00905_A456ContagemResultado_Codigo = new int[1] ;
         P00905_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00905_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_acatou__default(),
            new Object[][] {
                new Object[] {
               P00903_A456ContagemResultado_Codigo, P00903_A602ContagemResultado_OSVinculada, P00903_n602ContagemResultado_OSVinculada, P00903_A682ContagemResultado_PFBFMUltima, P00903_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               P00905_A602ContagemResultado_OSVinculada, P00905_n602ContagemResultado_OSVinculada, P00905_A456ContagemResultado_Codigo, P00905_A531ContagemResultado_StatusUltCnt, P00905_n531ContagemResultado_StatusUltCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A531ContagemResultado_StatusUltCnt ;
      private int AV12UserId ;
      private int AV22GXV1 ;
      private int AV8Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV13OSVinculada ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal AV11PFB ;
      private decimal AV10PFL ;
      private String scmdbuf ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private String AV9Observacao ;
      private IGxSession AV19WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00903_A456ContagemResultado_Codigo ;
      private int[] P00903_A602ContagemResultado_OSVinculada ;
      private bool[] P00903_n602ContagemResultado_OSVinculada ;
      private decimal[] P00903_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00903_A683ContagemResultado_PFLFMUltima ;
      private int[] P00905_A602ContagemResultado_OSVinculada ;
      private bool[] P00905_n602ContagemResultado_OSVinculada ;
      private int[] P00905_A456ContagemResultado_Codigo ;
      private short[] P00905_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00905_n531ContagemResultado_StatusUltCnt ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV18Selecionadas ;
   }

   public class prc_acatou__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00903 ;
          prmP00903 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00905 ;
          prmP00905 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00903", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], COALESCE( T2.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T2.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00903,1,0,false,true )
             ,new CursorDef("P00905", "SELECT TOP 1 T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_OSVinculada] = @AV8Codigo) AND (COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00905,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
