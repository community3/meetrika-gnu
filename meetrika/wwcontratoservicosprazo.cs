/*
               File: WWContratoServicosPrazo
        Description:  Prazo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:53:49.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoservicosprazo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoservicosprazo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoservicosprazo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavContratoservicosprazo_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavContratoservicosprazo_tipo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavContratoservicosprazo_tipo3 = new GXCombobox();
         cmbContratoServicosPrazo_Tipo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContratoServicosPrazo_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19ContratoServicosPrazo_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22ContratoServicosPrazo_Tipo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV31TFContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
               AV32TFContratoServicosPrazo_CntSrvCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
               AV39TFContratoServicosPrazo_Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
               AV40TFContratoServicosPrazo_Dias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
               AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace", AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace);
               AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace", AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace);
               AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace", AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV36TFContratoServicosPrazo_Tipo_Sels);
               AV63Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAG02( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTG02( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118534966");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoservicosprazo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZO_TIPO1", StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZO_TIPO2", StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZO_TIPO3", StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV42DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZO_CNTSRVCODTITLEFILTERDATA", AV30ContratoServicosPrazo_CntSrvCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZO_CNTSRVCODTITLEFILTERDATA", AV30ContratoServicosPrazo_CntSrvCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZO_TIPOTITLEFILTERDATA", AV34ContratoServicosPrazo_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZO_TIPOTITLEFILTERDATA", AV34ContratoServicosPrazo_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZO_DIASTITLEFILTERDATA", AV38ContratoServicosPrazo_DiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZO_DIASTITLEFILTERDATA", AV38ContratoServicosPrazo_DiasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSERVICOSPRAZO_TIPO_SELS", AV36TFContratoServicosPrazo_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSERVICOSPRAZO_TIPO_SELS", AV36TFContratoServicosPrazo_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV63Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Caption", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Cls", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_cntsrvcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_cntsrvcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazo_cntsrvcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazo_cntsrvcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazo_cntsrvcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Caption", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Cls", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazo_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazo_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratoservicosprazo_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Caption", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Cls", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_dias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazo_dias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazo_dias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazo_dias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazo_dias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosprazo_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazo_dias_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEG02( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTG02( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoservicosprazo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoServicosPrazo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Prazo" ;
      }

      protected void WBG00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_G02( true) ;
         }
         else
         {
            wb_table1_2_G02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(83, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazo_cntsrvcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazo_cntsrvcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazo_cntsrvcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrazo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazo_cntsrvcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazo_cntsrvcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrazo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazo_dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFContratoServicosPrazo_Dias), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazo_dias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazo_dias_Visible, 1, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrazo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazo_dias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFContratoServicosPrazo_Dias_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazo_dias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazo_dias_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrazo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZO_CNTSRVCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Internalname, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrazo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZO_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Internalname, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrazo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZO_DIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Internalname, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrazo.htm");
         }
         wbLoad = true;
      }

      protected void STARTG02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Prazo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPG00( ) ;
      }

      protected void WSG02( )
      {
         STARTG02( ) ;
         EVTG02( ) ;
      }

      protected void EVTG02( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11G02 */
                              E11G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12G02 */
                              E12G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZO_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13G02 */
                              E13G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZO_DIAS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14G02 */
                              E14G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15G02 */
                              E15G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16G02 */
                              E16G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17G02 */
                              E17G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18G02 */
                              E18G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19G02 */
                              E19G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20G02 */
                              E20G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21G02 */
                              E21G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22G02 */
                              E22G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23G02 */
                              E23G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24G02 */
                              E24G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25G02 */
                              E25G02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV61Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV62Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A903ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_CntSrvCod_Internalname), ",", "."));
                              cmbContratoServicosPrazo_Tipo.Name = cmbContratoServicosPrazo_Tipo_Internalname;
                              cmbContratoServicosPrazo_Tipo.CurrentValue = cgiGet( cmbContratoServicosPrazo_Tipo_Internalname);
                              A904ContratoServicosPrazo_Tipo = cgiGet( cmbContratoServicosPrazo_Tipo_Internalname);
                              A905ContratoServicosPrazo_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Dias_Internalname), ",", "."));
                              n905ContratoServicosPrazo_Dias = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26G02 */
                                    E26G02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27G02 */
                                    E27G02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28G02 */
                                    E28G02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprazo_tipo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO1"), AV16ContratoServicosPrazo_Tipo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprazo_tipo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO2"), AV19ContratoServicosPrazo_Tipo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprazo_tipo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO3"), AV22ContratoServicosPrazo_Tipo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprazo_cntsrvcod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV31TFContratoServicosPrazo_CntSrvCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprazo_cntsrvcod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosPrazo_CntSrvCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprazo_dias Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS"), ",", ".") != Convert.ToDecimal( AV39TFContratoServicosPrazo_Dias )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprazo_dias_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosPrazo_Dias_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEG02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAG02( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSPRAZO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavContratoservicosprazo_tipo1.Name = "vCONTRATOSERVICOSPRAZO_TIPO1";
            cmbavContratoservicosprazo_tipo1.WebTags = "";
            cmbavContratoservicosprazo_tipo1.addItem("", "Todos", 0);
            cmbavContratoservicosprazo_tipo1.addItem("A", "Acumulado", 0);
            cmbavContratoservicosprazo_tipo1.addItem("C", "Complexidade", 0);
            cmbavContratoservicosprazo_tipo1.addItem("S", "Solicitado", 0);
            cmbavContratoservicosprazo_tipo1.addItem("F", "Fixo", 0);
            cmbavContratoservicosprazo_tipo1.addItem("E", "El�stico", 0);
            cmbavContratoservicosprazo_tipo1.addItem("V", "Vari�vel", 0);
            cmbavContratoservicosprazo_tipo1.addItem("P", "Progress�o Aritm�tica", 0);
            cmbavContratoservicosprazo_tipo1.addItem("O", "Combinado", 0);
            if ( cmbavContratoservicosprazo_tipo1.ItemCount > 0 )
            {
               AV16ContratoServicosPrazo_Tipo1 = cmbavContratoservicosprazo_tipo1.getValidValue(AV16ContratoServicosPrazo_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSPRAZO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavContratoservicosprazo_tipo2.Name = "vCONTRATOSERVICOSPRAZO_TIPO2";
            cmbavContratoservicosprazo_tipo2.WebTags = "";
            cmbavContratoservicosprazo_tipo2.addItem("", "Todos", 0);
            cmbavContratoservicosprazo_tipo2.addItem("A", "Acumulado", 0);
            cmbavContratoservicosprazo_tipo2.addItem("C", "Complexidade", 0);
            cmbavContratoservicosprazo_tipo2.addItem("S", "Solicitado", 0);
            cmbavContratoservicosprazo_tipo2.addItem("F", "Fixo", 0);
            cmbavContratoservicosprazo_tipo2.addItem("E", "El�stico", 0);
            cmbavContratoservicosprazo_tipo2.addItem("V", "Vari�vel", 0);
            cmbavContratoservicosprazo_tipo2.addItem("P", "Progress�o Aritm�tica", 0);
            cmbavContratoservicosprazo_tipo2.addItem("O", "Combinado", 0);
            if ( cmbavContratoservicosprazo_tipo2.ItemCount > 0 )
            {
               AV19ContratoServicosPrazo_Tipo2 = cmbavContratoservicosprazo_tipo2.getValidValue(AV19ContratoServicosPrazo_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSPRAZO_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            cmbavContratoservicosprazo_tipo3.Name = "vCONTRATOSERVICOSPRAZO_TIPO3";
            cmbavContratoservicosprazo_tipo3.WebTags = "";
            cmbavContratoservicosprazo_tipo3.addItem("", "Todos", 0);
            cmbavContratoservicosprazo_tipo3.addItem("A", "Acumulado", 0);
            cmbavContratoservicosprazo_tipo3.addItem("C", "Complexidade", 0);
            cmbavContratoservicosprazo_tipo3.addItem("S", "Solicitado", 0);
            cmbavContratoservicosprazo_tipo3.addItem("F", "Fixo", 0);
            cmbavContratoservicosprazo_tipo3.addItem("E", "El�stico", 0);
            cmbavContratoservicosprazo_tipo3.addItem("V", "Vari�vel", 0);
            cmbavContratoservicosprazo_tipo3.addItem("P", "Progress�o Aritm�tica", 0);
            cmbavContratoservicosprazo_tipo3.addItem("O", "Combinado", 0);
            if ( cmbavContratoservicosprazo_tipo3.ItemCount > 0 )
            {
               AV22ContratoServicosPrazo_Tipo3 = cmbavContratoservicosprazo_tipo3.getValidValue(AV22ContratoServicosPrazo_Tipo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
            }
            GXCCtl = "CONTRATOSERVICOSPRAZO_TIPO_" + sGXsfl_73_idx;
            cmbContratoServicosPrazo_Tipo.Name = GXCCtl;
            cmbContratoServicosPrazo_Tipo.WebTags = "";
            cmbContratoServicosPrazo_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosPrazo_Tipo.addItem("A", "Acumulado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("C", "Complexidade", 0);
            cmbContratoServicosPrazo_Tipo.addItem("S", "Solicitado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("F", "Fixo", 0);
            cmbContratoServicosPrazo_Tipo.addItem("E", "El�stico", 0);
            cmbContratoServicosPrazo_Tipo.addItem("V", "Vari�vel", 0);
            cmbContratoServicosPrazo_Tipo.addItem("P", "Progress�o Aritm�tica", 0);
            cmbContratoServicosPrazo_Tipo.addItem("O", "Combinado", 0);
            if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
            {
               A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16ContratoServicosPrazo_Tipo1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19ContratoServicosPrazo_Tipo2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22ContratoServicosPrazo_Tipo3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       int AV31TFContratoServicosPrazo_CntSrvCod ,
                                       int AV32TFContratoServicosPrazo_CntSrvCod_To ,
                                       short AV39TFContratoServicosPrazo_Dias ,
                                       short AV40TFContratoServicosPrazo_Dias_To ,
                                       String AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace ,
                                       String AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace ,
                                       String AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace ,
                                       IGxCollection AV36TFContratoServicosPrazo_Tipo_Sels ,
                                       String AV63Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A903ContratoServicosPrazo_CntSrvCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFG02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A903ContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A904ContratoServicosPrazo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_TIPO", StringUtil.RTrim( A904ContratoServicosPrazo_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_DIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A905ContratoServicosPrazo_Dias), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavContratoservicosprazo_tipo1.ItemCount > 0 )
         {
            AV16ContratoServicosPrazo_Tipo1 = cmbavContratoservicosprazo_tipo1.getValidValue(AV16ContratoServicosPrazo_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavContratoservicosprazo_tipo2.ItemCount > 0 )
         {
            AV19ContratoServicosPrazo_Tipo2 = cmbavContratoservicosprazo_tipo2.getValidValue(AV19ContratoServicosPrazo_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
         if ( cmbavContratoservicosprazo_tipo3.ItemCount > 0 )
         {
            AV22ContratoServicosPrazo_Tipo3 = cmbavContratoservicosprazo_tipo3.getValidValue(AV22ContratoServicosPrazo_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFG02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV63Pgmname = "WWContratoServicosPrazo";
         context.Gx_err = 0;
      }

      protected void RFG02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E27G02 */
         E27G02 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A904ContratoServicosPrazo_Tipo ,
                                                 AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels ,
                                                 AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 ,
                                                 AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 ,
                                                 AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 ,
                                                 AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 ,
                                                 AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 ,
                                                 AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 ,
                                                 AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 ,
                                                 AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 ,
                                                 AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod ,
                                                 AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to ,
                                                 AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels.Count ,
                                                 AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias ,
                                                 AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to ,
                                                 A903ContratoServicosPrazo_CntSrvCod ,
                                                 A905ContratoServicosPrazo_Dias ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00G02 */
            pr_default.execute(0, new Object[] {AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1, AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2, AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3, AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod, AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to, AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias, AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A905ContratoServicosPrazo_Dias = H00G02_A905ContratoServicosPrazo_Dias[0];
               n905ContratoServicosPrazo_Dias = H00G02_n905ContratoServicosPrazo_Dias[0];
               A904ContratoServicosPrazo_Tipo = H00G02_A904ContratoServicosPrazo_Tipo[0];
               A903ContratoServicosPrazo_CntSrvCod = H00G02_A903ContratoServicosPrazo_CntSrvCod[0];
               /* Execute user event: E28G02 */
               E28G02 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 73;
            WBG00( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A904ContratoServicosPrazo_Tipo ,
                                              AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels ,
                                              AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 ,
                                              AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 ,
                                              AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 ,
                                              AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 ,
                                              AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 ,
                                              AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 ,
                                              AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 ,
                                              AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 ,
                                              AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod ,
                                              AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to ,
                                              AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels.Count ,
                                              AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias ,
                                              AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to ,
                                              A903ContratoServicosPrazo_CntSrvCod ,
                                              A905ContratoServicosPrazo_Dias ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00G03 */
         pr_default.execute(1, new Object[] {AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1, AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2, AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3, AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod, AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to, AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias, AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to});
         GRID_nRecordCount = H00G03_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPG00( )
      {
         /* Before Start, stand alone formulas. */
         AV63Pgmname = "WWContratoServicosPrazo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26G02 */
         E26G02 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV42DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZO_CNTSRVCODTITLEFILTERDATA"), AV30ContratoServicosPrazo_CntSrvCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZO_TIPOTITLEFILTERDATA"), AV34ContratoServicosPrazo_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZO_DIASTITLEFILTERDATA"), AV38ContratoServicosPrazo_DiasTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavContratoservicosprazo_tipo1.Name = cmbavContratoservicosprazo_tipo1_Internalname;
            cmbavContratoservicosprazo_tipo1.CurrentValue = cgiGet( cmbavContratoservicosprazo_tipo1_Internalname);
            AV16ContratoServicosPrazo_Tipo1 = cgiGet( cmbavContratoservicosprazo_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            cmbavContratoservicosprazo_tipo2.Name = cmbavContratoservicosprazo_tipo2_Internalname;
            cmbavContratoservicosprazo_tipo2.CurrentValue = cgiGet( cmbavContratoservicosprazo_tipo2_Internalname);
            AV19ContratoServicosPrazo_Tipo2 = cgiGet( cmbavContratoservicosprazo_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            cmbavContratoservicosprazo_tipo3.Name = cmbavContratoservicosprazo_tipo3_Internalname;
            cmbavContratoservicosprazo_tipo3.CurrentValue = cgiGet( cmbavContratoservicosprazo_tipo3_Internalname);
            AV22ContratoServicosPrazo_Tipo3 = cgiGet( cmbavContratoservicosprazo_tipo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD");
               GX_FocusControl = edtavTfcontratoservicosprazo_cntsrvcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContratoServicosPrazo_CntSrvCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
            }
            else
            {
               AV31TFContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO");
               GX_FocusControl = edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoServicosPrazo_CntSrvCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
            }
            else
            {
               AV32TFContratoServicosPrazo_CntSrvCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZO_DIAS");
               GX_FocusControl = edtavTfcontratoservicosprazo_dias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContratoServicosPrazo_Dias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
            }
            else
            {
               AV39TFContratoServicosPrazo_Dias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZO_DIAS_TO");
               GX_FocusControl = edtavTfcontratoservicosprazo_dias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoServicosPrazo_Dias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
            }
            else
            {
               AV40TFContratoServicosPrazo_Dias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazo_dias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
            }
            AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace", AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace);
            AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace", AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace);
            AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace", AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV44GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV45GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosprazo_cntsrvcod_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Caption");
            Ddo_contratoservicosprazo_cntsrvcod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Tooltip");
            Ddo_contratoservicosprazo_cntsrvcod_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Cls");
            Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtext_set");
            Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtextto_set");
            Ddo_contratoservicosprazo_cntsrvcod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Dropdownoptionstype");
            Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazo_cntsrvcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includesortasc"));
            Ddo_contratoservicosprazo_cntsrvcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includesortdsc"));
            Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortedstatus");
            Ddo_contratoservicosprazo_cntsrvcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includefilter"));
            Ddo_contratoservicosprazo_cntsrvcod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filtertype");
            Ddo_contratoservicosprazo_cntsrvcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filterisrange"));
            Ddo_contratoservicosprazo_cntsrvcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Includedatalist"));
            Ddo_contratoservicosprazo_cntsrvcod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortasc");
            Ddo_contratoservicosprazo_cntsrvcod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Sortdsc");
            Ddo_contratoservicosprazo_cntsrvcod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Cleanfilter");
            Ddo_contratoservicosprazo_cntsrvcod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Rangefilterfrom");
            Ddo_contratoservicosprazo_cntsrvcod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Rangefilterto");
            Ddo_contratoservicosprazo_cntsrvcod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Searchbuttontext");
            Ddo_contratoservicosprazo_tipo_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Caption");
            Ddo_contratoservicosprazo_tipo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Tooltip");
            Ddo_contratoservicosprazo_tipo_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Cls");
            Ddo_contratoservicosprazo_tipo_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Selectedvalue_set");
            Ddo_contratoservicosprazo_tipo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Dropdownoptionstype");
            Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazo_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includesortasc"));
            Ddo_contratoservicosprazo_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includesortdsc"));
            Ddo_contratoservicosprazo_tipo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortedstatus");
            Ddo_contratoservicosprazo_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includefilter"));
            Ddo_contratoservicosprazo_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Includedatalist"));
            Ddo_contratoservicosprazo_tipo_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Datalisttype");
            Ddo_contratoservicosprazo_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Allowmultipleselection"));
            Ddo_contratoservicosprazo_tipo_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Datalistfixedvalues");
            Ddo_contratoservicosprazo_tipo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortasc");
            Ddo_contratoservicosprazo_tipo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Sortdsc");
            Ddo_contratoservicosprazo_tipo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Cleanfilter");
            Ddo_contratoservicosprazo_tipo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Searchbuttontext");
            Ddo_contratoservicosprazo_dias_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Caption");
            Ddo_contratoservicosprazo_dias_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Tooltip");
            Ddo_contratoservicosprazo_dias_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Cls");
            Ddo_contratoservicosprazo_dias_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtext_set");
            Ddo_contratoservicosprazo_dias_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtextto_set");
            Ddo_contratoservicosprazo_dias_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Dropdownoptionstype");
            Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazo_dias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includesortasc"));
            Ddo_contratoservicosprazo_dias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includesortdsc"));
            Ddo_contratoservicosprazo_dias_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortedstatus");
            Ddo_contratoservicosprazo_dias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includefilter"));
            Ddo_contratoservicosprazo_dias_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filtertype");
            Ddo_contratoservicosprazo_dias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filterisrange"));
            Ddo_contratoservicosprazo_dias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Includedatalist"));
            Ddo_contratoservicosprazo_dias_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortasc");
            Ddo_contratoservicosprazo_dias_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Sortdsc");
            Ddo_contratoservicosprazo_dias_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Cleanfilter");
            Ddo_contratoservicosprazo_dias_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Rangefilterfrom");
            Ddo_contratoservicosprazo_dias_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Rangefilterto");
            Ddo_contratoservicosprazo_dias_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Activeeventkey");
            Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtext_get");
            Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD_Filteredtextto_get");
            Ddo_contratoservicosprazo_tipo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Activeeventkey");
            Ddo_contratoservicosprazo_tipo_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_TIPO_Selectedvalue_get");
            Ddo_contratoservicosprazo_dias_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Activeeventkey");
            Ddo_contratoservicosprazo_dias_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtext_get");
            Ddo_contratoservicosprazo_dias_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZO_DIAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO1"), AV16ContratoServicosPrazo_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO2"), AV19ContratoServicosPrazo_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRAZO_TIPO3"), AV22ContratoServicosPrazo_Tipo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV31TFContratoServicosPrazo_CntSrvCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosPrazo_CntSrvCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS"), ",", ".") != Convert.ToDecimal( AV39TFContratoServicosPrazo_Dias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosPrazo_Dias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26G02 */
         E26G02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26G02( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16ContratoServicosPrazo_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19ContratoServicosPrazo_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
         AV18DynamicFiltersSelector2 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22ContratoServicosPrazo_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
         AV21DynamicFiltersSelector3 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicosprazo_cntsrvcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazo_cntsrvcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazo_cntsrvcod_Visible), 5, 0)));
         edtavTfcontratoservicosprazo_cntsrvcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazo_cntsrvcod_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazo_dias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazo_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazo_dias_Visible), 5, 0)));
         edtavTfcontratoservicosprazo_dias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazo_dias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazo_dias_to_Visible), 5, 0)));
         Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazo_CntSrvCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace);
         AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace = Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace", AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace);
         edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazo_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace);
         AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace = Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace", AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace);
         edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazo_Dias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace);
         AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace = Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace", AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace);
         edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Prazo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tipo", 0);
         cmbavOrderedby.addItem("2", "Contrato Servicos Codigo", 0);
         cmbavOrderedby.addItem("3", "Dias", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV42DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV42DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27G02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30ContratoServicosPrazo_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosPrazo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoServicosPrazo_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosPrazo_CntSrvCod_Titleformat = 2;
         edtContratoServicosPrazo_CntSrvCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato Servicos Codigo", AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Title", edtContratoServicosPrazo_CntSrvCod_Title);
         cmbContratoServicosPrazo_Tipo_Titleformat = 2;
         cmbContratoServicosPrazo_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Tipo_Internalname, "Title", cmbContratoServicosPrazo_Tipo.Title.Text);
         edtContratoServicosPrazo_Dias_Titleformat = 2;
         edtContratoServicosPrazo_Dias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Title", edtContratoServicosPrazo_Dias_Title);
         AV44GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridCurrentPage), 10, 0)));
         AV45GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridPageCount), 10, 0)));
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = AV16ContratoServicosPrazo_Tipo1;
         AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = AV19ContratoServicosPrazo_Tipo2;
         AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = AV22ContratoServicosPrazo_Tipo3;
         AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod = AV31TFContratoServicosPrazo_CntSrvCod;
         AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to = AV32TFContratoServicosPrazo_CntSrvCod_To;
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = AV36TFContratoServicosPrazo_Tipo_Sels;
         AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias = AV39TFContratoServicosPrazo_Dias;
         AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to = AV40TFContratoServicosPrazo_Dias_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ContratoServicosPrazo_CntSrvCodTitleFilterData", AV30ContratoServicosPrazo_CntSrvCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34ContratoServicosPrazo_TipoTitleFilterData", AV34ContratoServicosPrazo_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ContratoServicosPrazo_DiasTitleFilterData", AV38ContratoServicosPrazo_DiasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11G02( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV43PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV43PageToGo) ;
         }
      }

      protected void E12G02( )
      {
         /* Ddo_contratoservicosprazo_cntsrvcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
            AV32TFContratoServicosPrazo_CntSrvCod_To = (int)(NumberUtil.Val( Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13G02( )
      {
         /* Ddo_contratoservicosprazo_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SortedStatus", Ddo_contratoservicosprazo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SortedStatus", Ddo_contratoservicosprazo_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContratoServicosPrazo_Tipo_SelsJson = Ddo_contratoservicosprazo_tipo_Selectedvalue_get;
            AV36TFContratoServicosPrazo_Tipo_Sels.FromJSonString(AV35TFContratoServicosPrazo_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36TFContratoServicosPrazo_Tipo_Sels", AV36TFContratoServicosPrazo_Tipo_Sels);
      }

      protected void E14G02( )
      {
         /* Ddo_contratoservicosprazo_dias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_dias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_dias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazo_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_dias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprazo_dias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazo_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazo_dias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContratoServicosPrazo_Dias = (short)(NumberUtil.Val( Ddo_contratoservicosprazo_dias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
            AV40TFContratoServicosPrazo_Dias_To = (short)(NumberUtil.Val( Ddo_contratoservicosprazo_dias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28G02( )
      {
         /* Grid_Load Routine */
         AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
         AV61Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A903ContratoServicosPrazo_CntSrvCod);
         AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
         AV62Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A903ContratoServicosPrazo_CntSrvCod);
         edtContratoServicosPrazo_CntSrvCod_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A903ContratoServicosPrazo_CntSrvCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E15G02( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21G02( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16G02( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo2.CurrentValue = StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Values", cmbavContratoservicosprazo_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo3.CurrentValue = StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Values", cmbavContratoservicosprazo_tipo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo1.CurrentValue = StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Values", cmbavContratoservicosprazo_tipo1.ToJavascriptSource());
      }

      protected void E22G02( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23G02( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17G02( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo2.CurrentValue = StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Values", cmbavContratoservicosprazo_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo3.CurrentValue = StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Values", cmbavContratoservicosprazo_tipo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo1.CurrentValue = StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Values", cmbavContratoservicosprazo_tipo1.ToJavascriptSource());
      }

      protected void E24G02( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18G02( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoServicosPrazo_Tipo1, AV18DynamicFiltersSelector2, AV19ContratoServicosPrazo_Tipo2, AV21DynamicFiltersSelector3, AV22ContratoServicosPrazo_Tipo3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV31TFContratoServicosPrazo_CntSrvCod, AV32TFContratoServicosPrazo_CntSrvCod_To, AV39TFContratoServicosPrazo_Dias, AV40TFContratoServicosPrazo_Dias_To, AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace, AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace, AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace, AV36TFContratoServicosPrazo_Tipo_Sels, AV63Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A903ContratoServicosPrazo_CntSrvCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo2.CurrentValue = StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Values", cmbavContratoservicosprazo_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo3.CurrentValue = StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Values", cmbavContratoservicosprazo_tipo3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo1.CurrentValue = StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Values", cmbavContratoservicosprazo_tipo1.ToJavascriptSource());
      }

      protected void E25G02( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19G02( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36TFContratoServicosPrazo_Tipo_Sels", AV36TFContratoServicosPrazo_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo1.CurrentValue = StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Values", cmbavContratoservicosprazo_tipo1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo2.CurrentValue = StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Values", cmbavContratoservicosprazo_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratoservicosprazo_tipo3.CurrentValue = StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Values", cmbavContratoservicosprazo_tipo3.ToJavascriptSource());
      }

      protected void E20G02( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus);
         Ddo_contratoservicosprazo_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SortedStatus", Ddo_contratoservicosprazo_tipo_Sortedstatus);
         Ddo_contratoservicosprazo_dias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazo_dias_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosprazo_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SortedStatus", Ddo_contratoservicosprazo_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosprazo_dias_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazo_dias_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         cmbavContratoservicosprazo_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
         {
            cmbavContratoservicosprazo_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         cmbavContratoservicosprazo_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
         {
            cmbavContratoservicosprazo_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         cmbavContratoservicosprazo_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
         {
            cmbavContratoservicosprazo_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicosprazo_tipo3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19ContratoServicosPrazo_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22ContratoServicosPrazo_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFContratoServicosPrazo_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "FilteredText_set", Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set);
         AV32TFContratoServicosPrazo_CntSrvCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set);
         AV36TFContratoServicosPrazo_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratoservicosprazo_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SelectedValue_set", Ddo_contratoservicosprazo_tipo_Selectedvalue_set);
         AV39TFContratoServicosPrazo_Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
         Ddo_contratoservicosprazo_dias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "FilteredText_set", Ddo_contratoservicosprazo_dias_Filteredtext_set);
         AV40TFContratoServicosPrazo_Dias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
         Ddo_contratoservicosprazo_dias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazo_dias_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSPRAZO_TIPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoServicosPrazo_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV63Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV63Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV63Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV64GXV1 = 1;
         while ( AV64GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV64GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZO_CNTSRVCOD") == 0 )
            {
               AV31TFContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0)));
               AV32TFContratoServicosPrazo_CntSrvCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazo_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0)));
               if ( ! (0==AV31TFContratoServicosPrazo_CntSrvCod) )
               {
                  Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set = StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "FilteredText_set", Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set);
               }
               if ( ! (0==AV32TFContratoServicosPrazo_CntSrvCod_To) )
               {
                  Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set = StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_cntsrvcod_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZO_TIPO_SEL") == 0 )
            {
               AV35TFContratoServicosPrazo_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV36TFContratoServicosPrazo_Tipo_Sels.FromJSonString(AV35TFContratoServicosPrazo_Tipo_SelsJson);
               if ( ! ( AV36TFContratoServicosPrazo_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_contratoservicosprazo_tipo_Selectedvalue_set = AV35TFContratoServicosPrazo_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_tipo_Internalname, "SelectedValue_set", Ddo_contratoservicosprazo_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRAZO_DIAS") == 0 )
            {
               AV39TFContratoServicosPrazo_Dias = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0)));
               AV40TFContratoServicosPrazo_Dias_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazo_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0)));
               if ( ! (0==AV39TFContratoServicosPrazo_Dias) )
               {
                  Ddo_contratoservicosprazo_dias_Filteredtext_set = StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "FilteredText_set", Ddo_contratoservicosprazo_dias_Filteredtext_set);
               }
               if ( ! (0==AV40TFContratoServicosPrazo_Dias_To) )
               {
                  Ddo_contratoservicosprazo_dias_Filteredtextto_set = StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazo_dias_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazo_dias_Filteredtextto_set);
               }
            }
            AV64GXV1 = (int)(AV64GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
            {
               AV16ContratoServicosPrazo_Tipo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoServicosPrazo_Tipo1", AV16ContratoServicosPrazo_Tipo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
               {
                  AV19ContratoServicosPrazo_Tipo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoServicosPrazo_Tipo2", AV19ContratoServicosPrazo_Tipo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZO_TIPO") == 0 )
                  {
                     AV22ContratoServicosPrazo_Tipo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazo_Tipo3", AV22ContratoServicosPrazo_Tipo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV63Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFContratoServicosPrazo_CntSrvCod) && (0==AV32TFContratoServicosPrazo_CntSrvCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZO_CNTSRVCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFContratoServicosPrazo_CntSrvCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFContratoServicosPrazo_CntSrvCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV36TFContratoServicosPrazo_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZO_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFContratoServicosPrazo_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV39TFContratoServicosPrazo_Dias) && (0==AV40TFContratoServicosPrazo_Dias_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRAZO_DIAS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39TFContratoServicosPrazo_Dias), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV40TFContratoServicosPrazo_Dias_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV63Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16ContratoServicosPrazo_Tipo1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContratoServicosPrazo_Tipo2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22ContratoServicosPrazo_Tipo3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV63Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosPrazo";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_G02( true) ;
         }
         else
         {
            wb_table2_8_G02( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_G02( true) ;
         }
         else
         {
            wb_table3_67_G02( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_G02e( true) ;
         }
         else
         {
            wb_table1_2_G02e( false) ;
         }
      }

      protected void wb_table3_67_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_G02( true) ;
         }
         else
         {
            wb_table4_70_G02( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_G02e( true) ;
         }
         else
         {
            wb_table3_67_G02e( false) ;
         }
      }

      protected void wb_table4_70_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazo_CntSrvCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazo_CntSrvCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazo_CntSrvCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosPrazo_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosPrazo_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosPrazo_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(28), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazo_Dias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazo_Dias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazo_Dias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazo_CntSrvCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazo_CntSrvCod_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoServicosPrazo_CntSrvCod_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A904ContratoServicosPrazo_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosPrazo_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosPrazo_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazo_Dias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazo_Dias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_G02e( true) ;
         }
         else
         {
            wb_table4_70_G02e( false) ;
         }
      }

      protected void wb_table2_8_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosprazotitle_Internalname, "Prazo", "", "", lblContratoservicosprazotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_G02( true) ;
         }
         else
         {
            wb_table5_13_G02( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_G02( true) ;
         }
         else
         {
            wb_table6_23_G02( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_G02e( true) ;
         }
         else
         {
            wb_table2_8_G02e( false) ;
         }
      }

      protected void wb_table6_23_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_G02( true) ;
         }
         else
         {
            wb_table7_28_G02( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_G02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_G02e( true) ;
         }
         else
         {
            wb_table6_23_G02e( false) ;
         }
      }

      protected void wb_table7_28_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosprazo_tipo1, cmbavContratoservicosprazo_tipo1_Internalname, StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1), 1, cmbavContratoservicosprazo_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContratoservicosprazo_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavContratoservicosprazo_tipo1.CurrentValue = StringUtil.RTrim( AV16ContratoServicosPrazo_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo1_Internalname, "Values", (String)(cmbavContratoservicosprazo_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosprazo_tipo2, cmbavContratoservicosprazo_tipo2_Internalname, StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2), 1, cmbavContratoservicosprazo_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContratoservicosprazo_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavContratoservicosprazo_tipo2.CurrentValue = StringUtil.RTrim( AV19ContratoServicosPrazo_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo2_Internalname, "Values", (String)(cmbavContratoservicosprazo_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicosprazo_tipo3, cmbavContratoservicosprazo_tipo3_Internalname, StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3), 1, cmbavContratoservicosprazo_tipo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContratoservicosprazo_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWContratoServicosPrazo.htm");
            cmbavContratoservicosprazo_tipo3.CurrentValue = StringUtil.RTrim( AV22ContratoServicosPrazo_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicosprazo_tipo3_Internalname, "Values", (String)(cmbavContratoservicosprazo_tipo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_G02e( true) ;
         }
         else
         {
            wb_table7_28_G02e( false) ;
         }
      }

      protected void wb_table5_13_G02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_G02e( true) ;
         }
         else
         {
            wb_table5_13_G02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAG02( ) ;
         WSG02( ) ;
         WEG02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118535466");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoservicosprazo.js", "?20203118535466");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtContratoServicosPrazo_CntSrvCod_Internalname = "CONTRATOSERVICOSPRAZO_CNTSRVCOD_"+sGXsfl_73_idx;
         cmbContratoServicosPrazo_Tipo_Internalname = "CONTRATOSERVICOSPRAZO_TIPO_"+sGXsfl_73_idx;
         edtContratoServicosPrazo_Dias_Internalname = "CONTRATOSERVICOSPRAZO_DIAS_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtContratoServicosPrazo_CntSrvCod_Internalname = "CONTRATOSERVICOSPRAZO_CNTSRVCOD_"+sGXsfl_73_fel_idx;
         cmbContratoServicosPrazo_Tipo_Internalname = "CONTRATOSERVICOSPRAZO_TIPO_"+sGXsfl_73_fel_idx;
         edtContratoServicosPrazo_Dias_Internalname = "CONTRATOSERVICOSPRAZO_DIAS_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WBG00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV61Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV62Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV62Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazo_CntSrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A903ContratoServicosPrazo_CntSrvCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoServicosPrazo_CntSrvCod_Link,(String)"",(String)"",(String)"",(String)edtContratoServicosPrazo_CntSrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_73_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSPRAZO_TIPO_" + sGXsfl_73_idx;
               cmbContratoServicosPrazo_Tipo.Name = GXCCtl;
               cmbContratoServicosPrazo_Tipo.WebTags = "";
               cmbContratoServicosPrazo_Tipo.addItem("", "(Nenhum)", 0);
               cmbContratoServicosPrazo_Tipo.addItem("A", "Acumulado", 0);
               cmbContratoServicosPrazo_Tipo.addItem("C", "Complexidade", 0);
               cmbContratoServicosPrazo_Tipo.addItem("S", "Solicitado", 0);
               cmbContratoServicosPrazo_Tipo.addItem("F", "Fixo", 0);
               cmbContratoServicosPrazo_Tipo.addItem("E", "El�stico", 0);
               cmbContratoServicosPrazo_Tipo.addItem("V", "Vari�vel", 0);
               cmbContratoServicosPrazo_Tipo.addItem("P", "Progress�o Aritm�tica", 0);
               cmbContratoServicosPrazo_Tipo.addItem("O", "Combinado", 0);
               if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
               {
                  A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosPrazo_Tipo,(String)cmbContratoServicosPrazo_Tipo_Internalname,StringUtil.RTrim( A904ContratoServicosPrazo_Tipo),(short)1,(String)cmbContratoServicosPrazo_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosPrazo_Tipo.CurrentValue = StringUtil.RTrim( A904ContratoServicosPrazo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Tipo_Internalname, "Values", (String)(cmbContratoServicosPrazo_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazo_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A905ContratoServicosPrazo_Dias), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazo_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)28,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_CNTSRVCOD"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A903ContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_TIPO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A904ContratoServicosPrazo_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZO_DIAS"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A905ContratoServicosPrazo_Dias), "ZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblContratoservicosprazotitle_Internalname = "CONTRATOSERVICOSPRAZOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavContratoservicosprazo_tipo1_Internalname = "vCONTRATOSERVICOSPRAZO_TIPO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavContratoservicosprazo_tipo2_Internalname = "vCONTRATOSERVICOSPRAZO_TIPO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavContratoservicosprazo_tipo3_Internalname = "vCONTRATOSERVICOSPRAZO_TIPO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoServicosPrazo_CntSrvCod_Internalname = "CONTRATOSERVICOSPRAZO_CNTSRVCOD";
         cmbContratoServicosPrazo_Tipo_Internalname = "CONTRATOSERVICOSPRAZO_TIPO";
         edtContratoServicosPrazo_Dias_Internalname = "CONTRATOSERVICOSPRAZO_DIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicosprazo_cntsrvcod_Internalname = "vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD";
         edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname = "vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO";
         edtavTfcontratoservicosprazo_dias_Internalname = "vTFCONTRATOSERVICOSPRAZO_DIAS";
         edtavTfcontratoservicosprazo_dias_to_Internalname = "vTFCONTRATOSERVICOSPRAZO_DIAS_TO";
         Ddo_contratoservicosprazo_cntsrvcod_Internalname = "DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD";
         edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazo_tipo_Internalname = "DDO_CONTRATOSERVICOSPRAZO_TIPO";
         edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazo_dias_Internalname = "DDO_CONTRATOSERVICOSPRAZO_DIAS";
         edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosPrazo_Dias_Jsonclick = "";
         cmbContratoServicosPrazo_Tipo_Jsonclick = "";
         edtContratoServicosPrazo_CntSrvCod_Jsonclick = "";
         cmbavContratoservicosprazo_tipo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavContratoservicosprazo_tipo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavContratoservicosprazo_tipo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosPrazo_CntSrvCod_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoServicosPrazo_Dias_Titleformat = 0;
         cmbContratoServicosPrazo_Tipo_Titleformat = 0;
         edtContratoServicosPrazo_CntSrvCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavContratoservicosprazo_tipo3.Visible = 1;
         cmbavContratoservicosprazo_tipo2.Visible = 1;
         cmbavContratoservicosprazo_tipo1.Visible = 1;
         edtContratoServicosPrazo_Dias_Title = "Dias";
         cmbContratoServicosPrazo_Tipo.Title.Text = "Tipo";
         edtContratoServicosPrazo_CntSrvCod_Title = "Contrato Servicos Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosprazo_dias_to_Jsonclick = "";
         edtavTfcontratoservicosprazo_dias_to_Visible = 1;
         edtavTfcontratoservicosprazo_dias_Jsonclick = "";
         edtavTfcontratoservicosprazo_dias_Visible = 1;
         edtavTfcontratoservicosprazo_cntsrvcod_to_Jsonclick = "";
         edtavTfcontratoservicosprazo_cntsrvcod_to_Visible = 1;
         edtavTfcontratoservicosprazo_cntsrvcod_Jsonclick = "";
         edtavTfcontratoservicosprazo_cntsrvcod_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoservicosprazo_dias_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazo_dias_Rangefilterto = "At�";
         Ddo_contratoservicosprazo_dias_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazo_dias_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazo_dias_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazo_dias_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazo_dias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazo_dias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_dias_Filtertype = "Numeric";
         Ddo_contratoservicosprazo_dias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_dias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_dias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazo_dias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazo_dias_Cls = "ColumnSettings";
         Ddo_contratoservicosprazo_dias_Tooltip = "Op��es";
         Ddo_contratoservicosprazo_dias_Caption = "";
         Ddo_contratoservicosprazo_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratoservicosprazo_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazo_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazo_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazo_tipo_Datalistfixedvalues = "A:Acumulado,C:Complexidade,S:Solicitado,F:Fixo,E:El�stico,V:Vari�vel,P:Progress�o Aritm�tica,O:Combinado";
         Ddo_contratoservicosprazo_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_tipo_Datalisttype = "FixedValues";
         Ddo_contratoservicosprazo_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazo_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazo_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazo_tipo_Cls = "ColumnSettings";
         Ddo_contratoservicosprazo_tipo_Tooltip = "Op��es";
         Ddo_contratoservicosprazo_tipo_Caption = "";
         Ddo_contratoservicosprazo_cntsrvcod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazo_cntsrvcod_Rangefilterto = "At�";
         Ddo_contratoservicosprazo_cntsrvcod_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazo_cntsrvcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazo_cntsrvcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazo_cntsrvcod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazo_cntsrvcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazo_cntsrvcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_cntsrvcod_Filtertype = "Numeric";
         Ddo_contratoservicosprazo_cntsrvcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_cntsrvcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_cntsrvcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazo_cntsrvcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazo_cntsrvcod_Cls = "ColumnSettings";
         Ddo_contratoservicosprazo_cntsrvcod_Tooltip = "Op��es";
         Ddo_contratoservicosprazo_cntsrvcod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Prazo";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30ContratoServicosPrazo_CntSrvCodTitleFilterData',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContratoServicosPrazo_TipoTitleFilterData',fld:'vCONTRATOSERVICOSPRAZO_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContratoServicosPrazo_DiasTitleFilterData',fld:'vCONTRATOSERVICOSPRAZO_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosPrazo_CntSrvCod_Titleformat',ctrl:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'Titleformat'},{av:'edtContratoServicosPrazo_CntSrvCod_Title',ctrl:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'Title'},{av:'cmbContratoServicosPrazo_Tipo'},{av:'edtContratoServicosPrazo_Dias_Titleformat',ctrl:'CONTRATOSERVICOSPRAZO_DIAS',prop:'Titleformat'},{av:'edtContratoServicosPrazo_Dias_Title',ctrl:'CONTRATOSERVICOSPRAZO_DIAS',prop:'Title'},{av:'AV44GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD.ONOPTIONCLICKED","{handler:'E12G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'SortedStatus'},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosprazo_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazo_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZO_TIPO.ONOPTIONCLICKED","{handler:'E13G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprazo_tipo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazo_tipo_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazo_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'SortedStatus'},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazo_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZO_DIAS.ONOPTIONCLICKED","{handler:'E14G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprazo_dias_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazo_dias_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazo_dias_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazo_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'SortedStatus'},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazo_tipo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28G02',iparms:[{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoServicosPrazo_CntSrvCod_Link',ctrl:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21G02',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratoservicosprazo_tipo2'},{av:'cmbavContratoservicosprazo_tipo3'},{av:'cmbavContratoservicosprazo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22G02',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'cmbavContratoservicosprazo_tipo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23G02',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratoservicosprazo_tipo2'},{av:'cmbavContratoservicosprazo_tipo3'},{av:'cmbavContratoservicosprazo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24G02',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'cmbavContratoservicosprazo_tipo2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratoservicosprazo_tipo2'},{av:'cmbavContratoservicosprazo_tipo3'},{av:'cmbavContratoservicosprazo_tipo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25G02',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'cmbavContratoservicosprazo_tipo3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19G02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'AV63Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV31TFContratoServicosPrazo_CntSrvCod',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'FilteredText_set'},{av:'AV32TFContratoServicosPrazo_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSPRAZO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZO_CNTSRVCOD',prop:'FilteredTextTo_set'},{av:'AV36TFContratoServicosPrazo_Tipo_Sels',fld:'vTFCONTRATOSERVICOSPRAZO_TIPO_SELS',pic:'',nv:null},{av:'Ddo_contratoservicosprazo_tipo_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSPRAZO_TIPO',prop:'SelectedValue_set'},{av:'AV39TFContratoServicosPrazo_Dias',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazo_dias_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'FilteredText_set'},{av:'AV40TFContratoServicosPrazo_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZO_DIAS_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazo_dias_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZO_DIAS',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoServicosPrazo_Tipo1',fld:'vCONTRATOSERVICOSPRAZO_TIPO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'cmbavContratoservicosprazo_tipo1'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoServicosPrazo_Tipo2',fld:'vCONTRATOSERVICOSPRAZO_TIPO2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoServicosPrazo_Tipo3',fld:'vCONTRATOSERVICOSPRAZO_TIPO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratoservicosprazo_tipo2'},{av:'cmbavContratoservicosprazo_tipo3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20G02',iparms:[{av:'A903ContratoServicosPrazo_CntSrvCod',fld:'CONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey = "";
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get = "";
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get = "";
         Ddo_contratoservicosprazo_tipo_Activeeventkey = "";
         Ddo_contratoservicosprazo_tipo_Selectedvalue_get = "";
         Ddo_contratoservicosprazo_dias_Activeeventkey = "";
         Ddo_contratoservicosprazo_dias_Filteredtext_get = "";
         Ddo_contratoservicosprazo_dias_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoServicosPrazo_Tipo1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19ContratoServicosPrazo_Tipo2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22ContratoServicosPrazo_Tipo3 = "";
         AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace = "";
         AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace = "";
         AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace = "";
         AV36TFContratoServicosPrazo_Tipo_Sels = new GxSimpleCollection();
         AV63Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV42DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30ContratoServicosPrazo_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosPrazo_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoServicosPrazo_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set = "";
         Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set = "";
         Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus = "";
         Ddo_contratoservicosprazo_tipo_Selectedvalue_set = "";
         Ddo_contratoservicosprazo_tipo_Sortedstatus = "";
         Ddo_contratoservicosprazo_dias_Filteredtext_set = "";
         Ddo_contratoservicosprazo_dias_Filteredtextto_set = "";
         Ddo_contratoservicosprazo_dias_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV61Update_GXI = "";
         AV26Delete = "";
         AV62Delete_GXI = "";
         A904ContratoServicosPrazo_Tipo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 = "";
         AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 = "";
         AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 = "";
         AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 = "";
         AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 = "";
         AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 = "";
         H00G02_A905ContratoServicosPrazo_Dias = new short[1] ;
         H00G02_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         H00G02_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         H00G02_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00G03_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35TFContratoServicosPrazo_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoservicosprazotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoservicosprazo__default(),
            new Object[][] {
                new Object[] {
               H00G02_A905ContratoServicosPrazo_Dias, H00G02_n905ContratoServicosPrazo_Dias, H00G02_A904ContratoServicosPrazo_Tipo, H00G02_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               H00G03_AGRID_nRecordCount
               }
            }
         );
         AV63Pgmname = "WWContratoServicosPrazo";
         /* GeneXus formulas. */
         AV63Pgmname = "WWContratoServicosPrazo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short AV39TFContratoServicosPrazo_Dias ;
      private short AV40TFContratoServicosPrazo_Dias_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A905ContratoServicosPrazo_Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias ;
      private short AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to ;
      private short edtContratoServicosPrazo_CntSrvCod_Titleformat ;
      private short cmbContratoServicosPrazo_Tipo_Titleformat ;
      private short edtContratoServicosPrazo_Dias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV31TFContratoServicosPrazo_CntSrvCod ;
      private int AV32TFContratoServicosPrazo_CntSrvCod_To ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfcontratoservicosprazo_cntsrvcod_Visible ;
      private int edtavTfcontratoservicosprazo_cntsrvcod_to_Visible ;
      private int edtavTfcontratoservicosprazo_dias_Visible ;
      private int edtavTfcontratoservicosprazo_dias_to_Visible ;
      private int edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels_Count ;
      private int AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod ;
      private int AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV43PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int AV64GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV44GridCurrentPage ;
      private long AV45GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Activeeventkey ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_get ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_get ;
      private String Ddo_contratoservicosprazo_tipo_Activeeventkey ;
      private String Ddo_contratoservicosprazo_tipo_Selectedvalue_get ;
      private String Ddo_contratoservicosprazo_dias_Activeeventkey ;
      private String Ddo_contratoservicosprazo_dias_Filteredtext_get ;
      private String Ddo_contratoservicosprazo_dias_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV16ContratoServicosPrazo_Tipo1 ;
      private String AV19ContratoServicosPrazo_Tipo2 ;
      private String AV22ContratoServicosPrazo_Tipo3 ;
      private String AV63Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Caption ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Tooltip ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Cls ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Filteredtext_set ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Filteredtextto_set ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Sortedstatus ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Filtertype ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Sortasc ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Sortdsc ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Cleanfilter ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Rangefilterfrom ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Rangefilterto ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Searchbuttontext ;
      private String Ddo_contratoservicosprazo_tipo_Caption ;
      private String Ddo_contratoservicosprazo_tipo_Tooltip ;
      private String Ddo_contratoservicosprazo_tipo_Cls ;
      private String Ddo_contratoservicosprazo_tipo_Selectedvalue_set ;
      private String Ddo_contratoservicosprazo_tipo_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazo_tipo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazo_tipo_Sortedstatus ;
      private String Ddo_contratoservicosprazo_tipo_Datalisttype ;
      private String Ddo_contratoservicosprazo_tipo_Datalistfixedvalues ;
      private String Ddo_contratoservicosprazo_tipo_Sortasc ;
      private String Ddo_contratoservicosprazo_tipo_Sortdsc ;
      private String Ddo_contratoservicosprazo_tipo_Cleanfilter ;
      private String Ddo_contratoservicosprazo_tipo_Searchbuttontext ;
      private String Ddo_contratoservicosprazo_dias_Caption ;
      private String Ddo_contratoservicosprazo_dias_Tooltip ;
      private String Ddo_contratoservicosprazo_dias_Cls ;
      private String Ddo_contratoservicosprazo_dias_Filteredtext_set ;
      private String Ddo_contratoservicosprazo_dias_Filteredtextto_set ;
      private String Ddo_contratoservicosprazo_dias_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazo_dias_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazo_dias_Sortedstatus ;
      private String Ddo_contratoservicosprazo_dias_Filtertype ;
      private String Ddo_contratoservicosprazo_dias_Sortasc ;
      private String Ddo_contratoservicosprazo_dias_Sortdsc ;
      private String Ddo_contratoservicosprazo_dias_Cleanfilter ;
      private String Ddo_contratoservicosprazo_dias_Rangefilterfrom ;
      private String Ddo_contratoservicosprazo_dias_Rangefilterto ;
      private String Ddo_contratoservicosprazo_dias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicosprazo_cntsrvcod_Internalname ;
      private String edtavTfcontratoservicosprazo_cntsrvcod_Jsonclick ;
      private String edtavTfcontratoservicosprazo_cntsrvcod_to_Internalname ;
      private String edtavTfcontratoservicosprazo_cntsrvcod_to_Jsonclick ;
      private String edtavTfcontratoservicosprazo_dias_Internalname ;
      private String edtavTfcontratoservicosprazo_dias_Jsonclick ;
      private String edtavTfcontratoservicosprazo_dias_to_Internalname ;
      private String edtavTfcontratoservicosprazo_dias_to_Jsonclick ;
      private String edtavDdo_contratoservicosprazo_cntsrvcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazo_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazo_diastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosPrazo_CntSrvCod_Internalname ;
      private String cmbContratoServicosPrazo_Tipo_Internalname ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String edtContratoServicosPrazo_Dias_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 ;
      private String AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 ;
      private String AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavContratoservicosprazo_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavContratoservicosprazo_tipo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavContratoservicosprazo_tipo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosprazo_cntsrvcod_Internalname ;
      private String Ddo_contratoservicosprazo_tipo_Internalname ;
      private String Ddo_contratoservicosprazo_dias_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosPrazo_CntSrvCod_Title ;
      private String edtContratoServicosPrazo_Dias_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoServicosPrazo_CntSrvCod_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoservicosprazotitle_Internalname ;
      private String lblContratoservicosprazotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String cmbavContratoservicosprazo_tipo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String cmbavContratoservicosprazo_tipo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String cmbavContratoservicosprazo_tipo3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosPrazo_CntSrvCod_Jsonclick ;
      private String cmbContratoServicosPrazo_Tipo_Jsonclick ;
      private String edtContratoServicosPrazo_Dias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosprazo_cntsrvcod_Includesortasc ;
      private bool Ddo_contratoservicosprazo_cntsrvcod_Includesortdsc ;
      private bool Ddo_contratoservicosprazo_cntsrvcod_Includefilter ;
      private bool Ddo_contratoservicosprazo_cntsrvcod_Filterisrange ;
      private bool Ddo_contratoservicosprazo_cntsrvcod_Includedatalist ;
      private bool Ddo_contratoservicosprazo_tipo_Includesortasc ;
      private bool Ddo_contratoservicosprazo_tipo_Includesortdsc ;
      private bool Ddo_contratoservicosprazo_tipo_Includefilter ;
      private bool Ddo_contratoservicosprazo_tipo_Includedatalist ;
      private bool Ddo_contratoservicosprazo_tipo_Allowmultipleselection ;
      private bool Ddo_contratoservicosprazo_dias_Includesortasc ;
      private bool Ddo_contratoservicosprazo_dias_Includesortdsc ;
      private bool Ddo_contratoservicosprazo_dias_Includefilter ;
      private bool Ddo_contratoservicosprazo_dias_Filterisrange ;
      private bool Ddo_contratoservicosprazo_dias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n905ContratoServicosPrazo_Dias ;
      private bool AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 ;
      private bool AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV35TFContratoServicosPrazo_Tipo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV33ddo_ContratoServicosPrazo_CntSrvCodTitleControlIdToReplace ;
      private String AV37ddo_ContratoServicosPrazo_TipoTitleControlIdToReplace ;
      private String AV41ddo_ContratoServicosPrazo_DiasTitleControlIdToReplace ;
      private String AV61Update_GXI ;
      private String AV62Delete_GXI ;
      private String AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 ;
      private String AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 ;
      private String AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 ;
      private String AV25Update ;
      private String AV26Delete ;
      private IGxSession AV27Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavContratoservicosprazo_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavContratoservicosprazo_tipo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavContratoservicosprazo_tipo3 ;
      private GXCombobox cmbContratoServicosPrazo_Tipo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00G02_A905ContratoServicosPrazo_Dias ;
      private bool[] H00G02_n905ContratoServicosPrazo_Dias ;
      private String[] H00G02_A904ContratoServicosPrazo_Tipo ;
      private int[] H00G02_A903ContratoServicosPrazo_CntSrvCod ;
      private long[] H00G03_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36TFContratoServicosPrazo_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoServicosPrazo_CntSrvCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContratoServicosPrazo_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContratoServicosPrazo_DiasTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV42DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoservicosprazo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00G02( IGxContext context ,
                                             String A904ContratoServicosPrazo_Tipo ,
                                             IGxCollection AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels ,
                                             String AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 ,
                                             String AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 ,
                                             bool AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 ,
                                             String AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 ,
                                             String AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 ,
                                             bool AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 ,
                                             String AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 ,
                                             String AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 ,
                                             int AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod ,
                                             int AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to ,
                                             int AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels_Count ,
                                             short AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias ,
                                             short AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             short A905ContratoServicosPrazo_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicosPrazo_Dias], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_CntSrvCod]";
         sFromString = " FROM [ContratoServicosPrazo] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_CntSrvCod] >= @AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_CntSrvCod] >= @AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_CntSrvCod] <= @AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_CntSrvCod] <= @AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels, "[ContratoServicosPrazo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels, "[ContratoServicosPrazo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Dias] >= @AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Dias] >= @AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Dias] <= @AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Dias] <= @AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_Tipo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_Dias]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_Dias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrazo_CntSrvCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00G03( IGxContext context ,
                                             String A904ContratoServicosPrazo_Tipo ,
                                             IGxCollection AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels ,
                                             String AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1 ,
                                             String AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1 ,
                                             bool AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 ,
                                             String AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2 ,
                                             String AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2 ,
                                             bool AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 ,
                                             String AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3 ,
                                             String AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3 ,
                                             int AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod ,
                                             int AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to ,
                                             int AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels_Count ,
                                             short AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias ,
                                             short AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             short A905ContratoServicosPrazo_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosPrazo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV48WWContratoServicosPrazoDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV50WWContratoServicosPrazoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWContratoServicosPrazoDS_4_Dynamicfiltersselector2, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV53WWContratoServicosPrazoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV54WWContratoServicosPrazoDS_7_Dynamicfiltersselector3, "CONTRATOSERVICOSPRAZO_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Tipo] = @AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Tipo] = @AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_CntSrvCod] >= @AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_CntSrvCod] >= @AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_CntSrvCod] <= @AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_CntSrvCod] <= @AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels, "[ContratoServicosPrazo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV58WWContratoServicosPrazoDS_11_Tfcontratoservicosprazo_tipo_sels, "[ContratoServicosPrazo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Dias] >= @AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Dias] >= @AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrazo_Dias] <= @AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrazo_Dias] <= @AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00G02(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
               case 1 :
                     return conditional_H00G03(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00G02 ;
          prmH00G02 = new Object[] {
          new Object[] {"@AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00G03 ;
          prmH00G03 = new Object[] {
          new Object[] {"@AV49WWContratoServicosPrazoDS_2_Contratoservicosprazo_tipo1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV52WWContratoServicosPrazoDS_5_Contratoservicosprazo_tipo2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV55WWContratoServicosPrazoDS_8_Contratoservicosprazo_tipo3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV56WWContratoServicosPrazoDS_9_Tfcontratoservicosprazo_cntsrvcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosPrazoDS_10_Tfcontratoservicosprazo_cntsrvcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59WWContratoServicosPrazoDS_12_Tfcontratoservicosprazo_dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV60WWContratoServicosPrazoDS_13_Tfcontratoservicosprazo_dias_to",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00G02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G02,11,0,true,false )
             ,new CursorDef("H00G03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G03,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                return;
       }
    }

 }

}
