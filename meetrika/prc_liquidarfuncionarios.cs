/*
               File: PRC_LiquidarFuncionarios
        Description: Liquidar Funcionarios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:6.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_liquidarfuncionarios : GXProcedure
   {
      public prc_liquidarfuncionarios( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_liquidarfuncionarios( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UserId ,
                           String aP1_Observacao )
      {
         this.AV14UserId = aP0_UserId;
         this.AV11Observacao = aP1_Observacao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_UserId ,
                                 String aP1_Observacao )
      {
         prc_liquidarfuncionarios objprc_liquidarfuncionarios;
         objprc_liquidarfuncionarios = new prc_liquidarfuncionarios();
         objprc_liquidarfuncionarios.AV14UserId = aP0_UserId;
         objprc_liquidarfuncionarios.AV11Observacao = aP1_Observacao;
         objprc_liquidarfuncionarios.context.SetSubmitInitialConfig(context);
         objprc_liquidarfuncionarios.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_liquidarfuncionarios);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_liquidarfuncionarios)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Codigos.FromXml(AV9WebSession.Get("SdtCodigos"), "Collection");
         /*
            INSERT RECORD ON TABLE ContagemResultadoLiqLog

         */
         A1034ContagemResultadoLiqLog_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1368ContagemResultadoLiqLog_Owner = AV14UserId;
         A1366ContagemResultadoLiqLog_Observacao = AV11Observacao;
         n1366ContagemResultadoLiqLog_Observacao = false;
         /* Using cursor P00942 */
         pr_default.execute(0, new Object[] {A1034ContagemResultadoLiqLog_Data, n1366ContagemResultadoLiqLog_Observacao, A1366ContagemResultadoLiqLog_Observacao, A1368ContagemResultadoLiqLog_Owner});
         A1033ContagemResultadoLiqLog_Codigo = P00942_A1033ContagemResultadoLiqLog_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLog") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV15ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
         AV16i = 1;
         AV19GXV1 = 1;
         while ( AV19GXV1 <= AV8Codigos.Count )
         {
            AV10Codigo = (int)(AV8Codigos.GetNumeric(AV19GXV1));
            /* Using cursor P00944 */
            pr_default.execute(1, new Object[] {AV10Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P00944_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00944_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00944_A456ContagemResultado_Codigo[0];
               A1043ContagemResultado_LiqLogCod = P00944_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = P00944_n1043ContagemResultado_LiqLogCod[0];
               A584ContagemResultado_ContadorFM = P00944_A584ContagemResultado_ContadorFM[0];
               A684ContagemResultado_PFBFSUltima = P00944_A684ContagemResultado_PFBFSUltima[0];
               A682ContagemResultado_PFBFMUltima = P00944_A682ContagemResultado_PFBFMUltima[0];
               A584ContagemResultado_ContadorFM = P00944_A584ContagemResultado_ContadorFM[0];
               A684ContagemResultado_PFBFSUltima = P00944_A684ContagemResultado_PFBFSUltima[0];
               A682ContagemResultado_PFBFMUltima = P00944_A682ContagemResultado_PFBFMUltima[0];
               AV13Funcionario = A584ContagemResultado_ContadorFM;
               /* Using cursor P00945 */
               pr_default.execute(2, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, A584ContagemResultado_ContadorFM});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = P00945_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = P00945_A69ContratadaUsuario_UsuarioCod[0];
                  A576ContratadaUsuario_CstUntPrdNrm = P00945_A576ContratadaUsuario_CstUntPrdNrm[0];
                  n576ContratadaUsuario_CstUntPrdNrm = P00945_n576ContratadaUsuario_CstUntPrdNrm[0];
                  if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
                  {
                     if ( A682ContagemResultado_PFBFMUltima < A684ContagemResultado_PFBFSUltima )
                     {
                        AV12Valor = (decimal)(A576ContratadaUsuario_CstUntPrdNrm*A682ContagemResultado_PFBFMUltima);
                     }
                     else
                     {
                        AV12Valor = (decimal)(A576ContratadaUsuario_CstUntPrdNrm*A684ContagemResultado_PFBFSUltima);
                     }
                  }
                  else
                  {
                     AV12Valor = (decimal)(A576ContratadaUsuario_CstUntPrdNrm*A682ContagemResultado_PFBFMUltima);
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /*
                  INSERT RECORD ON TABLE ContagemResultadoLiqLogOS

               */
               W1033ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
               A1033ContagemResultadoLiqLog_Codigo = AV15ContagemResultadoLiqLog_Codigo;
               A1370ContagemResultadoLiqLogOS_Codigo = AV16i;
               A1371ContagemResultadoLiqLogOS_OSCod = AV10Codigo;
               A1372ContagemResultadoLiqLogOS_UserCod = AV13Funcionario;
               A1375ContagemResultadoLiqLogOS_Valor = AV12Valor;
               /* Using cursor P00946 */
               pr_default.execute(3, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo, A1372ContagemResultadoLiqLogOS_UserCod, A1375ContagemResultadoLiqLogOS_Valor, A1371ContagemResultadoLiqLogOS_OSCod});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLogOS") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1033ContagemResultadoLiqLog_Codigo = W1033ContagemResultadoLiqLog_Codigo;
               /* End Insert */
               AV16i = (short)(AV16i+1);
               A1043ContagemResultado_LiqLogCod = AV15ContagemResultadoLiqLog_Codigo;
               n1043ContagemResultado_LiqLogCod = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00947 */
               pr_default.execute(4, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P00948 */
               pr_default.execute(5, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            AV19GXV1 = (int)(AV19GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_LiquidarFuncionarios");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Codigos = new GxSimpleCollection();
         AV9WebSession = context.GetSession();
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         A1366ContagemResultadoLiqLog_Observacao = "";
         P00942_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         Gx_emsg = "";
         scmdbuf = "";
         P00944_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00944_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00944_A456ContagemResultado_Codigo = new int[1] ;
         P00944_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00944_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00944_A584ContagemResultado_ContadorFM = new int[1] ;
         P00944_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00944_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00945_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00945_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00945_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00945_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_liquidarfuncionarios__default(),
            new Object[][] {
                new Object[] {
               P00942_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               P00944_A490ContagemResultado_ContratadaCod, P00944_n490ContagemResultado_ContratadaCod, P00944_A456ContagemResultado_Codigo, P00944_A1043ContagemResultado_LiqLogCod, P00944_n1043ContagemResultado_LiqLogCod, P00944_A584ContagemResultado_ContadorFM, P00944_A684ContagemResultado_PFBFSUltima, P00944_A682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               P00945_A66ContratadaUsuario_ContratadaCod, P00945_A69ContratadaUsuario_UsuarioCod, P00945_A576ContratadaUsuario_CstUntPrdNrm, P00945_n576ContratadaUsuario_CstUntPrdNrm
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16i ;
      private int AV14UserId ;
      private int GX_INS125 ;
      private int A1368ContagemResultadoLiqLog_Owner ;
      private int A1033ContagemResultadoLiqLog_Codigo ;
      private int AV15ContagemResultadoLiqLog_Codigo ;
      private int AV19GXV1 ;
      private int AV10Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV13Funcionario ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int GX_INS164 ;
      private int W1033ContagemResultadoLiqLog_Codigo ;
      private int A1370ContagemResultadoLiqLogOS_Codigo ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV12Valor ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private bool n1366ContagemResultadoLiqLog_Observacao ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private String AV11Observacao ;
      private String A1366ContagemResultadoLiqLog_Observacao ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00942_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] P00944_A490ContagemResultado_ContratadaCod ;
      private bool[] P00944_n490ContagemResultado_ContratadaCod ;
      private int[] P00944_A456ContagemResultado_Codigo ;
      private int[] P00944_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00944_n1043ContagemResultado_LiqLogCod ;
      private int[] P00944_A584ContagemResultado_ContadorFM ;
      private decimal[] P00944_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P00944_A682ContagemResultado_PFBFMUltima ;
      private int[] P00945_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00945_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] P00945_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00945_n576ContratadaUsuario_CstUntPrdNrm ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
   }

   public class prc_liquidarfuncionarios__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00942 ;
          prmP00942 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoLiqLog_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoLiqLog_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00944 ;
          prmP00944 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00945 ;
          prmP00945 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFM",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00946 ;
          prmP00946 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00947 ;
          prmP00947 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00948 ;
          prmP00948 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00942", "INSERT INTO [ContagemResultadoLiqLog]([ContagemResultadoLiqLog_Data], [ContagemResultadoLiqLog_Observacao], [ContagemResultadoLiqLog_Owner]) VALUES(@ContagemResultadoLiqLog_Data, @ContagemResultadoLiqLog_Observacao, @ContagemResultadoLiqLog_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00942)
             ,new CursorDef("P00944", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_LiqLogCod], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T2.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV10Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00944,1,0,true,true )
             ,new CursorDef("P00945", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_CstUntPrdNrm] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @ContagemResultado_ContadorFM ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00945,1,0,false,true )
             ,new CursorDef("P00946", "INSERT INTO [ContagemResultadoLiqLogOS]([ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLogOS_UserCod], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLogOS_OSCod]) VALUES(@ContagemResultadoLiqLog_Codigo, @ContagemResultadoLiqLogOS_Codigo, @ContagemResultadoLiqLogOS_UserCod, @ContagemResultadoLiqLogOS_Valor, @ContagemResultadoLiqLogOS_OSCod)", GxErrorMask.GX_NOMASK,prmP00946)
             ,new CursorDef("P00947", "UPDATE [ContagemResultado] SET [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00947)
             ,new CursorDef("P00948", "UPDATE [ContagemResultado] SET [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00948)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
