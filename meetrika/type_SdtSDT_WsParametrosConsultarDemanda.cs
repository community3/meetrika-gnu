/*
               File: type_SdtSDT_WsParametrosConsultarDemanda
        Description: SDT_WsParametrosConsultarDemanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 7/1/2019 20:17:9.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Parametros" )]
   [XmlType(TypeName =  "Parametros" , Namespace = "Parametros" )]
   [Serializable]
   public class SdtSDT_WsParametrosConsultarDemanda : GxUserType
   {
      public SdtSDT_WsParametrosConsultarDemanda( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda = "";
      }

      public SdtSDT_WsParametrosConsultarDemanda( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
            mapper[ "SituacaoDemanda" ] =  "Situacaodemanda" ;
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WsParametrosConsultarDemanda deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "Parametros" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WsParametrosConsultarDemanda)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WsParametrosConsultarDemanda obj ;
         obj = this;
         obj.gxTpr_Usuario = deserialized.gxTpr_Usuario;
         obj.gxTpr_Situacaodemanda = deserialized.gxTpr_Situacaodemanda;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario") )
               {
                  gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SituacaoDemanda") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "SituacaoDemanda") == 0 ) )
               {
                  gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Parametros";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "Parametros";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Usuario", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "Parametros") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "Parametros");
         }
         oWriter.WriteElement("SituacaoDemanda", StringUtil.RTrim( gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda));
         if ( StringUtil.StrCmp(sNameSpace, "SituacaoDemanda") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "SituacaoDemanda");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Usuario", gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario, false);
         AddObjectProperty("SituacaoDemanda", gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda, false);
         return  ;
      }

      [  SoapElement( ElementName = "Usuario" )]
      [  XmlElement( ElementName = "Usuario"   )]
      public int gxTpr_Usuario
      {
         get {
            return gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario ;
         }

         set {
            gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SituacaoDemanda" )]
      [  XmlElement( ElementName = "SituacaoDemanda" , Namespace = "SituacaoDemanda"  )]
      public String gxTpr_Situacaodemanda
      {
         get {
            return gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda ;
         }

         set {
            gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WsParametrosConsultarDemanda_Usuario ;
      protected String gxTv_SdtSDT_WsParametrosConsultarDemanda_Situacaodemanda ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_WsParametrosConsultarDemanda", Namespace = "Parametros")]
   public class SdtSDT_WsParametrosConsultarDemanda_RESTInterface : GxGenericCollectionItem<SdtSDT_WsParametrosConsultarDemanda>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WsParametrosConsultarDemanda_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WsParametrosConsultarDemanda_RESTInterface( SdtSDT_WsParametrosConsultarDemanda psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Usuario" , Order = 0 )]
      public Nullable<int> gxTpr_Usuario
      {
         get {
            return sdt.gxTpr_Usuario ;
         }

         set {
            sdt.gxTpr_Usuario = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SituacaoDemanda" , Order = 1 )]
      public String gxTpr_Situacaodemanda
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Situacaodemanda) ;
         }

         set {
            sdt.gxTpr_Situacaodemanda = (String)(value);
         }

      }

      public SdtSDT_WsParametrosConsultarDemanda sdt
      {
         get {
            return (SdtSDT_WsParametrosConsultarDemanda)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WsParametrosConsultarDemanda() ;
         }
      }

   }

}
