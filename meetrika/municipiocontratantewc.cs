/*
               File: MunicipioContratanteWC
        Description: Municipio Contratante WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:14:30.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class municipiocontratantewc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public municipiocontratantewc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public municipiocontratantewc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Municipio_Codigo )
      {
         this.AV7Municipio_Codigo = aP0_Municipio_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkContratante_Ativo = new GXCheckbox();
         cmbContratante_EmailSdaAut = new GXCombobox();
         cmbContratante_EmailSdaSec = new GXCombobox();
         cmbContratante_OSAutomatica = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Municipio_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_84_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Contratante_CNPJ1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
                  AV19Contratante_RazaoSocial1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
                  AV21DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
                  AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Contratante_CNPJ2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
                  AV24Contratante_RazaoSocial2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
                  AV26DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                  AV28Contratante_CNPJ3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
                  AV29Contratante_RazaoSocial3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
                  AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
                  AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV7Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
                  AV44Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
                  AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A335Contratante_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A14Contratante_Email = GetNextPar( );
                  n14Contratante_Email = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAP52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV44Pgmname = "MunicipioContratanteWC";
               context.Gx_err = 0;
               WSP52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Municipio Contratante WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117143080");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("municipiocontratantewc.aspx") + "?" + UrlEncode("" +AV7Municipio_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_CNPJ1", AV18Contratante_CNPJ1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL1", StringUtil.RTrim( AV19Contratante_RazaoSocial1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_CNPJ2", AV23Contratante_CNPJ2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL2", StringUtil.RTrim( AV24Contratante_RazaoSocial2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_CNPJ3", AV28Contratante_CNPJ3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL3", StringUtil.RTrim( AV29Contratante_RazaoSocial3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV44Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormP52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("municipiocontratantewc.js", "?20203117143140");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "MunicipioContratanteWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Municipio Contratante WC" ;
      }

      protected void WBP50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "municipiocontratantewc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_P52( true) ;
         }
         else
         {
            wb_table1_2_P52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtMunicipio_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_MunicipioContratanteWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
         }
         wbLoad = true;
      }

      protected void STARTP52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Municipio Contratante WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPP50( ) ;
            }
         }
      }

      protected void WSP52( )
      {
         STARTP52( ) ;
         EVTP52( ) ;
      }

      protected void EVTP52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11P52 */
                                    E11P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12P52 */
                                    E12P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13P52 */
                                    E13P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14P52 */
                                    E14P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15P52 */
                                    E15P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16P52 */
                                    E16P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17P52 */
                                    E17P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18P52 */
                                    E18P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19P52 */
                                    E19P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20P52 */
                                    E20P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21P52 */
                                    E21P52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP50( ) ;
                              }
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV32Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV41Update_GXI : context.convertURL( context.PathToRelativeUrl( AV32Update))));
                              AV33Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV42Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV33Delete))));
                              AV34Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV43Display_GXI : context.convertURL( context.PathToRelativeUrl( AV34Display))));
                              A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
                              A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratante_PessoaCod_Internalname), ",", "."));
                              A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
                              n12Contratante_CNPJ = false;
                              A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
                              n9Contratante_RazaoSocial = false;
                              A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
                              A11Contratante_IE = cgiGet( edtContratante_IE_Internalname);
                              A13Contratante_WebSite = cgiGet( edtContratante_WebSite_Internalname);
                              n13Contratante_WebSite = false;
                              A14Contratante_Email = cgiGet( edtContratante_Email_Internalname);
                              n14Contratante_Email = false;
                              A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
                              A32Contratante_Ramal = cgiGet( edtContratante_Ramal_Internalname);
                              n32Contratante_Ramal = false;
                              A33Contratante_Fax = cgiGet( edtContratante_Fax_Internalname);
                              n33Contratante_Fax = false;
                              A336Contratante_AgenciaNome = StringUtil.Upper( cgiGet( edtContratante_AgenciaNome_Internalname));
                              n336Contratante_AgenciaNome = false;
                              A337Contratante_AgenciaNro = cgiGet( edtContratante_AgenciaNro_Internalname);
                              n337Contratante_AgenciaNro = false;
                              A338Contratante_BancoNome = StringUtil.Upper( cgiGet( edtContratante_BancoNome_Internalname));
                              n338Contratante_BancoNome = false;
                              A339Contratante_BancoNro = cgiGet( edtContratante_BancoNro_Internalname);
                              n339Contratante_BancoNro = false;
                              A16Contratante_ContaCorrente = cgiGet( edtContratante_ContaCorrente_Internalname);
                              n16Contratante_ContaCorrente = false;
                              A30Contratante_Ativo = StringUtil.StrToBool( cgiGet( chkContratante_Ativo_Internalname));
                              A547Contratante_EmailSdaHost = cgiGet( edtContratante_EmailSdaHost_Internalname);
                              n547Contratante_EmailSdaHost = false;
                              A548Contratante_EmailSdaUser = cgiGet( edtContratante_EmailSdaUser_Internalname);
                              n548Contratante_EmailSdaUser = false;
                              A549Contratante_EmailSdaPass = cgiGet( edtContratante_EmailSdaPass_Internalname);
                              n549Contratante_EmailSdaPass = false;
                              A550Contratante_EmailSdaKey = cgiGet( edtContratante_EmailSdaKey_Internalname);
                              n550Contratante_EmailSdaKey = false;
                              cmbContratante_EmailSdaAut.Name = cmbContratante_EmailSdaAut_Internalname;
                              cmbContratante_EmailSdaAut.CurrentValue = cgiGet( cmbContratante_EmailSdaAut_Internalname);
                              A551Contratante_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbContratante_EmailSdaAut_Internalname));
                              n551Contratante_EmailSdaAut = false;
                              A552Contratante_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( edtContratante_EmailSdaPort_Internalname), ",", "."));
                              n552Contratante_EmailSdaPort = false;
                              cmbContratante_EmailSdaSec.Name = cmbContratante_EmailSdaSec_Internalname;
                              cmbContratante_EmailSdaSec.CurrentValue = cgiGet( cmbContratante_EmailSdaSec_Internalname);
                              A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cgiGet( cmbContratante_EmailSdaSec_Internalname), "."));
                              n1048Contratante_EmailSdaSec = false;
                              cmbContratante_OSAutomatica.Name = cmbContratante_OSAutomatica_Internalname;
                              cmbContratante_OSAutomatica.CurrentValue = cgiGet( cmbContratante_OSAutomatica_Internalname);
                              A593Contratante_OSAutomatica = StringUtil.StrToBool( cgiGet( cmbContratante_OSAutomatica_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22P52 */
                                          E22P52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23P52 */
                                          E23P52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24P52 */
                                          E24P52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_cnpj1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ1"), AV18Contratante_CNPJ1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_razaosocial1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL1"), AV19Contratante_RazaoSocial1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_cnpj2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ2"), AV23Contratante_CNPJ2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_razaosocial2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL2"), AV24Contratante_RazaoSocial2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_cnpj3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ3"), AV28Contratante_CNPJ3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratante_razaosocial3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL3"), AV29Contratante_RazaoSocial3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPP50( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEP52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormP52( ) ;
            }
         }
      }

      protected void PAP52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATANTE_CNPJ", "CNPJ", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATANTE_RAZAOSOCIAL", "Social", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATANTE_CNPJ", "CNPJ", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATANTE_RAZAOSOCIAL", "Social", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATANTE_CNPJ", "CNPJ", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATANTE_RAZAOSOCIAL", "Social", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTRATANTE_ATIVO_" + sGXsfl_84_idx;
            chkContratante_Ativo.Name = GXCCtl;
            chkContratante_Ativo.WebTags = "";
            chkContratante_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratante_Ativo_Internalname, "TitleCaption", chkContratante_Ativo.Caption);
            chkContratante_Ativo.CheckedValue = "false";
            GXCCtl = "CONTRATANTE_EMAILSDAAUT_" + sGXsfl_84_idx;
            cmbContratante_EmailSdaAut.Name = GXCCtl;
            cmbContratante_EmailSdaAut.WebTags = "";
            cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
            {
               A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
               n551Contratante_EmailSdaAut = false;
            }
            GXCCtl = "CONTRATANTE_EMAILSDASEC_" + sGXsfl_84_idx;
            cmbContratante_EmailSdaSec.Name = GXCCtl;
            cmbContratante_EmailSdaSec.WebTags = "";
            cmbContratante_EmailSdaSec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContratante_EmailSdaSec.addItem("1", "SSL e TLS", 0);
            if ( cmbContratante_EmailSdaSec.ItemCount > 0 )
            {
               A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbContratante_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0))), "."));
               n1048Contratante_EmailSdaSec = false;
            }
            GXCCtl = "CONTRATANTE_OSAUTOMATICA_" + sGXsfl_84_idx;
            cmbContratante_OSAutomatica.Name = GXCCtl;
            cmbContratante_OSAutomatica.WebTags = "";
            cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
            cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
            if ( cmbContratante_OSAutomatica.ItemCount > 0 )
            {
               A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Contratante_CNPJ1 ,
                                       String AV19Contratante_RazaoSocial1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23Contratante_CNPJ2 ,
                                       String AV24Contratante_RazaoSocial2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       String AV28Contratante_CNPJ3 ,
                                       String AV29Contratante_RazaoSocial3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV7Municipio_Codigo ,
                                       String AV44Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A29Contratante_Codigo ,
                                       int A335Contratante_PessoaCod ,
                                       String A14Contratante_Email ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFP52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_NOMEFANTASIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_NOMEFANTASIA", StringUtil.RTrim( A10Contratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_IE", StringUtil.RTrim( A11Contratante_IE));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_WEBSITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_WEBSITE", A13Contratante_WebSite);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A14Contratante_Email, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAIL", A14Contratante_Email);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_TELEFONE", StringUtil.RTrim( A31Contratante_Telefone));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_RAMAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_RAMAL", StringUtil.RTrim( A32Contratante_Ramal));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_FAX", StringUtil.RTrim( A33Contratante_Fax));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_AGENCIANOME", StringUtil.RTrim( A336Contratante_AgenciaNome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_AGENCIANRO", StringUtil.RTrim( A337Contratante_AgenciaNro));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_BANCONOME", StringUtil.RTrim( A338Contratante_BancoNome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_BANCONRO", StringUtil.RTrim( A339Contratante_BancoNro));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_CONTACORRENTE", StringUtil.RTrim( A16Contratante_ContaCorrente));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_ATIVO", GetSecureSignedToken( sPrefix, A30Contratante_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_ATIVO", StringUtil.BoolToStr( A30Contratante_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAHOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A547Contratante_EmailSdaHost, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAHOST", A547Contratante_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAUSER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A548Contratante_EmailSdaUser, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAUSER", A548Contratante_EmailSdaUser);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAPASS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A549Contratante_EmailSdaPass, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAPASS", A549Contratante_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAKEY", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A550Contratante_EmailSdaKey, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAKEY", StringUtil.RTrim( A550Contratante_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A551Contratante_EmailSdaAut));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAAUT", StringUtil.BoolToStr( A551Contratante_EmailSdaAut));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAPORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A552Contratante_EmailSdaPort), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDAPORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDASEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1048Contratante_EmailSdaSec), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_EMAILSDASEC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1048Contratante_EmailSdaSec), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSAUTOMATICA", GetSecureSignedToken( sPrefix, A593Contratante_OSAutomatica));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATANTE_OSAUTOMATICA", StringUtil.BoolToStr( A593Contratante_OSAutomatica));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFP52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV44Pgmname = "MunicipioContratanteWC";
         context.Gx_err = 0;
      }

      protected void RFP52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 84;
         /* Execute user event: E23P52 */
         E23P52 ();
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         nGXsfl_84_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Contratante_CNPJ1 ,
                                                 AV19Contratante_RazaoSocial1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23Contratante_CNPJ2 ,
                                                 AV24Contratante_RazaoSocial2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV28Contratante_CNPJ3 ,
                                                 AV29Contratante_RazaoSocial3 ,
                                                 A12Contratante_CNPJ ,
                                                 A9Contratante_RazaoSocial ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A25Municipio_Codigo ,
                                                 AV7Municipio_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV18Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratante_CNPJ1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
            lV18Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratante_CNPJ1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
            lV19Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratante_RazaoSocial1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
            lV19Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratante_RazaoSocial1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
            lV23Contratante_CNPJ2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratante_CNPJ2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
            lV23Contratante_CNPJ2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratante_CNPJ2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
            lV24Contratante_RazaoSocial2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratante_RazaoSocial2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
            lV24Contratante_RazaoSocial2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratante_RazaoSocial2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
            lV28Contratante_CNPJ3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratante_CNPJ3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
            lV28Contratante_CNPJ3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratante_CNPJ3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
            lV29Contratante_RazaoSocial3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratante_RazaoSocial3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
            lV29Contratante_RazaoSocial3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratante_RazaoSocial3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
            /* Using cursor H00P52 */
            pr_default.execute(0, new Object[] {AV7Municipio_Codigo, lV18Contratante_CNPJ1, lV18Contratante_CNPJ1, lV19Contratante_RazaoSocial1, lV19Contratante_RazaoSocial1, lV23Contratante_CNPJ2, lV23Contratante_CNPJ2, lV24Contratante_RazaoSocial2, lV24Contratante_RazaoSocial2, lV28Contratante_CNPJ3, lV28Contratante_CNPJ3, lV29Contratante_RazaoSocial3, lV29Contratante_RazaoSocial3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_84_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A25Municipio_Codigo = H00P52_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               n25Municipio_Codigo = H00P52_n25Municipio_Codigo[0];
               A593Contratante_OSAutomatica = H00P52_A593Contratante_OSAutomatica[0];
               A1048Contratante_EmailSdaSec = H00P52_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = H00P52_n1048Contratante_EmailSdaSec[0];
               A552Contratante_EmailSdaPort = H00P52_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = H00P52_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = H00P52_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = H00P52_n551Contratante_EmailSdaAut[0];
               A550Contratante_EmailSdaKey = H00P52_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = H00P52_n550Contratante_EmailSdaKey[0];
               A549Contratante_EmailSdaPass = H00P52_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = H00P52_n549Contratante_EmailSdaPass[0];
               A548Contratante_EmailSdaUser = H00P52_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = H00P52_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = H00P52_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = H00P52_n547Contratante_EmailSdaHost[0];
               A30Contratante_Ativo = H00P52_A30Contratante_Ativo[0];
               A16Contratante_ContaCorrente = H00P52_A16Contratante_ContaCorrente[0];
               n16Contratante_ContaCorrente = H00P52_n16Contratante_ContaCorrente[0];
               A339Contratante_BancoNro = H00P52_A339Contratante_BancoNro[0];
               n339Contratante_BancoNro = H00P52_n339Contratante_BancoNro[0];
               A338Contratante_BancoNome = H00P52_A338Contratante_BancoNome[0];
               n338Contratante_BancoNome = H00P52_n338Contratante_BancoNome[0];
               A337Contratante_AgenciaNro = H00P52_A337Contratante_AgenciaNro[0];
               n337Contratante_AgenciaNro = H00P52_n337Contratante_AgenciaNro[0];
               A336Contratante_AgenciaNome = H00P52_A336Contratante_AgenciaNome[0];
               n336Contratante_AgenciaNome = H00P52_n336Contratante_AgenciaNome[0];
               A33Contratante_Fax = H00P52_A33Contratante_Fax[0];
               n33Contratante_Fax = H00P52_n33Contratante_Fax[0];
               A32Contratante_Ramal = H00P52_A32Contratante_Ramal[0];
               n32Contratante_Ramal = H00P52_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H00P52_A31Contratante_Telefone[0];
               A14Contratante_Email = H00P52_A14Contratante_Email[0];
               n14Contratante_Email = H00P52_n14Contratante_Email[0];
               A13Contratante_WebSite = H00P52_A13Contratante_WebSite[0];
               n13Contratante_WebSite = H00P52_n13Contratante_WebSite[0];
               A11Contratante_IE = H00P52_A11Contratante_IE[0];
               A10Contratante_NomeFantasia = H00P52_A10Contratante_NomeFantasia[0];
               A9Contratante_RazaoSocial = H00P52_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H00P52_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H00P52_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H00P52_n12Contratante_CNPJ[0];
               A335Contratante_PessoaCod = H00P52_A335Contratante_PessoaCod[0];
               A29Contratante_Codigo = H00P52_A29Contratante_Codigo[0];
               A9Contratante_RazaoSocial = H00P52_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H00P52_n9Contratante_RazaoSocial[0];
               A12Contratante_CNPJ = H00P52_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H00P52_n12Contratante_CNPJ[0];
               /* Execute user event: E24P52 */
               E24P52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WBP50( ) ;
         }
         nGXsfl_84_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Contratante_CNPJ1 ,
                                              AV19Contratante_RazaoSocial1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23Contratante_CNPJ2 ,
                                              AV24Contratante_RazaoSocial2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV28Contratante_CNPJ3 ,
                                              AV29Contratante_RazaoSocial3 ,
                                              A12Contratante_CNPJ ,
                                              A9Contratante_RazaoSocial ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A25Municipio_Codigo ,
                                              AV7Municipio_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV18Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratante_CNPJ1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
         lV18Contratante_CNPJ1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratante_CNPJ1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
         lV19Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratante_RazaoSocial1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
         lV19Contratante_RazaoSocial1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratante_RazaoSocial1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
         lV23Contratante_CNPJ2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratante_CNPJ2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
         lV23Contratante_CNPJ2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratante_CNPJ2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
         lV24Contratante_RazaoSocial2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratante_RazaoSocial2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
         lV24Contratante_RazaoSocial2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratante_RazaoSocial2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
         lV28Contratante_CNPJ3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratante_CNPJ3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
         lV28Contratante_CNPJ3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratante_CNPJ3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
         lV29Contratante_RazaoSocial3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratante_RazaoSocial3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
         lV29Contratante_RazaoSocial3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratante_RazaoSocial3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
         /* Using cursor H00P53 */
         pr_default.execute(1, new Object[] {AV7Municipio_Codigo, lV18Contratante_CNPJ1, lV18Contratante_CNPJ1, lV19Contratante_RazaoSocial1, lV19Contratante_RazaoSocial1, lV23Contratante_CNPJ2, lV23Contratante_CNPJ2, lV24Contratante_RazaoSocial2, lV24Contratante_RazaoSocial2, lV28Contratante_CNPJ3, lV28Contratante_CNPJ3, lV29Contratante_RazaoSocial3, lV29Contratante_RazaoSocial3});
         GRID_nRecordCount = H00P53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPP50( )
      {
         /* Before Start, stand alone formulas. */
         AV44Pgmname = "MunicipioContratanteWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22P52 */
         E22P52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Contratante_CNPJ1 = cgiGet( edtavContratante_cnpj1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
            AV19Contratante_RazaoSocial1 = StringUtil.Upper( cgiGet( edtavContratante_razaosocial1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23Contratante_CNPJ2 = cgiGet( edtavContratante_cnpj2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
            AV24Contratante_RazaoSocial2 = StringUtil.Upper( cgiGet( edtavContratante_razaosocial2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            AV28Contratante_CNPJ3 = cgiGet( edtavContratante_cnpj3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
            AV29Contratante_RazaoSocial3 = StringUtil.Upper( cgiGet( edtavContratante_razaosocial3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            n25Municipio_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_84"), ",", "."));
            AV37GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV38GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Municipio_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ1"), AV18Contratante_CNPJ1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL1"), AV19Contratante_RazaoSocial1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ2"), AV23Contratante_CNPJ2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL2"), AV24Contratante_RazaoSocial2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_CNPJ3"), AV28Contratante_CNPJ3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATANTE_RAZAOSOCIAL3"), AV29Contratante_RazaoSocial3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22P52 */
         E22P52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22P52( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATANTE_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTRATANTE_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "CONTRATANTE_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtMunicipio_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "CNPJ", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Jur�dica", 0);
         cmbavOrderedby.addItem("4", "Social", 0);
         cmbavOrderedby.addItem("5", "Fantasia", 0);
         cmbavOrderedby.addItem("6", "Estadual", 0);
         cmbavOrderedby.addItem("7", "Site", 0);
         cmbavOrderedby.addItem("8", "Email", 0);
         cmbavOrderedby.addItem("9", "Telefone", 0);
         cmbavOrderedby.addItem("10", "Ramal", 0);
         cmbavOrderedby.addItem("11", "Fax", 0);
         cmbavOrderedby.addItem("12", "Nome", 0);
         cmbavOrderedby.addItem("13", "Nro", 0);
         cmbavOrderedby.addItem("14", "Nome", 0);
         cmbavOrderedby.addItem("15", "Nro", 0);
         cmbavOrderedby.addItem("16", "Corrente", 0);
         cmbavOrderedby.addItem("17", "Ativo?", 0);
         cmbavOrderedby.addItem("18", "SMTP", 0);
         cmbavOrderedby.addItem("19", "Usu�rio", 0);
         cmbavOrderedby.addItem("20", "Senha", 0);
         cmbavOrderedby.addItem("21", "Key", 0);
         cmbavOrderedby.addItem("22", "Autentica��o", 0);
         cmbavOrderedby.addItem("23", "Porta", 0);
         cmbavOrderedby.addItem("24", "Seguran�a", 0);
         cmbavOrderedby.addItem("25", "Gera��o das OS", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E23P52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratante_Codigo_Titleformat = 2;
         edtContratante_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "C�digo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Codigo_Internalname, "Title", edtContratante_Codigo_Title);
         edtContratante_PessoaCod_Titleformat = 2;
         edtContratante_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Jur�dica", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_PessoaCod_Internalname, "Title", edtContratante_PessoaCod_Title);
         edtContratante_CNPJ_Titleformat = 2;
         edtContratante_CNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CNPJ", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_CNPJ_Internalname, "Title", edtContratante_CNPJ_Title);
         edtContratante_RazaoSocial_Titleformat = 2;
         edtContratante_RazaoSocial_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Social", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_RazaoSocial_Internalname, "Title", edtContratante_RazaoSocial_Title);
         edtContratante_NomeFantasia_Titleformat = 2;
         edtContratante_NomeFantasia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fantasia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_NomeFantasia_Internalname, "Title", edtContratante_NomeFantasia_Title);
         edtContratante_IE_Titleformat = 2;
         edtContratante_IE_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Estadual", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_IE_Internalname, "Title", edtContratante_IE_Title);
         edtContratante_WebSite_Titleformat = 2;
         edtContratante_WebSite_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Site", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_WebSite_Internalname, "Title", edtContratante_WebSite_Title);
         edtContratante_Email_Titleformat = 2;
         edtContratante_Email_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Email", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Email_Internalname, "Title", edtContratante_Email_Title);
         edtContratante_Telefone_Titleformat = 2;
         edtContratante_Telefone_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Telefone", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Telefone_Internalname, "Title", edtContratante_Telefone_Title);
         edtContratante_Ramal_Titleformat = 2;
         edtContratante_Ramal_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV14OrderedBy==10) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ramal", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Ramal_Internalname, "Title", edtContratante_Ramal_Title);
         edtContratante_Fax_Titleformat = 2;
         edtContratante_Fax_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV14OrderedBy==11) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fax", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_Fax_Internalname, "Title", edtContratante_Fax_Title);
         edtContratante_AgenciaNome_Titleformat = 2;
         edtContratante_AgenciaNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV14OrderedBy==12) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_AgenciaNome_Internalname, "Title", edtContratante_AgenciaNome_Title);
         edtContratante_AgenciaNro_Titleformat = 2;
         edtContratante_AgenciaNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV14OrderedBy==13) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nro", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_AgenciaNro_Internalname, "Title", edtContratante_AgenciaNro_Title);
         edtContratante_BancoNome_Titleformat = 2;
         edtContratante_BancoNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV14OrderedBy==14) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_BancoNome_Internalname, "Title", edtContratante_BancoNome_Title);
         edtContratante_BancoNro_Titleformat = 2;
         edtContratante_BancoNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV14OrderedBy==15) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nro", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_BancoNro_Internalname, "Title", edtContratante_BancoNro_Title);
         edtContratante_ContaCorrente_Titleformat = 2;
         edtContratante_ContaCorrente_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV14OrderedBy==16) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Corrente", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_ContaCorrente_Internalname, "Title", edtContratante_ContaCorrente_Title);
         chkContratante_Ativo_Titleformat = 2;
         chkContratante_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV14OrderedBy==17) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratante_Ativo_Internalname, "Title", chkContratante_Ativo.Title.Text);
         edtContratante_EmailSdaHost_Titleformat = 2;
         edtContratante_EmailSdaHost_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV14OrderedBy==18) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "SMTP", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_EmailSdaHost_Internalname, "Title", edtContratante_EmailSdaHost_Title);
         edtContratante_EmailSdaUser_Titleformat = 2;
         edtContratante_EmailSdaUser_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 19);' >%5</span>", ((AV14OrderedBy==19) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_EmailSdaUser_Internalname, "Title", edtContratante_EmailSdaUser_Title);
         edtContratante_EmailSdaPass_Titleformat = 2;
         edtContratante_EmailSdaPass_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 20);' >%5</span>", ((AV14OrderedBy==20) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Senha", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_EmailSdaPass_Internalname, "Title", edtContratante_EmailSdaPass_Title);
         edtContratante_EmailSdaKey_Titleformat = 2;
         edtContratante_EmailSdaKey_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 21);' >%5</span>", ((AV14OrderedBy==21) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Key", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_EmailSdaKey_Internalname, "Title", edtContratante_EmailSdaKey_Title);
         cmbContratante_EmailSdaAut_Titleformat = 2;
         cmbContratante_EmailSdaAut.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 22);' >%5</span>", ((AV14OrderedBy==22) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Autentica��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_EmailSdaAut_Internalname, "Title", cmbContratante_EmailSdaAut.Title.Text);
         edtContratante_EmailSdaPort_Titleformat = 2;
         edtContratante_EmailSdaPort_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 23);' >%5</span>", ((AV14OrderedBy==23) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Porta", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratante_EmailSdaPort_Internalname, "Title", edtContratante_EmailSdaPort_Title);
         cmbContratante_EmailSdaSec_Titleformat = 2;
         cmbContratante_EmailSdaSec.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 24);' >%5</span>", ((AV14OrderedBy==24) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Seguran�a", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_EmailSdaSec_Internalname, "Title", cmbContratante_EmailSdaSec.Title.Text);
         cmbContratante_OSAutomatica_Titleformat = 2;
         cmbContratante_OSAutomatica.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 25);' >%5</span>", ((AV14OrderedBy==25) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Gera��o das OS", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSAutomatica_Internalname, "Title", cmbContratante_OSAutomatica.Title.Text);
         AV37GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GridCurrentPage), 10, 0)));
         AV38GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11P52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV36PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV36PageToGo) ;
         }
      }

      private void E24P52( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A29Contratante_Codigo);
            AV32Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV32Update);
            AV41Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV32Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV32Update);
            AV41Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A29Contratante_Codigo);
            AV33Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV33Delete);
            AV42Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV33Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV33Delete);
            AV42Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV34Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV34Display);
            AV43Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV34Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV34Display);
            AV43Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContratante_CNPJ_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratante_RazaoSocial_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A335Contratante_PessoaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratante_Email_Link = "mailto:"+A14Contratante_Email;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 84;
         }
         sendrow_842( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_84_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(84, GridRow);
         }
      }

      protected void E12P52( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17P52( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
      }

      protected void E13P52( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18P52( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19P52( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
      }

      protected void E14P52( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20P52( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15P52( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratante_CNPJ1, AV19Contratante_RazaoSocial1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratante_CNPJ2, AV24Contratante_RazaoSocial2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratante_CNPJ3, AV29Contratante_RazaoSocial3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Municipio_Codigo, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A29Contratante_Codigo, A335Contratante_PessoaCod, A14Contratante_Email, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21P52( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16P52( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratante_cnpj1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj1_Visible), 5, 0)));
         edtavContratante_razaosocial1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
         {
            edtavContratante_cnpj1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            edtavContratante_razaosocial1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratante_cnpj2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj2_Visible), 5, 0)));
         edtavContratante_razaosocial2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 )
         {
            edtavContratante_cnpj2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            edtavContratante_razaosocial2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratante_cnpj3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj3_Visible), 5, 0)));
         edtavContratante_razaosocial3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 )
         {
            edtavContratante_cnpj3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_cnpj3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_cnpj3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 )
         {
            edtavContratante_razaosocial3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratante_razaosocial3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTRATANTE_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Contratante_CNPJ2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "CONTRATANTE_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28Contratante_CNPJ3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get(AV44Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV44Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV35Session.Get(AV44Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Contratante_CNPJ1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratante_CNPJ1", AV18Contratante_CNPJ1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19Contratante_RazaoSocial1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratante_RazaoSocial1", AV19Contratante_RazaoSocial1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Contratante_CNPJ2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratante_CNPJ2", AV23Contratante_CNPJ2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Contratante_RazaoSocial2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratante_RazaoSocial2", AV24Contratante_RazaoSocial2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28Contratante_CNPJ3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratante_CNPJ3", AV28Contratante_CNPJ3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29Contratante_RazaoSocial3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratante_RazaoSocial3", AV29Contratante_RazaoSocial3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV35Session.Get(AV44Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV44Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_CNPJ1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Contratante_CNPJ1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratante_RazaoSocial1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Contratante_RazaoSocial1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratante_CNPJ2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23Contratante_CNPJ2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratante_RazaoSocial2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Contratante_RazaoSocial2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratante_CNPJ3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28Contratante_CNPJ3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratante_RazaoSocial3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29Contratante_RazaoSocial3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV44Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Contratante";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Municipio_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV35Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_P52( true) ;
         }
         else
         {
            wb_table2_8_P52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_P52( true) ;
         }
         else
         {
            wb_table3_81_P52( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_P52e( true) ;
         }
         else
         {
            wb_table1_2_P52e( false) ;
         }
      }

      protected void wb_table3_81_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_CNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_CNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_CNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_RazaoSocial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_RazaoSocial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_RazaoSocial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_NomeFantasia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_NomeFantasia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_NomeFantasia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_WebSite_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_WebSite_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_WebSite_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Email_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Email_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Email_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Ramal_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Ramal_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Ramal_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Fax_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Fax_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Fax_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_AgenciaNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_AgenciaNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_AgenciaNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_AgenciaNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_AgenciaNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_AgenciaNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_BancoNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_BancoNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_BancoNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(76), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_BancoNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_BancoNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_BancoNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_ContaCorrente_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_ContaCorrente_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_ContaCorrente_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratante_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratante_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratante_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaHost_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaHost_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaHost_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaUser_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaUser_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaUser_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaPass_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaPass_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaPass_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaKey_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaKey_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaKey_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratante_EmailSdaAut_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratante_EmailSdaAut.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratante_EmailSdaAut.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_EmailSdaPort_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_EmailSdaPort_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_EmailSdaPort_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratante_EmailSdaSec_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratante_EmailSdaSec.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratante_EmailSdaSec.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratante_OSAutomatica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratante_OSAutomatica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratante_OSAutomatica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A12Contratante_CNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_CNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_CNPJ_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratante_CNPJ_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A9Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_RazaoSocial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_RazaoSocial_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratante_RazaoSocial_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A10Contratante_NomeFantasia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_NomeFantasia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_NomeFantasia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A11Contratante_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_IE_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A13Contratante_WebSite);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_WebSite_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_WebSite_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A14Contratante_Email);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Email_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Email_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratante_Email_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A31Contratante_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A32Contratante_Ramal));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Ramal_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Ramal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A33Contratante_Fax));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Fax_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Fax_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A336Contratante_AgenciaNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_AgenciaNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_AgenciaNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A337Contratante_AgenciaNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_AgenciaNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_AgenciaNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A338Contratante_BancoNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_BancoNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_BancoNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A339Contratante_BancoNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_BancoNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_BancoNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A16Contratante_ContaCorrente));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_ContaCorrente_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_ContaCorrente_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A30Contratante_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratante_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratante_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A547Contratante_EmailSdaHost);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaHost_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaHost_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A548Contratante_EmailSdaUser);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaUser_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaUser_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A549Contratante_EmailSdaPass);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaPass_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaPass_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A550Contratante_EmailSdaKey));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaKey_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaKey_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A551Contratante_EmailSdaAut));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratante_EmailSdaAut.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratante_EmailSdaAut_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_EmailSdaPort_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_EmailSdaPort_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1048Contratante_EmailSdaSec), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratante_EmailSdaSec.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratante_EmailSdaSec_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A593Contratante_OSAutomatica));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratante_OSAutomatica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratante_OSAutomatica_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_P52e( true) ;
         }
         else
         {
            wb_table3_81_P52e( false) ;
         }
      }

      protected void wb_table2_8_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_P52( true) ;
         }
         else
         {
            wb_table4_11_P52( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_P52( true) ;
         }
         else
         {
            wb_table5_21_P52( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_P52e( true) ;
         }
         else
         {
            wb_table2_8_P52e( false) ;
         }
      }

      protected void wb_table5_21_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_P52( true) ;
         }
         else
         {
            wb_table6_24_P52( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_P52e( true) ;
         }
         else
         {
            wb_table5_21_P52e( false) ;
         }
      }

      protected void wb_table6_24_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_P52( true) ;
         }
         else
         {
            wb_table7_33_P52( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_51_P52( true) ;
         }
         else
         {
            wb_table8_51_P52( false) ;
         }
         return  ;
      }

      protected void wb_table8_51_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_69_P52( true) ;
         }
         else
         {
            wb_table9_69_P52( false) ;
         }
         return  ;
      }

      protected void wb_table9_69_P52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_P52e( true) ;
         }
         else
         {
            wb_table6_24_P52e( false) ;
         }
      }

      protected void wb_table9_69_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_cnpj3_Internalname, AV28Contratante_CNPJ3, StringUtil.RTrim( context.localUtil.Format( AV28Contratante_CNPJ3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_cnpj3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_cnpj3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_razaosocial3_Internalname, StringUtil.RTrim( AV29Contratante_RazaoSocial3), StringUtil.RTrim( context.localUtil.Format( AV29Contratante_RazaoSocial3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_razaosocial3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_razaosocial3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_69_P52e( true) ;
         }
         else
         {
            wb_table9_69_P52e( false) ;
         }
      }

      protected void wb_table8_51_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_cnpj2_Internalname, AV23Contratante_CNPJ2, StringUtil.RTrim( context.localUtil.Format( AV23Contratante_CNPJ2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_cnpj2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_cnpj2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_razaosocial2_Internalname, StringUtil.RTrim( AV24Contratante_RazaoSocial2), StringUtil.RTrim( context.localUtil.Format( AV24Contratante_RazaoSocial2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_razaosocial2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_razaosocial2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_51_P52e( true) ;
         }
         else
         {
            wb_table8_51_P52e( false) ;
         }
      }

      protected void wb_table7_33_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_MunicipioContratanteWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_cnpj1_Internalname, AV18Contratante_CNPJ1, StringUtil.RTrim( context.localUtil.Format( AV18Contratante_CNPJ1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_cnpj1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_cnpj1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratante_razaosocial1_Internalname, StringUtil.RTrim( AV19Contratante_RazaoSocial1), StringUtil.RTrim( context.localUtil.Format( AV19Contratante_RazaoSocial1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_razaosocial1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratante_razaosocial1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_P52e( true) ;
         }
         else
         {
            wb_table7_33_P52e( false) ;
         }
      }

      protected void wb_table4_11_P52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratanteWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_P52e( true) ;
         }
         else
         {
            wb_table4_11_P52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Municipio_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAP52( ) ;
         WSP52( ) ;
         WEP52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Municipio_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAP52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "municipiocontratantewc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAP52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Municipio_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
         }
         wcpOAV7Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Municipio_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Municipio_Codigo != wcpOAV7Municipio_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Municipio_Codigo = AV7Municipio_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Municipio_Codigo = cgiGet( sPrefix+"AV7Municipio_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Municipio_Codigo) > 0 )
         {
            AV7Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Municipio_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Municipio_Codigo), 6, 0)));
         }
         else
         {
            AV7Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Municipio_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAP52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSP52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSP52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Municipio_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Municipio_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Municipio_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Municipio_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Municipio_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEP52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117143857");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("municipiocontratantewc.js", "?20203117143857");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_84_idx;
         edtContratante_Codigo_Internalname = sPrefix+"CONTRATANTE_CODIGO_"+sGXsfl_84_idx;
         edtContratante_PessoaCod_Internalname = sPrefix+"CONTRATANTE_PESSOACOD_"+sGXsfl_84_idx;
         edtContratante_CNPJ_Internalname = sPrefix+"CONTRATANTE_CNPJ_"+sGXsfl_84_idx;
         edtContratante_RazaoSocial_Internalname = sPrefix+"CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_84_idx;
         edtContratante_NomeFantasia_Internalname = sPrefix+"CONTRATANTE_NOMEFANTASIA_"+sGXsfl_84_idx;
         edtContratante_IE_Internalname = sPrefix+"CONTRATANTE_IE_"+sGXsfl_84_idx;
         edtContratante_WebSite_Internalname = sPrefix+"CONTRATANTE_WEBSITE_"+sGXsfl_84_idx;
         edtContratante_Email_Internalname = sPrefix+"CONTRATANTE_EMAIL_"+sGXsfl_84_idx;
         edtContratante_Telefone_Internalname = sPrefix+"CONTRATANTE_TELEFONE_"+sGXsfl_84_idx;
         edtContratante_Ramal_Internalname = sPrefix+"CONTRATANTE_RAMAL_"+sGXsfl_84_idx;
         edtContratante_Fax_Internalname = sPrefix+"CONTRATANTE_FAX_"+sGXsfl_84_idx;
         edtContratante_AgenciaNome_Internalname = sPrefix+"CONTRATANTE_AGENCIANOME_"+sGXsfl_84_idx;
         edtContratante_AgenciaNro_Internalname = sPrefix+"CONTRATANTE_AGENCIANRO_"+sGXsfl_84_idx;
         edtContratante_BancoNome_Internalname = sPrefix+"CONTRATANTE_BANCONOME_"+sGXsfl_84_idx;
         edtContratante_BancoNro_Internalname = sPrefix+"CONTRATANTE_BANCONRO_"+sGXsfl_84_idx;
         edtContratante_ContaCorrente_Internalname = sPrefix+"CONTRATANTE_CONTACORRENTE_"+sGXsfl_84_idx;
         chkContratante_Ativo_Internalname = sPrefix+"CONTRATANTE_ATIVO_"+sGXsfl_84_idx;
         edtContratante_EmailSdaHost_Internalname = sPrefix+"CONTRATANTE_EMAILSDAHOST_"+sGXsfl_84_idx;
         edtContratante_EmailSdaUser_Internalname = sPrefix+"CONTRATANTE_EMAILSDAUSER_"+sGXsfl_84_idx;
         edtContratante_EmailSdaPass_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPASS_"+sGXsfl_84_idx;
         edtContratante_EmailSdaKey_Internalname = sPrefix+"CONTRATANTE_EMAILSDAKEY_"+sGXsfl_84_idx;
         cmbContratante_EmailSdaAut_Internalname = sPrefix+"CONTRATANTE_EMAILSDAAUT_"+sGXsfl_84_idx;
         edtContratante_EmailSdaPort_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPORT_"+sGXsfl_84_idx;
         cmbContratante_EmailSdaSec_Internalname = sPrefix+"CONTRATANTE_EMAILSDASEC_"+sGXsfl_84_idx;
         cmbContratante_OSAutomatica_Internalname = sPrefix+"CONTRATANTE_OSAUTOMATICA_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_84_fel_idx;
         edtContratante_Codigo_Internalname = sPrefix+"CONTRATANTE_CODIGO_"+sGXsfl_84_fel_idx;
         edtContratante_PessoaCod_Internalname = sPrefix+"CONTRATANTE_PESSOACOD_"+sGXsfl_84_fel_idx;
         edtContratante_CNPJ_Internalname = sPrefix+"CONTRATANTE_CNPJ_"+sGXsfl_84_fel_idx;
         edtContratante_RazaoSocial_Internalname = sPrefix+"CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_84_fel_idx;
         edtContratante_NomeFantasia_Internalname = sPrefix+"CONTRATANTE_NOMEFANTASIA_"+sGXsfl_84_fel_idx;
         edtContratante_IE_Internalname = sPrefix+"CONTRATANTE_IE_"+sGXsfl_84_fel_idx;
         edtContratante_WebSite_Internalname = sPrefix+"CONTRATANTE_WEBSITE_"+sGXsfl_84_fel_idx;
         edtContratante_Email_Internalname = sPrefix+"CONTRATANTE_EMAIL_"+sGXsfl_84_fel_idx;
         edtContratante_Telefone_Internalname = sPrefix+"CONTRATANTE_TELEFONE_"+sGXsfl_84_fel_idx;
         edtContratante_Ramal_Internalname = sPrefix+"CONTRATANTE_RAMAL_"+sGXsfl_84_fel_idx;
         edtContratante_Fax_Internalname = sPrefix+"CONTRATANTE_FAX_"+sGXsfl_84_fel_idx;
         edtContratante_AgenciaNome_Internalname = sPrefix+"CONTRATANTE_AGENCIANOME_"+sGXsfl_84_fel_idx;
         edtContratante_AgenciaNro_Internalname = sPrefix+"CONTRATANTE_AGENCIANRO_"+sGXsfl_84_fel_idx;
         edtContratante_BancoNome_Internalname = sPrefix+"CONTRATANTE_BANCONOME_"+sGXsfl_84_fel_idx;
         edtContratante_BancoNro_Internalname = sPrefix+"CONTRATANTE_BANCONRO_"+sGXsfl_84_fel_idx;
         edtContratante_ContaCorrente_Internalname = sPrefix+"CONTRATANTE_CONTACORRENTE_"+sGXsfl_84_fel_idx;
         chkContratante_Ativo_Internalname = sPrefix+"CONTRATANTE_ATIVO_"+sGXsfl_84_fel_idx;
         edtContratante_EmailSdaHost_Internalname = sPrefix+"CONTRATANTE_EMAILSDAHOST_"+sGXsfl_84_fel_idx;
         edtContratante_EmailSdaUser_Internalname = sPrefix+"CONTRATANTE_EMAILSDAUSER_"+sGXsfl_84_fel_idx;
         edtContratante_EmailSdaPass_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPASS_"+sGXsfl_84_fel_idx;
         edtContratante_EmailSdaKey_Internalname = sPrefix+"CONTRATANTE_EMAILSDAKEY_"+sGXsfl_84_fel_idx;
         cmbContratante_EmailSdaAut_Internalname = sPrefix+"CONTRATANTE_EMAILSDAAUT_"+sGXsfl_84_fel_idx;
         edtContratante_EmailSdaPort_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPORT_"+sGXsfl_84_fel_idx;
         cmbContratante_EmailSdaSec_Internalname = sPrefix+"CONTRATANTE_EMAILSDASEC_"+sGXsfl_84_fel_idx;
         cmbContratante_OSAutomatica_Internalname = sPrefix+"CONTRATANTE_OSAUTOMATICA_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WBP50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV41Update_GXI : context.PathToRelativeUrl( AV32Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV42Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV42Delete_GXI : context.PathToRelativeUrl( AV33Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV43Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV43Display_GXI : context.PathToRelativeUrl( AV34Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_CNPJ_Internalname,(String)A12Contratante_CNPJ,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratante_CNPJ_Link,(String)"",(String)"",(String)"",(String)edtContratante_CNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_RazaoSocial_Internalname,StringUtil.RTrim( A9Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratante_RazaoSocial_Link,(String)"",(String)"",(String)"",(String)edtContratante_RazaoSocial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_NomeFantasia_Internalname,StringUtil.RTrim( A10Contratante_NomeFantasia),StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_NomeFantasia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_IE_Internalname,StringUtil.RTrim( A11Contratante_IE),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_IE_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_WebSite_Internalname,(String)A13Contratante_WebSite,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_WebSite_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Email_Internalname,(String)A14Contratante_Email,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratante_Email_Link,(String)"",(String)"",(String)"",(String)edtContratante_Email_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"email",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Email",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Telefone_Internalname,StringUtil.RTrim( A31Contratante_Telefone),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)gxphoneLink,(String)"",(String)"",(String)"",(String)edtContratante_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"tel",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Phone",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Ramal_Internalname,StringUtil.RTrim( A32Contratante_Ramal),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Ramal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Ramal",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A33Contratante_Fax);
            }
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Fax_Internalname,StringUtil.RTrim( A33Contratante_Fax),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)gxphoneLink,(String)"",(String)"",(String)"",(String)edtContratante_Fax_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"tel",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Phone",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_AgenciaNome_Internalname,StringUtil.RTrim( A336Contratante_AgenciaNome),StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_AgenciaNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_AgenciaNro_Internalname,StringUtil.RTrim( A337Contratante_AgenciaNro),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_AgenciaNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Agencia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_BancoNome_Internalname,StringUtil.RTrim( A338Contratante_BancoNome),StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_BancoNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_BancoNro_Internalname,StringUtil.RTrim( A339Contratante_BancoNro),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_BancoNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)76,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroBanco",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_ContaCorrente_Internalname,StringUtil.RTrim( A16Contratante_ContaCorrente),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_ContaCorrente_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"ContaCorrente",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratante_Ativo_Internalname,StringUtil.BoolToStr( A30Contratante_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaHost_Internalname,(String)A547Contratante_EmailSdaHost,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaHost_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaUser_Internalname,(String)A548Contratante_EmailSdaUser,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaUser_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaPass_Internalname,(String)A549Contratante_EmailSdaPass,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaPass_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)-1,(short)0,(short)84,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaKey_Internalname,StringUtil.RTrim( A550Contratante_EmailSdaKey),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaKey_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)32,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"KeyEnc",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_84_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATANTE_EMAILSDAAUT_" + sGXsfl_84_idx;
               cmbContratante_EmailSdaAut.Name = GXCCtl;
               cmbContratante_EmailSdaAut.WebTags = "";
               cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbContratante_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbContratante_EmailSdaAut.ItemCount > 0 )
               {
                  A551Contratante_EmailSdaAut = StringUtil.StrToBool( cmbContratante_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A551Contratante_EmailSdaAut)));
                  n551Contratante_EmailSdaAut = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratante_EmailSdaAut,(String)cmbContratante_EmailSdaAut_Internalname,StringUtil.BoolToStr( A551Contratante_EmailSdaAut),(short)1,(String)cmbContratante_EmailSdaAut_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratante_EmailSdaAut.CurrentValue = StringUtil.BoolToStr( A551Contratante_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_EmailSdaAut_Internalname, "Values", (String)(cmbContratante_EmailSdaAut.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_EmailSdaPort_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A552Contratante_EmailSdaPort), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_EmailSdaPort_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_84_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATANTE_EMAILSDASEC_" + sGXsfl_84_idx;
               cmbContratante_EmailSdaSec.Name = GXCCtl;
               cmbContratante_EmailSdaSec.WebTags = "";
               cmbContratante_EmailSdaSec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
               cmbContratante_EmailSdaSec.addItem("1", "SSL e TLS", 0);
               if ( cmbContratante_EmailSdaSec.ItemCount > 0 )
               {
                  A1048Contratante_EmailSdaSec = (short)(NumberUtil.Val( cmbContratante_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0))), "."));
                  n1048Contratante_EmailSdaSec = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratante_EmailSdaSec,(String)cmbContratante_EmailSdaSec_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0)),(short)1,(String)cmbContratante_EmailSdaSec_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratante_EmailSdaSec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1048Contratante_EmailSdaSec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_EmailSdaSec_Internalname, "Values", (String)(cmbContratante_EmailSdaSec.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_84_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATANTE_OSAUTOMATICA_" + sGXsfl_84_idx;
               cmbContratante_OSAutomatica.Name = GXCCtl;
               cmbContratante_OSAutomatica.WebTags = "";
               cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( true), "Autom�tica", 0);
               cmbContratante_OSAutomatica.addItem(StringUtil.BoolToStr( false), "Manual", 0);
               if ( cmbContratante_OSAutomatica.ItemCount > 0 )
               {
                  A593Contratante_OSAutomatica = StringUtil.StrToBool( cmbContratante_OSAutomatica.getValidValue(StringUtil.BoolToStr( A593Contratante_OSAutomatica)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratante_OSAutomatica,(String)cmbContratante_OSAutomatica_Internalname,StringUtil.BoolToStr( A593Contratante_OSAutomatica),(short)1,(String)cmbContratante_OSAutomatica_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratante_OSAutomatica.CurrentValue = StringUtil.BoolToStr( A593Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratante_OSAutomatica_Internalname, "Values", (String)(cmbContratante_OSAutomatica.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CODIGO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_PESSOACOD"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_NOMEFANTASIA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_IE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A11Contratante_IE, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_WEBSITE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A13Contratante_WebSite, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAIL"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A14Contratante_Email, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_TELEFONE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A31Contratante_Telefone, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_RAMAL"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A32Contratante_Ramal, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_FAX"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A33Contratante_Fax, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANOME"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A336Contratante_AgenciaNome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_AGENCIANRO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A337Contratante_AgenciaNro, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONOME"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A338Contratante_BancoNome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_BANCONRO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A339Contratante_BancoNro, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_CONTACORRENTE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A16Contratante_ContaCorrente, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_ATIVO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A30Contratante_Ativo));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAHOST"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A547Contratante_EmailSdaHost, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAUSER"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A548Contratante_EmailSdaUser, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAPASS"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A549Contratante_EmailSdaPass, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAKEY"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A550Contratante_EmailSdaKey, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAAUT"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A551Contratante_EmailSdaAut));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDAPORT"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A552Contratante_EmailSdaPort), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_EMAILSDASEC"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A1048Contratante_EmailSdaSec), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATANTE_OSAUTOMATICA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A593Contratante_OSAutomatica));
            GridContainer.AddRow(GridRow);
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavContratante_cnpj1_Internalname = sPrefix+"vCONTRATANTE_CNPJ1";
         edtavContratante_razaosocial1_Internalname = sPrefix+"vCONTRATANTE_RAZAOSOCIAL1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavContratante_cnpj2_Internalname = sPrefix+"vCONTRATANTE_CNPJ2";
         edtavContratante_razaosocial2_Internalname = sPrefix+"vCONTRATANTE_RAZAOSOCIAL2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavContratante_cnpj3_Internalname = sPrefix+"vCONTRATANTE_CNPJ3";
         edtavContratante_razaosocial3_Internalname = sPrefix+"vCONTRATANTE_RAZAOSOCIAL3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContratante_Codigo_Internalname = sPrefix+"CONTRATANTE_CODIGO";
         edtContratante_PessoaCod_Internalname = sPrefix+"CONTRATANTE_PESSOACOD";
         edtContratante_CNPJ_Internalname = sPrefix+"CONTRATANTE_CNPJ";
         edtContratante_RazaoSocial_Internalname = sPrefix+"CONTRATANTE_RAZAOSOCIAL";
         edtContratante_NomeFantasia_Internalname = sPrefix+"CONTRATANTE_NOMEFANTASIA";
         edtContratante_IE_Internalname = sPrefix+"CONTRATANTE_IE";
         edtContratante_WebSite_Internalname = sPrefix+"CONTRATANTE_WEBSITE";
         edtContratante_Email_Internalname = sPrefix+"CONTRATANTE_EMAIL";
         edtContratante_Telefone_Internalname = sPrefix+"CONTRATANTE_TELEFONE";
         edtContratante_Ramal_Internalname = sPrefix+"CONTRATANTE_RAMAL";
         edtContratante_Fax_Internalname = sPrefix+"CONTRATANTE_FAX";
         edtContratante_AgenciaNome_Internalname = sPrefix+"CONTRATANTE_AGENCIANOME";
         edtContratante_AgenciaNro_Internalname = sPrefix+"CONTRATANTE_AGENCIANRO";
         edtContratante_BancoNome_Internalname = sPrefix+"CONTRATANTE_BANCONOME";
         edtContratante_BancoNro_Internalname = sPrefix+"CONTRATANTE_BANCONRO";
         edtContratante_ContaCorrente_Internalname = sPrefix+"CONTRATANTE_CONTACORRENTE";
         chkContratante_Ativo_Internalname = sPrefix+"CONTRATANTE_ATIVO";
         edtContratante_EmailSdaHost_Internalname = sPrefix+"CONTRATANTE_EMAILSDAHOST";
         edtContratante_EmailSdaUser_Internalname = sPrefix+"CONTRATANTE_EMAILSDAUSER";
         edtContratante_EmailSdaPass_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPASS";
         edtContratante_EmailSdaKey_Internalname = sPrefix+"CONTRATANTE_EMAILSDAKEY";
         cmbContratante_EmailSdaAut_Internalname = sPrefix+"CONTRATANTE_EMAILSDAAUT";
         edtContratante_EmailSdaPort_Internalname = sPrefix+"CONTRATANTE_EMAILSDAPORT";
         cmbContratante_EmailSdaSec_Internalname = sPrefix+"CONTRATANTE_EMAILSDASEC";
         cmbContratante_OSAutomatica_Internalname = sPrefix+"CONTRATANTE_OSAUTOMATICA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtMunicipio_Codigo_Internalname = sPrefix+"MUNICIPIO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContratante_OSAutomatica_Jsonclick = "";
         cmbContratante_EmailSdaSec_Jsonclick = "";
         edtContratante_EmailSdaPort_Jsonclick = "";
         cmbContratante_EmailSdaAut_Jsonclick = "";
         edtContratante_EmailSdaKey_Jsonclick = "";
         edtContratante_EmailSdaPass_Jsonclick = "";
         edtContratante_EmailSdaUser_Jsonclick = "";
         edtContratante_EmailSdaHost_Jsonclick = "";
         edtContratante_ContaCorrente_Jsonclick = "";
         edtContratante_BancoNro_Jsonclick = "";
         edtContratante_BancoNome_Jsonclick = "";
         edtContratante_AgenciaNro_Jsonclick = "";
         edtContratante_AgenciaNome_Jsonclick = "";
         edtContratante_Fax_Jsonclick = "";
         edtContratante_Ramal_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_Email_Jsonclick = "";
         edtContratante_WebSite_Jsonclick = "";
         edtContratante_IE_Jsonclick = "";
         edtContratante_NomeFantasia_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_PessoaCod_Jsonclick = "";
         edtContratante_Codigo_Jsonclick = "";
         edtavContratante_razaosocial1_Jsonclick = "";
         edtavContratante_cnpj1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratante_razaosocial2_Jsonclick = "";
         edtavContratante_cnpj2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratante_razaosocial3_Jsonclick = "";
         edtavContratante_cnpj3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratante_Email_Link = "";
         edtContratante_RazaoSocial_Link = "";
         edtContratante_CNPJ_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         cmbContratante_OSAutomatica_Titleformat = 0;
         cmbContratante_EmailSdaSec_Titleformat = 0;
         edtContratante_EmailSdaPort_Titleformat = 0;
         cmbContratante_EmailSdaAut_Titleformat = 0;
         edtContratante_EmailSdaKey_Titleformat = 0;
         edtContratante_EmailSdaPass_Titleformat = 0;
         edtContratante_EmailSdaUser_Titleformat = 0;
         edtContratante_EmailSdaHost_Titleformat = 0;
         chkContratante_Ativo_Titleformat = 0;
         edtContratante_ContaCorrente_Titleformat = 0;
         edtContratante_BancoNro_Titleformat = 0;
         edtContratante_BancoNome_Titleformat = 0;
         edtContratante_AgenciaNro_Titleformat = 0;
         edtContratante_AgenciaNome_Titleformat = 0;
         edtContratante_Fax_Titleformat = 0;
         edtContratante_Ramal_Titleformat = 0;
         edtContratante_Telefone_Titleformat = 0;
         edtContratante_Email_Titleformat = 0;
         edtContratante_WebSite_Titleformat = 0;
         edtContratante_IE_Titleformat = 0;
         edtContratante_NomeFantasia_Titleformat = 0;
         edtContratante_RazaoSocial_Titleformat = 0;
         edtContratante_CNPJ_Titleformat = 0;
         edtContratante_PessoaCod_Titleformat = 0;
         edtContratante_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratante_razaosocial3_Visible = 1;
         edtavContratante_cnpj3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratante_razaosocial2_Visible = 1;
         edtavContratante_cnpj2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratante_razaosocial1_Visible = 1;
         edtavContratante_cnpj1_Visible = 1;
         cmbContratante_OSAutomatica.Title.Text = "Gera��o das OS";
         cmbContratante_EmailSdaSec.Title.Text = "Seguran�a";
         edtContratante_EmailSdaPort_Title = "Porta";
         cmbContratante_EmailSdaAut.Title.Text = "Autentica��o";
         edtContratante_EmailSdaKey_Title = "Key";
         edtContratante_EmailSdaPass_Title = "Senha";
         edtContratante_EmailSdaUser_Title = "Usu�rio";
         edtContratante_EmailSdaHost_Title = "SMTP";
         chkContratante_Ativo.Title.Text = "Ativo?";
         edtContratante_ContaCorrente_Title = "Corrente";
         edtContratante_BancoNro_Title = "Nro";
         edtContratante_BancoNome_Title = "Nome";
         edtContratante_AgenciaNro_Title = "Nro";
         edtContratante_AgenciaNome_Title = "Nome";
         edtContratante_Fax_Title = "Fax";
         edtContratante_Ramal_Title = "Ramal";
         edtContratante_Telefone_Title = "Telefone";
         edtContratante_Email_Title = "Email";
         edtContratante_WebSite_Title = "Site";
         edtContratante_IE_Title = "Estadual";
         edtContratante_NomeFantasia_Title = "Fantasia";
         edtContratante_RazaoSocial_Title = "Social";
         edtContratante_CNPJ_Title = "CNPJ";
         edtContratante_PessoaCod_Title = "Jur�dica";
         edtContratante_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkContratante_Ativo.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtMunicipio_Codigo_Jsonclick = "";
         edtMunicipio_Codigo_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContratante_Codigo_Titleformat',ctrl:'CONTRATANTE_CODIGO',prop:'Titleformat'},{av:'edtContratante_Codigo_Title',ctrl:'CONTRATANTE_CODIGO',prop:'Title'},{av:'edtContratante_PessoaCod_Titleformat',ctrl:'CONTRATANTE_PESSOACOD',prop:'Titleformat'},{av:'edtContratante_PessoaCod_Title',ctrl:'CONTRATANTE_PESSOACOD',prop:'Title'},{av:'edtContratante_CNPJ_Titleformat',ctrl:'CONTRATANTE_CNPJ',prop:'Titleformat'},{av:'edtContratante_CNPJ_Title',ctrl:'CONTRATANTE_CNPJ',prop:'Title'},{av:'edtContratante_RazaoSocial_Titleformat',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Titleformat'},{av:'edtContratante_RazaoSocial_Title',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Title'},{av:'edtContratante_NomeFantasia_Titleformat',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Titleformat'},{av:'edtContratante_NomeFantasia_Title',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Title'},{av:'edtContratante_IE_Titleformat',ctrl:'CONTRATANTE_IE',prop:'Titleformat'},{av:'edtContratante_IE_Title',ctrl:'CONTRATANTE_IE',prop:'Title'},{av:'edtContratante_WebSite_Titleformat',ctrl:'CONTRATANTE_WEBSITE',prop:'Titleformat'},{av:'edtContratante_WebSite_Title',ctrl:'CONTRATANTE_WEBSITE',prop:'Title'},{av:'edtContratante_Email_Titleformat',ctrl:'CONTRATANTE_EMAIL',prop:'Titleformat'},{av:'edtContratante_Email_Title',ctrl:'CONTRATANTE_EMAIL',prop:'Title'},{av:'edtContratante_Telefone_Titleformat',ctrl:'CONTRATANTE_TELEFONE',prop:'Titleformat'},{av:'edtContratante_Telefone_Title',ctrl:'CONTRATANTE_TELEFONE',prop:'Title'},{av:'edtContratante_Ramal_Titleformat',ctrl:'CONTRATANTE_RAMAL',prop:'Titleformat'},{av:'edtContratante_Ramal_Title',ctrl:'CONTRATANTE_RAMAL',prop:'Title'},{av:'edtContratante_Fax_Titleformat',ctrl:'CONTRATANTE_FAX',prop:'Titleformat'},{av:'edtContratante_Fax_Title',ctrl:'CONTRATANTE_FAX',prop:'Title'},{av:'edtContratante_AgenciaNome_Titleformat',ctrl:'CONTRATANTE_AGENCIANOME',prop:'Titleformat'},{av:'edtContratante_AgenciaNome_Title',ctrl:'CONTRATANTE_AGENCIANOME',prop:'Title'},{av:'edtContratante_AgenciaNro_Titleformat',ctrl:'CONTRATANTE_AGENCIANRO',prop:'Titleformat'},{av:'edtContratante_AgenciaNro_Title',ctrl:'CONTRATANTE_AGENCIANRO',prop:'Title'},{av:'edtContratante_BancoNome_Titleformat',ctrl:'CONTRATANTE_BANCONOME',prop:'Titleformat'},{av:'edtContratante_BancoNome_Title',ctrl:'CONTRATANTE_BANCONOME',prop:'Title'},{av:'edtContratante_BancoNro_Titleformat',ctrl:'CONTRATANTE_BANCONRO',prop:'Titleformat'},{av:'edtContratante_BancoNro_Title',ctrl:'CONTRATANTE_BANCONRO',prop:'Title'},{av:'edtContratante_ContaCorrente_Titleformat',ctrl:'CONTRATANTE_CONTACORRENTE',prop:'Titleformat'},{av:'edtContratante_ContaCorrente_Title',ctrl:'CONTRATANTE_CONTACORRENTE',prop:'Title'},{av:'chkContratante_Ativo_Titleformat',ctrl:'CONTRATANTE_ATIVO',prop:'Titleformat'},{av:'chkContratante_Ativo.Title.Text',ctrl:'CONTRATANTE_ATIVO',prop:'Title'},{av:'edtContratante_EmailSdaHost_Titleformat',ctrl:'CONTRATANTE_EMAILSDAHOST',prop:'Titleformat'},{av:'edtContratante_EmailSdaHost_Title',ctrl:'CONTRATANTE_EMAILSDAHOST',prop:'Title'},{av:'edtContratante_EmailSdaUser_Titleformat',ctrl:'CONTRATANTE_EMAILSDAUSER',prop:'Titleformat'},{av:'edtContratante_EmailSdaUser_Title',ctrl:'CONTRATANTE_EMAILSDAUSER',prop:'Title'},{av:'edtContratante_EmailSdaPass_Titleformat',ctrl:'CONTRATANTE_EMAILSDAPASS',prop:'Titleformat'},{av:'edtContratante_EmailSdaPass_Title',ctrl:'CONTRATANTE_EMAILSDAPASS',prop:'Title'},{av:'edtContratante_EmailSdaKey_Titleformat',ctrl:'CONTRATANTE_EMAILSDAKEY',prop:'Titleformat'},{av:'edtContratante_EmailSdaKey_Title',ctrl:'CONTRATANTE_EMAILSDAKEY',prop:'Title'},{av:'cmbContratante_EmailSdaAut'},{av:'edtContratante_EmailSdaPort_Titleformat',ctrl:'CONTRATANTE_EMAILSDAPORT',prop:'Titleformat'},{av:'edtContratante_EmailSdaPort_Title',ctrl:'CONTRATANTE_EMAILSDAPORT',prop:'Title'},{av:'cmbContratante_EmailSdaSec'},{av:'cmbContratante_OSAutomatica'},{av:'AV37GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24P52',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV33Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV34Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContratante_CNPJ_Link',ctrl:'CONTRATANTE_CNPJ',prop:'Link'},{av:'edtContratante_RazaoSocial_Link',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Link'},{av:'edtContratante_Email_Link',ctrl:'CONTRATANTE_EMAIL',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'edtavContratante_cnpj2_Visible',ctrl:'vCONTRATANTE_CNPJ2',prop:'Visible'},{av:'edtavContratante_razaosocial2_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratante_cnpj3_Visible',ctrl:'vCONTRATANTE_CNPJ3',prop:'Visible'},{av:'edtavContratante_razaosocial3_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18P52',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'edtavContratante_cnpj2_Visible',ctrl:'vCONTRATANTE_CNPJ2',prop:'Visible'},{av:'edtavContratante_razaosocial2_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratante_cnpj3_Visible',ctrl:'vCONTRATANTE_CNPJ3',prop:'Visible'},{av:'edtavContratante_razaosocial3_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20P52',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContratante_cnpj2_Visible',ctrl:'vCONTRATANTE_CNPJ2',prop:'Visible'},{av:'edtavContratante_razaosocial2_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15P52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A335Contratante_PessoaCod',fld:'CONTRATANTE_PESSOACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratante_CNPJ2',fld:'vCONTRATANTE_CNPJ2',pic:'',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratante_CNPJ3',fld:'vCONTRATANTE_CNPJ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratante_CNPJ1',fld:'vCONTRATANTE_CNPJ1',pic:'',nv:''},{av:'AV19Contratante_RazaoSocial1',fld:'vCONTRATANTE_RAZAOSOCIAL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratante_RazaoSocial2',fld:'vCONTRATANTE_RAZAOSOCIAL2',pic:'@!',nv:''},{av:'AV29Contratante_RazaoSocial3',fld:'vCONTRATANTE_RAZAOSOCIAL3',pic:'@!',nv:''},{av:'edtavContratante_cnpj2_Visible',ctrl:'vCONTRATANTE_CNPJ2',prop:'Visible'},{av:'edtavContratante_razaosocial2_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratante_cnpj3_Visible',ctrl:'vCONTRATANTE_CNPJ3',prop:'Visible'},{av:'edtavContratante_razaosocial3_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratante_cnpj1_Visible',ctrl:'vCONTRATANTE_CNPJ1',prop:'Visible'},{av:'edtavContratante_razaosocial1_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21P52',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContratante_cnpj3_Visible',ctrl:'vCONTRATANTE_CNPJ3',prop:'Visible'},{av:'edtavContratante_razaosocial3_Visible',ctrl:'vCONTRATANTE_RAZAOSOCIAL3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16P52',iparms:[{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Contratante_CNPJ1 = "";
         AV19Contratante_RazaoSocial1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Contratante_CNPJ2 = "";
         AV24Contratante_RazaoSocial2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28Contratante_CNPJ3 = "";
         AV29Contratante_RazaoSocial3 = "";
         AV44Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A14Contratante_Email = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Update = "";
         AV41Update_GXI = "";
         AV33Delete = "";
         AV42Delete_GXI = "";
         AV34Display = "";
         AV43Display_GXI = "";
         A12Contratante_CNPJ = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A11Contratante_IE = "";
         A13Contratante_WebSite = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A33Contratante_Fax = "";
         A336Contratante_AgenciaNome = "";
         A337Contratante_AgenciaNro = "";
         A338Contratante_BancoNome = "";
         A339Contratante_BancoNro = "";
         A16Contratante_ContaCorrente = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Contratante_CNPJ1 = "";
         lV19Contratante_RazaoSocial1 = "";
         lV23Contratante_CNPJ2 = "";
         lV24Contratante_RazaoSocial2 = "";
         lV28Contratante_CNPJ3 = "";
         lV29Contratante_RazaoSocial3 = "";
         H00P52_A25Municipio_Codigo = new int[1] ;
         H00P52_n25Municipio_Codigo = new bool[] {false} ;
         H00P52_A593Contratante_OSAutomatica = new bool[] {false} ;
         H00P52_A1048Contratante_EmailSdaSec = new short[1] ;
         H00P52_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         H00P52_A552Contratante_EmailSdaPort = new short[1] ;
         H00P52_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00P52_A551Contratante_EmailSdaAut = new bool[] {false} ;
         H00P52_n551Contratante_EmailSdaAut = new bool[] {false} ;
         H00P52_A550Contratante_EmailSdaKey = new String[] {""} ;
         H00P52_n550Contratante_EmailSdaKey = new bool[] {false} ;
         H00P52_A549Contratante_EmailSdaPass = new String[] {""} ;
         H00P52_n549Contratante_EmailSdaPass = new bool[] {false} ;
         H00P52_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00P52_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00P52_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00P52_n547Contratante_EmailSdaHost = new bool[] {false} ;
         H00P52_A30Contratante_Ativo = new bool[] {false} ;
         H00P52_A16Contratante_ContaCorrente = new String[] {""} ;
         H00P52_n16Contratante_ContaCorrente = new bool[] {false} ;
         H00P52_A339Contratante_BancoNro = new String[] {""} ;
         H00P52_n339Contratante_BancoNro = new bool[] {false} ;
         H00P52_A338Contratante_BancoNome = new String[] {""} ;
         H00P52_n338Contratante_BancoNome = new bool[] {false} ;
         H00P52_A337Contratante_AgenciaNro = new String[] {""} ;
         H00P52_n337Contratante_AgenciaNro = new bool[] {false} ;
         H00P52_A336Contratante_AgenciaNome = new String[] {""} ;
         H00P52_n336Contratante_AgenciaNome = new bool[] {false} ;
         H00P52_A33Contratante_Fax = new String[] {""} ;
         H00P52_n33Contratante_Fax = new bool[] {false} ;
         H00P52_A32Contratante_Ramal = new String[] {""} ;
         H00P52_n32Contratante_Ramal = new bool[] {false} ;
         H00P52_A31Contratante_Telefone = new String[] {""} ;
         H00P52_A14Contratante_Email = new String[] {""} ;
         H00P52_n14Contratante_Email = new bool[] {false} ;
         H00P52_A13Contratante_WebSite = new String[] {""} ;
         H00P52_n13Contratante_WebSite = new bool[] {false} ;
         H00P52_A11Contratante_IE = new String[] {""} ;
         H00P52_A10Contratante_NomeFantasia = new String[] {""} ;
         H00P52_A9Contratante_RazaoSocial = new String[] {""} ;
         H00P52_n9Contratante_RazaoSocial = new bool[] {false} ;
         H00P52_A12Contratante_CNPJ = new String[] {""} ;
         H00P52_n12Contratante_CNPJ = new bool[] {false} ;
         H00P52_A335Contratante_PessoaCod = new int[1] ;
         H00P52_A29Contratante_Codigo = new int[1] ;
         H00P53_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV35Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Municipio_Codigo = "";
         ROClassString = "";
         gxphoneLink = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.municipiocontratantewc__default(),
            new Object[][] {
                new Object[] {
               H00P52_A25Municipio_Codigo, H00P52_n25Municipio_Codigo, H00P52_A593Contratante_OSAutomatica, H00P52_A1048Contratante_EmailSdaSec, H00P52_n1048Contratante_EmailSdaSec, H00P52_A552Contratante_EmailSdaPort, H00P52_n552Contratante_EmailSdaPort, H00P52_A551Contratante_EmailSdaAut, H00P52_n551Contratante_EmailSdaAut, H00P52_A550Contratante_EmailSdaKey,
               H00P52_n550Contratante_EmailSdaKey, H00P52_A549Contratante_EmailSdaPass, H00P52_n549Contratante_EmailSdaPass, H00P52_A548Contratante_EmailSdaUser, H00P52_n548Contratante_EmailSdaUser, H00P52_A547Contratante_EmailSdaHost, H00P52_n547Contratante_EmailSdaHost, H00P52_A30Contratante_Ativo, H00P52_A16Contratante_ContaCorrente, H00P52_n16Contratante_ContaCorrente,
               H00P52_A339Contratante_BancoNro, H00P52_n339Contratante_BancoNro, H00P52_A338Contratante_BancoNome, H00P52_n338Contratante_BancoNome, H00P52_A337Contratante_AgenciaNro, H00P52_n337Contratante_AgenciaNro, H00P52_A336Contratante_AgenciaNome, H00P52_n336Contratante_AgenciaNome, H00P52_A33Contratante_Fax, H00P52_n33Contratante_Fax,
               H00P52_A32Contratante_Ramal, H00P52_n32Contratante_Ramal, H00P52_A31Contratante_Telefone, H00P52_A14Contratante_Email, H00P52_n14Contratante_Email, H00P52_A13Contratante_WebSite, H00P52_n13Contratante_WebSite, H00P52_A11Contratante_IE, H00P52_A10Contratante_NomeFantasia, H00P52_A9Contratante_RazaoSocial,
               H00P52_n9Contratante_RazaoSocial, H00P52_A12Contratante_CNPJ, H00P52_n12Contratante_CNPJ, H00P52_A335Contratante_PessoaCod, H00P52_A29Contratante_Codigo
               }
               , new Object[] {
               H00P53_AGRID_nRecordCount
               }
            }
         );
         AV44Pgmname = "MunicipioContratanteWC";
         /* GeneXus formulas. */
         AV44Pgmname = "MunicipioContratanteWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_84_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratante_Codigo_Titleformat ;
      private short edtContratante_PessoaCod_Titleformat ;
      private short edtContratante_CNPJ_Titleformat ;
      private short edtContratante_RazaoSocial_Titleformat ;
      private short edtContratante_NomeFantasia_Titleformat ;
      private short edtContratante_IE_Titleformat ;
      private short edtContratante_WebSite_Titleformat ;
      private short edtContratante_Email_Titleformat ;
      private short edtContratante_Telefone_Titleformat ;
      private short edtContratante_Ramal_Titleformat ;
      private short edtContratante_Fax_Titleformat ;
      private short edtContratante_AgenciaNome_Titleformat ;
      private short edtContratante_AgenciaNro_Titleformat ;
      private short edtContratante_BancoNome_Titleformat ;
      private short edtContratante_BancoNro_Titleformat ;
      private short edtContratante_ContaCorrente_Titleformat ;
      private short chkContratante_Ativo_Titleformat ;
      private short edtContratante_EmailSdaHost_Titleformat ;
      private short edtContratante_EmailSdaUser_Titleformat ;
      private short edtContratante_EmailSdaPass_Titleformat ;
      private short edtContratante_EmailSdaKey_Titleformat ;
      private short cmbContratante_EmailSdaAut_Titleformat ;
      private short edtContratante_EmailSdaPort_Titleformat ;
      private short cmbContratante_EmailSdaSec_Titleformat ;
      private short cmbContratante_OSAutomatica_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Municipio_Codigo ;
      private int wcpOAV7Municipio_Codigo ;
      private int subGrid_Rows ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A25Municipio_Codigo ;
      private int edtMunicipio_Codigo_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV36PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratante_cnpj1_Visible ;
      private int edtavContratante_razaosocial1_Visible ;
      private int edtavContratante_cnpj2_Visible ;
      private int edtavContratante_razaosocial2_Visible ;
      private int edtavContratante_cnpj3_Visible ;
      private int edtavContratante_razaosocial3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV37GridCurrentPage ;
      private long AV38GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_84_idx="0001" ;
      private String AV19Contratante_RazaoSocial1 ;
      private String AV24Contratante_RazaoSocial2 ;
      private String AV29Contratante_RazaoSocial3 ;
      private String AV44Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_PessoaCod_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String A10Contratante_NomeFantasia ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String A11Contratante_IE ;
      private String edtContratante_IE_Internalname ;
      private String edtContratante_WebSite_Internalname ;
      private String edtContratante_Email_Internalname ;
      private String A31Contratante_Telefone ;
      private String edtContratante_Telefone_Internalname ;
      private String A32Contratante_Ramal ;
      private String edtContratante_Ramal_Internalname ;
      private String A33Contratante_Fax ;
      private String edtContratante_Fax_Internalname ;
      private String A336Contratante_AgenciaNome ;
      private String edtContratante_AgenciaNome_Internalname ;
      private String A337Contratante_AgenciaNro ;
      private String edtContratante_AgenciaNro_Internalname ;
      private String A338Contratante_BancoNome ;
      private String edtContratante_BancoNome_Internalname ;
      private String A339Contratante_BancoNro ;
      private String edtContratante_BancoNro_Internalname ;
      private String A16Contratante_ContaCorrente ;
      private String edtContratante_ContaCorrente_Internalname ;
      private String chkContratante_Ativo_Internalname ;
      private String edtContratante_EmailSdaHost_Internalname ;
      private String edtContratante_EmailSdaUser_Internalname ;
      private String edtContratante_EmailSdaPass_Internalname ;
      private String A550Contratante_EmailSdaKey ;
      private String edtContratante_EmailSdaKey_Internalname ;
      private String cmbContratante_EmailSdaAut_Internalname ;
      private String edtContratante_EmailSdaPort_Internalname ;
      private String cmbContratante_EmailSdaSec_Internalname ;
      private String cmbContratante_OSAutomatica_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV19Contratante_RazaoSocial1 ;
      private String lV24Contratante_RazaoSocial2 ;
      private String lV29Contratante_RazaoSocial3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratante_cnpj1_Internalname ;
      private String edtavContratante_razaosocial1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratante_cnpj2_Internalname ;
      private String edtavContratante_razaosocial2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratante_cnpj3_Internalname ;
      private String edtavContratante_razaosocial3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtContratante_Codigo_Title ;
      private String edtContratante_PessoaCod_Title ;
      private String edtContratante_CNPJ_Title ;
      private String edtContratante_RazaoSocial_Title ;
      private String edtContratante_NomeFantasia_Title ;
      private String edtContratante_IE_Title ;
      private String edtContratante_WebSite_Title ;
      private String edtContratante_Email_Title ;
      private String edtContratante_Telefone_Title ;
      private String edtContratante_Ramal_Title ;
      private String edtContratante_Fax_Title ;
      private String edtContratante_AgenciaNome_Title ;
      private String edtContratante_AgenciaNro_Title ;
      private String edtContratante_BancoNome_Title ;
      private String edtContratante_BancoNro_Title ;
      private String edtContratante_ContaCorrente_Title ;
      private String edtContratante_EmailSdaHost_Title ;
      private String edtContratante_EmailSdaUser_Title ;
      private String edtContratante_EmailSdaPass_Title ;
      private String edtContratante_EmailSdaKey_Title ;
      private String edtContratante_EmailSdaPort_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContratante_CNPJ_Link ;
      private String edtContratante_RazaoSocial_Link ;
      private String edtContratante_Email_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratante_cnpj3_Jsonclick ;
      private String edtavContratante_razaosocial3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratante_cnpj2_Jsonclick ;
      private String edtavContratante_razaosocial2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratante_cnpj1_Jsonclick ;
      private String edtavContratante_razaosocial1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Municipio_Codigo ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtContratante_PessoaCod_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String edtContratante_IE_Jsonclick ;
      private String edtContratante_WebSite_Jsonclick ;
      private String edtContratante_Email_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String edtContratante_Ramal_Jsonclick ;
      private String edtContratante_Fax_Jsonclick ;
      private String edtContratante_AgenciaNome_Jsonclick ;
      private String edtContratante_AgenciaNro_Jsonclick ;
      private String edtContratante_BancoNome_Jsonclick ;
      private String edtContratante_BancoNro_Jsonclick ;
      private String edtContratante_ContaCorrente_Jsonclick ;
      private String edtContratante_EmailSdaHost_Jsonclick ;
      private String edtContratante_EmailSdaUser_Jsonclick ;
      private String edtContratante_EmailSdaPass_Jsonclick ;
      private String edtContratante_EmailSdaKey_Jsonclick ;
      private String cmbContratante_EmailSdaAut_Jsonclick ;
      private String edtContratante_EmailSdaPort_Jsonclick ;
      private String cmbContratante_EmailSdaSec_Jsonclick ;
      private String cmbContratante_OSAutomatica_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool n14Contratante_Email ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n13Contratante_WebSite ;
      private bool n32Contratante_Ramal ;
      private bool n33Contratante_Fax ;
      private bool n336Contratante_AgenciaNome ;
      private bool n337Contratante_AgenciaNro ;
      private bool n338Contratante_BancoNome ;
      private bool n339Contratante_BancoNro ;
      private bool n16Contratante_ContaCorrente ;
      private bool A30Contratante_Ativo ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool A593Contratante_OSAutomatica ;
      private bool n25Municipio_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Update_IsBlob ;
      private bool AV33Delete_IsBlob ;
      private bool AV34Display_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV18Contratante_CNPJ1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV23Contratante_CNPJ2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV28Contratante_CNPJ3 ;
      private String A14Contratante_Email ;
      private String AV41Update_GXI ;
      private String AV42Delete_GXI ;
      private String AV43Display_GXI ;
      private String A12Contratante_CNPJ ;
      private String A13Contratante_WebSite ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String A549Contratante_EmailSdaPass ;
      private String lV18Contratante_CNPJ1 ;
      private String lV23Contratante_CNPJ2 ;
      private String lV28Contratante_CNPJ3 ;
      private String AV32Update ;
      private String AV33Delete ;
      private String AV34Display ;
      private IGxSession AV35Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkContratante_Ativo ;
      private GXCombobox cmbContratante_EmailSdaAut ;
      private GXCombobox cmbContratante_EmailSdaSec ;
      private GXCombobox cmbContratante_OSAutomatica ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00P52_A25Municipio_Codigo ;
      private bool[] H00P52_n25Municipio_Codigo ;
      private bool[] H00P52_A593Contratante_OSAutomatica ;
      private short[] H00P52_A1048Contratante_EmailSdaSec ;
      private bool[] H00P52_n1048Contratante_EmailSdaSec ;
      private short[] H00P52_A552Contratante_EmailSdaPort ;
      private bool[] H00P52_n552Contratante_EmailSdaPort ;
      private bool[] H00P52_A551Contratante_EmailSdaAut ;
      private bool[] H00P52_n551Contratante_EmailSdaAut ;
      private String[] H00P52_A550Contratante_EmailSdaKey ;
      private bool[] H00P52_n550Contratante_EmailSdaKey ;
      private String[] H00P52_A549Contratante_EmailSdaPass ;
      private bool[] H00P52_n549Contratante_EmailSdaPass ;
      private String[] H00P52_A548Contratante_EmailSdaUser ;
      private bool[] H00P52_n548Contratante_EmailSdaUser ;
      private String[] H00P52_A547Contratante_EmailSdaHost ;
      private bool[] H00P52_n547Contratante_EmailSdaHost ;
      private bool[] H00P52_A30Contratante_Ativo ;
      private String[] H00P52_A16Contratante_ContaCorrente ;
      private bool[] H00P52_n16Contratante_ContaCorrente ;
      private String[] H00P52_A339Contratante_BancoNro ;
      private bool[] H00P52_n339Contratante_BancoNro ;
      private String[] H00P52_A338Contratante_BancoNome ;
      private bool[] H00P52_n338Contratante_BancoNome ;
      private String[] H00P52_A337Contratante_AgenciaNro ;
      private bool[] H00P52_n337Contratante_AgenciaNro ;
      private String[] H00P52_A336Contratante_AgenciaNome ;
      private bool[] H00P52_n336Contratante_AgenciaNome ;
      private String[] H00P52_A33Contratante_Fax ;
      private bool[] H00P52_n33Contratante_Fax ;
      private String[] H00P52_A32Contratante_Ramal ;
      private bool[] H00P52_n32Contratante_Ramal ;
      private String[] H00P52_A31Contratante_Telefone ;
      private String[] H00P52_A14Contratante_Email ;
      private bool[] H00P52_n14Contratante_Email ;
      private String[] H00P52_A13Contratante_WebSite ;
      private bool[] H00P52_n13Contratante_WebSite ;
      private String[] H00P52_A11Contratante_IE ;
      private String[] H00P52_A10Contratante_NomeFantasia ;
      private String[] H00P52_A9Contratante_RazaoSocial ;
      private bool[] H00P52_n9Contratante_RazaoSocial ;
      private String[] H00P52_A12Contratante_CNPJ ;
      private bool[] H00P52_n12Contratante_CNPJ ;
      private int[] H00P52_A335Contratante_PessoaCod ;
      private int[] H00P52_A29Contratante_Codigo ;
      private long[] H00P53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class municipiocontratantewc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00P52( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Contratante_CNPJ1 ,
                                             String AV19Contratante_RazaoSocial1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Contratante_CNPJ2 ,
                                             String AV24Contratante_RazaoSocial2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28Contratante_CNPJ3 ,
                                             String AV29Contratante_RazaoSocial3 ,
                                             String A12Contratante_CNPJ ,
                                             String A9Contratante_RazaoSocial ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A25Municipio_Codigo ,
                                             int AV7Municipio_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Municipio_Codigo], T1.[Contratante_OSAutomatica], T1.[Contratante_EmailSdaSec], T1.[Contratante_EmailSdaPort], T1.[Contratante_EmailSdaAut], T1.[Contratante_EmailSdaKey], T1.[Contratante_EmailSdaPass], T1.[Contratante_EmailSdaUser], T1.[Contratante_EmailSdaHost], T1.[Contratante_Ativo], T1.[Contratante_ContaCorrente], T1.[Contratante_BancoNro], T1.[Contratante_BancoNome], T1.[Contratante_AgenciaNro], T1.[Contratante_AgenciaNome], T1.[Contratante_Fax], T1.[Contratante_Ramal], T1.[Contratante_Telefone], T1.[Contratante_Email], T1.[Contratante_WebSite], T1.[Contratante_IE], T1.[Contratante_NomeFantasia], T2.[Pessoa_Nome] AS Contratante_RazaoSocial, T2.[Pessoa_Docto] AS Contratante_CNPJ, T1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contratante_Codigo]";
         sFromString = " FROM ([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Municipio_Codigo] = @AV7Municipio_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_CNPJ1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV18Contratante_CNPJ1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_CNPJ1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV18Contratante_CNPJ1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratante_RazaoSocial1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV19Contratante_RazaoSocial1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratante_RazaoSocial1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV19Contratante_RazaoSocial1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratante_CNPJ2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV23Contratante_CNPJ2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratante_CNPJ2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV23Contratante_CNPJ2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratante_RazaoSocial2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV24Contratante_RazaoSocial2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratante_RazaoSocial2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV24Contratante_RazaoSocial2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratante_CNPJ3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV28Contratante_CNPJ3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratante_CNPJ3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV28Contratante_CNPJ3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratante_RazaoSocial3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV29Contratante_RazaoSocial3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratante_RazaoSocial3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV29Contratante_RazaoSocial3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T2.[Pessoa_Docto]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T2.[Pessoa_Docto] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_PessoaCod]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_PessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T2.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T2.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_NomeFantasia]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_NomeFantasia] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_IE]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_IE] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_WebSite]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_WebSite] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Email]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Email] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Telefone]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Telefone] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Ramal]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Ramal] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Fax]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Fax] DESC";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_AgenciaNome]";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_AgenciaNome] DESC";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_AgenciaNro]";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_AgenciaNro] DESC";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_BancoNome]";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_BancoNome] DESC";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_BancoNro]";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_BancoNro] DESC";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_ContaCorrente]";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_ContaCorrente] DESC";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_Ativo]";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_Ativo] DESC";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaHost]";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaHost] DESC";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaUser]";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaUser] DESC";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaPass]";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaPass] DESC";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaKey]";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaKey] DESC";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaAut]";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaAut] DESC";
         }
         else if ( ( AV14OrderedBy == 23 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaPort]";
         }
         else if ( ( AV14OrderedBy == 23 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaPort] DESC";
         }
         else if ( ( AV14OrderedBy == 24 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_EmailSdaSec]";
         }
         else if ( ( AV14OrderedBy == 24 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_EmailSdaSec] DESC";
         }
         else if ( ( AV14OrderedBy == 25 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo], T1.[Contratante_OSAutomatica]";
         }
         else if ( ( AV14OrderedBy == 25 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Municipio_Codigo] DESC, T1.[Contratante_OSAutomatica] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratante_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00P53( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Contratante_CNPJ1 ,
                                             String AV19Contratante_RazaoSocial1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Contratante_CNPJ2 ,
                                             String AV24Contratante_RazaoSocial2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28Contratante_CNPJ3 ,
                                             String AV29Contratante_RazaoSocial3 ,
                                             String A12Contratante_CNPJ ,
                                             String A9Contratante_RazaoSocial ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A25Municipio_Codigo ,
                                             int AV7Municipio_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Contratante] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Municipio_Codigo] = @AV7Municipio_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_CNPJ1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV18Contratante_CNPJ1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_CNPJ") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratante_CNPJ1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV18Contratante_CNPJ1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratante_RazaoSocial1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV19Contratante_RazaoSocial1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratante_RazaoSocial1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV19Contratante_RazaoSocial1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratante_CNPJ2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV23Contratante_CNPJ2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_CNPJ") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratante_CNPJ2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV23Contratante_CNPJ2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratante_RazaoSocial2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV24Contratante_RazaoSocial2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratante_RazaoSocial2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV24Contratante_RazaoSocial2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratante_CNPJ3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV28Contratante_CNPJ3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_CNPJ") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratante_CNPJ3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV28Contratante_CNPJ3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratante_RazaoSocial3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV29Contratante_RazaoSocial3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATANTE_RAZAOSOCIAL") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratante_RazaoSocial3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV29Contratante_RazaoSocial3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 23 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 23 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 24 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 24 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 25 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 25 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00P52(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_H00P53(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00P52 ;
          prmH00P52 = new Object[] {
          new Object[] {"@AV7Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV18Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV19Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratante_CNPJ2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV23Contratante_CNPJ2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24Contratante_RazaoSocial2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24Contratante_RazaoSocial2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratante_CNPJ3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV28Contratante_CNPJ3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV29Contratante_RazaoSocial3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Contratante_RazaoSocial3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00P53 ;
          prmH00P53 = new Object[] {
          new Object[] {"@AV7Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV18Contratante_CNPJ1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV19Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contratante_RazaoSocial1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratante_CNPJ2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV23Contratante_CNPJ2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV24Contratante_RazaoSocial2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24Contratante_RazaoSocial2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratante_CNPJ3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV28Contratante_CNPJ3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV29Contratante_RazaoSocial3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Contratante_RazaoSocial3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00P52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P52,11,0,true,false )
             ,new CursorDef("H00P53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 32) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((bool[]) buf[17])[0] = rslt.getBool(10) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 6) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 20) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 20) ;
                ((String[]) buf[33])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((String[]) buf[37])[0] = rslt.getString(21, 15) ;
                ((String[]) buf[38])[0] = rslt.getString(22, 100) ;
                ((String[]) buf[39])[0] = rslt.getString(23, 100) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((int[]) buf[43])[0] = rslt.getInt(25) ;
                ((int[]) buf[44])[0] = rslt.getInt(26) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
