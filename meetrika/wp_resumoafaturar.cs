/*
               File: WP_ResumoAFaturar
        Description: WP_Resumo a Faturar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:48:20.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_resumoafaturar : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_resumoafaturar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_resumoafaturar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_25 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_25_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_25_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV8Context);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV17Contratadas);
               AV13Total = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
               AV12QtdeGeral = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12QtdeGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12QtdeGeral), 4, 0)));
               AV11Qtde = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
               AV14TotalGeral = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TotalGeral", StringUtil.LTrim( StringUtil.Str( AV14TotalGeral, 12, 2)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               AV9Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A53Contratada_AreaTrabalhoDes = GetNextPar( );
               n53Contratada_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53Contratada_AreaTrabalhoDes", A53Contratada_AreaTrabalhoDes);
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               AV7AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
               A1600ContagemResultado_CntSrvSttPgmFnc = GetNextPar( );
               n1600ContagemResultado_CntSrvSttPgmFnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1600ContagemResultado_CntSrvSttPgmFnc", A1600ContagemResultado_CntSrvSttPgmFnc);
               A606ContagemResultado_ValorFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKC2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKC2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120482071");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_resumoafaturar.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            GxWebStd.gx_hidden_field( context, "_GxRefreshTimeout", "{\"Type\":\"always\",\"Time\":\"300\"}");
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_25", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_25), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTEXT", AV8Context);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTEXT", AV8Context);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAS", AV17Contratadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAS", AV17Contratadas);
         }
         GxWebStd.gx_hidden_field( context, "vQTDEGERAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12QtdeGeral), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTOTALGERAL", StringUtil.LTrim( StringUtil.NToC( AV14TotalGeral, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHODES", A53Contratada_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVSTTPGMFNC", StringUtil.RTrim( A1600ContagemResultado_CntSrvSttPgmFnc));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORFINAL", StringUtil.LTrim( StringUtil.NToC( A606ContagemResultado_ValorFinal, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Draggeable", StringUtil.BoolToStr( Confirmpanel_Draggeable));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKC2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKC2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_resumoafaturar.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ResumoAFaturar" ;
      }

      public override String GetPgmdesc( )
      {
         return "WP_Resumo a Faturar" ;
      }

      protected void WBKC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KC2( true) ;
         }
         else
         {
            wb_table1_2_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTKC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WP_Resumo a Faturar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKC0( ) ;
      }

      protected void WSKC2( )
      {
         STARTKC2( ) ;
         EVTKC2( ) ;
      }

      protected void EVTKC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KC2 */
                              E11KC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_25_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
                              SubsflControlProps_252( ) ;
                              AV5AreaDeTrabalho = cgiGet( edtavAreadetrabalho_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV5AreaDeTrabalho);
                              AV11Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
                              AV13Total = context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12KC2 */
                                    E12KC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13KC2 */
                                    E13KC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14KC2 */
                                    E14KC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_252( ) ;
         while ( nGXsfl_25_idx <= nRC_GXsfl_25 )
         {
            sendrow_252( ) ;
            nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       wwpbaseobjects.SdtWWPContext AV8Context ,
                                       IGxCollection AV17Contratadas ,
                                       decimal AV13Total ,
                                       short AV12QtdeGeral ,
                                       short AV11Qtde ,
                                       decimal AV14TotalGeral ,
                                       int A39Contratada_Codigo ,
                                       int AV9Contratada_Codigo ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       String A53Contratada_AreaTrabalhoDes ,
                                       String A484ContagemResultado_StatusDmn ,
                                       int AV7AreaTrabalho_Codigo ,
                                       String A1600ContagemResultado_CntSrvSttPgmFnc ,
                                       decimal A606ContagemResultado_ValorFinal )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
      }

      protected void RFKC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 25;
         /* Execute user event: E14KC2 */
         E14KC2 ();
         nGXsfl_25_idx = 1;
         sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
         SubsflControlProps_252( ) ;
         nGXsfl_25_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_252( ) ;
            /* Execute user event: E13KC2 */
            E13KC2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_25_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E13KC2 */
               E13KC2 ();
            }
            wbEnd = 25;
            WBKC0( ) ;
         }
         nGXsfl_25_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A69ContratadaUsuario_UsuarioCod, A66ContratadaUsuario_ContratadaCod, AV8Context, AV17Contratadas, AV13Total, AV12QtdeGeral, AV11Qtde, AV14TotalGeral, A39Contratada_Codigo, AV9Contratada_Codigo, A52Contratada_AreaTrabalhoCod, A53Contratada_AreaTrabalhoDes, A484ContagemResultado_StatusDmn, AV7AreaTrabalho_Codigo, A1600ContagemResultado_CntSrvSttPgmFnc, A606ContagemResultado_ValorFinal) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKC0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreadetrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreadetrabalho_Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         edtavTotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotal_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12KC2 */
         E12KC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_25 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_25"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Confirmpanel_Draggeable = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Draggeable"));
            /* Read subfile selected row values. */
            nGXsfl_25_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
            if ( nGXsfl_25_idx > 0 )
            {
               AV5AreaDeTrabalho = cgiGet( edtavAreadetrabalho_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV5AreaDeTrabalho);
               AV11Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
               AV13Total = context.localUtil.CToN( cgiGet( edtavTotal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12KC2 */
         E12KC2 ();
         if (returnInSub) return;
      }

      protected void E12KC2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8Context) ;
         if ( ! AV8Context.gxTpr_Userehfinanceiro )
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         subgrid_gotopage( 1) ;
      }

      private void E13KC2( )
      {
         /* Grid_Load Routine */
         if ( AV8Context.gxTpr_Userehcontratada )
         {
            AV20GXV1 = 1;
            while ( AV20GXV1 <= AV17Contratadas.Count )
            {
               AV9Contratada_Codigo = (int)(AV17Contratadas.GetNumeric(AV20GXV1));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
               /* Execute user subroutine: 'AREADACONTRATADA' */
               S112 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'RESUMODAAREA' */
               S122 ();
               if (returnInSub) return;
               if ( ( AV13Total > Convert.ToDecimal( 0 )) )
               {
                  AV12QtdeGeral = (short)(AV12QtdeGeral+AV11Qtde);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12QtdeGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12QtdeGeral), 4, 0)));
                  AV14TotalGeral = (decimal)(AV14TotalGeral+AV13Total);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TotalGeral", StringUtil.LTrim( StringUtil.Str( AV14TotalGeral, 12, 2)));
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 25;
                  }
                  if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
                  {
                     sendrow_252( ) ;
                     GRID_nEOF = 1;
                     GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                     if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                     {
                        GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                     }
                  }
                  if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
                  {
                     GRID_nEOF = 0;
                     GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  }
                  GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
                  if ( isFullAjaxMode( ) && ( nGXsfl_25_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(25, GridRow);
                  }
               }
               AV20GXV1 = (int)(AV20GXV1+1);
            }
         }
         AV5AreaDeTrabalho = "� T O T A L �";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV5AreaDeTrabalho);
         AV11Qtde = AV12QtdeGeral;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
         AV13Total = AV14TotalGeral;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 25;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_252( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_25_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(25, GridRow);
         }
      }

      protected void E14KC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8Context) ;
         AV12QtdeGeral = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12QtdeGeral", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12QtdeGeral), 4, 0)));
         AV14TotalGeral = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TotalGeral", StringUtil.LTrim( StringUtil.Str( AV14TotalGeral, 12, 2)));
         AV17Contratadas.Clear();
         /* Using cursor H00KC2 */
         pr_default.execute(0, new Object[] {AV8Context.gxTpr_Userid});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = H00KC2_A69ContratadaUsuario_UsuarioCod[0];
            A66ContratadaUsuario_ContratadaCod = H00KC2_A66ContratadaUsuario_ContratadaCod[0];
            AV17Contratadas.Add(A66ContratadaUsuario_ContratadaCod, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8Context", AV8Context);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17Contratadas", AV17Contratadas);
      }

      protected void E11KC2( )
      {
         /* Confirmpanel_Close Routine */
         context.wjLoc = formatLink("wwpbaseobjects.home.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'AREADACONTRATADA' Routine */
         /* Using cursor H00KC3 */
         pr_default.execute(1, new Object[] {AV9Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A39Contratada_Codigo = H00KC3_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00KC3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00KC3_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = H00KC3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00KC3_n53Contratada_AreaTrabalhoDes[0];
            A53Contratada_AreaTrabalhoDes = H00KC3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = H00KC3_n53Contratada_AreaTrabalhoDes[0];
            AV7AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
            AV5AreaDeTrabalho = A53Contratada_AreaTrabalhoDes;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV5AreaDeTrabalho);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void S132( )
      {
         /* 'AREADACONTRATANTE' Routine */
         /* Using cursor H00KC4 */
         pr_default.execute(2, new Object[] {AV10Contratante_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A29Contratante_Codigo = H00KC4_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00KC4_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00KC4_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = H00KC4_A6AreaTrabalho_Descricao[0];
            AV7AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AreaTrabalho_Codigo), 6, 0)));
            AV5AreaDeTrabalho = A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreadetrabalho_Internalname, AV5AreaDeTrabalho);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S122( )
      {
         /* 'RESUMODAAREA' Routine */
         AV11Qtde = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
         AV13Total = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
         /* Using cursor H00KC5 */
         pr_default.execute(3, new Object[] {AV7AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00KC5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00KC5_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00KC5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00KC5_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = H00KC5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00KC5_n52Contratada_AreaTrabalhoCod[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = H00KC5_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = H00KC5_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A484ContagemResultado_StatusDmn = H00KC5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00KC5_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = H00KC5_A456ContagemResultado_Codigo[0];
            A512ContagemResultado_ValorPF = H00KC5_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00KC5_n512ContagemResultado_ValorPF[0];
            A52Contratada_AreaTrabalhoCod = H00KC5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00KC5_n52Contratada_AreaTrabalhoCod[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = H00KC5_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = H00KC5_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A606ContagemResultado_ValorFinal", StringUtil.LTrim( StringUtil.Str( A606ContagemResultado_ValorFinal, 18, 5)));
            AV11Qtde = (short)(AV11Qtde+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Qtde), 4, 0)));
            AV13Total = (decimal)(AV13Total+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTotal_Internalname, StringUtil.LTrim( StringUtil.Str( AV13Total, 12, 2)));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KC2( true) ;
         }
         else
         {
            wb_table2_8_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_22_KC2( true) ;
         }
         else
         {
            wb_table3_22_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_31_KC2( true) ;
         }
         else
         {
            wb_table4_31_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_31_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KC2e( true) ;
         }
         else
         {
            wb_table1_2_KC2e( false) ;
         }
      }

      protected void wb_table4_31_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_31_KC2e( true) ;
         }
         else
         {
            wb_table4_31_KC2e( false) ;
         }
      }

      protected void wb_table3_22_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"25\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Qtde") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Total R$") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV5AreaDeTrabalho));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreadetrabalho_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Qtde), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtde_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV13Total, 14, 2, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTotal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 25 )
         {
            wbEnd = 0;
            nRC_GXsfl_25 = (short)(nGXsfl_25_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_KC2e( true) ;
         }
         else
         {
            wb_table3_22_KC2e( false) ;
         }
      }

      protected void wb_table2_8_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Resumo a Facturar", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ResumoAFaturar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table5_13_KC2( true) ;
         }
         else
         {
            wb_table5_13_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_17_KC2( true) ;
         }
         else
         {
            wb_table6_17_KC2( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_KC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KC2e( true) ;
         }
         else
         {
            wb_table2_8_KC2e( false) ;
         }
      }

      protected void wb_table6_17_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_KC2e( true) ;
         }
         else
         {
            wb_table6_17_KC2e( false) ;
         }
      }

      protected void wb_table5_13_KC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_KC2e( true) ;
         }
         else
         {
            wb_table5_13_KC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKC2( ) ;
         WSKC2( ) ;
         WEKC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120482126");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_resumoafaturar.js", "?20203120482126");
            context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
            context.AddJavascriptSource("DVelop/Shared/dom.js", "");
            context.AddJavascriptSource("DVelop/Shared/event.js", "");
            context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
            context.AddJavascriptSource("DVelop/Shared/container.js", "");
            context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_252( )
      {
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO_"+sGXsfl_25_idx;
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_25_idx;
         edtavTotal_Internalname = "vTOTAL_"+sGXsfl_25_idx;
      }

      protected void SubsflControlProps_fel_252( )
      {
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO_"+sGXsfl_25_fel_idx;
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_25_fel_idx;
         edtavTotal_Internalname = "vTOTAL_"+sGXsfl_25_fel_idx;
      }

      protected void sendrow_252( )
      {
         SubsflControlProps_252( ) ;
         WBKC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_25_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_25_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_25_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreadetrabalho_Internalname,StringUtil.RTrim( AV5AreaDeTrabalho),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreadetrabalho_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAreadetrabalho_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtde_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Qtde), 4, 0, ",", "")),((edtavQtde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV11Qtde), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtde_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtde_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTotal_Internalname,StringUtil.LTrim( StringUtil.NToC( AV13Total, 14, 2, ",", "")),((edtavTotal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13Total, "ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV13Total, "ZZZ,ZZZ,ZZ9.99")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTotal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTotal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
         }
         /* End function sendrow_252 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavAreadetrabalho_Internalname = "vAREADETRABALHO";
         edtavQtde_Internalname = "vQTDE";
         edtavTotal_Internalname = "vTOTAL";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavTotal_Jsonclick = "";
         edtavQtde_Jsonclick = "";
         edtavAreadetrabalho_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowhovering = 0;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavTotal_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavAreadetrabalho_Enabled = 0;
         subGrid_Class = "WorkWith";
         subGrid_Backcolorstyle = 3;
         Confirmpanel_Draggeable = Convert.ToBoolean( 0);
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Buttoncanceltext = "Fechar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Fechar";
         Confirmpanel_Confirmtext = "Seu perfil de usu�rio n�o permite acesso ao Financeiro!";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Height = "50";
         Confirmpanel_Width = "400";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "WP_Resumo a Faturar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV13Total',fld:'vTOTAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV12QtdeGeral',fld:'vQTDEGERAL',pic:'ZZZ9',nv:0},{av:'AV11Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV14TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1600ContagemResultado_CntSrvSttPgmFnc',fld:'CONTAGEMRESULTADO_CNTSRVSTTPGMFNC',pic:'',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV12QtdeGeral',fld:'vQTDEGERAL',pic:'ZZZ9',nv:0},{av:'AV14TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E13KC2',iparms:[{av:'AV8Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV17Contratadas',fld:'vCONTRATADAS',pic:'',nv:null},{av:'AV13Total',fld:'vTOTAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV12QtdeGeral',fld:'vQTDEGERAL',pic:'ZZZ9',nv:0},{av:'AV11Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV14TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A53Contratada_AreaTrabalhoDes',fld:'CONTRATADA_AREATRABALHODES',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1600ContagemResultado_CntSrvSttPgmFnc',fld:'CONTAGEMRESULTADO_CNTSRVSTTPGMFNC',pic:'',nv:''},{av:'A606ContagemResultado_ValorFinal',fld:'CONTAGEMRESULTADO_VALORFINAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV9Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12QtdeGeral',fld:'vQTDEGERAL',pic:'ZZZ9',nv:0},{av:'AV14TotalGeral',fld:'vTOTALGERAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV5AreaDeTrabalho',fld:'vAREADETRABALHO',pic:'',nv:''},{av:'AV11Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV13Total',fld:'vTOTAL',pic:'ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11KC2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8Context = new wwpbaseobjects.SdtWWPContext(context);
         AV17Contratadas = new GxSimpleCollection();
         A53Contratada_AreaTrabalhoDes = "";
         A484ContagemResultado_StatusDmn = "";
         A1600ContagemResultado_CntSrvSttPgmFnc = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV5AreaDeTrabalho = "";
         GridContainer = new GXWebGrid( context);
         GridRow = new GXWebRow();
         scmdbuf = "";
         H00KC2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00KC2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00KC3_A39Contratada_Codigo = new int[1] ;
         H00KC3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KC3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KC3_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00KC3_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00KC4_A29Contratante_Codigo = new int[1] ;
         H00KC4_n29Contratante_Codigo = new bool[] {false} ;
         H00KC4_A5AreaTrabalho_Codigo = new int[1] ;
         H00KC4_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         H00KC5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00KC5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00KC5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00KC5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00KC5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KC5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00KC5_A1600ContagemResultado_CntSrvSttPgmFnc = new String[] {""} ;
         H00KC5_n1600ContagemResultado_CntSrvSttPgmFnc = new bool[] {false} ;
         H00KC5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00KC5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00KC5_A456ContagemResultado_Codigo = new int[1] ;
         H00KC5_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00KC5_n512ContagemResultado_ValorPF = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblocktitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_resumoafaturar__default(),
            new Object[][] {
                new Object[] {
               H00KC2_A69ContratadaUsuario_UsuarioCod, H00KC2_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00KC3_A39Contratada_Codigo, H00KC3_A52Contratada_AreaTrabalhoCod, H00KC3_A53Contratada_AreaTrabalhoDes, H00KC3_n53Contratada_AreaTrabalhoDes
               }
               , new Object[] {
               H00KC4_A29Contratante_Codigo, H00KC4_n29Contratante_Codigo, H00KC4_A5AreaTrabalho_Codigo, H00KC4_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00KC5_A490ContagemResultado_ContratadaCod, H00KC5_n490ContagemResultado_ContratadaCod, H00KC5_A1553ContagemResultado_CntSrvCod, H00KC5_n1553ContagemResultado_CntSrvCod, H00KC5_A52Contratada_AreaTrabalhoCod, H00KC5_n52Contratada_AreaTrabalhoCod, H00KC5_A1600ContagemResultado_CntSrvSttPgmFnc, H00KC5_n1600ContagemResultado_CntSrvSttPgmFnc, H00KC5_A484ContagemResultado_StatusDmn, H00KC5_n484ContagemResultado_StatusDmn,
               H00KC5_A456ContagemResultado_Codigo, H00KC5_A512ContagemResultado_ValorPF, H00KC5_n512ContagemResultado_ValorPF
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAreadetrabalho_Enabled = 0;
         edtavQtde_Enabled = 0;
         edtavTotal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_25 ;
      private short nGXsfl_25_idx=1 ;
      private short AV12QtdeGeral ;
      private short AV11Qtde ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_25_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A39Contratada_Codigo ;
      private int AV9Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV7AreaTrabalho_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavAreadetrabalho_Enabled ;
      private int edtavQtde_Enabled ;
      private int edtavTotal_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int AV20GXV1 ;
      private int AV10Contratante_Codigo ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV13Total ;
      private decimal AV14TotalGeral ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_25_idx="0001" ;
      private String edtavTotal_Internalname ;
      private String edtavQtde_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1600ContagemResultado_CntSrvSttPgmFnc ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV5AreaDeTrabalho ;
      private String edtavAreadetrabalho_Internalname ;
      private String subGrid_Internalname ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUsertable_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_25_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavAreadetrabalho_Jsonclick ;
      private String edtavQtde_Jsonclick ;
      private String edtavTotal_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private bool entryPointCalled ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool toggleJsOutput ;
      private bool Confirmpanel_Draggeable ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n29Contratante_Codigo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n512ContagemResultado_ValorPF ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A6AreaTrabalho_Descricao ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00KC2_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00KC2_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00KC3_A39Contratada_Codigo ;
      private int[] H00KC3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KC3_n52Contratada_AreaTrabalhoCod ;
      private String[] H00KC3_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00KC3_n53Contratada_AreaTrabalhoDes ;
      private int[] H00KC4_A29Contratante_Codigo ;
      private bool[] H00KC4_n29Contratante_Codigo ;
      private int[] H00KC4_A5AreaTrabalho_Codigo ;
      private String[] H00KC4_A6AreaTrabalho_Descricao ;
      private int[] H00KC5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00KC5_n490ContagemResultado_ContratadaCod ;
      private int[] H00KC5_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00KC5_n1553ContagemResultado_CntSrvCod ;
      private int[] H00KC5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00KC5_n52Contratada_AreaTrabalhoCod ;
      private String[] H00KC5_A1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool[] H00KC5_n1600ContagemResultado_CntSrvSttPgmFnc ;
      private String[] H00KC5_A484ContagemResultado_StatusDmn ;
      private bool[] H00KC5_n484ContagemResultado_StatusDmn ;
      private int[] H00KC5_A456ContagemResultado_Codigo ;
      private decimal[] H00KC5_A512ContagemResultado_ValorPF ;
      private bool[] H00KC5_n512ContagemResultado_ValorPF ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17Contratadas ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8Context ;
   }

   public class wp_resumoafaturar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KC2 ;
          prmH00KC2 = new Object[] {
          new Object[] {"@AV8Context__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00KC3 ;
          prmH00KC3 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KC4 ;
          prmH00KC4 = new Object[] {
          new Object[] {"@AV10Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KC5 ;
          prmH00KC5 = new Object[] {
          new Object[] {"@AV7AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KC2", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV8Context__Userid ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KC2,100,0,false,false )
             ,new CursorDef("H00KC3", "SELECT TOP 1 T1.[Contratada_Codigo], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE T1.[Contratada_Codigo] = @AV9Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KC3,1,0,false,true )
             ,new CursorDef("H00KC4", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @AV10Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KC4,1,0,false,true )
             ,new CursorDef("H00KC5", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[ContratoServicos_StatusPagFnc] AS ContagemResultado_CntSrvSttPgmFnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T2.[Contratada_AreaTrabalhoCod] = @AV7AreaTrabalho_Codigo) AND (T1.[ContagemResultado_StatusDmn] = T3.[ContratoServicos_StatusPagFnc]) ORDER BY T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KC5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
