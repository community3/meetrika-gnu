/*
               File: FiltroStatusDemandaCopy1
        Description: FiltroStatusDemandaCopy1
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 8/21/2019 8:23:28.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainfiltrostatusdemandacopy1
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainfiltrostatusdemandacopy1 ()
      {
         domain["S"] = "Solicitadas";
         domain["A"] = "Em An�lise";
         domain["E"] = "Em Execu��o";
         domain["S"] = "Em Stand By";
         domain["D"] = "Diverg�ncias";
         domain["Q"] = "Demandas QA";
         domain["I"] = "Enviadas";
         domain["H"] = "� Homologar";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
