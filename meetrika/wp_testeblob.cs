/*
               File: WP_TesteBlob
        Description: WP_Teste Blob
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/31/2019 2:8:17.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_testeblob : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_testeblob( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_testeblob( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavTipodocumento_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODOCUMENTO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODOCUMENTO_CODIGOSL2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PASL2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               GXVvTIPODOCUMENTO_CODIGO_htmlSL2( ) ;
               WSSL2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WESL2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "WP_Teste Blob") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2019531281745");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_testeblob.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARQUIVOEVIDENCIA", AV15Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARQUIVOEVIDENCIA", AV15Sdt_ArquivoEvidencia);
         }
         GxWebStd.gx_hidden_field( context, "vBLOB", AV9Blob);
         GxWebStd.gx_hidden_field( context, "vARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Artefatos_Codigo), 6, 0, ",", "")));
         GXCCtlgxBlob = "vCONTAGEMRESULTADOEVIDENCIA_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV5ContagemResultadoEvidencia_Arquivo);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormSL2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_TesteBlob" ;
      }

      public override String GetPgmdesc( )
      {
         return "WP_Teste Blob" ;
      }

      protected void WBSL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            context.WriteHtmlText( "<p>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 3,'',false,'',0)\"";
            edtavContagemresultadoevidencia_arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_arquivo_Internalname, "Filetype", edtavContagemresultadoevidencia_arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5ContagemResultadoEvidencia_Arquivo)) )
            {
               gxblobfileaux.Source = AV5ContagemResultadoEvidencia_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavContagemresultadoevidencia_arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavContagemresultadoevidencia_arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV5ContagemResultadoEvidencia_Arquivo = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_arquivo_Internalname, "URL", context.PathToRelativeUrl( AV5ContagemResultadoEvidencia_Arquivo));
                  edtavContagemresultadoevidencia_arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_arquivo_Internalname, "Filetype", edtavContagemresultadoevidencia_arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_arquivo_Internalname, "URL", context.PathToRelativeUrl( AV5ContagemResultadoEvidencia_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtavContagemresultadoevidencia_arquivo_Internalname, StringUtil.RTrim( AV5ContagemResultadoEvidencia_Arquivo), context.PathToRelativeUrl( AV5ContagemResultadoEvidencia_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtavContagemresultadoevidencia_arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavContagemresultadoevidencia_arquivo_Filetype)) ? AV5ContagemResultadoEvidencia_Arquivo : edtavContagemresultadoevidencia_arquivo_Filetype)) : edtavContagemresultadoevidencia_arquivo_Contenttype), false, "", edtavContagemresultadoevidencia_arquivo_Parameters, 0, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavContagemresultadoevidencia_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,3);\"", "", "", "HLP_WP_TesteBlob.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 6,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodocumento_codigo, dynavTipodocumento_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0)), 1, dynavTipodocumento_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,6);\"", "", true, "HLP_WP_TesteBlob.htm");
            dynavTipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodocumento_codigo_Internalname, "Values", (String)(dynavTipodocumento_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_TesteBlob.htm");
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void STARTSL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WP_Teste Blob", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSL0( ) ;
      }

      protected void WSSL2( )
      {
         STARTSL2( ) ;
         EVTSL2( ) ;
      }

      protected void EVTSL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E11SL2 */
                                 E11SL2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12SL2 */
                           E12SL2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WESL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormSL2( ) ;
            }
         }
      }

      protected void PASL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavTipodocumento_codigo.Name = "vTIPODOCUMENTO_CODIGO";
            dynavTipodocumento_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultadoevidencia_arquivo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGOSL2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODOCUMENTO_CODIGO_dataSL2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODOCUMENTO_CODIGO_htmlSL2( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPODOCUMENTO_CODIGO_dataSL2( ) ;
         gxdynajaxindex = 1;
         dynavTipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV17TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGO_dataSL2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00SL2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SL2_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00SL2_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV17TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSL2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12SL2 */
            E12SL2 ();
            WBSL0( ) ;
         }
      }

      protected void STRUPSL0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvTIPODOCUMENTO_CODIGO_htmlSL2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5ContagemResultadoEvidencia_Arquivo = cgiGet( edtavContagemresultadoevidencia_arquivo_Internalname);
            dynavTipodocumento_codigo.CurrentValue = cgiGet( dynavTipodocumento_codigo_Internalname);
            AV17TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipodocumento_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TipoDocumento_Codigo), 6, 0)));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5ContagemResultadoEvidencia_Arquivo)) )
            {
               GXCCtlgxBlob = "vCONTAGEMRESULTADOEVIDENCIA_ARQUIVO" + "_gxBlob";
               AV5ContagemResultadoEvidencia_Arquivo = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvTIPODOCUMENTO_CODIGO_htmlSL2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11SL2 */
         E11SL2 ();
         if (returnInSub) return;
      }

      protected void E11SL2( )
      {
         /* Enter Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV19WWPContext) ;
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = AV9Blob;
         AV15Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = AV17TipoDocumento_Codigo;
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = "&FileName";
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = "&Ext";
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao = "&Descricao";
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = "&Link";
         AV15Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner = AV19WWPContext.gxTpr_Userid;
         AV15Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo = AV8Artefatos_Codigo;
         GXt_char1 = AV15Sdt_ArquivoEvidencia.ToXml(false, true, "", "GxEv3Up14_Meetrika");
         new geralog(context ).execute( ref  GXt_char1) ;
         GX_msglist.addItem(AV15Sdt_ArquivoEvidencia.ToXml(false, true, "", "GxEv3Up14_Meetrika"));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Sdt_ArquivoEvidencia", AV15Sdt_ArquivoEvidencia);
      }

      protected void nextLoad( )
      {
      }

      protected void E12SL2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASL2( ) ;
         WSSL2( ) ;
         WESL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?7215850");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2019531281759");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_testeblob.js", "?2019531281759");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavContagemresultadoevidencia_arquivo_Internalname = "vCONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         dynavTipodocumento_codigo_Internalname = "vTIPODOCUMENTO_CODIGO";
         bttButton1_Internalname = "BUTTON1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavTipodocumento_codigo_Jsonclick = "";
         edtavContagemresultadoevidencia_arquivo_Jsonclick = "";
         edtavContagemresultadoevidencia_arquivo_Parameters = "";
         edtavContagemresultadoevidencia_arquivo_Contenttype = "";
         edtavContagemresultadoevidencia_arquivo_Filetype = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E11SL2',iparms:[{av:'AV15Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV9Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV17TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV15Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV9Blob = "";
         GXCCtlgxBlob = "";
         AV5ContagemResultadoEvidencia_Arquivo = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         bttButton1_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00SL2_A645TipoDocumento_Codigo = new int[1] ;
         H00SL2_A646TipoDocumento_Nome = new String[] {""} ;
         H00SL2_A647TipoDocumento_Ativo = new bool[] {false} ;
         AV19WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXt_char1 = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_testeblob__default(),
            new Object[][] {
                new Object[] {
               H00SL2_A645TipoDocumento_Codigo, H00SL2_A646TipoDocumento_Nome, H00SL2_A647TipoDocumento_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV8Artefatos_Codigo ;
      private int AV17TipoDocumento_Codigo ;
      private int gxdynajaxindex ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavContagemresultadoevidencia_arquivo_Filetype ;
      private String edtavContagemresultadoevidencia_arquivo_Internalname ;
      private String edtavContagemresultadoevidencia_arquivo_Contenttype ;
      private String edtavContagemresultadoevidencia_arquivo_Parameters ;
      private String edtavContagemresultadoevidencia_arquivo_Jsonclick ;
      private String dynavTipodocumento_codigo_Internalname ;
      private String dynavTipodocumento_codigo_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV9Blob ;
      private String AV5ContagemResultadoEvidencia_Arquivo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavTipodocumento_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00SL2_A645TipoDocumento_Codigo ;
      private String[] H00SL2_A646TipoDocumento_Nome ;
      private bool[] H00SL2_A647TipoDocumento_Ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV15Sdt_ArquivoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV19WWPContext ;
   }

   public class wp_testeblob__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SL2 ;
          prmH00SL2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SL2", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SL2,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
