/*
               File: FuncaoAPF
        Description: Fun��o de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:29.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"FUNCAOAPF_MODULOCOD") == 0 )
         {
            AV15FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15FuncaoAPF_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAFUNCAOAPF_MODULOCOD0Z36( AV15FuncaoAPF_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"FUNCAOAPF_FUNAPFPAICOD") == 0 )
         {
            AV15FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15FuncaoAPF_SistemaCod), 6, 0)));
            AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAFUNCAOAPF_FUNAPFPAICOD0Z36( AV15FuncaoAPF_SistemaCod, AV7FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"FUNCAOAPF_TD") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASAFUNCAOAPF_TD0Z36( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"FUNCAOAPF_AR") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASAFUNCAOAPF_AR0Z36( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"FUNCAOAPF_PF") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASAFUNCAOAPF_PF0Z36( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"FUNCAOAPF_COMPLEXIDADE") == 0 )
         {
            A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASAFUNCAOAPF_COMPLEXIDADE0Z36( A165FuncaoAPF_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel9"+"_"+"FUNCAOAPF_SOLUCAOTECNICA") == 0 )
         {
            A744FuncaoAPF_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n744FuncaoAPF_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
            A756FuncaoAPF_SistemaProjCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n756FuncaoAPF_SistemaProjCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX9ASAFUNCAOAPF_SOLUCAOTECNICA0Z36( A744FuncaoAPF_MelhoraCod, A756FuncaoAPF_SistemaProjCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel20"+"_"+"") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_35") == 0 )
         {
            A359FuncaoAPF_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n359FuncaoAPF_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_35( A359FuncaoAPF_ModuloCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_36") == 0 )
         {
            A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n358FuncaoAPF_FunAPFPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_36( A358FuncaoAPF_FunAPFPaiCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_34") == 0 )
         {
            A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n360FuncaoAPF_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_34( A360FuncaoAPF_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_37") == 0 )
         {
            A744FuncaoAPF_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n744FuncaoAPF_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_37( A744FuncaoAPF_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
               AV15FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15FuncaoAPF_SistemaCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynFuncaoAPF_ModuloCod.Name = "FUNCAOAPF_MODULOCOD";
         dynFuncaoAPF_ModuloCod.WebTags = "";
         cmbFuncaoAPF_Tipo.Name = "FUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo.WebTags = "";
         cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
         cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
         cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
         cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
         cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         chkFuncaoAPF_Acao.Name = "FUNCAOAPF_ACAO";
         chkFuncaoAPF_Acao.WebTags = "";
         chkFuncaoAPF_Acao.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoAPF_Acao_Internalname, "TitleCaption", chkFuncaoAPF_Acao.Caption);
         chkFuncaoAPF_Acao.CheckedValue = "false";
         chkFuncaoAPF_Mensagem.Name = "FUNCAOAPF_MENSAGEM";
         chkFuncaoAPF_Mensagem.WebTags = "";
         chkFuncaoAPF_Mensagem.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoAPF_Mensagem_Internalname, "TitleCaption", chkFuncaoAPF_Mensagem.Caption);
         chkFuncaoAPF_Mensagem.CheckedValue = "false";
         dynFuncaoAPF_FunAPFPaiCod.Name = "FUNCAOAPF_FUNAPFPAICOD";
         dynFuncaoAPF_FunAPFPaiCod.WebTags = "";
         cmbFuncaoAPF_Complexidade.Name = "FUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade.WebTags = "";
         cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
         cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
         cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
         cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         }
         cmbFuncaoAPF_Ativo.Name = "FUNCAOAPF_ATIVO";
         cmbFuncaoAPF_Ativo.WebTags = "";
         cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
         cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
         cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
         if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A183FuncaoAPF_Ativo)) )
            {
               A183FuncaoAPF_Ativo = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
            }
            A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         }
         cmbFuncaoAPF_UpdAoImpBsln.Name = "FUNCAOAPF_UPDAOIMPBSLN";
         cmbFuncaoAPF_UpdAoImpBsln.WebTags = "";
         cmbFuncaoAPF_UpdAoImpBsln.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbFuncaoAPF_UpdAoImpBsln.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbFuncaoAPF_UpdAoImpBsln.ItemCount > 0 )
         {
            if ( (false==A1268FuncaoAPF_UpdAoImpBsln) )
            {
               A1268FuncaoAPF_UpdAoImpBsln = true;
               n1268FuncaoAPF_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
            }
            A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoAPF_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln)));
            n1268FuncaoAPF_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Fun��o de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaoapf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaoapf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_FuncaoAPF_Codigo ,
                           ref int aP2_FuncaoAPF_SistemaCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7FuncaoAPF_Codigo = aP1_FuncaoAPF_Codigo;
         this.AV15FuncaoAPF_SistemaCod = aP2_FuncaoAPF_SistemaCod;
         executePrivate();
         aP2_FuncaoAPF_SistemaCod=this.AV15FuncaoAPF_SistemaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynFuncaoAPF_ModuloCod = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         chkFuncaoAPF_Acao = new GXCheckbox();
         chkFuncaoAPF_Mensagem = new GXCheckbox();
         dynFuncaoAPF_FunAPFPaiCod = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbFuncaoAPF_Ativo = new GXCombobox();
         cmbFuncaoAPF_UpdAoImpBsln = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynFuncaoAPF_ModuloCod.ItemCount > 0 )
         {
            A359FuncaoAPF_ModuloCod = (int)(NumberUtil.Val( dynFuncaoAPF_ModuloCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0))), "."));
            n359FuncaoAPF_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
         }
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         if ( dynFuncaoAPF_FunAPFPaiCod.ItemCount > 0 )
         {
            A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( dynFuncaoAPF_FunAPFPaiCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0))), "."));
            n358FuncaoAPF_FunAPFPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
         }
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         }
         if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
         {
            A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         }
         if ( cmbFuncaoAPF_UpdAoImpBsln.ItemCount > 0 )
         {
            A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoAPF_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln)));
            n1268FuncaoAPF_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")), ((edtFuncaoAPF_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPF_Codigo_Visible, edtFuncaoAPF_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoAPF.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_127_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table3_127_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0Z36e( true) ;
         }
         else
         {
            wb_table1_2_0Z36e( false) ;
         }
      }

      protected void wb_table3_127_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtneliminar_Internalname, "", "Eliminar", bttBtneliminar_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtneliminar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOELIMINAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_127_0Z36e( true) ;
         }
         else
         {
            wb_table3_127_0Z36e( false) ;
         }
      }

      protected void wb_table2_5_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0Z36e( true) ;
         }
         else
         {
            wb_table2_5_0Z36e( false) ;
         }
      }

      protected void wb_table4_13_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(90), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_modulocod_Internalname, "Modulo", "", "", lblTextblockfuncaoapf_modulocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_18_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table5_18_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Nome", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, 1, edtFuncaoAPF_Nome_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaoapf_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Descricao_Internalname, A167FuncaoAPF_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, 1, edtFuncaoAPF_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_link_Internalname, "Link", "", "", lblTextblockfuncaoapf_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            wb_table6_54_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table6_54_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_funapfpaicod_Internalname, "Vinculada com", "", "", lblTextblockfuncaoapf_funapfpaicod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynFuncaoAPF_FunAPFPaiCod, dynFuncaoAPF_FunAPFPaiCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)), 1, dynFuncaoAPF_FunAPFPaiCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynFuncaoAPF_FunAPFPaiCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_FuncaoAPF.htm");
            dynFuncaoAPF_FunAPFPaiCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_FunAPFPaiCod_Internalname, "Values", (String)(dynFuncaoAPF_FunAPFPaiCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_td_Internalname, "DER", "", "", lblTextblockfuncaoapf_td_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_75_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table7_75_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_observacao_Internalname, "Observa��o", "", "", lblTextblockfuncaoapf_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"FUNCAOAPF_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_parecerse_Internalname, "Parecer SE", "", "", lblTextblockfuncaoapf_parecerse_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_ParecerSE_Internalname, A1233FuncaoAPF_ParecerSE, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, 1, edtFuncaoAPF_ParecerSE_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_ativo_Internalname, "Status", "", "", lblTextblockfuncaoapf_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfuncaoapf_ativo_Visible, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_111_0Z36( true) ;
         }
         return  ;
      }

      protected void wb_table8_111_0Z36e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0Z36e( true) ;
         }
         else
         {
            wb_table4_13_0Z36e( false) ;
         }
      }

      protected void wb_table8_111_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapf_ativo_Internalname, tblTablemergedfuncaoapf_ativo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Ativo, cmbFuncaoAPF_Ativo_Internalname, StringUtil.RTrim( A183FuncaoAPF_Ativo), 1, cmbFuncaoAPF_Ativo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbFuncaoAPF_Ativo.Visible, cmbFuncaoAPF_Ativo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", "", true, "HLP_FuncaoAPF.htm");
            cmbFuncaoAPF_Ativo.CurrentValue = StringUtil.RTrim( A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Values", (String)(cmbFuncaoAPF_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_updaoimpbsln_Internalname, "Atualizar ao importar Baseline", "", "", lblTextblockfuncaoapf_updaoimpbsln_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_UpdAoImpBsln, cmbFuncaoAPF_UpdAoImpBsln_Internalname, StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln), 1, cmbFuncaoAPF_UpdAoImpBsln_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbFuncaoAPF_UpdAoImpBsln.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", "", true, "HLP_FuncaoAPF.htm");
            cmbFuncaoAPF_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_UpdAoImpBsln_Internalname, "Values", (String)(cmbFuncaoAPF_UpdAoImpBsln.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_111_0Z36e( true) ;
         }
         else
         {
            wb_table8_111_0Z36e( false) ;
         }
      }

      protected void wb_table7_75_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapf_td_Internalname, tblTablemergedfuncaoapf_td_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_TD_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")), ((edtFuncaoAPF_TD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")) : context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_TD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPF_TD_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_ar_Internalname, "ALR", "", "", lblTextblockfuncaoapf_ar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_AR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")), ((edtFuncaoAPF_AR_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")) : context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_AR_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPF_AR_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaoapf_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Complexidade, cmbFuncaoAPF_Complexidade_Internalname, StringUtil.RTrim( A185FuncaoAPF_Complexidade), 1, cmbFuncaoAPF_Complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoAPF_Complexidade.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_FuncaoAPF.htm");
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_pf_Internalname, "PF", "", "", lblTextblockfuncaoapf_pf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")), ((edtFuncaoAPF_PF_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_PF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoAPF_PF_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_75_0Z36e( true) ;
         }
         else
         {
            wb_table7_75_0Z36e( false) ;
         }
      }

      protected void wb_table6_54_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapf_link_Internalname, tblTablemergedfuncaoapf_link_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Link_Internalname, A432FuncaoAPF_Link, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, 1, edtFuncaoAPF_Link_Enabled, 0, 566, "px", 2, "row", StyleString, ClassString, "", "1000", 1, "", "", -1, true, "URLString", "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFuncaoapf_link_righttext_Internalname, "(se url: http[s]://...)", "", "", lblFuncaoapf_link_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_54_0Z36e( true) ;
         }
         else
         {
            wb_table6_54_0Z36e( false) ;
         }
      }

      protected void wb_table5_18_0Z36( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaoapf_modulocod_Internalname, tblTablemergedfuncaoapf_modulocod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynFuncaoAPF_ModuloCod, dynFuncaoAPF_ModuloCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)), 1, dynFuncaoAPF_ModuloCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynFuncaoAPF_ModuloCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_FuncaoAPF.htm");
            dynFuncaoAPF_ModuloCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_ModuloCod_Internalname, "Values", (String)(dynFuncaoAPF_ModuloCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_tipo_Internalname, "Tipo da Fun��o", "", "", lblTextblockfuncaoapf_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Tipo, cmbFuncaoAPF_Tipo_Internalname, StringUtil.RTrim( A184FuncaoAPF_Tipo), 1, cmbFuncaoAPF_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoAPF_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_FuncaoAPF.htm");
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_acao_Internalname, "A��o", "", "", lblTextblockfuncaoapf_acao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncaoAPF_Acao_Internalname, StringUtil.BoolToStr( A413FuncaoAPF_Acao), "", "", 1, chkFuncaoAPF_Acao.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(29, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_mensagem_Internalname, "Mensagem", "", "", lblTextblockfuncaoapf_mensagem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncaoAPF_Mensagem_Internalname, StringUtil.BoolToStr( A414FuncaoAPF_Mensagem), "", "", 1, chkFuncaoAPF_Mensagem.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(33, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_0Z36e( true) ;
         }
         else
         {
            wb_table5_18_0Z36e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110Z2 */
         E110Z2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynFuncaoAPF_ModuloCod.CurrentValue = cgiGet( dynFuncaoAPF_ModuloCod_Internalname);
               A359FuncaoAPF_ModuloCod = (int)(NumberUtil.Val( cgiGet( dynFuncaoAPF_ModuloCod_Internalname), "."));
               n359FuncaoAPF_ModuloCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
               n359FuncaoAPF_ModuloCod = ((0==A359FuncaoAPF_ModuloCod) ? true : false);
               cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
               A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A413FuncaoAPF_Acao = StringUtil.StrToBool( cgiGet( chkFuncaoAPF_Acao_Internalname));
               n413FuncaoAPF_Acao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A413FuncaoAPF_Acao", A413FuncaoAPF_Acao);
               n413FuncaoAPF_Acao = ((false==A413FuncaoAPF_Acao) ? true : false);
               A414FuncaoAPF_Mensagem = StringUtil.StrToBool( cgiGet( chkFuncaoAPF_Mensagem_Internalname));
               n414FuncaoAPF_Mensagem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A414FuncaoAPF_Mensagem", A414FuncaoAPF_Mensagem);
               n414FuncaoAPF_Mensagem = ((false==A414FuncaoAPF_Mensagem) ? true : false);
               A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A167FuncaoAPF_Descricao = cgiGet( edtFuncaoAPF_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
               A432FuncaoAPF_Link = cgiGet( edtFuncaoAPF_Link_Internalname);
               n432FuncaoAPF_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
               n432FuncaoAPF_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A432FuncaoAPF_Link)) ? true : false);
               dynFuncaoAPF_FunAPFPaiCod.CurrentValue = cgiGet( dynFuncaoAPF_FunAPFPaiCod_Internalname);
               A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( cgiGet( dynFuncaoAPF_FunAPFPaiCod_Internalname), "."));
               n358FuncaoAPF_FunAPFPaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
               n358FuncaoAPF_FunAPFPaiCod = ((0==A358FuncaoAPF_FunAPFPaiCod) ? true : false);
               A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
               A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               A1233FuncaoAPF_ParecerSE = cgiGet( edtFuncaoAPF_ParecerSE_Internalname);
               n1233FuncaoAPF_ParecerSE = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
               n1233FuncaoAPF_ParecerSE = (String.IsNullOrEmpty(StringUtil.RTrim( A1233FuncaoAPF_ParecerSE)) ? true : false);
               cmbFuncaoAPF_Ativo.CurrentValue = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
               A183FuncaoAPF_Ativo = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
               cmbFuncaoAPF_UpdAoImpBsln.CurrentValue = cgiGet( cmbFuncaoAPF_UpdAoImpBsln_Internalname);
               A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( cmbFuncaoAPF_UpdAoImpBsln_Internalname));
               n1268FuncaoAPF_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
               n1268FuncaoAPF_UpdAoImpBsln = ((false==A1268FuncaoAPF_UpdAoImpBsln) ? true : false);
               A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
               n165FuncaoAPF_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               /* Read saved values. */
               Z165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z165FuncaoAPF_Codigo"), ",", "."));
               Z166FuncaoAPF_Nome = cgiGet( "Z166FuncaoAPF_Nome");
               Z184FuncaoAPF_Tipo = cgiGet( "Z184FuncaoAPF_Tipo");
               Z1022FuncaoAPF_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1022FuncaoAPF_DERImp"), ",", "."));
               n1022FuncaoAPF_DERImp = ((0==A1022FuncaoAPF_DERImp) ? true : false);
               Z1026FuncaoAPF_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1026FuncaoAPF_RAImp"), ",", "."));
               n1026FuncaoAPF_RAImp = ((0==A1026FuncaoAPF_RAImp) ? true : false);
               Z413FuncaoAPF_Acao = StringUtil.StrToBool( cgiGet( "Z413FuncaoAPF_Acao"));
               n413FuncaoAPF_Acao = ((false==A413FuncaoAPF_Acao) ? true : false);
               Z414FuncaoAPF_Mensagem = StringUtil.StrToBool( cgiGet( "Z414FuncaoAPF_Mensagem"));
               n414FuncaoAPF_Mensagem = ((false==A414FuncaoAPF_Mensagem) ? true : false);
               Z432FuncaoAPF_Link = cgiGet( "Z432FuncaoAPF_Link");
               n432FuncaoAPF_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A432FuncaoAPF_Link)) ? true : false);
               Z1146FuncaoAPF_Tecnica = (short)(context.localUtil.CToN( cgiGet( "Z1146FuncaoAPF_Tecnica"), ",", "."));
               n1146FuncaoAPF_Tecnica = ((0==A1146FuncaoAPF_Tecnica) ? true : false);
               Z1246FuncaoAPF_Importada = context.localUtil.CToT( cgiGet( "Z1246FuncaoAPF_Importada"), 0);
               n1246FuncaoAPF_Importada = ((DateTime.MinValue==A1246FuncaoAPF_Importada) ? true : false);
               Z1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( "Z1268FuncaoAPF_UpdAoImpBsln"));
               n1268FuncaoAPF_UpdAoImpBsln = ((false==A1268FuncaoAPF_UpdAoImpBsln) ? true : false);
               Z183FuncaoAPF_Ativo = cgiGet( "Z183FuncaoAPF_Ativo");
               Z360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z360FuncaoAPF_SistemaCod"), ",", "."));
               n360FuncaoAPF_SistemaCod = ((0==A360FuncaoAPF_SistemaCod) ? true : false);
               Z359FuncaoAPF_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "Z359FuncaoAPF_ModuloCod"), ",", "."));
               n359FuncaoAPF_ModuloCod = ((0==A359FuncaoAPF_ModuloCod) ? true : false);
               Z358FuncaoAPF_FunAPFPaiCod = (int)(context.localUtil.CToN( cgiGet( "Z358FuncaoAPF_FunAPFPaiCod"), ",", "."));
               n358FuncaoAPF_FunAPFPaiCod = ((0==A358FuncaoAPF_FunAPFPaiCod) ? true : false);
               Z744FuncaoAPF_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z744FuncaoAPF_MelhoraCod"), ",", "."));
               n744FuncaoAPF_MelhoraCod = ((0==A744FuncaoAPF_MelhoraCod) ? true : false);
               A1022FuncaoAPF_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1022FuncaoAPF_DERImp"), ",", "."));
               n1022FuncaoAPF_DERImp = false;
               n1022FuncaoAPF_DERImp = ((0==A1022FuncaoAPF_DERImp) ? true : false);
               A1026FuncaoAPF_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1026FuncaoAPF_RAImp"), ",", "."));
               n1026FuncaoAPF_RAImp = false;
               n1026FuncaoAPF_RAImp = ((0==A1026FuncaoAPF_RAImp) ? true : false);
               A1146FuncaoAPF_Tecnica = (short)(context.localUtil.CToN( cgiGet( "Z1146FuncaoAPF_Tecnica"), ",", "."));
               n1146FuncaoAPF_Tecnica = false;
               n1146FuncaoAPF_Tecnica = ((0==A1146FuncaoAPF_Tecnica) ? true : false);
               A1246FuncaoAPF_Importada = context.localUtil.CToT( cgiGet( "Z1246FuncaoAPF_Importada"), 0);
               n1246FuncaoAPF_Importada = false;
               n1246FuncaoAPF_Importada = ((DateTime.MinValue==A1246FuncaoAPF_Importada) ? true : false);
               A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z360FuncaoAPF_SistemaCod"), ",", "."));
               n360FuncaoAPF_SistemaCod = false;
               n360FuncaoAPF_SistemaCod = ((0==A360FuncaoAPF_SistemaCod) ? true : false);
               A744FuncaoAPF_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z744FuncaoAPF_MelhoraCod"), ",", "."));
               n744FuncaoAPF_MelhoraCod = false;
               n744FuncaoAPF_MelhoraCod = ((0==A744FuncaoAPF_MelhoraCod) ? true : false);
               O166FuncaoAPF_Nome = cgiGet( "O166FuncaoAPF_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N360FuncaoAPF_SistemaCod"), ",", "."));
               n360FuncaoAPF_SistemaCod = ((0==A360FuncaoAPF_SistemaCod) ? true : false);
               N359FuncaoAPF_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "N359FuncaoAPF_ModuloCod"), ",", "."));
               n359FuncaoAPF_ModuloCod = ((0==A359FuncaoAPF_ModuloCod) ? true : false);
               N358FuncaoAPF_FunAPFPaiCod = (int)(context.localUtil.CToN( cgiGet( "N358FuncaoAPF_FunAPFPaiCod"), ",", "."));
               n358FuncaoAPF_FunAPFPaiCod = ((0==A358FuncaoAPF_FunAPFPaiCod) ? true : false);
               N744FuncaoAPF_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N744FuncaoAPF_MelhoraCod"), ",", "."));
               n744FuncaoAPF_MelhoraCod = ((0==A744FuncaoAPF_MelhoraCod) ? true : false);
               A744FuncaoAPF_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_MELHORACOD"), ",", "."));
               n744FuncaoAPF_MelhoraCod = ((0==A744FuncaoAPF_MelhoraCod) ? true : false);
               A756FuncaoAPF_SistemaProjCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_SISTEMAPROJCOD"), ",", "."));
               A757FuncaoAPF_SolucaoTecnica = cgiGet( "FUNCAOAPF_SOLUCAOTECNICA");
               AV7FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOAPF_CODIGO"), ",", "."));
               AV11Insert_FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_SISTEMACOD"), ",", "."));
               AV15FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOAPF_SISTEMACOD"), ",", "."));
               A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_SISTEMACOD"), ",", "."));
               n360FuncaoAPF_SistemaCod = ((0==A360FuncaoAPF_SistemaCod) ? true : false);
               AV12Insert_FuncaoAPF_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_MODULOCOD"), ",", "."));
               AV13Insert_FuncaoAPF_FunAPFPaiCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_FUNAPFPAICOD"), ",", "."));
               AV18Insert_FuncaoAPF_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_FUNCAOAPF_MELHORACOD"), ",", "."));
               A362FuncaoAPF_SistemaDes = cgiGet( "FUNCAOAPF_SISTEMADES");
               n362FuncaoAPF_SistemaDes = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1022FuncaoAPF_DERImp = (short)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_DERIMP"), ",", "."));
               n1022FuncaoAPF_DERImp = ((0==A1022FuncaoAPF_DERImp) ? true : false);
               A1026FuncaoAPF_RAImp = (short)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_RAIMP"), ",", "."));
               n1026FuncaoAPF_RAImp = ((0==A1026FuncaoAPF_RAImp) ? true : false);
               A1146FuncaoAPF_Tecnica = (short)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_TECNICA"), ",", "."));
               n1146FuncaoAPF_Tecnica = ((0==A1146FuncaoAPF_Tecnica) ? true : false);
               A1244FuncaoAPF_Observacao = cgiGet( "FUNCAOAPF_OBSERVACAO");
               n1244FuncaoAPF_Observacao = false;
               n1244FuncaoAPF_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1244FuncaoAPF_Observacao)) ? true : false);
               A1246FuncaoAPF_Importada = context.localUtil.CToT( cgiGet( "FUNCAOAPF_IMPORTADA"), 0);
               n1246FuncaoAPF_Importada = ((DateTime.MinValue==A1246FuncaoAPF_Importada) ? true : false);
               A706FuncaoAPF_SistemaTec = cgiGet( "FUNCAOAPF_SISTEMATEC");
               n706FuncaoAPF_SistemaTec = false;
               A361FuncaoAPF_ModuloNom = cgiGet( "FUNCAOAPF_MODULONOM");
               n361FuncaoAPF_ModuloNom = false;
               A363FuncaoAPF_FunAPFPaiNom = cgiGet( "FUNCAOAPF_FUNAPFPAINOM");
               n363FuncaoAPF_FunAPFPaiNom = false;
               AV19Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Funcaoapf_observacao_Width = cgiGet( "FUNCAOAPF_OBSERVACAO_Width");
               Funcaoapf_observacao_Height = cgiGet( "FUNCAOAPF_OBSERVACAO_Height");
               Funcaoapf_observacao_Skin = cgiGet( "FUNCAOAPF_OBSERVACAO_Skin");
               Funcaoapf_observacao_Toolbar = cgiGet( "FUNCAOAPF_OBSERVACAO_Toolbar");
               Funcaoapf_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_OBSERVACAO_Color"), ",", "."));
               Funcaoapf_observacao_Enabled = StringUtil.StrToBool( cgiGet( "FUNCAOAPF_OBSERVACAO_Enabled"));
               Funcaoapf_observacao_Class = cgiGet( "FUNCAOAPF_OBSERVACAO_Class");
               Funcaoapf_observacao_Customtoolbar = cgiGet( "FUNCAOAPF_OBSERVACAO_Customtoolbar");
               Funcaoapf_observacao_Customconfiguration = cgiGet( "FUNCAOAPF_OBSERVACAO_Customconfiguration");
               Funcaoapf_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "FUNCAOAPF_OBSERVACAO_Toolbarcancollapse"));
               Funcaoapf_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "FUNCAOAPF_OBSERVACAO_Toolbarexpanded"));
               Funcaoapf_observacao_Buttonpressedid = cgiGet( "FUNCAOAPF_OBSERVACAO_Buttonpressedid");
               Funcaoapf_observacao_Captionvalue = cgiGet( "FUNCAOAPF_OBSERVACAO_Captionvalue");
               Funcaoapf_observacao_Captionclass = cgiGet( "FUNCAOAPF_OBSERVACAO_Captionclass");
               Funcaoapf_observacao_Captionposition = cgiGet( "FUNCAOAPF_OBSERVACAO_Captionposition");
               Funcaoapf_observacao_Coltitle = cgiGet( "FUNCAOAPF_OBSERVACAO_Coltitle");
               Funcaoapf_observacao_Coltitlefont = cgiGet( "FUNCAOAPF_OBSERVACAO_Coltitlefont");
               Funcaoapf_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPF_OBSERVACAO_Coltitlecolor"), ",", "."));
               Funcaoapf_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "FUNCAOAPF_OBSERVACAO_Usercontroliscolumn"));
               Funcaoapf_observacao_Visible = StringUtil.StrToBool( cgiGet( "FUNCAOAPF_OBSERVACAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FuncaoAPF";
               A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
               n165FuncaoAPF_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A744FuncaoAPF_MelhoraCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1022FuncaoAPF_DERImp), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1026FuncaoAPF_RAImp), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1146FuncaoAPF_Tecnica), "9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1246FuncaoAPF_Importada, "99/99/99 99:99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_Codigo:"+context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_SistemaCod:"+context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_MelhoraCod:"+context.localUtil.Format( (decimal)(A744FuncaoAPF_MelhoraCod), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_DERImp:"+context.localUtil.Format( (decimal)(A1022FuncaoAPF_DERImp), "ZZZ9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_RAImp:"+context.localUtil.Format( (decimal)(A1026FuncaoAPF_RAImp), "ZZZ9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_Tecnica:"+context.localUtil.Format( (decimal)(A1146FuncaoAPF_Tecnica), "9"));
                  GXUtil.WriteLog("funcaoapf:[SecurityCheckFailed value for]"+"FuncaoAPF_Importada:"+context.localUtil.Format( A1246FuncaoAPF_Importada, "99/99/99 99:99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n165FuncaoAPF_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode36 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode36;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound36 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0Z0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAOAPF_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110Z2 */
                           E110Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120Z2 */
                           E120Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOELIMINAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130Z2 */
                           E130Z2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120Z2 */
            E120Z2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0Z36( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0Z36( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0Z0( )
      {
         BeforeValidate0Z36( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0Z36( ) ;
            }
            else
            {
               CheckExtendedTable0Z36( ) ;
               CloseExtendedTableCursors0Z36( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0Z0( )
      {
      }

      protected void E110Z2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_SistemaCod") == 0 )
               {
                  AV11Insert_FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_FuncaoAPF_SistemaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_ModuloCod") == 0 )
               {
                  AV12Insert_FuncaoAPF_ModuloCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_FuncaoAPF_ModuloCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_FunAPFPaiCod") == 0 )
               {
                  AV13Insert_FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_FuncaoAPF_FunAPFPaiCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "FuncaoAPF_MelhoraCod") == 0 )
               {
                  AV18Insert_FuncaoAPF_MelhoraCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_FuncaoAPF_MelhoraCod), 6, 0)));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            }
         }
         edtFuncaoAPF_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Visible), 5, 0)));
         edtFuncaoAPF_Nome_Width = 570;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Width), 9, 0)));
         edtFuncaoAPF_Nome_Height = 27;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Height", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Height), 9, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            cmbFuncaoAPF_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Ativo.Visible), 5, 0)));
            lblTextblockfuncaoapf_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncaoapf_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncaoapf_ativo_Visible), 5, 0)));
         }
      }

      protected void E120Z2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV15FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
            context.wjLocDisableFrm = 1;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfuncaoapf.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV15FuncaoAPF_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E130Z2( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "FnT",  AV7FuncaoAPF_Codigo,  0) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         context.setWebReturnParms(new Object[] {(int)AV15FuncaoAPF_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0Z36( short GX_JID )
      {
         if ( ( GX_JID == 33 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z166FuncaoAPF_Nome = T000Z3_A166FuncaoAPF_Nome[0];
               Z184FuncaoAPF_Tipo = T000Z3_A184FuncaoAPF_Tipo[0];
               Z1022FuncaoAPF_DERImp = T000Z3_A1022FuncaoAPF_DERImp[0];
               Z1026FuncaoAPF_RAImp = T000Z3_A1026FuncaoAPF_RAImp[0];
               Z413FuncaoAPF_Acao = T000Z3_A413FuncaoAPF_Acao[0];
               Z414FuncaoAPF_Mensagem = T000Z3_A414FuncaoAPF_Mensagem[0];
               Z432FuncaoAPF_Link = T000Z3_A432FuncaoAPF_Link[0];
               Z1146FuncaoAPF_Tecnica = T000Z3_A1146FuncaoAPF_Tecnica[0];
               Z1246FuncaoAPF_Importada = T000Z3_A1246FuncaoAPF_Importada[0];
               Z1268FuncaoAPF_UpdAoImpBsln = T000Z3_A1268FuncaoAPF_UpdAoImpBsln[0];
               Z183FuncaoAPF_Ativo = T000Z3_A183FuncaoAPF_Ativo[0];
               Z360FuncaoAPF_SistemaCod = T000Z3_A360FuncaoAPF_SistemaCod[0];
               Z359FuncaoAPF_ModuloCod = T000Z3_A359FuncaoAPF_ModuloCod[0];
               Z358FuncaoAPF_FunAPFPaiCod = T000Z3_A358FuncaoAPF_FunAPFPaiCod[0];
               Z744FuncaoAPF_MelhoraCod = T000Z3_A744FuncaoAPF_MelhoraCod[0];
            }
            else
            {
               Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
               Z184FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
               Z1022FuncaoAPF_DERImp = A1022FuncaoAPF_DERImp;
               Z1026FuncaoAPF_RAImp = A1026FuncaoAPF_RAImp;
               Z413FuncaoAPF_Acao = A413FuncaoAPF_Acao;
               Z414FuncaoAPF_Mensagem = A414FuncaoAPF_Mensagem;
               Z432FuncaoAPF_Link = A432FuncaoAPF_Link;
               Z1146FuncaoAPF_Tecnica = A1146FuncaoAPF_Tecnica;
               Z1246FuncaoAPF_Importada = A1246FuncaoAPF_Importada;
               Z1268FuncaoAPF_UpdAoImpBsln = A1268FuncaoAPF_UpdAoImpBsln;
               Z183FuncaoAPF_Ativo = A183FuncaoAPF_Ativo;
               Z360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
               Z359FuncaoAPF_ModuloCod = A359FuncaoAPF_ModuloCod;
               Z358FuncaoAPF_FunAPFPaiCod = A358FuncaoAPF_FunAPFPaiCod;
               Z744FuncaoAPF_MelhoraCod = A744FuncaoAPF_MelhoraCod;
            }
         }
         if ( GX_JID == -33 )
         {
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            Z166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            Z167FuncaoAPF_Descricao = A167FuncaoAPF_Descricao;
            Z184FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
            Z1022FuncaoAPF_DERImp = A1022FuncaoAPF_DERImp;
            Z1026FuncaoAPF_RAImp = A1026FuncaoAPF_RAImp;
            Z413FuncaoAPF_Acao = A413FuncaoAPF_Acao;
            Z414FuncaoAPF_Mensagem = A414FuncaoAPF_Mensagem;
            Z432FuncaoAPF_Link = A432FuncaoAPF_Link;
            Z1146FuncaoAPF_Tecnica = A1146FuncaoAPF_Tecnica;
            Z1233FuncaoAPF_ParecerSE = A1233FuncaoAPF_ParecerSE;
            Z1244FuncaoAPF_Observacao = A1244FuncaoAPF_Observacao;
            Z1246FuncaoAPF_Importada = A1246FuncaoAPF_Importada;
            Z1268FuncaoAPF_UpdAoImpBsln = A1268FuncaoAPF_UpdAoImpBsln;
            Z183FuncaoAPF_Ativo = A183FuncaoAPF_Ativo;
            Z360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
            Z359FuncaoAPF_ModuloCod = A359FuncaoAPF_ModuloCod;
            Z358FuncaoAPF_FunAPFPaiCod = A358FuncaoAPF_FunAPFPaiCod;
            Z744FuncaoAPF_MelhoraCod = A744FuncaoAPF_MelhoraCod;
            Z362FuncaoAPF_SistemaDes = A362FuncaoAPF_SistemaDes;
            Z706FuncaoAPF_SistemaTec = A706FuncaoAPF_SistemaTec;
            Z756FuncaoAPF_SistemaProjCod = A756FuncaoAPF_SistemaProjCod;
            Z361FuncaoAPF_ModuloNom = A361FuncaoAPF_ModuloNom;
            Z363FuncaoAPF_FunAPFPaiNom = A363FuncaoAPF_FunAPFPaiNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtFuncaoAPF_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV19Pgmname = "FuncaoAPF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pgmname", AV19Pgmname);
         edtFuncaoAPF_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         if ( ! (0==AV7FuncaoAPF_Codigo) )
         {
            A165FuncaoAPF_Codigo = AV7FuncaoAPF_Codigo;
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         GXAFUNCAOAPF_MODULOCOD_html0Z36( AV15FuncaoAPF_SistemaCod) ;
         GXAFUNCAOAPF_FUNAPFPAICOD_html0Z36( AV15FuncaoAPF_SistemaCod, AV7FuncaoAPF_Codigo) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPF_ModuloCod) )
         {
            dynFuncaoAPF_ModuloCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_ModuloCod.Enabled), 5, 0)));
         }
         else
         {
            dynFuncaoAPF_ModuloCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_ModuloCod.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_FuncaoAPF_FunAPFPaiCod) )
         {
            dynFuncaoAPF_FunAPFPaiCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_FunAPFPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_FunAPFPaiCod.Enabled), 5, 0)));
         }
         else
         {
            dynFuncaoAPF_FunAPFPaiCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_FunAPFPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_FunAPFPaiCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         bttBtn_trn_enter_Visible = (!( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_FuncaoAPF_MelhoraCod) )
         {
            A744FuncaoAPF_MelhoraCod = AV18Insert_FuncaoAPF_MelhoraCod;
            n744FuncaoAPF_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_FuncaoAPF_FunAPFPaiCod) )
         {
            A358FuncaoAPF_FunAPFPaiCod = AV13Insert_FuncaoAPF_FunAPFPaiCod;
            n358FuncaoAPF_FunAPFPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_FuncaoAPF_ModuloCod) )
         {
            A359FuncaoAPF_ModuloCod = AV12Insert_FuncaoAPF_ModuloCod;
            n359FuncaoAPF_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_FuncaoAPF_SistemaCod) )
         {
            A360FuncaoAPF_SistemaCod = AV11Insert_FuncaoAPF_SistemaCod;
            n360FuncaoAPF_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! (0==AV15FuncaoAPF_SistemaCod) )
            {
               A360FuncaoAPF_SistemaCod = AV15FuncaoAPF_SistemaCod;
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
            }
         }
         GXt_boolean1 = false;
         new prc_setnaopodedeletar(context ).execute(  "FnT",  AV7FuncaoAPF_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         bttBtneliminar_Visible = (( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) &&GXt_boolean1 ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtneliminar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtneliminar_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A183FuncaoAPF_Ativo)) && ( Gx_BScreen == 0 ) )
         {
            A183FuncaoAPF_Ativo = "A";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1268FuncaoAPF_UpdAoImpBsln) && ( Gx_BScreen == 0 ) )
         {
            A1268FuncaoAPF_UpdAoImpBsln = true;
            n1268FuncaoAPF_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXt_int2 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A388FuncaoAPF_TD = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            GXt_int2 = A387FuncaoAPF_AR;
            new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A387FuncaoAPF_AR = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            GXt_decimal3 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A386FuncaoAPF_PF = GXt_decimal3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            GXt_char4 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            /* Using cursor T000Z5 */
            pr_default.execute(3, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
            A361FuncaoAPF_ModuloNom = T000Z5_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = T000Z5_n361FuncaoAPF_ModuloNom[0];
            pr_default.close(3);
            /* Using cursor T000Z6 */
            pr_default.execute(4, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
            A363FuncaoAPF_FunAPFPaiNom = T000Z6_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = T000Z6_n363FuncaoAPF_FunAPFPaiNom[0];
            pr_default.close(4);
            /* Using cursor T000Z4 */
            pr_default.execute(2, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
            A362FuncaoAPF_SistemaDes = T000Z4_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = T000Z4_n362FuncaoAPF_SistemaDes[0];
            A706FuncaoAPF_SistemaTec = T000Z4_A706FuncaoAPF_SistemaTec[0];
            n706FuncaoAPF_SistemaTec = T000Z4_n706FuncaoAPF_SistemaTec[0];
            A756FuncaoAPF_SistemaProjCod = T000Z4_A756FuncaoAPF_SistemaProjCod[0];
            n756FuncaoAPF_SistemaProjCod = T000Z4_n756FuncaoAPF_SistemaProjCod[0];
            pr_default.close(2);
            Dvpanel_tableattributes_Title = "Fun��o de Transa��o do sistema: "+A362FuncaoAPF_SistemaDes;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            if ( ! T000Z4_n756FuncaoAPF_SistemaProjCod[0] )
            {
               GXt_char4 = A757FuncaoAPF_SolucaoTecnica;
               new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  A756FuncaoAPF_SistemaProjCod,  "T", out  GXt_char4) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
               A757FuncaoAPF_SolucaoTecnica = GXt_char4;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
            }
            else
            {
               A757FuncaoAPF_SolucaoTecnica = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
            }
         }
      }

      protected void Load0Z36( )
      {
         /* Using cursor T000Z8 */
         pr_default.execute(6, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound36 = 1;
            A166FuncaoAPF_Nome = T000Z8_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A167FuncaoAPF_Descricao = T000Z8_A167FuncaoAPF_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
            A184FuncaoAPF_Tipo = T000Z8_A184FuncaoAPF_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            A1022FuncaoAPF_DERImp = T000Z8_A1022FuncaoAPF_DERImp[0];
            n1022FuncaoAPF_DERImp = T000Z8_n1022FuncaoAPF_DERImp[0];
            A1026FuncaoAPF_RAImp = T000Z8_A1026FuncaoAPF_RAImp[0];
            n1026FuncaoAPF_RAImp = T000Z8_n1026FuncaoAPF_RAImp[0];
            A362FuncaoAPF_SistemaDes = T000Z8_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = T000Z8_n362FuncaoAPF_SistemaDes[0];
            A706FuncaoAPF_SistemaTec = T000Z8_A706FuncaoAPF_SistemaTec[0];
            n706FuncaoAPF_SistemaTec = T000Z8_n706FuncaoAPF_SistemaTec[0];
            A361FuncaoAPF_ModuloNom = T000Z8_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = T000Z8_n361FuncaoAPF_ModuloNom[0];
            A363FuncaoAPF_FunAPFPaiNom = T000Z8_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = T000Z8_n363FuncaoAPF_FunAPFPaiNom[0];
            A413FuncaoAPF_Acao = T000Z8_A413FuncaoAPF_Acao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A413FuncaoAPF_Acao", A413FuncaoAPF_Acao);
            n413FuncaoAPF_Acao = T000Z8_n413FuncaoAPF_Acao[0];
            A414FuncaoAPF_Mensagem = T000Z8_A414FuncaoAPF_Mensagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A414FuncaoAPF_Mensagem", A414FuncaoAPF_Mensagem);
            n414FuncaoAPF_Mensagem = T000Z8_n414FuncaoAPF_Mensagem[0];
            A432FuncaoAPF_Link = T000Z8_A432FuncaoAPF_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
            n432FuncaoAPF_Link = T000Z8_n432FuncaoAPF_Link[0];
            A1146FuncaoAPF_Tecnica = T000Z8_A1146FuncaoAPF_Tecnica[0];
            n1146FuncaoAPF_Tecnica = T000Z8_n1146FuncaoAPF_Tecnica[0];
            A1233FuncaoAPF_ParecerSE = T000Z8_A1233FuncaoAPF_ParecerSE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
            n1233FuncaoAPF_ParecerSE = T000Z8_n1233FuncaoAPF_ParecerSE[0];
            A1244FuncaoAPF_Observacao = T000Z8_A1244FuncaoAPF_Observacao[0];
            n1244FuncaoAPF_Observacao = T000Z8_n1244FuncaoAPF_Observacao[0];
            A1246FuncaoAPF_Importada = T000Z8_A1246FuncaoAPF_Importada[0];
            n1246FuncaoAPF_Importada = T000Z8_n1246FuncaoAPF_Importada[0];
            A1268FuncaoAPF_UpdAoImpBsln = T000Z8_A1268FuncaoAPF_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
            n1268FuncaoAPF_UpdAoImpBsln = T000Z8_n1268FuncaoAPF_UpdAoImpBsln[0];
            A183FuncaoAPF_Ativo = T000Z8_A183FuncaoAPF_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
            A360FuncaoAPF_SistemaCod = T000Z8_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = T000Z8_n360FuncaoAPF_SistemaCod[0];
            A359FuncaoAPF_ModuloCod = T000Z8_A359FuncaoAPF_ModuloCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
            n359FuncaoAPF_ModuloCod = T000Z8_n359FuncaoAPF_ModuloCod[0];
            A358FuncaoAPF_FunAPFPaiCod = T000Z8_A358FuncaoAPF_FunAPFPaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
            n358FuncaoAPF_FunAPFPaiCod = T000Z8_n358FuncaoAPF_FunAPFPaiCod[0];
            A744FuncaoAPF_MelhoraCod = T000Z8_A744FuncaoAPF_MelhoraCod[0];
            n744FuncaoAPF_MelhoraCod = T000Z8_n744FuncaoAPF_MelhoraCod[0];
            A756FuncaoAPF_SistemaProjCod = T000Z8_A756FuncaoAPF_SistemaProjCod[0];
            n756FuncaoAPF_SistemaProjCod = T000Z8_n756FuncaoAPF_SistemaProjCod[0];
            ZM0Z36( -33) ;
         }
         pr_default.close(6);
         OnLoadActions0Z36( ) ;
      }

      protected void OnLoadActions0Z36( )
      {
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A388FuncaoAPF_TD = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A387FuncaoAPF_AR = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A386FuncaoAPF_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         GXt_char4 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         Dvpanel_tableattributes_Title = "Fun��o de Transa��o do sistema: "+A362FuncaoAPF_SistemaDes;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         if ( ! T000Z4_n756FuncaoAPF_SistemaProjCod[0] )
         {
            GXt_char4 = A757FuncaoAPF_SolucaoTecnica;
            new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  A756FuncaoAPF_SistemaProjCod,  "T", out  GXt_char4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
            A757FuncaoAPF_SolucaoTecnica = GXt_char4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
         else
         {
            A757FuncaoAPF_SolucaoTecnica = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
      }

      protected void CheckExtendedTable0Z36( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A388FuncaoAPF_TD = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A387FuncaoAPF_AR = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A386FuncaoAPF_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         GXt_char4 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
         {
            GX_msglist.addItem("Fun��o de Transa��o � obrigat�rio.", 1, "FUNCAOAPF_NOME");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "EE") == 0 ) || ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "SE") == 0 ) || ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "CE") == 0 ) || ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "NM") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "FUNCAOAPF_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoAPF_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A184FuncaoAPF_Tipo)) )
         {
            GX_msglist.addItem("Tipo � obrigat�rio.", 1, "FUNCAOAPF_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoAPF_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000Z5 */
         pr_default.execute(3, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A359FuncaoAPF_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao Apf_Modulo'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A361FuncaoAPF_ModuloNom = T000Z5_A361FuncaoAPF_ModuloNom[0];
         n361FuncaoAPF_ModuloNom = T000Z5_n361FuncaoAPF_ModuloNom[0];
         pr_default.close(3);
         /* Using cursor T000Z6 */
         pr_default.execute(4, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A358FuncaoAPF_FunAPFPaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_FUNAPFPAICOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_FunAPFPaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A363FuncaoAPF_FunAPFPaiNom = T000Z6_A363FuncaoAPF_FunAPFPaiNom[0];
         n363FuncaoAPF_FunAPFPaiNom = T000Z6_n363FuncaoAPF_FunAPFPaiNom[0];
         pr_default.close(4);
         if ( ! ( ( StringUtil.StrCmp(A183FuncaoAPF_Ativo, "A") == 0 ) || ( StringUtil.StrCmp(A183FuncaoAPF_Ativo, "E") == 0 ) || ( StringUtil.StrCmp(A183FuncaoAPF_Ativo, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status da Fun��o fora do intervalo", "OutOfRange", 1, "FUNCAOAPF_ATIVO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoAPF_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000Z4 */
         pr_default.execute(2, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A360FuncaoAPF_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A362FuncaoAPF_SistemaDes = T000Z4_A362FuncaoAPF_SistemaDes[0];
         n362FuncaoAPF_SistemaDes = T000Z4_n362FuncaoAPF_SistemaDes[0];
         A706FuncaoAPF_SistemaTec = T000Z4_A706FuncaoAPF_SistemaTec[0];
         n706FuncaoAPF_SistemaTec = T000Z4_n706FuncaoAPF_SistemaTec[0];
         A756FuncaoAPF_SistemaProjCod = T000Z4_A756FuncaoAPF_SistemaProjCod[0];
         n756FuncaoAPF_SistemaProjCod = T000Z4_n756FuncaoAPF_SistemaProjCod[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "Fun��o de Transa��o do sistema: "+A362FuncaoAPF_SistemaDes;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         if ( ! T000Z4_n756FuncaoAPF_SistemaProjCod[0] )
         {
            GXt_char4 = A757FuncaoAPF_SolucaoTecnica;
            new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  A756FuncaoAPF_SistemaProjCod,  "T", out  GXt_char4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
            A757FuncaoAPF_SolucaoTecnica = GXt_char4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
         else
         {
            A757FuncaoAPF_SolucaoTecnica = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
         /* Using cursor T000Z7 */
         pr_default.execute(5, new Object[] {n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A744FuncaoAPF_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Funcao APFMelhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors0Z36( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_35( int A359FuncaoAPF_ModuloCod )
      {
         /* Using cursor T000Z9 */
         pr_default.execute(7, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A359FuncaoAPF_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao Apf_Modulo'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A361FuncaoAPF_ModuloNom = T000Z9_A361FuncaoAPF_ModuloNom[0];
         n361FuncaoAPF_ModuloNom = T000Z9_n361FuncaoAPF_ModuloNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A361FuncaoAPF_ModuloNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_36( int A358FuncaoAPF_FunAPFPaiCod )
      {
         /* Using cursor T000Z10 */
         pr_default.execute(8, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A358FuncaoAPF_FunAPFPaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_FUNAPFPAICOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_FunAPFPaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A363FuncaoAPF_FunAPFPaiNom = T000Z10_A363FuncaoAPF_FunAPFPaiNom[0];
         n363FuncaoAPF_FunAPFPaiNom = T000Z10_n363FuncaoAPF_FunAPFPaiNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A363FuncaoAPF_FunAPFPaiNom)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_34( int A360FuncaoAPF_SistemaCod )
      {
         /* Using cursor T000Z11 */
         pr_default.execute(9, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A360FuncaoAPF_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Sistema'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A362FuncaoAPF_SistemaDes = T000Z11_A362FuncaoAPF_SistemaDes[0];
         n362FuncaoAPF_SistemaDes = T000Z11_n362FuncaoAPF_SistemaDes[0];
         A706FuncaoAPF_SistemaTec = T000Z11_A706FuncaoAPF_SistemaTec[0];
         n706FuncaoAPF_SistemaTec = T000Z11_n706FuncaoAPF_SistemaTec[0];
         A756FuncaoAPF_SistemaProjCod = T000Z11_A756FuncaoAPF_SistemaProjCod[0];
         n756FuncaoAPF_SistemaProjCod = T000Z11_n756FuncaoAPF_SistemaProjCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A362FuncaoAPF_SistemaDes)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A706FuncaoAPF_SistemaTec))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_37( int A744FuncaoAPF_MelhoraCod )
      {
         /* Using cursor T000Z12 */
         pr_default.execute(10, new Object[] {n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A744FuncaoAPF_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Funcao APFMelhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey0Z36( )
      {
         /* Using cursor T000Z13 */
         pr_default.execute(11, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound36 = 1;
         }
         else
         {
            RcdFound36 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000Z3 */
         pr_default.execute(1, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0Z36( 33) ;
            RcdFound36 = 1;
            A165FuncaoAPF_Codigo = T000Z3_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            n165FuncaoAPF_Codigo = T000Z3_n165FuncaoAPF_Codigo[0];
            A166FuncaoAPF_Nome = T000Z3_A166FuncaoAPF_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A167FuncaoAPF_Descricao = T000Z3_A167FuncaoAPF_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
            A184FuncaoAPF_Tipo = T000Z3_A184FuncaoAPF_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            A1022FuncaoAPF_DERImp = T000Z3_A1022FuncaoAPF_DERImp[0];
            n1022FuncaoAPF_DERImp = T000Z3_n1022FuncaoAPF_DERImp[0];
            A1026FuncaoAPF_RAImp = T000Z3_A1026FuncaoAPF_RAImp[0];
            n1026FuncaoAPF_RAImp = T000Z3_n1026FuncaoAPF_RAImp[0];
            A413FuncaoAPF_Acao = T000Z3_A413FuncaoAPF_Acao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A413FuncaoAPF_Acao", A413FuncaoAPF_Acao);
            n413FuncaoAPF_Acao = T000Z3_n413FuncaoAPF_Acao[0];
            A414FuncaoAPF_Mensagem = T000Z3_A414FuncaoAPF_Mensagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A414FuncaoAPF_Mensagem", A414FuncaoAPF_Mensagem);
            n414FuncaoAPF_Mensagem = T000Z3_n414FuncaoAPF_Mensagem[0];
            A432FuncaoAPF_Link = T000Z3_A432FuncaoAPF_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
            n432FuncaoAPF_Link = T000Z3_n432FuncaoAPF_Link[0];
            A1146FuncaoAPF_Tecnica = T000Z3_A1146FuncaoAPF_Tecnica[0];
            n1146FuncaoAPF_Tecnica = T000Z3_n1146FuncaoAPF_Tecnica[0];
            A1233FuncaoAPF_ParecerSE = T000Z3_A1233FuncaoAPF_ParecerSE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
            n1233FuncaoAPF_ParecerSE = T000Z3_n1233FuncaoAPF_ParecerSE[0];
            A1244FuncaoAPF_Observacao = T000Z3_A1244FuncaoAPF_Observacao[0];
            n1244FuncaoAPF_Observacao = T000Z3_n1244FuncaoAPF_Observacao[0];
            A1246FuncaoAPF_Importada = T000Z3_A1246FuncaoAPF_Importada[0];
            n1246FuncaoAPF_Importada = T000Z3_n1246FuncaoAPF_Importada[0];
            A1268FuncaoAPF_UpdAoImpBsln = T000Z3_A1268FuncaoAPF_UpdAoImpBsln[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
            n1268FuncaoAPF_UpdAoImpBsln = T000Z3_n1268FuncaoAPF_UpdAoImpBsln[0];
            A183FuncaoAPF_Ativo = T000Z3_A183FuncaoAPF_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
            A360FuncaoAPF_SistemaCod = T000Z3_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = T000Z3_n360FuncaoAPF_SistemaCod[0];
            A359FuncaoAPF_ModuloCod = T000Z3_A359FuncaoAPF_ModuloCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
            n359FuncaoAPF_ModuloCod = T000Z3_n359FuncaoAPF_ModuloCod[0];
            A358FuncaoAPF_FunAPFPaiCod = T000Z3_A358FuncaoAPF_FunAPFPaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
            n358FuncaoAPF_FunAPFPaiCod = T000Z3_n358FuncaoAPF_FunAPFPaiCod[0];
            A744FuncaoAPF_MelhoraCod = T000Z3_A744FuncaoAPF_MelhoraCod[0];
            n744FuncaoAPF_MelhoraCod = T000Z3_n744FuncaoAPF_MelhoraCod[0];
            O166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            Z165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
            sMode36 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0Z36( ) ;
            if ( AnyError == 1 )
            {
               RcdFound36 = 0;
               InitializeNonKey0Z36( ) ;
            }
            Gx_mode = sMode36;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound36 = 0;
            InitializeNonKey0Z36( ) ;
            sMode36 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode36;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0Z36( ) ;
         if ( RcdFound36 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound36 = 0;
         /* Using cursor T000Z14 */
         pr_default.execute(12, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T000Z14_A165FuncaoAPF_Codigo[0] < A165FuncaoAPF_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T000Z14_A165FuncaoAPF_Codigo[0] > A165FuncaoAPF_Codigo ) ) )
            {
               A165FuncaoAPF_Codigo = T000Z14_A165FuncaoAPF_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               n165FuncaoAPF_Codigo = T000Z14_n165FuncaoAPF_Codigo[0];
               RcdFound36 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound36 = 0;
         /* Using cursor T000Z15 */
         pr_default.execute(13, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T000Z15_A165FuncaoAPF_Codigo[0] > A165FuncaoAPF_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T000Z15_A165FuncaoAPF_Codigo[0] < A165FuncaoAPF_Codigo ) ) )
            {
               A165FuncaoAPF_Codigo = T000Z15_A165FuncaoAPF_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               n165FuncaoAPF_Codigo = T000Z15_n165FuncaoAPF_Codigo[0];
               RcdFound36 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0Z36( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0Z36( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound36 == 1 )
            {
               if ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo )
               {
                  A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
                  n165FuncaoAPF_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAOAPF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0Z36( ) ;
                  GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0Z36( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAOAPF_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0Z36( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A165FuncaoAPF_Codigo != Z165FuncaoAPF_Codigo )
         {
            A165FuncaoAPF_Codigo = Z165FuncaoAPF_Codigo;
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAOAPF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0Z36( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000Z2 */
            pr_default.execute(0, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPF"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z166FuncaoAPF_Nome, T000Z2_A166FuncaoAPF_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z184FuncaoAPF_Tipo, T000Z2_A184FuncaoAPF_Tipo[0]) != 0 ) || ( Z1022FuncaoAPF_DERImp != T000Z2_A1022FuncaoAPF_DERImp[0] ) || ( Z1026FuncaoAPF_RAImp != T000Z2_A1026FuncaoAPF_RAImp[0] ) || ( Z413FuncaoAPF_Acao != T000Z2_A413FuncaoAPF_Acao[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z414FuncaoAPF_Mensagem != T000Z2_A414FuncaoAPF_Mensagem[0] ) || ( StringUtil.StrCmp(Z432FuncaoAPF_Link, T000Z2_A432FuncaoAPF_Link[0]) != 0 ) || ( Z1146FuncaoAPF_Tecnica != T000Z2_A1146FuncaoAPF_Tecnica[0] ) || ( Z1246FuncaoAPF_Importada != T000Z2_A1246FuncaoAPF_Importada[0] ) || ( Z1268FuncaoAPF_UpdAoImpBsln != T000Z2_A1268FuncaoAPF_UpdAoImpBsln[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z183FuncaoAPF_Ativo, T000Z2_A183FuncaoAPF_Ativo[0]) != 0 ) || ( Z360FuncaoAPF_SistemaCod != T000Z2_A360FuncaoAPF_SistemaCod[0] ) || ( Z359FuncaoAPF_ModuloCod != T000Z2_A359FuncaoAPF_ModuloCod[0] ) || ( Z358FuncaoAPF_FunAPFPaiCod != T000Z2_A358FuncaoAPF_FunAPFPaiCod[0] ) || ( Z744FuncaoAPF_MelhoraCod != T000Z2_A744FuncaoAPF_MelhoraCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z166FuncaoAPF_Nome, T000Z2_A166FuncaoAPF_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z166FuncaoAPF_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A166FuncaoAPF_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z184FuncaoAPF_Tipo, T000Z2_A184FuncaoAPF_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z184FuncaoAPF_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A184FuncaoAPF_Tipo[0]);
               }
               if ( Z1022FuncaoAPF_DERImp != T000Z2_A1022FuncaoAPF_DERImp[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_DERImp");
                  GXUtil.WriteLogRaw("Old: ",Z1022FuncaoAPF_DERImp);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A1022FuncaoAPF_DERImp[0]);
               }
               if ( Z1026FuncaoAPF_RAImp != T000Z2_A1026FuncaoAPF_RAImp[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_RAImp");
                  GXUtil.WriteLogRaw("Old: ",Z1026FuncaoAPF_RAImp);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A1026FuncaoAPF_RAImp[0]);
               }
               if ( Z413FuncaoAPF_Acao != T000Z2_A413FuncaoAPF_Acao[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Acao");
                  GXUtil.WriteLogRaw("Old: ",Z413FuncaoAPF_Acao);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A413FuncaoAPF_Acao[0]);
               }
               if ( Z414FuncaoAPF_Mensagem != T000Z2_A414FuncaoAPF_Mensagem[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Mensagem");
                  GXUtil.WriteLogRaw("Old: ",Z414FuncaoAPF_Mensagem);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A414FuncaoAPF_Mensagem[0]);
               }
               if ( StringUtil.StrCmp(Z432FuncaoAPF_Link, T000Z2_A432FuncaoAPF_Link[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Link");
                  GXUtil.WriteLogRaw("Old: ",Z432FuncaoAPF_Link);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A432FuncaoAPF_Link[0]);
               }
               if ( Z1146FuncaoAPF_Tecnica != T000Z2_A1146FuncaoAPF_Tecnica[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Tecnica");
                  GXUtil.WriteLogRaw("Old: ",Z1146FuncaoAPF_Tecnica);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A1146FuncaoAPF_Tecnica[0]);
               }
               if ( Z1246FuncaoAPF_Importada != T000Z2_A1246FuncaoAPF_Importada[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Importada");
                  GXUtil.WriteLogRaw("Old: ",Z1246FuncaoAPF_Importada);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A1246FuncaoAPF_Importada[0]);
               }
               if ( Z1268FuncaoAPF_UpdAoImpBsln != T000Z2_A1268FuncaoAPF_UpdAoImpBsln[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_UpdAoImpBsln");
                  GXUtil.WriteLogRaw("Old: ",Z1268FuncaoAPF_UpdAoImpBsln);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A1268FuncaoAPF_UpdAoImpBsln[0]);
               }
               if ( StringUtil.StrCmp(Z183FuncaoAPF_Ativo, T000Z2_A183FuncaoAPF_Ativo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z183FuncaoAPF_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A183FuncaoAPF_Ativo[0]);
               }
               if ( Z360FuncaoAPF_SistemaCod != T000Z2_A360FuncaoAPF_SistemaCod[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z360FuncaoAPF_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A360FuncaoAPF_SistemaCod[0]);
               }
               if ( Z359FuncaoAPF_ModuloCod != T000Z2_A359FuncaoAPF_ModuloCod[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_ModuloCod");
                  GXUtil.WriteLogRaw("Old: ",Z359FuncaoAPF_ModuloCod);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A359FuncaoAPF_ModuloCod[0]);
               }
               if ( Z358FuncaoAPF_FunAPFPaiCod != T000Z2_A358FuncaoAPF_FunAPFPaiCod[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_FunAPFPaiCod");
                  GXUtil.WriteLogRaw("Old: ",Z358FuncaoAPF_FunAPFPaiCod);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A358FuncaoAPF_FunAPFPaiCod[0]);
               }
               if ( Z744FuncaoAPF_MelhoraCod != T000Z2_A744FuncaoAPF_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("funcaoapf:[seudo value changed for attri]"+"FuncaoAPF_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z744FuncaoAPF_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T000Z2_A744FuncaoAPF_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncoesAPF"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0Z36( )
      {
         BeforeValidate0Z36( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0Z36( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0Z36( 0) ;
            CheckOptimisticConcurrency0Z36( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0Z36( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0Z36( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000Z16 */
                     pr_default.execute(14, new Object[] {A166FuncaoAPF_Nome, A167FuncaoAPF_Descricao, A184FuncaoAPF_Tipo, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n413FuncaoAPF_Acao, A413FuncaoAPF_Acao, n414FuncaoAPF_Mensagem, A414FuncaoAPF_Mensagem, n432FuncaoAPF_Link, A432FuncaoAPF_Link, n1146FuncaoAPF_Tecnica, A1146FuncaoAPF_Tecnica, n1233FuncaoAPF_ParecerSE, A1233FuncaoAPF_ParecerSE, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, n1268FuncaoAPF_UpdAoImpBsln, A1268FuncaoAPF_UpdAoImpBsln, A183FuncaoAPF_Ativo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod, n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod, n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod});
                     A165FuncaoAPF_Codigo = T000Z16_A165FuncaoAPF_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                     n165FuncaoAPF_Codigo = T000Z16_n165FuncaoAPF_Codigo[0];
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0Z0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0Z36( ) ;
            }
            EndLevel0Z36( ) ;
         }
         CloseExtendedTableCursors0Z36( ) ;
      }

      protected void Update0Z36( )
      {
         BeforeValidate0Z36( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0Z36( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0Z36( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0Z36( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0Z36( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000Z17 */
                     pr_default.execute(15, new Object[] {A166FuncaoAPF_Nome, A167FuncaoAPF_Descricao, A184FuncaoAPF_Tipo, n1022FuncaoAPF_DERImp, A1022FuncaoAPF_DERImp, n1026FuncaoAPF_RAImp, A1026FuncaoAPF_RAImp, n413FuncaoAPF_Acao, A413FuncaoAPF_Acao, n414FuncaoAPF_Mensagem, A414FuncaoAPF_Mensagem, n432FuncaoAPF_Link, A432FuncaoAPF_Link, n1146FuncaoAPF_Tecnica, A1146FuncaoAPF_Tecnica, n1233FuncaoAPF_ParecerSE, A1233FuncaoAPF_ParecerSE, n1244FuncaoAPF_Observacao, A1244FuncaoAPF_Observacao, n1246FuncaoAPF_Importada, A1246FuncaoAPF_Importada, n1268FuncaoAPF_UpdAoImpBsln, A1268FuncaoAPF_UpdAoImpBsln, A183FuncaoAPF_Ativo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod, n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod, n744FuncaoAPF_MelhoraCod, A744FuncaoAPF_MelhoraCod, n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncoesAPF"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0Z36( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0Z36( ) ;
         }
         CloseExtendedTableCursors0Z36( ) ;
      }

      protected void DeferredUpdate0Z36( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0Z36( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0Z36( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0Z36( ) ;
            AfterConfirm0Z36( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0Z36( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000Z18 */
                  pr_default.execute(16, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode36 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0Z36( ) ;
         Gx_mode = sMode36;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0Z36( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int2 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A388FuncaoAPF_TD = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            GXt_int2 = A387FuncaoAPF_AR;
            new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A387FuncaoAPF_AR = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            GXt_decimal3 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A386FuncaoAPF_PF = GXt_decimal3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            GXt_char4 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            /* Using cursor T000Z19 */
            pr_default.execute(17, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
            A361FuncaoAPF_ModuloNom = T000Z19_A361FuncaoAPF_ModuloNom[0];
            n361FuncaoAPF_ModuloNom = T000Z19_n361FuncaoAPF_ModuloNom[0];
            pr_default.close(17);
            /* Using cursor T000Z20 */
            pr_default.execute(18, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
            A363FuncaoAPF_FunAPFPaiNom = T000Z20_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = T000Z20_n363FuncaoAPF_FunAPFPaiNom[0];
            pr_default.close(18);
            /* Using cursor T000Z21 */
            pr_default.execute(19, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
            A362FuncaoAPF_SistemaDes = T000Z21_A362FuncaoAPF_SistemaDes[0];
            n362FuncaoAPF_SistemaDes = T000Z21_n362FuncaoAPF_SistemaDes[0];
            A706FuncaoAPF_SistemaTec = T000Z21_A706FuncaoAPF_SistemaTec[0];
            n706FuncaoAPF_SistemaTec = T000Z21_n706FuncaoAPF_SistemaTec[0];
            A756FuncaoAPF_SistemaProjCod = T000Z21_A756FuncaoAPF_SistemaProjCod[0];
            n756FuncaoAPF_SistemaProjCod = T000Z21_n756FuncaoAPF_SistemaProjCod[0];
            pr_default.close(19);
            Dvpanel_tableattributes_Title = "Fun��o de Transa��o do sistema: "+A362FuncaoAPF_SistemaDes;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            if ( ! T000Z21_n756FuncaoAPF_SistemaProjCod[0] )
            {
               GXt_char4 = A757FuncaoAPF_SolucaoTecnica;
               new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  A756FuncaoAPF_SistemaProjCod,  "T", out  GXt_char4) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
               A757FuncaoAPF_SolucaoTecnica = GXt_char4;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
            }
            else
            {
               A757FuncaoAPF_SolucaoTecnica = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000Z22 */
            pr_default.execute(20, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T000Z23 */
            pr_default.execute(21, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Fun��es APF - An�lise de Ponto de Fun��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T000Z24 */
            pr_default.execute(22, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao APF Pre Impprta��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T000Z25 */
            pr_default.execute(23, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Item da Mudan�a"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T000Z26 */
            pr_default.execute(24, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T000Z27 */
            pr_default.execute(25, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Evidencia da Funcao de Transa��o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor T000Z28 */
            pr_default.execute(26, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor T000Z29 */
            pr_default.execute(27, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor T000Z30 */
            pr_default.execute(28, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcoes Usuario Funcoes APF"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
         }
      }

      protected void EndLevel0Z36( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0Z36( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(17);
            pr_default.close(18);
            context.CommitDataStores( "FuncaoAPF");
            if ( AnyError == 0 )
            {
               ConfirmValues0Z0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(17);
            pr_default.close(18);
            context.RollbackDataStores( "FuncaoAPF");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0Z36( )
      {
         /* Scan By routine */
         /* Using cursor T000Z31 */
         pr_default.execute(29);
         RcdFound36 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound36 = 1;
            A165FuncaoAPF_Codigo = T000Z31_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            n165FuncaoAPF_Codigo = T000Z31_n165FuncaoAPF_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0Z36( )
      {
         /* Scan next routine */
         pr_default.readNext(29);
         RcdFound36 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound36 = 1;
            A165FuncaoAPF_Codigo = T000Z31_A165FuncaoAPF_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            n165FuncaoAPF_Codigo = T000Z31_n165FuncaoAPF_Codigo[0];
         }
      }

      protected void ScanEnd0Z36( )
      {
         pr_default.close(29);
      }

      protected void AfterConfirm0Z36( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0Z36( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0Z36( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0Z36( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0Z36( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0Z36( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A166FuncaoAPF_Nome, O166FuncaoAPF_Nome) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "FnT",  A166FuncaoAPF_Nome) )
         {
            GX_msglist.addItem("Nome de Fun��o de Transa��o j� cadastrado!", 1, "FUNCAOAPF_NOME");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPF_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes0Z36( )
      {
         dynFuncaoAPF_ModuloCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_ModuloCod.Enabled), 5, 0)));
         cmbFuncaoAPF_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Tipo.Enabled), 5, 0)));
         chkFuncaoAPF_Acao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoAPF_Acao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncaoAPF_Acao.Enabled), 5, 0)));
         chkFuncaoAPF_Mensagem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoAPF_Mensagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncaoAPF_Mensagem.Enabled), 5, 0)));
         edtFuncaoAPF_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Nome_Enabled), 5, 0)));
         edtFuncaoAPF_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Descricao_Enabled), 5, 0)));
         edtFuncaoAPF_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Link_Enabled), 5, 0)));
         dynFuncaoAPF_FunAPFPaiCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoAPF_FunAPFPaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoAPF_FunAPFPaiCod.Enabled), 5, 0)));
         edtFuncaoAPF_TD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_TD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_TD_Enabled), 5, 0)));
         edtFuncaoAPF_AR_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_AR_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_AR_Enabled), 5, 0)));
         cmbFuncaoAPF_Complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Complexidade.Enabled), 5, 0)));
         edtFuncaoAPF_PF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_PF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_PF_Enabled), 5, 0)));
         edtFuncaoAPF_ParecerSE_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_ParecerSE_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_ParecerSE_Enabled), 5, 0)));
         cmbFuncaoAPF_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_Ativo.Enabled), 5, 0)));
         cmbFuncaoAPF_UpdAoImpBsln.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_UpdAoImpBsln_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPF_UpdAoImpBsln.Enabled), 5, 0)));
         edtFuncaoAPF_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Enabled), 5, 0)));
         Funcaoapf_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Funcaoapf_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Funcaoapf_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0Z0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117183458");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +AV15FuncaoAPF_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z166FuncaoAPF_Nome", Z166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "Z184FuncaoAPF_Tipo", StringUtil.RTrim( Z184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1022FuncaoAPF_DERImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1022FuncaoAPF_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1026FuncaoAPF_RAImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1026FuncaoAPF_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z413FuncaoAPF_Acao", Z413FuncaoAPF_Acao);
         GxWebStd.gx_boolean_hidden_field( context, "Z414FuncaoAPF_Mensagem", Z414FuncaoAPF_Mensagem);
         GxWebStd.gx_hidden_field( context, "Z432FuncaoAPF_Link", Z432FuncaoAPF_Link);
         GxWebStd.gx_hidden_field( context, "Z1146FuncaoAPF_Tecnica", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1146FuncaoAPF_Tecnica), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1246FuncaoAPF_Importada", context.localUtil.TToC( Z1246FuncaoAPF_Importada, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_boolean_hidden_field( context, "Z1268FuncaoAPF_UpdAoImpBsln", Z1268FuncaoAPF_UpdAoImpBsln);
         GxWebStd.gx_hidden_field( context, "Z183FuncaoAPF_Ativo", StringUtil.RTrim( Z183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, "Z360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z359FuncaoAPF_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z358FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z744FuncaoAPF_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O166FuncaoAPF_Nome", O166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMAPROJCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SOLUCAOTECNICA", A757FuncaoAPF_SolucaoTecnica);
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_FuncaoAPF_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_FUNAPFPAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_FUNCAOAPF_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Insert_FuncaoAPF_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMADES", A362FuncaoAPF_SistemaDes);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_DERIMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1022FuncaoAPF_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_RAIMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1026FuncaoAPF_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TECNICA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1146FuncaoAPF_Tecnica), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_OBSERVACAO", A1244FuncaoAPF_Observacao);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_IMPORTADA", context.localUtil.TToC( A1246FuncaoAPF_Importada, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMATEC", StringUtil.RTrim( A706FuncaoAPF_SistemaTec));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_MODULONOM", StringUtil.RTrim( A361FuncaoAPF_ModuloNom));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_FUNAPFPAINOM", A363FuncaoAPF_FunAPFPaiNom);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV19Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaoapf_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FuncaoAPF";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A744FuncaoAPF_MelhoraCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1022FuncaoAPF_DERImp), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1026FuncaoAPF_RAImp), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1146FuncaoAPF_Tecnica), "9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1246FuncaoAPF_Importada, "99/99/99 99:99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_Codigo:"+context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_SistemaCod:"+context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_MelhoraCod:"+context.localUtil.Format( (decimal)(A744FuncaoAPF_MelhoraCod), "ZZZZZ9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_DERImp:"+context.localUtil.Format( (decimal)(A1022FuncaoAPF_DERImp), "ZZZ9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_RAImp:"+context.localUtil.Format( (decimal)(A1026FuncaoAPF_RAImp), "ZZZ9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_Tecnica:"+context.localUtil.Format( (decimal)(A1146FuncaoAPF_Tecnica), "9"));
         GXUtil.WriteLog("funcaoapf:[SendSecurityCheck value for]"+"FuncaoAPF_Importada:"+context.localUtil.Format( A1246FuncaoAPF_Importada, "99/99/99 99:99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoAPF_Codigo) + "," + UrlEncode("" +AV15FuncaoAPF_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPF" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fun��o de Transa��o" ;
      }

      protected void InitializeNonKey0Z36( )
      {
         A360FuncaoAPF_SistemaCod = 0;
         n360FuncaoAPF_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         A359FuncaoAPF_ModuloCod = 0;
         n359FuncaoAPF_ModuloCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A359FuncaoAPF_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0)));
         n359FuncaoAPF_ModuloCod = ((0==A359FuncaoAPF_ModuloCod) ? true : false);
         A358FuncaoAPF_FunAPFPaiCod = 0;
         n358FuncaoAPF_FunAPFPaiCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
         n358FuncaoAPF_FunAPFPaiCod = ((0==A358FuncaoAPF_FunAPFPaiCod) ? true : false);
         A744FuncaoAPF_MelhoraCod = 0;
         n744FuncaoAPF_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
         A757FuncaoAPF_SolucaoTecnica = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         A185FuncaoAPF_Complexidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         A386FuncaoAPF_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         A387FuncaoAPF_AR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         A388FuncaoAPF_TD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         A166FuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         A167FuncaoAPF_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
         A184FuncaoAPF_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         A1022FuncaoAPF_DERImp = 0;
         n1022FuncaoAPF_DERImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1022FuncaoAPF_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1022FuncaoAPF_DERImp), 4, 0)));
         A1026FuncaoAPF_RAImp = 0;
         n1026FuncaoAPF_RAImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1026FuncaoAPF_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1026FuncaoAPF_RAImp), 4, 0)));
         A362FuncaoAPF_SistemaDes = "";
         n362FuncaoAPF_SistemaDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A362FuncaoAPF_SistemaDes", A362FuncaoAPF_SistemaDes);
         A706FuncaoAPF_SistemaTec = "";
         n706FuncaoAPF_SistemaTec = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A706FuncaoAPF_SistemaTec", A706FuncaoAPF_SistemaTec);
         A756FuncaoAPF_SistemaProjCod = 0;
         n756FuncaoAPF_SistemaProjCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
         A361FuncaoAPF_ModuloNom = "";
         n361FuncaoAPF_ModuloNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A361FuncaoAPF_ModuloNom", A361FuncaoAPF_ModuloNom);
         A363FuncaoAPF_FunAPFPaiNom = "";
         n363FuncaoAPF_FunAPFPaiNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A363FuncaoAPF_FunAPFPaiNom", A363FuncaoAPF_FunAPFPaiNom);
         A413FuncaoAPF_Acao = false;
         n413FuncaoAPF_Acao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A413FuncaoAPF_Acao", A413FuncaoAPF_Acao);
         n413FuncaoAPF_Acao = ((false==A413FuncaoAPF_Acao) ? true : false);
         A414FuncaoAPF_Mensagem = false;
         n414FuncaoAPF_Mensagem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A414FuncaoAPF_Mensagem", A414FuncaoAPF_Mensagem);
         n414FuncaoAPF_Mensagem = ((false==A414FuncaoAPF_Mensagem) ? true : false);
         A432FuncaoAPF_Link = "";
         n432FuncaoAPF_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
         n432FuncaoAPF_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A432FuncaoAPF_Link)) ? true : false);
         A1146FuncaoAPF_Tecnica = 0;
         n1146FuncaoAPF_Tecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1146FuncaoAPF_Tecnica", StringUtil.Str( (decimal)(A1146FuncaoAPF_Tecnica), 1, 0));
         A1233FuncaoAPF_ParecerSE = "";
         n1233FuncaoAPF_ParecerSE = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
         n1233FuncaoAPF_ParecerSE = (String.IsNullOrEmpty(StringUtil.RTrim( A1233FuncaoAPF_ParecerSE)) ? true : false);
         A1244FuncaoAPF_Observacao = "";
         n1244FuncaoAPF_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1244FuncaoAPF_Observacao", A1244FuncaoAPF_Observacao);
         A1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         n1246FuncaoAPF_Importada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1246FuncaoAPF_Importada", context.localUtil.TToC( A1246FuncaoAPF_Importada, 8, 5, 0, 3, "/", ":", " "));
         A1268FuncaoAPF_UpdAoImpBsln = true;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         A183FuncaoAPF_Ativo = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         O166FuncaoAPF_Nome = A166FuncaoAPF_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         Z166FuncaoAPF_Nome = "";
         Z184FuncaoAPF_Tipo = "";
         Z1022FuncaoAPF_DERImp = 0;
         Z1026FuncaoAPF_RAImp = 0;
         Z413FuncaoAPF_Acao = false;
         Z414FuncaoAPF_Mensagem = false;
         Z432FuncaoAPF_Link = "";
         Z1146FuncaoAPF_Tecnica = 0;
         Z1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         Z1268FuncaoAPF_UpdAoImpBsln = false;
         Z183FuncaoAPF_Ativo = "";
         Z360FuncaoAPF_SistemaCod = 0;
         Z359FuncaoAPF_ModuloCod = 0;
         Z358FuncaoAPF_FunAPFPaiCod = 0;
         Z744FuncaoAPF_MelhoraCod = 0;
      }

      protected void InitAll0Z36( )
      {
         A165FuncaoAPF_Codigo = 0;
         n165FuncaoAPF_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         InitializeNonKey0Z36( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A360FuncaoAPF_SistemaCod = i360FuncaoAPF_SistemaCod;
         n360FuncaoAPF_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         A183FuncaoAPF_Ativo = i183FuncaoAPF_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         A1268FuncaoAPF_UpdAoImpBsln = i1268FuncaoAPF_UpdAoImpBsln;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117183519");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaoapf.js", "?20203117183519");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapf_modulocod_Internalname = "TEXTBLOCKFUNCAOAPF_MODULOCOD";
         dynFuncaoAPF_ModuloCod_Internalname = "FUNCAOAPF_MODULOCOD";
         lblTextblockfuncaoapf_tipo_Internalname = "TEXTBLOCKFUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         lblTextblockfuncaoapf_acao_Internalname = "TEXTBLOCKFUNCAOAPF_ACAO";
         chkFuncaoAPF_Acao_Internalname = "FUNCAOAPF_ACAO";
         lblTextblockfuncaoapf_mensagem_Internalname = "TEXTBLOCKFUNCAOAPF_MENSAGEM";
         chkFuncaoAPF_Mensagem_Internalname = "FUNCAOAPF_MENSAGEM";
         tblTablemergedfuncaoapf_modulocod_Internalname = "TABLEMERGEDFUNCAOAPF_MODULOCOD";
         lblTextblockfuncaoapf_nome_Internalname = "TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         lblTextblockfuncaoapf_descricao_Internalname = "TEXTBLOCKFUNCAOAPF_DESCRICAO";
         edtFuncaoAPF_Descricao_Internalname = "FUNCAOAPF_DESCRICAO";
         lblTextblockfuncaoapf_link_Internalname = "TEXTBLOCKFUNCAOAPF_LINK";
         edtFuncaoAPF_Link_Internalname = "FUNCAOAPF_LINK";
         lblFuncaoapf_link_righttext_Internalname = "FUNCAOAPF_LINK_RIGHTTEXT";
         tblTablemergedfuncaoapf_link_Internalname = "TABLEMERGEDFUNCAOAPF_LINK";
         lblTextblockfuncaoapf_funapfpaicod_Internalname = "TEXTBLOCKFUNCAOAPF_FUNAPFPAICOD";
         dynFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD";
         lblTextblockfuncaoapf_td_Internalname = "TEXTBLOCKFUNCAOAPF_TD";
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD";
         lblTextblockfuncaoapf_ar_Internalname = "TEXTBLOCKFUNCAOAPF_AR";
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR";
         lblTextblockfuncaoapf_complexidade_Internalname = "TEXTBLOCKFUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE";
         lblTextblockfuncaoapf_pf_Internalname = "TEXTBLOCKFUNCAOAPF_PF";
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF";
         tblTablemergedfuncaoapf_td_Internalname = "TABLEMERGEDFUNCAOAPF_TD";
         lblTextblockfuncaoapf_observacao_Internalname = "TEXTBLOCKFUNCAOAPF_OBSERVACAO";
         Funcaoapf_observacao_Internalname = "FUNCAOAPF_OBSERVACAO";
         lblTextblockfuncaoapf_parecerse_Internalname = "TEXTBLOCKFUNCAOAPF_PARECERSE";
         edtFuncaoAPF_ParecerSE_Internalname = "FUNCAOAPF_PARECERSE";
         lblTextblockfuncaoapf_ativo_Internalname = "TEXTBLOCKFUNCAOAPF_ATIVO";
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO";
         lblTextblockfuncaoapf_updaoimpbsln_Internalname = "TEXTBLOCKFUNCAOAPF_UPDAOIMPBSLN";
         cmbFuncaoAPF_UpdAoImpBsln_Internalname = "FUNCAOAPF_UPDAOIMPBSLN";
         tblTablemergedfuncaoapf_ativo_Internalname = "TABLEMERGEDFUNCAOAPF_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtneliminar_Internalname = "BTNELIMINAR";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Fun��o de Transa��o";
         Dvpanel_tableattributes_Title = "Fun��o de Transa��o";
         edtFuncaoAPF_Nome_Height = 3;
         edtFuncaoAPF_Nome_Width = 80;
         chkFuncaoAPF_Mensagem.Enabled = 1;
         chkFuncaoAPF_Acao.Enabled = 1;
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         cmbFuncaoAPF_Tipo.Enabled = 1;
         dynFuncaoAPF_ModuloCod_Jsonclick = "";
         dynFuncaoAPF_ModuloCod.Enabled = 1;
         edtFuncaoAPF_Link_Enabled = 1;
         edtFuncaoAPF_PF_Jsonclick = "";
         edtFuncaoAPF_PF_Enabled = 0;
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Complexidade.Enabled = 0;
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_AR_Enabled = 0;
         edtFuncaoAPF_TD_Jsonclick = "";
         edtFuncaoAPF_TD_Enabled = 0;
         cmbFuncaoAPF_UpdAoImpBsln_Jsonclick = "";
         cmbFuncaoAPF_UpdAoImpBsln.Enabled = 1;
         cmbFuncaoAPF_Ativo_Jsonclick = "";
         cmbFuncaoAPF_Ativo.Enabled = 1;
         cmbFuncaoAPF_Ativo.Visible = 1;
         lblTextblockfuncaoapf_ativo_Visible = 1;
         edtFuncaoAPF_ParecerSE_Enabled = 1;
         Funcaoapf_observacao_Enabled = Convert.ToBoolean( 1);
         dynFuncaoAPF_FunAPFPaiCod_Jsonclick = "";
         dynFuncaoAPF_FunAPFPaiCod.Enabled = 1;
         edtFuncaoAPF_Descricao_Enabled = 1;
         edtFuncaoAPF_Nome_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtneliminar_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPF_Codigo_Enabled = 0;
         edtFuncaoAPF_Codigo_Visible = 1;
         chkFuncaoAPF_Mensagem.Caption = "";
         chkFuncaoAPF_Acao.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAFUNCAOAPF_MODULOCOD0Z36( int AV15FuncaoAPF_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAFUNCAOAPF_MODULOCOD_data0Z36( AV15FuncaoAPF_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAFUNCAOAPF_MODULOCOD_html0Z36( int AV15FuncaoAPF_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLAFUNCAOAPF_MODULOCOD_data0Z36( AV15FuncaoAPF_SistemaCod) ;
         gxdynajaxindex = 1;
         dynFuncaoAPF_ModuloCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynFuncaoAPF_ModuloCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAFUNCAOAPF_MODULOCOD_data0Z36( int AV15FuncaoAPF_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000Z32 */
         pr_default.execute(30, new Object[] {AV15FuncaoAPF_SistemaCod});
         while ( (pr_default.getStatus(30) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000Z32_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000Z32_A143Modulo_Nome[0]));
            pr_default.readNext(30);
         }
         pr_default.close(30);
      }

      protected void GXDLAFUNCAOAPF_FUNAPFPAICOD0Z36( int AV15FuncaoAPF_SistemaCod ,
                                                      int AV7FuncaoAPF_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAFUNCAOAPF_FUNAPFPAICOD_data0Z36( AV15FuncaoAPF_SistemaCod, AV7FuncaoAPF_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAFUNCAOAPF_FUNAPFPAICOD_html0Z36( int AV15FuncaoAPF_SistemaCod ,
                                                         int AV7FuncaoAPF_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLAFUNCAOAPF_FUNAPFPAICOD_data0Z36( AV15FuncaoAPF_SistemaCod, AV7FuncaoAPF_Codigo) ;
         gxdynajaxindex = 1;
         dynFuncaoAPF_FunAPFPaiCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynFuncaoAPF_FunAPFPaiCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAFUNCAOAPF_FUNAPFPAICOD_data0Z36( int AV15FuncaoAPF_SistemaCod ,
                                                           int AV7FuncaoAPF_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T000Z33 */
         pr_default.execute(31, new Object[] {AV7FuncaoAPF_Codigo, AV15FuncaoAPF_SistemaCod});
         while ( (pr_default.getStatus(31) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000Z33_A165FuncaoAPF_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000Z33_A166FuncaoAPF_Nome[0]);
            pr_default.readNext(31);
         }
         pr_default.close(31);
      }

      protected void GX5ASAFUNCAOAPF_TD0Z36( int A165FuncaoAPF_Codigo )
      {
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A388FuncaoAPF_TD = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX6ASAFUNCAOAPF_AR0Z36( int A165FuncaoAPF_Codigo )
      {
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A387FuncaoAPF_AR = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX7ASAFUNCAOAPF_PF0Z36( int A165FuncaoAPF_Codigo )
      {
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A386FuncaoAPF_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASAFUNCAOAPF_COMPLEXIDADE0Z36( int A165FuncaoAPF_Codigo )
      {
         GXt_char4 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A185FuncaoAPF_Complexidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX9ASAFUNCAOAPF_SOLUCAOTECNICA0Z36( int A744FuncaoAPF_MelhoraCod ,
                                                         int A756FuncaoAPF_SistemaProjCod )
      {
         if ( ! T000Z21_n756FuncaoAPF_SistemaProjCod[0] )
         {
            GXt_char4 = A757FuncaoAPF_SolucaoTecnica;
            new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  A756FuncaoAPF_SistemaProjCod,  "T", out  GXt_char4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A756FuncaoAPF_SistemaProjCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A756FuncaoAPF_SistemaProjCod), 6, 0)));
            A757FuncaoAPF_SolucaoTecnica = GXt_char4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
         else
         {
            A757FuncaoAPF_SolucaoTecnica = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A757FuncaoAPF_SolucaoTecnica", A757FuncaoAPF_SolucaoTecnica);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A757FuncaoAPF_SolucaoTecnica)+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Funcaoapf_codigo( int GX_Parm1 ,
                                          short GX_Parm2 ,
                                          short GX_Parm3 ,
                                          decimal GX_Parm4 ,
                                          GXCombobox cmbGX_Parm5 )
      {
         A165FuncaoAPF_Codigo = GX_Parm1;
         n165FuncaoAPF_Codigo = false;
         A388FuncaoAPF_TD = GX_Parm2;
         A387FuncaoAPF_AR = GX_Parm3;
         A386FuncaoAPF_PF = GX_Parm4;
         cmbFuncaoAPF_Complexidade = cmbGX_Parm5;
         A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.CurrentValue;
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         A388FuncaoAPF_TD = GXt_int2;
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         A387FuncaoAPF_AR = GXt_int2;
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         A386FuncaoAPF_PF = GXt_decimal3;
         GXt_char4 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char4) ;
         A185FuncaoAPF_Complexidade = GXt_char4;
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
         cmbFuncaoAPF_Complexidade.CurrentValue = A185FuncaoAPF_Complexidade;
         isValidOutput.Add(cmbFuncaoAPF_Complexidade);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapf_modulocod( GXCombobox dynGX_Parm1 ,
                                             String GX_Parm2 )
      {
         dynFuncaoAPF_ModuloCod = dynGX_Parm1;
         A359FuncaoAPF_ModuloCod = (int)(NumberUtil.Val( dynFuncaoAPF_ModuloCod.CurrentValue, "."));
         n359FuncaoAPF_ModuloCod = false;
         A361FuncaoAPF_ModuloNom = GX_Parm2;
         n361FuncaoAPF_ModuloNom = false;
         /* Using cursor T000Z34 */
         pr_default.execute(32, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
         if ( (pr_default.getStatus(32) == 101) )
         {
            if ( ! ( (0==A359FuncaoAPF_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao Apf_Modulo'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_ModuloCod_Internalname;
            }
         }
         A361FuncaoAPF_ModuloNom = T000Z34_A361FuncaoAPF_ModuloNom[0];
         n361FuncaoAPF_ModuloNom = T000Z34_n361FuncaoAPF_ModuloNom[0];
         pr_default.close(32);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A361FuncaoAPF_ModuloNom = "";
            n361FuncaoAPF_ModuloNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A361FuncaoAPF_ModuloNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapf_funapfpaicod( GXCombobox dynGX_Parm1 ,
                                                String GX_Parm2 )
      {
         dynFuncaoAPF_FunAPFPaiCod = dynGX_Parm1;
         A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( dynFuncaoAPF_FunAPFPaiCod.CurrentValue, "."));
         n358FuncaoAPF_FunAPFPaiCod = false;
         A363FuncaoAPF_FunAPFPaiNom = GX_Parm2;
         n363FuncaoAPF_FunAPFPaiNom = false;
         /* Using cursor T000Z35 */
         pr_default.execute(33, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
         if ( (pr_default.getStatus(33) == 101) )
         {
            if ( ! ( (0==A358FuncaoAPF_FunAPFPaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Funcao APF_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPF_FUNAPFPAICOD");
               AnyError = 1;
               GX_FocusControl = dynFuncaoAPF_FunAPFPaiCod_Internalname;
            }
         }
         A363FuncaoAPF_FunAPFPaiNom = T000Z35_A363FuncaoAPF_FunAPFPaiNom[0];
         n363FuncaoAPF_FunAPFPaiNom = T000Z35_n363FuncaoAPF_FunAPFPaiNom[0];
         pr_default.close(33);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A363FuncaoAPF_FunAPFPaiNom = "";
            n363FuncaoAPF_FunAPFPaiNom = false;
         }
         isValidOutput.Add(A363FuncaoAPF_FunAPFPaiNom);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120Z2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOELIMINAR'","{handler:'E130Z2',iparms:[{av:'AV7FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(32);
         pr_default.close(17);
         pr_default.close(33);
         pr_default.close(18);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z166FuncaoAPF_Nome = "";
         Z184FuncaoAPF_Tipo = "";
         Z432FuncaoAPF_Link = "";
         Z1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         Z183FuncaoAPF_Ativo = "";
         O166FuncaoAPF_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         A183FuncaoAPF_Ativo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtneliminar_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockfuncaoapf_modulocod_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         A166FuncaoAPF_Nome = "";
         lblTextblockfuncaoapf_descricao_Jsonclick = "";
         A167FuncaoAPF_Descricao = "";
         lblTextblockfuncaoapf_link_Jsonclick = "";
         lblTextblockfuncaoapf_funapfpaicod_Jsonclick = "";
         lblTextblockfuncaoapf_td_Jsonclick = "";
         lblTextblockfuncaoapf_observacao_Jsonclick = "";
         lblTextblockfuncaoapf_parecerse_Jsonclick = "";
         A1233FuncaoAPF_ParecerSE = "";
         lblTextblockfuncaoapf_ativo_Jsonclick = "";
         lblTextblockfuncaoapf_updaoimpbsln_Jsonclick = "";
         lblTextblockfuncaoapf_ar_Jsonclick = "";
         lblTextblockfuncaoapf_complexidade_Jsonclick = "";
         lblTextblockfuncaoapf_pf_Jsonclick = "";
         A432FuncaoAPF_Link = "";
         lblFuncaoapf_link_righttext_Jsonclick = "";
         lblTextblockfuncaoapf_tipo_Jsonclick = "";
         lblTextblockfuncaoapf_acao_Jsonclick = "";
         lblTextblockfuncaoapf_mensagem_Jsonclick = "";
         A1246FuncaoAPF_Importada = (DateTime)(DateTime.MinValue);
         A757FuncaoAPF_SolucaoTecnica = "";
         A362FuncaoAPF_SistemaDes = "";
         A1244FuncaoAPF_Observacao = "";
         A706FuncaoAPF_SistemaTec = "";
         A361FuncaoAPF_ModuloNom = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         AV19Pgmname = "";
         Funcaoapf_observacao_Width = "";
         Funcaoapf_observacao_Height = "";
         Funcaoapf_observacao_Skin = "";
         Funcaoapf_observacao_Toolbar = "";
         Funcaoapf_observacao_Class = "";
         Funcaoapf_observacao_Customtoolbar = "";
         Funcaoapf_observacao_Customconfiguration = "";
         Funcaoapf_observacao_Buttonpressedid = "";
         Funcaoapf_observacao_Captionvalue = "";
         Funcaoapf_observacao_Captionclass = "";
         Funcaoapf_observacao_Captionposition = "";
         Funcaoapf_observacao_Coltitle = "";
         Funcaoapf_observacao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode36 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z167FuncaoAPF_Descricao = "";
         Z1233FuncaoAPF_ParecerSE = "";
         Z1244FuncaoAPF_Observacao = "";
         Z362FuncaoAPF_SistemaDes = "";
         Z706FuncaoAPF_SistemaTec = "";
         Z361FuncaoAPF_ModuloNom = "";
         Z363FuncaoAPF_FunAPFPaiNom = "";
         T000Z5_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         T000Z5_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         T000Z6_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         T000Z6_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         T000Z4_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         T000Z4_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         T000Z4_A706FuncaoAPF_SistemaTec = new String[] {""} ;
         T000Z4_n706FuncaoAPF_SistemaTec = new bool[] {false} ;
         T000Z4_A756FuncaoAPF_SistemaProjCod = new int[1] ;
         T000Z4_n756FuncaoAPF_SistemaProjCod = new bool[] {false} ;
         T000Z8_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z8_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z8_A166FuncaoAPF_Nome = new String[] {""} ;
         T000Z8_A167FuncaoAPF_Descricao = new String[] {""} ;
         T000Z8_A184FuncaoAPF_Tipo = new String[] {""} ;
         T000Z8_A1022FuncaoAPF_DERImp = new short[1] ;
         T000Z8_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         T000Z8_A1026FuncaoAPF_RAImp = new short[1] ;
         T000Z8_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         T000Z8_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         T000Z8_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         T000Z8_A706FuncaoAPF_SistemaTec = new String[] {""} ;
         T000Z8_n706FuncaoAPF_SistemaTec = new bool[] {false} ;
         T000Z8_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         T000Z8_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         T000Z8_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         T000Z8_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         T000Z8_A413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z8_n413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z8_A414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z8_n414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z8_A432FuncaoAPF_Link = new String[] {""} ;
         T000Z8_n432FuncaoAPF_Link = new bool[] {false} ;
         T000Z8_A1146FuncaoAPF_Tecnica = new short[1] ;
         T000Z8_n1146FuncaoAPF_Tecnica = new bool[] {false} ;
         T000Z8_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         T000Z8_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         T000Z8_A1244FuncaoAPF_Observacao = new String[] {""} ;
         T000Z8_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         T000Z8_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         T000Z8_n1246FuncaoAPF_Importada = new bool[] {false} ;
         T000Z8_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z8_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z8_A183FuncaoAPF_Ativo = new String[] {""} ;
         T000Z8_A360FuncaoAPF_SistemaCod = new int[1] ;
         T000Z8_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T000Z8_A359FuncaoAPF_ModuloCod = new int[1] ;
         T000Z8_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         T000Z8_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         T000Z8_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         T000Z8_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z8_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z8_A756FuncaoAPF_SistemaProjCod = new int[1] ;
         T000Z8_n756FuncaoAPF_SistemaProjCod = new bool[] {false} ;
         T000Z7_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z7_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z9_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         T000Z9_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         T000Z10_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         T000Z10_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         T000Z11_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         T000Z11_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         T000Z11_A706FuncaoAPF_SistemaTec = new String[] {""} ;
         T000Z11_n706FuncaoAPF_SistemaTec = new bool[] {false} ;
         T000Z11_A756FuncaoAPF_SistemaProjCod = new int[1] ;
         T000Z11_n756FuncaoAPF_SistemaProjCod = new bool[] {false} ;
         T000Z12_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z12_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z13_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z13_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z3_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z3_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z3_A166FuncaoAPF_Nome = new String[] {""} ;
         T000Z3_A167FuncaoAPF_Descricao = new String[] {""} ;
         T000Z3_A184FuncaoAPF_Tipo = new String[] {""} ;
         T000Z3_A1022FuncaoAPF_DERImp = new short[1] ;
         T000Z3_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         T000Z3_A1026FuncaoAPF_RAImp = new short[1] ;
         T000Z3_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         T000Z3_A413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z3_n413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z3_A414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z3_n414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z3_A432FuncaoAPF_Link = new String[] {""} ;
         T000Z3_n432FuncaoAPF_Link = new bool[] {false} ;
         T000Z3_A1146FuncaoAPF_Tecnica = new short[1] ;
         T000Z3_n1146FuncaoAPF_Tecnica = new bool[] {false} ;
         T000Z3_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         T000Z3_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         T000Z3_A1244FuncaoAPF_Observacao = new String[] {""} ;
         T000Z3_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         T000Z3_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         T000Z3_n1246FuncaoAPF_Importada = new bool[] {false} ;
         T000Z3_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z3_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z3_A183FuncaoAPF_Ativo = new String[] {""} ;
         T000Z3_A360FuncaoAPF_SistemaCod = new int[1] ;
         T000Z3_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T000Z3_A359FuncaoAPF_ModuloCod = new int[1] ;
         T000Z3_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         T000Z3_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         T000Z3_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         T000Z3_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z3_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z14_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z14_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z15_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z15_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z2_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z2_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z2_A166FuncaoAPF_Nome = new String[] {""} ;
         T000Z2_A167FuncaoAPF_Descricao = new String[] {""} ;
         T000Z2_A184FuncaoAPF_Tipo = new String[] {""} ;
         T000Z2_A1022FuncaoAPF_DERImp = new short[1] ;
         T000Z2_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         T000Z2_A1026FuncaoAPF_RAImp = new short[1] ;
         T000Z2_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         T000Z2_A413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z2_n413FuncaoAPF_Acao = new bool[] {false} ;
         T000Z2_A414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z2_n414FuncaoAPF_Mensagem = new bool[] {false} ;
         T000Z2_A432FuncaoAPF_Link = new String[] {""} ;
         T000Z2_n432FuncaoAPF_Link = new bool[] {false} ;
         T000Z2_A1146FuncaoAPF_Tecnica = new short[1] ;
         T000Z2_n1146FuncaoAPF_Tecnica = new bool[] {false} ;
         T000Z2_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         T000Z2_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         T000Z2_A1244FuncaoAPF_Observacao = new String[] {""} ;
         T000Z2_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         T000Z2_A1246FuncaoAPF_Importada = new DateTime[] {DateTime.MinValue} ;
         T000Z2_n1246FuncaoAPF_Importada = new bool[] {false} ;
         T000Z2_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z2_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         T000Z2_A183FuncaoAPF_Ativo = new String[] {""} ;
         T000Z2_A360FuncaoAPF_SistemaCod = new int[1] ;
         T000Z2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         T000Z2_A359FuncaoAPF_ModuloCod = new int[1] ;
         T000Z2_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         T000Z2_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         T000Z2_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         T000Z2_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z2_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z16_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z16_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z19_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         T000Z19_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         T000Z20_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         T000Z20_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         T000Z21_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         T000Z21_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         T000Z21_A706FuncaoAPF_SistemaTec = new String[] {""} ;
         T000Z21_n706FuncaoAPF_SistemaTec = new bool[] {false} ;
         T000Z21_A756FuncaoAPF_SistemaProjCod = new int[1] ;
         T000Z21_n756FuncaoAPF_SistemaProjCod = new bool[] {false} ;
         T000Z22_A744FuncaoAPF_MelhoraCod = new int[1] ;
         T000Z22_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         T000Z23_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         T000Z23_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         T000Z24_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T000Z24_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T000Z25_A996SolicitacaoMudanca_Codigo = new int[1] ;
         T000Z25_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         T000Z26_A736ProjetoMelhoria_Codigo = new int[1] ;
         T000Z27_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         T000Z28_A224ContagemItem_Lancamento = new int[1] ;
         T000Z29_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z29_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z29_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T000Z30_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Z30_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z30_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z31_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z31_n165FuncaoAPF_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i183FuncaoAPF_Ativo = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000Z32_A146Modulo_Codigo = new int[1] ;
         T000Z32_A143Modulo_Nome = new String[] {""} ;
         T000Z32_A127Sistema_Codigo = new int[1] ;
         T000Z33_A165FuncaoAPF_Codigo = new int[1] ;
         T000Z33_n165FuncaoAPF_Codigo = new bool[] {false} ;
         T000Z33_A166FuncaoAPF_Nome = new String[] {""} ;
         GXt_char4 = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000Z34_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         T000Z34_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         T000Z35_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         T000Z35_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapf__default(),
            new Object[][] {
                new Object[] {
               T000Z2_A165FuncaoAPF_Codigo, T000Z2_A166FuncaoAPF_Nome, T000Z2_A167FuncaoAPF_Descricao, T000Z2_A184FuncaoAPF_Tipo, T000Z2_A1022FuncaoAPF_DERImp, T000Z2_n1022FuncaoAPF_DERImp, T000Z2_A1026FuncaoAPF_RAImp, T000Z2_n1026FuncaoAPF_RAImp, T000Z2_A413FuncaoAPF_Acao, T000Z2_n413FuncaoAPF_Acao,
               T000Z2_A414FuncaoAPF_Mensagem, T000Z2_n414FuncaoAPF_Mensagem, T000Z2_A432FuncaoAPF_Link, T000Z2_n432FuncaoAPF_Link, T000Z2_A1146FuncaoAPF_Tecnica, T000Z2_n1146FuncaoAPF_Tecnica, T000Z2_A1233FuncaoAPF_ParecerSE, T000Z2_n1233FuncaoAPF_ParecerSE, T000Z2_A1244FuncaoAPF_Observacao, T000Z2_n1244FuncaoAPF_Observacao,
               T000Z2_A1246FuncaoAPF_Importada, T000Z2_n1246FuncaoAPF_Importada, T000Z2_A1268FuncaoAPF_UpdAoImpBsln, T000Z2_n1268FuncaoAPF_UpdAoImpBsln, T000Z2_A183FuncaoAPF_Ativo, T000Z2_A360FuncaoAPF_SistemaCod, T000Z2_n360FuncaoAPF_SistemaCod, T000Z2_A359FuncaoAPF_ModuloCod, T000Z2_n359FuncaoAPF_ModuloCod, T000Z2_A358FuncaoAPF_FunAPFPaiCod,
               T000Z2_n358FuncaoAPF_FunAPFPaiCod, T000Z2_A744FuncaoAPF_MelhoraCod, T000Z2_n744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               T000Z3_A165FuncaoAPF_Codigo, T000Z3_A166FuncaoAPF_Nome, T000Z3_A167FuncaoAPF_Descricao, T000Z3_A184FuncaoAPF_Tipo, T000Z3_A1022FuncaoAPF_DERImp, T000Z3_n1022FuncaoAPF_DERImp, T000Z3_A1026FuncaoAPF_RAImp, T000Z3_n1026FuncaoAPF_RAImp, T000Z3_A413FuncaoAPF_Acao, T000Z3_n413FuncaoAPF_Acao,
               T000Z3_A414FuncaoAPF_Mensagem, T000Z3_n414FuncaoAPF_Mensagem, T000Z3_A432FuncaoAPF_Link, T000Z3_n432FuncaoAPF_Link, T000Z3_A1146FuncaoAPF_Tecnica, T000Z3_n1146FuncaoAPF_Tecnica, T000Z3_A1233FuncaoAPF_ParecerSE, T000Z3_n1233FuncaoAPF_ParecerSE, T000Z3_A1244FuncaoAPF_Observacao, T000Z3_n1244FuncaoAPF_Observacao,
               T000Z3_A1246FuncaoAPF_Importada, T000Z3_n1246FuncaoAPF_Importada, T000Z3_A1268FuncaoAPF_UpdAoImpBsln, T000Z3_n1268FuncaoAPF_UpdAoImpBsln, T000Z3_A183FuncaoAPF_Ativo, T000Z3_A360FuncaoAPF_SistemaCod, T000Z3_n360FuncaoAPF_SistemaCod, T000Z3_A359FuncaoAPF_ModuloCod, T000Z3_n359FuncaoAPF_ModuloCod, T000Z3_A358FuncaoAPF_FunAPFPaiCod,
               T000Z3_n358FuncaoAPF_FunAPFPaiCod, T000Z3_A744FuncaoAPF_MelhoraCod, T000Z3_n744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               T000Z4_A362FuncaoAPF_SistemaDes, T000Z4_n362FuncaoAPF_SistemaDes, T000Z4_A706FuncaoAPF_SistemaTec, T000Z4_n706FuncaoAPF_SistemaTec, T000Z4_A756FuncaoAPF_SistemaProjCod, T000Z4_n756FuncaoAPF_SistemaProjCod
               }
               , new Object[] {
               T000Z5_A361FuncaoAPF_ModuloNom, T000Z5_n361FuncaoAPF_ModuloNom
               }
               , new Object[] {
               T000Z6_A363FuncaoAPF_FunAPFPaiNom, T000Z6_n363FuncaoAPF_FunAPFPaiNom
               }
               , new Object[] {
               T000Z7_A744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               T000Z8_A165FuncaoAPF_Codigo, T000Z8_A166FuncaoAPF_Nome, T000Z8_A167FuncaoAPF_Descricao, T000Z8_A184FuncaoAPF_Tipo, T000Z8_A1022FuncaoAPF_DERImp, T000Z8_n1022FuncaoAPF_DERImp, T000Z8_A1026FuncaoAPF_RAImp, T000Z8_n1026FuncaoAPF_RAImp, T000Z8_A362FuncaoAPF_SistemaDes, T000Z8_n362FuncaoAPF_SistemaDes,
               T000Z8_A706FuncaoAPF_SistemaTec, T000Z8_n706FuncaoAPF_SistemaTec, T000Z8_A361FuncaoAPF_ModuloNom, T000Z8_n361FuncaoAPF_ModuloNom, T000Z8_A363FuncaoAPF_FunAPFPaiNom, T000Z8_n363FuncaoAPF_FunAPFPaiNom, T000Z8_A413FuncaoAPF_Acao, T000Z8_n413FuncaoAPF_Acao, T000Z8_A414FuncaoAPF_Mensagem, T000Z8_n414FuncaoAPF_Mensagem,
               T000Z8_A432FuncaoAPF_Link, T000Z8_n432FuncaoAPF_Link, T000Z8_A1146FuncaoAPF_Tecnica, T000Z8_n1146FuncaoAPF_Tecnica, T000Z8_A1233FuncaoAPF_ParecerSE, T000Z8_n1233FuncaoAPF_ParecerSE, T000Z8_A1244FuncaoAPF_Observacao, T000Z8_n1244FuncaoAPF_Observacao, T000Z8_A1246FuncaoAPF_Importada, T000Z8_n1246FuncaoAPF_Importada,
               T000Z8_A1268FuncaoAPF_UpdAoImpBsln, T000Z8_n1268FuncaoAPF_UpdAoImpBsln, T000Z8_A183FuncaoAPF_Ativo, T000Z8_A360FuncaoAPF_SistemaCod, T000Z8_n360FuncaoAPF_SistemaCod, T000Z8_A359FuncaoAPF_ModuloCod, T000Z8_n359FuncaoAPF_ModuloCod, T000Z8_A358FuncaoAPF_FunAPFPaiCod, T000Z8_n358FuncaoAPF_FunAPFPaiCod, T000Z8_A744FuncaoAPF_MelhoraCod,
               T000Z8_n744FuncaoAPF_MelhoraCod, T000Z8_A756FuncaoAPF_SistemaProjCod, T000Z8_n756FuncaoAPF_SistemaProjCod
               }
               , new Object[] {
               T000Z9_A361FuncaoAPF_ModuloNom, T000Z9_n361FuncaoAPF_ModuloNom
               }
               , new Object[] {
               T000Z10_A363FuncaoAPF_FunAPFPaiNom, T000Z10_n363FuncaoAPF_FunAPFPaiNom
               }
               , new Object[] {
               T000Z11_A362FuncaoAPF_SistemaDes, T000Z11_n362FuncaoAPF_SistemaDes, T000Z11_A706FuncaoAPF_SistemaTec, T000Z11_n706FuncaoAPF_SistemaTec, T000Z11_A756FuncaoAPF_SistemaProjCod, T000Z11_n756FuncaoAPF_SistemaProjCod
               }
               , new Object[] {
               T000Z12_A744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               T000Z13_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Z14_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Z15_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Z16_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000Z19_A361FuncaoAPF_ModuloNom, T000Z19_n361FuncaoAPF_ModuloNom
               }
               , new Object[] {
               T000Z20_A363FuncaoAPF_FunAPFPaiNom, T000Z20_n363FuncaoAPF_FunAPFPaiNom
               }
               , new Object[] {
               T000Z21_A362FuncaoAPF_SistemaDes, T000Z21_n362FuncaoAPF_SistemaDes, T000Z21_A706FuncaoAPF_SistemaTec, T000Z21_n706FuncaoAPF_SistemaTec, T000Z21_A756FuncaoAPF_SistemaProjCod, T000Z21_n756FuncaoAPF_SistemaProjCod
               }
               , new Object[] {
               T000Z22_A744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               T000Z23_A358FuncaoAPF_FunAPFPaiCod
               }
               , new Object[] {
               T000Z24_A1260FuncaoAPFPreImp_FnApfCodigo, T000Z24_A1257FuncaoAPFPreImp_Sequencial
               }
               , new Object[] {
               T000Z25_A996SolicitacaoMudanca_Codigo, T000Z25_A995SolicitacaoMudancaItem_FuncaoAPF
               }
               , new Object[] {
               T000Z26_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T000Z27_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               T000Z28_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T000Z29_A165FuncaoAPF_Codigo, T000Z29_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T000Z30_A161FuncaoUsuario_Codigo, T000Z30_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Z31_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Z32_A146Modulo_Codigo, T000Z32_A143Modulo_Nome, T000Z32_A127Sistema_Codigo
               }
               , new Object[] {
               T000Z33_A165FuncaoAPF_Codigo, T000Z33_A166FuncaoAPF_Nome
               }
               , new Object[] {
               T000Z34_A361FuncaoAPF_ModuloNom, T000Z34_n361FuncaoAPF_ModuloNom
               }
               , new Object[] {
               T000Z35_A363FuncaoAPF_FunAPFPaiNom, T000Z35_n363FuncaoAPF_FunAPFPaiNom
               }
            }
         );
         Z183FuncaoAPF_Ativo = "A";
         A183FuncaoAPF_Ativo = "A";
         i183FuncaoAPF_Ativo = "A";
         Z1268FuncaoAPF_UpdAoImpBsln = true;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         A1268FuncaoAPF_UpdAoImpBsln = true;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         i1268FuncaoAPF_UpdAoImpBsln = true;
         n1268FuncaoAPF_UpdAoImpBsln = false;
         AV19Pgmname = "FuncaoAPF";
      }

      private short Z1022FuncaoAPF_DERImp ;
      private short Z1026FuncaoAPF_RAImp ;
      private short Z1146FuncaoAPF_Tecnica ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short A1022FuncaoAPF_DERImp ;
      private short A1026FuncaoAPF_RAImp ;
      private short A1146FuncaoAPF_Tecnica ;
      private short Gx_BScreen ;
      private short RcdFound36 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short GXt_int2 ;
      private short wbTemp ;
      private int wcpOAV7FuncaoAPF_Codigo ;
      private int wcpOAV15FuncaoAPF_SistemaCod ;
      private int Z165FuncaoAPF_Codigo ;
      private int Z360FuncaoAPF_SistemaCod ;
      private int Z359FuncaoAPF_ModuloCod ;
      private int Z358FuncaoAPF_FunAPFPaiCod ;
      private int Z744FuncaoAPF_MelhoraCod ;
      private int N360FuncaoAPF_SistemaCod ;
      private int N359FuncaoAPF_ModuloCod ;
      private int N358FuncaoAPF_FunAPFPaiCod ;
      private int N744FuncaoAPF_MelhoraCod ;
      private int AV15FuncaoAPF_SistemaCod ;
      private int AV7FuncaoAPF_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A744FuncaoAPF_MelhoraCod ;
      private int A756FuncaoAPF_SistemaProjCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int trnEnded ;
      private int edtFuncaoAPF_Codigo_Enabled ;
      private int edtFuncaoAPF_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtneliminar_Visible ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtFuncaoAPF_Nome_Enabled ;
      private int edtFuncaoAPF_Descricao_Enabled ;
      private int edtFuncaoAPF_ParecerSE_Enabled ;
      private int lblTextblockfuncaoapf_ativo_Visible ;
      private int edtFuncaoAPF_TD_Enabled ;
      private int edtFuncaoAPF_AR_Enabled ;
      private int edtFuncaoAPF_PF_Enabled ;
      private int edtFuncaoAPF_Link_Enabled ;
      private int AV11Insert_FuncaoAPF_SistemaCod ;
      private int AV12Insert_FuncaoAPF_ModuloCod ;
      private int AV13Insert_FuncaoAPF_FunAPFPaiCod ;
      private int AV18Insert_FuncaoAPF_MelhoraCod ;
      private int Funcaoapf_observacao_Color ;
      private int Funcaoapf_observacao_Coltitlecolor ;
      private int AV20GXV1 ;
      private int edtFuncaoAPF_Nome_Width ;
      private int edtFuncaoAPF_Nome_Height ;
      private int Z756FuncaoAPF_SistemaProjCod ;
      private int i360FuncaoAPF_SistemaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z184FuncaoAPF_Tipo ;
      private String Z183FuncaoAPF_Ativo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A184FuncaoAPF_Tipo ;
      private String chkFuncaoAPF_Acao_Internalname ;
      private String chkFuncaoAPF_Mensagem_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String A183FuncaoAPF_Ativo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynFuncaoAPF_ModuloCod_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtneliminar_Internalname ;
      private String bttBtneliminar_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapf_modulocod_Internalname ;
      private String lblTextblockfuncaoapf_modulocod_Jsonclick ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String lblTextblockfuncaoapf_descricao_Internalname ;
      private String lblTextblockfuncaoapf_descricao_Jsonclick ;
      private String edtFuncaoAPF_Descricao_Internalname ;
      private String lblTextblockfuncaoapf_link_Internalname ;
      private String lblTextblockfuncaoapf_link_Jsonclick ;
      private String lblTextblockfuncaoapf_funapfpaicod_Internalname ;
      private String lblTextblockfuncaoapf_funapfpaicod_Jsonclick ;
      private String dynFuncaoAPF_FunAPFPaiCod_Internalname ;
      private String dynFuncaoAPF_FunAPFPaiCod_Jsonclick ;
      private String lblTextblockfuncaoapf_td_Internalname ;
      private String lblTextblockfuncaoapf_td_Jsonclick ;
      private String lblTextblockfuncaoapf_observacao_Internalname ;
      private String lblTextblockfuncaoapf_observacao_Jsonclick ;
      private String lblTextblockfuncaoapf_parecerse_Internalname ;
      private String lblTextblockfuncaoapf_parecerse_Jsonclick ;
      private String edtFuncaoAPF_ParecerSE_Internalname ;
      private String lblTextblockfuncaoapf_ativo_Internalname ;
      private String lblTextblockfuncaoapf_ativo_Jsonclick ;
      private String tblTablemergedfuncaoapf_ativo_Internalname ;
      private String cmbFuncaoAPF_Ativo_Internalname ;
      private String cmbFuncaoAPF_Ativo_Jsonclick ;
      private String lblTextblockfuncaoapf_updaoimpbsln_Internalname ;
      private String lblTextblockfuncaoapf_updaoimpbsln_Jsonclick ;
      private String cmbFuncaoAPF_UpdAoImpBsln_Internalname ;
      private String cmbFuncaoAPF_UpdAoImpBsln_Jsonclick ;
      private String tblTablemergedfuncaoapf_td_Internalname ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String lblTextblockfuncaoapf_ar_Internalname ;
      private String lblTextblockfuncaoapf_ar_Jsonclick ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String lblTextblockfuncaoapf_complexidade_Internalname ;
      private String lblTextblockfuncaoapf_complexidade_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String lblTextblockfuncaoapf_pf_Internalname ;
      private String lblTextblockfuncaoapf_pf_Jsonclick ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String tblTablemergedfuncaoapf_link_Internalname ;
      private String edtFuncaoAPF_Link_Internalname ;
      private String lblFuncaoapf_link_righttext_Internalname ;
      private String lblFuncaoapf_link_righttext_Jsonclick ;
      private String tblTablemergedfuncaoapf_modulocod_Internalname ;
      private String dynFuncaoAPF_ModuloCod_Jsonclick ;
      private String lblTextblockfuncaoapf_tipo_Internalname ;
      private String lblTextblockfuncaoapf_tipo_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String lblTextblockfuncaoapf_acao_Internalname ;
      private String lblTextblockfuncaoapf_acao_Jsonclick ;
      private String lblTextblockfuncaoapf_mensagem_Internalname ;
      private String lblTextblockfuncaoapf_mensagem_Jsonclick ;
      private String A706FuncaoAPF_SistemaTec ;
      private String A361FuncaoAPF_ModuloNom ;
      private String AV19Pgmname ;
      private String Funcaoapf_observacao_Width ;
      private String Funcaoapf_observacao_Height ;
      private String Funcaoapf_observacao_Skin ;
      private String Funcaoapf_observacao_Toolbar ;
      private String Funcaoapf_observacao_Class ;
      private String Funcaoapf_observacao_Customtoolbar ;
      private String Funcaoapf_observacao_Customconfiguration ;
      private String Funcaoapf_observacao_Buttonpressedid ;
      private String Funcaoapf_observacao_Captionvalue ;
      private String Funcaoapf_observacao_Captionclass ;
      private String Funcaoapf_observacao_Captionposition ;
      private String Funcaoapf_observacao_Coltitle ;
      private String Funcaoapf_observacao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode36 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z706FuncaoAPF_SistemaTec ;
      private String Z361FuncaoAPF_ModuloNom ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Funcaoapf_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i183FuncaoAPF_Ativo ;
      private String gxwrpcisep ;
      private String GXt_char4 ;
      private DateTime Z1246FuncaoAPF_Importada ;
      private DateTime A1246FuncaoAPF_Importada ;
      private bool Z413FuncaoAPF_Acao ;
      private bool Z414FuncaoAPF_Mensagem ;
      private bool Z1268FuncaoAPF_UpdAoImpBsln ;
      private bool entryPointCalled ;
      private bool n165FuncaoAPF_Codigo ;
      private bool n744FuncaoAPF_MelhoraCod ;
      private bool n756FuncaoAPF_SistemaProjCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool A1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1268FuncaoAPF_UpdAoImpBsln ;
      private bool wbErr ;
      private bool A413FuncaoAPF_Acao ;
      private bool A414FuncaoAPF_Mensagem ;
      private bool n413FuncaoAPF_Acao ;
      private bool n414FuncaoAPF_Mensagem ;
      private bool n432FuncaoAPF_Link ;
      private bool n1233FuncaoAPF_ParecerSE ;
      private bool n1022FuncaoAPF_DERImp ;
      private bool n1026FuncaoAPF_RAImp ;
      private bool n1146FuncaoAPF_Tecnica ;
      private bool n1246FuncaoAPF_Importada ;
      private bool n362FuncaoAPF_SistemaDes ;
      private bool n1244FuncaoAPF_Observacao ;
      private bool n706FuncaoAPF_SistemaTec ;
      private bool n361FuncaoAPF_ModuloNom ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool Funcaoapf_observacao_Enabled ;
      private bool Funcaoapf_observacao_Toolbarcancollapse ;
      private bool Funcaoapf_observacao_Toolbarexpanded ;
      private bool Funcaoapf_observacao_Usercontroliscolumn ;
      private bool Funcaoapf_observacao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool GXt_boolean1 ;
      private bool Gx_longc ;
      private bool i1268FuncaoAPF_UpdAoImpBsln ;
      private String A167FuncaoAPF_Descricao ;
      private String A1233FuncaoAPF_ParecerSE ;
      private String A757FuncaoAPF_SolucaoTecnica ;
      private String A1244FuncaoAPF_Observacao ;
      private String Z167FuncaoAPF_Descricao ;
      private String Z1233FuncaoAPF_ParecerSE ;
      private String Z1244FuncaoAPF_Observacao ;
      private String Z166FuncaoAPF_Nome ;
      private String Z432FuncaoAPF_Link ;
      private String O166FuncaoAPF_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String A432FuncaoAPF_Link ;
      private String A362FuncaoAPF_SistemaDes ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String Z362FuncaoAPF_SistemaDes ;
      private String Z363FuncaoAPF_FunAPFPaiNom ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_FuncaoAPF_SistemaCod ;
      private GXCombobox dynFuncaoAPF_ModuloCod ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCheckbox chkFuncaoAPF_Acao ;
      private GXCheckbox chkFuncaoAPF_Mensagem ;
      private GXCombobox dynFuncaoAPF_FunAPFPaiCod ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbFuncaoAPF_Ativo ;
      private GXCombobox cmbFuncaoAPF_UpdAoImpBsln ;
      private IDataStoreProvider pr_default ;
      private String[] T000Z5_A361FuncaoAPF_ModuloNom ;
      private bool[] T000Z5_n361FuncaoAPF_ModuloNom ;
      private String[] T000Z6_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z6_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] T000Z4_A362FuncaoAPF_SistemaDes ;
      private bool[] T000Z4_n362FuncaoAPF_SistemaDes ;
      private String[] T000Z4_A706FuncaoAPF_SistemaTec ;
      private bool[] T000Z4_n706FuncaoAPF_SistemaTec ;
      private int[] T000Z4_A756FuncaoAPF_SistemaProjCod ;
      private bool[] T000Z4_n756FuncaoAPF_SistemaProjCod ;
      private int[] T000Z8_A165FuncaoAPF_Codigo ;
      private bool[] T000Z8_n165FuncaoAPF_Codigo ;
      private String[] T000Z8_A166FuncaoAPF_Nome ;
      private String[] T000Z8_A167FuncaoAPF_Descricao ;
      private String[] T000Z8_A184FuncaoAPF_Tipo ;
      private short[] T000Z8_A1022FuncaoAPF_DERImp ;
      private bool[] T000Z8_n1022FuncaoAPF_DERImp ;
      private short[] T000Z8_A1026FuncaoAPF_RAImp ;
      private bool[] T000Z8_n1026FuncaoAPF_RAImp ;
      private String[] T000Z8_A362FuncaoAPF_SistemaDes ;
      private bool[] T000Z8_n362FuncaoAPF_SistemaDes ;
      private String[] T000Z8_A706FuncaoAPF_SistemaTec ;
      private bool[] T000Z8_n706FuncaoAPF_SistemaTec ;
      private String[] T000Z8_A361FuncaoAPF_ModuloNom ;
      private bool[] T000Z8_n361FuncaoAPF_ModuloNom ;
      private String[] T000Z8_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z8_n363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z8_A413FuncaoAPF_Acao ;
      private bool[] T000Z8_n413FuncaoAPF_Acao ;
      private bool[] T000Z8_A414FuncaoAPF_Mensagem ;
      private bool[] T000Z8_n414FuncaoAPF_Mensagem ;
      private String[] T000Z8_A432FuncaoAPF_Link ;
      private bool[] T000Z8_n432FuncaoAPF_Link ;
      private short[] T000Z8_A1146FuncaoAPF_Tecnica ;
      private bool[] T000Z8_n1146FuncaoAPF_Tecnica ;
      private String[] T000Z8_A1233FuncaoAPF_ParecerSE ;
      private bool[] T000Z8_n1233FuncaoAPF_ParecerSE ;
      private String[] T000Z8_A1244FuncaoAPF_Observacao ;
      private bool[] T000Z8_n1244FuncaoAPF_Observacao ;
      private DateTime[] T000Z8_A1246FuncaoAPF_Importada ;
      private bool[] T000Z8_n1246FuncaoAPF_Importada ;
      private bool[] T000Z8_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] T000Z8_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] T000Z8_A183FuncaoAPF_Ativo ;
      private int[] T000Z8_A360FuncaoAPF_SistemaCod ;
      private bool[] T000Z8_n360FuncaoAPF_SistemaCod ;
      private int[] T000Z8_A359FuncaoAPF_ModuloCod ;
      private bool[] T000Z8_n359FuncaoAPF_ModuloCod ;
      private int[] T000Z8_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] T000Z8_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] T000Z8_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z8_n744FuncaoAPF_MelhoraCod ;
      private int[] T000Z8_A756FuncaoAPF_SistemaProjCod ;
      private bool[] T000Z8_n756FuncaoAPF_SistemaProjCod ;
      private int[] T000Z7_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z7_n744FuncaoAPF_MelhoraCod ;
      private String[] T000Z9_A361FuncaoAPF_ModuloNom ;
      private bool[] T000Z9_n361FuncaoAPF_ModuloNom ;
      private String[] T000Z10_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z10_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] T000Z11_A362FuncaoAPF_SistemaDes ;
      private bool[] T000Z11_n362FuncaoAPF_SistemaDes ;
      private String[] T000Z11_A706FuncaoAPF_SistemaTec ;
      private bool[] T000Z11_n706FuncaoAPF_SistemaTec ;
      private int[] T000Z11_A756FuncaoAPF_SistemaProjCod ;
      private bool[] T000Z11_n756FuncaoAPF_SistemaProjCod ;
      private int[] T000Z12_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z12_n744FuncaoAPF_MelhoraCod ;
      private int[] T000Z13_A165FuncaoAPF_Codigo ;
      private bool[] T000Z13_n165FuncaoAPF_Codigo ;
      private int[] T000Z3_A165FuncaoAPF_Codigo ;
      private bool[] T000Z3_n165FuncaoAPF_Codigo ;
      private String[] T000Z3_A166FuncaoAPF_Nome ;
      private String[] T000Z3_A167FuncaoAPF_Descricao ;
      private String[] T000Z3_A184FuncaoAPF_Tipo ;
      private short[] T000Z3_A1022FuncaoAPF_DERImp ;
      private bool[] T000Z3_n1022FuncaoAPF_DERImp ;
      private short[] T000Z3_A1026FuncaoAPF_RAImp ;
      private bool[] T000Z3_n1026FuncaoAPF_RAImp ;
      private bool[] T000Z3_A413FuncaoAPF_Acao ;
      private bool[] T000Z3_n413FuncaoAPF_Acao ;
      private bool[] T000Z3_A414FuncaoAPF_Mensagem ;
      private bool[] T000Z3_n414FuncaoAPF_Mensagem ;
      private String[] T000Z3_A432FuncaoAPF_Link ;
      private bool[] T000Z3_n432FuncaoAPF_Link ;
      private short[] T000Z3_A1146FuncaoAPF_Tecnica ;
      private bool[] T000Z3_n1146FuncaoAPF_Tecnica ;
      private String[] T000Z3_A1233FuncaoAPF_ParecerSE ;
      private bool[] T000Z3_n1233FuncaoAPF_ParecerSE ;
      private String[] T000Z3_A1244FuncaoAPF_Observacao ;
      private bool[] T000Z3_n1244FuncaoAPF_Observacao ;
      private DateTime[] T000Z3_A1246FuncaoAPF_Importada ;
      private bool[] T000Z3_n1246FuncaoAPF_Importada ;
      private bool[] T000Z3_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] T000Z3_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] T000Z3_A183FuncaoAPF_Ativo ;
      private int[] T000Z3_A360FuncaoAPF_SistemaCod ;
      private bool[] T000Z3_n360FuncaoAPF_SistemaCod ;
      private int[] T000Z3_A359FuncaoAPF_ModuloCod ;
      private bool[] T000Z3_n359FuncaoAPF_ModuloCod ;
      private int[] T000Z3_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] T000Z3_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] T000Z3_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z3_n744FuncaoAPF_MelhoraCod ;
      private int[] T000Z14_A165FuncaoAPF_Codigo ;
      private bool[] T000Z14_n165FuncaoAPF_Codigo ;
      private int[] T000Z15_A165FuncaoAPF_Codigo ;
      private bool[] T000Z15_n165FuncaoAPF_Codigo ;
      private int[] T000Z2_A165FuncaoAPF_Codigo ;
      private bool[] T000Z2_n165FuncaoAPF_Codigo ;
      private String[] T000Z2_A166FuncaoAPF_Nome ;
      private String[] T000Z2_A167FuncaoAPF_Descricao ;
      private String[] T000Z2_A184FuncaoAPF_Tipo ;
      private short[] T000Z2_A1022FuncaoAPF_DERImp ;
      private bool[] T000Z2_n1022FuncaoAPF_DERImp ;
      private short[] T000Z2_A1026FuncaoAPF_RAImp ;
      private bool[] T000Z2_n1026FuncaoAPF_RAImp ;
      private bool[] T000Z2_A413FuncaoAPF_Acao ;
      private bool[] T000Z2_n413FuncaoAPF_Acao ;
      private bool[] T000Z2_A414FuncaoAPF_Mensagem ;
      private bool[] T000Z2_n414FuncaoAPF_Mensagem ;
      private String[] T000Z2_A432FuncaoAPF_Link ;
      private bool[] T000Z2_n432FuncaoAPF_Link ;
      private short[] T000Z2_A1146FuncaoAPF_Tecnica ;
      private bool[] T000Z2_n1146FuncaoAPF_Tecnica ;
      private String[] T000Z2_A1233FuncaoAPF_ParecerSE ;
      private bool[] T000Z2_n1233FuncaoAPF_ParecerSE ;
      private String[] T000Z2_A1244FuncaoAPF_Observacao ;
      private bool[] T000Z2_n1244FuncaoAPF_Observacao ;
      private DateTime[] T000Z2_A1246FuncaoAPF_Importada ;
      private bool[] T000Z2_n1246FuncaoAPF_Importada ;
      private bool[] T000Z2_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] T000Z2_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] T000Z2_A183FuncaoAPF_Ativo ;
      private int[] T000Z2_A360FuncaoAPF_SistemaCod ;
      private bool[] T000Z2_n360FuncaoAPF_SistemaCod ;
      private int[] T000Z2_A359FuncaoAPF_ModuloCod ;
      private bool[] T000Z2_n359FuncaoAPF_ModuloCod ;
      private int[] T000Z2_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] T000Z2_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] T000Z2_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z2_n744FuncaoAPF_MelhoraCod ;
      private int[] T000Z16_A165FuncaoAPF_Codigo ;
      private bool[] T000Z16_n165FuncaoAPF_Codigo ;
      private String[] T000Z19_A361FuncaoAPF_ModuloNom ;
      private bool[] T000Z19_n361FuncaoAPF_ModuloNom ;
      private String[] T000Z20_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z20_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] T000Z21_A362FuncaoAPF_SistemaDes ;
      private bool[] T000Z21_n362FuncaoAPF_SistemaDes ;
      private String[] T000Z21_A706FuncaoAPF_SistemaTec ;
      private bool[] T000Z21_n706FuncaoAPF_SistemaTec ;
      private int[] T000Z21_A756FuncaoAPF_SistemaProjCod ;
      private bool[] T000Z21_n756FuncaoAPF_SistemaProjCod ;
      private int[] T000Z22_A744FuncaoAPF_MelhoraCod ;
      private bool[] T000Z22_n744FuncaoAPF_MelhoraCod ;
      private int[] T000Z23_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] T000Z23_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] T000Z24_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private short[] T000Z24_A1257FuncaoAPFPreImp_Sequencial ;
      private int[] T000Z25_A996SolicitacaoMudanca_Codigo ;
      private int[] T000Z25_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] T000Z26_A736ProjetoMelhoria_Codigo ;
      private int[] T000Z27_A406FuncaoAPFEvidencia_Codigo ;
      private int[] T000Z28_A224ContagemItem_Lancamento ;
      private int[] T000Z29_A165FuncaoAPF_Codigo ;
      private bool[] T000Z29_n165FuncaoAPF_Codigo ;
      private int[] T000Z29_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T000Z30_A161FuncaoUsuario_Codigo ;
      private int[] T000Z30_A165FuncaoAPF_Codigo ;
      private bool[] T000Z30_n165FuncaoAPF_Codigo ;
      private int[] T000Z31_A165FuncaoAPF_Codigo ;
      private bool[] T000Z31_n165FuncaoAPF_Codigo ;
      private int[] T000Z32_A146Modulo_Codigo ;
      private String[] T000Z32_A143Modulo_Nome ;
      private int[] T000Z32_A127Sistema_Codigo ;
      private int[] T000Z33_A165FuncaoAPF_Codigo ;
      private bool[] T000Z33_n165FuncaoAPF_Codigo ;
      private String[] T000Z33_A166FuncaoAPF_Nome ;
      private String[] T000Z34_A361FuncaoAPF_ModuloNom ;
      private bool[] T000Z34_n361FuncaoAPF_ModuloNom ;
      private String[] T000Z35_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] T000Z35_n363FuncaoAPF_FunAPFPaiNom ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class funcaoapf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000Z8 ;
          prmT000Z8 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z5 ;
          prmT000Z5 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z6 ;
          prmT000Z6 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z4 ;
          prmT000Z4 = new Object[] {
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z7 ;
          prmT000Z7 = new Object[] {
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z9 ;
          prmT000Z9 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z10 ;
          prmT000Z10 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z11 ;
          prmT000Z11 = new Object[] {
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z12 ;
          prmT000Z12 = new Object[] {
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z13 ;
          prmT000Z13 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z3 ;
          prmT000Z3 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z14 ;
          prmT000Z14 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z15 ;
          prmT000Z15 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z2 ;
          prmT000Z2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z16 ;
          prmT000Z16 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoAPF_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Acao",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Mensagem",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Link",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoAPF_ParecerSE",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z17 ;
          prmT000Z17 = new Object[] {
          new Object[] {"@FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoAPF_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPF_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPF_Acao",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Mensagem",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Link",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Tecnica",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@FuncaoAPF_ParecerSE",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@FuncaoAPF_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPF_Importada",SqlDbType.DateTime,8,5} ,
          new Object[] {"@FuncaoAPF_UpdAoImpBsln",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoAPF_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z18 ;
          prmT000Z18 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z19 ;
          prmT000Z19 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z20 ;
          prmT000Z20 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z21 ;
          prmT000Z21 = new Object[] {
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z22 ;
          prmT000Z22 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z23 ;
          prmT000Z23 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z24 ;
          prmT000Z24 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z25 ;
          prmT000Z25 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z26 ;
          prmT000Z26 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z27 ;
          prmT000Z27 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z28 ;
          prmT000Z28 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z29 ;
          prmT000Z29 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z30 ;
          prmT000Z30 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z31 ;
          prmT000Z31 = new Object[] {
          } ;
          Object[] prmT000Z32 ;
          prmT000Z32 = new Object[] {
          new Object[] {"@AV15FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z33 ;
          prmT000Z33 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z34 ;
          prmT000Z34 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Z35 ;
          prmT000Z35 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000Z2", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_Nome], [FuncaoAPF_Descricao], [FuncaoAPF_Tipo], [FuncaoAPF_DERImp], [FuncaoAPF_RAImp], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_Link], [FuncaoAPF_Tecnica], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Importada], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, [FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, [FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, [FuncaoAPF_MelhoraCod] AS FuncaoAPF_MelhoraCod FROM [FuncoesAPF] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z2,1,0,true,false )
             ,new CursorDef("T000Z3", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_Nome], [FuncaoAPF_Descricao], [FuncaoAPF_Tipo], [FuncaoAPF_DERImp], [FuncaoAPF_RAImp], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_Link], [FuncaoAPF_Tecnica], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Importada], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, [FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, [FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, [FuncaoAPF_MelhoraCod] AS FuncaoAPF_MelhoraCod FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z3,1,0,true,false )
             ,new CursorDef("T000Z4", "SELECT [Sistema_Nome] AS FuncaoAPF_SistemaDes, [Sistema_Tecnica] AS FuncaoAPF_SistemaTec, [Sistema_ProjetoCod] AS FuncaoAPF_SistemaProjCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoAPF_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z4,1,0,true,false )
             ,new CursorDef("T000Z5", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z5,1,0,true,false )
             ,new CursorDef("T000Z6", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z6,1,0,true,false )
             ,new CursorDef("T000Z7", "SELECT [FuncaoAPF_Codigo] AS FuncaoAPF_MelhoraCod FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z7,1,0,true,false )
             ,new CursorDef("T000Z8", "SELECT TM1.[FuncaoAPF_Codigo], TM1.[FuncaoAPF_Nome], TM1.[FuncaoAPF_Descricao], TM1.[FuncaoAPF_Tipo], TM1.[FuncaoAPF_DERImp], TM1.[FuncaoAPF_RAImp], T2.[Sistema_Nome] AS FuncaoAPF_SistemaDes, T2.[Sistema_Tecnica] AS FuncaoAPF_SistemaTec, T3.[Modulo_Nome] AS FuncaoAPF_ModuloNom, T4.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, TM1.[FuncaoAPF_Acao], TM1.[FuncaoAPF_Mensagem], TM1.[FuncaoAPF_Link], TM1.[FuncaoAPF_Tecnica], TM1.[FuncaoAPF_ParecerSE], TM1.[FuncaoAPF_Observacao], TM1.[FuncaoAPF_Importada], TM1.[FuncaoAPF_UpdAoImpBsln], TM1.[FuncaoAPF_Ativo], TM1.[FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, TM1.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, TM1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, TM1.[FuncaoAPF_MelhoraCod] AS FuncaoAPF_MelhoraCod, T2.[Sistema_ProjetoCod] AS FuncaoAPF_SistemaProjCod FROM ((([FuncoesAPF] TM1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[FuncaoAPF_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = TM1.[FuncaoAPF_ModuloCod]) LEFT JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = TM1.[FuncaoAPF_FunAPFPaiCod]) WHERE TM1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY TM1.[FuncaoAPF_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z8,100,0,true,false )
             ,new CursorDef("T000Z9", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z9,1,0,true,false )
             ,new CursorDef("T000Z10", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z10,1,0,true,false )
             ,new CursorDef("T000Z11", "SELECT [Sistema_Nome] AS FuncaoAPF_SistemaDes, [Sistema_Tecnica] AS FuncaoAPF_SistemaTec, [Sistema_ProjetoCod] AS FuncaoAPF_SistemaProjCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoAPF_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z11,1,0,true,false )
             ,new CursorDef("T000Z12", "SELECT [FuncaoAPF_Codigo] AS FuncaoAPF_MelhoraCod FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z12,1,0,true,false )
             ,new CursorDef("T000Z13", "SELECT [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z13,1,0,true,false )
             ,new CursorDef("T000Z14", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ( [FuncaoAPF_Codigo] > @FuncaoAPF_Codigo) ORDER BY [FuncaoAPF_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z14,1,0,true,true )
             ,new CursorDef("T000Z15", "SELECT TOP 1 [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ( [FuncaoAPF_Codigo] < @FuncaoAPF_Codigo) ORDER BY [FuncaoAPF_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z15,1,0,true,true )
             ,new CursorDef("T000Z16", "INSERT INTO [FuncoesAPF]([FuncaoAPF_Nome], [FuncaoAPF_Descricao], [FuncaoAPF_Tipo], [FuncaoAPF_DERImp], [FuncaoAPF_RAImp], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_Link], [FuncaoAPF_Tecnica], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Importada], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Ativo], [FuncaoAPF_SistemaCod], [FuncaoAPF_ModuloCod], [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_MelhoraCod]) VALUES(@FuncaoAPF_Nome, @FuncaoAPF_Descricao, @FuncaoAPF_Tipo, @FuncaoAPF_DERImp, @FuncaoAPF_RAImp, @FuncaoAPF_Acao, @FuncaoAPF_Mensagem, @FuncaoAPF_Link, @FuncaoAPF_Tecnica, @FuncaoAPF_ParecerSE, @FuncaoAPF_Observacao, @FuncaoAPF_Importada, @FuncaoAPF_UpdAoImpBsln, @FuncaoAPF_Ativo, @FuncaoAPF_SistemaCod, @FuncaoAPF_ModuloCod, @FuncaoAPF_FunAPFPaiCod, @FuncaoAPF_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000Z16)
             ,new CursorDef("T000Z17", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Nome]=@FuncaoAPF_Nome, [FuncaoAPF_Descricao]=@FuncaoAPF_Descricao, [FuncaoAPF_Tipo]=@FuncaoAPF_Tipo, [FuncaoAPF_DERImp]=@FuncaoAPF_DERImp, [FuncaoAPF_RAImp]=@FuncaoAPF_RAImp, [FuncaoAPF_Acao]=@FuncaoAPF_Acao, [FuncaoAPF_Mensagem]=@FuncaoAPF_Mensagem, [FuncaoAPF_Link]=@FuncaoAPF_Link, [FuncaoAPF_Tecnica]=@FuncaoAPF_Tecnica, [FuncaoAPF_ParecerSE]=@FuncaoAPF_ParecerSE, [FuncaoAPF_Observacao]=@FuncaoAPF_Observacao, [FuncaoAPF_Importada]=@FuncaoAPF_Importada, [FuncaoAPF_UpdAoImpBsln]=@FuncaoAPF_UpdAoImpBsln, [FuncaoAPF_Ativo]=@FuncaoAPF_Ativo, [FuncaoAPF_SistemaCod]=@FuncaoAPF_SistemaCod, [FuncaoAPF_ModuloCod]=@FuncaoAPF_ModuloCod, [FuncaoAPF_FunAPFPaiCod]=@FuncaoAPF_FunAPFPaiCod, [FuncaoAPF_MelhoraCod]=@FuncaoAPF_MelhoraCod  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK,prmT000Z17)
             ,new CursorDef("T000Z18", "DELETE FROM [FuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK,prmT000Z18)
             ,new CursorDef("T000Z19", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z19,1,0,true,false )
             ,new CursorDef("T000Z20", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z20,1,0,true,false )
             ,new CursorDef("T000Z21", "SELECT [Sistema_Nome] AS FuncaoAPF_SistemaDes, [Sistema_Tecnica] AS FuncaoAPF_SistemaTec, [Sistema_ProjetoCod] AS FuncaoAPF_SistemaProjCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @FuncaoAPF_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z21,1,0,true,false )
             ,new CursorDef("T000Z22", "SELECT TOP 1 [FuncaoAPF_Codigo] AS FuncaoAPF_MelhoraCod FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_MelhoraCod] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z22,1,0,true,true )
             ,new CursorDef("T000Z23", "SELECT TOP 1 [FuncaoAPF_Codigo] AS FuncaoAPF_FunAPFPaiCod FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_FunAPFPaiCod] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z23,1,0,true,true )
             ,new CursorDef("T000Z24", "SELECT TOP 1 [FuncaoAPFPreImp_FnApfCodigo], [FuncaoAPFPreImp_Sequencial] FROM [FuncaoAPFPreImp] WITH (NOLOCK) WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z24,1,0,true,true )
             ,new CursorDef("T000Z25", "SELECT TOP 1 [SolicitacaoMudanca_Codigo], [SolicitacaoMudancaItem_FuncaoAPF] FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE [SolicitacaoMudancaItem_FuncaoAPF] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z25,1,0,true,true )
             ,new CursorDef("T000Z26", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_FnAPFCod] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z26,1,0,true,true )
             ,new CursorDef("T000Z27", "SELECT TOP 1 [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z27,1,0,true,true )
             ,new CursorDef("T000Z28", "SELECT TOP 1 [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z28,1,0,true,true )
             ,new CursorDef("T000Z29", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z29,1,0,true,true )
             ,new CursorDef("T000Z30", "SELECT TOP 1 [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z30,1,0,true,true )
             ,new CursorDef("T000Z31", "SELECT [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) ORDER BY [FuncaoAPF_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z31,100,0,true,false )
             ,new CursorDef("T000Z32", "SELECT [Modulo_Codigo], [Modulo_Nome], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV15FuncaoAPF_SistemaCod ORDER BY [Modulo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z32,0,0,true,false )
             ,new CursorDef("T000Z33", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_Codigo] <> @AV7FuncaoAPF_Codigo) AND ([FuncaoAPF_SistemaCod] = @AV15FuncaoAPF_SistemaCod) ORDER BY [FuncaoAPF_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z33,0,0,true,false )
             ,new CursorDef("T000Z34", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z34,1,0,true,false )
             ,new CursorDef("T000Z35", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Z35,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((bool[]) buf[22])[0] = rslt.getBool(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 1) ;
                ((int[]) buf[25])[0] = rslt.getInt(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((short[]) buf[14])[0] = rslt.getShort(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((bool[]) buf[22])[0] = rslt.getBool(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 1) ;
                ((int[]) buf[25])[0] = rslt.getInt(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((bool[]) buf[18])[0] = rslt.getBool(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((String[]) buf[32])[0] = rslt.getString(19, 1) ;
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((int[]) buf[35])[0] = rslt.getInt(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((int[]) buf[41])[0] = rslt.getInt(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 33 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[22]);
                }
                stmt.SetParameter(14, (String)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[22]);
                }
                stmt.SetParameter(14, (String)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[33]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
