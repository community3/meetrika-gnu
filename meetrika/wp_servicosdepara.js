/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:20:32.13
*/
gx.evt.autoSkip = false;
gx.define('wp_servicosdepara', false, function () {
   this.ServerClass =  "wp_servicosdepara" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A40000GXC1=gx.fn.getIntegerValue("GXC1",'.') ;
      this.A39Contratada_Codigo=gx.fn.getIntegerValue("CONTRATADA_CODIGO",'.') ;
      this.AV35Contratada_Codigo=gx.fn.getIntegerValue("vCONTRATADA_CODIGO",'.') ;
      this.A160ContratoServicos_Codigo=gx.fn.getIntegerValue("CONTRATOSERVICOS_CODIGO",'.') ;
      this.A1466ContratoServicosDePara_Origem=gx.fn.getControlValue("CONTRATOSERVICOSDEPARA_ORIGEM") ;
      this.AV6Origem=gx.fn.getControlValue("vORIGEM") ;
      this.A1470ContratoServicosDePara_OrigemDsc2=gx.fn.getControlValue("CONTRATOSERVICOSDEPARA_ORIGEMDSC2") ;
      this.A1468ContratoServicosDePara_OrigemDsc=gx.fn.getControlValue("CONTRATOSERVICOSDEPARA_ORIGEMDSC") ;
      this.A1465ContratoServicosDePara_Codigo=gx.fn.getIntegerValue("CONTRATOSERVICOSDEPARA_CODIGO",'.') ;
      this.A1467ContratoServicosDePara_OrigenId=gx.fn.getIntegerValue("CONTRATOSERVICOSDEPARA_ORIGENID",'.') ;
      this.A1469ContratoServicosDePara_OrigenId2=gx.fn.getIntegerValue("CONTRATOSERVICOSDEPARA_ORIGENID2",'.') ;
      this.AV22ContratoServicos_Codigo=gx.fn.getIntegerValue("vCONTRATOSERVICOS_CODIGO",'.') ;
      this.AV23ContratoServicosDePara_Codigo=gx.fn.getIntegerValue("vCONTRATOSERVICOSDEPARA_CODIGO",'.') ;
      this.AV7OrigemDsc=gx.fn.getControlValue("vORIGEMDSC") ;
      this.AV18OrigemDsc2=gx.fn.getControlValue("vORIGEMDSC2") ;
   };
   this.e14ky2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e11ky2_client=function()
   {
      this.executeServerEvent("CONFIRMPANEL.CLOSE", false, null, true, true);
   };
   this.e12ky2_client=function()
   {
      this.executeServerEvent("DESVINCULAPANEL.CLOSE", false, null, true, true);
   };
   this.e15ky2_client=function()
   {
      this.executeServerEvent("'FECHAR'", false, null, false, false);
   };
   this.e17ky2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[4,7,13,15,18,20,23,25,30,36];
   this.GXLastCtrlId =36;
   this.CONFIRMPANELContainer = gx.uc.getNew(this, 33, 15, "DVelop_ConfirmPanel", "CONFIRMPANELContainer", "Confirmpanel");
   var CONFIRMPANELContainer = this.CONFIRMPANELContainer;
   CONFIRMPANELContainer.setProp("Width", "Width", "", "str");
   CONFIRMPANELContainer.setProp("Height", "Height", "50", "str");
   CONFIRMPANELContainer.setProp("Closeable", "Closeable", false, "bool");
   CONFIRMPANELContainer.setProp("Title", "Title", "Atenção", "str");
   CONFIRMPANELContainer.setProp("ConfirmText", "Confirmtext", "Confirma o vinculo com o Serviço", "str");
   CONFIRMPANELContainer.setProp("Icon", "Icon", "2", "str");
   CONFIRMPANELContainer.setProp("Modal", "Modal", true, "bool");
   CONFIRMPANELContainer.setProp("Result", "Result", "", "char");
   CONFIRMPANELContainer.setProp("ButtonYesText", "Buttonyestext", "Sim", "str");
   CONFIRMPANELContainer.setProp("ButtonNoText", "Buttonnotext", "Não", "str");
   CONFIRMPANELContainer.setProp("ButtonCancelText", "Buttoncanceltext", "Cancel", "str");
   CONFIRMPANELContainer.setProp("ConfirmType", "Confirmtype", "1", "str");
   CONFIRMPANELContainer.setProp("Draggable", "Draggeable", true, "bool");
   CONFIRMPANELContainer.setProp("Visible", "Visible", true, "bool");
   CONFIRMPANELContainer.setProp("Enabled", "Enabled", true, "boolean");
   CONFIRMPANELContainer.setProp("Class", "Class", "", "char");
   CONFIRMPANELContainer.setC2ShowFunction(function(UC) { UC.Show(); });
   CONFIRMPANELContainer.addEventHandler("Close", this.e11ky2_client);
   this.setUserControl(CONFIRMPANELContainer);
   this.DESVINCULAPANELContainer = gx.uc.getNew(this, 34, 15, "DVelop_ConfirmPanel", "DESVINCULAPANELContainer", "Desvinculapanel");
   var DESVINCULAPANELContainer = this.DESVINCULAPANELContainer;
   DESVINCULAPANELContainer.setProp("Width", "Width", "", "str");
   DESVINCULAPANELContainer.setProp("Height", "Height", "50", "str");
   DESVINCULAPANELContainer.setProp("Closeable", "Closeable", false, "bool");
   DESVINCULAPANELContainer.setProp("Title", "Title", "Aviso", "str");
   DESVINCULAPANELContainer.setDynProp("ConfirmText", "Confirmtext", "Do you want to continue?", "str");
   DESVINCULAPANELContainer.setProp("Icon", "Icon", "0", "str");
   DESVINCULAPANELContainer.setProp("Modal", "Modal", true, "bool");
   DESVINCULAPANELContainer.setProp("Result", "Result", "", "char");
   DESVINCULAPANELContainer.setProp("ButtonYesText", "Buttonyestext", "Vincular", "str");
   DESVINCULAPANELContainer.setProp("ButtonNoText", "Buttonnotext", "Voltar", "str");
   DESVINCULAPANELContainer.setProp("ButtonCancelText", "Buttoncanceltext", "Eliminar", "str");
   DESVINCULAPANELContainer.setProp("ConfirmType", "Confirmtype", "2", "str");
   DESVINCULAPANELContainer.setProp("Draggable", "Draggeable", true, "bool");
   DESVINCULAPANELContainer.setProp("Visible", "Visible", true, "bool");
   DESVINCULAPANELContainer.setProp("Enabled", "Enabled", true, "boolean");
   DESVINCULAPANELContainer.setProp("Class", "Class", "", "char");
   DESVINCULAPANELContainer.setC2ShowFunction(function(UC) { UC.Show(); });
   DESVINCULAPANELContainer.addEventHandler("Close", this.e12ky2_client);
   this.setUserControl(DESVINCULAPANELContainer);
   GXValidFnc[4]={fld:"TABLE2",grid:0};
   GXValidFnc[7]={fld:"TABLE1",grid:0};
   GXValidFnc[13]={fld:"TEXTBLOCK1", format:0,grid:0};
   GXValidFnc[15]={lvl:0,type:"int",len:18,dec:0,sign:false,pic:"ZZZZZZZZZZZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vTRACKER",gxz:"ZV15Tracker",gxold:"OV15Tracker",gxvar:"AV15Tracker",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15Tracker=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15Tracker=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vTRACKER",gx.O.AV15Tracker)},c2v:function(){if(this.val()!==undefined)gx.O.AV15Tracker=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTRACKER",'.')},nac:gx.falseFn};
   GXValidFnc[18]={fld:"TEXTBLOCK3", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:18,dec:0,sign:false,pic:"ZZZZZZZZZZZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vESTADO",gxz:"ZV16Estado",gxold:"OV16Estado",gxvar:"AV16Estado",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16Estado=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16Estado=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vESTADO",gx.O.AV16Estado)},c2v:function(){if(this.val()!==undefined)gx.O.AV16Estado=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vESTADO",'.')},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TEXTBLOCK2", format:0,grid:0};
   GXValidFnc[25]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSERVICO_CODIGO",gxz:"ZV11Servico_Codigo",gxold:"OV11Servico_Codigo",gxvar:"AV11Servico_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV11Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV11Servico_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_CODIGO",gx.O.AV11Servico_Codigo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV11Servico_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 25 , function() {
   });
   GXValidFnc[30]={fld:"TABLE3",grid:0};
   GXValidFnc[36]={fld:"TBJAVA", format:1,grid:0};
   this.AV15Tracker = 0 ;
   this.ZV15Tracker = 0 ;
   this.OV15Tracker = 0 ;
   this.AV16Estado = 0 ;
   this.ZV16Estado = 0 ;
   this.OV16Estado = 0 ;
   this.AV11Servico_Codigo = 0 ;
   this.ZV11Servico_Codigo = 0 ;
   this.OV11Servico_Codigo = 0 ;
   this.AV15Tracker = 0 ;
   this.AV16Estado = 0 ;
   this.AV11Servico_Codigo = 0 ;
   this.AV6Origem = "" ;
   this.AV35Contratada_Codigo = 0 ;
   this.A160ContratoServicos_Codigo = 0 ;
   this.A827ContratoServicos_ServicoCod = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.A155Servico_Codigo = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.A1466ContratoServicosDePara_Origem = "" ;
   this.A1470ContratoServicosDePara_OrigemDsc2 = "" ;
   this.A1468ContratoServicosDePara_OrigemDsc = "" ;
   this.A1465ContratoServicosDePara_Codigo = 0 ;
   this.A1467ContratoServicosDePara_OrigenId = 0 ;
   this.A1469ContratoServicosDePara_OrigenId2 = 0 ;
   this.A638ContratoServicos_Ativo = false ;
   this.A826ContratoServicos_ServicoSigla = "" ;
   this.A605Servico_Sigla = "" ;
   this.AV22ContratoServicos_Codigo = 0 ;
   this.AV23ContratoServicosDePara_Codigo = 0 ;
   this.AV7OrigemDsc = "" ;
   this.AV18OrigemDsc2 = "" ;
   this.Events = {"e14ky2_client": ["ENTER", true] ,"e11ky2_client": ["CONFIRMPANEL.CLOSE", true] ,"e12ky2_client": ["DESVINCULAPANEL.CLOSE", true] ,"e15ky2_client": ["'FECHAR'", true] ,"e17ky2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["ENTER"] = [[{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV11Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["CONFIRMPANEL.CLOSE"] = [[{av:'this.CONFIRMPANELContainer.Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1466ContratoServicosDePara_Origem',fld:'CONTRATOSERVICOSDEPARA_ORIGEM',pic:'',nv:''},{av:'AV6Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{av:'A1470ContratoServicosDePara_OrigemDsc2',fld:'CONTRATOSERVICOSDEPARA_ORIGEMDSC2',pic:'',nv:''},{av:'A1468ContratoServicosDePara_OrigemDsc',fld:'CONTRATOSERVICOSDEPARA_ORIGEMDSC',pic:'',nv:''},{av:'A1465ContratoServicosDePara_Codigo',fld:'CONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1467ContratoServicosDePara_OrigenId',fld:'CONTRATOSERVICOSDEPARA_ORIGENID',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'A1469ContratoServicosDePara_OrigenId2',fld:'CONTRATOSERVICOSDEPARA_ORIGENID2',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{ctrl:'vTRACKER'},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{ctrl:'vESTADO'}],[{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DESVINCULAPANELContainer.ConfirmText',ctrl:'DESVINCULAPANEL',prop:'ConfirmText'},{av:'AV23ContratoServicosDePara_Codigo',fld:'vCONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV7OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''},{av:'AV18OrigemDsc2',fld:'vORIGEMDSC2',pic:'',nv:''}]];
   this.EvtParms["DESVINCULAPANEL.CLOSE"] = [[{av:'this.DESVINCULAPANELContainer.Result',ctrl:'DESVINCULAPANEL',prop:'Result'},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23ContratoServicosDePara_Codigo',fld:'vCONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV7OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV18OrigemDsc2',fld:'vORIGEMDSC2',pic:'',nv:''},{av:'AV6Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{ctrl:'vTRACKER'},{ctrl:'vESTADO'}],[{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0}]];
   this.EvtParms["'FECHAR'"] = [[],[]];
   this.EnterCtrl = ["BUTTON1"];
   this.setVCMap("A40000GXC1", "GXC1", 0, "int");
   this.setVCMap("A155Servico_Codigo", "SERVICO_CODIGO", 0, "int");
   this.setVCMap("A827ContratoServicos_ServicoCod", "CONTRATOSERVICOS_SERVICOCOD", 0, "int");
   this.setVCMap("A39Contratada_Codigo", "CONTRATADA_CODIGO", 0, "int");
   this.setVCMap("AV35Contratada_Codigo", "vCONTRATADA_CODIGO", 0, "int");
   this.setVCMap("A160ContratoServicos_Codigo", "CONTRATOSERVICOS_CODIGO", 0, "int");
   this.setVCMap("A1466ContratoServicosDePara_Origem", "CONTRATOSERVICOSDEPARA_ORIGEM", 0, "char");
   this.setVCMap("AV6Origem", "vORIGEM", 0, "char");
   this.setVCMap("A1470ContratoServicosDePara_OrigemDsc2", "CONTRATOSERVICOSDEPARA_ORIGEMDSC2", 0, "vchar");
   this.setVCMap("A1468ContratoServicosDePara_OrigemDsc", "CONTRATOSERVICOSDEPARA_ORIGEMDSC", 0, "vchar");
   this.setVCMap("A1465ContratoServicosDePara_Codigo", "CONTRATOSERVICOSDEPARA_CODIGO", 0, "int");
   this.setVCMap("A1467ContratoServicosDePara_OrigenId", "CONTRATOSERVICOSDEPARA_ORIGENID", 0, "int");
   this.setVCMap("A1469ContratoServicosDePara_OrigenId2", "CONTRATOSERVICOSDEPARA_ORIGENID2", 0, "int");
   this.setVCMap("AV22ContratoServicos_Codigo", "vCONTRATOSERVICOS_CODIGO", 0, "int");
   this.setVCMap("AV23ContratoServicosDePara_Codigo", "vCONTRATOSERVICOSDEPARA_CODIGO", 0, "int");
   this.setVCMap("AV7OrigemDsc", "vORIGEMDSC", 0, "vchar");
   this.setVCMap("AV18OrigemDsc2", "vORIGEMDSC2", 0, "vchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_servicosdepara);
