/*
               File: WP_AssociationServicoUsuarios
        Description: Usu�rios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:47:56.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associationservicousuarios : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associationservicousuarios( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associationservicousuarios( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UsuarioServicos_ServicoCod )
      {
         this.AV7UsuarioServicos_ServicoCod = aP0_UsuarioServicos_ServicoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_ServicoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIOSERVICOS_SERVICOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_ServicoCod), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAET2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavServico_sigla_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
               WSET2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEET2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Usu�rios") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117475648");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associationservicousuarios.aspx") + "?" + UrlEncode("" +AV7UsuarioServicos_ServicoCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV31jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV31jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV29jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV29jqAssociados);
         }
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV35MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV35MenuPerfil);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV28All);
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIOSERVICOS_SERVICOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_ServicoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIOSERVICOS_SERVICOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_ServicoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormET2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociationServicoUsuarios" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usu�rios" ;
      }

      protected void WBET0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_ET2( true) ;
         }
         else
         {
            wb_table1_2_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTET2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Usu�rios", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPET0( ) ;
      }

      protected void WSET2( )
      {
         STARTET2( ) ;
         EVTET2( ) ;
      }

      protected void EVTET2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11ET2 */
                           E11ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12ET2 */
                           E12ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13ET2 */
                                 E13ET2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14ET2 */
                           E14ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15ET2 */
                           E15ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16ET2 */
                           E16ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17ET2 */
                           E17ET2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18ET2 */
                           E18ET2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEET2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormET2( ) ;
            }
         }
      }

      protected void PAET2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavServico_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFET2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
      }

      protected void RFET2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12ET2 */
         E12ET2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E18ET2 */
            E18ET2 ();
            WBET0( ) ;
         }
      }

      protected void STRUPET0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11ET2 */
         E11ET2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV31jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV29jqAssociados);
            /* Read variables values. */
            AV27Servico_Sigla = StringUtil.Upper( cgiGet( edtavServico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Servico_Sigla", AV27Servico_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!"))));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11ET2 */
         E11ET2 ();
         if (returnInSub) return;
      }

      protected void E11ET2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV38GXLvl7 = 0;
            /* Using cursor H00ET2 */
            pr_default.execute(0, new Object[] {AV7UsuarioServicos_ServicoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A155Servico_Codigo = H00ET2_A155Servico_Codigo[0];
               A605Servico_Sigla = H00ET2_A605Servico_Sigla[0];
               AV38GXLvl7 = 1;
               AV27Servico_Sigla = A605Servico_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Servico_Sigla", AV27Servico_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV38GXLvl7 == 0 )
            {
               GX_msglist.addItem("Servi�o n�o encontrado.");
            }
            /* Using cursor H00ET3 */
            pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H00ET3_A66ContratadaUsuario_ContratadaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00ET3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00ET3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00ET3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00ET3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A69ContratadaUsuario_UsuarioCod = H00ET3_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00ET3_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00ET3_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00ET3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00ET3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00ET3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00ET3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00ET3_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00ET3_n71ContratadaUsuario_UsuarioPessoaNom[0];
               AV8UsuarioServicos_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_UsuarioCod), 6, 0)));
               AV10Exist = false;
               /* Using cursor H00ET4 */
               pr_default.execute(2, new Object[] {AV8UsuarioServicos_UsuarioCod, AV7UsuarioServicos_ServicoCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00ET4_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00ET4_A829UsuarioServicos_ServicoCod[0];
                  AV10Exist = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               if ( AV10Exist )
               {
                  AV30jqItem = new SdtjqSelectData_Item(context);
                  AV30jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0);
                  AV30jqItem.gxTpr_Descr = A71ContratadaUsuario_UsuarioPessoaNom;
                  AV30jqItem.gxTpr_Selected = false;
                  AV29jqAssociados.Add(AV30jqItem, 0);
               }
               else
               {
                  AV30jqItem = new SdtjqSelectData_Item(context);
                  AV30jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0);
                  AV30jqItem.gxTpr_Descr = A71ContratadaUsuario_UsuarioPessoaNom;
                  AV30jqItem.gxTpr_Selected = false;
                  AV31jqNaoAssociados.Add(AV30jqItem, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
      }

      protected void E12ET2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13ET2 */
         E13ET2 ();
         if (returnInSub) return;
      }

      protected void E13ET2( )
      {
         /* Enter Routine */
         AV12Success = true;
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV29jqAssociados.Count )
         {
            AV30jqItem = ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV41GXV1));
            if ( AV12Success )
            {
               AV8UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( AV30jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_UsuarioCod), 6, 0)));
               AV42GXLvl63 = 0;
               /* Using cursor H00ET5 */
               pr_default.execute(3, new Object[] {AV8UsuarioServicos_UsuarioCod, AV7UsuarioServicos_ServicoCod});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00ET5_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00ET5_A829UsuarioServicos_ServicoCod[0];
                  AV42GXLvl63 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( AV42GXLvl63 == 0 )
               {
                  AV11UsuarioServicos = new SdtUsuarioServicos(context);
                  AV11UsuarioServicos.gxTpr_Usuarioservicos_servicocod = AV7UsuarioServicos_ServicoCod;
                  AV11UsuarioServicos.gxTpr_Usuarioservicos_usuariocod = AV8UsuarioServicos_UsuarioCod;
                  AV11UsuarioServicos.Save();
                  if ( ! AV11UsuarioServicos.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         AV43GXV2 = 1;
         while ( AV43GXV2 <= AV31jqNaoAssociados.Count )
         {
            AV30jqItem = ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV43GXV2));
            if ( AV12Success )
            {
               AV8UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( AV30jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_UsuarioCod), 6, 0)));
               /* Using cursor H00ET6 */
               pr_default.execute(4, new Object[] {AV8UsuarioServicos_UsuarioCod, AV7UsuarioServicos_ServicoCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00ET6_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00ET6_A829UsuarioServicos_ServicoCod[0];
                  AV11UsuarioServicos = new SdtUsuarioServicos(context);
                  AV11UsuarioServicos.Load(AV8UsuarioServicos_UsuarioCod, AV7UsuarioServicos_ServicoCod);
                  if ( AV11UsuarioServicos.Success() )
                  {
                     AV11UsuarioServicos.Delete();
                  }
                  if ( ! AV11UsuarioServicos.Success() )
                  {
                     AV12Success = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
            }
            AV43GXV2 = (int)(AV43GXV2+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociationServicoUsuarios");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociationServicoUsuarios");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E14ET2( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
      }

      protected void E15ET2( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
      }

      protected void E16ET2( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
      }

      protected void E17ET2( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
      }

      protected void S112( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV46GXV4 = 1;
         AV45GXV3 = AV35MenuPerfil.GetMessages();
         while ( AV46GXV4 <= AV45GXV3.Count )
         {
            AV15Message = ((SdtMessages_Message)AV45GXV3.Item(AV46GXV4));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV46GXV4 = (int)(AV46GXV4+1);
         }
      }

      protected void S132( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV31jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)).gxTpr_Selected || AV28All )
            {
               ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV29jqAssociados.Add(((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)), 0);
               AV31jqNaoAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV29jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)).gxTpr_Selected || AV28All )
            {
               ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV31jqNaoAssociados.Add(((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)), 0);
               AV29jqAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV31jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV29jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E18ET2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_ET2( true) ;
         }
         else
         {
            wb_table2_8_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_ET2( true) ;
         }
         else
         {
            wb_table3_16_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_48_ET2( true) ;
         }
         else
         {
            wb_table4_48_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_ET2e( true) ;
         }
         else
         {
            wb_table1_2_ET2e( false) ;
         }
      }

      protected void wb_table4_48_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_ET2e( true) ;
         }
         else
         {
            wb_table4_48_ET2e( false) ;
         }
      }

      protected void wb_table3_16_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_ET2( true) ;
         }
         else
         {
            wb_table5_19_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_ET2e( true) ;
         }
         else
         {
            wb_table3_16_ET2e( false) ;
         }
      }

      protected void wb_table5_19_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_31_ET2( true) ;
         }
         else
         {
            wb_table6_31_ET2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_ET2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_ET2e( true) ;
         }
         else
         {
            wb_table5_19_ET2e( false) ;
         }
      }

      protected void wb_table6_31_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_ET2e( true) ;
         }
         else
         {
            wb_table6_31_ET2e( false) ;
         }
      }

      protected void wb_table2_8_ET2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar ao Servi�o :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla_Internalname, StringUtil.RTrim( AV27Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV27Servico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavServico_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociationServicoUsuarios.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_ET2e( true) ;
         }
         else
         {
            wb_table2_8_ET2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7UsuarioServicos_ServicoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_ServicoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIOSERVICOS_SERVICOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_ServicoCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAET2( ) ;
         WSET2( ) ;
         WEET2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117475717");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associationservicousuarios.js", "?20203117475717");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavServico_sigla_Internalname = "vSERVICO_SIGLA";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavServico_sigla_Jsonclick = "";
         edtavServico_sigla_Enabled = 1;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         lblAssociatedrecordstitle_Caption = "Usu�rios Associados";
         lblNotassociatedrecordstitle_Caption = "Usu�rios N�o Associados";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 300;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E13ET2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7UsuarioServicos_ServicoCod',fld:'vUSUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV35MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null}],oparms:[{av:'AV8UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14ET2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15ET2',iparms:[{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16ET2',iparms:[{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17ET2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV29jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV35MenuPerfil = new SdtMenuPerfil(context);
         AV27Servico_Sigla = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00ET2_A155Servico_Codigo = new int[1] ;
         H00ET2_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         H00ET3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00ET3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00ET3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00ET3_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00ET3_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00ET3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00ET3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00ET3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00ET4_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00ET4_A829UsuarioServicos_ServicoCod = new int[1] ;
         AV30jqItem = new SdtjqSelectData_Item(context);
         H00ET5_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00ET5_A829UsuarioServicos_ServicoCod = new int[1] ;
         AV11UsuarioServicos = new SdtUsuarioServicos(context);
         H00ET6_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00ET6_A829UsuarioServicos_ServicoCod = new int[1] ;
         AV45GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associationservicousuarios__default(),
            new Object[][] {
                new Object[] {
               H00ET2_A155Servico_Codigo, H00ET2_A605Servico_Sigla
               }
               , new Object[] {
               H00ET3_A66ContratadaUsuario_ContratadaCod, H00ET3_A70ContratadaUsuario_UsuarioPessoaCod, H00ET3_n70ContratadaUsuario_UsuarioPessoaCod, H00ET3_A1228ContratadaUsuario_AreaTrabalhoCod, H00ET3_n1228ContratadaUsuario_AreaTrabalhoCod, H00ET3_A69ContratadaUsuario_UsuarioCod, H00ET3_A71ContratadaUsuario_UsuarioPessoaNom, H00ET3_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00ET4_A828UsuarioServicos_UsuarioCod, H00ET4_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00ET5_A828UsuarioServicos_UsuarioCod, H00ET5_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00ET6_A828UsuarioServicos_UsuarioCod, H00ET6_A829UsuarioServicos_ServicoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavServico_sigla_Enabled = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV38GXLvl7 ;
      private short AV42GXLvl63 ;
      private short nGXWrapped ;
      private int AV7UsuarioServicos_ServicoCod ;
      private int wcpOAV7UsuarioServicos_ServicoCod ;
      private int edtavServico_sigla_Enabled ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int A155Servico_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV8UsuarioServicos_UsuarioCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV41GXV1 ;
      private int AV43GXV2 ;
      private int AV46GXV4 ;
      private int AV14i ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavServico_sigla_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV27Servico_Sigla ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A605Servico_Sigla ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtavServico_sigla_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV28All ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00ET2_A155Servico_Codigo ;
      private String[] H00ET2_A605Servico_Sigla ;
      private int[] H00ET3_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00ET3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00ET3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00ET3_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00ET3_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00ET3_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00ET3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00ET3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00ET4_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ET4_A829UsuarioServicos_ServicoCod ;
      private int[] H00ET5_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ET5_A829UsuarioServicos_ServicoCod ;
      private int[] H00ET6_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ET6_A829UsuarioServicos_ServicoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV31jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV29jqAssociados ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV45GXV3 ;
      private SdtjqSelectData_Item AV30jqItem ;
      private SdtMessages_Message AV15Message ;
      private SdtUsuarioServicos AV11UsuarioServicos ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private SdtMenuPerfil AV35MenuPerfil ;
   }

   public class wp_associationservicousuarios__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ET2 ;
          prmH00ET2 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ET3 ;
          prmH00ET3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ET4 ;
          prmH00ET4 = new Object[] {
          new Object[] {"@AV8UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ET5 ;
          prmH00ET5 = new Object[] {
          new Object[] {"@AV8UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ET6 ;
          prmH00ET6 = new Object[] {
          new Object[] {"@AV8UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ET2", "SELECT TOP 1 [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV7UsuarioServicos_ServicoCod ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ET2,1,0,false,true )
             ,new CursorDef("H00ET3", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ET3,100,0,true,false )
             ,new CursorDef("H00ET4", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV8UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV7UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ET4,1,0,false,true )
             ,new CursorDef("H00ET5", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV8UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV7UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ET5,1,0,false,true )
             ,new CursorDef("H00ET6", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV8UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV7UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ET6,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
