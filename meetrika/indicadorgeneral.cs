/*
               File: IndicadorGeneral
        Description: Indicador General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:24.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class indicadorgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public indicadorgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public indicadorgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Indicador_Codigo )
      {
         this.A2058Indicador_Codigo = aP0_Indicador_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbIndicador_Refer = new GXCombobox();
         cmbIndicador_Aplica = new GXCombobox();
         dynIndicador_NaoCnfCod = new GXCombobox();
         cmbIndicador_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A2058Indicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A2058Indicador_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"INDICADOR_NAOCNFCOD") == 0 )
               {
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAINDICADOR_NAOCNFCODRX2( AV6WWPContext) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PARX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "IndicadorGeneral";
               context.Gx_err = 0;
               WSRX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Indicador General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117302440");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("indicadorgeneral.aspx") + "?" + UrlEncode("" +A2058Indicador_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA2058Indicador_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA2058Indicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"INDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_INDICADOR_REFER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_INDICADOR_APLICA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_INDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2065Indicador_Tipo, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormRX2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("indicadorgeneral.js", "?20203117302441");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "IndicadorGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Indicador General" ;
      }

      protected void WBRX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "indicadorgeneral.aspx");
            }
            wb_table1_2_RX2( true) ;
         }
         else
         {
            wb_table1_2_RX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RX2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTRX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Indicador General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPRX0( ) ;
            }
         }
      }

      protected void WSRX2( )
      {
         STARTRX2( ) ;
         EVTRX2( ) ;
      }

      protected void EVTRX2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11RX2 */
                                    E11RX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12RX2 */
                                    E12RX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13RX2 */
                                    E13RX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14RX2 */
                                    E14RX2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRX0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormRX2( ) ;
            }
         }
      }

      protected void PARX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbIndicador_Refer.Name = "INDICADOR_REFER";
            cmbIndicador_Refer.WebTags = "";
            cmbIndicador_Refer.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbIndicador_Refer.addItem("1", "Prazo", 0);
            cmbIndicador_Refer.addItem("2", "Frequ�ncia", 0);
            cmbIndicador_Refer.addItem("3", "Valores", 0);
            if ( cmbIndicador_Refer.ItemCount > 0 )
            {
               A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
               n2063Indicador_Refer = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_REFER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
            }
            cmbIndicador_Aplica.Name = "INDICADOR_APLICA";
            cmbIndicador_Aplica.WebTags = "";
            cmbIndicador_Aplica.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbIndicador_Aplica.addItem("1", "Demandas", 0);
            cmbIndicador_Aplica.addItem("2", "Lotes", 0);
            if ( cmbIndicador_Aplica.ItemCount > 0 )
            {
               A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
               n2064Indicador_Aplica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_APLICA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
            }
            dynIndicador_NaoCnfCod.Name = "INDICADOR_NAOCNFCOD";
            dynIndicador_NaoCnfCod.WebTags = "";
            cmbIndicador_Tipo.Name = "INDICADOR_TIPO";
            cmbIndicador_Tipo.WebTags = "";
            cmbIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
            cmbIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
            cmbIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
            cmbIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
            cmbIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
            cmbIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
            cmbIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
            cmbIndicador_Tipo.addItem("T", "Tempestividade", 0);
            if ( cmbIndicador_Tipo.ItemCount > 0 )
            {
               A2065Indicador_Tipo = cmbIndicador_Tipo.getValidValue(A2065Indicador_Tipo);
               n2065Indicador_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2065Indicador_Tipo, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAINDICADOR_NAOCNFCODRX2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAINDICADOR_NAOCNFCOD_dataRX2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAINDICADOR_NAOCNFCOD_htmlRX2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLAINDICADOR_NAOCNFCOD_dataRX2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynIndicador_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynIndicador_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynIndicador_NaoCnfCod.ItemCount > 0 )
         {
            A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( dynIndicador_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0))), "."));
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
         }
      }

      protected void GXDLAINDICADOR_NAOCNFCOD_dataRX2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00RX2 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00RX2_A2062Indicador_NaoCnfCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00RX2_A427NaoConformidade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbIndicador_Refer.ItemCount > 0 )
         {
            A2063Indicador_Refer = (short)(NumberUtil.Val( cmbIndicador_Refer.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0))), "."));
            n2063Indicador_Refer = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_REFER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
         }
         if ( cmbIndicador_Aplica.ItemCount > 0 )
         {
            A2064Indicador_Aplica = (short)(NumberUtil.Val( cmbIndicador_Aplica.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0))), "."));
            n2064Indicador_Aplica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_APLICA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
         }
         if ( dynIndicador_NaoCnfCod.ItemCount > 0 )
         {
            A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( dynIndicador_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0))), "."));
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
         }
         if ( cmbIndicador_Tipo.ItemCount > 0 )
         {
            A2065Indicador_Tipo = cmbIndicador_Tipo.getValidValue(A2065Indicador_Tipo);
            n2065Indicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2065Indicador_Tipo, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "IndicadorGeneral";
         context.Gx_err = 0;
      }

      protected void RFRX2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00RX3 */
            pr_default.execute(1, new Object[] {A2058Indicador_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A2065Indicador_Tipo = H00RX3_A2065Indicador_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2065Indicador_Tipo, ""))));
               n2065Indicador_Tipo = H00RX3_n2065Indicador_Tipo[0];
               A2062Indicador_NaoCnfCod = H00RX3_A2062Indicador_NaoCnfCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
               n2062Indicador_NaoCnfCod = H00RX3_n2062Indicador_NaoCnfCod[0];
               A2064Indicador_Aplica = H00RX3_A2064Indicador_Aplica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_APLICA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
               n2064Indicador_Aplica = H00RX3_n2064Indicador_Aplica[0];
               A2063Indicador_Refer = H00RX3_A2063Indicador_Refer[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_REFER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
               n2063Indicador_Refer = H00RX3_n2063Indicador_Refer[0];
               GXAINDICADOR_NAOCNFCOD_htmlRX2( AV6WWPContext) ;
               /* Execute user event: E12RX2 */
               E12RX2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBRX0( ) ;
         }
      }

      protected void STRUPRX0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "IndicadorGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11RX2 */
         E11RX2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXAINDICADOR_NAOCNFCOD_htmlRX2( AV6WWPContext) ;
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbIndicador_Refer.CurrentValue = cgiGet( cmbIndicador_Refer_Internalname);
            A2063Indicador_Refer = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Refer_Internalname), "."));
            n2063Indicador_Refer = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2063Indicador_Refer", StringUtil.LTrim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_REFER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2063Indicador_Refer), "ZZZ9")));
            cmbIndicador_Aplica.CurrentValue = cgiGet( cmbIndicador_Aplica_Internalname);
            A2064Indicador_Aplica = (short)(NumberUtil.Val( cgiGet( cmbIndicador_Aplica_Internalname), "."));
            n2064Indicador_Aplica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2064Indicador_Aplica", StringUtil.LTrim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_APLICA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2064Indicador_Aplica), "ZZZ9")));
            dynIndicador_NaoCnfCod.CurrentValue = cgiGet( dynIndicador_NaoCnfCod_Internalname);
            A2062Indicador_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynIndicador_NaoCnfCod_Internalname), "."));
            n2062Indicador_NaoCnfCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2062Indicador_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_NAOCNFCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2062Indicador_NaoCnfCod), "ZZZZZ9")));
            cmbIndicador_Tipo.CurrentValue = cgiGet( cmbIndicador_Tipo_Internalname);
            A2065Indicador_Tipo = cgiGet( cmbIndicador_Tipo_Internalname);
            n2065Indicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2065Indicador_Tipo", A2065Indicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_INDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2065Indicador_Tipo, ""))));
            /* Read saved values. */
            wcpOA2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2058Indicador_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXAINDICADOR_NAOCNFCOD_htmlRX2( AV6WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11RX2 */
         E11RX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11RX2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12RX2( )
      {
         /* Load Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13RX2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2058Indicador_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14RX2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2058Indicador_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Indicador";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Indicador_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Indicador_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_RX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RX2( true) ;
         }
         else
         {
            wb_table2_8_RX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_RX2( true) ;
         }
         else
         {
            wb_table3_31_RX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_31_RX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RX2e( true) ;
         }
         else
         {
            wb_table1_2_RX2e( false) ;
         }
      }

      protected void wb_table3_31_RX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 7, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e15rx1_client"+"'", TempTags, "", 2, "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_RX2e( true) ;
         }
         else
         {
            wb_table3_31_RX2e( false) ;
         }
      }

      protected void wb_table2_8_RX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_refer_Internalname, "Refere-se a", "", "", lblTextblockindicador_refer_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Refer, cmbIndicador_Refer_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0)), 1, cmbIndicador_Refer_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_IndicadorGeneral.htm");
            cmbIndicador_Refer.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbIndicador_Refer_Internalname, "Values", (String)(cmbIndicador_Refer.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_aplica_Internalname, "Aplica-se a", "", "", lblTextblockindicador_aplica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Aplica, cmbIndicador_Aplica_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0)), 1, cmbIndicador_Aplica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_IndicadorGeneral.htm");
            cmbIndicador_Aplica.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbIndicador_Aplica_Internalname, "Values", (String)(cmbIndicador_Aplica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_naocnfcod_Internalname, "N�o Conformidade", "", "", lblTextblockindicador_naocnfcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynIndicador_NaoCnfCod, dynIndicador_NaoCnfCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0)), 1, dynIndicador_NaoCnfCod_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_IndicadorGeneral.htm");
            dynIndicador_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynIndicador_NaoCnfCod_Internalname, "Values", (String)(dynIndicador_NaoCnfCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_tipo_Internalname, "Associado a", "", "", lblTextblockindicador_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_IndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbIndicador_Tipo, cmbIndicador_Tipo_Internalname, StringUtil.RTrim( A2065Indicador_Tipo), 1, cmbIndicador_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_IndicadorGeneral.htm");
            cmbIndicador_Tipo.CurrentValue = StringUtil.RTrim( A2065Indicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbIndicador_Tipo_Internalname, "Values", (String)(cmbIndicador_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RX2e( true) ;
         }
         else
         {
            wb_table2_8_RX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A2058Indicador_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARX2( ) ;
         WSRX2( ) ;
         WERX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA2058Indicador_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PARX2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "indicadorgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PARX2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A2058Indicador_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         }
         wcpOA2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2058Indicador_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A2058Indicador_Codigo != wcpOA2058Indicador_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA2058Indicador_Codigo = A2058Indicador_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA2058Indicador_Codigo = cgiGet( sPrefix+"A2058Indicador_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA2058Indicador_Codigo) > 0 )
         {
            A2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA2058Indicador_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2058Indicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0)));
         }
         else
         {
            A2058Indicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A2058Indicador_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PARX2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSRX2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSRX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A2058Indicador_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2058Indicador_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA2058Indicador_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A2058Indicador_Codigo_CTRL", StringUtil.RTrim( sCtrlA2058Indicador_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WERX2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117302467");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("indicadorgeneral.js", "?20203117302467");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockindicador_refer_Internalname = sPrefix+"TEXTBLOCKINDICADOR_REFER";
         cmbIndicador_Refer_Internalname = sPrefix+"INDICADOR_REFER";
         lblTextblockindicador_aplica_Internalname = sPrefix+"TEXTBLOCKINDICADOR_APLICA";
         cmbIndicador_Aplica_Internalname = sPrefix+"INDICADOR_APLICA";
         lblTextblockindicador_naocnfcod_Internalname = sPrefix+"TEXTBLOCKINDICADOR_NAOCNFCOD";
         dynIndicador_NaoCnfCod_Internalname = sPrefix+"INDICADOR_NAOCNFCOD";
         lblTextblockindicador_tipo_Internalname = sPrefix+"TEXTBLOCKINDICADOR_TIPO";
         cmbIndicador_Tipo_Internalname = sPrefix+"INDICADOR_TIPO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbIndicador_Tipo_Jsonclick = "";
         dynIndicador_NaoCnfCod_Jsonclick = "";
         cmbIndicador_Aplica_Jsonclick = "";
         cmbIndicador_Refer_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13RX2',iparms:[{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14RX2',iparms:[{av:'A2058Indicador_Codigo',fld:'INDICADOR_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15RX1',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2065Indicador_Tipo = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00RX2_A2062Indicador_NaoCnfCod = new int[1] ;
         H00RX2_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         H00RX2_A427NaoConformidade_Nome = new String[] {""} ;
         H00RX2_n427NaoConformidade_Nome = new bool[] {false} ;
         H00RX2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00RX2_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00RX3_A2058Indicador_Codigo = new int[1] ;
         H00RX3_A2065Indicador_Tipo = new String[] {""} ;
         H00RX3_n2065Indicador_Tipo = new bool[] {false} ;
         H00RX3_A2062Indicador_NaoCnfCod = new int[1] ;
         H00RX3_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         H00RX3_A2064Indicador_Aplica = new short[1] ;
         H00RX3_n2064Indicador_Aplica = new bool[] {false} ;
         H00RX3_A2063Indicador_Refer = new short[1] ;
         H00RX3_n2063Indicador_Refer = new bool[] {false} ;
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockindicador_refer_Jsonclick = "";
         lblTextblockindicador_aplica_Jsonclick = "";
         lblTextblockindicador_naocnfcod_Jsonclick = "";
         lblTextblockindicador_tipo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA2058Indicador_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.indicadorgeneral__default(),
            new Object[][] {
                new Object[] {
               H00RX2_A2062Indicador_NaoCnfCod, H00RX2_A427NaoConformidade_Nome, H00RX2_n427NaoConformidade_Nome, H00RX2_A428NaoConformidade_AreaTrabalhoCod, H00RX2_n428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               H00RX3_A2058Indicador_Codigo, H00RX3_A2065Indicador_Tipo, H00RX3_n2065Indicador_Tipo, H00RX3_A2062Indicador_NaoCnfCod, H00RX3_n2062Indicador_NaoCnfCod, H00RX3_A2064Indicador_Aplica, H00RX3_n2064Indicador_Aplica, H00RX3_A2063Indicador_Refer, H00RX3_n2063Indicador_Refer
               }
            }
         );
         AV14Pgmname = "IndicadorGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "IndicadorGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A2063Indicador_Refer ;
      private short A2064Indicador_Aplica ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A2058Indicador_Codigo ;
      private int wcpOA2058Indicador_Codigo ;
      private int A2062Indicador_NaoCnfCod ;
      private int gxdynajaxindex ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Indicador_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A2065Indicador_Tipo ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String cmbIndicador_Refer_Internalname ;
      private String cmbIndicador_Aplica_Internalname ;
      private String dynIndicador_NaoCnfCod_Internalname ;
      private String cmbIndicador_Tipo_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockindicador_refer_Internalname ;
      private String lblTextblockindicador_refer_Jsonclick ;
      private String cmbIndicador_Refer_Jsonclick ;
      private String lblTextblockindicador_aplica_Internalname ;
      private String lblTextblockindicador_aplica_Jsonclick ;
      private String cmbIndicador_Aplica_Jsonclick ;
      private String lblTextblockindicador_naocnfcod_Internalname ;
      private String lblTextblockindicador_naocnfcod_Jsonclick ;
      private String dynIndicador_NaoCnfCod_Jsonclick ;
      private String lblTextblockindicador_tipo_Internalname ;
      private String lblTextblockindicador_tipo_Jsonclick ;
      private String cmbIndicador_Tipo_Jsonclick ;
      private String sCtrlA2058Indicador_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2063Indicador_Refer ;
      private bool n2064Indicador_Aplica ;
      private bool n2065Indicador_Tipo ;
      private bool n2062Indicador_NaoCnfCod ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbIndicador_Refer ;
      private GXCombobox cmbIndicador_Aplica ;
      private GXCombobox dynIndicador_NaoCnfCod ;
      private GXCombobox cmbIndicador_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00RX2_A2062Indicador_NaoCnfCod ;
      private bool[] H00RX2_n2062Indicador_NaoCnfCod ;
      private String[] H00RX2_A427NaoConformidade_Nome ;
      private bool[] H00RX2_n427NaoConformidade_Nome ;
      private int[] H00RX2_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00RX2_n428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00RX3_A2058Indicador_Codigo ;
      private String[] H00RX3_A2065Indicador_Tipo ;
      private bool[] H00RX3_n2065Indicador_Tipo ;
      private int[] H00RX3_A2062Indicador_NaoCnfCod ;
      private bool[] H00RX3_n2062Indicador_NaoCnfCod ;
      private short[] H00RX3_A2064Indicador_Aplica ;
      private bool[] H00RX3_n2064Indicador_Aplica ;
      private short[] H00RX3_A2063Indicador_Refer ;
      private bool[] H00RX3_n2063Indicador_Refer ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class indicadorgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RX2 ;
          prmH00RX2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RX3 ;
          prmH00RX3 = new Object[] {
          new Object[] {"@Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RX2", "SELECT [NaoConformidade_Codigo] AS Indicador_NaoCnfCod, [NaoConformidade_Nome], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RX2,0,0,true,false )
             ,new CursorDef("H00RX3", "SELECT [Indicador_Codigo], [Indicador_Tipo], [Indicador_NaoCnfCod] AS Indicador_NaoCnfCod, [Indicador_Aplica], [Indicador_Refer] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @Indicador_Codigo ORDER BY [Indicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RX3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
