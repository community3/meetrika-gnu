/*
               File: type_SdtGAMSecurityPolicyFilter
        Description: GAMSecurityPolicyFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:40.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSecurityPolicyFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMSecurityPolicyFilter( )
      {
         initialize();
      }

      public SdtGAMSecurityPolicyFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSecurityPolicyFilter_externalReference == null )
         {
            GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSecurityPolicyFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.GUID ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.Name ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.Description ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.Description = value;
         }

      }

      public SdtGAMDescription gxTpr_Descriptions
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            SdtGAMDescription intValue ;
            intValue = new SdtGAMDescription(context);
            Artech.Security.GAMDescription externalParm0 ;
            externalParm0 = GAMSecurityPolicyFilter_externalReference.Descriptions;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            SdtGAMDescription intValue ;
            Artech.Security.GAMDescription externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMDescription)(intValue.ExternalInstance);
            GAMSecurityPolicyFilter_externalReference.Descriptions = externalParm1;
         }

      }

      public bool gxTpr_Loaddescriptions
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.LoadDescriptions ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.LoadDescriptions = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.Start ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.Limit ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.Limit = value;
         }

      }

      public int gxTpr_Id
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.Id ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.Id = value;
         }

      }

      public SdtGAMProperty gxTpr_Properties
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            SdtGAMProperty intValue ;
            intValue = new SdtGAMProperty(context);
            Artech.Security.GAMProperty externalParm2 ;
            externalParm2 = GAMSecurityPolicyFilter_externalReference.Properties;
            intValue.ExternalInstance = externalParm2;
            return intValue ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            SdtGAMProperty intValue ;
            Artech.Security.GAMProperty externalParm3 ;
            intValue = value;
            externalParm3 = (Artech.Security.GAMProperty)(intValue.ExternalInstance);
            GAMSecurityPolicyFilter_externalReference.Properties = externalParm3;
         }

      }

      public bool gxTpr_Loadproperties
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference.LoadProperties ;
         }

         set {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            GAMSecurityPolicyFilter_externalReference.LoadProperties = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSecurityPolicyFilter_externalReference == null )
            {
               GAMSecurityPolicyFilter_externalReference = new Artech.Security.GAMSecurityPolicyFilter(context);
            }
            return GAMSecurityPolicyFilter_externalReference ;
         }

         set {
            GAMSecurityPolicyFilter_externalReference = (Artech.Security.GAMSecurityPolicyFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSecurityPolicyFilter GAMSecurityPolicyFilter_externalReference=null ;
   }

}
