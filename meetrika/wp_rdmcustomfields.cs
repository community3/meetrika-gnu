/*
               File: WP_RdmCustomFields
        Description: Integra��o Redmine
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:59:55.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_rdmcustomfields : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_rdmcustomfields( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_rdmcustomfields( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavProjeto = new GXCombobox();
         cmbavTracker = new GXCombobox();
         cmbavStatus = new GXCombobox();
         cmbavCtlpara = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_62_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKN2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKN2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118595535");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_rdmcustomfields.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_customfields", AV29SDT_CustomFields);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_customfields", AV29SDT_CustomFields);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_62", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Servico_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ISSUES", AV15SDT_Issues);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ISSUES", AV15SDT_Issues);
         }
         GxWebStd.gx_hidden_field( context, "vTXT", StringUtil.RTrim( AV32txt));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CUSTOMFIELDITEM", AV30SDT_CustomFieldItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CUSTOMFIELDITEM", AV30SDT_CustomFieldItem);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKN2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKN2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_rdmcustomfields.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_RdmCustomFields" ;
      }

      public override String GetPgmdesc( )
      {
         return "Integra��o Redmine" ;
      }

      protected void WBKN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KN2( true) ;
         }
         else
         {
            wb_table1_2_KN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p></p>") ;
         }
         wbLoad = true;
      }

      protected void STARTKN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Integra��o Redmine", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKN0( ) ;
      }

      protected void WSKN2( )
      {
         STARTKN2( ) ;
         EVTKN2( ) ;
      }

      protected void EVTKN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CARREGARFILTROS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KN2 */
                              E11KN2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12KN2 */
                                    E12KN2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
                              SubsflControlProps_622( ) ;
                              AV35GXV1 = nGXsfl_62_idx;
                              if ( ( AV29SDT_CustomFields.Count >= AV35GXV1 ) && ( AV35GXV1 > 0 ) )
                              {
                                 AV29SDT_CustomFields.CurrentItem = ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13KN2 */
                                    E13KN2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14KN2 */
                                    E14KN2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavProjeto.Name = "vPROJETO";
            cmbavProjeto.WebTags = "";
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos)", 0);
            if ( cmbavProjeto.ItemCount > 0 )
            {
               AV14Projeto = (short)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0)));
            }
            cmbavTracker.Name = "vTRACKER";
            cmbavTracker.WebTags = "";
            cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos)", 0);
            if ( cmbavTracker.ItemCount > 0 )
            {
               AV22Tracker = (short)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0)));
            }
            cmbavStatus.Name = "vSTATUS";
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV20Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Status), 4, 0)));
            }
            GXCCtl = "CTLPARA_" + sGXsfl_62_idx;
            cmbavCtlpara.Name = GXCCtl;
            cmbavCtlpara.WebTags = "";
            cmbavCtlpara.addItem("", "(Nenhum)", 0);
            cmbavCtlpara.addItem("N� OS", "N� OS", 0);
            cmbavCtlpara.addItem("N� OS Refer�ncia", "N� OS Refer�ncia", 0);
            cmbavCtlpara.addItem("Bruto Refer�ncia", "Bruto Refer�ncia", 0);
            cmbavCtlpara.addItem("Liquido Refer�ncia", "Liquido Refer�ncia", 0);
            cmbavCtlpara.addItem("Sistema", "Sistema", 0);
            cmbavCtlpara.addItem("M�dulo", "M�dulo", 0);
            if ( cmbavCtlpara.ItemCount > 0 )
            {
               if ( ( AV35GXV1 > 0 ) && ( AV29SDT_CustomFields.Count >= AV35GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para)) )
               {
                  ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para = cmbavCtlpara.getValidValue(((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para);
               }
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavHost_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_622( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_622( ) ;
            nGXsfl_62_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
            SubsflControlProps_622( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFKN2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavProjeto.ItemCount > 0 )
         {
            AV14Projeto = (short)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0)));
         }
         if ( cmbavTracker.ItemCount > 0 )
         {
            AV22Tracker = (short)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0)));
         }
         if ( cmbavStatus.ItemCount > 0 )
         {
            AV20Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Status), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
      }

      protected void RFKN2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 62;
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         nGXsfl_62_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_622( ) ;
            /* Execute user event: E14KN2 */
            E14KN2 ();
            wbEnd = 62;
            WBKN0( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPKN0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13KN2 */
         E13KN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_customfields"), AV29SDT_CustomFields);
            /* Read variables values. */
            AV10Host = cgiGet( edtavHost_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Host", AV10Host);
            AV24Url = cgiGet( edtavUrl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Url", AV24Url);
            AV12Key = cgiGet( edtavKey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Key", AV12Key);
            cmbavProjeto.Name = cmbavProjeto_Internalname;
            cmbavProjeto.CurrentValue = cgiGet( cmbavProjeto_Internalname);
            AV14Projeto = (short)(NumberUtil.Val( cgiGet( cmbavProjeto_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0)));
            cmbavTracker.Name = cmbavTracker_Internalname;
            cmbavTracker.CurrentValue = cgiGet( cmbavTracker_Internalname);
            AV22Tracker = (short)(NumberUtil.Val( cgiGet( cmbavTracker_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0)));
            cmbavStatus.Name = cmbavStatus_Internalname;
            cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
            AV20Status = (short)(NumberUtil.Val( cgiGet( cmbavStatus_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Status), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavCreated_from_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Created_From"}), 1, "vCREATED_FROM");
               GX_FocusControl = edtavCreated_from_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7Created_From = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Created_From", context.localUtil.TToC( AV7Created_From, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV7Created_From = context.localUtil.CToT( cgiGet( edtavCreated_from_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Created_From", context.localUtil.TToC( AV7Created_From, 10, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavCreated_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Created_To"}), 1, "vCREATED_TO");
               GX_FocusControl = edtavCreated_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Created_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Created_To", context.localUtil.TToC( AV8Created_To, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV8Created_To = context.localUtil.CToT( cgiGet( edtavCreated_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Created_To", context.localUtil.TToC( AV8Created_To, 10, 5, 0, 3, "/", ":", " "));
            }
            /* Read saved values. */
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_62"), ",", "."));
            nGXsfl_62_fel_idx = 0;
            while ( nGXsfl_62_fel_idx < nRC_GXsfl_62 )
            {
               nGXsfl_62_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_62_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_62_fel_idx+1));
               sGXsfl_62_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_622( ) ;
               AV35GXV1 = nGXsfl_62_fel_idx;
               if ( ( AV29SDT_CustomFields.Count >= AV35GXV1 ) && ( AV35GXV1 > 0 ) )
               {
                  AV29SDT_CustomFields.CurrentItem = ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1));
               }
            }
            if ( nGXsfl_62_fel_idx == 0 )
            {
               nGXsfl_62_idx = 1;
               sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
               SubsflControlProps_622( ) ;
            }
            nGXsfl_62_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13KN2 */
         E13KN2 ();
         if (returnInSub) return;
      }

      protected void E13KN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV26WWPContext) ;
         /* Using cursor H00KN2 */
         pr_default.execute(0, new Object[] {AV26WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = H00KN2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00KN2_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00KN2_A5AreaTrabalho_Codigo[0];
            /* Using cursor H00KN3 */
            pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1384Redmine_ContratanteCod = H00KN3_A1384Redmine_ContratanteCod[0];
               A1381Redmine_Host = H00KN3_A1381Redmine_Host[0];
               A1383Redmine_Key = H00KN3_A1383Redmine_Key[0];
               A1391Redmine_Versao = H00KN3_A1391Redmine_Versao[0];
               n1391Redmine_Versao = H00KN3_n1391Redmine_Versao[0];
               AV10Host = A1381Redmine_Host;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Host", AV10Host);
               AV12Key = A1383Redmine_Key;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Key", AV12Key);
               AV25Versao = A1391Redmine_Versao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25Versao)) )
         {
            AV25Versao = "223";
         }
         else
         {
            AV25Versao = StringUtil.StringReplace( AV25Versao, ".", "");
         }
         /* Execute user subroutine: 'CARREGARFILTROS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E11KN2( )
      {
         /* 'CarregarFiltros' Routine */
         /* Execute user subroutine: 'CARREGARFILTROS' */
         S112 ();
         if (returnInSub) return;
         cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", cmbavStatus.ToJavascriptSource());
         cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", cmbavProjeto.ToJavascriptSource());
         cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", cmbavTracker.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E12KN2 */
         E12KN2 ();
         if (returnInSub) return;
      }

      protected void E12KN2( )
      {
         /* Enter Routine */
         if ( (0==AV14Projeto) )
         {
            GX_msglist.addItem("Informe o Projeto!");
         }
         else
         {
            AV11httpclient.Host = StringUtil.Trim( AV10Host);
            AV11httpclient.BaseURL = StringUtil.Trim( AV24Url);
            AV9Execute = "issues.xml?project_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0)) + "&include=journals&sort=&offset=0&limit=1&page=";
            if ( ! (0==AV20Status) )
            {
               AV9Execute = AV9Execute + "&status_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0));
            }
            if ( ! (0==AV22Tracker) )
            {
               AV9Execute = AV9Execute + "&tracker_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0));
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Key)) )
            {
               AV9Execute = AV9Execute + "&key=" + StringUtil.Trim( AV12Key);
            }
            if ( ! (DateTime.MinValue==AV7Created_From) )
            {
               AV9Execute = AV9Execute + "&created_on=" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV7Created_From)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV7Created_From)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( AV7Created_From)), 10, 0)) + "|";
            }
            if ( ! (DateTime.MinValue==AV8Created_To) )
            {
               if ( (DateTime.MinValue==AV7Created_From) )
               {
                  AV9Execute = AV9Execute + "|";
               }
               AV9Execute = AV9Execute + "&created_on=" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV8Created_To)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV8Created_To)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( AV8Created_To)), 10, 0)) + "|";
            }
            AV11httpclient.Execute("GET", AV9Execute);
            GX_msglist.addItem(StringUtil.StringReplace( StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0), StringUtil.Str( (decimal)(AV6Contratada_Codigo), 6, 0), "")+StringUtil.StringReplace( StringUtil.Str( (decimal)(AV19Servico_Codigo), 6, 0), StringUtil.Str( (decimal)(AV19Servico_Codigo), 6, 0), ""));
            if ( AV11httpclient.ErrCode == 0 )
            {
               AV15SDT_Issues.gxTpr_Issues.Clear();
               AV15SDT_Issues.FromXml(AV11httpclient.ToString(), "");
               /* Execute user subroutine: 'CUSTOMFIELDS' */
               S122 ();
               if (returnInSub) return;
            }
            else
            {
               GX_msglist.addItem(AV11httpclient.ErrDescription);
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15SDT_Issues", AV15SDT_Issues);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29SDT_CustomFields", AV29SDT_CustomFields);
         nGXsfl_62_bak_idx = nGXsfl_62_idx;
         gxgrGrid1_refresh( ) ;
         nGXsfl_62_idx = nGXsfl_62_bak_idx;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30SDT_CustomFieldItem", AV30SDT_CustomFieldItem);
      }

      protected void S132( )
      {
         /* 'GETPROJECTS' Routine */
         AV11httpclient.Host = StringUtil.Trim( AV10Host);
         AV11httpclient.BaseURL = StringUtil.Trim( AV24Url);
         AV9Execute = "projects.xml?";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Key)) )
         {
            AV9Execute = AV9Execute + "&key=" + AV12Key;
         }
         AV11httpclient.Execute("GET", AV9Execute);
         if ( AV11httpclient.ErrCode == 0 )
         {
            AV16SDT_Project.FromXml(AV11httpclient.ToString(), "");
            AV16SDT_Project.gxTpr_Projects.Sort("name") ;
            cmbavProjeto.removeAllItems();
            cmbavProjeto.addItem("0", "(Todos)", 0);
            AV40GXV2 = 1;
            while ( AV40GXV2 <= AV16SDT_Project.gxTpr_Projects.Count )
            {
               AV13ProjectItem = ((SdtSDT_Redmineprojects_project)AV16SDT_Project.gxTpr_Projects.Item(AV40GXV2));
               cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV13ProjectItem.gxTpr_Id), 4, 0)), AV13ProjectItem.gxTpr_Name, 0);
               AV40GXV2 = (int)(AV40GXV2+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV11httpclient.ErrDescription);
         }
      }

      protected void S142( )
      {
         /* 'GETSTATUS' Routine */
         AV11httpclient.Host = StringUtil.Trim( AV10Host);
         AV11httpclient.BaseURL = StringUtil.Trim( AV24Url);
         AV9Execute = "issue_statuses.xml";
         AV11httpclient.Execute("GET", AV9Execute);
         if ( AV11httpclient.ErrCode == 0 )
         {
            AV17SDT_Status.FromXml(AV11httpclient.ToString(), "");
            AV17SDT_Status.gxTpr_Issue_statuss.Sort("name") ;
            cmbavStatus.removeAllItems();
            cmbavStatus.addItem("0", "(Todos)", 0);
            AV41GXV3 = 1;
            while ( AV41GXV3 <= AV17SDT_Status.gxTpr_Issue_statuss.Count )
            {
               AV21StatusItem = ((SdtSDT_RedmineStatus_issue_status)AV17SDT_Status.gxTpr_Issue_statuss.Item(AV41GXV3));
               cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV21StatusItem.gxTpr_Id), 4, 0)), AV21StatusItem.gxTpr_Name, 0);
               AV41GXV3 = (int)(AV41GXV3+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV11httpclient.ErrDescription);
         }
      }

      protected void S152( )
      {
         /* 'GETTRACKERS' Routine */
         AV11httpclient.Host = StringUtil.Trim( AV10Host);
         AV11httpclient.BaseURL = StringUtil.Trim( AV24Url);
         AV9Execute = "trackers.xml";
         AV11httpclient.Execute("GET", AV9Execute);
         if ( AV11httpclient.ErrCode == 0 )
         {
            AV18SDT_Trackers.FromXml(AV11httpclient.ToString(), "");
            AV18SDT_Trackers.gxTpr_Trackers.Sort("name") ;
            cmbavTracker.removeAllItems();
            cmbavTracker.addItem("0", "(Todos)", 0);
            AV42GXV4 = 1;
            while ( AV42GXV4 <= AV18SDT_Trackers.gxTpr_Trackers.Count )
            {
               AV23TrackersItem = ((SdtSDT_RedmineTrackers_tracker)AV18SDT_Trackers.gxTpr_Trackers.Item(AV42GXV4));
               cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV23TrackersItem.gxTpr_Id), 4, 0)), AV23TrackersItem.gxTpr_Name, 0);
               AV42GXV4 = (int)(AV42GXV4+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV11httpclient.ErrDescription);
         }
      }

      protected void S112( )
      {
         /* 'CARREGARFILTROS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Host)) )
         {
            /* Execute user subroutine: 'GETSTATUS' */
            S142 ();
            if (returnInSub) return;
            if ( AV11httpclient.ErrCode == 0 )
            {
               /* Execute user subroutine: 'GETPROJECTS' */
               S132 ();
               if (returnInSub) return;
               if ( AV11httpclient.ErrCode == 0 )
               {
                  /* Execute user subroutine: 'GETTRACKERS' */
                  S152 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               GX_msglist.addItem(AV11httpclient.ErrDescription);
            }
         }
      }

      protected void S122( )
      {
         /* 'CUSTOMFIELDS' Routine */
         AV29SDT_CustomFields.Clear();
         gx_BV62 = true;
         AV27XMLReader.OpenFromString(AV11httpclient.ToString());
         if ( new prc_rdmcompativel(context).executeUdp(  AV11httpclient.ToString(), out  AV32txt) )
         {
            AV27XMLReader.ReadType(1, "custom_fields");
            AV27XMLReader.Read();
            while ( StringUtil.StrCmp(AV27XMLReader.Name, "custom_fields") != 0 )
            {
               AV30SDT_CustomFieldItem.gxTpr_Name = AV27XMLReader.GetAttributeByName("name");
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30SDT_CustomFieldItem.gxTpr_Name)) )
               {
                  AV29SDT_CustomFields.Add(AV30SDT_CustomFieldItem, 0);
                  gx_BV62 = true;
                  AV30SDT_CustomFieldItem = new SdtSDT_CustomFields_Item(context);
               }
               AV27XMLReader.Read();
            }
         }
         else
         {
            GX_msglist.addItem("Elemento esperado: "+AV32txt);
         }
         AV27XMLReader.Close();
      }

      private void E14KN2( )
      {
         /* Grid1_Load Routine */
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV29SDT_CustomFields.Count )
         {
            AV29SDT_CustomFields.CurrentItem = ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 62;
            }
            sendrow_622( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(62, Grid1Row);
            }
            AV35GXV1 = (short)(AV35GXV1+1);
         }
      }

      protected void wb_table1_2_KN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(157), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(232), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Host:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavHost_Internalname, StringUtil.RTrim( AV10Host), StringUtil.RTrim( context.localUtil.Format( AV10Host, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavHost_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Url:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUrl_Internalname, StringUtil.RTrim( AV24Url), StringUtil.RTrim( context.localUtil.Format( AV24Url, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUrl_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "Key:", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_62_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavKey_Internalname, StringUtil.RTrim( AV12Key), StringUtil.RTrim( context.localUtil.Format( AV12Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavKey_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(62), 2, 0)+","+"null"+");", "Carregar Filtros do Redmine", bttButton2_Jsonclick, 5, "Carregar Filtros do Redmine", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CARREGARFILTROS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "Projeto a importar:", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto, cmbavProjeto_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0)), 1, cmbavProjeto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WP_RdmCustomFields.htm");
            cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Projeto), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", (String)(cmbavProjeto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock36_Internalname, "Tracker:", "", "", lblTextblock36_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTracker, cmbavTracker_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0)), 1, cmbavTracker_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_WP_RdmCustomFields.htm");
            cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22Tracker), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", (String)(cmbavTracker.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Status:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_62_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatus, cmbavStatus_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0)), 1, cmbavStatus_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WP_RdmCustomFields.htm");
            cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock42_Internalname, "Periodo:", "", "", lblTextblock42_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_62_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCreated_from_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCreated_from_Internalname, context.localUtil.TToC( AV7Created_From, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV7Created_From, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCreated_from_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RdmCustomFields.htm");
            GxWebStd.gx_bitmap( context, edtavCreated_from_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock43_Internalname, "-", "", "", lblTextblock43_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_62_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCreated_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCreated_to_Internalname, context.localUtil.TToC( AV8Created_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV8Created_To, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCreated_to_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RdmCustomFields.htm");
            GxWebStd.gx_bitmap( context, edtavCreated_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:16px")+"\">") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(62), 2, 0)+","+"null"+");", "Consultar", bttButton1_Jsonclick, 5, "Consultar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RdmCustomFields.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"62\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "De") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Para") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlname_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 62 )
         {
            wbEnd = 0;
            nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV35GXV1 = nGXsfl_62_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KN2e( true) ;
         }
         else
         {
            wb_table1_2_KN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKN2( ) ;
         WSKN2( ) ;
         WEKN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311859565");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_rdmcustomfields.js", "?2020311859565");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_622( )
      {
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_62_idx;
         cmbavCtlpara_Internalname = "CTLPARA_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_622( )
      {
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_62_fel_idx;
         cmbavCtlpara_Internalname = "CTLPARA_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_622( )
      {
         SubsflControlProps_622( ) ;
         WBKN0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_62_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_62_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname_Internalname,StringUtil.RTrim( ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Name),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlname_Enabled,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavCtlpara.Enabled!=0)&&(cmbavCtlpara.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 64,'',false,'"+sGXsfl_62_idx+"',62)\"" : " ");
         if ( ( nGXsfl_62_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLPARA_" + sGXsfl_62_idx;
            cmbavCtlpara.Name = GXCCtl;
            cmbavCtlpara.WebTags = "";
            cmbavCtlpara.addItem("", "(Nenhum)", 0);
            cmbavCtlpara.addItem("N� OS", "N� OS", 0);
            cmbavCtlpara.addItem("N� OS Refer�ncia", "N� OS Refer�ncia", 0);
            cmbavCtlpara.addItem("Bruto Refer�ncia", "Bruto Refer�ncia", 0);
            cmbavCtlpara.addItem("Liquido Refer�ncia", "Liquido Refer�ncia", 0);
            cmbavCtlpara.addItem("Sistema", "Sistema", 0);
            cmbavCtlpara.addItem("M�dulo", "M�dulo", 0);
            if ( cmbavCtlpara.ItemCount > 0 )
            {
               if ( ( AV35GXV1 > 0 ) && ( AV29SDT_CustomFields.Count >= AV35GXV1 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para)) )
               {
                  ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para = cmbavCtlpara.getValidValue(((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para);
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlpara,(String)cmbavCtlpara_Internalname,StringUtil.RTrim( ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para),(short)1,(String)cmbavCtlpara_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavCtlpara.Enabled!=0)&&(cmbavCtlpara.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavCtlpara.Enabled!=0)&&(cmbavCtlpara.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,64);\"" : " "),(String)"",(bool)true});
         cmbavCtlpara.CurrentValue = StringUtil.RTrim( ((SdtSDT_CustomFields_Item)AV29SDT_CustomFields.Item(AV35GXV1)).gxTpr_Para);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlpara_Internalname, "Values", (String)(cmbavCtlpara.ToJavascriptSource()));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_62_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_62_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0");
         SubsflControlProps_622( ) ;
         /* End function sendrow_622 */
      }

      protected void init_default_properties( )
      {
         lblTextblock37_Internalname = "TEXTBLOCK37";
         edtavHost_Internalname = "vHOST";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         edtavUrl_Internalname = "vURL";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         edtavKey_Internalname = "vKEY";
         bttButton2_Internalname = "BUTTON2";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         cmbavProjeto_Internalname = "vPROJETO";
         lblTextblock36_Internalname = "TEXTBLOCK36";
         cmbavTracker_Internalname = "vTRACKER";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         cmbavStatus_Internalname = "vSTATUS";
         lblTextblock42_Internalname = "TEXTBLOCK42";
         edtavCreated_from_Internalname = "vCREATED_FROM";
         lblTextblock43_Internalname = "TEXTBLOCK43";
         edtavCreated_to_Internalname = "vCREATED_TO";
         bttButton1_Internalname = "BUTTON1";
         edtavCtlname_Internalname = "CTLNAME";
         cmbavCtlpara_Internalname = "CTLPARA";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavCtlpara_Jsonclick = "";
         cmbavCtlpara.Visible = -1;
         cmbavCtlpara.Enabled = 1;
         edtavCtlname_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavCtlname_Enabled = 0;
         subGrid1_Class = "Grid";
         edtavCreated_to_Jsonclick = "";
         edtavCreated_from_Jsonclick = "";
         cmbavStatus_Jsonclick = "";
         cmbavTracker_Jsonclick = "";
         cmbavProjeto_Jsonclick = "";
         edtavKey_Jsonclick = "";
         edtavUrl_Jsonclick = "";
         edtavHost_Jsonclick = "";
         subGrid1_Backcolorstyle = 0;
         edtavCtlname_Enabled = -1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Integra��o Redmine";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV29SDT_CustomFields',fld:'vSDT_CUSTOMFIELDS',grid:62,pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'CARREGARFILTROS'","{handler:'E11KN2',iparms:[{av:'AV10Host',fld:'vHOST',pic:'',nv:''},{av:'AV24Url',fld:'vURL',pic:'',nv:''},{av:'AV12Key',fld:'vKEY',pic:'',nv:''}],oparms:[{av:'AV20Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV14Projeto',fld:'vPROJETO',pic:'ZZZ9',nv:0},{av:'AV22Tracker',fld:'vTRACKER',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E12KN2',iparms:[{av:'AV14Projeto',fld:'vPROJETO',pic:'ZZZ9',nv:0},{av:'AV10Host',fld:'vHOST',pic:'',nv:''},{av:'AV24Url',fld:'vURL',pic:'',nv:''},{av:'AV20Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV22Tracker',fld:'vTRACKER',pic:'ZZZ9',nv:0},{av:'AV12Key',fld:'vKEY',pic:'',nv:''},{av:'AV7Created_From',fld:'vCREATED_FROM',pic:'99/99/9999 99:99',nv:''},{av:'AV8Created_To',fld:'vCREATED_TO',pic:'99/99/9999 99:99',nv:''},{av:'AV6Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV32txt',fld:'vTXT',pic:'',nv:''},{av:'AV30SDT_CustomFieldItem',fld:'vSDT_CUSTOMFIELDITEM',pic:'',nv:null},{av:'AV29SDT_CustomFields',fld:'vSDT_CUSTOMFIELDS',grid:62,pic:'',nv:null},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0}],oparms:[{av:'AV15SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV29SDT_CustomFields',fld:'vSDT_CUSTOMFIELDS',grid:62,pic:'',nv:null},{av:'AV30SDT_CustomFieldItem',fld:'vSDT_CUSTOMFIELDITEM',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV29SDT_CustomFields = new GxObjectCollection( context, "SDT_CustomFields.Item", "GxEv3Up14_Meetrika", "SdtSDT_CustomFields_Item", "GeneXus.Programs");
         AV15SDT_Issues = new SdtSDT_RedmineIssues(context);
         AV32txt = "";
         AV30SDT_CustomFieldItem = new SdtSDT_CustomFields_Item(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Grid1Container = new GXWebGrid( context);
         AV10Host = "";
         AV24Url = "";
         AV12Key = "";
         AV7Created_From = (DateTime)(DateTime.MinValue);
         AV8Created_To = (DateTime)(DateTime.MinValue);
         AV26WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H00KN2_A29Contratante_Codigo = new int[1] ;
         H00KN2_n29Contratante_Codigo = new bool[] {false} ;
         H00KN2_A5AreaTrabalho_Codigo = new int[1] ;
         H00KN3_A1380Redmine_Codigo = new int[1] ;
         H00KN3_A1384Redmine_ContratanteCod = new int[1] ;
         H00KN3_A1381Redmine_Host = new String[] {""} ;
         H00KN3_A1383Redmine_Key = new String[] {""} ;
         H00KN3_A1391Redmine_Versao = new String[] {""} ;
         H00KN3_n1391Redmine_Versao = new bool[] {false} ;
         A1381Redmine_Host = "";
         A1383Redmine_Key = "";
         A1391Redmine_Versao = "";
         AV25Versao = "";
         AV11httpclient = new GxHttpClient( context);
         AV9Execute = "";
         AV16SDT_Project = new SdtSDT_Redmineprojects(context);
         AV13ProjectItem = new SdtSDT_Redmineprojects_project(context);
         AV17SDT_Status = new SdtSDT_RedmineStatus(context);
         AV21StatusItem = new SdtSDT_RedmineStatus_issue_status(context);
         AV18SDT_Trackers = new SdtSDT_RedmineTrackers(context);
         AV23TrackersItem = new SdtSDT_RedmineTrackers_tracker(context);
         AV27XMLReader = new GXXMLReader(context.GetPhysicalPath());
         Grid1Row = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock37_Jsonclick = "";
         TempTags = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock36_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         lblTextblock42_Jsonclick = "";
         lblTextblock43_Jsonclick = "";
         bttButton1_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_rdmcustomfields__default(),
            new Object[][] {
                new Object[] {
               H00KN2_A29Contratante_Codigo, H00KN2_n29Contratante_Codigo, H00KN2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00KN3_A1380Redmine_Codigo, H00KN3_A1384Redmine_ContratanteCod, H00KN3_A1381Redmine_Host, H00KN3_A1383Redmine_Key, H00KN3_A1391Redmine_Versao, H00KN3_n1391Redmine_Versao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV35GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV14Projeto ;
      private short AV22Tracker ;
      private short AV20Status ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_62_fel_idx=1 ;
      private short GRID1_nEOF ;
      private short nGXsfl_62_bak_idx=1 ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int AV6Contratada_Codigo ;
      private int AV19Servico_Codigo ;
      private int subGrid1_Islastpage ;
      private int edtavCtlname_Enabled ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int AV40GXV2 ;
      private int AV41GXV3 ;
      private int AV42GXV4 ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_62_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV32txt ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String edtavHost_Internalname ;
      private String edtavCtlname_Internalname ;
      private String AV10Host ;
      private String AV24Url ;
      private String edtavUrl_Internalname ;
      private String AV12Key ;
      private String edtavKey_Internalname ;
      private String cmbavProjeto_Internalname ;
      private String cmbavTracker_Internalname ;
      private String cmbavStatus_Internalname ;
      private String edtavCreated_from_Internalname ;
      private String edtavCreated_to_Internalname ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String scmdbuf ;
      private String A1381Redmine_Host ;
      private String A1383Redmine_Key ;
      private String A1391Redmine_Versao ;
      private String AV25Versao ;
      private String AV9Execute ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String TempTags ;
      private String edtavHost_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String edtavUrl_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String edtavKey_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String cmbavProjeto_Jsonclick ;
      private String lblTextblock36_Internalname ;
      private String lblTextblock36_Jsonclick ;
      private String cmbavTracker_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private String lblTextblock42_Internalname ;
      private String lblTextblock42_Jsonclick ;
      private String edtavCreated_from_Jsonclick ;
      private String lblTextblock43_Internalname ;
      private String lblTextblock43_Jsonclick ;
      private String edtavCreated_to_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String cmbavCtlpara_Internalname ;
      private String ROClassString ;
      private String edtavCtlname_Jsonclick ;
      private String cmbavCtlpara_Jsonclick ;
      private DateTime AV7Created_From ;
      private DateTime AV8Created_To ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool n1391Redmine_Versao ;
      private bool gx_BV62 ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavProjeto ;
      private GXCombobox cmbavTracker ;
      private GXCombobox cmbavStatus ;
      private GXCombobox cmbavCtlpara ;
      private IDataStoreProvider pr_default ;
      private int[] H00KN2_A29Contratante_Codigo ;
      private bool[] H00KN2_n29Contratante_Codigo ;
      private int[] H00KN2_A5AreaTrabalho_Codigo ;
      private int[] H00KN3_A1380Redmine_Codigo ;
      private int[] H00KN3_A1384Redmine_ContratanteCod ;
      private String[] H00KN3_A1381Redmine_Host ;
      private String[] H00KN3_A1383Redmine_Key ;
      private String[] H00KN3_A1391Redmine_Versao ;
      private bool[] H00KN3_n1391Redmine_Versao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXXMLReader AV27XMLReader ;
      private GxHttpClient AV11httpclient ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CustomFields_Item ))]
      private IGxCollection AV29SDT_CustomFields ;
      private GXWebForm Form ;
      private SdtSDT_Redmineprojects AV16SDT_Project ;
      private SdtSDT_Redmineprojects_project AV13ProjectItem ;
      private SdtSDT_CustomFields_Item AV30SDT_CustomFieldItem ;
      private SdtSDT_RedmineIssues AV15SDT_Issues ;
      private SdtSDT_RedmineStatus AV17SDT_Status ;
      private SdtSDT_RedmineStatus_issue_status AV21StatusItem ;
      private SdtSDT_RedmineTrackers AV18SDT_Trackers ;
      private SdtSDT_RedmineTrackers_tracker AV23TrackersItem ;
      private wwpbaseobjects.SdtWWPContext AV26WWPContext ;
   }

   public class wp_rdmcustomfields__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KN2 ;
          prmH00KN2 = new Object[] {
          new Object[] {"@AV26WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KN3 ;
          prmH00KN3 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KN2", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV26WWPC_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KN2,1,0,true,true )
             ,new CursorDef("H00KN3", "SELECT TOP 1 [Redmine_Codigo], [Redmine_ContratanteCod], [Redmine_Host], [Redmine_Key], [Redmine_Versao] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KN3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
