/*
               File: type_SdtSDT_AuntenticacaoOUT_Usuario
        Description: SDT_AuntenticacaoOUT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:58.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_AuntenticacaoOUT.Usuario" )]
   [XmlType(TypeName =  "SDT_AuntenticacaoOUT.Usuario" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_AuntenticacaoOUT_Usuario_Perfil ))]
   [Serializable]
   public class SdtSDT_AuntenticacaoOUT_Usuario : GxUserType
   {
      public SdtSDT_AuntenticacaoOUT_Usuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar = "";
      }

      public SdtSDT_AuntenticacaoOUT_Usuario( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_AuntenticacaoOUT_Usuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_AuntenticacaoOUT_Usuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_AuntenticacaoOUT_Usuario obj ;
         obj = this;
         obj.gxTpr_Usuario_codigo = deserialized.gxTpr_Usuario_codigo;
         obj.gxTpr_Usuario_areatrabalhocod = deserialized.gxTpr_Usuario_areatrabalhocod;
         obj.gxTpr_Usuario_areatrabalhodes = deserialized.gxTpr_Usuario_areatrabalhodes;
         obj.gxTpr_Usuario_cargocod = deserialized.gxTpr_Usuario_cargocod;
         obj.gxTpr_Usuario_cargonom = deserialized.gxTpr_Usuario_cargonom;
         obj.gxTpr_Usuario_cargouocod = deserialized.gxTpr_Usuario_cargouocod;
         obj.gxTpr_Usuario_cargouonom = deserialized.gxTpr_Usuario_cargouonom;
         obj.gxTpr_Usuario_entidade = deserialized.gxTpr_Usuario_entidade;
         obj.gxTpr_Usuario_pessoacod = deserialized.gxTpr_Usuario_pessoacod;
         obj.gxTpr_Usuario_pessoanom = deserialized.gxTpr_Usuario_pessoanom;
         obj.gxTpr_Usuario_pessoatip = deserialized.gxTpr_Usuario_pessoatip;
         obj.gxTpr_Usuario_pessoadoc = deserialized.gxTpr_Usuario_pessoadoc;
         obj.gxTpr_Usuario_usergamguid = deserialized.gxTpr_Usuario_usergamguid;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Usuario_ehcontador = deserialized.gxTpr_Usuario_ehcontador;
         obj.gxTpr_Usuario_ehauditorfm = deserialized.gxTpr_Usuario_ehauditorfm;
         obj.gxTpr_Usuario_ehcontratada = deserialized.gxTpr_Usuario_ehcontratada;
         obj.gxTpr_Usuario_ehcontratante = deserialized.gxTpr_Usuario_ehcontratante;
         obj.gxTpr_Usuario_ehfinanceiro = deserialized.gxTpr_Usuario_ehfinanceiro;
         obj.gxTpr_Usuario_ehgestor = deserialized.gxTpr_Usuario_ehgestor;
         obj.gxTpr_Usuario_ehpreposto = deserialized.gxTpr_Usuario_ehpreposto;
         obj.gxTpr_Usuario_crtfpath = deserialized.gxTpr_Usuario_crtfpath;
         obj.gxTpr_Usuario_notificar = deserialized.gxTpr_Usuario_notificar;
         obj.gxTpr_Usuario_ativo = deserialized.gxTpr_Usuario_ativo;
         obj.gxTpr_Perfil_ativo = deserialized.gxTpr_Perfil_ativo;
         obj.gxTpr_Usuarioperfil_delete = deserialized.gxTpr_Usuarioperfil_delete;
         obj.gxTpr_Usuarioperfil_update = deserialized.gxTpr_Usuarioperfil_update;
         obj.gxTpr_Usuarioperfil_insert = deserialized.gxTpr_Usuarioperfil_insert;
         obj.gxTpr_Usuarioperfil_display = deserialized.gxTpr_Usuarioperfil_display;
         obj.gxTpr_Perfis = deserialized.gxTpr_Perfis;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_AreaTrabalhoCod") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_AreaTrabalhoDes") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoCod") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoNom") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUOCod") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CargoUONom") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Entidade") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaCod") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaNom") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaTip") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_PessoaDoc") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContador") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhAuditorFM") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratada") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhContratante") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhFinanceiro") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhGestor") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_EhPreposto") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_CrtfPath") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Notificar") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Ativo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Ativo") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Delete") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Update") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Insert") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UsuarioPerfil_Display") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfis") )
               {
                  if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis == null )
                  {
                     gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Usuario.Perfil", "", "SdtSDT_AuntenticacaoOUT_Usuario_Perfil", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis.readxmlcollection(oReader, "Perfis", "Perfil");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_AuntenticacaoOUT.Usuario";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Usuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CargoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CargoNom", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CargoUOCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CargoUONom", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Entidade", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_PessoaNom", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_PessoaTip", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_PessoaDoc", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_UserGamGuid", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhContador", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhAuditorFM", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhContratada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhFinanceiro", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhGestor", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_EhPreposto", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_CrtfPath", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Notificar", StringUtil.RTrim( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Perfil_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UsuarioPerfil_Delete", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UsuarioPerfil_Update", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UsuarioPerfil_Insert", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UsuarioPerfil_Display", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis.writexmlcollection(oWriter, "Perfis", sNameSpace1, "Perfil", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Usuario_Codigo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo, false);
         AddObjectProperty("Usuario_AreaTrabalhoCod", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod, false);
         AddObjectProperty("Usuario_AreaTrabalhoDes", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes, false);
         AddObjectProperty("Usuario_CargoCod", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod, false);
         AddObjectProperty("Usuario_CargoNom", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom, false);
         AddObjectProperty("Usuario_CargoUOCod", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod, false);
         AddObjectProperty("Usuario_CargoUONom", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom, false);
         AddObjectProperty("Usuario_Entidade", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade, false);
         AddObjectProperty("Usuario_PessoaCod", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod, false);
         AddObjectProperty("Usuario_PessoaNom", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom, false);
         AddObjectProperty("Usuario_PessoaTip", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip, false);
         AddObjectProperty("Usuario_PessoaDoc", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc, false);
         AddObjectProperty("Usuario_UserGamGuid", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome, false);
         AddObjectProperty("Usuario_EhContador", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador, false);
         AddObjectProperty("Usuario_EhAuditorFM", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm, false);
         AddObjectProperty("Usuario_EhContratada", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada, false);
         AddObjectProperty("Usuario_EhContratante", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante, false);
         AddObjectProperty("Usuario_EhFinanceiro", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro, false);
         AddObjectProperty("Usuario_EhGestor", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor, false);
         AddObjectProperty("Usuario_EhPreposto", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto, false);
         AddObjectProperty("Usuario_CrtfPath", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath, false);
         AddObjectProperty("Usuario_Notificar", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar, false);
         AddObjectProperty("Usuario_Ativo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo, false);
         AddObjectProperty("Perfil_Ativo", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo, false);
         AddObjectProperty("UsuarioPerfil_Delete", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete, false);
         AddObjectProperty("UsuarioPerfil_Update", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update, false);
         AddObjectProperty("UsuarioPerfil_Insert", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert, false);
         AddObjectProperty("UsuarioPerfil_Display", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display, false);
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis != null )
         {
            AddObjectProperty("Perfis", gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Usuario_Codigo" )]
      [  XmlElement( ElementName = "Usuario_Codigo"   )]
      public int gxTpr_Usuario_codigo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Usuario_AreaTrabalhoCod"   )]
      public int gxTpr_Usuario_areatrabalhocod
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "Usuario_AreaTrabalhoDes"   )]
      public String gxTpr_Usuario_areatrabalhodes
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoCod" )]
      [  XmlElement( ElementName = "Usuario_CargoCod"   )]
      public int gxTpr_Usuario_cargocod
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoNom" )]
      [  XmlElement( ElementName = "Usuario_CargoNom"   )]
      public String gxTpr_Usuario_cargonom
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoUOCod" )]
      [  XmlElement( ElementName = "Usuario_CargoUOCod"   )]
      public int gxTpr_Usuario_cargouocod
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_CargoUONom" )]
      [  XmlElement( ElementName = "Usuario_CargoUONom"   )]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Entidade" )]
      [  XmlElement( ElementName = "Usuario_Entidade"   )]
      public String gxTpr_Usuario_entidade
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaCod" )]
      [  XmlElement( ElementName = "Usuario_PessoaCod"   )]
      public int gxTpr_Usuario_pessoacod
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaNom" )]
      [  XmlElement( ElementName = "Usuario_PessoaNom"   )]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaTip" )]
      [  XmlElement( ElementName = "Usuario_PessoaTip"   )]
      public String gxTpr_Usuario_pessoatip
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_PessoaDoc" )]
      [  XmlElement( ElementName = "Usuario_PessoaDoc"   )]
      public String gxTpr_Usuario_pessoadoc
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid"   )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_EhContador" )]
      [  XmlElement( ElementName = "Usuario_EhContador"   )]
      public bool gxTpr_Usuario_ehcontador
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhAuditorFM" )]
      [  XmlElement( ElementName = "Usuario_EhAuditorFM"   )]
      public bool gxTpr_Usuario_ehauditorfm
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhContratada" )]
      [  XmlElement( ElementName = "Usuario_EhContratada"   )]
      public bool gxTpr_Usuario_ehcontratada
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhContratante" )]
      [  XmlElement( ElementName = "Usuario_EhContratante"   )]
      public bool gxTpr_Usuario_ehcontratante
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhFinanceiro" )]
      [  XmlElement( ElementName = "Usuario_EhFinanceiro"   )]
      public bool gxTpr_Usuario_ehfinanceiro
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhGestor" )]
      [  XmlElement( ElementName = "Usuario_EhGestor"   )]
      public bool gxTpr_Usuario_ehgestor
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_EhPreposto" )]
      [  XmlElement( ElementName = "Usuario_EhPreposto"   )]
      public bool gxTpr_Usuario_ehpreposto
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto = value;
         }

      }

      [  SoapElement( ElementName = "Usuario_CrtfPath" )]
      [  XmlElement( ElementName = "Usuario_CrtfPath"   )]
      public String gxTpr_Usuario_crtfpath
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Notificar" )]
      [  XmlElement( ElementName = "Usuario_Notificar"   )]
      public String gxTpr_Usuario_notificar
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Ativo" )]
      [  XmlElement( ElementName = "Usuario_Ativo"   )]
      public bool gxTpr_Usuario_ativo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Perfil_Ativo" )]
      [  XmlElement( ElementName = "Perfil_Ativo"   )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo = value;
         }

      }

      [  SoapElement( ElementName = "UsuarioPerfil_Delete" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Delete"   )]
      public bool gxTpr_Usuarioperfil_delete
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete = value;
         }

      }

      [  SoapElement( ElementName = "UsuarioPerfil_Update" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Update"   )]
      public bool gxTpr_Usuarioperfil_update
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update = value;
         }

      }

      [  SoapElement( ElementName = "UsuarioPerfil_Insert" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Insert"   )]
      public bool gxTpr_Usuarioperfil_insert
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert = value;
         }

      }

      [  SoapElement( ElementName = "UsuarioPerfil_Display" )]
      [  XmlElement( ElementName = "UsuarioPerfil_Display"   )]
      public bool gxTpr_Usuarioperfil_display
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display = value;
         }

      }

      public class gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_80compatibility:SdtSDT_AuntenticacaoOUT_Usuario_Perfil {}
      [  SoapElement( ElementName = "Perfis" )]
      [  XmlArray( ElementName = "Perfis"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_AuntenticacaoOUT_Usuario_Perfil ), ElementName= "Perfil"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_80compatibility ), ElementName= "SDT_AuntenticacaoOUT.Usuario.Perfil"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Perfis_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Usuario.Perfil", "", "SdtSDT_AuntenticacaoOUT_Usuario_Perfil", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis ;
         }

         set {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Usuario.Perfil", "", "SdtSDT_AuntenticacaoOUT_Usuario_Perfil", "GeneXus.Programs");
            }
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Perfis
      {
         get {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Usuario.Perfil", "", "SdtSDT_AuntenticacaoOUT_Usuario_Perfil", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = value;
         }

      }

      public void gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis_SetNull( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis = null;
         return  ;
      }

      public bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis_IsNull( )
      {
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath = "";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar = "A";
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor = false;
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto = false;
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo = true;
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo = true;
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display = true;
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_codigo ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhocod ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargocod ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouocod ;
      protected int gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoacod ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargouonom ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_entidade ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoanom ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoatip ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_usergamguid ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_nome ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_notificar ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontador ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehauditorfm ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratada ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehcontratante ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehfinanceiro ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehgestor ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ehpreposto ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_ativo ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfil_ativo ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_delete ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_update ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_insert ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuarioperfil_display ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_areatrabalhodes ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_cargonom ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_pessoadoc ;
      protected String gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Usuario_crtfpath ;
      [ObjectCollection(ItemType=typeof( SdtSDT_AuntenticacaoOUT_Usuario_Perfil ))]
      protected IGxCollection gxTv_SdtSDT_AuntenticacaoOUT_Usuario_Perfis=null ;
   }

   [DataContract(Name = @"SDT_AuntenticacaoOUT.Usuario", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_AuntenticacaoOUT_Usuario_RESTInterface : GxGenericCollectionItem<SdtSDT_AuntenticacaoOUT_Usuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_AuntenticacaoOUT_Usuario_RESTInterface( ) : base()
      {
      }

      public SdtSDT_AuntenticacaoOUT_Usuario_RESTInterface( SdtSDT_AuntenticacaoOUT_Usuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Usuario_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Usuario_codigo
      {
         get {
            return sdt.gxTpr_Usuario_codigo ;
         }

         set {
            sdt.gxTpr_Usuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_AreaTrabalhoCod" , Order = 1 )]
      public Nullable<int> gxTpr_Usuario_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Usuario_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Usuario_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_AreaTrabalhoDes" , Order = 2 )]
      public String gxTpr_Usuario_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Usuario_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Usuario_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_CargoCod" , Order = 3 )]
      public Nullable<int> gxTpr_Usuario_cargocod
      {
         get {
            return sdt.gxTpr_Usuario_cargocod ;
         }

         set {
            sdt.gxTpr_Usuario_cargocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_CargoNom" , Order = 4 )]
      public String gxTpr_Usuario_cargonom
      {
         get {
            return sdt.gxTpr_Usuario_cargonom ;
         }

         set {
            sdt.gxTpr_Usuario_cargonom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_CargoUOCod" , Order = 5 )]
      public Nullable<int> gxTpr_Usuario_cargouocod
      {
         get {
            return sdt.gxTpr_Usuario_cargouocod ;
         }

         set {
            sdt.gxTpr_Usuario_cargouocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_CargoUONom" , Order = 6 )]
      public String gxTpr_Usuario_cargouonom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_cargouonom) ;
         }

         set {
            sdt.gxTpr_Usuario_cargouonom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Entidade" , Order = 7 )]
      public String gxTpr_Usuario_entidade
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_entidade) ;
         }

         set {
            sdt.gxTpr_Usuario_entidade = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaCod" , Order = 8 )]
      public Nullable<int> gxTpr_Usuario_pessoacod
      {
         get {
            return sdt.gxTpr_Usuario_pessoacod ;
         }

         set {
            sdt.gxTpr_Usuario_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_PessoaNom" , Order = 9 )]
      public String gxTpr_Usuario_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoanom) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaTip" , Order = 10 )]
      public String gxTpr_Usuario_pessoatip
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_pessoatip) ;
         }

         set {
            sdt.gxTpr_Usuario_pessoatip = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_PessoaDoc" , Order = 11 )]
      public String gxTpr_Usuario_pessoadoc
      {
         get {
            return sdt.gxTpr_Usuario_pessoadoc ;
         }

         set {
            sdt.gxTpr_Usuario_pessoadoc = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_UserGamGuid" , Order = 12 )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_usergamguid) ;
         }

         set {
            sdt.gxTpr_Usuario_usergamguid = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 13 )]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_EhContador" , Order = 14 )]
      public bool gxTpr_Usuario_ehcontador
      {
         get {
            return sdt.gxTpr_Usuario_ehcontador ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontador = value;
         }

      }

      [DataMember( Name = "Usuario_EhAuditorFM" , Order = 15 )]
      public bool gxTpr_Usuario_ehauditorfm
      {
         get {
            return sdt.gxTpr_Usuario_ehauditorfm ;
         }

         set {
            sdt.gxTpr_Usuario_ehauditorfm = value;
         }

      }

      [DataMember( Name = "Usuario_EhContratada" , Order = 16 )]
      public bool gxTpr_Usuario_ehcontratada
      {
         get {
            return sdt.gxTpr_Usuario_ehcontratada ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontratada = value;
         }

      }

      [DataMember( Name = "Usuario_EhContratante" , Order = 17 )]
      public bool gxTpr_Usuario_ehcontratante
      {
         get {
            return sdt.gxTpr_Usuario_ehcontratante ;
         }

         set {
            sdt.gxTpr_Usuario_ehcontratante = value;
         }

      }

      [DataMember( Name = "Usuario_EhFinanceiro" , Order = 18 )]
      public bool gxTpr_Usuario_ehfinanceiro
      {
         get {
            return sdt.gxTpr_Usuario_ehfinanceiro ;
         }

         set {
            sdt.gxTpr_Usuario_ehfinanceiro = value;
         }

      }

      [DataMember( Name = "Usuario_EhGestor" , Order = 19 )]
      public bool gxTpr_Usuario_ehgestor
      {
         get {
            return sdt.gxTpr_Usuario_ehgestor ;
         }

         set {
            sdt.gxTpr_Usuario_ehgestor = value;
         }

      }

      [DataMember( Name = "Usuario_EhPreposto" , Order = 20 )]
      public bool gxTpr_Usuario_ehpreposto
      {
         get {
            return sdt.gxTpr_Usuario_ehpreposto ;
         }

         set {
            sdt.gxTpr_Usuario_ehpreposto = value;
         }

      }

      [DataMember( Name = "Usuario_CrtfPath" , Order = 21 )]
      public String gxTpr_Usuario_crtfpath
      {
         get {
            return sdt.gxTpr_Usuario_crtfpath ;
         }

         set {
            sdt.gxTpr_Usuario_crtfpath = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Notificar" , Order = 22 )]
      public String gxTpr_Usuario_notificar
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_notificar) ;
         }

         set {
            sdt.gxTpr_Usuario_notificar = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Ativo" , Order = 23 )]
      public bool gxTpr_Usuario_ativo
      {
         get {
            return sdt.gxTpr_Usuario_ativo ;
         }

         set {
            sdt.gxTpr_Usuario_ativo = value;
         }

      }

      [DataMember( Name = "Perfil_Ativo" , Order = 24 )]
      public bool gxTpr_Perfil_ativo
      {
         get {
            return sdt.gxTpr_Perfil_ativo ;
         }

         set {
            sdt.gxTpr_Perfil_ativo = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Delete" , Order = 25 )]
      public bool gxTpr_Usuarioperfil_delete
      {
         get {
            return sdt.gxTpr_Usuarioperfil_delete ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_delete = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Update" , Order = 26 )]
      public bool gxTpr_Usuarioperfil_update
      {
         get {
            return sdt.gxTpr_Usuarioperfil_update ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_update = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Insert" , Order = 27 )]
      public bool gxTpr_Usuarioperfil_insert
      {
         get {
            return sdt.gxTpr_Usuarioperfil_insert ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_insert = value;
         }

      }

      [DataMember( Name = "UsuarioPerfil_Display" , Order = 28 )]
      public bool gxTpr_Usuarioperfil_display
      {
         get {
            return sdt.gxTpr_Usuarioperfil_display ;
         }

         set {
            sdt.gxTpr_Usuarioperfil_display = value;
         }

      }

      [DataMember( Name = "Perfis" , Order = 29 )]
      public GxGenericCollection<SdtSDT_AuntenticacaoOUT_Usuario_Perfil_RESTInterface> gxTpr_Perfis
      {
         get {
            return new GxGenericCollection<SdtSDT_AuntenticacaoOUT_Usuario_Perfil_RESTInterface>(sdt.gxTpr_Perfis) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Perfis);
         }

      }

      public SdtSDT_AuntenticacaoOUT_Usuario sdt
      {
         get {
            return (SdtSDT_AuntenticacaoOUT_Usuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_AuntenticacaoOUT_Usuario() ;
         }
      }

   }

}
