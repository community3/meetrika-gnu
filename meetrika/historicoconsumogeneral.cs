/*
               File: HistoricoConsumoGeneral
        Description: Historico Consumo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:10.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class historicoconsumogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public historicoconsumogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public historicoconsumogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_HistoricoConsumo_Codigo )
      {
         this.A1562HistoricoConsumo_Codigo = aP0_HistoricoConsumo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1562HistoricoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1562HistoricoConsumo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAMF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "HistoricoConsumoGeneral";
               context.Gx_err = 0;
               WSMF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Historico Consumo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117291081");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("historicoconsumogeneral.aspx") + "?" + UrlEncode("" +A1562HistoricoConsumo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1562HistoricoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("historicoconsumogeneral.js", "?20203117291083");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "HistoricoConsumoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Historico Consumo General" ;
      }

      protected void WBMF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "historicoconsumogeneral.aspx");
            }
            wb_table1_2_MF2( true) ;
         }
         else
         {
            wb_table1_2_MF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MF2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Historico Consumo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPMF0( ) ;
            }
         }
      }

      protected void WSMF2( )
      {
         STARTMF2( ) ;
         EVTMF2( ) ;
      }

      protected void EVTMF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11MF2 */
                                    E11MF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12MF2 */
                                    E12MF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13MF2 */
                                    E13MF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MF2 */
                                    E14MF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMF2( ) ;
            }
         }
      }

      protected void PAMF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "HistoricoConsumoGeneral";
         context.Gx_err = 0;
      }

      protected void RFMF2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00MF2 */
            pr_default.execute(0, new Object[] {A1562HistoricoConsumo_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1578HistoricoConsumo_Valor = H00MF2_A1578HistoricoConsumo_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1577HistoricoConsumo_Data = H00MF2_A1577HistoricoConsumo_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
               A1563HistoricoConsumo_UsuarioCod = H00MF2_A1563HistoricoConsumo_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
               n1563HistoricoConsumo_UsuarioCod = H00MF2_n1563HistoricoConsumo_UsuarioCod[0];
               A1579HistoricoConsumo_ContratoCod = H00MF2_A1579HistoricoConsumo_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_CONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
               n1579HistoricoConsumo_ContratoCod = H00MF2_n1579HistoricoConsumo_ContratoCod[0];
               A1582HistoricoConsumo_ContagemResultadoCod = H00MF2_A1582HistoricoConsumo_ContagemResultadoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
               n1582HistoricoConsumo_ContagemResultadoCod = H00MF2_n1582HistoricoConsumo_ContagemResultadoCod[0];
               A1581HistoricoConsumo_NotaEmpenhoCod = H00MF2_A1581HistoricoConsumo_NotaEmpenhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
               n1581HistoricoConsumo_NotaEmpenhoCod = H00MF2_n1581HistoricoConsumo_NotaEmpenhoCod[0];
               A1580HistoricoConsumo_SaldoContratoCod = H00MF2_A1580HistoricoConsumo_SaldoContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
               /* Execute user event: E12MF2 */
               E12MF2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBMF0( ) ;
         }
      }

      protected void STRUPMF0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "HistoricoConsumoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MF2 */
         E11MF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1580HistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1580HistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
            A1581HistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", "."));
            n1581HistoricoConsumo_NotaEmpenhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1581HistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
            A1582HistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", "."));
            n1582HistoricoConsumo_ContagemResultadoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1582HistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
            A1579HistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", "."));
            n1579HistoricoConsumo_ContratoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1579HistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_CONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
            A1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", "."));
            n1563HistoricoConsumo_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1563HistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
            A1577HistoricoConsumo_Data = context.localUtil.CToT( cgiGet( edtHistoricoConsumo_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1577HistoricoConsumo_Data", context.localUtil.TToC( A1577HistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
            A1578HistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1578HistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( A1578HistoricoConsumo_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_HISTORICOCONSUMO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            /* Read saved values. */
            wcpOA1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1562HistoricoConsumo_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MF2 */
         E11MF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11MF2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12MF2( )
      {
         /* Load Routine */
      }

      protected void E13MF2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("historicoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1562HistoricoConsumo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14MF2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("historicoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1562HistoricoConsumo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "HistoricoConsumo";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "HistoricoConsumo_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7HistoricoConsumo_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_MF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MF2( true) ;
         }
         else
         {
            wb_table2_8_MF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_MF2( true) ;
         }
         else
         {
            wb_table3_51_MF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_51_MF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MF2e( true) ;
         }
         else
         {
            wb_table1_2_MF2e( false) ;
         }
      }

      protected void wb_table3_51_MF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_MF2e( true) ;
         }
         else
         {
            wb_table3_51_MF2e( false) ;
         }
      }

      protected void wb_table2_8_MF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_codigo_Internalname, "Consumo_Codigo", "", "", lblTextblockhistoricoconsumo_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_saldocontratocod_Internalname, "Contrato Cod", "", "", lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_SaldoContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_SaldoContratoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_notaempenhocod_Internalname, "Empenho Cod", "", "", lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_NotaEmpenhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_contagemresultadocod_Internalname, "Resultado Cod", "", "", lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_ContagemResultadoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_ContagemResultadoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_contratocod_Internalname, "C�digo", "", "", lblTextblockhistoricoconsumo_contratocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_ContratoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_usuariocod_Internalname, "C�digo", "", "", lblTextblockhistoricoconsumo_usuariocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_UsuarioCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_data_Internalname, "Data", "", "", lblTextblockhistoricoconsumo_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtHistoricoConsumo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Data_Internalname, context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtHistoricoConsumo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockhistoricoconsumo_valor_Internalname, "Valor", "", "", lblTextblockhistoricoconsumo_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtHistoricoConsumo_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ",", "")), context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtHistoricoConsumo_Valor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_HistoricoConsumoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MF2e( true) ;
         }
         else
         {
            wb_table2_8_MF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1562HistoricoConsumo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMF2( ) ;
         WSMF2( ) ;
         WEMF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1562HistoricoConsumo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAMF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "historicoconsumogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAMF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1562HistoricoConsumo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         }
         wcpOA1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1562HistoricoConsumo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1562HistoricoConsumo_Codigo != wcpOA1562HistoricoConsumo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1562HistoricoConsumo_Codigo = A1562HistoricoConsumo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1562HistoricoConsumo_Codigo = cgiGet( sPrefix+"A1562HistoricoConsumo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1562HistoricoConsumo_Codigo) > 0 )
         {
            A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1562HistoricoConsumo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1562HistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0)));
         }
         else
         {
            A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1562HistoricoConsumo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAMF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSMF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSMF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1562HistoricoConsumo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1562HistoricoConsumo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1562HistoricoConsumo_Codigo_CTRL", StringUtil.RTrim( sCtrlA1562HistoricoConsumo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEMF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311729118");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("historicoconsumogeneral.js", "?2020311729118");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockhistoricoconsumo_codigo_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_CODIGO";
         edtHistoricoConsumo_Codigo_Internalname = sPrefix+"HISTORICOCONSUMO_CODIGO";
         lblTextblockhistoricoconsumo_saldocontratocod_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtHistoricoConsumo_SaldoContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATOCOD";
         lblTextblockhistoricoconsumo_notaempenhocod_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = sPrefix+"HISTORICOCONSUMO_NOTAEMPENHOCOD";
         lblTextblockhistoricoconsumo_contagemresultadocod_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         lblTextblockhistoricoconsumo_contratocod_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_CONTRATOCOD";
         edtHistoricoConsumo_ContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTRATOCOD";
         lblTextblockhistoricoconsumo_usuariocod_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_USUARIOCOD";
         edtHistoricoConsumo_UsuarioCod_Internalname = sPrefix+"HISTORICOCONSUMO_USUARIOCOD";
         lblTextblockhistoricoconsumo_data_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_DATA";
         edtHistoricoConsumo_Data_Internalname = sPrefix+"HISTORICOCONSUMO_DATA";
         lblTextblockhistoricoconsumo_valor_Internalname = sPrefix+"TEXTBLOCKHISTORICOCONSUMO_VALOR";
         edtHistoricoConsumo_Valor_Internalname = sPrefix+"HISTORICOCONSUMO_VALOR";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtHistoricoConsumo_Valor_Jsonclick = "";
         edtHistoricoConsumo_Data_Jsonclick = "";
         edtHistoricoConsumo_UsuarioCod_Jsonclick = "";
         edtHistoricoConsumo_ContratoCod_Jsonclick = "";
         edtHistoricoConsumo_ContagemResultadoCod_Jsonclick = "";
         edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoCod_Jsonclick = "";
         edtHistoricoConsumo_Codigo_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13MF2',iparms:[{av:'A1562HistoricoConsumo_Codigo',fld:'HISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14MF2',iparms:[{av:'A1562HistoricoConsumo_Codigo',fld:'HISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00MF2_A1562HistoricoConsumo_Codigo = new int[1] ;
         H00MF2_A1578HistoricoConsumo_Valor = new decimal[1] ;
         H00MF2_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         H00MF2_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         H00MF2_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         H00MF2_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         H00MF2_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         H00MF2_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         H00MF2_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         H00MF2_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         H00MF2_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         H00MF2_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockhistoricoconsumo_codigo_Jsonclick = "";
         lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_contratocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_usuariocod_Jsonclick = "";
         lblTextblockhistoricoconsumo_data_Jsonclick = "";
         lblTextblockhistoricoconsumo_valor_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1562HistoricoConsumo_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.historicoconsumogeneral__default(),
            new Object[][] {
                new Object[] {
               H00MF2_A1562HistoricoConsumo_Codigo, H00MF2_A1578HistoricoConsumo_Valor, H00MF2_A1577HistoricoConsumo_Data, H00MF2_A1563HistoricoConsumo_UsuarioCod, H00MF2_n1563HistoricoConsumo_UsuarioCod, H00MF2_A1579HistoricoConsumo_ContratoCod, H00MF2_n1579HistoricoConsumo_ContratoCod, H00MF2_A1582HistoricoConsumo_ContagemResultadoCod, H00MF2_n1582HistoricoConsumo_ContagemResultadoCod, H00MF2_A1581HistoricoConsumo_NotaEmpenhoCod,
               H00MF2_n1581HistoricoConsumo_NotaEmpenhoCod, H00MF2_A1580HistoricoConsumo_SaldoContratoCod
               }
            }
         );
         AV14Pgmname = "HistoricoConsumoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "HistoricoConsumoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1562HistoricoConsumo_Codigo ;
      private int wcpOA1562HistoricoConsumo_Codigo ;
      private int A1580HistoricoConsumo_SaldoContratoCod ;
      private int A1581HistoricoConsumo_NotaEmpenhoCod ;
      private int A1582HistoricoConsumo_ContagemResultadoCod ;
      private int A1579HistoricoConsumo_ContratoCod ;
      private int A1563HistoricoConsumo_UsuarioCod ;
      private int AV7HistoricoConsumo_Codigo ;
      private int idxLst ;
      private decimal A1578HistoricoConsumo_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtHistoricoConsumo_SaldoContratoCod_Internalname ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Internalname ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Internalname ;
      private String edtHistoricoConsumo_ContratoCod_Internalname ;
      private String edtHistoricoConsumo_UsuarioCod_Internalname ;
      private String edtHistoricoConsumo_Data_Internalname ;
      private String edtHistoricoConsumo_Valor_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockhistoricoconsumo_codigo_Internalname ;
      private String lblTextblockhistoricoconsumo_codigo_Jsonclick ;
      private String edtHistoricoConsumo_Codigo_Internalname ;
      private String edtHistoricoConsumo_Codigo_Jsonclick ;
      private String lblTextblockhistoricoconsumo_saldocontratocod_Internalname ;
      private String lblTextblockhistoricoconsumo_saldocontratocod_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_notaempenhocod_Internalname ;
      private String lblTextblockhistoricoconsumo_notaempenhocod_Jsonclick ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_contagemresultadocod_Internalname ;
      private String lblTextblockhistoricoconsumo_contagemresultadocod_Jsonclick ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_contratocod_Internalname ;
      private String lblTextblockhistoricoconsumo_contratocod_Jsonclick ;
      private String edtHistoricoConsumo_ContratoCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_usuariocod_Internalname ;
      private String lblTextblockhistoricoconsumo_usuariocod_Jsonclick ;
      private String edtHistoricoConsumo_UsuarioCod_Jsonclick ;
      private String lblTextblockhistoricoconsumo_data_Internalname ;
      private String lblTextblockhistoricoconsumo_data_Jsonclick ;
      private String edtHistoricoConsumo_Data_Jsonclick ;
      private String lblTextblockhistoricoconsumo_valor_Internalname ;
      private String lblTextblockhistoricoconsumo_valor_Jsonclick ;
      private String edtHistoricoConsumo_Valor_Jsonclick ;
      private String sCtrlA1562HistoricoConsumo_Codigo ;
      private DateTime A1577HistoricoConsumo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1563HistoricoConsumo_UsuarioCod ;
      private bool n1579HistoricoConsumo_ContratoCod ;
      private bool n1582HistoricoConsumo_ContagemResultadoCod ;
      private bool n1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00MF2_A1562HistoricoConsumo_Codigo ;
      private decimal[] H00MF2_A1578HistoricoConsumo_Valor ;
      private DateTime[] H00MF2_A1577HistoricoConsumo_Data ;
      private int[] H00MF2_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] H00MF2_n1563HistoricoConsumo_UsuarioCod ;
      private int[] H00MF2_A1579HistoricoConsumo_ContratoCod ;
      private bool[] H00MF2_n1579HistoricoConsumo_ContratoCod ;
      private int[] H00MF2_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] H00MF2_n1582HistoricoConsumo_ContagemResultadoCod ;
      private int[] H00MF2_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] H00MF2_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] H00MF2_A1580HistoricoConsumo_SaldoContratoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class historicoconsumogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MF2 ;
          prmH00MF2 = new Object[] {
          new Object[] {"@HistoricoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MF2", "SELECT [HistoricoConsumo_Codigo], [HistoricoConsumo_Valor], [HistoricoConsumo_Data], [HistoricoConsumo_UsuarioCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_SaldoContratoCod] FROM [HistoricoConsumo] WITH (NOLOCK) WHERE [HistoricoConsumo_Codigo] = @HistoricoConsumo_Codigo ORDER BY [HistoricoConsumo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MF2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
