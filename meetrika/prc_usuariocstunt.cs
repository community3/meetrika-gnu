/*
               File: PRC_UsuarioCstUnt
        Description: Custo unitario de produ��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:59.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuariocstunt : GXProcedure
   {
      public prc_usuariocstunt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuariocstunt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           ref decimal aP1_ContratadaUsuario_CstUntPrdNrm ,
                           ref decimal aP2_ContratadaUsuario_CstUntPrdExt ,
                           String aP3_Acao )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV9ContratadaUsuario_CstUntPrdNrm = aP1_ContratadaUsuario_CstUntPrdNrm;
         this.AV10ContratadaUsuario_CstUntPrdExt = aP2_ContratadaUsuario_CstUntPrdExt;
         this.AV11Acao = aP3_Acao;
         initialize();
         executePrivate();
         aP1_ContratadaUsuario_CstUntPrdNrm=this.AV9ContratadaUsuario_CstUntPrdNrm;
         aP2_ContratadaUsuario_CstUntPrdExt=this.AV10ContratadaUsuario_CstUntPrdExt;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 ref decimal aP1_ContratadaUsuario_CstUntPrdNrm ,
                                 ref decimal aP2_ContratadaUsuario_CstUntPrdExt ,
                                 String aP3_Acao )
      {
         prc_usuariocstunt objprc_usuariocstunt;
         objprc_usuariocstunt = new prc_usuariocstunt();
         objprc_usuariocstunt.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_usuariocstunt.AV9ContratadaUsuario_CstUntPrdNrm = aP1_ContratadaUsuario_CstUntPrdNrm;
         objprc_usuariocstunt.AV10ContratadaUsuario_CstUntPrdExt = aP2_ContratadaUsuario_CstUntPrdExt;
         objprc_usuariocstunt.AV11Acao = aP3_Acao;
         objprc_usuariocstunt.context.SetSubmitInitialConfig(context);
         objprc_usuariocstunt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuariocstunt);
         aP1_ContratadaUsuario_CstUntPrdNrm=this.AV9ContratadaUsuario_CstUntPrdNrm;
         aP2_ContratadaUsuario_CstUntPrdExt=this.AV10ContratadaUsuario_CstUntPrdExt;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuariocstunt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV11Acao, "R") == 0 )
         {
            /* Using cursor P00452 */
            pr_default.execute(0, new Object[] {AV8Usuario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = P00452_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = P00452_A66ContratadaUsuario_ContratadaCod[0];
               A576ContratadaUsuario_CstUntPrdNrm = P00452_A576ContratadaUsuario_CstUntPrdNrm[0];
               n576ContratadaUsuario_CstUntPrdNrm = P00452_n576ContratadaUsuario_CstUntPrdNrm[0];
               A577ContratadaUsuario_CstUntPrdExt = P00452_A577ContratadaUsuario_CstUntPrdExt[0];
               n577ContratadaUsuario_CstUntPrdExt = P00452_n577ContratadaUsuario_CstUntPrdExt[0];
               if ( (Convert.ToDecimal( A66ContratadaUsuario_ContratadaCod ) == NumberUtil.Val( AV12Websession.Get("Contratada_Codigo"), ".") ) )
               {
                  AV9ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
                  AV10ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else
         {
            /* Using cursor P00453 */
            pr_default.execute(1, new Object[] {AV8Usuario_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = P00453_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = P00453_A66ContratadaUsuario_ContratadaCod[0];
               A576ContratadaUsuario_CstUntPrdNrm = P00453_A576ContratadaUsuario_CstUntPrdNrm[0];
               n576ContratadaUsuario_CstUntPrdNrm = P00453_n576ContratadaUsuario_CstUntPrdNrm[0];
               A577ContratadaUsuario_CstUntPrdExt = P00453_A577ContratadaUsuario_CstUntPrdExt[0];
               n577ContratadaUsuario_CstUntPrdExt = P00453_n577ContratadaUsuario_CstUntPrdExt[0];
               if ( (Convert.ToDecimal( A66ContratadaUsuario_ContratadaCod ) == NumberUtil.Val( AV12Websession.Get("Contratada_Codigo"), ".") ) )
               {
                  A576ContratadaUsuario_CstUntPrdNrm = AV9ContratadaUsuario_CstUntPrdNrm;
                  n576ContratadaUsuario_CstUntPrdNrm = false;
                  A577ContratadaUsuario_CstUntPrdExt = AV10ContratadaUsuario_CstUntPrdExt;
                  n577ContratadaUsuario_CstUntPrdExt = false;
                  /* Using cursor P00454 */
                  pr_default.execute(2, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                  pr_default.close(2);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UsuarioCstUnt");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00452_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00452_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00452_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00452_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P00452_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P00452_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         AV12Websession = context.GetSession();
         P00453_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00453_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00453_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P00453_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P00453_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P00453_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuariocstunt__default(),
            new Object[][] {
                new Object[] {
               P00452_A69ContratadaUsuario_UsuarioCod, P00452_A66ContratadaUsuario_ContratadaCod, P00452_A576ContratadaUsuario_CstUntPrdNrm, P00452_n576ContratadaUsuario_CstUntPrdNrm, P00452_A577ContratadaUsuario_CstUntPrdExt, P00452_n577ContratadaUsuario_CstUntPrdExt
               }
               , new Object[] {
               P00453_A69ContratadaUsuario_UsuarioCod, P00453_A66ContratadaUsuario_ContratadaCod, P00453_A576ContratadaUsuario_CstUntPrdNrm, P00453_n576ContratadaUsuario_CstUntPrdNrm, P00453_A577ContratadaUsuario_CstUntPrdExt, P00453_n577ContratadaUsuario_CstUntPrdExt
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Usuario_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private decimal AV9ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV10ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private String AV11Acao ;
      private String scmdbuf ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private IGxSession AV12Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private decimal aP1_ContratadaUsuario_CstUntPrdNrm ;
      private decimal aP2_ContratadaUsuario_CstUntPrdExt ;
      private IDataStoreProvider pr_default ;
      private int[] P00452_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00452_A66ContratadaUsuario_ContratadaCod ;
      private decimal[] P00452_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00452_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] P00452_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P00452_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] P00453_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00453_A66ContratadaUsuario_ContratadaCod ;
      private decimal[] P00453_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P00453_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] P00453_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P00453_n577ContratadaUsuario_CstUntPrdExt ;
   }

   public class prc_usuariocstunt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00452 ;
          prmP00452 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00453 ;
          prmP00453 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00454 ;
          prmP00454 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00452", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV8Usuario_Codigo ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00452,100,0,false,false )
             ,new CursorDef("P00453", "SELECT [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt] FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV8Usuario_Codigo ORDER BY [ContratadaUsuario_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00453,1,0,true,false )
             ,new CursorDef("P00454", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00454)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
       }
    }

 }

}
