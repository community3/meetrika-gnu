/*
               File: PRC_UsuarioEmailUPD
        Description: Usuario Email UPD
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:49.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioemailupd : GXProcedure
   {
      public prc_usuarioemailupd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioemailupd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Usuario_UserGamGuid ,
                           ref String aP1_Email )
      {
         this.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV9Email = aP1_Email;
         initialize();
         executePrivate();
         aP0_Usuario_UserGamGuid=this.AV10Usuario_UserGamGuid;
         aP1_Email=this.AV9Email;
      }

      public String executeUdp( ref String aP0_Usuario_UserGamGuid )
      {
         this.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV9Email = aP1_Email;
         initialize();
         executePrivate();
         aP0_Usuario_UserGamGuid=this.AV10Usuario_UserGamGuid;
         aP1_Email=this.AV9Email;
         return AV9Email ;
      }

      public void executeSubmit( ref String aP0_Usuario_UserGamGuid ,
                                 ref String aP1_Email )
      {
         prc_usuarioemailupd objprc_usuarioemailupd;
         objprc_usuarioemailupd = new prc_usuarioemailupd();
         objprc_usuarioemailupd.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         objprc_usuarioemailupd.AV9Email = aP1_Email;
         objprc_usuarioemailupd.context.SetSubmitInitialConfig(context);
         objprc_usuarioemailupd.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioemailupd);
         aP0_Usuario_UserGamGuid=this.AV10Usuario_UserGamGuid;
         aP1_Email=this.AV9Email;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioemailupd)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8GamUser.load( AV10Usuario_UserGamGuid);
         AV8GamUser.gxTpr_Email = AV9Email;
         AV8GamUser.save();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8GamUser = new SdtGAMUser(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV10Usuario_UserGamGuid ;
      private String AV9Email ;
      private String aP0_Usuario_UserGamGuid ;
      private String aP1_Email ;
      private SdtGAMUser AV8GamUser ;
   }

}
