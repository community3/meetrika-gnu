/*
               File: REL_ContagemCapa
        Description: Stub for REL_ContagemCapa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:30.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemcapa : GXProcedure
   {
      public rel_contagemcapa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_contagemcapa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_LoteAceite ,
                           int aP1_ContagemResultado_SS )
      {
         this.AV2ContagemResultado_LoteAceite = aP0_ContagemResultado_LoteAceite;
         this.AV3ContagemResultado_SS = aP1_ContagemResultado_SS;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_LoteAceite ,
                                 int aP1_ContagemResultado_SS )
      {
         rel_contagemcapa objrel_contagemcapa;
         objrel_contagemcapa = new rel_contagemcapa();
         objrel_contagemcapa.AV2ContagemResultado_LoteAceite = aP0_ContagemResultado_LoteAceite;
         objrel_contagemcapa.AV3ContagemResultado_SS = aP1_ContagemResultado_SS;
         objrel_contagemcapa.context.SetSubmitInitialConfig(context);
         objrel_contagemcapa.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemcapa);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemcapa)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2ContagemResultado_LoteAceite,(int)AV3ContagemResultado_SS} ;
         ClassLoader.Execute("arel_contagemcapa","GeneXus.Programs.arel_contagemcapa", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 2 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2ContagemResultado_LoteAceite ;
      private int AV3ContagemResultado_SS ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
