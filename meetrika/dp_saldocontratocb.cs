/*
               File: DP_SaldoContratoCB
        Description: DP_Saldo Contrato CB
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:25:17.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_saldocontratocb : GXProcedure
   {
      public dp_saldocontratocb( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_saldocontratocb( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_SaldoContratoCB", "GxEv3Up14_Meetrika", "SdtSDT_SaldoContratoCB", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Contrato_Codigo )
      {
         this.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_SaldoContratoCB", "GxEv3Up14_Meetrika", "SdtSDT_SaldoContratoCB", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_saldocontratocb objdp_saldocontratocb;
         objdp_saldocontratocb = new dp_saldocontratocb();
         objdp_saldocontratocb.AV5Contrato_Codigo = aP0_Contrato_Codigo;
         objdp_saldocontratocb.Gxm2rootcol = new GxObjectCollection( context, "SDT_SaldoContratoCB", "GxEv3Up14_Meetrika", "SdtSDT_SaldoContratoCB", "GeneXus.Programs") ;
         objdp_saldocontratocb.context.SetSubmitInitialConfig(context);
         objdp_saldocontratocb.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_saldocontratocb);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_saldocontratocb)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000C2 */
         pr_default.execute(0, new Object[] {AV5Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1783SaldoContrato_UnidadeMedicao_Codigo = P000C2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P000C2_A74Contrato_Codigo[0];
            A1561SaldoContrato_Codigo = P000C2_A1561SaldoContrato_Codigo[0];
            A1784SaldoContrato_UnidadeMedicao_Nome = P000C2_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = P000C2_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            A1572SaldoContrato_VigenciaFim = P000C2_A1572SaldoContrato_VigenciaFim[0];
            A1571SaldoContrato_VigenciaInicio = P000C2_A1571SaldoContrato_VigenciaInicio[0];
            A1784SaldoContrato_UnidadeMedicao_Nome = P000C2_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = P000C2_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            Gxm1sdt_saldocontratocb = new SdtSDT_SaldoContratoCB(context);
            Gxm2rootcol.Add(Gxm1sdt_saldocontratocb, 0);
            Gxm1sdt_saldocontratocb.gxTpr_Saldocontrato_codigo = A1561SaldoContrato_Codigo;
            Gxm1sdt_saldocontratocb.gxTpr_Saldocontrato_descricao = context.localUtil.DToC( A1571SaldoContrato_VigenciaInicio, 2, "/")+" - "+context.localUtil.DToC( A1572SaldoContrato_VigenciaFim, 2, "/")+" - "+A1784SaldoContrato_UnidadeMedicao_Nome;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000C2_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P000C2_A74Contrato_Codigo = new int[1] ;
         P000C2_A1561SaldoContrato_Codigo = new int[1] ;
         P000C2_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         P000C2_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         P000C2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P000C2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         A1784SaldoContrato_UnidadeMedicao_Nome = "";
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         Gxm1sdt_saldocontratocb = new SdtSDT_SaldoContratoCB(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_saldocontratocb__default(),
            new Object[][] {
                new Object[] {
               P000C2_A1783SaldoContrato_UnidadeMedicao_Codigo, P000C2_A74Contrato_Codigo, P000C2_A1561SaldoContrato_Codigo, P000C2_A1784SaldoContrato_UnidadeMedicao_Nome, P000C2_n1784SaldoContrato_UnidadeMedicao_Nome, P000C2_A1572SaldoContrato_VigenciaFim, P000C2_A1571SaldoContrato_VigenciaInicio
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private String scmdbuf ;
      private String A1784SaldoContrato_UnidadeMedicao_Nome ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private bool n1784SaldoContrato_UnidadeMedicao_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000C2_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P000C2_A74Contrato_Codigo ;
      private int[] P000C2_A1561SaldoContrato_Codigo ;
      private String[] P000C2_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] P000C2_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private DateTime[] P000C2_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] P000C2_A1571SaldoContrato_VigenciaInicio ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_SaldoContratoCB ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_SaldoContratoCB Gxm1sdt_saldocontratocb ;
   }

   public class dp_saldocontratocb__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000C2 ;
          prmP000C2 = new Object[] {
          new Object[] {"@AV5Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000C2", "SELECT T1.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T1.[Contrato_Codigo], T1.[SaldoContrato_Codigo], T2.[UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, T1.[SaldoContrato_VigenciaFim], T1.[SaldoContrato_VigenciaInicio] FROM ([SaldoContrato] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[SaldoContrato_UnidadeMedicao_Codigo]) WHERE T1.[Contrato_Codigo] = @AV5Contrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000C2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
