/*
               File: PromptSolicitacaoMudanca
        Description: Selecione Solicita��o de Mudan�a
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:55:0.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptsolicitacaomudanca : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptsolicitacaomudanca( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptsolicitacaomudanca( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutSolicitacaoMudanca_Codigo ,
                           ref DateTime aP1_InOutSolicitacaoMudanca_Data )
      {
         this.AV7InOutSolicitacaoMudanca_Codigo = aP0_InOutSolicitacaoMudanca_Codigo;
         this.AV8InOutSolicitacaoMudanca_Data = aP1_InOutSolicitacaoMudanca_Data;
         executePrivate();
         aP0_InOutSolicitacaoMudanca_Codigo=this.AV7InOutSolicitacaoMudanca_Codigo;
         aP1_InOutSolicitacaoMudanca_Data=this.AV8InOutSolicitacaoMudanca_Data;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16SolicitacaoMudanca_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SolicitacaoMudanca_Data1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
               AV17SolicitacaoMudanca_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SolicitacaoMudanca_Data_To1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20SolicitacaoMudanca_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SolicitacaoMudanca_Data2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
               AV21SolicitacaoMudanca_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SolicitacaoMudanca_Data_To2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24SolicitacaoMudanca_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SolicitacaoMudanca_Data3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
               AV25SolicitacaoMudanca_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SolicitacaoMudanca_Data_To3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0)));
               AV32TFSolicitacaoMudanca_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0)));
               AV35TFSolicitacaoMudanca_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSolicitacaoMudanca_Data", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
               AV36TFSolicitacaoMudanca_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSolicitacaoMudanca_Data_To", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
               AV41TFSolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0)));
               AV42TFSolicitacaoMudanca_SistemaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
               AV45TFSolicitacaoMudanca_Solicitante = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFSolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0)));
               AV46TFSolicitacaoMudanca_Solicitante_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSolicitacaoMudanca_Solicitante_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0)));
               AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
               AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace", AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace);
               AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
               AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace", AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutSolicitacaoMudanca_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudanca_Data", context.localUtil.Format(AV8InOutSolicitacaoMudanca_Data, "99/99/99"));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAGZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSGZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEGZ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311855042");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptsolicitacaomudanca.aspx") + "?" + UrlEncode("" +AV7InOutSolicitacaoMudanca_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutSolicitacaoMudanca_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA_TO1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA_TO2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACAOMUDANCA_DATA_TO3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_DATA", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_DATA_TO", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA", AV30SolicitacaoMudanca_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA", AV30SolicitacaoMudanca_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_DATATITLEFILTERDATA", AV34SolicitacaoMudanca_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_DATATITLEFILTERDATA", AV34SolicitacaoMudanca_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA", AV40SolicitacaoMudanca_SistemaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA", AV40SolicitacaoMudanca_SistemaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_SOLICITANTETITLEFILTERDATA", AV44SolicitacaoMudanca_SolicitanteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_SOLICITANTETITLEFILTERDATA", AV44SolicitacaoMudanca_SolicitanteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACAOMUDANCA_DATA", context.localUtil.DToC( AV8InOutSolicitacaoMudanca_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_solicitante_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_solicitante_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_solicitante_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_solicitante_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_solicitante_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_solicitante_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGZ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptSolicitacaoMudanca" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Solicita��o de Mudan�a" ;
      }

      protected void WBGZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GZ2( true) ;
         }
         else
         {
            wb_table1_2_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFSolicitacaoMudanca_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacaomudanca_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_data_Internalname, context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"), context.localUtil.Format( AV35TFSolicitacaoMudanca_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacaomudanca_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacaomudanca_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacaomudanca_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_data_to_Internalname, context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"), context.localUtil.Format( AV36TFSolicitacaoMudanca_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacaomudanca_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacaomudanca_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_solicitacaomudanca_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacaomudanca_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacaomudanca_dataauxdate_Internalname, context.localUtil.Format(AV37DDO_SolicitacaoMudanca_DataAuxDate, "99/99/99"), context.localUtil.Format( AV37DDO_SolicitacaoMudanca_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacaomudanca_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacaomudanca_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacaomudanca_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacaomudanca_dataauxdateto_Internalname, context.localUtil.Format(AV38DDO_SolicitacaoMudanca_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV38DDO_SolicitacaoMudanca_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacaomudanca_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacaomudanca_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_sistemacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_sistemacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_sistemacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_sistemacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_sistemacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_solicitante_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_solicitante_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_solicitante_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_solicitante_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_solicitante_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_solicitante_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudanca.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Internalname, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudanca.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_SISTEMACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudanca.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_SOLICITANTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Internalname, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudanca.htm");
         }
         wbLoad = true;
      }

      protected void STARTGZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Solicita��o de Mudan�a", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGZ0( ) ;
      }

      protected void WSGZ2( )
      {
         STARTGZ2( ) ;
         EVTGZ2( ) ;
      }

      protected void EVTGZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11GZ2 */
                           E11GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12GZ2 */
                           E12GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13GZ2 */
                           E13GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_SISTEMACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14GZ2 */
                           E14GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_SOLICITANTE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15GZ2 */
                           E15GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16GZ2 */
                           E16GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17GZ2 */
                           E17GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18GZ2 */
                           E18GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19GZ2 */
                           E19GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20GZ2 */
                           E20GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21GZ2 */
                           E21GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22GZ2 */
                           E22GZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV54Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", "."));
                           A997SolicitacaoMudanca_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSolicitacaoMudanca_Data_Internalname), 0));
                           A993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", "."));
                           n993SolicitacaoMudanca_SistemaCod = false;
                           A994SolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Solicitante_Internalname), ",", "."));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23GZ2 */
                                 E23GZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24GZ2 */
                                 E24GZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25GZ2 */
                                 E25GZ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA1"), 0) != AV16SolicitacaoMudanca_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO1"), 0) != AV17SolicitacaoMudanca_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA2"), 0) != AV20SolicitacaoMudanca_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO2"), 0) != AV21SolicitacaoMudanca_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA3"), 0) != AV24SolicitacaoMudanca_Data3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacaomudanca_data_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO3"), 0) != AV25SolicitacaoMudanca_Data_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFSolicitacaoMudanca_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFSolicitacaoMudanca_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_DATA"), 0) != AV35TFSolicitacaoMudanca_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_DATA_TO"), 0) != AV36TFSolicitacaoMudanca_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_sistemacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV41TFSolicitacaoMudanca_SistemaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_sistemacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV42TFSolicitacaoMudanca_SistemaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_solicitante Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE"), ",", ".") != Convert.ToDecimal( AV45TFSolicitacaoMudanca_Solicitante )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_solicitante_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE_TO"), ",", ".") != Convert.ToDecimal( AV46TFSolicitacaoMudanca_Solicitante_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E26GZ2 */
                                       E26GZ2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEGZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGZ2( ) ;
            }
         }
      }

      protected void PAGZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SOLICITACAOMUDANCA_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SOLICITACAOMUDANCA_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SOLICITACAOMUDANCA_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16SolicitacaoMudanca_Data1 ,
                                       DateTime AV17SolicitacaoMudanca_Data_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20SolicitacaoMudanca_Data2 ,
                                       DateTime AV21SolicitacaoMudanca_Data_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24SolicitacaoMudanca_Data3 ,
                                       DateTime AV25SolicitacaoMudanca_Data_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFSolicitacaoMudanca_Codigo ,
                                       int AV32TFSolicitacaoMudanca_Codigo_To ,
                                       DateTime AV35TFSolicitacaoMudanca_Data ,
                                       DateTime AV36TFSolicitacaoMudanca_Data_To ,
                                       int AV41TFSolicitacaoMudanca_SistemaCod ,
                                       int AV42TFSolicitacaoMudanca_SistemaCod_To ,
                                       int AV45TFSolicitacaoMudanca_Solicitante ,
                                       int AV46TFSolicitacaoMudanca_Solicitante_To ,
                                       String AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace ,
                                       String AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace ,
                                       String AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace ,
                                       String AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGZ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_DATA", GetSecureSignedToken( "", A997SolicitacaoMudanca_Data));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_DATA", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_SOLICITANTE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_SOLICITANTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFGZ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E24GZ2 */
         E24GZ2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16SolicitacaoMudanca_Data1 ,
                                                 AV17SolicitacaoMudanca_Data_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20SolicitacaoMudanca_Data2 ,
                                                 AV21SolicitacaoMudanca_Data_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24SolicitacaoMudanca_Data3 ,
                                                 AV25SolicitacaoMudanca_Data_To3 ,
                                                 AV31TFSolicitacaoMudanca_Codigo ,
                                                 AV32TFSolicitacaoMudanca_Codigo_To ,
                                                 AV35TFSolicitacaoMudanca_Data ,
                                                 AV36TFSolicitacaoMudanca_Data_To ,
                                                 AV41TFSolicitacaoMudanca_SistemaCod ,
                                                 AV42TFSolicitacaoMudanca_SistemaCod_To ,
                                                 AV45TFSolicitacaoMudanca_Solicitante ,
                                                 AV46TFSolicitacaoMudanca_Solicitante_To ,
                                                 A997SolicitacaoMudanca_Data ,
                                                 A996SolicitacaoMudanca_Codigo ,
                                                 A993SolicitacaoMudanca_SistemaCod ,
                                                 A994SolicitacaoMudanca_Solicitante ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00GZ2 */
            pr_default.execute(0, new Object[] {AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A994SolicitacaoMudanca_Solicitante = H00GZ2_A994SolicitacaoMudanca_Solicitante[0];
               A993SolicitacaoMudanca_SistemaCod = H00GZ2_A993SolicitacaoMudanca_SistemaCod[0];
               n993SolicitacaoMudanca_SistemaCod = H00GZ2_n993SolicitacaoMudanca_SistemaCod[0];
               A997SolicitacaoMudanca_Data = H00GZ2_A997SolicitacaoMudanca_Data[0];
               A996SolicitacaoMudanca_Codigo = H00GZ2_A996SolicitacaoMudanca_Codigo[0];
               /* Execute user event: E25GZ2 */
               E25GZ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBGZ0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16SolicitacaoMudanca_Data1 ,
                                              AV17SolicitacaoMudanca_Data_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20SolicitacaoMudanca_Data2 ,
                                              AV21SolicitacaoMudanca_Data_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24SolicitacaoMudanca_Data3 ,
                                              AV25SolicitacaoMudanca_Data_To3 ,
                                              AV31TFSolicitacaoMudanca_Codigo ,
                                              AV32TFSolicitacaoMudanca_Codigo_To ,
                                              AV35TFSolicitacaoMudanca_Data ,
                                              AV36TFSolicitacaoMudanca_Data_To ,
                                              AV41TFSolicitacaoMudanca_SistemaCod ,
                                              AV42TFSolicitacaoMudanca_SistemaCod_To ,
                                              AV45TFSolicitacaoMudanca_Solicitante ,
                                              AV46TFSolicitacaoMudanca_Solicitante_To ,
                                              A997SolicitacaoMudanca_Data ,
                                              A996SolicitacaoMudanca_Codigo ,
                                              A993SolicitacaoMudanca_SistemaCod ,
                                              A994SolicitacaoMudanca_Solicitante ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00GZ3 */
         pr_default.execute(1, new Object[] {AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To});
         GRID_nRecordCount = H00GZ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGZ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23GZ2 */
         E23GZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV48DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA"), AV30SolicitacaoMudanca_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_DATATITLEFILTERDATA"), AV34SolicitacaoMudanca_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA"), AV40SolicitacaoMudanca_SistemaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_SOLICITANTETITLEFILTERDATA"), AV44SolicitacaoMudanca_SolicitanteTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data1"}), 1, "vSOLICITACAOMUDANCA_DATA1");
               GX_FocusControl = edtavSolicitacaomudanca_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16SolicitacaoMudanca_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SolicitacaoMudanca_Data1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
            }
            else
            {
               AV16SolicitacaoMudanca_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SolicitacaoMudanca_Data1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data_To1"}), 1, "vSOLICITACAOMUDANCA_DATA_TO1");
               GX_FocusControl = edtavSolicitacaomudanca_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17SolicitacaoMudanca_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SolicitacaoMudanca_Data_To1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
            }
            else
            {
               AV17SolicitacaoMudanca_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SolicitacaoMudanca_Data_To1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data2"}), 1, "vSOLICITACAOMUDANCA_DATA2");
               GX_FocusControl = edtavSolicitacaomudanca_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20SolicitacaoMudanca_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SolicitacaoMudanca_Data2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
            }
            else
            {
               AV20SolicitacaoMudanca_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SolicitacaoMudanca_Data2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data_To2"}), 1, "vSOLICITACAOMUDANCA_DATA_TO2");
               GX_FocusControl = edtavSolicitacaomudanca_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21SolicitacaoMudanca_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SolicitacaoMudanca_Data_To2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
            }
            else
            {
               AV21SolicitacaoMudanca_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SolicitacaoMudanca_Data_To2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data3"}), 1, "vSOLICITACAOMUDANCA_DATA3");
               GX_FocusControl = edtavSolicitacaomudanca_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24SolicitacaoMudanca_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SolicitacaoMudanca_Data3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
            }
            else
            {
               AV24SolicitacaoMudanca_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SolicitacaoMudanca_Data3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSolicitacaomudanca_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Solicitacao Mudanca_Data_To3"}), 1, "vSOLICITACAOMUDANCA_DATA_TO3");
               GX_FocusControl = edtavSolicitacaomudanca_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25SolicitacaoMudanca_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SolicitacaoMudanca_Data_To3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
            }
            else
            {
               AV25SolicitacaoMudanca_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSolicitacaomudanca_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SolicitacaoMudanca_Data_To3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_CODIGO");
               GX_FocusControl = edtavTfsolicitacaomudanca_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFSolicitacaoMudanca_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0)));
            }
            else
            {
               AV31TFSolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_CODIGO_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFSolicitacaoMudanca_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFSolicitacaoMudanca_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacaomudanca_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSolicitacao Mudanca_Data"}), 1, "vTFSOLICITACAOMUDANCA_DATA");
               GX_FocusControl = edtavTfsolicitacaomudanca_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFSolicitacaoMudanca_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSolicitacaoMudanca_Data", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
            }
            else
            {
               AV35TFSolicitacaoMudanca_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsolicitacaomudanca_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSolicitacaoMudanca_Data", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacaomudanca_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFSolicitacao Mudanca_Data_To"}), 1, "vTFSOLICITACAOMUDANCA_DATA_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFSolicitacaoMudanca_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSolicitacaoMudanca_Data_To", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
            }
            else
            {
               AV36TFSolicitacaoMudanca_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfsolicitacaomudanca_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSolicitacaoMudanca_Data_To", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacaomudanca_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacao Mudanca_Data Aux Date"}), 1, "vDDO_SOLICITACAOMUDANCA_DATAAUXDATE");
               GX_FocusControl = edtavDdo_solicitacaomudanca_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DDO_SolicitacaoMudanca_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DDO_SolicitacaoMudanca_DataAuxDate", context.localUtil.Format(AV37DDO_SolicitacaoMudanca_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV37DDO_SolicitacaoMudanca_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacaomudanca_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DDO_SolicitacaoMudanca_DataAuxDate", context.localUtil.Format(AV37DDO_SolicitacaoMudanca_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacaomudanca_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacao Mudanca_Data Aux Date To"}), 1, "vDDO_SOLICITACAOMUDANCA_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_solicitacaomudanca_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38DDO_SolicitacaoMudanca_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_SolicitacaoMudanca_DataAuxDateTo", context.localUtil.Format(AV38DDO_SolicitacaoMudanca_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV38DDO_SolicitacaoMudanca_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacaomudanca_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_SolicitacaoMudanca_DataAuxDateTo", context.localUtil.Format(AV38DDO_SolicitacaoMudanca_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SISTEMACOD");
               GX_FocusControl = edtavTfsolicitacaomudanca_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFSolicitacaoMudanca_SistemaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            }
            else
            {
               AV41TFSolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SISTEMACOD_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_sistemacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFSolicitacaoMudanca_SistemaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            }
            else
            {
               AV42TFSolicitacaoMudanca_SistemaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SOLICITANTE");
               GX_FocusControl = edtavTfsolicitacaomudanca_solicitante_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFSolicitacaoMudanca_Solicitante = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFSolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0)));
            }
            else
            {
               AV45TFSolicitacaoMudanca_Solicitante = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFSolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SOLICITANTE_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_solicitante_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFSolicitacaoMudanca_Solicitante_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSolicitacaoMudanca_Solicitante_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0)));
            }
            else
            {
               AV46TFSolicitacaoMudanca_Solicitante_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_solicitante_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSolicitacaoMudanca_Solicitante_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0)));
            }
            AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
            AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace", AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace);
            AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
            AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace", AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV50GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV51GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_solicitacaomudanca_codigo_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Caption");
            Ddo_solicitacaomudanca_codigo_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Tooltip");
            Ddo_solicitacaomudanca_codigo_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Cls");
            Ddo_solicitacaomudanca_codigo_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_set");
            Ddo_solicitacaomudanca_codigo_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_set");
            Ddo_solicitacaomudanca_codigo_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Dropdownoptionstype");
            Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortasc"));
            Ddo_solicitacaomudanca_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortdsc"));
            Ddo_solicitacaomudanca_codigo_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortedstatus");
            Ddo_solicitacaomudanca_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includefilter"));
            Ddo_solicitacaomudanca_codigo_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filtertype");
            Ddo_solicitacaomudanca_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filterisrange"));
            Ddo_solicitacaomudanca_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includedatalist"));
            Ddo_solicitacaomudanca_codigo_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortasc");
            Ddo_solicitacaomudanca_codigo_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortdsc");
            Ddo_solicitacaomudanca_codigo_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Cleanfilter");
            Ddo_solicitacaomudanca_codigo_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterfrom");
            Ddo_solicitacaomudanca_codigo_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterto");
            Ddo_solicitacaomudanca_codigo_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Searchbuttontext");
            Ddo_solicitacaomudanca_data_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Caption");
            Ddo_solicitacaomudanca_data_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Tooltip");
            Ddo_solicitacaomudanca_data_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Cls");
            Ddo_solicitacaomudanca_data_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filteredtext_set");
            Ddo_solicitacaomudanca_data_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filteredtextto_set");
            Ddo_solicitacaomudanca_data_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Dropdownoptionstype");
            Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Includesortasc"));
            Ddo_solicitacaomudanca_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Includesortdsc"));
            Ddo_solicitacaomudanca_data_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Sortedstatus");
            Ddo_solicitacaomudanca_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Includefilter"));
            Ddo_solicitacaomudanca_data_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filtertype");
            Ddo_solicitacaomudanca_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filterisrange"));
            Ddo_solicitacaomudanca_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Includedatalist"));
            Ddo_solicitacaomudanca_data_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Sortasc");
            Ddo_solicitacaomudanca_data_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Sortdsc");
            Ddo_solicitacaomudanca_data_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Cleanfilter");
            Ddo_solicitacaomudanca_data_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Rangefilterfrom");
            Ddo_solicitacaomudanca_data_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Rangefilterto");
            Ddo_solicitacaomudanca_data_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Searchbuttontext");
            Ddo_solicitacaomudanca_sistemacod_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Caption");
            Ddo_solicitacaomudanca_sistemacod_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Tooltip");
            Ddo_solicitacaomudanca_sistemacod_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cls");
            Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_set");
            Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_set");
            Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Dropdownoptionstype");
            Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_sistemacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortasc"));
            Ddo_solicitacaomudanca_sistemacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortdsc"));
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortedstatus");
            Ddo_solicitacaomudanca_sistemacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includefilter"));
            Ddo_solicitacaomudanca_sistemacod_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filtertype");
            Ddo_solicitacaomudanca_sistemacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filterisrange"));
            Ddo_solicitacaomudanca_sistemacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includedatalist"));
            Ddo_solicitacaomudanca_sistemacod_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortasc");
            Ddo_solicitacaomudanca_sistemacod_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortdsc");
            Ddo_solicitacaomudanca_sistemacod_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cleanfilter");
            Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterfrom");
            Ddo_solicitacaomudanca_sistemacod_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterto");
            Ddo_solicitacaomudanca_sistemacod_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Searchbuttontext");
            Ddo_solicitacaomudanca_solicitante_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Caption");
            Ddo_solicitacaomudanca_solicitante_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Tooltip");
            Ddo_solicitacaomudanca_solicitante_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Cls");
            Ddo_solicitacaomudanca_solicitante_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtext_set");
            Ddo_solicitacaomudanca_solicitante_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtextto_set");
            Ddo_solicitacaomudanca_solicitante_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Dropdownoptionstype");
            Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_solicitante_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includesortasc"));
            Ddo_solicitacaomudanca_solicitante_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includesortdsc"));
            Ddo_solicitacaomudanca_solicitante_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortedstatus");
            Ddo_solicitacaomudanca_solicitante_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includefilter"));
            Ddo_solicitacaomudanca_solicitante_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filtertype");
            Ddo_solicitacaomudanca_solicitante_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filterisrange"));
            Ddo_solicitacaomudanca_solicitante_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Includedatalist"));
            Ddo_solicitacaomudanca_solicitante_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortasc");
            Ddo_solicitacaomudanca_solicitante_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Sortdsc");
            Ddo_solicitacaomudanca_solicitante_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Cleanfilter");
            Ddo_solicitacaomudanca_solicitante_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Rangefilterfrom");
            Ddo_solicitacaomudanca_solicitante_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Rangefilterto");
            Ddo_solicitacaomudanca_solicitante_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_solicitacaomudanca_codigo_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Activeeventkey");
            Ddo_solicitacaomudanca_codigo_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_get");
            Ddo_solicitacaomudanca_codigo_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_get");
            Ddo_solicitacaomudanca_data_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Activeeventkey");
            Ddo_solicitacaomudanca_data_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filteredtext_get");
            Ddo_solicitacaomudanca_data_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_DATA_Filteredtextto_get");
            Ddo_solicitacaomudanca_sistemacod_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Activeeventkey");
            Ddo_solicitacaomudanca_sistemacod_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_get");
            Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_get");
            Ddo_solicitacaomudanca_solicitante_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Activeeventkey");
            Ddo_solicitacaomudanca_solicitante_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtext_get");
            Ddo_solicitacaomudanca_solicitante_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SOLICITANTE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA1"), 0) != AV16SolicitacaoMudanca_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO1"), 0) != AV17SolicitacaoMudanca_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA2"), 0) != AV20SolicitacaoMudanca_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO2"), 0) != AV21SolicitacaoMudanca_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA3"), 0) != AV24SolicitacaoMudanca_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vSOLICITACAOMUDANCA_DATA_TO3"), 0) != AV25SolicitacaoMudanca_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFSolicitacaoMudanca_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFSolicitacaoMudanca_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_DATA"), 0) != AV35TFSolicitacaoMudanca_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_DATA_TO"), 0) != AV36TFSolicitacaoMudanca_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV41TFSolicitacaoMudanca_SistemaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV42TFSolicitacaoMudanca_SistemaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE"), ",", ".") != Convert.ToDecimal( AV45TFSolicitacaoMudanca_Solicitante )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SOLICITANTE_TO"), ",", ".") != Convert.ToDecimal( AV46TFSolicitacaoMudanca_Solicitante_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23GZ2 */
         E23GZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23GZ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfsolicitacaomudanca_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_codigo_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_codigo_to_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_data_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_data_to_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_sistemacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_sistemacod_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_sistemacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_sistemacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_sistemacod_to_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_solicitante_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_solicitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_solicitante_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_solicitante_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_solicitante_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_solicitante_to_Visible), 5, 0)));
         Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace);
         AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace);
         AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace = Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace", AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_SistemaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace);
         AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_Solicitante";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace);
         AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace = Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace", AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Solicita��o de Mudan�a";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Id", 0);
         cmbavOrderedby.addItem("3", "Sistema", 0);
         cmbavOrderedby.addItem("4", "Solicitante", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV48DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV48DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24GZ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30SolicitacaoMudanca_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34SolicitacaoMudanca_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40SolicitacaoMudanca_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44SolicitacaoMudanca_SolicitanteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtSolicitacaoMudanca_Codigo_Titleformat = 2;
         edtSolicitacaoMudanca_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Id", AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Title", edtSolicitacaoMudanca_Codigo_Title);
         edtSolicitacaoMudanca_Data_Titleformat = 2;
         edtSolicitacaoMudanca_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Data_Internalname, "Title", edtSolicitacaoMudanca_Data_Title);
         edtSolicitacaoMudanca_SistemaCod_Titleformat = 2;
         edtSolicitacaoMudanca_SistemaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Title", edtSolicitacaoMudanca_SistemaCod_Title);
         edtSolicitacaoMudanca_Solicitante_Titleformat = 2;
         edtSolicitacaoMudanca_Solicitante_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Solicitante", AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Solicitante_Internalname, "Title", edtSolicitacaoMudanca_Solicitante_Title);
         AV50GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridCurrentPage), 10, 0)));
         AV51GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30SolicitacaoMudanca_CodigoTitleFilterData", AV30SolicitacaoMudanca_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34SolicitacaoMudanca_DataTitleFilterData", AV34SolicitacaoMudanca_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40SolicitacaoMudanca_SistemaCodTitleFilterData", AV40SolicitacaoMudanca_SistemaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44SolicitacaoMudanca_SolicitanteTitleFilterData", AV44SolicitacaoMudanca_SolicitanteTitleFilterData);
      }

      protected void E11GZ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV49PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV49PageToGo) ;
         }
      }

      protected void E12GZ2( )
      {
         /* Ddo_solicitacaomudanca_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0)));
            AV32TFSolicitacaoMudanca_Codigo_To = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GZ2( )
      {
         /* Ddo_solicitacaomudanca_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "SortedStatus", Ddo_solicitacaomudanca_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "SortedStatus", Ddo_solicitacaomudanca_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFSolicitacaoMudanca_Data = context.localUtil.CToD( Ddo_solicitacaomudanca_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSolicitacaoMudanca_Data", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
            AV36TFSolicitacaoMudanca_Data_To = context.localUtil.CToD( Ddo_solicitacaomudanca_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSolicitacaoMudanca_Data_To", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14GZ2( )
      {
         /* Ddo_solicitacaomudanca_sistemacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFSolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_sistemacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            AV42TFSolicitacaoMudanca_SistemaCod_To = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15GZ2( )
      {
         /* Ddo_solicitacaomudanca_solicitante_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_solicitante_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_solicitante_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "SortedStatus", Ddo_solicitacaomudanca_solicitante_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_solicitante_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacaomudanca_solicitante_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "SortedStatus", Ddo_solicitacaomudanca_solicitante_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_solicitante_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFSolicitacaoMudanca_Solicitante = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_solicitante_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFSolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0)));
            AV46TFSolicitacaoMudanca_Solicitante_To = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_solicitante_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSolicitacaoMudanca_Solicitante_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E25GZ2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV54Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E26GZ2 */
         E26GZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26GZ2( )
      {
         /* Enter Routine */
         AV7InOutSolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
         AV8InOutSolicitacaoMudanca_Data = A997SolicitacaoMudanca_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudanca_Data", context.localUtil.Format(AV8InOutSolicitacaoMudanca_Data, "99/99/99"));
         context.setWebReturnParms(new Object[] {(int)AV7InOutSolicitacaoMudanca_Codigo,context.localUtil.Format( AV8InOutSolicitacaoMudanca_Data, "99/99/99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E16GZ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21GZ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17GZ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22GZ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18GZ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19GZ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16SolicitacaoMudanca_Data1, AV17SolicitacaoMudanca_Data_To1, AV19DynamicFiltersSelector2, AV20SolicitacaoMudanca_Data2, AV21SolicitacaoMudanca_Data_To2, AV23DynamicFiltersSelector3, AV24SolicitacaoMudanca_Data3, AV25SolicitacaoMudanca_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacaoMudanca_Codigo, AV32TFSolicitacaoMudanca_Codigo_To, AV35TFSolicitacaoMudanca_Data, AV36TFSolicitacaoMudanca_Data_To, AV41TFSolicitacaoMudanca_SistemaCod, AV42TFSolicitacaoMudanca_SistemaCod_To, AV45TFSolicitacaoMudanca_Solicitante, AV46TFSolicitacaoMudanca_Solicitante_To, AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace, AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E20GZ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_solicitacaomudanca_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
         Ddo_solicitacaomudanca_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "SortedStatus", Ddo_solicitacaomudanca_data_Sortedstatus);
         Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
         Ddo_solicitacaomudanca_solicitante_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "SortedStatus", Ddo_solicitacaomudanca_solicitante_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_solicitacaomudanca_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_solicitacaomudanca_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "SortedStatus", Ddo_solicitacaomudanca_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_solicitacaomudanca_solicitante_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "SortedStatus", Ddo_solicitacaomudanca_solicitante_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20SolicitacaoMudanca_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SolicitacaoMudanca_Data2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
         AV21SolicitacaoMudanca_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SolicitacaoMudanca_Data_To2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24SolicitacaoMudanca_Data3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SolicitacaoMudanca_Data3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
         AV25SolicitacaoMudanca_Data_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SolicitacaoMudanca_Data_To3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFSolicitacaoMudanca_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacaoMudanca_Codigo), 6, 0)));
         Ddo_solicitacaomudanca_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_codigo_Filteredtext_set);
         AV32TFSolicitacaoMudanca_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacaoMudanca_Codigo_To), 6, 0)));
         Ddo_solicitacaomudanca_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_codigo_Filteredtextto_set);
         AV35TFSolicitacaoMudanca_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSolicitacaoMudanca_Data", context.localUtil.Format(AV35TFSolicitacaoMudanca_Data, "99/99/99"));
         Ddo_solicitacaomudanca_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_data_Filteredtext_set);
         AV36TFSolicitacaoMudanca_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSolicitacaoMudanca_Data_To", context.localUtil.Format(AV36TFSolicitacaoMudanca_Data_To, "99/99/99"));
         Ddo_solicitacaomudanca_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_data_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_data_Filteredtextto_set);
         AV41TFSolicitacaoMudanca_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFSolicitacaoMudanca_SistemaCod), 6, 0)));
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_sistemacod_Filteredtext_set);
         AV42TFSolicitacaoMudanca_SistemaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set);
         AV45TFSolicitacaoMudanca_Solicitante = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFSolicitacaoMudanca_Solicitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFSolicitacaoMudanca_Solicitante), 6, 0)));
         Ddo_solicitacaomudanca_solicitante_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_solicitante_Filteredtext_set);
         AV46TFSolicitacaoMudanca_Solicitante_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFSolicitacaoMudanca_Solicitante_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFSolicitacaoMudanca_Solicitante_To), 6, 0)));
         Ddo_solicitacaomudanca_solicitante_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_solicitante_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_solicitante_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "SOLICITACAOMUDANCA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16SolicitacaoMudanca_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SolicitacaoMudanca_Data1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
         AV17SolicitacaoMudanca_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SolicitacaoMudanca_Data_To1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 )
            {
               AV16SolicitacaoMudanca_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SolicitacaoMudanca_Data1", context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"));
               AV17SolicitacaoMudanca_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SolicitacaoMudanca_Data_To1", context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 )
               {
                  AV20SolicitacaoMudanca_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20SolicitacaoMudanca_Data2", context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"));
                  AV21SolicitacaoMudanca_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SolicitacaoMudanca_Data_To2", context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 )
                  {
                     AV24SolicitacaoMudanca_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24SolicitacaoMudanca_Data3", context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"));
                     AV25SolicitacaoMudanca_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SolicitacaoMudanca_Data_To3", context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16SolicitacaoMudanca_Data1) && (DateTime.MinValue==AV17SolicitacaoMudanca_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16SolicitacaoMudanca_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17SolicitacaoMudanca_Data_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20SolicitacaoMudanca_Data2) && (DateTime.MinValue==AV21SolicitacaoMudanca_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20SolicitacaoMudanca_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21SolicitacaoMudanca_Data_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV24SolicitacaoMudanca_Data3) && (DateTime.MinValue==AV25SolicitacaoMudanca_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24SolicitacaoMudanca_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25SolicitacaoMudanca_Data_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_GZ2( true) ;
         }
         else
         {
            wb_table2_5_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_GZ2( true) ;
         }
         else
         {
            wb_table3_80_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GZ2e( true) ;
         }
         else
         {
            wb_table1_2_GZ2e( false) ;
         }
      }

      protected void wb_table3_80_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_GZ2( true) ;
         }
         else
         {
            wb_table4_83_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_GZ2e( true) ;
         }
         else
         {
            wb_table3_80_GZ2e( false) ;
         }
      }

      protected void wb_table4_83_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_Solicitante_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_Solicitante_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_Solicitante_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_Solicitante_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_Solicitante_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_GZ2e( true) ;
         }
         else
         {
            wb_table4_83_GZ2e( false) ;
         }
      }

      protected void wb_table2_5_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptSolicitacaoMudanca.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_GZ2( true) ;
         }
         else
         {
            wb_table5_14_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GZ2e( true) ;
         }
         else
         {
            wb_table2_5_GZ2e( false) ;
         }
      }

      protected void wb_table5_14_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_GZ2( true) ;
         }
         else
         {
            wb_table6_19_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_GZ2e( true) ;
         }
         else
         {
            wb_table5_14_GZ2e( false) ;
         }
      }

      protected void wb_table6_19_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e27gz1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptSolicitacaoMudanca.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_GZ2( true) ;
         }
         else
         {
            wb_table7_28_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e28gz1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptSolicitacaoMudanca.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_GZ2( true) ;
         }
         else
         {
            wb_table8_47_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e29gz1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptSolicitacaoMudanca.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_GZ2( true) ;
         }
         else
         {
            wb_table9_66_GZ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_GZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_GZ2e( true) ;
         }
         else
         {
            wb_table6_19_GZ2e( false) ;
         }
      }

      protected void wb_table9_66_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname, tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data3_Internalname, context.localUtil.Format(AV24SolicitacaoMudanca_Data3, "99/99/99"), context.localUtil.Format( AV24SolicitacaoMudanca_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data_to3_Internalname, context.localUtil.Format(AV25SolicitacaoMudanca_Data_To3, "99/99/99"), context.localUtil.Format( AV25SolicitacaoMudanca_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_GZ2e( true) ;
         }
         else
         {
            wb_table9_66_GZ2e( false) ;
         }
      }

      protected void wb_table8_47_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname, tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data2_Internalname, context.localUtil.Format(AV20SolicitacaoMudanca_Data2, "99/99/99"), context.localUtil.Format( AV20SolicitacaoMudanca_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data_to2_Internalname, context.localUtil.Format(AV21SolicitacaoMudanca_Data_To2, "99/99/99"), context.localUtil.Format( AV21SolicitacaoMudanca_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_GZ2e( true) ;
         }
         else
         {
            wb_table8_47_GZ2e( false) ;
         }
      }

      protected void wb_table7_28_GZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname, tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data1_Internalname, context.localUtil.Format(AV16SolicitacaoMudanca_Data1, "99/99/99"), context.localUtil.Format( AV16SolicitacaoMudanca_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSolicitacaomudanca_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSolicitacaomudanca_data_to1_Internalname, context.localUtil.Format(AV17SolicitacaoMudanca_Data_To1, "99/99/99"), context.localUtil.Format( AV17SolicitacaoMudanca_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacaomudanca_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudanca.htm");
            GxWebStd.gx_bitmap( context, edtavSolicitacaomudanca_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacaoMudanca.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_GZ2e( true) ;
         }
         else
         {
            wb_table7_28_GZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutSolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
         AV8InOutSolicitacaoMudanca_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudanca_Data", context.localUtil.Format(AV8InOutSolicitacaoMudanca_Data, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGZ2( ) ;
         WSGZ2( ) ;
         WEGZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311855633");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptsolicitacaomudanca.js", "?2020311855633");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO_"+sGXsfl_86_idx;
         edtSolicitacaoMudanca_Data_Internalname = "SOLICITACAOMUDANCA_DATA_"+sGXsfl_86_idx;
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD_"+sGXsfl_86_idx;
         edtSolicitacaoMudanca_Solicitante_Internalname = "SOLICITACAOMUDANCA_SOLICITANTE_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO_"+sGXsfl_86_fel_idx;
         edtSolicitacaoMudanca_Data_Internalname = "SOLICITACAOMUDANCA_DATA_"+sGXsfl_86_fel_idx;
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD_"+sGXsfl_86_fel_idx;
         edtSolicitacaoMudanca_Solicitante_Internalname = "SOLICITACAOMUDANCA_SOLICITANTE_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBGZ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV54Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_Data_Internalname,context.localUtil.Format(A997SolicitacaoMudanca_Data, "99/99/99"),context.localUtil.Format( A997SolicitacaoMudanca_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_Solicitante_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A994SolicitacaoMudanca_Solicitante), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_Solicitante_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_DATA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A997SolicitacaoMudanca_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_SISTEMACOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_SOLICITANTE"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A994SolicitacaoMudanca_Solicitante), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavSolicitacaomudanca_data1_Internalname = "vSOLICITACAOMUDANCA_DATA1";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Internalname = "DYNAMICFILTERSSOLICITACAOMUDANCA_DATA_RANGEMIDDLETEXT1";
         edtavSolicitacaomudanca_data_to1_Internalname = "vSOLICITACAOMUDANCA_DATA_TO1";
         tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavSolicitacaomudanca_data2_Internalname = "vSOLICITACAOMUDANCA_DATA2";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Internalname = "DYNAMICFILTERSSOLICITACAOMUDANCA_DATA_RANGEMIDDLETEXT2";
         edtavSolicitacaomudanca_data_to2_Internalname = "vSOLICITACAOMUDANCA_DATA_TO2";
         tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavSolicitacaomudanca_data3_Internalname = "vSOLICITACAOMUDANCA_DATA3";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Internalname = "DYNAMICFILTERSSOLICITACAOMUDANCA_DATA_RANGEMIDDLETEXT3";
         edtavSolicitacaomudanca_data_to3_Internalname = "vSOLICITACAOMUDANCA_DATA_TO3";
         tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO";
         edtSolicitacaoMudanca_Data_Internalname = "SOLICITACAOMUDANCA_DATA";
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD";
         edtSolicitacaoMudanca_Solicitante_Internalname = "SOLICITACAOMUDANCA_SOLICITANTE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfsolicitacaomudanca_codigo_Internalname = "vTFSOLICITACAOMUDANCA_CODIGO";
         edtavTfsolicitacaomudanca_codigo_to_Internalname = "vTFSOLICITACAOMUDANCA_CODIGO_TO";
         edtavTfsolicitacaomudanca_data_Internalname = "vTFSOLICITACAOMUDANCA_DATA";
         edtavTfsolicitacaomudanca_data_to_Internalname = "vTFSOLICITACAOMUDANCA_DATA_TO";
         edtavDdo_solicitacaomudanca_dataauxdate_Internalname = "vDDO_SOLICITACAOMUDANCA_DATAAUXDATE";
         edtavDdo_solicitacaomudanca_dataauxdateto_Internalname = "vDDO_SOLICITACAOMUDANCA_DATAAUXDATETO";
         divDdo_solicitacaomudanca_dataauxdates_Internalname = "DDO_SOLICITACAOMUDANCA_DATAAUXDATES";
         edtavTfsolicitacaomudanca_sistemacod_Internalname = "vTFSOLICITACAOMUDANCA_SISTEMACOD";
         edtavTfsolicitacaomudanca_sistemacod_to_Internalname = "vTFSOLICITACAOMUDANCA_SISTEMACOD_TO";
         edtavTfsolicitacaomudanca_solicitante_Internalname = "vTFSOLICITACAOMUDANCA_SOLICITANTE";
         edtavTfsolicitacaomudanca_solicitante_to_Internalname = "vTFSOLICITACAOMUDANCA_SOLICITANTE_TO";
         Ddo_solicitacaomudanca_codigo_Internalname = "DDO_SOLICITACAOMUDANCA_CODIGO";
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacaomudanca_data_Internalname = "DDO_SOLICITACAOMUDANCA_DATA";
         edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE";
         Ddo_solicitacaomudanca_sistemacod_Internalname = "DDO_SOLICITACAOMUDANCA_SISTEMACOD";
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE";
         Ddo_solicitacaomudanca_solicitante_Internalname = "DDO_SOLICITACAOMUDANCA_SOLICITANTE";
         edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtSolicitacaoMudanca_Solicitante_Jsonclick = "";
         edtSolicitacaoMudanca_SistemaCod_Jsonclick = "";
         edtSolicitacaoMudanca_Data_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavSolicitacaomudanca_data_to1_Jsonclick = "";
         edtavSolicitacaomudanca_data1_Jsonclick = "";
         edtavSolicitacaomudanca_data_to2_Jsonclick = "";
         edtavSolicitacaomudanca_data2_Jsonclick = "";
         edtavSolicitacaomudanca_data_to3_Jsonclick = "";
         edtavSolicitacaomudanca_data3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtSolicitacaoMudanca_Solicitante_Titleformat = 0;
         edtSolicitacaoMudanca_SistemaCod_Titleformat = 0;
         edtSolicitacaoMudanca_Data_Titleformat = 0;
         edtSolicitacaoMudanca_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible = 1;
         tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible = 1;
         tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible = 1;
         edtSolicitacaoMudanca_Solicitante_Title = "Solicitante";
         edtSolicitacaoMudanca_SistemaCod_Title = "Sistema";
         edtSolicitacaoMudanca_Data_Title = "Data";
         edtSolicitacaoMudanca_Codigo_Title = "Id";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfsolicitacaomudanca_solicitante_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_solicitante_to_Visible = 1;
         edtavTfsolicitacaomudanca_solicitante_Jsonclick = "";
         edtavTfsolicitacaomudanca_solicitante_Visible = 1;
         edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_sistemacod_to_Visible = 1;
         edtavTfsolicitacaomudanca_sistemacod_Jsonclick = "";
         edtavTfsolicitacaomudanca_sistemacod_Visible = 1;
         edtavDdo_solicitacaomudanca_dataauxdateto_Jsonclick = "";
         edtavDdo_solicitacaomudanca_dataauxdate_Jsonclick = "";
         edtavTfsolicitacaomudanca_data_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_data_to_Visible = 1;
         edtavTfsolicitacaomudanca_data_Jsonclick = "";
         edtavTfsolicitacaomudanca_data_Visible = 1;
         edtavTfsolicitacaomudanca_codigo_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_codigo_to_Visible = 1;
         edtavTfsolicitacaomudanca_codigo_Jsonclick = "";
         edtavTfsolicitacaomudanca_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_solicitacaomudanca_solicitante_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_solicitante_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_solicitante_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_solicitante_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_solicitante_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_solicitante_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_solicitante_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_solicitante_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_solicitante_Filtertype = "Numeric";
         Ddo_solicitacaomudanca_solicitante_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_solicitante_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_solicitante_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_solicitante_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_solicitante_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_solicitante_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_solicitante_Caption = "";
         Ddo_solicitacaomudanca_sistemacod_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_sistemacod_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_sistemacod_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_sistemacod_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_sistemacod_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_sistemacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_sistemacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Filtertype = "Numeric";
         Ddo_solicitacaomudanca_sistemacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_sistemacod_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_sistemacod_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_sistemacod_Caption = "";
         Ddo_solicitacaomudanca_data_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_data_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_data_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_data_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_data_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_data_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_data_Filtertype = "Date";
         Ddo_solicitacaomudanca_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_data_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_data_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_data_Caption = "";
         Ddo_solicitacaomudanca_codigo_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_codigo_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_codigo_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Filtertype = "Numeric";
         Ddo_solicitacaomudanca_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_codigo_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_codigo_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Solicita��o de Mudan�a";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV30SolicitacaoMudanca_CodigoTitleFilterData',fld:'vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34SolicitacaoMudanca_DataTitleFilterData',fld:'vSOLICITACAOMUDANCA_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV40SolicitacaoMudanca_SistemaCodTitleFilterData',fld:'vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV44SolicitacaoMudanca_SolicitanteTitleFilterData',fld:'vSOLICITACAOMUDANCA_SOLICITANTETITLEFILTERDATA',pic:'',nv:null},{av:'edtSolicitacaoMudanca_Codigo_Titleformat',ctrl:'SOLICITACAOMUDANCA_CODIGO',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_Codigo_Title',ctrl:'SOLICITACAOMUDANCA_CODIGO',prop:'Title'},{av:'edtSolicitacaoMudanca_Data_Titleformat',ctrl:'SOLICITACAOMUDANCA_DATA',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_Data_Title',ctrl:'SOLICITACAOMUDANCA_DATA',prop:'Title'},{av:'edtSolicitacaoMudanca_SistemaCod_Titleformat',ctrl:'SOLICITACAOMUDANCA_SISTEMACOD',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_SistemaCod_Title',ctrl:'SOLICITACAOMUDANCA_SISTEMACOD',prop:'Title'},{av:'edtSolicitacaoMudanca_Solicitante_Titleformat',ctrl:'SOLICITACAOMUDANCA_SOLICITANTE',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_Solicitante_Title',ctrl:'SOLICITACAOMUDANCA_SOLICITANTE',prop:'Title'},{av:'AV50GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV51GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_CODIGO.ONOPTIONCLICKED","{handler:'E12GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_codigo_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_codigo_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_codigo_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_data_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_solicitante_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_DATA.ONOPTIONCLICKED","{handler:'E13GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_data_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_data_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_data_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_data_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'SortedStatus'},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_solicitante_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_SISTEMACOD.ONOPTIONCLICKED","{handler:'E14GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_sistemacod_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_data_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_solicitante_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_SOLICITANTE.ONOPTIONCLICKED","{handler:'E15GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_solicitante_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_solicitante_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_solicitante_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_solicitante_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'SortedStatus'},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_data_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25GZ2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E26GZ2',iparms:[{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A997SolicitacaoMudanca_Data',fld:'SOLICITACAOMUDANCA_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutSolicitacaoMudanca_Codigo',fld:'vINOUTSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutSolicitacaoMudanca_Data',fld:'vINOUTSOLICITACAOMUDANCA_DATA',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21GZ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E27GZ1',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22GZ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28GZ1',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29GZ1',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20GZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SOLICITANTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredText_set'},{av:'AV32TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFSolicitacaoMudanca_Data',fld:'vTFSOLICITACAOMUDANCA_DATA',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_data_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'FilteredText_set'},{av:'AV36TFSolicitacaoMudanca_Data_To',fld:'vTFSOLICITACAOMUDANCA_DATA_TO',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_data_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_DATA',prop:'FilteredTextTo_set'},{av:'AV41TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredText_set'},{av:'AV42TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredTextTo_set'},{av:'AV45TFSolicitacaoMudanca_Solicitante',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_solicitante_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'FilteredText_set'},{av:'AV46TFSolicitacaoMudanca_Solicitante_To',fld:'vTFSOLICITACAOMUDANCA_SOLICITANTE_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_solicitante_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_SOLICITANTE',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16SolicitacaoMudanca_Data1',fld:'vSOLICITACAOMUDANCA_DATA1',pic:'',nv:''},{av:'AV17SolicitacaoMudanca_Data_To1',fld:'vSOLICITACAOMUDANCA_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20SolicitacaoMudanca_Data2',fld:'vSOLICITACAOMUDANCA_DATA2',pic:'',nv:''},{av:'AV21SolicitacaoMudanca_Data_To2',fld:'vSOLICITACAOMUDANCA_DATA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24SolicitacaoMudanca_Data3',fld:'vSOLICITACAOMUDANCA_DATA3',pic:'',nv:''},{av:'AV25SolicitacaoMudanca_Data_To3',fld:'vSOLICITACAOMUDANCA_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSSOLICITACAOMUDANCA_DATA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutSolicitacaoMudanca_Data = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_solicitacaomudanca_codigo_Activeeventkey = "";
         Ddo_solicitacaomudanca_codigo_Filteredtext_get = "";
         Ddo_solicitacaomudanca_codigo_Filteredtextto_get = "";
         Ddo_solicitacaomudanca_data_Activeeventkey = "";
         Ddo_solicitacaomudanca_data_Filteredtext_get = "";
         Ddo_solicitacaomudanca_data_Filteredtextto_get = "";
         Ddo_solicitacaomudanca_sistemacod_Activeeventkey = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_get = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get = "";
         Ddo_solicitacaomudanca_solicitante_Activeeventkey = "";
         Ddo_solicitacaomudanca_solicitante_Filteredtext_get = "";
         Ddo_solicitacaomudanca_solicitante_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16SolicitacaoMudanca_Data1 = DateTime.MinValue;
         AV17SolicitacaoMudanca_Data_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20SolicitacaoMudanca_Data2 = DateTime.MinValue;
         AV21SolicitacaoMudanca_Data_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24SolicitacaoMudanca_Data3 = DateTime.MinValue;
         AV25SolicitacaoMudanca_Data_To3 = DateTime.MinValue;
         AV35TFSolicitacaoMudanca_Data = DateTime.MinValue;
         AV36TFSolicitacaoMudanca_Data_To = DateTime.MinValue;
         AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = "";
         AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace = "";
         AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = "";
         AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV48DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30SolicitacaoMudanca_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34SolicitacaoMudanca_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40SolicitacaoMudanca_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44SolicitacaoMudanca_SolicitanteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_solicitacaomudanca_codigo_Filteredtext_set = "";
         Ddo_solicitacaomudanca_codigo_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_codigo_Sortedstatus = "";
         Ddo_solicitacaomudanca_data_Filteredtext_set = "";
         Ddo_solicitacaomudanca_data_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_data_Sortedstatus = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "";
         Ddo_solicitacaomudanca_solicitante_Filteredtext_set = "";
         Ddo_solicitacaomudanca_solicitante_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_solicitante_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV37DDO_SolicitacaoMudanca_DataAuxDate = DateTime.MinValue;
         AV38DDO_SolicitacaoMudanca_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV54Select_GXI = "";
         A997SolicitacaoMudanca_Data = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00GZ2_A994SolicitacaoMudanca_Solicitante = new int[1] ;
         H00GZ2_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         H00GZ2_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         H00GZ2_A997SolicitacaoMudanca_Data = new DateTime[] {DateTime.MinValue} ;
         H00GZ2_A996SolicitacaoMudanca_Codigo = new int[1] ;
         H00GZ3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptsolicitacaomudanca__default(),
            new Object[][] {
                new Object[] {
               H00GZ2_A994SolicitacaoMudanca_Solicitante, H00GZ2_A993SolicitacaoMudanca_SistemaCod, H00GZ2_n993SolicitacaoMudanca_SistemaCod, H00GZ2_A997SolicitacaoMudanca_Data, H00GZ2_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               H00GZ3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSolicitacaoMudanca_Codigo_Titleformat ;
      private short edtSolicitacaoMudanca_Data_Titleformat ;
      private short edtSolicitacaoMudanca_SistemaCod_Titleformat ;
      private short edtSolicitacaoMudanca_Solicitante_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutSolicitacaoMudanca_Codigo ;
      private int wcpOAV7InOutSolicitacaoMudanca_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFSolicitacaoMudanca_Codigo ;
      private int AV32TFSolicitacaoMudanca_Codigo_To ;
      private int AV41TFSolicitacaoMudanca_SistemaCod ;
      private int AV42TFSolicitacaoMudanca_SistemaCod_To ;
      private int AV45TFSolicitacaoMudanca_Solicitante ;
      private int AV46TFSolicitacaoMudanca_Solicitante_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfsolicitacaomudanca_codigo_Visible ;
      private int edtavTfsolicitacaomudanca_codigo_to_Visible ;
      private int edtavTfsolicitacaomudanca_data_Visible ;
      private int edtavTfsolicitacaomudanca_data_to_Visible ;
      private int edtavTfsolicitacaomudanca_sistemacod_Visible ;
      private int edtavTfsolicitacaomudanca_sistemacod_to_Visible ;
      private int edtavTfsolicitacaomudanca_solicitante_Visible ;
      private int edtavTfsolicitacaomudanca_solicitante_to_Visible ;
      private int edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Visible ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int A993SolicitacaoMudanca_SistemaCod ;
      private int A994SolicitacaoMudanca_Solicitante ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV49PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterssolicitacaomudanca_data1_Visible ;
      private int tblTablemergeddynamicfilterssolicitacaomudanca_data2_Visible ;
      private int tblTablemergeddynamicfilterssolicitacaomudanca_data3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV50GridCurrentPage ;
      private long AV51GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_solicitacaomudanca_codigo_Activeeventkey ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtextto_get ;
      private String Ddo_solicitacaomudanca_data_Activeeventkey ;
      private String Ddo_solicitacaomudanca_data_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_data_Filteredtextto_get ;
      private String Ddo_solicitacaomudanca_sistemacod_Activeeventkey ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get ;
      private String Ddo_solicitacaomudanca_solicitante_Activeeventkey ;
      private String Ddo_solicitacaomudanca_solicitante_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_solicitante_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_solicitacaomudanca_codigo_Caption ;
      private String Ddo_solicitacaomudanca_codigo_Tooltip ;
      private String Ddo_solicitacaomudanca_codigo_Cls ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_codigo_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_codigo_Sortedstatus ;
      private String Ddo_solicitacaomudanca_codigo_Filtertype ;
      private String Ddo_solicitacaomudanca_codigo_Sortasc ;
      private String Ddo_solicitacaomudanca_codigo_Sortdsc ;
      private String Ddo_solicitacaomudanca_codigo_Cleanfilter ;
      private String Ddo_solicitacaomudanca_codigo_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_codigo_Rangefilterto ;
      private String Ddo_solicitacaomudanca_codigo_Searchbuttontext ;
      private String Ddo_solicitacaomudanca_data_Caption ;
      private String Ddo_solicitacaomudanca_data_Tooltip ;
      private String Ddo_solicitacaomudanca_data_Cls ;
      private String Ddo_solicitacaomudanca_data_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_data_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_data_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_data_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_data_Sortedstatus ;
      private String Ddo_solicitacaomudanca_data_Filtertype ;
      private String Ddo_solicitacaomudanca_data_Sortasc ;
      private String Ddo_solicitacaomudanca_data_Sortdsc ;
      private String Ddo_solicitacaomudanca_data_Cleanfilter ;
      private String Ddo_solicitacaomudanca_data_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_data_Rangefilterto ;
      private String Ddo_solicitacaomudanca_data_Searchbuttontext ;
      private String Ddo_solicitacaomudanca_sistemacod_Caption ;
      private String Ddo_solicitacaomudanca_sistemacod_Tooltip ;
      private String Ddo_solicitacaomudanca_sistemacod_Cls ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortedstatus ;
      private String Ddo_solicitacaomudanca_sistemacod_Filtertype ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortasc ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortdsc ;
      private String Ddo_solicitacaomudanca_sistemacod_Cleanfilter ;
      private String Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_sistemacod_Rangefilterto ;
      private String Ddo_solicitacaomudanca_sistemacod_Searchbuttontext ;
      private String Ddo_solicitacaomudanca_solicitante_Caption ;
      private String Ddo_solicitacaomudanca_solicitante_Tooltip ;
      private String Ddo_solicitacaomudanca_solicitante_Cls ;
      private String Ddo_solicitacaomudanca_solicitante_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_solicitante_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_solicitante_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_solicitante_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_solicitante_Sortedstatus ;
      private String Ddo_solicitacaomudanca_solicitante_Filtertype ;
      private String Ddo_solicitacaomudanca_solicitante_Sortasc ;
      private String Ddo_solicitacaomudanca_solicitante_Sortdsc ;
      private String Ddo_solicitacaomudanca_solicitante_Cleanfilter ;
      private String Ddo_solicitacaomudanca_solicitante_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_solicitante_Rangefilterto ;
      private String Ddo_solicitacaomudanca_solicitante_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfsolicitacaomudanca_codigo_Internalname ;
      private String edtavTfsolicitacaomudanca_codigo_Jsonclick ;
      private String edtavTfsolicitacaomudanca_codigo_to_Internalname ;
      private String edtavTfsolicitacaomudanca_codigo_to_Jsonclick ;
      private String edtavTfsolicitacaomudanca_data_Internalname ;
      private String edtavTfsolicitacaomudanca_data_Jsonclick ;
      private String edtavTfsolicitacaomudanca_data_to_Internalname ;
      private String edtavTfsolicitacaomudanca_data_to_Jsonclick ;
      private String divDdo_solicitacaomudanca_dataauxdates_Internalname ;
      private String edtavDdo_solicitacaomudanca_dataauxdate_Internalname ;
      private String edtavDdo_solicitacaomudanca_dataauxdate_Jsonclick ;
      private String edtavDdo_solicitacaomudanca_dataauxdateto_Internalname ;
      private String edtavDdo_solicitacaomudanca_dataauxdateto_Jsonclick ;
      private String edtavTfsolicitacaomudanca_sistemacod_Internalname ;
      private String edtavTfsolicitacaomudanca_sistemacod_Jsonclick ;
      private String edtavTfsolicitacaomudanca_sistemacod_to_Internalname ;
      private String edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick ;
      private String edtavTfsolicitacaomudanca_solicitante_Internalname ;
      private String edtavTfsolicitacaomudanca_solicitante_Jsonclick ;
      private String edtavTfsolicitacaomudanca_solicitante_to_Internalname ;
      private String edtavTfsolicitacaomudanca_solicitante_to_Jsonclick ;
      private String edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacaomudanca_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacaomudanca_solicitantetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Internalname ;
      private String edtSolicitacaoMudanca_Data_Internalname ;
      private String edtSolicitacaoMudanca_SistemaCod_Internalname ;
      private String edtSolicitacaoMudanca_Solicitante_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavSolicitacaomudanca_data1_Internalname ;
      private String edtavSolicitacaomudanca_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavSolicitacaomudanca_data2_Internalname ;
      private String edtavSolicitacaomudanca_data_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavSolicitacaomudanca_data3_Internalname ;
      private String edtavSolicitacaomudanca_data_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_solicitacaomudanca_codigo_Internalname ;
      private String Ddo_solicitacaomudanca_data_Internalname ;
      private String Ddo_solicitacaomudanca_sistemacod_Internalname ;
      private String Ddo_solicitacaomudanca_solicitante_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Title ;
      private String edtSolicitacaoMudanca_Data_Title ;
      private String edtSolicitacaoMudanca_SistemaCod_Title ;
      private String edtSolicitacaoMudanca_Solicitante_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterssolicitacaomudanca_data1_Internalname ;
      private String tblTablemergeddynamicfilterssolicitacaomudanca_data2_Internalname ;
      private String tblTablemergeddynamicfilterssolicitacaomudanca_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavSolicitacaomudanca_data3_Jsonclick ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Internalname ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext3_Jsonclick ;
      private String edtavSolicitacaomudanca_data_to3_Jsonclick ;
      private String edtavSolicitacaomudanca_data2_Jsonclick ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext2_Jsonclick ;
      private String edtavSolicitacaomudanca_data_to2_Jsonclick ;
      private String edtavSolicitacaomudanca_data1_Jsonclick ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterssolicitacaomudanca_data_rangemiddletext1_Jsonclick ;
      private String edtavSolicitacaomudanca_data_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtSolicitacaoMudanca_Codigo_Jsonclick ;
      private String edtSolicitacaoMudanca_Data_Jsonclick ;
      private String edtSolicitacaoMudanca_SistemaCod_Jsonclick ;
      private String edtSolicitacaoMudanca_Solicitante_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutSolicitacaoMudanca_Data ;
      private DateTime wcpOAV8InOutSolicitacaoMudanca_Data ;
      private DateTime AV16SolicitacaoMudanca_Data1 ;
      private DateTime AV17SolicitacaoMudanca_Data_To1 ;
      private DateTime AV20SolicitacaoMudanca_Data2 ;
      private DateTime AV21SolicitacaoMudanca_Data_To2 ;
      private DateTime AV24SolicitacaoMudanca_Data3 ;
      private DateTime AV25SolicitacaoMudanca_Data_To3 ;
      private DateTime AV35TFSolicitacaoMudanca_Data ;
      private DateTime AV36TFSolicitacaoMudanca_Data_To ;
      private DateTime AV37DDO_SolicitacaoMudanca_DataAuxDate ;
      private DateTime AV38DDO_SolicitacaoMudanca_DataAuxDateTo ;
      private DateTime A997SolicitacaoMudanca_Data ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_solicitacaomudanca_codigo_Includesortasc ;
      private bool Ddo_solicitacaomudanca_codigo_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_codigo_Includefilter ;
      private bool Ddo_solicitacaomudanca_codigo_Filterisrange ;
      private bool Ddo_solicitacaomudanca_codigo_Includedatalist ;
      private bool Ddo_solicitacaomudanca_data_Includesortasc ;
      private bool Ddo_solicitacaomudanca_data_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_data_Includefilter ;
      private bool Ddo_solicitacaomudanca_data_Filterisrange ;
      private bool Ddo_solicitacaomudanca_data_Includedatalist ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includesortasc ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includefilter ;
      private bool Ddo_solicitacaomudanca_sistemacod_Filterisrange ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includedatalist ;
      private bool Ddo_solicitacaomudanca_solicitante_Includesortasc ;
      private bool Ddo_solicitacaomudanca_solicitante_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_solicitante_Includefilter ;
      private bool Ddo_solicitacaomudanca_solicitante_Filterisrange ;
      private bool Ddo_solicitacaomudanca_solicitante_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n993SolicitacaoMudanca_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace ;
      private String AV39ddo_SolicitacaoMudanca_DataTitleControlIdToReplace ;
      private String AV43ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace ;
      private String AV47ddo_SolicitacaoMudanca_SolicitanteTitleControlIdToReplace ;
      private String AV54Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutSolicitacaoMudanca_Codigo ;
      private DateTime aP1_InOutSolicitacaoMudanca_Data ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00GZ2_A994SolicitacaoMudanca_Solicitante ;
      private int[] H00GZ2_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] H00GZ2_n993SolicitacaoMudanca_SistemaCod ;
      private DateTime[] H00GZ2_A997SolicitacaoMudanca_Data ;
      private int[] H00GZ2_A996SolicitacaoMudanca_Codigo ;
      private long[] H00GZ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30SolicitacaoMudanca_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34SolicitacaoMudanca_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40SolicitacaoMudanca_SistemaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44SolicitacaoMudanca_SolicitanteTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV48DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptsolicitacaomudanca__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GZ2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16SolicitacaoMudanca_Data1 ,
                                             DateTime AV17SolicitacaoMudanca_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20SolicitacaoMudanca_Data2 ,
                                             DateTime AV21SolicitacaoMudanca_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24SolicitacaoMudanca_Data3 ,
                                             DateTime AV25SolicitacaoMudanca_Data_To3 ,
                                             int AV31TFSolicitacaoMudanca_Codigo ,
                                             int AV32TFSolicitacaoMudanca_Codigo_To ,
                                             DateTime AV35TFSolicitacaoMudanca_Data ,
                                             DateTime AV36TFSolicitacaoMudanca_Data_To ,
                                             int AV41TFSolicitacaoMudanca_SistemaCod ,
                                             int AV42TFSolicitacaoMudanca_SistemaCod_To ,
                                             int AV45TFSolicitacaoMudanca_Solicitante ,
                                             int AV46TFSolicitacaoMudanca_Solicitante_To ,
                                             DateTime A997SolicitacaoMudanca_Data ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int A993SolicitacaoMudanca_SistemaCod ,
                                             int A994SolicitacaoMudanca_Solicitante ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [19] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [SolicitacaoMudanca_Solicitante], [SolicitacaoMudanca_SistemaCod], [SolicitacaoMudanca_Data], [SolicitacaoMudanca_Codigo]";
         sFromString = " FROM [SolicitacaoMudanca] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16SolicitacaoMudanca_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV16SolicitacaoMudanca_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV16SolicitacaoMudanca_Data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17SolicitacaoMudanca_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV17SolicitacaoMudanca_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV17SolicitacaoMudanca_Data_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20SolicitacaoMudanca_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV20SolicitacaoMudanca_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV20SolicitacaoMudanca_Data2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21SolicitacaoMudanca_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV21SolicitacaoMudanca_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV21SolicitacaoMudanca_Data_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24SolicitacaoMudanca_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV24SolicitacaoMudanca_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV24SolicitacaoMudanca_Data3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25SolicitacaoMudanca_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV25SolicitacaoMudanca_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV25SolicitacaoMudanca_Data_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFSolicitacaoMudanca_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Codigo] >= @AV31TFSolicitacaoMudanca_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Codigo] >= @AV31TFSolicitacaoMudanca_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFSolicitacaoMudanca_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Codigo] <= @AV32TFSolicitacaoMudanca_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Codigo] <= @AV32TFSolicitacaoMudanca_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFSolicitacaoMudanca_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV35TFSolicitacaoMudanca_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV35TFSolicitacaoMudanca_Data)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFSolicitacaoMudanca_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV36TFSolicitacaoMudanca_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV36TFSolicitacaoMudanca_Data_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV41TFSolicitacaoMudanca_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_SistemaCod] >= @AV41TFSolicitacaoMudanca_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_SistemaCod] >= @AV41TFSolicitacaoMudanca_SistemaCod)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV42TFSolicitacaoMudanca_SistemaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_SistemaCod] <= @AV42TFSolicitacaoMudanca_SistemaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_SistemaCod] <= @AV42TFSolicitacaoMudanca_SistemaCod_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV45TFSolicitacaoMudanca_Solicitante) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Solicitante] >= @AV45TFSolicitacaoMudanca_Solicitante)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Solicitante] >= @AV45TFSolicitacaoMudanca_Solicitante)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV46TFSolicitacaoMudanca_Solicitante_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Solicitante] <= @AV46TFSolicitacaoMudanca_Solicitante_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Solicitante] <= @AV46TFSolicitacaoMudanca_Solicitante_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_SistemaCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_SistemaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Solicitante]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Solicitante] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [SolicitacaoMudanca_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GZ3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16SolicitacaoMudanca_Data1 ,
                                             DateTime AV17SolicitacaoMudanca_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20SolicitacaoMudanca_Data2 ,
                                             DateTime AV21SolicitacaoMudanca_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24SolicitacaoMudanca_Data3 ,
                                             DateTime AV25SolicitacaoMudanca_Data_To3 ,
                                             int AV31TFSolicitacaoMudanca_Codigo ,
                                             int AV32TFSolicitacaoMudanca_Codigo_To ,
                                             DateTime AV35TFSolicitacaoMudanca_Data ,
                                             DateTime AV36TFSolicitacaoMudanca_Data_To ,
                                             int AV41TFSolicitacaoMudanca_SistemaCod ,
                                             int AV42TFSolicitacaoMudanca_SistemaCod_To ,
                                             int AV45TFSolicitacaoMudanca_Solicitante ,
                                             int AV46TFSolicitacaoMudanca_Solicitante_To ,
                                             DateTime A997SolicitacaoMudanca_Data ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int A993SolicitacaoMudanca_SistemaCod ,
                                             int A994SolicitacaoMudanca_Solicitante ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [14] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [SolicitacaoMudanca] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16SolicitacaoMudanca_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV16SolicitacaoMudanca_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV16SolicitacaoMudanca_Data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17SolicitacaoMudanca_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV17SolicitacaoMudanca_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV17SolicitacaoMudanca_Data_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20SolicitacaoMudanca_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV20SolicitacaoMudanca_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV20SolicitacaoMudanca_Data2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21SolicitacaoMudanca_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV21SolicitacaoMudanca_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV21SolicitacaoMudanca_Data_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24SolicitacaoMudanca_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV24SolicitacaoMudanca_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV24SolicitacaoMudanca_Data3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACAOMUDANCA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25SolicitacaoMudanca_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV25SolicitacaoMudanca_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV25SolicitacaoMudanca_Data_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFSolicitacaoMudanca_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Codigo] >= @AV31TFSolicitacaoMudanca_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Codigo] >= @AV31TFSolicitacaoMudanca_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFSolicitacaoMudanca_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Codigo] <= @AV32TFSolicitacaoMudanca_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Codigo] <= @AV32TFSolicitacaoMudanca_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFSolicitacaoMudanca_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] >= @AV35TFSolicitacaoMudanca_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] >= @AV35TFSolicitacaoMudanca_Data)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFSolicitacaoMudanca_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Data] <= @AV36TFSolicitacaoMudanca_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Data] <= @AV36TFSolicitacaoMudanca_Data_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV41TFSolicitacaoMudanca_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_SistemaCod] >= @AV41TFSolicitacaoMudanca_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_SistemaCod] >= @AV41TFSolicitacaoMudanca_SistemaCod)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV42TFSolicitacaoMudanca_SistemaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_SistemaCod] <= @AV42TFSolicitacaoMudanca_SistemaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_SistemaCod] <= @AV42TFSolicitacaoMudanca_SistemaCod_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV45TFSolicitacaoMudanca_Solicitante) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Solicitante] >= @AV45TFSolicitacaoMudanca_Solicitante)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Solicitante] >= @AV45TFSolicitacaoMudanca_Solicitante)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV46TFSolicitacaoMudanca_Solicitante_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SolicitacaoMudanca_Solicitante] <= @AV46TFSolicitacaoMudanca_Solicitante_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SolicitacaoMudanca_Solicitante] <= @AV46TFSolicitacaoMudanca_Solicitante_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GZ2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H00GZ3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GZ2 ;
          prmH00GZ2 = new Object[] {
          new Object[] {"@AV16SolicitacaoMudanca_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17SolicitacaoMudanca_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20SolicitacaoMudanca_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21SolicitacaoMudanca_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24SolicitacaoMudanca_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25SolicitacaoMudanca_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31TFSolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFSolicitacaoMudanca_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSolicitacaoMudanca_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36TFSolicitacaoMudanca_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41TFSolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFSolicitacaoMudanca_SistemaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFSolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFSolicitacaoMudanca_Solicitante_To",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GZ3 ;
          prmH00GZ3 = new Object[] {
          new Object[] {"@AV16SolicitacaoMudanca_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17SolicitacaoMudanca_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20SolicitacaoMudanca_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21SolicitacaoMudanca_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24SolicitacaoMudanca_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25SolicitacaoMudanca_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31TFSolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFSolicitacaoMudanca_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSolicitacaoMudanca_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36TFSolicitacaoMudanca_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41TFSolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFSolicitacaoMudanca_SistemaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFSolicitacaoMudanca_Solicitante",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFSolicitacaoMudanca_Solicitante_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GZ2,11,0,true,false )
             ,new CursorDef("H00GZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GZ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
       }
    }

 }

}
