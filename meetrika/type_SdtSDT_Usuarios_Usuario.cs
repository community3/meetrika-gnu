/*
               File: type_SdtSDT_Usuarios_Usuario
        Description: SDT_Usuarios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:6.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Usuarios.Usuario" )]
   [XmlType(TypeName =  "SDT_Usuarios.Usuario" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_Usuarios_Usuario : GxUserType
   {
      public SdtSDT_Usuarios_Usuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Usuarios_Usuario_Nome = "";
      }

      public SdtSDT_Usuarios_Usuario( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Usuarios_Usuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Usuarios_Usuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Usuarios_Usuario obj ;
         obj = this;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Nome = deserialized.gxTpr_Nome;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_Usuarios_Usuario_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Nome") )
               {
                  gxTv_SdtSDT_Usuarios_Usuario_Nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Usuarios.Usuario";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Usuarios_Usuario_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Nome", StringUtil.RTrim( gxTv_SdtSDT_Usuarios_Usuario_Nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Codigo", gxTv_SdtSDT_Usuarios_Usuario_Codigo, false);
         AddObjectProperty("Nome", gxTv_SdtSDT_Usuarios_Usuario_Nome, false);
         return  ;
      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_Usuarios_Usuario_Codigo ;
         }

         set {
            gxTv_SdtSDT_Usuarios_Usuario_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Nome" )]
      [  XmlElement( ElementName = "Nome"   )]
      public String gxTpr_Nome
      {
         get {
            return gxTv_SdtSDT_Usuarios_Usuario_Nome ;
         }

         set {
            gxTv_SdtSDT_Usuarios_Usuario_Nome = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Usuarios_Usuario_Nome = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Usuarios_Usuario_Codigo ;
      protected String gxTv_SdtSDT_Usuarios_Usuario_Nome ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_Usuarios.Usuario", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_Usuarios_Usuario_RESTInterface : GxGenericCollectionItem<SdtSDT_Usuarios_Usuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Usuarios_Usuario_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Usuarios_Usuario_RESTInterface( SdtSDT_Usuarios_Usuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Nome" , Order = 1 )]
      public String gxTpr_Nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Nome) ;
         }

         set {
            sdt.gxTpr_Nome = (String)(value);
         }

      }

      public SdtSDT_Usuarios_Usuario sdt
      {
         get {
            return (SdtSDT_Usuarios_Usuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Usuarios_Usuario() ;
         }
      }

   }

}
