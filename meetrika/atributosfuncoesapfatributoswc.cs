/*
               File: AtributosFuncoesAPFAtributosWC
        Description: Atributos Funcoes APFAtributos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:32.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atributosfuncoesapfatributoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public atributosfuncoesapfatributoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public atributosfuncoesapfatributoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPFAtributos_AtributosCod )
      {
         this.AV56FuncaoAPFAtributos_AtributosCod = aP0_FuncaoAPFAtributos_AtributosCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV56FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV56FuncaoAPFAtributos_AtributosCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_41 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_41_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_41_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV19FuncaoAPF_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV60TFFuncaoAPF_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
                  AV61TFFuncaoAPF_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFFuncaoAPF_Nome_Sel", AV61TFFuncaoAPF_Nome_Sel);
                  AV68TFFuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
                  AV69TFFuncaoAPF_PF_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
                  AV56FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace);
                  AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
                  AV70ddo_FuncaoAPF_PFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70ddo_FuncaoAPF_PFTitleControlIdToReplace", AV70ddo_FuncaoAPF_PFTitleControlIdToReplace);
                  AV80Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV65TFFuncaoAPF_Complexidade_Sels);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n360FuncaoAPF_SistemaCod = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "AtributosFuncoesAPFAtributosWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("atributosfuncoesapfatributoswc:[SendSecurityCheck value for]"+"FuncaoAPFAtributos_AtributosCod:"+context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA4G2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV80Pgmname = "AtributosFuncoesAPFAtributosWC";
               context.Gx_err = 0;
               WS4G2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Atributos Funcoes APFAtributos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117183321");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("atributosfuncoesapfatributoswc.aspx") + "?" + UrlEncode("" +AV56FuncaoAPFAtributos_AtributosCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME1", AV19FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPF_NOME", AV60TFFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPF_NOME_SEL", AV61TFFuncaoAPF_Nome_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( AV68TFFuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPF_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV69TFFuncaoAPF_PF_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_41", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_41), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPF_NOMETITLEFILTERDATA", AV59FuncaoAPF_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPF_NOMETITLEFILTERDATA", AV59FuncaoAPF_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV63FuncaoAPF_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV63FuncaoAPF_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPF_PFTITLEFILTERDATA", AV67FuncaoAPF_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPF_PFTITLEFILTERDATA", AV67FuncaoAPF_PFTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV56FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV80Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV65TFFuncaoAPF_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV65TFFuncaoAPF_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Caption", StringUtil.RTrim( Ddo_funcaoapf_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Cls", StringUtil.RTrim( Ddo_funcaoapf_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Caption", StringUtil.RTrim( Ddo_funcaoapf_pf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Cls", StringUtil.RTrim( Ddo_funcaoapf_pf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPF_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "AtributosFuncoesAPFAtributosWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("atributosfuncoesapfatributoswc:[SendSecurityCheck value for]"+"FuncaoAPFAtributos_AtributosCod:"+context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm4G2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("atributosfuncoesapfatributoswc.js", "?20203117183453");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AtributosFuncoesAPFAtributosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Atributos Funcoes APFAtributos WC" ;
      }

      protected void WB4G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "atributosfuncoesapfatributoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_4G2( true) ;
         }
         else
         {
            wb_table1_2_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_Internalname, AV60TFFuncaoAPF_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavTffuncaoapf_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_sel_Internalname, AV61TFFuncaoAPF_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavTffuncaoapf_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV68TFFuncaoAPF_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV68TFFuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV69TFFuncaoAPF_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV69TFFuncaoAPF_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPF_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPF_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
         }
         wbLoad = true;
      }

      protected void START4G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Atributos Funcoes APFAtributos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP4G0( ) ;
            }
         }
      }

      protected void WS4G2( )
      {
         START4G2( ) ;
         EVT4G2( ) ;
      }

      protected void EVT4G2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E114G2 */
                                    E114G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E124G2 */
                                    E124G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E134G2 */
                                    E134G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_PF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E144G2 */
                                    E144G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E154G2 */
                                    E154G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E164G2 */
                                    E164G2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4G0( ) ;
                              }
                              nGXsfl_41_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
                              SubsflControlProps_412( ) ;
                              AV33Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV78Update_GXI : context.convertURL( context.PathToRelativeUrl( AV33Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV79Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_SistemaCod_Internalname), ",", "."));
                              n360FuncaoAPF_SistemaCod = false;
                              A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                              cmbFuncaoAPF_Complexidade.Name = cmbFuncaoAPF_Complexidade_Internalname;
                              cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E174G2 */
                                          E174G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E184G2 */
                                          E184G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E194G2 */
                                          E194G2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV19FuncaoAPF_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapf_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_NOME"), AV60TFFuncaoAPF_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapf_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_NOME_SEL"), AV61TFFuncaoAPF_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapf_pf Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_PF"), ",", ".") != AV68TFFuncaoAPF_PF )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapf_pf_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_PF_TO"), ",", ".") != AV69TFFuncaoAPF_PF_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP4G0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE4G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm4G2( ) ;
            }
         }
      }

      protected void PA4G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_41_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_412( ) ;
         while ( nGXsfl_41_idx <= nRC_GXsfl_41 )
         {
            sendrow_412( ) ;
            nGXsfl_41_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_41_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_41_idx+1));
            sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
            SubsflControlProps_412( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV19FuncaoAPF_Nome1 ,
                                       bool AV15OrderedDsc ,
                                       String AV60TFFuncaoAPF_Nome ,
                                       String AV61TFFuncaoAPF_Nome_Sel ,
                                       decimal AV68TFFuncaoAPF_PF ,
                                       decimal AV69TFFuncaoAPF_PF_To ,
                                       int AV56FuncaoAPFAtributos_AtributosCod ,
                                       String AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace ,
                                       String AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ,
                                       String AV70ddo_FuncaoAPF_PFTitleControlIdToReplace ,
                                       String AV80Pgmname ,
                                       IGxCollection AV65TFFuncaoAPF_Complexidade_Sels ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       int A165FuncaoAPF_Codigo ,
                                       int A364FuncaoAPFAtributos_AtributosCod ,
                                       int A360FuncaoAPF_SistemaCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF4G2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV80Pgmname = "AtributosFuncoesAPFAtributosWC";
         context.Gx_err = 0;
      }

      protected void RF4G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 41;
         /* Execute user event: E184G2 */
         E184G2 ();
         nGXsfl_41_idx = 1;
         sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
         SubsflControlProps_412( ) ;
         nGXsfl_41_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_412( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A185FuncaoAPF_Complexidade ,
                                                 AV65TFFuncaoAPF_Complexidade_Sels ,
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV19FuncaoAPF_Nome1 ,
                                                 AV61TFFuncaoAPF_Nome_Sel ,
                                                 AV60TFFuncaoAPF_Nome ,
                                                 A166FuncaoAPF_Nome ,
                                                 AV15OrderedDsc ,
                                                 A364FuncaoAPFAtributos_AtributosCod ,
                                                 AV56FuncaoAPFAtributos_AtributosCod ,
                                                 AV65TFFuncaoAPF_Complexidade_Sels.Count ,
                                                 AV68TFFuncaoAPF_PF ,
                                                 A386FuncaoAPF_PF ,
                                                 AV69TFFuncaoAPF_PF_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            lV19FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV19FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            lV60TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV60TFFuncaoAPF_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
            /* Using cursor H004G2 */
            pr_default.execute(0, new Object[] {AV56FuncaoAPFAtributos_AtributosCod, AV65TFFuncaoAPF_Complexidade_Sels.Count, lV19FuncaoAPF_Nome1, lV19FuncaoAPF_Nome1, lV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel});
            nGXsfl_41_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A364FuncaoAPFAtributos_AtributosCod = H004G2_A364FuncaoAPFAtributos_AtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               A166FuncaoAPF_Nome = H004G2_A166FuncaoAPF_Nome[0];
               A360FuncaoAPF_SistemaCod = H004G2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H004G2_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = H004G2_A165FuncaoAPF_Codigo[0];
               A166FuncaoAPF_Nome = H004G2_A166FuncaoAPF_Nome[0];
               A360FuncaoAPF_SistemaCod = H004G2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H004G2_n360FuncaoAPF_SistemaCod[0];
               GXt_char1 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
               A185FuncaoAPF_Complexidade = GXt_char1;
               if ( ( AV65TFFuncaoAPF_Complexidade_Sels.Count <= 0 ) || ( (AV65TFFuncaoAPF_Complexidade_Sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
               {
                  GXt_decimal2 = A386FuncaoAPF_PF;
                  new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal2) ;
                  A386FuncaoAPF_PF = GXt_decimal2;
                  if ( (Convert.ToDecimal(0)==AV68TFFuncaoAPF_PF) || ( ( A386FuncaoAPF_PF >= AV68TFFuncaoAPF_PF ) ) )
                  {
                     if ( (Convert.ToDecimal(0)==AV69TFFuncaoAPF_PF_To) || ( ( A386FuncaoAPF_PF <= AV69TFFuncaoAPF_PF_To ) ) )
                     {
                        /* Execute user event: E194G2 */
                        E194G2 ();
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 41;
            WB4G0( ) ;
         }
         nGXsfl_41_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV19FuncaoAPF_Nome1, AV15OrderedDsc, AV60TFFuncaoAPF_Nome, AV61TFFuncaoAPF_Nome_Sel, AV68TFFuncaoAPF_PF, AV69TFFuncaoAPF_PF_To, AV56FuncaoAPFAtributos_AtributosCod, AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, AV80Pgmname, AV65TFFuncaoAPF_Complexidade_Sels, AV11GridState, A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, A360FuncaoAPF_SistemaCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP4G0( )
      {
         /* Before Start, stand alone formulas. */
         AV80Pgmname = "AtributosFuncoesAPFAtributosWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E174G2 */
         E174G2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV71DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPF_NOMETITLEFILTERDATA"), AV59FuncaoAPF_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA"), AV63FuncaoAPF_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPF_PFTITLEFILTERDATA"), AV67FuncaoAPF_PFTitleFilterData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV19FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV60TFFuncaoAPF_Nome = cgiGet( edtavTffuncaoapf_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
            AV61TFFuncaoAPF_Nome_Sel = cgiGet( edtavTffuncaoapf_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFFuncaoAPF_Nome_Sel", AV61TFFuncaoAPF_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF");
               GX_FocusControl = edtavTffuncaoapf_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFFuncaoAPF_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
            }
            else
            {
               AV68TFFuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF_TO");
               GX_FocusControl = edtavTffuncaoapf_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFFuncaoAPF_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
            }
            else
            {
               AV69TFFuncaoAPF_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
            }
            AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace);
            AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
            AV70ddo_FuncaoAPF_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70ddo_FuncaoAPF_PFTitleControlIdToReplace", AV70ddo_FuncaoAPF_PFTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_41 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_41"), ",", "."));
            AV73GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV74GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV56FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV56FuncaoAPFAtributos_AtributosCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapf_nome_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Caption");
            Ddo_funcaoapf_nome_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Tooltip");
            Ddo_funcaoapf_nome_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Cls");
            Ddo_funcaoapf_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Filteredtext_set");
            Ddo_funcaoapf_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Selectedvalue_set");
            Ddo_funcaoapf_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Dropdownoptionstype");
            Ddo_funcaoapf_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace");
            Ddo_funcaoapf_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Includesortasc"));
            Ddo_funcaoapf_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Includesortdsc"));
            Ddo_funcaoapf_nome_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Sortedstatus");
            Ddo_funcaoapf_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Includefilter"));
            Ddo_funcaoapf_nome_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Filtertype");
            Ddo_funcaoapf_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Filterisrange"));
            Ddo_funcaoapf_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Includedatalist"));
            Ddo_funcaoapf_nome_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Datalisttype");
            Ddo_funcaoapf_nome_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Datalistproc");
            Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_nome_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Sortasc");
            Ddo_funcaoapf_nome_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Sortdsc");
            Ddo_funcaoapf_nome_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Loadingdata");
            Ddo_funcaoapf_nome_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Cleanfilter");
            Ddo_funcaoapf_nome_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Noresultsfound");
            Ddo_funcaoapf_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Searchbuttontext");
            Ddo_funcaoapf_complexidade_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Caption");
            Ddo_funcaoapf_complexidade_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip");
            Ddo_funcaoapf_complexidade_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Cls");
            Ddo_funcaoapf_complexidade_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaoapf_complexidade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaoapf_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaoapf_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaoapf_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter"));
            Ddo_funcaoapf_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaoapf_complexidade_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype");
            Ddo_funcaoapf_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaoapf_complexidade_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaoapf_complexidade_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaoapf_complexidade_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaoapf_pf_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Caption");
            Ddo_funcaoapf_pf_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Tooltip");
            Ddo_funcaoapf_pf_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Cls");
            Ddo_funcaoapf_pf_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filteredtext_set");
            Ddo_funcaoapf_pf_Filteredtextto_set = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filteredtextto_set");
            Ddo_funcaoapf_pf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Dropdownoptionstype");
            Ddo_funcaoapf_pf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace");
            Ddo_funcaoapf_pf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Includesortasc"));
            Ddo_funcaoapf_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Includesortdsc"));
            Ddo_funcaoapf_pf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Includefilter"));
            Ddo_funcaoapf_pf_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filtertype");
            Ddo_funcaoapf_pf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filterisrange"));
            Ddo_funcaoapf_pf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Includedatalist"));
            Ddo_funcaoapf_pf_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Cleanfilter");
            Ddo_funcaoapf_pf_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Rangefilterfrom");
            Ddo_funcaoapf_pf_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Rangefilterto");
            Ddo_funcaoapf_pf_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapf_nome_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Activeeventkey");
            Ddo_funcaoapf_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Filteredtext_get");
            Ddo_funcaoapf_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPF_NOME_Selectedvalue_get");
            Ddo_funcaoapf_complexidade_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaoapf_complexidade_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaoapf_pf_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Activeeventkey");
            Ddo_funcaoapf_pf_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filteredtext_get");
            Ddo_funcaoapf_pf_Filteredtextto_get = cgiGet( sPrefix+"DDO_FUNCAOAPF_PF_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "AtributosFuncoesAPFAtributosWC";
            A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("atributosfuncoesapfatributoswc:[SecurityCheckFailed value for]"+"FuncaoAPFAtributos_AtributosCod:"+context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV19FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_NOME"), AV60TFFuncaoAPF_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_NOME_SEL"), AV61TFFuncaoAPF_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_PF"), ",", ".") != AV68TFFuncaoAPF_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFFUNCAOAPF_PF_TO"), ",", ".") != AV69TFFuncaoAPF_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E174G2 */
         E174G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E174G2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV16DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTffuncaoapf_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapf_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_Visible), 5, 0)));
         edtavTffuncaoapf_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapf_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_sel_Visible), 5, 0)));
         edtavTffuncaoapf_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapf_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_Visible), 5, 0)));
         edtavTffuncaoapf_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapf_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_to_Visible), 5, 0)));
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_nome_Titlecontrolidtoreplace);
         AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace = Ddo_funcaoapf_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace);
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace);
         AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_pf_Titlecontrolidtoreplace);
         AV70ddo_FuncaoAPF_PFTitleControlIdToReplace = Ddo_funcaoapf_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV70ddo_FuncaoAPF_PFTitleControlIdToReplace", AV70ddo_FuncaoAPF_PFTitleControlIdToReplace);
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible), 5, 0)));
         edtFuncaoAPFAtributos_AtributosCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = AV71DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3) ;
         AV71DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3;
      }

      protected void E184G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV59FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Complexidade_Titleformat = 2;
         cmbFuncaoAPF_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Title", cmbFuncaoAPF_Complexidade.Title.Text);
         edtFuncaoAPF_PF_Titleformat = 2;
         edtFuncaoAPF_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV70ddo_FuncaoAPF_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_PF_Internalname, "Title", edtFuncaoAPF_PF_Title);
         AV73GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV73GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73GridCurrentPage), 10, 0)));
         AV74GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV74GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV59FuncaoAPF_NomeTitleFilterData", AV59FuncaoAPF_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV63FuncaoAPF_ComplexidadeTitleFilterData", AV63FuncaoAPF_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV67FuncaoAPF_PFTitleFilterData", AV67FuncaoAPF_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E114G2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV72PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV72PageToGo) ;
         }
      }

      protected void E124G2( )
      {
         /* Ddo_funcaoapf_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFFuncaoAPF_Nome = Ddo_funcaoapf_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
            AV61TFFuncaoAPF_Nome_Sel = Ddo_funcaoapf_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFFuncaoAPF_Nome_Sel", AV61TFFuncaoAPF_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E134G2( )
      {
         /* Ddo_funcaoapf_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFFuncaoAPF_Complexidade_SelsJson = Ddo_funcaoapf_complexidade_Selectedvalue_get;
            AV65TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV64TFFuncaoAPF_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65TFFuncaoAPF_Complexidade_Sels", AV65TFFuncaoAPF_Complexidade_Sels);
      }

      protected void E144G2( )
      {
         /* Ddo_funcaoapf_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFFuncaoAPF_PF = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
            AV69TFFuncaoAPF_PF_To = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E194G2( )
      {
         /* Grid_Load Routine */
         AV33Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
         AV78Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV32Delete);
         AV79Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod);
         edtFuncaoAPF_Nome_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 41;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_412( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_41_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(41, GridRow);
         }
      }

      protected void E164G2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E154G2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV65TFFuncaoAPF_Complexidade_Sels", AV65TFFuncaoAPF_Complexidade_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapf_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S152( )
      {
         /* 'CLEANFILTERS' Routine */
         AV60TFFuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
         AV61TFFuncaoAPF_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFFuncaoAPF_Nome_Sel", AV61TFFuncaoAPF_Nome_Sel);
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
         AV65TFFuncaoAPF_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_complexidade_Internalname, "SelectedValue_set", Ddo_funcaoapf_complexidade_Selectedvalue_set);
         AV68TFFuncaoAPF_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_pf_Internalname, "FilteredText_set", Ddo_funcaoapf_pf_Filteredtext_set);
         AV69TFFuncaoAPF_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_pf_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_pf_Filteredtextto_set);
         AV16DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV19FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get(AV80Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV80Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV34Session.Get(AV80Pgmname+"GridState"), "");
         }
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV81GXV1 = 1;
         while ( AV81GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV81GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV60TFFuncaoAPF_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFFuncaoAPF_Nome", AV60TFFuncaoAPF_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPF_Nome)) )
               {
                  Ddo_funcaoapf_nome_Filteredtext_set = AV60TFFuncaoAPF_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV61TFFuncaoAPF_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61TFFuncaoAPF_Nome_Sel", AV61TFFuncaoAPF_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFFuncaoAPF_Nome_Sel)) )
               {
                  Ddo_funcaoapf_nome_Selectedvalue_set = AV61TFFuncaoAPF_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_COMPLEXIDADE_SEL") == 0 )
            {
               AV64TFFuncaoAPF_Complexidade_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV65TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV64TFFuncaoAPF_Complexidade_SelsJson);
               if ( ! ( AV65TFFuncaoAPF_Complexidade_Sels.Count == 0 ) )
               {
                  Ddo_funcaoapf_complexidade_Selectedvalue_set = AV64TFFuncaoAPF_Complexidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_complexidade_Internalname, "SelectedValue_set", Ddo_funcaoapf_complexidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_PF") == 0 )
            {
               AV68TFFuncaoAPF_PF = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5)));
               AV69TFFuncaoAPF_PF_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV68TFFuncaoAPF_PF) )
               {
                  Ddo_funcaoapf_pf_Filteredtext_set = StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_pf_Internalname, "FilteredText_set", Ddo_funcaoapf_pf_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV69TFFuncaoAPF_PF_To) )
               {
                  Ddo_funcaoapf_pf_Filteredtextto_set = StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapf_pf_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_pf_Filteredtextto_set);
               }
            }
            AV81GXV1 = (int)(AV81GXV1+1);
         }
      }

      protected void S182( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19FuncaoAPF_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Nome1", AV19FuncaoAPF_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV34Session.Get(AV80Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPF_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV60TFFuncaoAPF_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFFuncaoAPF_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV61TFFuncaoAPF_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV65TFFuncaoAPF_Complexidade_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_COMPLEXIDADE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV65TFFuncaoAPF_Complexidade_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV68TFFuncaoAPF_PF) && (Convert.ToDecimal(0)==AV69TFFuncaoAPF_PF_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_PF";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV68TFFuncaoAPF_PF, 14, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV69TFFuncaoAPF_PF_To, 14, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV56FuncaoAPFAtributos_AtributosCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV80Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) )
         {
            AV13GridStateDynamicFilter.gxTpr_Value = AV19FuncaoAPF_Nome1;
            AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV80Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncoesAPFAtributos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoAPFAtributos_AtributosCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV34Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S172( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_4G2( true) ;
         }
         else
         {
            wb_table2_8_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_38_4G2( true) ;
         }
         else
         {
            wb_table3_38_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_38_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4G2e( true) ;
         }
         else
         {
            wb_table1_2_4G2e( false) ;
         }
      }

      protected void wb_table3_38_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"41\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoAPF_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 41 )
         {
            wbEnd = 0;
            nRC_GXsfl_41 = (short)(nGXsfl_41_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_38_4G2e( true) ;
         }
         else
         {
            wb_table3_38_4G2e( false) ;
         }
      }

      protected void wb_table2_8_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_4G2( true) ;
         }
         else
         {
            wb_table4_11_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_15_4G2( true) ;
         }
         else
         {
            wb_table5_15_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_15_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4G2e( true) ;
         }
         else
         {
            wb_table2_8_4G2e( false) ;
         }
      }

      protected void wb_table5_15_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_20_4G2( true) ;
         }
         else
         {
            wb_table6_20_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_20_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_15_4G2e( true) ;
         }
         else
         {
            wb_table5_15_4G2e( false) ;
         }
      }

      protected void wb_table6_20_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_29_4G2( true) ;
         }
         else
         {
            wb_table7_29_4G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_29_4G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_20_4G2e( true) ;
         }
         else
         {
            wb_table6_20_4G2e( false) ;
         }
      }

      protected void wb_table7_29_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_AtributosFuncoesAPFAtributosWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_41_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV19FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_AtributosFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_29_4G2e( true) ;
         }
         else
         {
            wb_table7_29_4G2e( false) ;
         }
      }

      protected void wb_table4_11_4G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_4G2e( true) ;
         }
         else
         {
            wb_table4_11_4G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV56FuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4G2( ) ;
         WS4G2( ) ;
         WE4G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV56FuncaoAPFAtributos_AtributosCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA4G2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "atributosfuncoesapfatributoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA4G2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV56FuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         wcpOAV56FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV56FuncaoAPFAtributos_AtributosCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV56FuncaoAPFAtributos_AtributosCod != wcpOAV56FuncaoAPFAtributos_AtributosCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV56FuncaoAPFAtributos_AtributosCod = AV56FuncaoAPFAtributos_AtributosCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV56FuncaoAPFAtributos_AtributosCod = cgiGet( sPrefix+"AV56FuncaoAPFAtributos_AtributosCod_CTRL");
         if ( StringUtil.Len( sCtrlAV56FuncaoAPFAtributos_AtributosCod) > 0 )
         {
            AV56FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV56FuncaoAPFAtributos_AtributosCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         else
         {
            AV56FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV56FuncaoAPFAtributos_AtributosCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA4G2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS4G2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS4G2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV56FuncaoAPFAtributos_AtributosCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV56FuncaoAPFAtributos_AtributosCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV56FuncaoAPFAtributos_AtributosCod_CTRL", StringUtil.RTrim( sCtrlAV56FuncaoAPFAtributos_AtributosCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE4G2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117183866");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("atributosfuncoesapfatributoswc.js", "?20203117183866");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_412( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_41_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_41_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_41_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_41_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_41_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_41_idx;
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF_"+sGXsfl_41_idx;
      }

      protected void SubsflControlProps_fel_412( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_41_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_41_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_41_fel_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_41_fel_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_41_fel_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_41_fel_idx;
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF_"+sGXsfl_41_fel_idx;
      }

      protected void sendrow_412( )
      {
         SubsflControlProps_412( ) ;
         WB4G0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_41_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_41_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_41_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV78Update_GXI : context.PathToRelativeUrl( AV33Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV79Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV79Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)41,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)41,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtFuncaoAPF_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)41,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_41_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Complexidade,(String)cmbFuncaoAPF_Complexidade_Internalname,StringUtil.RTrim( A185FuncaoAPF_Complexidade),(short)1,(String)cmbFuncaoAPF_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")),context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)41,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_41_idx, GetSecureSignedToken( sPrefix+sGXsfl_41_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE"+"_"+sGXsfl_41_idx, GetSecureSignedToken( sPrefix+sGXsfl_41_idx, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_PF"+"_"+sGXsfl_41_idx, GetSecureSignedToken( sPrefix+sGXsfl_41_idx, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_41_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_41_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_41_idx+1));
            sGXsfl_41_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_41_idx), 4, 0)), 4, "0");
            SubsflControlProps_412( ) ;
         }
         /* End function sendrow_412 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavFuncaoapf_nome1_Internalname = sPrefix+"vFUNCAOAPF_NOME1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE";
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTffuncaoapf_nome_Internalname = sPrefix+"vTFFUNCAOAPF_NOME";
         edtavTffuncaoapf_nome_sel_Internalname = sPrefix+"vTFFUNCAOAPF_NOME_SEL";
         edtavTffuncaoapf_pf_Internalname = sPrefix+"vTFFUNCAOAPF_PF";
         edtavTffuncaoapf_pf_to_Internalname = sPrefix+"vTFFUNCAOAPF_PF_TO";
         Ddo_funcaoapf_nome_Internalname = sPrefix+"DDO_FUNCAOAPF_NOME";
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_complexidade_Internalname = sPrefix+"DDO_FUNCAOAPF_COMPLEXIDADE";
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_pf_Internalname = sPrefix+"DDO_FUNCAOAPF_PF";
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoAPF_PF_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtFuncaoAPF_SistemaCod_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoAPF_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtFuncaoAPF_PF_Titleformat = 0;
         cmbFuncaoAPF_Complexidade_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         edtFuncaoAPF_PF_Title = "PF";
         cmbFuncaoAPF_Complexidade.Title.Text = "Complexidade";
         edtFuncaoAPF_Nome_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapf_pf_to_Jsonclick = "";
         edtavTffuncaoapf_pf_to_Visible = 1;
         edtavTffuncaoapf_pf_Jsonclick = "";
         edtavTffuncaoapf_pf_Visible = 1;
         edtavTffuncaoapf_nome_sel_Visible = 1;
         edtavTffuncaoapf_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Visible = 1;
         Ddo_funcaoapf_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_pf_Rangefilterto = "At�";
         Ddo_funcaoapf_pf_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Filtertype = "Numeric";
         Ddo_funcaoapf_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_pf_Cls = "ColumnSettings";
         Ddo_funcaoapf_pf_Tooltip = "Op��es";
         Ddo_funcaoapf_pf_Caption = "";
         Ddo_funcaoapf_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaoapf_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaoapf_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_complexidade_Cls = "ColumnSettings";
         Ddo_funcaoapf_complexidade_Tooltip = "Op��es";
         Ddo_funcaoapf_complexidade_Caption = "";
         Ddo_funcaoapf_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_nome_Datalistproc = "GetAtributosFuncoesAPFAtributosWCFilterData";
         Ddo_funcaoapf_nome_Datalisttype = "Dynamic";
         Ddo_funcaoapf_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_nome_Filtertype = "Character";
         Ddo_funcaoapf_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_nome_Cls = "ColumnSettings";
         Ddo_funcaoapf_nome_Tooltip = "Op��es";
         Ddo_funcaoapf_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0}],oparms:[{av:'AV59FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV63FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV67FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'AV73GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV74GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E114G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED","{handler:'E124G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapf_nome_Activeeventkey',ctrl:'DDO_FUNCAOAPF_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_nome_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E134G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapf_complexidade_Activeeventkey',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAOAPF_PF.ONOPTIONCLICKED","{handler:'E144G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapf_pf_Activeeventkey',ctrl:'DDO_FUNCAOAPF_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_pf_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_pf_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E194G2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV33Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtFuncaoAPF_Nome_Link',ctrl:'FUNCAOAPF_NOME',prop:'Link'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E164G2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E154G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV56FuncaoAPFAtributos_AtributosCod',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV60TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_set'},{av:'AV61TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_set'},{av:'AV65TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV68TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_set'},{av:'AV69TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapf_nome_Activeeventkey = "";
         Ddo_funcaoapf_nome_Filteredtext_get = "";
         Ddo_funcaoapf_nome_Selectedvalue_get = "";
         Ddo_funcaoapf_complexidade_Activeeventkey = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_get = "";
         Ddo_funcaoapf_pf_Activeeventkey = "";
         Ddo_funcaoapf_pf_Filteredtext_get = "";
         Ddo_funcaoapf_pf_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV19FuncaoAPF_Nome1 = "";
         AV60TFFuncaoAPF_Nome = "";
         AV61TFFuncaoAPF_Nome_Sel = "";
         AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace = "";
         AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = "";
         AV70ddo_FuncaoAPF_PFTitleControlIdToReplace = "";
         AV80Pgmname = "";
         AV65TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV71DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV59FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         Ddo_funcaoapf_nome_Sortedstatus = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Update = "";
         AV78Update_GXI = "";
         AV32Delete = "";
         AV79Delete_GXI = "";
         A166FuncaoAPF_Nome = "";
         A185FuncaoAPF_Complexidade = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19FuncaoAPF_Nome1 = "";
         lV60TFFuncaoAPF_Nome = "";
         H004G2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H004G2_A166FuncaoAPF_Nome = new String[] {""} ;
         H004G2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H004G2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H004G2_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV64TFFuncaoAPF_Complexidade_SelsJson = "";
         GridRow = new GXWebRow();
         AV34Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV56FuncaoAPFAtributos_AtributosCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atributosfuncoesapfatributoswc__default(),
            new Object[][] {
                new Object[] {
               H004G2_A364FuncaoAPFAtributos_AtributosCod, H004G2_A166FuncaoAPF_Nome, H004G2_A360FuncaoAPF_SistemaCod, H004G2_n360FuncaoAPF_SistemaCod, H004G2_A165FuncaoAPF_Codigo
               }
            }
         );
         AV80Pgmname = "AtributosFuncoesAPFAtributosWC";
         /* GeneXus formulas. */
         AV80Pgmname = "AtributosFuncoesAPFAtributosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_41 ;
      private short nGXsfl_41_idx=1 ;
      private short AV17DynamicFiltersOperator1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_41_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Complexidade_Titleformat ;
      private short edtFuncaoAPF_PF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV56FuncaoAPFAtributos_AtributosCod ;
      private int wcpOAV56FuncaoAPFAtributos_AtributosCod ;
      private int subGrid_Rows ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapf_nome_Datalistupdateminimumcharacters ;
      private int edtFuncaoAPFAtributos_AtributosCod_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTffuncaoapf_nome_Visible ;
      private int edtavTffuncaoapf_nome_sel_Visible ;
      private int edtavTffuncaoapf_pf_Visible ;
      private int edtavTffuncaoapf_pf_to_Visible ;
      private int edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int AV65TFFuncaoAPF_Complexidade_Sels_Count ;
      private int AV72PageToGo ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int AV81GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV73GridCurrentPage ;
      private long AV74GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV68TFFuncaoAPF_PF ;
      private decimal AV69TFFuncaoAPF_PF_To ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal2 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapf_nome_Activeeventkey ;
      private String Ddo_funcaoapf_nome_Filteredtext_get ;
      private String Ddo_funcaoapf_nome_Selectedvalue_get ;
      private String Ddo_funcaoapf_complexidade_Activeeventkey ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_get ;
      private String Ddo_funcaoapf_pf_Activeeventkey ;
      private String Ddo_funcaoapf_pf_Filteredtext_get ;
      private String Ddo_funcaoapf_pf_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_41_idx="0001" ;
      private String AV80Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapf_nome_Caption ;
      private String Ddo_funcaoapf_nome_Tooltip ;
      private String Ddo_funcaoapf_nome_Cls ;
      private String Ddo_funcaoapf_nome_Filteredtext_set ;
      private String Ddo_funcaoapf_nome_Selectedvalue_set ;
      private String Ddo_funcaoapf_nome_Dropdownoptionstype ;
      private String Ddo_funcaoapf_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_nome_Sortedstatus ;
      private String Ddo_funcaoapf_nome_Filtertype ;
      private String Ddo_funcaoapf_nome_Datalisttype ;
      private String Ddo_funcaoapf_nome_Datalistproc ;
      private String Ddo_funcaoapf_nome_Sortasc ;
      private String Ddo_funcaoapf_nome_Sortdsc ;
      private String Ddo_funcaoapf_nome_Loadingdata ;
      private String Ddo_funcaoapf_nome_Cleanfilter ;
      private String Ddo_funcaoapf_nome_Noresultsfound ;
      private String Ddo_funcaoapf_nome_Searchbuttontext ;
      private String Ddo_funcaoapf_complexidade_Caption ;
      private String Ddo_funcaoapf_complexidade_Tooltip ;
      private String Ddo_funcaoapf_complexidade_Cls ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_set ;
      private String Ddo_funcaoapf_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_complexidade_Datalisttype ;
      private String Ddo_funcaoapf_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaoapf_complexidade_Cleanfilter ;
      private String Ddo_funcaoapf_complexidade_Searchbuttontext ;
      private String Ddo_funcaoapf_pf_Caption ;
      private String Ddo_funcaoapf_pf_Tooltip ;
      private String Ddo_funcaoapf_pf_Cls ;
      private String Ddo_funcaoapf_pf_Filteredtext_set ;
      private String Ddo_funcaoapf_pf_Filteredtextto_set ;
      private String Ddo_funcaoapf_pf_Dropdownoptionstype ;
      private String Ddo_funcaoapf_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_pf_Filtertype ;
      private String Ddo_funcaoapf_pf_Cleanfilter ;
      private String Ddo_funcaoapf_pf_Rangefilterfrom ;
      private String Ddo_funcaoapf_pf_Rangefilterto ;
      private String Ddo_funcaoapf_pf_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTffuncaoapf_nome_Internalname ;
      private String edtavTffuncaoapf_nome_sel_Internalname ;
      private String edtavTffuncaoapf_pf_Internalname ;
      private String edtavTffuncaoapf_pf_Jsonclick ;
      private String edtavTffuncaoapf_pf_to_Internalname ;
      private String edtavTffuncaoapf_pf_to_Jsonclick ;
      private String edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_SistemaCod_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapf_nome_Internalname ;
      private String Ddo_funcaoapf_complexidade_Internalname ;
      private String Ddo_funcaoapf_pf_Internalname ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtFuncaoAPF_PF_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFuncaoAPF_Nome_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV56FuncaoAPFAtributos_AtributosCod ;
      private String sGXsfl_41_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_SistemaCod_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapf_nome_Includesortasc ;
      private bool Ddo_funcaoapf_nome_Includesortdsc ;
      private bool Ddo_funcaoapf_nome_Includefilter ;
      private bool Ddo_funcaoapf_nome_Filterisrange ;
      private bool Ddo_funcaoapf_nome_Includedatalist ;
      private bool Ddo_funcaoapf_complexidade_Includesortasc ;
      private bool Ddo_funcaoapf_complexidade_Includesortdsc ;
      private bool Ddo_funcaoapf_complexidade_Includefilter ;
      private bool Ddo_funcaoapf_complexidade_Includedatalist ;
      private bool Ddo_funcaoapf_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaoapf_pf_Includesortasc ;
      private bool Ddo_funcaoapf_pf_Includesortdsc ;
      private bool Ddo_funcaoapf_pf_Includefilter ;
      private bool Ddo_funcaoapf_pf_Filterisrange ;
      private bool Ddo_funcaoapf_pf_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV33Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String AV64TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19FuncaoAPF_Nome1 ;
      private String AV60TFFuncaoAPF_Nome ;
      private String AV61TFFuncaoAPF_Nome_Sel ;
      private String AV62ddo_FuncaoAPF_NomeTitleControlIdToReplace ;
      private String AV66ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ;
      private String AV70ddo_FuncaoAPF_PFTitleControlIdToReplace ;
      private String AV78Update_GXI ;
      private String AV79Delete_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String lV19FuncaoAPF_Nome1 ;
      private String lV60TFFuncaoAPF_Nome ;
      private String AV33Update ;
      private String AV32Delete ;
      private IGxSession AV34Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private IDataStoreProvider pr_default ;
      private int[] H004G2_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] H004G2_A166FuncaoAPF_Nome ;
      private int[] H004G2_A360FuncaoAPF_SistemaCod ;
      private bool[] H004G2_n360FuncaoAPF_SistemaCod ;
      private int[] H004G2_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV65TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59FuncaoAPF_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63FuncaoAPF_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67FuncaoAPF_PFTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV71DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 ;
   }

   public class atributosfuncoesapfatributoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H004G2( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV65TFFuncaoAPF_Complexidade_Sels ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV19FuncaoAPF_Nome1 ,
                                             String AV61TFFuncaoAPF_Nome_Sel ,
                                             String AV60TFFuncaoAPF_Nome ,
                                             String A166FuncaoAPF_Nome ,
                                             bool AV15OrderedDsc ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int AV56FuncaoAPFAtributos_AtributosCod ,
                                             int AV65TFFuncaoAPF_Complexidade_Sels_Count ,
                                             decimal AV68TFFuncaoAPF_PF ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV69TFFuncaoAPF_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFAtributos_AtributosCod], T2.[FuncaoAPF_Nome], T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPFAtributos_AtributosCod] = @AV56FuncaoAPFAtributos_AtributosCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV19FuncaoAPF_Nome1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPF_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV60TFFuncaoAPF_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFFuncaoAPF_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV61TFFuncaoAPF_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod], T2.[FuncaoAPF_Nome]";
         }
         else if ( AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod] DESC, T2.[FuncaoAPF_Nome] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H004G2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004G2 ;
          prmH004G2 = new Object[] {
          new Object[] {"@AV56FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV19FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV61TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004G2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
