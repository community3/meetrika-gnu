/*
               File: WP_AssociationUsuarioServicos
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:46:14.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associationusuarioservicos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associationusuarioservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associationusuarioservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UsuarioServicos_UsuarioCod ,
                           ref int aP1_Contratada_Codigo )
      {
         this.AV7UsuarioServicos_UsuarioCod = aP0_UsuarioServicos_UsuarioCod;
         this.A39Contratada_Codigo = aP1_Contratada_Codigo;
         executePrivate();
         aP1_Contratada_Codigo=this.A39Contratada_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIOSERVICOS_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_UsuarioCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAER2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavUsuario_pessoanom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
               WSER2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEER2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120461412");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associationusuarioservicos.aspx") + "?" + UrlEncode("" +AV7UsuarioServicos_UsuarioCod) + "," + UrlEncode("" +A39Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV31jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV31jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV29jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV29jqAssociados);
         }
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV34MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV34MenuPerfil);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV28All);
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIOSERVICOS_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIOSERVICOS_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormER2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociationUsuarioServicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WBER0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_ER2( true) ;
         }
         else
         {
            wb_table1_2_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTER2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPER0( ) ;
      }

      protected void WSER2( )
      {
         STARTER2( ) ;
         EVTER2( ) ;
      }

      protected void EVTER2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11ER2 */
                           E11ER2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E12ER2 */
                                 E12ER2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13ER2 */
                           E13ER2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14ER2 */
                           E14ER2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15ER2 */
                           E15ER2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16ER2 */
                           E16ER2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17ER2 */
                           E17ER2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEER2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormER2( ) ;
            }
         }
      }

      protected void PAER2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsuario_pessoanom_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFER2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
      }

      protected void RFER2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E17ER2 */
            E17ER2 ();
            WBER0( ) ;
         }
      }

      protected void STRUPER0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11ER2 */
         E11ER2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV31jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV29jqAssociados);
            /* Read variables values. */
            AV27Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Usuario_PessoaNom", AV27Usuario_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!"))));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11ER2 */
         E11ER2 ();
         if (returnInSub) return;
      }

      protected void E11ER2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV38GXLvl6 = 0;
            /* Using cursor H00ER2 */
            pr_default.execute(0, new Object[] {AV7UsuarioServicos_UsuarioCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A57Usuario_PessoaCod = H00ER2_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = H00ER2_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H00ER2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00ER2_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = H00ER2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00ER2_n58Usuario_PessoaNom[0];
               AV38GXLvl6 = 1;
               AV27Usuario_PessoaNom = A58Usuario_PessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Usuario_PessoaNom", AV27Usuario_PessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV38GXLvl6 == 0 )
            {
               GX_msglist.addItem("Usu�rio n�o encontrado.");
            }
            /* Using cursor H00ER3 */
            pr_default.execute(1, new Object[] {A39Contratada_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = H00ER3_A74Contrato_Codigo[0];
               A632Servico_Ativo = H00ER3_A632Servico_Ativo[0];
               A92Contrato_Ativo = H00ER3_A92Contrato_Ativo[0];
               A43Contratada_Ativo = H00ER3_A43Contratada_Ativo[0];
               A155Servico_Codigo = H00ER3_A155Servico_Codigo[0];
               A92Contrato_Ativo = H00ER3_A92Contrato_Ativo[0];
               A632Servico_Ativo = H00ER3_A632Servico_Ativo[0];
               A43Contratada_Ativo = H00ER3_A43Contratada_Ativo[0];
               AV35Codigos.Add(A155Servico_Codigo, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 A155Servico_Codigo ,
                                                 AV35Codigos ,
                                                 A1530Servico_TipoHierarquia },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00ER4 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1530Servico_TipoHierarquia = H00ER4_A1530Servico_TipoHierarquia[0];
               n1530Servico_TipoHierarquia = H00ER4_n1530Servico_TipoHierarquia[0];
               A155Servico_Codigo = H00ER4_A155Servico_Codigo[0];
               A605Servico_Sigla = H00ER4_A605Servico_Sigla[0];
               AV8UsuarioServicos_ServicoCod = A155Servico_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_ServicoCod), 6, 0)));
               AV10Exist = false;
               if ( AV6WWPContext.gxTpr_Userehcontratante )
               {
                  /* Using cursor H00ER5 */
                  pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A29Contratante_Codigo = H00ER5_A29Contratante_Codigo[0];
                     n29Contratante_Codigo = H00ER5_n29Contratante_Codigo[0];
                     A5AreaTrabalho_Codigo = H00ER5_A5AreaTrabalho_Codigo[0];
                     /* Using cursor H00ER6 */
                     pr_default.execute(4, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, AV7UsuarioServicos_UsuarioCod});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A63ContratanteUsuario_ContratanteCod = H00ER6_A63ContratanteUsuario_ContratanteCod[0];
                        A60ContratanteUsuario_UsuarioCod = H00ER6_A60ContratanteUsuario_UsuarioCod[0];
                        /* Using cursor H00ER7 */
                        pr_default.execute(5, new Object[] {A60ContratanteUsuario_UsuarioCod, AV8UsuarioServicos_ServicoCod});
                        while ( (pr_default.getStatus(5) != 101) )
                        {
                           A828UsuarioServicos_UsuarioCod = H00ER7_A828UsuarioServicos_UsuarioCod[0];
                           A829UsuarioServicos_ServicoCod = H00ER7_A829UsuarioServicos_ServicoCod[0];
                           AV10Exist = true;
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        pr_default.close(5);
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(4);
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(3);
               }
               else
               {
                  /* Using cursor H00ER8 */
                  pr_default.execute(6, new Object[] {AV7UsuarioServicos_UsuarioCod, AV6WWPContext.gxTpr_Areatrabalho_codigo});
                  while ( (pr_default.getStatus(6) != 101) )
                  {
                     A66ContratadaUsuario_ContratadaCod = H00ER8_A66ContratadaUsuario_ContratadaCod[0];
                     A69ContratadaUsuario_UsuarioCod = H00ER8_A69ContratadaUsuario_UsuarioCod[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H00ER8_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H00ER8_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H00ER8_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H00ER8_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     /* Using cursor H00ER9 */
                     pr_default.execute(7, new Object[] {A69ContratadaUsuario_UsuarioCod, AV8UsuarioServicos_ServicoCod});
                     while ( (pr_default.getStatus(7) != 101) )
                     {
                        A828UsuarioServicos_UsuarioCod = H00ER9_A828UsuarioServicos_UsuarioCod[0];
                        A829UsuarioServicos_ServicoCod = H00ER9_A829UsuarioServicos_ServicoCod[0];
                        AV10Exist = true;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(7);
                     pr_default.readNext(6);
                  }
                  pr_default.close(6);
               }
               if ( AV10Exist )
               {
                  AV30jqItem = new SdtjqSelectData_Item(context);
                  AV30jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
                  AV30jqItem.gxTpr_Descr = A605Servico_Sigla;
                  AV30jqItem.gxTpr_Selected = false;
                  AV29jqAssociados.Add(AV30jqItem, 0);
               }
               else
               {
                  AV30jqItem = new SdtjqSelectData_Item(context);
                  AV30jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
                  AV30jqItem.gxTpr_Descr = A605Servico_Sigla;
                  AV30jqItem.gxTpr_Selected = false;
                  AV31jqNaoAssociados.Add(AV30jqItem, 0);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         /* Execute user subroutine: 'TITLES' */
         S112 ();
         if (returnInSub) return;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
      }

      public void GXEnter( )
      {
         /* Execute user event: E12ER2 */
         E12ER2 ();
         if (returnInSub) return;
      }

      protected void E12ER2( )
      {
         /* Enter Routine */
         AV12Success = true;
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV29jqAssociados.Count )
         {
            AV30jqItem = ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV46GXV1));
            if ( AV12Success )
            {
               AV8UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( AV30jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_ServicoCod), 6, 0)));
               AV47GXLvl83 = 0;
               /* Using cursor H00ER10 */
               pr_default.execute(8, new Object[] {AV7UsuarioServicos_UsuarioCod, AV8UsuarioServicos_ServicoCod});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A829UsuarioServicos_ServicoCod = H00ER10_A829UsuarioServicos_ServicoCod[0];
                  A828UsuarioServicos_UsuarioCod = H00ER10_A828UsuarioServicos_UsuarioCod[0];
                  AV47GXLvl83 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(8);
               if ( AV47GXLvl83 == 0 )
               {
                  AV11UsuarioServicos = new SdtUsuarioServicos(context);
                  AV11UsuarioServicos.gxTpr_Usuarioservicos_usuariocod = AV7UsuarioServicos_UsuarioCod;
                  AV11UsuarioServicos.gxTpr_Usuarioservicos_servicocod = AV8UsuarioServicos_ServicoCod;
                  AV11UsuarioServicos.Save();
                  if ( ! AV11UsuarioServicos.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         AV48GXV2 = 1;
         while ( AV48GXV2 <= AV31jqNaoAssociados.Count )
         {
            AV30jqItem = ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV48GXV2));
            if ( AV12Success )
            {
               AV8UsuarioServicos_ServicoCod = (int)(NumberUtil.Val( AV30jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UsuarioServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8UsuarioServicos_ServicoCod), 6, 0)));
               /* Using cursor H00ER11 */
               pr_default.execute(9, new Object[] {AV7UsuarioServicos_UsuarioCod, AV8UsuarioServicos_ServicoCod});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A829UsuarioServicos_ServicoCod = H00ER11_A829UsuarioServicos_ServicoCod[0];
                  A828UsuarioServicos_UsuarioCod = H00ER11_A828UsuarioServicos_UsuarioCod[0];
                  AV11UsuarioServicos = new SdtUsuarioServicos(context);
                  AV11UsuarioServicos.Load(AV7UsuarioServicos_UsuarioCod, AV8UsuarioServicos_ServicoCod);
                  if ( AV11UsuarioServicos.Success() )
                  {
                     AV11UsuarioServicos.Delete();
                  }
                  if ( ! AV11UsuarioServicos.Success() )
                  {
                     AV12Success = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(9);
            }
            AV48GXV2 = (int)(AV48GXV2+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociationUsuarioServicos");
            context.setWebReturnParms(new Object[] {(int)A39Contratada_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociationUsuarioServicos");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S122 ();
            if (returnInSub) return;
         }
      }

      protected void E13ER2( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
      }

      protected void E14ER2( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
      }

      protected void E15ER2( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
      }

      protected void E16ER2( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV28All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28All", AV28All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29jqAssociados", AV29jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31jqNaoAssociados", AV31jqNaoAssociados);
      }

      protected void S122( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV51GXV4 = 1;
         AV50GXV3 = AV34MenuPerfil.GetMessages();
         while ( AV51GXV4 <= AV50GXV3.Count )
         {
            AV15Message = ((SdtMessages_Message)AV50GXV3.Item(AV51GXV4));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV51GXV4 = (int)(AV51GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV31jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)).gxTpr_Selected || AV28All )
            {
               ((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV29jqAssociados.Add(((SdtjqSelectData_Item)AV31jqNaoAssociados.Item(AV14i)), 0);
               AV31jqNaoAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV29jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)).gxTpr_Selected || AV28All )
            {
               ((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV31jqNaoAssociados.Add(((SdtjqSelectData_Item)AV29jqAssociados.Item(AV14i)), 0);
               AV29jqAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV31jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV29jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E17ER2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_ER2( true) ;
         }
         else
         {
            wb_table2_8_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_ER2( true) ;
         }
         else
         {
            wb_table3_16_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_48_ER2( true) ;
         }
         else
         {
            wb_table4_48_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_ER2e( true) ;
         }
         else
         {
            wb_table1_2_ER2e( false) ;
         }
      }

      protected void wb_table4_48_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_ER2e( true) ;
         }
         else
         {
            wb_table4_48_ER2e( false) ;
         }
      }

      protected void wb_table3_16_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_ER2( true) ;
         }
         else
         {
            wb_table5_19_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_ER2e( true) ;
         }
         else
         {
            wb_table3_16_ER2e( false) ;
         }
      }

      protected void wb_table5_19_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_31_ER2( true) ;
         }
         else
         {
            wb_table6_31_ER2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_ER2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_ER2e( true) ;
         }
         else
         {
            wb_table5_19_ER2e( false) ;
         }
      }

      protected void wb_table6_31_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_ER2e( true) ;
         }
         else
         {
            wb_table6_31_ER2e( false) ;
         }
      }

      protected void wb_table2_8_ER2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Servi�os ao Usu�rio :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom_Internalname, StringUtil.RTrim( AV27Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV27Usuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavUsuario_pessoanom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociationUsuarioServicos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_ER2e( true) ;
         }
         else
         {
            wb_table2_8_ER2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7UsuarioServicos_UsuarioCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIOSERVICOS_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UsuarioServicos_UsuarioCod), "ZZZZZ9")));
         A39Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAER2( ) ;
         WSER2( ) ;
         WEER2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120461493");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associationusuarioservicos.js", "?20203120461493");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavUsuario_pessoanom_Jsonclick = "";
         edtavUsuario_pessoanom_Enabled = 1;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         lblAssociatedrecordstitle_Caption = "Associados";
         lblNotassociatedrecordstitle_Caption = "N�o Associados";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 300;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12ER2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null}],oparms:[{av:'AV8UsuarioServicos_ServicoCod',fld:'vUSUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E13ER2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E14ER2',iparms:[{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E15ER2',iparms:[{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E16ER2',iparms:[{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV28All',fld:'vALL',pic:'',nv:false},{av:'AV29jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV31jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV29jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_Meetrika", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV34MenuPerfil = new SdtMenuPerfil(context);
         AV27Usuario_PessoaNom = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00ER2_A57Usuario_PessoaCod = new int[1] ;
         H00ER2_A1Usuario_Codigo = new int[1] ;
         H00ER2_A58Usuario_PessoaNom = new String[] {""} ;
         H00ER2_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00ER3_A74Contrato_Codigo = new int[1] ;
         H00ER3_A39Contratada_Codigo = new int[1] ;
         H00ER3_A632Servico_Ativo = new bool[] {false} ;
         H00ER3_A92Contrato_Ativo = new bool[] {false} ;
         H00ER3_A43Contratada_Ativo = new bool[] {false} ;
         H00ER3_A155Servico_Codigo = new int[1] ;
         AV35Codigos = new GxSimpleCollection();
         H00ER4_A1530Servico_TipoHierarquia = new short[1] ;
         H00ER4_n1530Servico_TipoHierarquia = new bool[] {false} ;
         H00ER4_A155Servico_Codigo = new int[1] ;
         H00ER4_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         H00ER5_A29Contratante_Codigo = new int[1] ;
         H00ER5_n29Contratante_Codigo = new bool[] {false} ;
         H00ER5_A5AreaTrabalho_Codigo = new int[1] ;
         H00ER6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00ER6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00ER7_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00ER7_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00ER8_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00ER8_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00ER8_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00ER8_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00ER9_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00ER9_A829UsuarioServicos_ServicoCod = new int[1] ;
         AV30jqItem = new SdtjqSelectData_Item(context);
         H00ER10_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00ER10_A828UsuarioServicos_UsuarioCod = new int[1] ;
         AV11UsuarioServicos = new SdtUsuarioServicos(context);
         H00ER11_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00ER11_A828UsuarioServicos_UsuarioCod = new int[1] ;
         AV50GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associationusuarioservicos__default(),
            new Object[][] {
                new Object[] {
               H00ER2_A57Usuario_PessoaCod, H00ER2_A1Usuario_Codigo, H00ER2_A58Usuario_PessoaNom, H00ER2_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00ER3_A74Contrato_Codigo, H00ER3_A39Contratada_Codigo, H00ER3_A632Servico_Ativo, H00ER3_A92Contrato_Ativo, H00ER3_A43Contratada_Ativo, H00ER3_A155Servico_Codigo
               }
               , new Object[] {
               H00ER4_A1530Servico_TipoHierarquia, H00ER4_n1530Servico_TipoHierarquia, H00ER4_A155Servico_Codigo, H00ER4_A605Servico_Sigla
               }
               , new Object[] {
               H00ER5_A29Contratante_Codigo, H00ER5_n29Contratante_Codigo, H00ER5_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00ER6_A63ContratanteUsuario_ContratanteCod, H00ER6_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               H00ER7_A828UsuarioServicos_UsuarioCod, H00ER7_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00ER8_A66ContratadaUsuario_ContratadaCod, H00ER8_A69ContratadaUsuario_UsuarioCod, H00ER8_A1228ContratadaUsuario_AreaTrabalhoCod, H00ER8_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00ER9_A828UsuarioServicos_UsuarioCod, H00ER9_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00ER10_A829UsuarioServicos_ServicoCod, H00ER10_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               H00ER11_A829UsuarioServicos_ServicoCod, H00ER11_A828UsuarioServicos_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsuario_pessoanom_Enabled = 0;
      }

      private short nRcdExists_12 ;
      private short nIsMod_12 ;
      private short nRcdExists_11 ;
      private short nIsMod_11 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV38GXLvl6 ;
      private short A1530Servico_TipoHierarquia ;
      private short AV47GXLvl83 ;
      private short nGXWrapped ;
      private int AV7UsuarioServicos_UsuarioCod ;
      private int A39Contratada_Codigo ;
      private int wcpOAV7UsuarioServicos_UsuarioCod ;
      private int wcpOA39Contratada_Codigo ;
      private int edtavUsuario_pessoanom_Enabled ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int AV8UsuarioServicos_ServicoCod ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV46GXV1 ;
      private int AV48GXV2 ;
      private int AV51GXV4 ;
      private int AV14i ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavUsuario_pessoanom_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV27Usuario_PessoaNom ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A58Usuario_PessoaNom ;
      private String A605Servico_Sigla ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtavUsuario_pessoanom_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV28All ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n58Usuario_PessoaNom ;
      private bool A632Servico_Ativo ;
      private bool A92Contrato_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool n1530Servico_TipoHierarquia ;
      private bool AV10Exist ;
      private bool n29Contratante_Codigo ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool AV12Success ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_Contratada_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00ER2_A57Usuario_PessoaCod ;
      private int[] H00ER2_A1Usuario_Codigo ;
      private String[] H00ER2_A58Usuario_PessoaNom ;
      private bool[] H00ER2_n58Usuario_PessoaNom ;
      private int[] H00ER3_A74Contrato_Codigo ;
      private int[] H00ER3_A39Contratada_Codigo ;
      private bool[] H00ER3_A632Servico_Ativo ;
      private bool[] H00ER3_A92Contrato_Ativo ;
      private bool[] H00ER3_A43Contratada_Ativo ;
      private int[] H00ER3_A155Servico_Codigo ;
      private short[] H00ER4_A1530Servico_TipoHierarquia ;
      private bool[] H00ER4_n1530Servico_TipoHierarquia ;
      private int[] H00ER4_A155Servico_Codigo ;
      private String[] H00ER4_A605Servico_Sigla ;
      private int[] H00ER5_A29Contratante_Codigo ;
      private bool[] H00ER5_n29Contratante_Codigo ;
      private int[] H00ER5_A5AreaTrabalho_Codigo ;
      private int[] H00ER6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00ER6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00ER7_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ER7_A829UsuarioServicos_ServicoCod ;
      private int[] H00ER8_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00ER8_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00ER8_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00ER8_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00ER9_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ER9_A829UsuarioServicos_ServicoCod ;
      private int[] H00ER10_A829UsuarioServicos_ServicoCod ;
      private int[] H00ER10_A828UsuarioServicos_UsuarioCod ;
      private int[] H00ER11_A829UsuarioServicos_ServicoCod ;
      private int[] H00ER11_A828UsuarioServicos_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV35Codigos ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV50GXV3 ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV31jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV29jqAssociados ;
      private SdtMenuPerfil AV34MenuPerfil ;
      private SdtMessages_Message AV15Message ;
      private SdtjqSelectData_Item AV30jqItem ;
      private SdtUsuarioServicos AV11UsuarioServicos ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_associationusuarioservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00ER4( IGxContext context ,
                                             int A155Servico_Codigo ,
                                             IGxCollection AV35Codigos ,
                                             short A1530Servico_TipoHierarquia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [Servico_TipoHierarquia], [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV35Codigos, "[Servico_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([Servico_TipoHierarquia] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Servico_Sigla]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00ER4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ER2 ;
          prmH00ER2 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER3 ;
          prmH00ER3 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER5 ;
          prmH00ER5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER6 ;
          prmH00ER6 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER7 ;
          prmH00ER7 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER8 ;
          prmH00ER8 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER9 ;
          prmH00ER9 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER10 ;
          prmH00ER10 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER11 ;
          prmH00ER11 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00ER4 ;
          prmH00ER4 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ER2", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV7UsuarioServicos_UsuarioCod ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER2,1,0,false,true )
             ,new CursorDef("H00ER3", "SELECT DISTINCT NULL AS [Contrato_Codigo], NULL AS [Contratada_Codigo], NULL AS [Servico_Ativo], NULL AS [Contrato_Ativo], NULL AS [Contratada_Ativo], [Servico_Codigo] FROM ( SELECT TOP(100) PERCENT T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Servico_Ativo], T2.[Contrato_Ativo], T4.[Contratada_Ativo], T1.[Servico_Codigo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T2.[Contratada_Codigo] = @Contratada_Codigo) AND (T4.[Contratada_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Servico_Ativo] = 1) ORDER BY T2.[Contratada_Codigo], T1.[Servico_Codigo]) DistinctT ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER3,100,0,false,false )
             ,new CursorDef("H00ER4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER4,100,0,true,false )
             ,new CursorDef("H00ER5", "SELECT [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER5,1,0,true,true )
             ,new CursorDef("H00ER6", "SELECT [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER6,1,0,true,true )
             ,new CursorDef("H00ER7", "SELECT [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratanteUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV8UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER7,1,0,false,true )
             ,new CursorDef("H00ER8", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER8,100,0,true,false )
             ,new CursorDef("H00ER9", "SELECT [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV8UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER9,1,0,false,true )
             ,new CursorDef("H00ER10", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV8UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER10,1,0,false,true )
             ,new CursorDef("H00ER11", "SELECT TOP 1 [UsuarioServicos_ServicoCod], [UsuarioServicos_UsuarioCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV8UsuarioServicos_ServicoCod ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ER11,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
