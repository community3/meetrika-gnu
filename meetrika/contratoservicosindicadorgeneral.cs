/*
               File: ContratoServicosIndicadorGeneral
        Description: Contrato Servicos Indicador General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:33.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosindicadorgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosindicadorgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosindicadorgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosIndicador_Codigo )
      {
         this.A1269ContratoServicosIndicador_Codigo = aP0_ContratoServicosIndicador_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoServicosIndicador_Tipo = new GXCombobox();
         cmbContratoServicosIndicador_Periodicidade = new GXCombobox();
         cmbContratoServicosIndicador_CalculoSob = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1269ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1269ContratoServicosIndicador_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAJI2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "ContratoServicosIndicadorGeneral";
               context.Gx_err = 0;
               WSJI2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Indicador General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312029336");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosindicadorgeneral.aspx") + "?" + UrlEncode("" +A1269ContratoServicosIndicador_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCALLER", StringUtil.RTrim( AV13Caller));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR", GetSecureSignedToken( sPrefix, A1274ContratoServicosIndicador_Indicador));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2051ContratoServicosIndicador_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_FINALIDADE", GetSecureSignedToken( sPrefix, A1305ContratoServicosIndicador_Finalidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_META", GetSecureSignedToken( sPrefix, A1306ContratoServicosIndicador_Meta));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", GetSecureSignedToken( sPrefix, A1307ContratoServicosIndicador_InstrumentoMedicao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_CALCULOSOB", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1345ContratoServicosIndicador_CalculoSob, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_VIGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosIndicadorGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosindicadorgeneral:[SendSecurityCheck value for]"+"ContratoServicosIndicador_CntSrvCod:"+context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormJI2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosindicadorgeneral.js", "?2020312029338");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosIndicadorGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Indicador General" ;
      }

      protected void WBJI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosindicadorgeneral.aspx");
            }
            wb_table1_2_JI2( true) ;
         }
         else
         {
            wb_table1_2_JI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicadorGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_CntSrvCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicadorGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1296ContratoServicosIndicador_ContratoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_ContratoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_ContratoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicadorGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_AreaTrabalhoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicadorGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTJI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Indicador General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPJI0( ) ;
            }
         }
      }

      protected void WSJI2( )
      {
         STARTJI2( ) ;
         EVTJI2( ) ;
      }

      protected void EVTJI2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11JI2 */
                                    E11JI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12JI2 */
                                    E12JI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JI2 */
                                    E13JI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JI2 */
                                    E14JI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15JI2 */
                                    E15JI2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJI0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJI2( ) ;
            }
         }
      }

      protected void PAJI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratoServicosIndicador_Tipo.Name = "CONTRATOSERVICOSINDICADOR_TIPO";
            cmbContratoServicosIndicador_Tipo.WebTags = "";
            cmbContratoServicosIndicador_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("P", "Pontualidade (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("PL", "Frequ�ncia de atrasos (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("D", "Diverg�ncias (demanda)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FD", "Frequ�ncia de diverg�ncias (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("FP", "Frequ�ncia de pend�ncias (�ndice de rejei��o de demandas) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AC", "Ajustes de contagens (qtd) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("AV", "Ajustes de contagens (soma do valor bruto) (lote)", 0);
            cmbContratoServicosIndicador_Tipo.addItem("T", "Tempestividade", 0);
            if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
            {
               A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
               n1308ContratoServicosIndicador_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
            }
            cmbContratoServicosIndicador_Periodicidade.Name = "CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
            cmbContratoServicosIndicador_Periodicidade.WebTags = "";
            cmbContratoServicosIndicador_Periodicidade.addItem("M", "Mensal", 0);
            if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
            {
               A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
               n1309ContratoServicosIndicador_Periodicidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
            }
            cmbContratoServicosIndicador_CalculoSob.Name = "CONTRATOSERVICOSINDICADOR_CALCULOSOB";
            cmbContratoServicosIndicador_CalculoSob.WebTags = "";
            cmbContratoServicosIndicador_CalculoSob.addItem("", "(Nenhum)", 0);
            cmbContratoServicosIndicador_CalculoSob.addItem("D", "Dias", 0);
            cmbContratoServicosIndicador_CalculoSob.addItem("P", "Percentuais", 0);
            if ( cmbContratoServicosIndicador_CalculoSob.ItemCount > 0 )
            {
               A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.getValidValue(A1345ContratoServicosIndicador_CalculoSob);
               n1345ContratoServicosIndicador_CalculoSob = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CALCULOSOB", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1345ContratoServicosIndicador_CalculoSob, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosIndicador_Tipo.ItemCount > 0 )
         {
            A1308ContratoServicosIndicador_Tipo = cmbContratoServicosIndicador_Tipo.getValidValue(A1308ContratoServicosIndicador_Tipo);
            n1308ContratoServicosIndicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
         }
         if ( cmbContratoServicosIndicador_Periodicidade.ItemCount > 0 )
         {
            A1309ContratoServicosIndicador_Periodicidade = cmbContratoServicosIndicador_Periodicidade.getValidValue(A1309ContratoServicosIndicador_Periodicidade);
            n1309ContratoServicosIndicador_Periodicidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
         }
         if ( cmbContratoServicosIndicador_CalculoSob.ItemCount > 0 )
         {
            A1345ContratoServicosIndicador_CalculoSob = cmbContratoServicosIndicador_CalculoSob.getValidValue(A1345ContratoServicosIndicador_CalculoSob);
            n1345ContratoServicosIndicador_CalculoSob = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CALCULOSOB", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1345ContratoServicosIndicador_CalculoSob, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosIndicadorGeneral";
         context.Gx_err = 0;
      }

      protected void RFJI2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00JI2 */
            pr_default.execute(0, new Object[] {A1269ContratoServicosIndicador_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JI2_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1295ContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
               n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JI2_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               A1296ContratoServicosIndicador_ContratoCod = H00JI2_A1296ContratoServicosIndicador_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1296ContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0)));
               n1296ContratoServicosIndicador_ContratoCod = H00JI2_n1296ContratoServicosIndicador_ContratoCod[0];
               A1270ContratoServicosIndicador_CntSrvCod = H00JI2_A1270ContratoServicosIndicador_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
               A1310ContratoServicosIndicador_Vigencia = H00JI2_A1310ContratoServicosIndicador_Vigencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_VIGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!"))));
               n1310ContratoServicosIndicador_Vigencia = H00JI2_n1310ContratoServicosIndicador_Vigencia[0];
               A1345ContratoServicosIndicador_CalculoSob = H00JI2_A1345ContratoServicosIndicador_CalculoSob[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CALCULOSOB", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1345ContratoServicosIndicador_CalculoSob, ""))));
               n1345ContratoServicosIndicador_CalculoSob = H00JI2_n1345ContratoServicosIndicador_CalculoSob[0];
               A1309ContratoServicosIndicador_Periodicidade = H00JI2_A1309ContratoServicosIndicador_Periodicidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
               n1309ContratoServicosIndicador_Periodicidade = H00JI2_n1309ContratoServicosIndicador_Periodicidade[0];
               A1308ContratoServicosIndicador_Tipo = H00JI2_A1308ContratoServicosIndicador_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
               n1308ContratoServicosIndicador_Tipo = H00JI2_n1308ContratoServicosIndicador_Tipo[0];
               A1307ContratoServicosIndicador_InstrumentoMedicao = H00JI2_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", GetSecureSignedToken( sPrefix, A1307ContratoServicosIndicador_InstrumentoMedicao));
               n1307ContratoServicosIndicador_InstrumentoMedicao = H00JI2_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
               A1306ContratoServicosIndicador_Meta = H00JI2_A1306ContratoServicosIndicador_Meta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_META", GetSecureSignedToken( sPrefix, A1306ContratoServicosIndicador_Meta));
               n1306ContratoServicosIndicador_Meta = H00JI2_n1306ContratoServicosIndicador_Meta[0];
               A1305ContratoServicosIndicador_Finalidade = H00JI2_A1305ContratoServicosIndicador_Finalidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_FINALIDADE", GetSecureSignedToken( sPrefix, A1305ContratoServicosIndicador_Finalidade));
               n1305ContratoServicosIndicador_Finalidade = H00JI2_n1305ContratoServicosIndicador_Finalidade[0];
               A2051ContratoServicosIndicador_Sigla = H00JI2_A2051ContratoServicosIndicador_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2051ContratoServicosIndicador_Sigla, "@!"))));
               n2051ContratoServicosIndicador_Sigla = H00JI2_n2051ContratoServicosIndicador_Sigla[0];
               A1274ContratoServicosIndicador_Indicador = H00JI2_A1274ContratoServicosIndicador_Indicador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR", GetSecureSignedToken( sPrefix, A1274ContratoServicosIndicador_Indicador));
               A1271ContratoServicosIndicador_Numero = H00JI2_A1271ContratoServicosIndicador_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
               A1296ContratoServicosIndicador_ContratoCod = H00JI2_A1296ContratoServicosIndicador_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1296ContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0)));
               n1296ContratoServicosIndicador_ContratoCod = H00JI2_n1296ContratoServicosIndicador_ContratoCod[0];
               A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JI2_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1295ContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
               n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JI2_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
               /* Execute user event: E12JI2 */
               E12JI2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBJI0( ) ;
         }
      }

      protected void STRUPJI0( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "ContratoServicosIndicadorGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11JI2 */
         E11JI2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1271ContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
            A1274ContratoServicosIndicador_Indicador = cgiGet( edtContratoServicosIndicador_Indicador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1274ContratoServicosIndicador_Indicador", A1274ContratoServicosIndicador_Indicador);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR", GetSecureSignedToken( sPrefix, A1274ContratoServicosIndicador_Indicador));
            A2051ContratoServicosIndicador_Sigla = StringUtil.Upper( cgiGet( edtContratoServicosIndicador_Sigla_Internalname));
            n2051ContratoServicosIndicador_Sigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2051ContratoServicosIndicador_Sigla", A2051ContratoServicosIndicador_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2051ContratoServicosIndicador_Sigla, "@!"))));
            A1305ContratoServicosIndicador_Finalidade = cgiGet( edtContratoServicosIndicador_Finalidade_Internalname);
            n1305ContratoServicosIndicador_Finalidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1305ContratoServicosIndicador_Finalidade", A1305ContratoServicosIndicador_Finalidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_FINALIDADE", GetSecureSignedToken( sPrefix, A1305ContratoServicosIndicador_Finalidade));
            A1306ContratoServicosIndicador_Meta = cgiGet( edtContratoServicosIndicador_Meta_Internalname);
            n1306ContratoServicosIndicador_Meta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1306ContratoServicosIndicador_Meta", A1306ContratoServicosIndicador_Meta);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_META", GetSecureSignedToken( sPrefix, A1306ContratoServicosIndicador_Meta));
            A1307ContratoServicosIndicador_InstrumentoMedicao = cgiGet( edtContratoServicosIndicador_InstrumentoMedicao_Internalname);
            n1307ContratoServicosIndicador_InstrumentoMedicao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1307ContratoServicosIndicador_InstrumentoMedicao", A1307ContratoServicosIndicador_InstrumentoMedicao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO", GetSecureSignedToken( sPrefix, A1307ContratoServicosIndicador_InstrumentoMedicao));
            cmbContratoServicosIndicador_Tipo.CurrentValue = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
            A1308ContratoServicosIndicador_Tipo = cgiGet( cmbContratoServicosIndicador_Tipo_Internalname);
            n1308ContratoServicosIndicador_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1308ContratoServicosIndicador_Tipo", A1308ContratoServicosIndicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1308ContratoServicosIndicador_Tipo, ""))));
            cmbContratoServicosIndicador_Periodicidade.CurrentValue = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
            A1309ContratoServicosIndicador_Periodicidade = cgiGet( cmbContratoServicosIndicador_Periodicidade_Internalname);
            n1309ContratoServicosIndicador_Periodicidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1309ContratoServicosIndicador_Periodicidade", A1309ContratoServicosIndicador_Periodicidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_PERIODICIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1309ContratoServicosIndicador_Periodicidade, ""))));
            cmbContratoServicosIndicador_CalculoSob.CurrentValue = cgiGet( cmbContratoServicosIndicador_CalculoSob_Internalname);
            A1345ContratoServicosIndicador_CalculoSob = cgiGet( cmbContratoServicosIndicador_CalculoSob_Internalname);
            n1345ContratoServicosIndicador_CalculoSob = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CALCULOSOB", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1345ContratoServicosIndicador_CalculoSob, ""))));
            A1310ContratoServicosIndicador_Vigencia = StringUtil.Upper( cgiGet( edtContratoServicosIndicador_Vigencia_Internalname));
            n1310ContratoServicosIndicador_Vigencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1310ContratoServicosIndicador_Vigencia", A1310ContratoServicosIndicador_Vigencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_VIGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!"))));
            A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
            A1296ContratoServicosIndicador_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_ContratoCod_Internalname), ",", "."));
            n1296ContratoServicosIndicador_ContratoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1296ContratoServicosIndicador_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0)));
            A1295ContratoServicosIndicador_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_AreaTrabalhoCod_Internalname), ",", "."));
            n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1295ContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
            /* Read saved values. */
            wcpOA1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1269ContratoServicosIndicador_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosIndicadorGeneral";
            A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSINDICADOR_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoservicosindicadorgeneral:[SecurityCheckFailed value for]"+"ContratoServicosIndicador_CntSrvCod:"+context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11JI2 */
         E11JI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11JI2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV13Caller = AV14WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Caller", AV13Caller);
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12JI2( )
      {
         /* Load Routine */
         edtContratoServicosIndicador_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Visible), 5, 0)));
         edtContratoServicosIndicador_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Visible), 5, 0)));
         edtContratoServicosIndicador_ContratoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_ContratoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_ContratoCod_Visible), 5, 0)));
         edtContratoServicosIndicador_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_AreaTrabalhoCod_Visible), 5, 0)));
      }

      protected void E13JI2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1269ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV12ContratoServicosIndicador_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14JI2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1269ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV12ContratoServicosIndicador_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E15JI2( )
      {
         /* 'DoFechar' Routine */
         if ( StringUtil.StrCmp(AV13Caller, "Cnt") == 0 )
         {
            context.wjLoc = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A1270ContratoServicosIndicador_CntSrvCod) + "," + UrlEncode(StringUtil.RTrim("Indicadores"));
            context.wjLocDisableFrm = 1;
         }
         else if ( StringUtil.StrCmp(AV13Caller, "Ind") == 0 )
         {
            context.wjLoc = formatLink("wwcontratoservicosindicador.aspx") ;
            context.wjLocDisableFrm = 1;
         }
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV17Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosIndicador";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoServicosIndicador_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_JI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JI2( true) ;
         }
         else
         {
            wb_table2_8_JI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_61_JI2( true) ;
         }
         else
         {
            wb_table3_61_JI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_61_JI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JI2e( true) ;
         }
         else
         {
            wb_table1_2_JI2e( false) ;
         }
      }

      protected void wb_table3_61_JI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_JI2e( true) ;
         }
         else
         {
            wb_table3_61_JI2e( false) ;
         }
      }

      protected void wb_table2_8_JI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_numero_Internalname, "N�mero", "", "", lblTextblockcontratoservicosindicador_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Numero_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_indicador_Internalname, "Indicador", "", "", lblTextblockcontratoservicosindicador_indicador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Indicador_Internalname, A1274ContratoServicosIndicador_Indicador, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_sigla_Internalname, "Sigla", "", "", lblTextblockcontratoservicosindicador_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Sigla_Internalname, StringUtil.RTrim( A2051ContratoServicosIndicador_Sigla), StringUtil.RTrim( context.localUtil.Format( A2051ContratoServicosIndicador_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_finalidade_Internalname, "Finalidade", "", "", lblTextblockcontratoservicosindicador_finalidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Finalidade_Internalname, A1305ContratoServicosIndicador_Finalidade, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_meta_Internalname, "Meta a cumprir", "", "", lblTextblockcontratoservicosindicador_meta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_Meta_Internalname, A1306ContratoServicosIndicador_Meta, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname, "Instrumento de Medi��o", "", "", lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoServicosIndicador_InstrumentoMedicao_Internalname, A1307ContratoServicosIndicador_InstrumentoMedicao, "", "", 0, 1, 0, 0, 100, "%", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_tipo_Internalname, "Tipo de Indicador", "", "", lblTextblockcontratoservicosindicador_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Tipo, cmbContratoServicosIndicador_Tipo_Internalname, StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo), 1, cmbContratoServicosIndicador_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoServicosIndicadorGeneral.htm");
            cmbContratoServicosIndicador_Tipo.CurrentValue = StringUtil.RTrim( A1308ContratoServicosIndicador_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosIndicador_Tipo_Internalname, "Values", (String)(cmbContratoServicosIndicador_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_periodicidade_Internalname, "Periodicidade", "", "", lblTextblockcontratoservicosindicador_periodicidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Periodicidade, cmbContratoServicosIndicador_Periodicidade_Internalname, StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade), 1, cmbContratoServicosIndicador_Periodicidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoServicosIndicadorGeneral.htm");
            cmbContratoServicosIndicador_Periodicidade.CurrentValue = StringUtil.RTrim( A1309ContratoServicosIndicador_Periodicidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosIndicador_Periodicidade_Internalname, "Values", (String)(cmbContratoServicosIndicador_Periodicidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_calculosob_Internalname, "C�lculo Sob", "", "", lblTextblockcontratoservicosindicador_calculosob_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_CalculoSob, cmbContratoServicosIndicador_CalculoSob_Internalname, StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob), 1, cmbContratoServicosIndicador_CalculoSob_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoServicosIndicadorGeneral.htm");
            cmbContratoServicosIndicador_CalculoSob.CurrentValue = StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosIndicador_CalculoSob_Internalname, "Values", (String)(cmbContratoServicosIndicador_CalculoSob.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_vigencia_Internalname, "Vig�ncia", "", "", lblTextblockcontratoservicosindicador_vigencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Vigencia_Internalname, A1310ContratoServicosIndicador_Vigencia, StringUtil.RTrim( context.localUtil.Format( A1310ContratoServicosIndicador_Vigencia, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Vigencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoServicosIndicadorGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JI2e( true) ;
         }
         else
         {
            wb_table2_8_JI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1269ContratoServicosIndicador_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJI2( ) ;
         WSJI2( ) ;
         WEJI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1269ContratoServicosIndicador_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAJI2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosindicadorgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAJI2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1269ContratoServicosIndicador_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         }
         wcpOA1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1269ContratoServicosIndicador_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1269ContratoServicosIndicador_Codigo != wcpOA1269ContratoServicosIndicador_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1269ContratoServicosIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1269ContratoServicosIndicador_Codigo = cgiGet( sPrefix+"A1269ContratoServicosIndicador_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1269ContratoServicosIndicador_Codigo) > 0 )
         {
            A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1269ContratoServicosIndicador_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
         }
         else
         {
            A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1269ContratoServicosIndicador_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAJI2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSJI2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSJI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1269ContratoServicosIndicador_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1269ContratoServicosIndicador_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1269ContratoServicosIndicador_Codigo_CTRL", StringUtil.RTrim( sCtrlA1269ContratoServicosIndicador_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEJI2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120293345");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosindicadorgeneral.js", "?20203120293345");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosindicador_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_NUMERO";
         edtContratoServicosIndicador_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_NUMERO";
         lblTextblockcontratoservicosindicador_indicador_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_INDICADOR";
         edtContratoServicosIndicador_Indicador_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_INDICADOR";
         lblTextblockcontratoservicosindicador_sigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_SIGLA";
         edtContratoServicosIndicador_Sigla_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_SIGLA";
         lblTextblockcontratoservicosindicador_finalidade_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_FINALIDADE";
         edtContratoServicosIndicador_Finalidade_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_FINALIDADE";
         lblTextblockcontratoservicosindicador_meta_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_META";
         edtContratoServicosIndicador_Meta_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_META";
         lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         edtContratoServicosIndicador_InstrumentoMedicao_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_INSTRUMENTOMEDICAO";
         lblTextblockcontratoservicosindicador_tipo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_TIPO";
         cmbContratoServicosIndicador_Tipo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_TIPO";
         lblTextblockcontratoservicosindicador_periodicidade_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         cmbContratoServicosIndicador_Periodicidade_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_PERIODICIDADE";
         lblTextblockcontratoservicosindicador_calculosob_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_CALCULOSOB";
         cmbContratoServicosIndicador_CalculoSob_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CALCULOSOB";
         lblTextblockcontratoservicosindicador_vigencia_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_VIGENCIA";
         edtContratoServicosIndicador_Vigencia_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_VIGENCIA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoServicosIndicador_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO";
         edtContratoServicosIndicador_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         edtContratoServicosIndicador_ContratoCod_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CONTRATOCOD";
         edtContratoServicosIndicador_AreaTrabalhoCod_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosIndicador_Vigencia_Jsonclick = "";
         cmbContratoServicosIndicador_CalculoSob_Jsonclick = "";
         cmbContratoServicosIndicador_Periodicidade_Jsonclick = "";
         cmbContratoServicosIndicador_Tipo_Jsonclick = "";
         edtContratoServicosIndicador_Sigla_Jsonclick = "";
         edtContratoServicosIndicador_Numero_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick = "";
         edtContratoServicosIndicador_AreaTrabalhoCod_Visible = 1;
         edtContratoServicosIndicador_ContratoCod_Jsonclick = "";
         edtContratoServicosIndicador_ContratoCod_Visible = 1;
         edtContratoServicosIndicador_CntSrvCod_Jsonclick = "";
         edtContratoServicosIndicador_CntSrvCod_Visible = 1;
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13JI2',iparms:[{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14JI2',iparms:[{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15JI2',iparms:[{av:'AV13Caller',fld:'vCALLER',pic:'',nv:''},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV13Caller = "";
         A1274ContratoServicosIndicador_Indicador = "";
         A2051ContratoServicosIndicador_Sigla = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00JI2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JI2_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         H00JI2_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         H00JI2_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         H00JI2_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         H00JI2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00JI2_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         H00JI2_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         H00JI2_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         H00JI2_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         H00JI2_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         H00JI2_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         H00JI2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         H00JI2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         H00JI2_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         H00JI2_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         H00JI2_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         H00JI2_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         H00JI2_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         H00JI2_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         H00JI2_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         H00JI2_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         H00JI2_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         H00JI2_A1271ContratoServicosIndicador_Numero = new short[1] ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV14WebSession = context.GetSession();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontratoservicosindicador_numero_Jsonclick = "";
         lblTextblockcontratoservicosindicador_indicador_Jsonclick = "";
         lblTextblockcontratoservicosindicador_sigla_Jsonclick = "";
         lblTextblockcontratoservicosindicador_finalidade_Jsonclick = "";
         lblTextblockcontratoservicosindicador_meta_Jsonclick = "";
         lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick = "";
         lblTextblockcontratoservicosindicador_tipo_Jsonclick = "";
         lblTextblockcontratoservicosindicador_periodicidade_Jsonclick = "";
         lblTextblockcontratoservicosindicador_calculosob_Jsonclick = "";
         lblTextblockcontratoservicosindicador_vigencia_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1269ContratoServicosIndicador_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosindicadorgeneral__default(),
            new Object[][] {
                new Object[] {
               H00JI2_A1269ContratoServicosIndicador_Codigo, H00JI2_A1295ContratoServicosIndicador_AreaTrabalhoCod, H00JI2_n1295ContratoServicosIndicador_AreaTrabalhoCod, H00JI2_A1296ContratoServicosIndicador_ContratoCod, H00JI2_n1296ContratoServicosIndicador_ContratoCod, H00JI2_A1270ContratoServicosIndicador_CntSrvCod, H00JI2_A1310ContratoServicosIndicador_Vigencia, H00JI2_n1310ContratoServicosIndicador_Vigencia, H00JI2_A1345ContratoServicosIndicador_CalculoSob, H00JI2_n1345ContratoServicosIndicador_CalculoSob,
               H00JI2_A1309ContratoServicosIndicador_Periodicidade, H00JI2_n1309ContratoServicosIndicador_Periodicidade, H00JI2_A1308ContratoServicosIndicador_Tipo, H00JI2_n1308ContratoServicosIndicador_Tipo, H00JI2_A1307ContratoServicosIndicador_InstrumentoMedicao, H00JI2_n1307ContratoServicosIndicador_InstrumentoMedicao, H00JI2_A1306ContratoServicosIndicador_Meta, H00JI2_n1306ContratoServicosIndicador_Meta, H00JI2_A1305ContratoServicosIndicador_Finalidade, H00JI2_n1305ContratoServicosIndicador_Finalidade,
               H00JI2_A2051ContratoServicosIndicador_Sigla, H00JI2_n2051ContratoServicosIndicador_Sigla, H00JI2_A1274ContratoServicosIndicador_Indicador, H00JI2_A1271ContratoServicosIndicador_Numero
               }
            }
         );
         AV17Pgmname = "ContratoServicosIndicadorGeneral";
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosIndicadorGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int wcpOA1269ContratoServicosIndicador_Codigo ;
      private int AV12ContratoServicosIndicador_CntSrvCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int edtContratoServicosIndicador_Codigo_Visible ;
      private int edtContratoServicosIndicador_CntSrvCod_Visible ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int edtContratoServicosIndicador_ContratoCod_Visible ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int edtContratoServicosIndicador_AreaTrabalhoCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoServicosIndicador_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV17Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV13Caller ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String edtContratoServicosIndicador_CntSrvCod_Internalname ;
      private String edtContratoServicosIndicador_CntSrvCod_Jsonclick ;
      private String edtContratoServicosIndicador_ContratoCod_Internalname ;
      private String edtContratoServicosIndicador_ContratoCod_Jsonclick ;
      private String edtContratoServicosIndicador_AreaTrabalhoCod_Internalname ;
      private String edtContratoServicosIndicador_AreaTrabalhoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContratoServicosIndicador_Numero_Internalname ;
      private String edtContratoServicosIndicador_Indicador_Internalname ;
      private String edtContratoServicosIndicador_Sigla_Internalname ;
      private String edtContratoServicosIndicador_Finalidade_Internalname ;
      private String edtContratoServicosIndicador_Meta_Internalname ;
      private String edtContratoServicosIndicador_InstrumentoMedicao_Internalname ;
      private String cmbContratoServicosIndicador_Tipo_Internalname ;
      private String cmbContratoServicosIndicador_Periodicidade_Internalname ;
      private String cmbContratoServicosIndicador_CalculoSob_Internalname ;
      private String edtContratoServicosIndicador_Vigencia_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosindicador_numero_Internalname ;
      private String lblTextblockcontratoservicosindicador_numero_Jsonclick ;
      private String edtContratoServicosIndicador_Numero_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_indicador_Internalname ;
      private String lblTextblockcontratoservicosindicador_indicador_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_sigla_Internalname ;
      private String lblTextblockcontratoservicosindicador_sigla_Jsonclick ;
      private String edtContratoServicosIndicador_Sigla_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_finalidade_Internalname ;
      private String lblTextblockcontratoservicosindicador_finalidade_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_meta_Internalname ;
      private String lblTextblockcontratoservicosindicador_meta_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_instrumentomedicao_Internalname ;
      private String lblTextblockcontratoservicosindicador_instrumentomedicao_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_tipo_Internalname ;
      private String lblTextblockcontratoservicosindicador_tipo_Jsonclick ;
      private String cmbContratoServicosIndicador_Tipo_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_periodicidade_Internalname ;
      private String lblTextblockcontratoservicosindicador_periodicidade_Jsonclick ;
      private String cmbContratoServicosIndicador_Periodicidade_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_calculosob_Internalname ;
      private String lblTextblockcontratoservicosindicador_calculosob_Jsonclick ;
      private String cmbContratoServicosIndicador_CalculoSob_Jsonclick ;
      private String lblTextblockcontratoservicosindicador_vigencia_Internalname ;
      private String lblTextblockcontratoservicosindicador_vigencia_Jsonclick ;
      private String edtContratoServicosIndicador_Vigencia_Jsonclick ;
      private String sCtrlA1269ContratoServicosIndicador_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool returnInSub ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private IGxSession AV14WebSession ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosIndicador_Tipo ;
      private GXCombobox cmbContratoServicosIndicador_Periodicidade ;
      private GXCombobox cmbContratoServicosIndicador_CalculoSob ;
      private IDataStoreProvider pr_default ;
      private int[] H00JI2_A1269ContratoServicosIndicador_Codigo ;
      private int[] H00JI2_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] H00JI2_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] H00JI2_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] H00JI2_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] H00JI2_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] H00JI2_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] H00JI2_n1310ContratoServicosIndicador_Vigencia ;
      private String[] H00JI2_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] H00JI2_n1345ContratoServicosIndicador_CalculoSob ;
      private String[] H00JI2_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] H00JI2_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] H00JI2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] H00JI2_n1308ContratoServicosIndicador_Tipo ;
      private String[] H00JI2_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] H00JI2_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] H00JI2_A1306ContratoServicosIndicador_Meta ;
      private bool[] H00JI2_n1306ContratoServicosIndicador_Meta ;
      private String[] H00JI2_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] H00JI2_n1305ContratoServicosIndicador_Finalidade ;
      private String[] H00JI2_A2051ContratoServicosIndicador_Sigla ;
      private bool[] H00JI2_n2051ContratoServicosIndicador_Sigla ;
      private String[] H00JI2_A1274ContratoServicosIndicador_Indicador ;
      private short[] H00JI2_A1271ContratoServicosIndicador_Numero ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoservicosindicadorgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JI2 ;
          prmH00JI2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JI2", "SELECT T1.[ContratoServicosIndicador_Codigo], T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_CalculoSob], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Sigla], T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Numero] FROM (([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T1.[ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY T1.[ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JI2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 2) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getLongVarchar(13) ;
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
