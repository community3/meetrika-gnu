/*
               File: GAMRepositoryConfiguration
        Description: Repository configuration
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:12.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamrepositoryconfiguration : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamrepositoryconfiguration( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamrepositoryconfiguration( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( long aP0_pId )
      {
         this.AV34pId = aP0_pId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDefaultauthtypename = new GXCombobox();
         chkavSessionexpiresonipchange = new GXCheckbox();
         chkavAllowoauthaccess = new GXCheckbox();
         cmbavUseridentification = new GXCombobox();
         cmbavUseractivationmethod = new GXCombobox();
         chkavUseremailisunique = new GXCheckbox();
         chkavRequiredemail = new GXCheckbox();
         chkavRequiredpassword = new GXCheckbox();
         cmbavGeneratesessionstatistics = new GXCombobox();
         cmbavUserremembermetype = new GXCombobox();
         chkavGiveanonymoussession = new GXCheckbox();
         cmbavDefaultsecuritypolicyid = new GXCombobox();
         cmbavDefaultroleid = new GXCombobox();
         cmbavEnabletracing = new GXCombobox();
         chkavRequiredfirstname = new GXCheckbox();
         chkavRequiredlastname = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV34pId = (long)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34pId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34pId), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34pId), "ZZZZZZZZZZZ9")));
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2A2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2A2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117321416");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamrepositoryconfiguration.aspx") + "?" + UrlEncode("" +AV34pId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vPID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34pId), 12, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2A2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2A2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamrepositoryconfiguration.aspx") + "?" + UrlEncode("" +AV34pId) ;
      }

      public override String GetPgmname( )
      {
         return "GAMRepositoryConfiguration" ;
      }

      public override String GetPgmdesc( )
      {
         return "Repository configuration " ;
      }

      protected void WB2A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, "Repository Configuration", "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            wb_table1_3_2A2( true) ;
         }
         else
         {
            wb_table1_3_2A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_2A2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Repository configuration ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2A0( ) ;
      }

      protected void WS2A2( )
      {
         START2A2( ) ;
         EVT2A2( ) ;
      }

      protected void EVT2A2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E112A2 */
                              E112A2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E122A2 */
                                    E122A2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E132A2 */
                              E132A2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavDefaultauthtypename.Name = "vDEFAULTAUTHTYPENAME";
            cmbavDefaultauthtypename.WebTags = "";
            if ( cmbavDefaultauthtypename.ItemCount > 0 )
            {
               AV9DefaultAuthTypeName = cmbavDefaultauthtypename.getValidValue(AV9DefaultAuthTypeName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
            }
            chkavSessionexpiresonipchange.Name = "vSESSIONEXPIRESONIPCHANGE";
            chkavSessionexpiresonipchange.WebTags = "";
            chkavSessionexpiresonipchange.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSessionexpiresonipchange_Internalname, "TitleCaption", chkavSessionexpiresonipchange.Caption);
            chkavSessionexpiresonipchange.CheckedValue = "false";
            chkavAllowoauthaccess.Name = "vALLOWOAUTHACCESS";
            chkavAllowoauthaccess.WebTags = "";
            chkavAllowoauthaccess.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAllowoauthaccess_Internalname, "TitleCaption", chkavAllowoauthaccess.Caption);
            chkavAllowoauthaccess.CheckedValue = "false";
            cmbavUseridentification.Name = "vUSERIDENTIFICATION";
            cmbavUseridentification.WebTags = "";
            cmbavUseridentification.addItem("name", "Name", 0);
            cmbavUseridentification.addItem("email", "EMail", 0);
            cmbavUseridentification.addItem("namema", "Name and Email", 0);
            if ( cmbavUseridentification.ItemCount > 0 )
            {
               AV52UserIdentification = cmbavUseridentification.getValidValue(AV52UserIdentification);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52UserIdentification", AV52UserIdentification);
            }
            cmbavUseractivationmethod.Name = "vUSERACTIVATIONMETHOD";
            cmbavUseractivationmethod.WebTags = "";
            cmbavUseractivationmethod.addItem("A", "Automatic", 0);
            cmbavUseractivationmethod.addItem("U", "User", 0);
            cmbavUseractivationmethod.addItem("D", "Administrator", 0);
            if ( cmbavUseractivationmethod.ItemCount > 0 )
            {
               AV49UserActivationMethod = cmbavUseractivationmethod.getValidValue(AV49UserActivationMethod);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserActivationMethod", AV49UserActivationMethod);
            }
            chkavUseremailisunique.Name = "vUSEREMAILISUNIQUE";
            chkavUseremailisunique.WebTags = "";
            chkavUseremailisunique.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUseremailisunique_Internalname, "TitleCaption", chkavUseremailisunique.Caption);
            chkavUseremailisunique.CheckedValue = "false";
            chkavRequiredemail.Name = "vREQUIREDEMAIL";
            chkavRequiredemail.WebTags = "";
            chkavRequiredemail.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredemail_Internalname, "TitleCaption", chkavRequiredemail.Caption);
            chkavRequiredemail.CheckedValue = "false";
            chkavRequiredpassword.Name = "vREQUIREDPASSWORD";
            chkavRequiredpassword.WebTags = "";
            chkavRequiredpassword.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredpassword_Internalname, "TitleCaption", chkavRequiredpassword.Caption);
            chkavRequiredpassword.CheckedValue = "false";
            cmbavGeneratesessionstatistics.Name = "vGENERATESESSIONSTATISTICS";
            cmbavGeneratesessionstatistics.WebTags = "";
            cmbavGeneratesessionstatistics.addItem("None", "None", 0);
            cmbavGeneratesessionstatistics.addItem("Minimum", "Minimum (Only authenticated users)", 0);
            cmbavGeneratesessionstatistics.addItem("Detail", "Detail (Authenticated and anonymous users)", 0);
            if ( cmbavGeneratesessionstatistics.ItemCount > 0 )
            {
               AV21GenerateSessionStatistics = cmbavGeneratesessionstatistics.getValidValue(AV21GenerateSessionStatistics);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GenerateSessionStatistics", AV21GenerateSessionStatistics);
            }
            cmbavUserremembermetype.Name = "vUSERREMEMBERMETYPE";
            cmbavUserremembermetype.WebTags = "";
            cmbavUserremembermetype.addItem("None", "None", 0);
            cmbavUserremembermetype.addItem("Login", "Login", 0);
            cmbavUserremembermetype.addItem("Auth", "Authentication", 0);
            cmbavUserremembermetype.addItem("Both", "Both", 0);
            if ( cmbavUserremembermetype.ItemCount > 0 )
            {
               AV55UserRememberMeType = cmbavUserremembermetype.getValidValue(AV55UserRememberMeType);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55UserRememberMeType", AV55UserRememberMeType);
            }
            chkavGiveanonymoussession.Name = "vGIVEANONYMOUSSESSION";
            chkavGiveanonymoussession.WebTags = "";
            chkavGiveanonymoussession.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavGiveanonymoussession_Internalname, "TitleCaption", chkavGiveanonymoussession.Caption);
            chkavGiveanonymoussession.CheckedValue = "false";
            cmbavDefaultsecuritypolicyid.Name = "vDEFAULTSECURITYPOLICYID";
            cmbavDefaultsecuritypolicyid.WebTags = "";
            if ( cmbavDefaultsecuritypolicyid.ItemCount > 0 )
            {
               AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cmbavDefaultsecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
            }
            cmbavDefaultroleid.Name = "vDEFAULTROLEID";
            cmbavDefaultroleid.WebTags = "";
            if ( cmbavDefaultroleid.ItemCount > 0 )
            {
               AV10DefaultRoleId = (long)(NumberUtil.Val( cmbavDefaultroleid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
            }
            cmbavEnabletracing.Name = "vENABLETRACING";
            cmbavEnabletracing.WebTags = "";
            cmbavEnabletracing.addItem("0", "0 - Off", 0);
            cmbavEnabletracing.addItem("1", "1 - Debug", 0);
            if ( cmbavEnabletracing.ItemCount > 0 )
            {
               AV57EnableTracing = (short)(NumberUtil.Val( cmbavEnabletracing.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0)));
            }
            chkavRequiredfirstname.Name = "vREQUIREDFIRSTNAME";
            chkavRequiredfirstname.WebTags = "";
            chkavRequiredfirstname.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredfirstname_Internalname, "TitleCaption", chkavRequiredfirstname.Caption);
            chkavRequiredfirstname.CheckedValue = "false";
            chkavRequiredlastname.Name = "vREQUIREDLASTNAME";
            chkavRequiredlastname.WebTags = "";
            chkavRequiredlastname.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredlastname_Internalname, "TitleCaption", chkavRequiredlastname.Caption);
            chkavRequiredlastname.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDefaultauthtypename.ItemCount > 0 )
         {
            AV9DefaultAuthTypeName = cmbavDefaultauthtypename.getValidValue(AV9DefaultAuthTypeName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
         }
         if ( cmbavUseridentification.ItemCount > 0 )
         {
            AV52UserIdentification = cmbavUseridentification.getValidValue(AV52UserIdentification);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52UserIdentification", AV52UserIdentification);
         }
         if ( cmbavUseractivationmethod.ItemCount > 0 )
         {
            AV49UserActivationMethod = cmbavUseractivationmethod.getValidValue(AV49UserActivationMethod);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserActivationMethod", AV49UserActivationMethod);
         }
         if ( cmbavGeneratesessionstatistics.ItemCount > 0 )
         {
            AV21GenerateSessionStatistics = cmbavGeneratesessionstatistics.getValidValue(AV21GenerateSessionStatistics);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GenerateSessionStatistics", AV21GenerateSessionStatistics);
         }
         if ( cmbavUserremembermetype.ItemCount > 0 )
         {
            AV55UserRememberMeType = cmbavUserremembermetype.getValidValue(AV55UserRememberMeType);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55UserRememberMeType", AV55UserRememberMeType);
         }
         if ( cmbavDefaultsecuritypolicyid.ItemCount > 0 )
         {
            AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cmbavDefaultsecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
         }
         if ( cmbavDefaultroleid.ItemCount > 0 )
         {
            AV10DefaultRoleId = (long)(NumberUtil.Val( cmbavDefaultroleid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
         }
         if ( cmbavEnabletracing.ItemCount > 0 )
         {
            AV57EnableTracing = (short)(NumberUtil.Val( cmbavEnabletracing.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2A2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         edtavNamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNamespace_Enabled), 5, 0)));
      }

      protected void RF2A2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E132A2 */
            E132A2 ();
            WB2A0( ) ;
         }
      }

      protected void STRUP2A0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         edtavNamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNamespace_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112A2 */
         E112A2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vID");
               GX_FocusControl = edtavId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24Id = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
            }
            else
            {
               AV24Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
            }
            AV23GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GUID", AV23GUID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23GUID, ""))));
            AV33NameSpace = cgiGet( edtavNamespace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33NameSpace", AV33NameSpace);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAMESPACE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV33NameSpace, ""))));
            AV32Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
            AV12Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Dsc", AV12Dsc);
            cmbavDefaultauthtypename.CurrentValue = cgiGet( cmbavDefaultauthtypename_Internalname);
            AV9DefaultAuthTypeName = cgiGet( cmbavDefaultauthtypename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
            AV48SessionExpiresOnIPChange = StringUtil.StrToBool( cgiGet( chkavSessionexpiresonipchange_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48SessionExpiresOnIPChange", AV48SessionExpiresOnIPChange);
            AV5AllowOauthAccess = StringUtil.StrToBool( cgiGet( chkavAllowoauthaccess_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowOauthAccess", AV5AllowOauthAccess);
            cmbavUseridentification.CurrentValue = cgiGet( cmbavUseridentification_Internalname);
            AV52UserIdentification = cgiGet( cmbavUseridentification_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52UserIdentification", AV52UserIdentification);
            cmbavUseractivationmethod.CurrentValue = cgiGet( cmbavUseractivationmethod_Internalname);
            AV49UserActivationMethod = cgiGet( cmbavUseractivationmethod_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserActivationMethod", AV49UserActivationMethod);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERAUTOMATICACTIVATIONTIMEOUT");
               GX_FocusControl = edtavUserautomaticactivationtimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50UserAutomaticActivationTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UserAutomaticActivationTimeout), 4, 0)));
            }
            else
            {
               AV50UserAutomaticActivationTimeout = (short)(context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UserAutomaticActivationTimeout), 4, 0)));
            }
            AV51UserEmailisUnique = StringUtil.StrToBool( cgiGet( chkavUseremailisunique_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51UserEmailisUnique", AV51UserEmailisUnique);
            AV38RequiredEmail = StringUtil.StrToBool( cgiGet( chkavRequiredemail_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38RequiredEmail", AV38RequiredEmail);
            AV41RequiredPassword = StringUtil.StrToBool( cgiGet( chkavRequiredpassword_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41RequiredPassword", AV41RequiredPassword);
            cmbavGeneratesessionstatistics.CurrentValue = cgiGet( cmbavGeneratesessionstatistics_Internalname);
            AV21GenerateSessionStatistics = cgiGet( cmbavGeneratesessionstatistics_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GenerateSessionStatistics", AV21GenerateSessionStatistics);
            cmbavUserremembermetype.CurrentValue = cgiGet( cmbavUserremembermetype_Internalname);
            AV55UserRememberMeType = cgiGet( cmbavUserremembermetype_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55UserRememberMeType", AV55UserRememberMeType);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERREMEMBERMETIMEOUT");
               GX_FocusControl = edtavUserremembermetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54UserRememberMeTimeOut = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54UserRememberMeTimeOut), 4, 0)));
            }
            else
            {
               AV54UserRememberMeTimeOut = (short)(context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54UserRememberMeTimeOut), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERRECOVERYPASSWORDKEYTIMEOUT");
               GX_FocusControl = edtavUserrecoverypasswordkeytimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53UserRecoveryPasswordKeyTimeOut = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53UserRecoveryPasswordKeyTimeOut), 4, 0)));
            }
            else
            {
               AV53UserRecoveryPasswordKeyTimeOut = (short)(context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53UserRecoveryPasswordKeyTimeOut), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMAMOUNTCHARACTERSINLOGIN");
               GX_FocusControl = edtavMinimumamountcharactersinlogin_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31MinimumAmountCharactersInLogin = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31MinimumAmountCharactersInLogin), 2, 0)));
            }
            else
            {
               AV31MinimumAmountCharactersInLogin = (short)(context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31MinimumAmountCharactersInLogin), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOGINATTEMPTSTOLOCKUSER");
               GX_FocusControl = edtavLoginattemptstolockuser_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28LoginAttemptsToLockUser = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28LoginAttemptsToLockUser), 2, 0)));
            }
            else
            {
               AV28LoginAttemptsToLockUser = (short)(context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28LoginAttemptsToLockUser), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGAMUNBLOCKUSERTIMEOUT");
               GX_FocusControl = edtavGamunblockusertimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19GAMUnblockUserTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GAMUnblockUserTimeout), 4, 0)));
            }
            else
            {
               AV19GAMUnblockUserTimeout = (short)(context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GAMUnblockUserTimeout), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOGINATTEMPTSTOLOCKSESSION");
               GX_FocusControl = edtavLoginattemptstolocksession_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27LoginAttemptsToLockSession = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27LoginAttemptsToLockSession), 2, 0)));
            }
            else
            {
               AV27LoginAttemptsToLockSession = (short)(context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27LoginAttemptsToLockSession), 2, 0)));
            }
            AV22GiveAnonymousSession = StringUtil.StrToBool( cgiGet( chkavGiveanonymoussession_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22GiveAnonymousSession", AV22GiveAnonymousSession);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERSESSIONCACHETIMEOUT");
               GX_FocusControl = edtavUsersessioncachetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56UserSessionCacheTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56UserSessionCacheTimeout), 6, 0)));
            }
            else
            {
               AV56UserSessionCacheTimeout = (int)(context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56UserSessionCacheTimeout), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREPOSITORYCACHETIMEOUT");
               GX_FocusControl = edtavRepositorycachetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36RepositoryCacheTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36RepositoryCacheTimeout), 6, 0)));
            }
            else
            {
               AV36RepositoryCacheTimeout = (int)(context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36RepositoryCacheTimeout), 6, 0)));
            }
            cmbavDefaultsecuritypolicyid.CurrentValue = cgiGet( cmbavDefaultsecuritypolicyid_Internalname);
            AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cgiGet( cmbavDefaultsecuritypolicyid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
            cmbavDefaultroleid.CurrentValue = cgiGet( cmbavDefaultroleid_Internalname);
            AV10DefaultRoleId = (long)(NumberUtil.Val( cgiGet( cmbavDefaultroleid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
            cmbavEnabletracing.CurrentValue = cgiGet( cmbavEnabletracing_Internalname);
            AV57EnableTracing = (short)(NumberUtil.Val( cgiGet( cmbavEnabletracing_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0)));
            AV39RequiredFirstName = StringUtil.StrToBool( cgiGet( chkavRequiredfirstname_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39RequiredFirstName", AV39RequiredFirstName);
            AV40RequiredLastName = StringUtil.StrToBool( cgiGet( chkavRequiredlastname_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40RequiredLastName", AV40RequiredLastName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112A2 */
         E112A2 ();
         if (returnInSub) return;
      }

      protected void E112A2( )
      {
         /* Start Routine */
         AV25isLoginRepositoryAdm = new SdtGAMRepository(context).isgamadministrator(out  AV14Errors);
         lblTbdefaultauthtype_Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdefaultauthtype_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbdefaultauthtype_Visible), 5, 0)));
         cmbavDefaultauthtypename.Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultauthtypename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDefaultauthtypename.Visible), 5, 0)));
         lblTbdefaultsecpolicy_Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdefaultsecpolicy_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbdefaultsecpolicy_Visible), 5, 0)));
         cmbavDefaultsecuritypolicyid.Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultsecuritypolicyid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDefaultsecuritypolicyid.Visible), 5, 0)));
         lblTbdefaultrole_Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbdefaultrole_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbdefaultrole_Visible), 5, 0)));
         cmbavDefaultroleid.Visible = (!AV25isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultroleid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDefaultroleid.Visible), 5, 0)));
         if ( ! AV25isLoginRepositoryAdm )
         {
            AV7AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV26Language, out  AV14Errors);
            AV60GXV1 = 1;
            while ( AV60GXV1 <= AV7AuthenticationTypes.Count )
            {
               AV6AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV7AuthenticationTypes.Item(AV60GXV1));
               cmbavDefaultauthtypename.addItem(AV6AuthenticationType.gxTpr_Name, AV6AuthenticationType.gxTpr_Description, 0);
               AV60GXV1 = (int)(AV60GXV1+1);
            }
            AV46SecurityPolicies = new SdtGAMRepository(context).getsecuritypolicies(AV17FilterSecPol, out  AV14Errors);
            AV61GXV2 = 1;
            while ( AV61GXV2 <= AV46SecurityPolicies.Count )
            {
               AV47SecurityPolicy = ((SdtGAMSecurityPolicy)AV46SecurityPolicies.Item(AV61GXV2));
               cmbavDefaultsecuritypolicyid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV47SecurityPolicy.gxTpr_Id), 9, 0)), AV47SecurityPolicy.gxTpr_Name, 0);
               AV61GXV2 = (int)(AV61GXV2+1);
            }
            AV43Roles = new SdtGAMRepository(context).getroles(AV16FilterRole, out  AV14Errors);
            AV62GXV3 = 1;
            while ( AV62GXV3 <= AV43Roles.Count )
            {
               AV42Role = ((SdtGAMRole)AV43Roles.Item(AV62GXV3));
               cmbavDefaultroleid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Role.gxTpr_Id), 12, 0)), AV42Role.gxTpr_Name, 0);
               AV62GXV3 = (int)(AV62GXV3+1);
            }
         }
         if ( (0==AV34pId) )
         {
            AV24Id = new SdtGAMRepository(context).getid();
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
         }
         else
         {
            AV24Id = AV34pId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
         }
         AV35Repository.load( (int)(AV24Id));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")));
         AV23GUID = AV35Repository.gxTpr_Guid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GUID", AV23GUID);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV23GUID, ""))));
         AV33NameSpace = AV35Repository.gxTpr_Namespace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33NameSpace", AV33NameSpace);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAMESPACE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV33NameSpace, ""))));
         AV32Name = AV35Repository.gxTpr_Name;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
         AV12Dsc = AV35Repository.gxTpr_Description;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Dsc", AV12Dsc);
         AV9DefaultAuthTypeName = AV35Repository.gxTpr_Defaultauthenticationtypename;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
         AV52UserIdentification = AV35Repository.gxTpr_Useridentification;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52UserIdentification", AV52UserIdentification);
         AV21GenerateSessionStatistics = AV35Repository.gxTpr_Generatesessionstatistics;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GenerateSessionStatistics", AV21GenerateSessionStatistics);
         AV49UserActivationMethod = AV35Repository.gxTpr_Useractivationmethod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserActivationMethod", AV49UserActivationMethod);
         AV50UserAutomaticActivationTimeout = AV35Repository.gxTpr_Userautomaticactivationtimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UserAutomaticActivationTimeout), 4, 0)));
         AV55UserRememberMeType = AV35Repository.gxTpr_Userremembermetype;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55UserRememberMeType", AV55UserRememberMeType);
         AV54UserRememberMeTimeOut = AV35Repository.gxTpr_Userremembermetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54UserRememberMeTimeOut), 4, 0)));
         AV53UserRecoveryPasswordKeyTimeOut = AV35Repository.gxTpr_Userrecoverypasswordkeytimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53UserRecoveryPasswordKeyTimeOut), 4, 0)));
         AV31MinimumAmountCharactersInLogin = AV35Repository.gxTpr_Minimumamountcharactersinlogin;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31MinimumAmountCharactersInLogin), 2, 0)));
         AV28LoginAttemptsToLockUser = AV35Repository.gxTpr_Loginattemptstolockuser;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28LoginAttemptsToLockUser), 2, 0)));
         AV19GAMUnblockUserTimeout = AV35Repository.gxTpr_Gamunblockusertimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GAMUnblockUserTimeout), 4, 0)));
         AV27LoginAttemptsToLockSession = AV35Repository.gxTpr_Loginattemptstolocksession;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27LoginAttemptsToLockSession), 2, 0)));
         AV56UserSessionCacheTimeout = AV35Repository.gxTpr_Usersessioncachetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56UserSessionCacheTimeout), 6, 0)));
         AV36RepositoryCacheTimeout = AV35Repository.gxTpr_Cachetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36RepositoryCacheTimeout), 6, 0)));
         AV45SecurityAdministratorEmail = AV35Repository.gxTpr_Securityadministratoremail;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45SecurityAdministratorEmail", AV45SecurityAdministratorEmail);
         AV22GiveAnonymousSession = AV35Repository.gxTpr_Giveanonymoussession;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22GiveAnonymousSession", AV22GiveAnonymousSession);
         AV8CanRegisterUsers = AV35Repository.gxTpr_Canregisterusers;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8CanRegisterUsers", AV8CanRegisterUsers);
         AV51UserEmailisUnique = AV35Repository.gxTpr_Useremailisunique;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51UserEmailisUnique", AV51UserEmailisUnique);
         AV11DefaultSecurityPolicyId = AV35Repository.gxTpr_Defaultsecuritypolicyid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
         AV10DefaultRoleId = AV35Repository.gxTpr_Defaultroleid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
         AV57EnableTracing = AV35Repository.gxTpr_Enabletracing;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0)));
         AV5AllowOauthAccess = AV35Repository.gxTpr_Allowoauthaccess;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowOauthAccess", AV5AllowOauthAccess);
         AV48SessionExpiresOnIPChange = AV35Repository.gxTpr_Sessionexpiresonipchange;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48SessionExpiresOnIPChange", AV48SessionExpiresOnIPChange);
         AV41RequiredPassword = AV35Repository.gxTpr_Requiredpassword;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41RequiredPassword", AV41RequiredPassword);
         AV38RequiredEmail = AV35Repository.gxTpr_Requiredemail;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38RequiredEmail", AV38RequiredEmail);
         AV39RequiredFirstName = AV35Repository.gxTpr_Requiredfirstname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39RequiredFirstName", AV39RequiredFirstName);
         AV40RequiredLastName = AV35Repository.gxTpr_Requiredlastname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40RequiredLastName", AV40RequiredLastName);
      }

      public void GXEnter( )
      {
         /* Execute user event: E122A2 */
         E122A2 ();
         if (returnInSub) return;
      }

      protected void E122A2( )
      {
         /* Enter Routine */
         AV35Repository.gxTpr_Name = AV32Name;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Description = AV12Dsc;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Defaultauthenticationtypename = AV9DefaultAuthTypeName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Useridentification = AV52UserIdentification;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Generatesessionstatistics = AV21GenerateSessionStatistics;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Useractivationmethod = AV49UserActivationMethod;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Userautomaticactivationtimeout = AV50UserAutomaticActivationTimeout;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Gamunblockusertimeout = AV19GAMUnblockUserTimeout;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Userremembermetype = AV55UserRememberMeType;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Userremembermetimeout = AV54UserRememberMeTimeOut;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Userrecoverypasswordkeytimeout = AV53UserRecoveryPasswordKeyTimeOut;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Minimumamountcharactersinlogin = AV31MinimumAmountCharactersInLogin;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Loginattemptstolockuser = AV28LoginAttemptsToLockUser;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Loginattemptstolocksession = AV27LoginAttemptsToLockSession;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Usersessioncachetimeout = AV56UserSessionCacheTimeout;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Cachetimeout = AV36RepositoryCacheTimeout;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Securityadministratoremail = AV45SecurityAdministratorEmail;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Giveanonymoussession = AV22GiveAnonymousSession;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Canregisterusers = AV8CanRegisterUsers;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Useremailisunique = AV51UserEmailisUnique;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Defaultsecuritypolicyid = AV11DefaultSecurityPolicyId;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Defaultroleid = AV10DefaultRoleId;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Enabletracing = AV57EnableTracing;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Allowoauthaccess = AV5AllowOauthAccess;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Sessionexpiresonipchange = AV48SessionExpiresOnIPChange;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Requiredpassword = AV41RequiredPassword;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Requiredemail = AV38RequiredEmail;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Requiredfirstname = AV39RequiredFirstName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.gxTpr_Requiredlastname = AV40RequiredLastName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35Repository", AV35Repository);
         AV35Repository.save();
         if ( AV35Repository.success() )
         {
            context.CommitDataStores( "GAMRepositoryConfiguration");
            GX_msglist.addItem(context.GetMessage( "Os dados foram atualizados com sucesso.", ""));
         }
         else
         {
            AV14Errors = AV35Repository.geterrors();
            AV63GXV4 = 1;
            while ( AV63GXV4 <= AV14Errors.Count )
            {
               AV13Error = ((SdtGAMError)AV14Errors.Item(AV63GXV4));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV13Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV63GXV4 = (int)(AV63GXV4+1);
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E132A2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_2A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbl_Internalname, tblTbl_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:17px;width:150px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:300px")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbid_Internalname, "Id", "", "", lblTbid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Id), 12, 0, ",", "")), ((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV24Id), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,12);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavId_Jsonclick, 0, "Attribute", "", "", "", 1, edtavId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid_Internalname, "GUID", "", "", lblTbguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV23GUID), StringUtil.RTrim( context.localUtil.Format( AV23GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavGuid_Enabled, 0, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnamespace_Internalname, "Name Space", "", "", lblTbnamespace_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNamespace_Internalname, StringUtil.RTrim( AV33NameSpace), StringUtil.RTrim( context.localUtil.Format( AV33NameSpace, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNamespace_Jsonclick, 0, "Attribute", "", "", "", 1, edtavNamespace_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, 0, true, "GAMRepositoryNameSpace", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV32Name), StringUtil.RTrim( context.localUtil.Format( AV32Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 17, "px", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc_Internalname, "Description", "", "", lblTbdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV12Dsc), StringUtil.RTrim( context.localUtil.Format( AV12Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 421, "px", 17, "px", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdefaultauthtype_Internalname, "Default authentication type", "", "", lblTbdefaultauthtype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbdefaultauthtype_Visible, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultauthtypename, cmbavDefaultauthtypename_Internalname, StringUtil.RTrim( AV9DefaultAuthTypeName), 1, cmbavDefaultauthtypename_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavDefaultauthtypename.Visible, 1, 0, 0, 250, "px", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultauthtypename.CurrentValue = StringUtil.RTrim( AV9DefaultAuthTypeName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultauthtypename_Internalname, "Values", (String)(cmbavDefaultauthtypename.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsessionexpiresonipchange_Internalname, "GAM Session expires on IP change?", "", "", lblTbsessionexpiresonipchange_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSessionexpiresonipchange_Internalname, StringUtil.BoolToStr( AV48SessionExpiresOnIPChange), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(42, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTballowoauthaccess_Internalname, "Allow Oauth access (Smart Devices)", "", "", lblTballowoauthaccess_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAllowoauthaccess_Internalname, StringUtil.BoolToStr( AV5AllowOauthAccess), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(47, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:23px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuseridentifications_Internalname, "User identification", "", "", lblTbuseridentifications_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUseridentification, cmbavUseridentification_Internalname, StringUtil.RTrim( AV52UserIdentification), 1, cmbavUseridentification_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 250, "px", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUseridentification.CurrentValue = StringUtil.RTrim( AV52UserIdentification);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseridentification_Internalname, "Values", (String)(cmbavUseridentification.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuseractivarionmethod_Internalname, "User activation method", "", "", lblTbuseractivarionmethod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUseractivationmethod, cmbavUseractivationmethod_Internalname, StringUtil.RTrim( AV49UserActivationMethod), 1, cmbavUseractivationmethod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUseractivationmethod.CurrentValue = StringUtil.RTrim( AV49UserActivationMethod);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseractivationmethod_Internalname, "Values", (String)(cmbavUseractivationmethod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserautomaticactivationtimeout_Internalname, "User automatic activation timeout (hours)", "", "", lblTbuserautomaticactivationtimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserautomaticactivationtimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50UserAutomaticActivationTimeout), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50UserAutomaticActivationTimeout), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserautomaticactivationtimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuseremailisunique_Internalname, "User Email is unique?", "", "", lblTbuseremailisunique_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUseremailisunique_Internalname, StringUtil.BoolToStr( AV51UserEmailisUnique), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(70, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc9_Internalname, "Required email?", "", "", lblTbdsc9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredemail_Internalname, StringUtil.BoolToStr( AV38RequiredEmail), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(75, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdrequiredpassword_Internalname, "Required password?", "", "", lblTbdrequiredpassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredpassword_Internalname, StringUtil.BoolToStr( AV41RequiredPassword), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgeneratesessionstatistics_Internalname, "Generate session statistics?", "", "", lblTbgeneratesessionstatistics_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGeneratesessionstatistics, cmbavGeneratesessionstatistics_Internalname, StringUtil.RTrim( AV21GenerateSessionStatistics), 1, cmbavGeneratesessionstatistics_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavGeneratesessionstatistics.CurrentValue = StringUtil.RTrim( AV21GenerateSessionStatistics);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGeneratesessionstatistics_Internalname, "Values", (String)(cmbavGeneratesessionstatistics.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:24px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserremembermetype_Internalname, "User remember me type", "", "", lblTbuserremembermetype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUserremembermetype, cmbavUserremembermetype_Internalname, StringUtil.RTrim( AV55UserRememberMeType), 1, cmbavUserremembermetype_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUserremembermetype.CurrentValue = StringUtil.RTrim( AV55UserRememberMeType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUserremembermetype_Internalname, "Values", (String)(cmbavUserremembermetype.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserremembermetimeout_Internalname, "User remember me timeout (days)", "", "", lblTbuserremembermetimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserremembermetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54UserRememberMeTimeOut), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54UserRememberMeTimeOut), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserremembermetimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserrecoverypasswordkeytimeout_Internalname, "User recovery password key timeout (minutes)", "", "", lblTbuserrecoverypasswordkeytimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserrecoverypasswordkeytimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53UserRecoveryPasswordKeyTimeOut), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53UserRecoveryPasswordKeyTimeOut), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserrecoverypasswordkeytimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbminimumamountofcharactersinlogin_Internalname, "Minimum amount of characters in login", "", "", lblTbminimumamountofcharactersinlogin_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumamountcharactersinlogin_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31MinimumAmountCharactersInLogin), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31MinimumAmountCharactersInLogin), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumamountcharactersinlogin_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbloginretriestolockuser_Internalname, "Login retries to lock user", "", "", lblTbloginretriestolockuser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLoginattemptstolockuser_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28LoginAttemptsToLockUser), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28LoginAttemptsToLockUser), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLoginattemptstolockuser_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgamunblockusertimeout_Internalname, "GAM Unblock user timeout (minutes)", "", "", lblTbgamunblockusertimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamunblockusertimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19GAMUnblockUserTimeout), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19GAMUnblockUserTimeout), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamunblockusertimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbloginretriestolocksession_Internalname, "Login retries to lock session", "", "", lblTbloginretriestolocksession_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLoginattemptstolocksession_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27LoginAttemptsToLockSession), 2, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27LoginAttemptsToLockSession), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLoginattemptstolocksession_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgiveanonymoussession_Internalname, "Give web Anonymous sessions?", "", "", lblTbgiveanonymoussession_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavGiveanonymoussession_Internalname, StringUtil.BoolToStr( AV22GiveAnonymousSession), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(131, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbusersessioncachetimeout_Internalname, "User session cache timeout (seconds)", "", "", lblTbusersessioncachetimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsersessioncachetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56UserSessionCacheTimeout), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56UserSessionCacheTimeout), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsersessioncachetimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcachetimeout_Internalname, "Repository cache timeout (minutes)", "", "", lblTbcachetimeout_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRepositorycachetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36RepositoryCacheTimeout), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36RepositoryCacheTimeout), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRepositorycachetimeout_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdefaultsecpolicy_Internalname, "Repository default security policy", "", "", lblTbdefaultsecpolicy_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbdefaultsecpolicy_Visible, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultsecuritypolicyid, cmbavDefaultsecuritypolicyid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)), 1, cmbavDefaultsecuritypolicyid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDefaultsecuritypolicyid.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultsecuritypolicyid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultsecuritypolicyid_Internalname, "Values", (String)(cmbavDefaultsecuritypolicyid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdefaultrole_Internalname, "Repository default role", "", "", lblTbdefaultrole_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbdefaultrole_Visible, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultroleid, cmbavDefaultroleid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)), 1, cmbavDefaultroleid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDefaultroleid.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,154);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultroleid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultroleid_Internalname, "Values", (String)(cmbavDefaultroleid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbenabletracing_Internalname, "Enable tracing", "", "", lblTbenabletracing_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavEnabletracing, cmbavEnabletracing_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0)), 1, cmbavEnabletracing_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavEnabletracing.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV57EnableTracing), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEnabletracing_Internalname, "Values", (String)(cmbavEnabletracing.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrequiredfirstname_Internalname, "Required first name?", "", "", lblTbrequiredfirstname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredfirstname_Internalname, StringUtil.BoolToStr( AV39RequiredFirstName), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(167, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,167);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrequiredlastname_Internalname, "Required last name?", "", "", lblTbrequiredlastname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredlastname_Internalname, StringUtil.BoolToStr( AV40RequiredLastName), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(172, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,172);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_182_2A2( true) ;
         }
         else
         {
            wb_table2_182_2A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_182_2A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_2A2e( true) ;
         }
         else
         {
            wb_table1_3_2A2e( false) ;
         }
      }

      protected void wb_table2_182_2A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 185,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 188,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMRepositoryConfiguration.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_182_2A2e( true) ;
         }
         else
         {
            wb_table2_182_2A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV34pId = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34pId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34pId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV34pId), "ZZZZZZZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2A2( ) ;
         WS2A2( ) ;
         WE2A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117321891");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamrepositoryconfiguration.js", "?20203117321892");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle_Internalname = "TBFORMTITLE";
         lblTbid_Internalname = "TBID";
         edtavId_Internalname = "vID";
         lblTbguid_Internalname = "TBGUID";
         edtavGuid_Internalname = "vGUID";
         lblTbnamespace_Internalname = "TBNAMESPACE";
         edtavNamespace_Internalname = "vNAMESPACE";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbdsc_Internalname = "TBDSC";
         edtavDsc_Internalname = "vDSC";
         lblTbdefaultauthtype_Internalname = "TBDEFAULTAUTHTYPE";
         cmbavDefaultauthtypename_Internalname = "vDEFAULTAUTHTYPENAME";
         lblTbsessionexpiresonipchange_Internalname = "TBSESSIONEXPIRESONIPCHANGE";
         chkavSessionexpiresonipchange_Internalname = "vSESSIONEXPIRESONIPCHANGE";
         lblTballowoauthaccess_Internalname = "TBALLOWOAUTHACCESS";
         chkavAllowoauthaccess_Internalname = "vALLOWOAUTHACCESS";
         lblTbuseridentifications_Internalname = "TBUSERIDENTIFICATIONS";
         cmbavUseridentification_Internalname = "vUSERIDENTIFICATION";
         lblTbuseractivarionmethod_Internalname = "TBUSERACTIVARIONMETHOD";
         cmbavUseractivationmethod_Internalname = "vUSERACTIVATIONMETHOD";
         lblTbuserautomaticactivationtimeout_Internalname = "TBUSERAUTOMATICACTIVATIONTIMEOUT";
         edtavUserautomaticactivationtimeout_Internalname = "vUSERAUTOMATICACTIVATIONTIMEOUT";
         lblTbuseremailisunique_Internalname = "TBUSEREMAILISUNIQUE";
         chkavUseremailisunique_Internalname = "vUSEREMAILISUNIQUE";
         lblTbdsc9_Internalname = "TBDSC9";
         chkavRequiredemail_Internalname = "vREQUIREDEMAIL";
         lblTbdrequiredpassword_Internalname = "TBDREQUIREDPASSWORD";
         chkavRequiredpassword_Internalname = "vREQUIREDPASSWORD";
         lblTbgeneratesessionstatistics_Internalname = "TBGENERATESESSIONSTATISTICS";
         cmbavGeneratesessionstatistics_Internalname = "vGENERATESESSIONSTATISTICS";
         lblTbuserremembermetype_Internalname = "TBUSERREMEMBERMETYPE";
         cmbavUserremembermetype_Internalname = "vUSERREMEMBERMETYPE";
         lblTbuserremembermetimeout_Internalname = "TBUSERREMEMBERMETIMEOUT";
         edtavUserremembermetimeout_Internalname = "vUSERREMEMBERMETIMEOUT";
         lblTbuserrecoverypasswordkeytimeout_Internalname = "TBUSERRECOVERYPASSWORDKEYTIMEOUT";
         edtavUserrecoverypasswordkeytimeout_Internalname = "vUSERRECOVERYPASSWORDKEYTIMEOUT";
         lblTbminimumamountofcharactersinlogin_Internalname = "TBMINIMUMAMOUNTOFCHARACTERSINLOGIN";
         edtavMinimumamountcharactersinlogin_Internalname = "vMINIMUMAMOUNTCHARACTERSINLOGIN";
         lblTbloginretriestolockuser_Internalname = "TBLOGINRETRIESTOLOCKUSER";
         edtavLoginattemptstolockuser_Internalname = "vLOGINATTEMPTSTOLOCKUSER";
         lblTbgamunblockusertimeout_Internalname = "TBGAMUNBLOCKUSERTIMEOUT";
         edtavGamunblockusertimeout_Internalname = "vGAMUNBLOCKUSERTIMEOUT";
         lblTbloginretriestolocksession_Internalname = "TBLOGINRETRIESTOLOCKSESSION";
         edtavLoginattemptstolocksession_Internalname = "vLOGINATTEMPTSTOLOCKSESSION";
         lblTbgiveanonymoussession_Internalname = "TBGIVEANONYMOUSSESSION";
         chkavGiveanonymoussession_Internalname = "vGIVEANONYMOUSSESSION";
         lblTbusersessioncachetimeout_Internalname = "TBUSERSESSIONCACHETIMEOUT";
         edtavUsersessioncachetimeout_Internalname = "vUSERSESSIONCACHETIMEOUT";
         lblTbcachetimeout_Internalname = "TBCACHETIMEOUT";
         edtavRepositorycachetimeout_Internalname = "vREPOSITORYCACHETIMEOUT";
         lblTbdefaultsecpolicy_Internalname = "TBDEFAULTSECPOLICY";
         cmbavDefaultsecuritypolicyid_Internalname = "vDEFAULTSECURITYPOLICYID";
         lblTbdefaultrole_Internalname = "TBDEFAULTROLE";
         cmbavDefaultroleid_Internalname = "vDEFAULTROLEID";
         lblTbenabletracing_Internalname = "TBENABLETRACING";
         cmbavEnabletracing_Internalname = "vENABLETRACING";
         lblTbrequiredfirstname_Internalname = "TBREQUIREDFIRSTNAME";
         chkavRequiredfirstname_Internalname = "vREQUIREDFIRSTNAME";
         lblTbrequiredlastname_Internalname = "TBREQUIREDLASTNAME";
         chkavRequiredlastname_Internalname = "vREQUIREDLASTNAME";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTbl_Internalname = "TBL";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         cmbavEnabletracing_Jsonclick = "";
         cmbavDefaultroleid_Jsonclick = "";
         lblTbdefaultrole_Visible = 1;
         cmbavDefaultsecuritypolicyid_Jsonclick = "";
         lblTbdefaultsecpolicy_Visible = 1;
         edtavRepositorycachetimeout_Jsonclick = "";
         edtavUsersessioncachetimeout_Jsonclick = "";
         edtavLoginattemptstolocksession_Jsonclick = "";
         edtavGamunblockusertimeout_Jsonclick = "";
         edtavLoginattemptstolockuser_Jsonclick = "";
         edtavMinimumamountcharactersinlogin_Jsonclick = "";
         edtavUserrecoverypasswordkeytimeout_Jsonclick = "";
         edtavUserremembermetimeout_Jsonclick = "";
         cmbavUserremembermetype_Jsonclick = "";
         cmbavGeneratesessionstatistics_Jsonclick = "";
         edtavUserautomaticactivationtimeout_Jsonclick = "";
         cmbavUseractivationmethod_Jsonclick = "";
         cmbavUseridentification_Jsonclick = "";
         cmbavDefaultauthtypename_Jsonclick = "";
         lblTbdefaultauthtype_Visible = 1;
         edtavDsc_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavNamespace_Jsonclick = "";
         edtavNamespace_Enabled = 1;
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 1;
         edtavId_Jsonclick = "";
         edtavId_Enabled = 1;
         cmbavDefaultroleid.Visible = 1;
         cmbavDefaultsecuritypolicyid.Visible = 1;
         cmbavDefaultauthtypename.Visible = 1;
         chkavRequiredlastname.Caption = "";
         chkavRequiredfirstname.Caption = "";
         chkavGiveanonymoussession.Caption = "";
         chkavRequiredpassword.Caption = "";
         chkavRequiredemail.Caption = "";
         chkavUseremailisunique.Caption = "";
         chkavAllowoauthaccess.Caption = "";
         chkavSessionexpiresonipchange.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Repository configuration ";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbformtitle_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9DefaultAuthTypeName = "";
         AV52UserIdentification = "";
         AV49UserActivationMethod = "";
         AV21GenerateSessionStatistics = "";
         AV55UserRememberMeType = "";
         AV23GUID = "";
         AV33NameSpace = "";
         AV32Name = "";
         AV12Dsc = "";
         AV14Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV7AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV26Language = "";
         AV6AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV46SecurityPolicies = new GxExternalCollection( context, "SdtGAMSecurityPolicy", "GeneXus.Programs");
         AV17FilterSecPol = new SdtGAMSecurityPolicyFilter(context);
         AV47SecurityPolicy = new SdtGAMSecurityPolicy(context);
         AV43Roles = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV16FilterRole = new SdtGAMRoleFilter(context);
         AV42Role = new SdtGAMRole(context);
         AV35Repository = new SdtGAMRepository(context);
         AV45SecurityAdministratorEmail = "";
         AV13Error = new SdtGAMError(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbid_Jsonclick = "";
         TempTags = "";
         lblTbguid_Jsonclick = "";
         lblTbnamespace_Jsonclick = "";
         lblTbname_Jsonclick = "";
         lblTbdsc_Jsonclick = "";
         lblTbdefaultauthtype_Jsonclick = "";
         lblTbsessionexpiresonipchange_Jsonclick = "";
         lblTballowoauthaccess_Jsonclick = "";
         lblTbuseridentifications_Jsonclick = "";
         lblTbuseractivarionmethod_Jsonclick = "";
         lblTbuserautomaticactivationtimeout_Jsonclick = "";
         lblTbuseremailisunique_Jsonclick = "";
         lblTbdsc9_Jsonclick = "";
         lblTbdrequiredpassword_Jsonclick = "";
         lblTbgeneratesessionstatistics_Jsonclick = "";
         lblTbuserremembermetype_Jsonclick = "";
         lblTbuserremembermetimeout_Jsonclick = "";
         lblTbuserrecoverypasswordkeytimeout_Jsonclick = "";
         lblTbminimumamountofcharactersinlogin_Jsonclick = "";
         lblTbloginretriestolockuser_Jsonclick = "";
         lblTbgamunblockusertimeout_Jsonclick = "";
         lblTbloginretriestolocksession_Jsonclick = "";
         lblTbgiveanonymoussession_Jsonclick = "";
         lblTbusersessioncachetimeout_Jsonclick = "";
         lblTbcachetimeout_Jsonclick = "";
         lblTbdefaultsecpolicy_Jsonclick = "";
         lblTbdefaultrole_Jsonclick = "";
         lblTbenabletracing_Jsonclick = "";
         lblTbrequiredfirstname_Jsonclick = "";
         lblTbrequiredlastname_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamrepositoryconfiguration__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavNamespace_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV57EnableTracing ;
      private short AV50UserAutomaticActivationTimeout ;
      private short AV54UserRememberMeTimeOut ;
      private short AV53UserRecoveryPasswordKeyTimeOut ;
      private short AV31MinimumAmountCharactersInLogin ;
      private short AV28LoginAttemptsToLockUser ;
      private short AV19GAMUnblockUserTimeout ;
      private short AV27LoginAttemptsToLockSession ;
      private short nGXWrapped ;
      private int AV11DefaultSecurityPolicyId ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavNamespace_Enabled ;
      private int AV56UserSessionCacheTimeout ;
      private int AV36RepositoryCacheTimeout ;
      private int lblTbdefaultauthtype_Visible ;
      private int lblTbdefaultsecpolicy_Visible ;
      private int lblTbdefaultrole_Visible ;
      private int AV60GXV1 ;
      private int AV61GXV2 ;
      private int AV62GXV3 ;
      private int AV63GXV4 ;
      private int idxLst ;
      private long AV34pId ;
      private long wcpOAV34pId ;
      private long AV10DefaultRoleId ;
      private long AV24Id ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbformtitle_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV9DefaultAuthTypeName ;
      private String chkavSessionexpiresonipchange_Internalname ;
      private String chkavAllowoauthaccess_Internalname ;
      private String AV52UserIdentification ;
      private String AV49UserActivationMethod ;
      private String chkavUseremailisunique_Internalname ;
      private String chkavRequiredemail_Internalname ;
      private String chkavRequiredpassword_Internalname ;
      private String AV21GenerateSessionStatistics ;
      private String AV55UserRememberMeType ;
      private String chkavGiveanonymoussession_Internalname ;
      private String chkavRequiredfirstname_Internalname ;
      private String chkavRequiredlastname_Internalname ;
      private String edtavId_Internalname ;
      private String edtavGuid_Internalname ;
      private String edtavNamespace_Internalname ;
      private String AV23GUID ;
      private String AV33NameSpace ;
      private String AV32Name ;
      private String edtavName_Internalname ;
      private String AV12Dsc ;
      private String edtavDsc_Internalname ;
      private String cmbavDefaultauthtypename_Internalname ;
      private String cmbavUseridentification_Internalname ;
      private String cmbavUseractivationmethod_Internalname ;
      private String edtavUserautomaticactivationtimeout_Internalname ;
      private String cmbavGeneratesessionstatistics_Internalname ;
      private String cmbavUserremembermetype_Internalname ;
      private String edtavUserremembermetimeout_Internalname ;
      private String edtavUserrecoverypasswordkeytimeout_Internalname ;
      private String edtavMinimumamountcharactersinlogin_Internalname ;
      private String edtavLoginattemptstolockuser_Internalname ;
      private String edtavGamunblockusertimeout_Internalname ;
      private String edtavLoginattemptstolocksession_Internalname ;
      private String edtavUsersessioncachetimeout_Internalname ;
      private String edtavRepositorycachetimeout_Internalname ;
      private String cmbavDefaultsecuritypolicyid_Internalname ;
      private String cmbavDefaultroleid_Internalname ;
      private String cmbavEnabletracing_Internalname ;
      private String lblTbdefaultauthtype_Internalname ;
      private String lblTbdefaultsecpolicy_Internalname ;
      private String lblTbdefaultrole_Internalname ;
      private String AV26Language ;
      private String sStyleString ;
      private String tblTbl_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbid_Internalname ;
      private String lblTbid_Jsonclick ;
      private String TempTags ;
      private String edtavId_Jsonclick ;
      private String lblTbguid_Internalname ;
      private String lblTbguid_Jsonclick ;
      private String edtavGuid_Jsonclick ;
      private String lblTbnamespace_Internalname ;
      private String lblTbnamespace_Jsonclick ;
      private String edtavNamespace_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbdsc_Internalname ;
      private String lblTbdsc_Jsonclick ;
      private String edtavDsc_Jsonclick ;
      private String lblTbdefaultauthtype_Jsonclick ;
      private String cmbavDefaultauthtypename_Jsonclick ;
      private String lblTbsessionexpiresonipchange_Internalname ;
      private String lblTbsessionexpiresonipchange_Jsonclick ;
      private String lblTballowoauthaccess_Internalname ;
      private String lblTballowoauthaccess_Jsonclick ;
      private String lblTbuseridentifications_Internalname ;
      private String lblTbuseridentifications_Jsonclick ;
      private String cmbavUseridentification_Jsonclick ;
      private String lblTbuseractivarionmethod_Internalname ;
      private String lblTbuseractivarionmethod_Jsonclick ;
      private String cmbavUseractivationmethod_Jsonclick ;
      private String lblTbuserautomaticactivationtimeout_Internalname ;
      private String lblTbuserautomaticactivationtimeout_Jsonclick ;
      private String edtavUserautomaticactivationtimeout_Jsonclick ;
      private String lblTbuseremailisunique_Internalname ;
      private String lblTbuseremailisunique_Jsonclick ;
      private String lblTbdsc9_Internalname ;
      private String lblTbdsc9_Jsonclick ;
      private String lblTbdrequiredpassword_Internalname ;
      private String lblTbdrequiredpassword_Jsonclick ;
      private String lblTbgeneratesessionstatistics_Internalname ;
      private String lblTbgeneratesessionstatistics_Jsonclick ;
      private String cmbavGeneratesessionstatistics_Jsonclick ;
      private String lblTbuserremembermetype_Internalname ;
      private String lblTbuserremembermetype_Jsonclick ;
      private String cmbavUserremembermetype_Jsonclick ;
      private String lblTbuserremembermetimeout_Internalname ;
      private String lblTbuserremembermetimeout_Jsonclick ;
      private String edtavUserremembermetimeout_Jsonclick ;
      private String lblTbuserrecoverypasswordkeytimeout_Internalname ;
      private String lblTbuserrecoverypasswordkeytimeout_Jsonclick ;
      private String edtavUserrecoverypasswordkeytimeout_Jsonclick ;
      private String lblTbminimumamountofcharactersinlogin_Internalname ;
      private String lblTbminimumamountofcharactersinlogin_Jsonclick ;
      private String edtavMinimumamountcharactersinlogin_Jsonclick ;
      private String lblTbloginretriestolockuser_Internalname ;
      private String lblTbloginretriestolockuser_Jsonclick ;
      private String edtavLoginattemptstolockuser_Jsonclick ;
      private String lblTbgamunblockusertimeout_Internalname ;
      private String lblTbgamunblockusertimeout_Jsonclick ;
      private String edtavGamunblockusertimeout_Jsonclick ;
      private String lblTbloginretriestolocksession_Internalname ;
      private String lblTbloginretriestolocksession_Jsonclick ;
      private String edtavLoginattemptstolocksession_Jsonclick ;
      private String lblTbgiveanonymoussession_Internalname ;
      private String lblTbgiveanonymoussession_Jsonclick ;
      private String lblTbusersessioncachetimeout_Internalname ;
      private String lblTbusersessioncachetimeout_Jsonclick ;
      private String edtavUsersessioncachetimeout_Jsonclick ;
      private String lblTbcachetimeout_Internalname ;
      private String lblTbcachetimeout_Jsonclick ;
      private String edtavRepositorycachetimeout_Jsonclick ;
      private String lblTbdefaultsecpolicy_Jsonclick ;
      private String cmbavDefaultsecuritypolicyid_Jsonclick ;
      private String lblTbdefaultrole_Jsonclick ;
      private String cmbavDefaultroleid_Jsonclick ;
      private String lblTbenabletracing_Internalname ;
      private String lblTbenabletracing_Jsonclick ;
      private String cmbavEnabletracing_Jsonclick ;
      private String lblTbrequiredfirstname_Internalname ;
      private String lblTbrequiredfirstname_Jsonclick ;
      private String lblTbrequiredlastname_Internalname ;
      private String lblTbrequiredlastname_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV48SessionExpiresOnIPChange ;
      private bool AV5AllowOauthAccess ;
      private bool AV51UserEmailisUnique ;
      private bool AV38RequiredEmail ;
      private bool AV41RequiredPassword ;
      private bool AV22GiveAnonymousSession ;
      private bool AV39RequiredFirstName ;
      private bool AV40RequiredLastName ;
      private bool returnInSub ;
      private bool AV25isLoginRepositoryAdm ;
      private bool AV8CanRegisterUsers ;
      private String AV45SecurityAdministratorEmail ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDefaultauthtypename ;
      private GXCheckbox chkavSessionexpiresonipchange ;
      private GXCheckbox chkavAllowoauthaccess ;
      private GXCombobox cmbavUseridentification ;
      private GXCombobox cmbavUseractivationmethod ;
      private GXCheckbox chkavUseremailisunique ;
      private GXCheckbox chkavRequiredemail ;
      private GXCheckbox chkavRequiredpassword ;
      private GXCombobox cmbavGeneratesessionstatistics ;
      private GXCombobox cmbavUserremembermetype ;
      private GXCheckbox chkavGiveanonymoussession ;
      private GXCombobox cmbavDefaultsecuritypolicyid ;
      private GXCombobox cmbavDefaultroleid ;
      private GXCombobox cmbavEnabletracing ;
      private GXCheckbox chkavRequiredfirstname ;
      private GXCheckbox chkavRequiredlastname ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationTypeSimple ))]
      private IGxCollection AV7AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV14Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMSecurityPolicy ))]
      private IGxCollection AV46SecurityPolicies ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV43Roles ;
      private GXWebForm Form ;
      private SdtGAMAuthenticationTypeSimple AV6AuthenticationType ;
      private SdtGAMError AV13Error ;
      private SdtGAMRoleFilter AV16FilterRole ;
      private SdtGAMSecurityPolicyFilter AV17FilterSecPol ;
      private SdtGAMRepository AV35Repository ;
      private SdtGAMSecurityPolicy AV47SecurityPolicy ;
      private SdtGAMRole AV42Role ;
   }

   public class gamrepositoryconfiguration__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
