/*
               File: ContratoServicos_TelasWC
        Description: Contrato Servicos_Telas WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:16:51.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicos_telaswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicos_telaswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicos_telaswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosTelas_ContratoCod )
      {
         this.AV7ContratoServicosTelas_ContratoCod = aP0_ContratoServicosTelas_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoServicosTelas_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicosTelas_ContratoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV19TFContratada_PessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
                  AV20TFContratada_PessoaNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratada_PessoaNom_Sel", AV20TFContratada_PessoaNom_Sel);
                  AV23TFContratoServicosTelas_Tela = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
                  AV24TFContratoServicosTelas_Tela_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosTelas_Tela_Sel", AV24TFContratoServicosTelas_Tela_Sel);
                  AV27TFContratoServicosTelas_Link = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
                  AV28TFContratoServicosTelas_Link_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosTelas_Link_Sel", AV28TFContratoServicosTelas_Link_Sel);
                  AV31TFContratoServicosTelas_Parms = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
                  AV32TFContratoServicosTelas_Parms_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContratoServicosTelas_Parms_Sel", AV32TFContratoServicosTelas_Parms_Sel);
                  AV7ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
                  AV21ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contratada_PessoaNomTitleControlIdToReplace", AV21ddo_Contratada_PessoaNomTitleControlIdToReplace);
                  AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
                  AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
                  AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
                  AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
                  AV46Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV36TFContratoServicosTelas_Status_Sels);
                  A926ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A938ContratoServicosTelas_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A938ContratoServicosTelas_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGB2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV46Pgmname = "ContratoServicos_TelasWC";
               context.Gx_err = 0;
               WSGB2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos_Telas WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031523165191");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicos_telaswc.aspx") + "?" + UrlEncode("" +AV7ContratoServicosTelas_ContratoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV19TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA", StringUtil.RTrim( AV23TFContratoServicosTelas_Tela));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL", StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK", AV27TFContratoServicosTelas_Link);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL", AV28TFContratoServicosTelas_Link_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS", AV31TFContratoServicosTelas_Parms);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL", AV32TFContratoServicosTelas_Parms_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV38DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV18Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV18Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA", AV22ContratoServicosTelas_TelaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA", AV22ContratoServicosTelas_TelaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA", AV26ContratoServicosTelas_LinkTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA", AV26ContratoServicosTelas_LinkTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA", AV30ContratoServicosTelas_ParmsTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA", AV30ContratoServicosTelas_ParmsTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA", AV34ContratoServicosTelas_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA", AV34ContratoServicosTelas_StatusTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSTELAS_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV46Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFCONTRATOSERVICOSTELAS_STATUS_SELS", AV36TFContratoServicosTelas_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFCONTRATOSERVICOSTELAS_STATUS_SELS", AV36TFContratoServicosTelas_Status_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_tela_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_link_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_link_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_link_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_link_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_link_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_link_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_link_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_link_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_link_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_link_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_link_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_link_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filtertype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_parms_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistproc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Loadingdata", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Caption", StringUtil.RTrim( Ddo_contratoservicostelas_status_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Tooltip", StringUtil.RTrim( Ddo_contratoservicostelas_status_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Cls", StringUtil.RTrim( Ddo_contratoservicostelas_status_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicostelas_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicostelas_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicostelas_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Datalisttype", StringUtil.RTrim( Ddo_contratoservicostelas_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratoservicostelas_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratoservicostelas_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortasc", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicostelas_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicostelas_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicostelas_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_tela_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_link_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_link_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_link_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_parms_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicostelas_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicostelas_status_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGB2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicos_telaswc.js", "?202031523165326");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicos_TelasWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos_Telas WC" ;
      }

      protected void WBGB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicos_telaswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_GB2( true) ;
         }
         else
         {
            wb_table1_2_GB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV19TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV19TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV20TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_tela_Internalname, StringUtil.RTrim( AV23TFContratoServicosTelas_Tela), StringUtil.RTrim( context.localUtil.Format( AV23TFContratoServicosTelas_Tela, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_tela_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_tela_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_tela_sel_Internalname, StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel), StringUtil.RTrim( context.localUtil.Format( AV24TFContratoServicosTelas_Tela_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_tela_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_tela_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_link_Internalname, AV27TFContratoServicosTelas_Link, StringUtil.RTrim( context.localUtil.Format( AV27TFContratoServicosTelas_Link, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_link_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_link_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicostelas_link_sel_Internalname, AV28TFContratoServicosTelas_Link_Sel, StringUtil.RTrim( context.localUtil.Format( AV28TFContratoServicosTelas_Link_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicostelas_link_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicostelas_link_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicos_TelasWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicostelas_parms_Internalname, AV31TFContratoServicosTelas_Parms, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", 0, edtavTfcontratoservicostelas_parms_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicostelas_parms_sel_Internalname, AV32TFContratoServicosTelas_Parms_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, edtavTfcontratoservicostelas_parms_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINKContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicos_TelasWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTGB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos_Telas WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGB0( ) ;
            }
         }
      }

      protected void WSGB2( )
      {
         STARTGB2( ) ;
         EVTGB2( ) ;
      }

      protected void EVTGB2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GB2 */
                                    E11GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GB2 */
                                    E12GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_TELA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GB2 */
                                    E13GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_LINK.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GB2 */
                                    E14GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_PARMS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15GB2 */
                                    E15GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSTELAS_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16GB2 */
                                    E16GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17GB2 */
                                    E17GB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGB0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV44Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV45Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              A926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosTelas_ContratoCod_Internalname), ",", "."));
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A931ContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtContratoServicosTelas_Tela_Internalname));
                              A928ContratoServicosTelas_Link = cgiGet( edtContratoServicosTelas_Link_Internalname);
                              A929ContratoServicosTelas_Parms = cgiGet( edtContratoServicosTelas_Parms_Internalname);
                              n929ContratoServicosTelas_Parms = false;
                              cmbContratoServicosTelas_Status.Name = cmbContratoServicosTelas_Status_Internalname;
                              cmbContratoServicosTelas_Status.CurrentValue = cgiGet( cmbContratoServicosTelas_Status_Internalname);
                              A932ContratoServicosTelas_Status = cgiGet( cmbContratoServicosTelas_Status_Internalname);
                              n932ContratoServicosTelas_Status = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18GB2 */
                                          E18GB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19GB2 */
                                          E19GB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20GB2 */
                                          E20GB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratada_pessoanom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADA_PESSOANOM"), AV19TFContratada_PessoaNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV20TFContratada_PessoaNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_tela Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA"), AV23TFContratoServicosTelas_Tela) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_tela_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL"), AV24TFContratoServicosTelas_Tela_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_link Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK"), AV27TFContratoServicosTelas_Link) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_link_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL"), AV28TFContratoServicosTelas_Link_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_parms Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS"), AV31TFContratoServicosTelas_Parms) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicostelas_parms_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL"), AV32TFContratoServicosTelas_Parms_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPGB0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGB2( ) ;
            }
         }
      }

      protected void PAGB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTRATOSERVICOSTELAS_STATUS_" + sGXsfl_8_idx;
            cmbContratoServicosTelas_Status.Name = GXCCtl;
            cmbContratoServicosTelas_Status.WebTags = "";
            cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
            cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
            cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
            cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
            cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
            cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
            cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
            cmbContratoServicosTelas_Status.addItem("D", "Rejeitada", 0);
            cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
            cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
            cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
            cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
            cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
            cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
            cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
            cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
            cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
            if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
            {
               A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
               n932ContratoServicosTelas_Status = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV19TFContratada_PessoaNom ,
                                       String AV20TFContratada_PessoaNom_Sel ,
                                       String AV23TFContratoServicosTelas_Tela ,
                                       String AV24TFContratoServicosTelas_Tela_Sel ,
                                       String AV27TFContratoServicosTelas_Link ,
                                       String AV28TFContratoServicosTelas_Link_Sel ,
                                       String AV31TFContratoServicosTelas_Parms ,
                                       String AV32TFContratoServicosTelas_Parms_Sel ,
                                       int AV7ContratoServicosTelas_ContratoCod ,
                                       String AV21ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace ,
                                       String AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace ,
                                       String AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace ,
                                       String AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace ,
                                       String AV46Pgmname ,
                                       IGxCollection AV36TFContratoServicosTelas_Status_Sels ,
                                       int A926ContratoServicosTelas_ContratoCod ,
                                       short A938ContratoServicosTelas_Sequencial ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGB2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_CONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_TELA", StringUtil.RTrim( A931ContratoServicosTelas_Tela));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_LINK", A928ContratoServicosTelas_Link);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_PARMS", GetSecureSignedToken( sPrefix, A929ContratoServicosTelas_Parms));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_PARMS", A929ContratoServicosTelas_Parms);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_STATUS", StringUtil.RTrim( A932ContratoServicosTelas_Status));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV46Pgmname = "ContratoServicos_TelasWC";
         context.Gx_err = 0;
      }

      protected void RFGB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E19GB2 */
         E19GB2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A932ContratoServicosTelas_Status ,
                                                 AV36TFContratoServicosTelas_Status_Sels ,
                                                 AV20TFContratada_PessoaNom_Sel ,
                                                 AV19TFContratada_PessoaNom ,
                                                 AV24TFContratoServicosTelas_Tela_Sel ,
                                                 AV23TFContratoServicosTelas_Tela ,
                                                 AV28TFContratoServicosTelas_Link_Sel ,
                                                 AV27TFContratoServicosTelas_Link ,
                                                 AV32TFContratoServicosTelas_Parms_Sel ,
                                                 AV31TFContratoServicosTelas_Parms ,
                                                 AV36TFContratoServicosTelas_Status_Sels.Count ,
                                                 A41Contratada_PessoaNom ,
                                                 A931ContratoServicosTelas_Tela ,
                                                 A928ContratoServicosTelas_Link ,
                                                 A929ContratoServicosTelas_Parms ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A926ContratoServicosTelas_ContratoCod ,
                                                 AV7ContratoServicosTelas_ContratoCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV19TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV19TFContratada_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
            lV23TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV23TFContratoServicosTelas_Tela), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
            lV27TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV27TFContratoServicosTelas_Link), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
            lV31TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV31TFContratoServicosTelas_Parms), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
            /* Using cursor H00GB2 */
            pr_default.execute(0, new Object[] {AV7ContratoServicosTelas_ContratoCod, lV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, lV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, lV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, lV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H00GB2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00GB2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = H00GB2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00GB2_A40Contratada_PessoaCod[0];
               A938ContratoServicosTelas_Sequencial = H00GB2_A938ContratoServicosTelas_Sequencial[0];
               A932ContratoServicosTelas_Status = H00GB2_A932ContratoServicosTelas_Status[0];
               n932ContratoServicosTelas_Status = H00GB2_n932ContratoServicosTelas_Status[0];
               A929ContratoServicosTelas_Parms = H00GB2_A929ContratoServicosTelas_Parms[0];
               n929ContratoServicosTelas_Parms = H00GB2_n929ContratoServicosTelas_Parms[0];
               A928ContratoServicosTelas_Link = H00GB2_A928ContratoServicosTelas_Link[0];
               A931ContratoServicosTelas_Tela = H00GB2_A931ContratoServicosTelas_Tela[0];
               A41Contratada_PessoaNom = H00GB2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00GB2_n41Contratada_PessoaNom[0];
               A926ContratoServicosTelas_ContratoCod = H00GB2_A926ContratoServicosTelas_ContratoCod[0];
               A74Contrato_Codigo = H00GB2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00GB2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = H00GB2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00GB2_A40Contratada_PessoaCod[0];
               A41Contratada_PessoaNom = H00GB2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00GB2_n41Contratada_PessoaNom[0];
               /* Execute user event: E20GB2 */
               E20GB2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBGB0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV36TFContratoServicosTelas_Status_Sels ,
                                              AV20TFContratada_PessoaNom_Sel ,
                                              AV19TFContratada_PessoaNom ,
                                              AV24TFContratoServicosTelas_Tela_Sel ,
                                              AV23TFContratoServicosTelas_Tela ,
                                              AV28TFContratoServicosTelas_Link_Sel ,
                                              AV27TFContratoServicosTelas_Link ,
                                              AV32TFContratoServicosTelas_Parms_Sel ,
                                              AV31TFContratoServicosTelas_Parms ,
                                              AV36TFContratoServicosTelas_Status_Sels.Count ,
                                              A41Contratada_PessoaNom ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A926ContratoServicosTelas_ContratoCod ,
                                              AV7ContratoServicosTelas_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV19TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV19TFContratada_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
         lV23TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV23TFContratoServicosTelas_Tela), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
         lV27TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV27TFContratoServicosTelas_Link), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
         lV31TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV31TFContratoServicosTelas_Parms), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
         /* Using cursor H00GB3 */
         pr_default.execute(1, new Object[] {AV7ContratoServicosTelas_ContratoCod, lV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, lV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, lV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, lV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel});
         GRID_nRecordCount = H00GB3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV19TFContratada_PessoaNom, AV20TFContratada_PessoaNom_Sel, AV23TFContratoServicosTelas_Tela, AV24TFContratoServicosTelas_Tela_Sel, AV27TFContratoServicosTelas_Link, AV28TFContratoServicosTelas_Link_Sel, AV31TFContratoServicosTelas_Parms, AV32TFContratoServicosTelas_Parms_Sel, AV7ContratoServicosTelas_ContratoCod, AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, AV46Pgmname, AV36TFContratoServicosTelas_Status_Sels, A926ContratoServicosTelas_ContratoCod, A938ContratoServicosTelas_Sequencial, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGB0( )
      {
         /* Before Start, stand alone formulas. */
         AV46Pgmname = "ContratoServicos_TelasWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E18GB2 */
         E18GB2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV38DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV18Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA"), AV22ContratoServicosTelas_TelaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA"), AV26ContratoServicosTelas_LinkTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA"), AV30ContratoServicosTelas_ParmsTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA"), AV34ContratoServicosTelas_StatusTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV19TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
            AV20TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratada_PessoaNom_Sel", AV20TFContratada_PessoaNom_Sel);
            AV23TFContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_tela_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
            AV24TFContratoServicosTelas_Tela_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicostelas_tela_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosTelas_Tela_Sel", AV24TFContratoServicosTelas_Tela_Sel);
            AV27TFContratoServicosTelas_Link = cgiGet( edtavTfcontratoservicostelas_link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
            AV28TFContratoServicosTelas_Link_Sel = cgiGet( edtavTfcontratoservicostelas_link_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosTelas_Link_Sel", AV28TFContratoServicosTelas_Link_Sel);
            AV31TFContratoServicosTelas_Parms = cgiGet( edtavTfcontratoservicostelas_parms_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
            AV32TFContratoServicosTelas_Parms_Sel = cgiGet( edtavTfcontratoservicostelas_parms_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContratoServicosTelas_Parms_Sel", AV32TFContratoServicosTelas_Parms_Sel);
            AV21ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contratada_PessoaNomTitleControlIdToReplace", AV21ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
            AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
            AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
            AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosTelas_ContratoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratada_pessoanom_Caption = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratoservicostelas_tela_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Caption");
            Ddo_contratoservicostelas_tela_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Tooltip");
            Ddo_contratoservicostelas_tela_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Cls");
            Ddo_contratoservicostelas_tela_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_set");
            Ddo_contratoservicostelas_tela_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_set");
            Ddo_contratoservicostelas_tela_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Dropdownoptionstype");
            Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_tela_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includesortasc"));
            Ddo_contratoservicostelas_tela_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includesortdsc"));
            Ddo_contratoservicostelas_tela_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortedstatus");
            Ddo_contratoservicostelas_tela_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includefilter"));
            Ddo_contratoservicostelas_tela_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filtertype");
            Ddo_contratoservicostelas_tela_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filterisrange"));
            Ddo_contratoservicostelas_tela_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Includedatalist"));
            Ddo_contratoservicostelas_tela_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalisttype");
            Ddo_contratoservicostelas_tela_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalistproc");
            Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_tela_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortasc");
            Ddo_contratoservicostelas_tela_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Sortdsc");
            Ddo_contratoservicostelas_tela_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Loadingdata");
            Ddo_contratoservicostelas_tela_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Cleanfilter");
            Ddo_contratoservicostelas_tela_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Noresultsfound");
            Ddo_contratoservicostelas_tela_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Searchbuttontext");
            Ddo_contratoservicostelas_link_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Caption");
            Ddo_contratoservicostelas_link_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Tooltip");
            Ddo_contratoservicostelas_link_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Cls");
            Ddo_contratoservicostelas_link_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_set");
            Ddo_contratoservicostelas_link_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_set");
            Ddo_contratoservicostelas_link_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Dropdownoptionstype");
            Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_link_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includesortasc"));
            Ddo_contratoservicostelas_link_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includesortdsc"));
            Ddo_contratoservicostelas_link_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortedstatus");
            Ddo_contratoservicostelas_link_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includefilter"));
            Ddo_contratoservicostelas_link_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filtertype");
            Ddo_contratoservicostelas_link_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filterisrange"));
            Ddo_contratoservicostelas_link_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Includedatalist"));
            Ddo_contratoservicostelas_link_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalisttype");
            Ddo_contratoservicostelas_link_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalistproc");
            Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_link_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortasc");
            Ddo_contratoservicostelas_link_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Sortdsc");
            Ddo_contratoservicostelas_link_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Loadingdata");
            Ddo_contratoservicostelas_link_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Cleanfilter");
            Ddo_contratoservicostelas_link_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Noresultsfound");
            Ddo_contratoservicostelas_link_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Searchbuttontext");
            Ddo_contratoservicostelas_parms_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Caption");
            Ddo_contratoservicostelas_parms_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Tooltip");
            Ddo_contratoservicostelas_parms_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Cls");
            Ddo_contratoservicostelas_parms_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_set");
            Ddo_contratoservicostelas_parms_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_set");
            Ddo_contratoservicostelas_parms_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Dropdownoptionstype");
            Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_parms_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortasc"));
            Ddo_contratoservicostelas_parms_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includesortdsc"));
            Ddo_contratoservicostelas_parms_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortedstatus");
            Ddo_contratoservicostelas_parms_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includefilter"));
            Ddo_contratoservicostelas_parms_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filtertype");
            Ddo_contratoservicostelas_parms_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filterisrange"));
            Ddo_contratoservicostelas_parms_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Includedatalist"));
            Ddo_contratoservicostelas_parms_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalisttype");
            Ddo_contratoservicostelas_parms_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistproc");
            Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicostelas_parms_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortasc");
            Ddo_contratoservicostelas_parms_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Sortdsc");
            Ddo_contratoservicostelas_parms_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Loadingdata");
            Ddo_contratoservicostelas_parms_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Cleanfilter");
            Ddo_contratoservicostelas_parms_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Noresultsfound");
            Ddo_contratoservicostelas_parms_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Searchbuttontext");
            Ddo_contratoservicostelas_status_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Caption");
            Ddo_contratoservicostelas_status_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Tooltip");
            Ddo_contratoservicostelas_status_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Cls");
            Ddo_contratoservicostelas_status_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_set");
            Ddo_contratoservicostelas_status_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Dropdownoptionstype");
            Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Titlecontrolidtoreplace");
            Ddo_contratoservicostelas_status_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortasc"));
            Ddo_contratoservicostelas_status_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includesortdsc"));
            Ddo_contratoservicostelas_status_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortedstatus");
            Ddo_contratoservicostelas_status_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includefilter"));
            Ddo_contratoservicostelas_status_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Includedatalist"));
            Ddo_contratoservicostelas_status_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Datalisttype");
            Ddo_contratoservicostelas_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Allowmultipleselection"));
            Ddo_contratoservicostelas_status_Datalistfixedvalues = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Datalistfixedvalues");
            Ddo_contratoservicostelas_status_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortasc");
            Ddo_contratoservicostelas_status_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Sortdsc");
            Ddo_contratoservicostelas_status_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Cleanfilter");
            Ddo_contratoservicostelas_status_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratoservicostelas_tela_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Activeeventkey");
            Ddo_contratoservicostelas_tela_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Filteredtext_get");
            Ddo_contratoservicostelas_tela_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA_Selectedvalue_get");
            Ddo_contratoservicostelas_link_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Activeeventkey");
            Ddo_contratoservicostelas_link_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Filteredtext_get");
            Ddo_contratoservicostelas_link_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK_Selectedvalue_get");
            Ddo_contratoservicostelas_parms_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Activeeventkey");
            Ddo_contratoservicostelas_parms_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Filteredtext_get");
            Ddo_contratoservicostelas_parms_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS_Selectedvalue_get");
            Ddo_contratoservicostelas_status_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Activeeventkey");
            Ddo_contratoservicostelas_status_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADA_PESSOANOM"), AV19TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV20TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA"), AV23TFContratoServicosTelas_Tela) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_TELA_SEL"), AV24TFContratoServicosTelas_Tela_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK"), AV27TFContratoServicosTelas_Link) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_LINK_SEL"), AV28TFContratoServicosTelas_Link_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS"), AV31TFContratoServicosTelas_Parms) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSTELAS_PARMS_SEL"), AV32TFContratoServicosTelas_Parms_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E18GB2 */
         E18GB2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18GB2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_tela_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_tela_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_tela_Visible), 5, 0)));
         edtavTfcontratoservicostelas_tela_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_tela_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_tela_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_link_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_link_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_link_Visible), 5, 0)));
         edtavTfcontratoservicostelas_link_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_link_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_link_sel_Visible), 5, 0)));
         edtavTfcontratoservicostelas_parms_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_parms_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_parms_Visible), 5, 0)));
         edtavTfcontratoservicostelas_parms_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicostelas_parms_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicostelas_parms_sel_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV21ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_Contratada_PessoaNomTitleControlIdToReplace", AV21ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Tela";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace);
         AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace", AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Link";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_link_Titlecontrolidtoreplace);
         AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = Ddo_contratoservicostelas_link_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace", AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Parms";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace);
         AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace", AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosTelas_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "TitleControlIdToReplace", Ddo_contratoservicostelas_status_Titlecontrolidtoreplace);
         AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = Ddo_contratoservicostelas_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace", AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace);
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV38DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV38DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E19GB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV18Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ContratoServicosTelas_TelaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContratoServicosTelas_LinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContratoServicosTelas_ParmsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosTelas_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV21ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratoServicosTelas_Tela_Titleformat = 2;
         edtContratoServicosTelas_Tela_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tela", AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosTelas_Tela_Internalname, "Title", edtContratoServicosTelas_Tela_Title);
         edtContratoServicosTelas_Link_Titleformat = 2;
         edtContratoServicosTelas_Link_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Link", AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosTelas_Link_Internalname, "Title", edtContratoServicosTelas_Link_Title);
         edtContratoServicosTelas_Parms_Titleformat = 2;
         edtContratoServicosTelas_Parms_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Par�metros", AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosTelas_Parms_Internalname, "Title", edtContratoServicosTelas_Parms_Title);
         cmbContratoServicosTelas_Status_Titleformat = 2;
         cmbContratoServicosTelas_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosTelas_Status_Internalname, "Title", cmbContratoServicosTelas_Status.Title.Text);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV18Contratada_PessoaNomTitleFilterData", AV18Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22ContratoServicosTelas_TelaTitleFilterData", AV22ContratoServicosTelas_TelaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26ContratoServicosTelas_LinkTitleFilterData", AV26ContratoServicosTelas_LinkTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV30ContratoServicosTelas_ParmsTitleFilterData", AV30ContratoServicosTelas_ParmsTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV34ContratoServicosTelas_StatusTitleFilterData", AV34ContratoServicosTelas_StatusTitleFilterData);
      }

      protected void E11GB2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      protected void E12GB2( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV19TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
            AV20TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratada_PessoaNom_Sel", AV20TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E13GB2( )
      {
         /* Ddo_contratoservicostelas_tela_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_tela_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_tela_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_tela_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFContratoServicosTelas_Tela = Ddo_contratoservicostelas_tela_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
            AV24TFContratoServicosTelas_Tela_Sel = Ddo_contratoservicostelas_tela_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosTelas_Tela_Sel", AV24TFContratoServicosTelas_Tela_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14GB2( )
      {
         /* Ddo_contratoservicostelas_link_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_link_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_link_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_link_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFContratoServicosTelas_Link = Ddo_contratoservicostelas_link_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
            AV28TFContratoServicosTelas_Link_Sel = Ddo_contratoservicostelas_link_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosTelas_Link_Sel", AV28TFContratoServicosTelas_Link_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E15GB2( )
      {
         /* Ddo_contratoservicostelas_parms_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_parms_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_parms_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_parms_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoServicosTelas_Parms = Ddo_contratoservicostelas_parms_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
            AV32TFContratoServicosTelas_Parms_Sel = Ddo_contratoservicostelas_parms_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContratoServicosTelas_Parms_Sel", AV32TFContratoServicosTelas_Parms_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E16GB2( )
      {
         /* Ddo_contratoservicostelas_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicostelas_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicostelas_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContratoServicosTelas_Status_SelsJson = Ddo_contratoservicostelas_status_Selectedvalue_get;
            AV36TFContratoServicosTelas_Status_Sels.FromJSonString(AV35TFContratoServicosTelas_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36TFContratoServicosTelas_Status_Sels", AV36TFContratoServicosTelas_Status_Sels);
      }

      private void E20GB2( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV44Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A926ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +A938ContratoServicosTelas_Sequencial);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV45Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A926ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +A938ContratoServicosTelas_Sequencial);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E17GB2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +AV7ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratoservicostelas_tela_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
         Ddo_contratoservicostelas_link_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
         Ddo_contratoservicostelas_parms_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
         Ddo_contratoservicostelas_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicostelas_tela_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "SortedStatus", Ddo_contratoservicostelas_tela_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicostelas_link_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "SortedStatus", Ddo_contratoservicostelas_link_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicostelas_parms_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "SortedStatus", Ddo_contratoservicostelas_parms_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoservicostelas_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "SortedStatus", Ddo_contratoservicostelas_status_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV46Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV46Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV46Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV19TFContratada_PessoaNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratada_PessoaNom", AV19TFContratada_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaNom)) )
               {
                  Ddo_contratada_pessoanom_Filteredtext_set = AV19TFContratada_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV20TFContratada_PessoaNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratada_PessoaNom_Sel", AV20TFContratada_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) )
               {
                  Ddo_contratada_pessoanom_Selectedvalue_set = AV20TFContratada_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA") == 0 )
            {
               AV23TFContratoServicosTelas_Tela = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosTelas_Tela", AV23TFContratoServicosTelas_Tela);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosTelas_Tela)) )
               {
                  Ddo_contratoservicostelas_tela_Filteredtext_set = AV23TFContratoServicosTelas_Tela;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "FilteredText_set", Ddo_contratoservicostelas_tela_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA_SEL") == 0 )
            {
               AV24TFContratoServicosTelas_Tela_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosTelas_Tela_Sel", AV24TFContratoServicosTelas_Tela_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) )
               {
                  Ddo_contratoservicostelas_tela_Selectedvalue_set = AV24TFContratoServicosTelas_Tela_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_tela_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_tela_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK") == 0 )
            {
               AV27TFContratoServicosTelas_Link = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosTelas_Link", AV27TFContratoServicosTelas_Link);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosTelas_Link)) )
               {
                  Ddo_contratoservicostelas_link_Filteredtext_set = AV27TFContratoServicosTelas_Link;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "FilteredText_set", Ddo_contratoservicostelas_link_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK_SEL") == 0 )
            {
               AV28TFContratoServicosTelas_Link_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosTelas_Link_Sel", AV28TFContratoServicosTelas_Link_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) )
               {
                  Ddo_contratoservicostelas_link_Selectedvalue_set = AV28TFContratoServicosTelas_Link_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_link_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_link_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS") == 0 )
            {
               AV31TFContratoServicosTelas_Parms = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosTelas_Parms", AV31TFContratoServicosTelas_Parms);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoServicosTelas_Parms)) )
               {
                  Ddo_contratoservicostelas_parms_Filteredtext_set = AV31TFContratoServicosTelas_Parms;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "FilteredText_set", Ddo_contratoservicostelas_parms_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS_SEL") == 0 )
            {
               AV32TFContratoServicosTelas_Parms_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32TFContratoServicosTelas_Parms_Sel", AV32TFContratoServicosTelas_Parms_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) )
               {
                  Ddo_contratoservicostelas_parms_Selectedvalue_set = AV32TFContratoServicosTelas_Parms_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_parms_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_parms_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_STATUS_SEL") == 0 )
            {
               AV35TFContratoServicosTelas_Status_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV36TFContratoServicosTelas_Status_Sels.FromJSonString(AV35TFContratoServicosTelas_Status_SelsJson);
               if ( ! ( AV36TFContratoServicosTelas_Status_Sels.Count == 0 ) )
               {
                  Ddo_contratoservicostelas_status_Selectedvalue_set = AV35TFContratoServicosTelas_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicostelas_status_Internalname, "SelectedValue_set", Ddo_contratoservicostelas_status_Selectedvalue_set);
               }
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV46Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV12GridStateFilterValue.gxTpr_Value = AV19TFContratada_PessoaNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV20TFContratada_PessoaNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosTelas_Tela)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_TELA";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFContratoServicosTelas_Tela;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_TELA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV24TFContratoServicosTelas_Tela_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosTelas_Link)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_LINK";
            AV12GridStateFilterValue.gxTpr_Value = AV27TFContratoServicosTelas_Link;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_LINK_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV28TFContratoServicosTelas_Link_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoServicosTelas_Parms)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_PARMS";
            AV12GridStateFilterValue.gxTpr_Value = AV31TFContratoServicosTelas_Parms;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_PARMS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV32TFContratoServicosTelas_Parms_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV36TFContratoServicosTelas_Status_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSTELAS_STATUS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFContratoServicosTelas_Status_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ContratoServicosTelas_ContratoCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOSERVICOSTELAS_CONTRATOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV46Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV46Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosTelas";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosTelas_ContratoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_GB2( true) ;
         }
         else
         {
            wb_table2_5_GB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicos_TelasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GB2e( true) ;
         }
         else
         {
            wb_table1_2_GB2e( false) ;
         }
      }

      protected void wb_table2_5_GB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Tela_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Tela_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Tela_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Link_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Link_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Link_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosTelas_Parms_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosTelas_Parms_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosTelas_Parms_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoServicosTelas_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoServicosTelas_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoServicosTelas_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A931ContratoServicosTelas_Tela));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Tela_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Tela_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A928ContratoServicosTelas_Link);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Link_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Link_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A929ContratoServicosTelas_Parms);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosTelas_Parms_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosTelas_Parms_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A932ContratoServicosTelas_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoServicosTelas_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoServicosTelas_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GB2e( true) ;
         }
         else
         {
            wb_table2_5_GB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicosTelas_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGB2( ) ;
         WSGB2( ) ;
         WEGB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicosTelas_ContratoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGB2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicos_telaswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGB2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicosTelas_ContratoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
         }
         wcpOAV7ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosTelas_ContratoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicosTelas_ContratoCod != wcpOAV7ContratoServicosTelas_ContratoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicosTelas_ContratoCod = AV7ContratoServicosTelas_ContratoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicosTelas_ContratoCod = cgiGet( sPrefix+"AV7ContratoServicosTelas_ContratoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicosTelas_ContratoCod) > 0 )
         {
            AV7ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicosTelas_ContratoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0)));
         }
         else
         {
            AV7ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicosTelas_ContratoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGB2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGB2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosTelas_ContratoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicosTelas_ContratoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosTelas_ContratoCod_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicosTelas_ContratoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031523165674");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicos_telaswc.js", "?202031523165674");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtContratoServicosTelas_ContratoCod_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_CONTRATOCOD_"+sGXsfl_8_idx;
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM_"+sGXsfl_8_idx;
         edtContratoServicosTelas_Tela_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_TELA_"+sGXsfl_8_idx;
         edtContratoServicosTelas_Link_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_LINK_"+sGXsfl_8_idx;
         edtContratoServicosTelas_Parms_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_PARMS_"+sGXsfl_8_idx;
         cmbContratoServicosTelas_Status_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_STATUS_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtContratoServicosTelas_ContratoCod_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_CONTRATOCOD_"+sGXsfl_8_fel_idx;
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM_"+sGXsfl_8_fel_idx;
         edtContratoServicosTelas_Tela_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_TELA_"+sGXsfl_8_fel_idx;
         edtContratoServicosTelas_Link_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_LINK_"+sGXsfl_8_fel_idx;
         edtContratoServicosTelas_Parms_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_PARMS_"+sGXsfl_8_fel_idx;
         cmbContratoServicosTelas_Status_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_STATUS_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBGB0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV44Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV45Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV45Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Tela_Internalname,StringUtil.RTrim( A931ContratoServicosTelas_Tela),StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Tela_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Link_Internalname,(String)A928ContratoServicosTelas_Link,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Link_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"LinkMenu",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosTelas_Parms_Internalname,(String)A929ContratoServicosTelas_Parms,(String)A929ContratoServicosTelas_Parms,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosTelas_Parms_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)0,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSERVICOSTELAS_STATUS_" + sGXsfl_8_idx;
               cmbContratoServicosTelas_Status.Name = GXCCtl;
               cmbContratoServicosTelas_Status.WebTags = "";
               cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
               cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
               cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
               cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
               cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
               cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
               cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
               cmbContratoServicosTelas_Status.addItem("D", "Rejeitada", 0);
               cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
               cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
               cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
               cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
               cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
               cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
               cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
               cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
               cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
               cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
               cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
               if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
               {
                  A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
                  n932ContratoServicosTelas_Status = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoServicosTelas_Status,(String)cmbContratoServicosTelas_Status_Internalname,StringUtil.RTrim( A932ContratoServicosTelas_Status),(short)1,(String)cmbContratoServicosTelas_Status_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoServicosTelas_Status.CurrentValue = StringUtil.RTrim( A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosTelas_Status_Internalname, "Values", (String)(cmbContratoServicosTelas_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_CONTRATOCOD"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_TELA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_LINK"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_PARMS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A929ContratoServicosTelas_Parms));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_STATUS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratoServicosTelas_ContratoCod_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_CONTRATOCOD";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         edtContratoServicosTelas_Tela_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_TELA";
         edtContratoServicosTelas_Link_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_LINK";
         edtContratoServicosTelas_Parms_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_PARMS";
         cmbContratoServicosTelas_Status_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_STATUS";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratada_pessoanom_Internalname = sPrefix+"vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = sPrefix+"vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratoservicostelas_tela_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_TELA";
         edtavTfcontratoservicostelas_tela_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_TELA_SEL";
         edtavTfcontratoservicostelas_link_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_LINK";
         edtavTfcontratoservicostelas_link_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_LINK_SEL";
         edtavTfcontratoservicostelas_parms_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_PARMS";
         edtavTfcontratoservicostelas_parms_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSTELAS_PARMS_SEL";
         Ddo_contratada_pessoanom_Internalname = sPrefix+"DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_tela_Internalname = sPrefix+"DDO_CONTRATOSERVICOSTELAS_TELA";
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_link_Internalname = sPrefix+"DDO_CONTRATOSERVICOSTELAS_LINK";
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_parms_Internalname = sPrefix+"DDO_CONTRATOSERVICOSTELAS_PARMS";
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicostelas_status_Internalname = sPrefix+"DDO_CONTRATOSERVICOSTELAS_STATUS";
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContratoServicosTelas_Status_Jsonclick = "";
         edtContratoServicosTelas_Parms_Jsonclick = "";
         edtContratoServicosTelas_Link_Jsonclick = "";
         edtContratoServicosTelas_Tela_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratoServicosTelas_ContratoCod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbContratoServicosTelas_Status_Titleformat = 0;
         edtContratoServicosTelas_Parms_Titleformat = 0;
         edtContratoServicosTelas_Link_Titleformat = 0;
         edtContratoServicosTelas_Tela_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         cmbContratoServicosTelas_Status.Title.Text = "Status";
         edtContratoServicosTelas_Parms_Title = "Par�metros";
         edtContratoServicosTelas_Link_Title = "Link";
         edtContratoServicosTelas_Tela_Title = "Tela";
         edtContratada_PessoaNom_Title = "Contratada";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicostelas_parms_sel_Visible = 1;
         edtavTfcontratoservicostelas_parms_Visible = 1;
         edtavTfcontratoservicostelas_link_sel_Jsonclick = "";
         edtavTfcontratoservicostelas_link_sel_Visible = 1;
         edtavTfcontratoservicostelas_link_Jsonclick = "";
         edtavTfcontratoservicostelas_link_Visible = 1;
         edtavTfcontratoservicostelas_tela_sel_Jsonclick = "";
         edtavTfcontratoservicostelas_tela_sel_Visible = 1;
         edtavTfcontratoservicostelas_tela_Jsonclick = "";
         edtavTfcontratoservicostelas_tela_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_contratoservicostelas_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratoservicostelas_status_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_status_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_status_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_status_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratoservicostelas_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Datalisttype = "FixedValues";
         Ddo_contratoservicostelas_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_status_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_status_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_status_Tooltip = "Op��es";
         Ddo_contratoservicostelas_status_Caption = "";
         Ddo_contratoservicostelas_parms_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_parms_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_parms_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_parms_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_parms_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_parms_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_parms_Datalistproc = "GetContratoServicos_TelasWCFilterData";
         Ddo_contratoservicostelas_parms_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_parms_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_parms_Filtertype = "Character";
         Ddo_contratoservicostelas_parms_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_parms_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_parms_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_parms_Tooltip = "Op��es";
         Ddo_contratoservicostelas_parms_Caption = "";
         Ddo_contratoservicostelas_link_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_link_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_link_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_link_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_link_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_link_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_link_Datalistproc = "GetContratoServicos_TelasWCFilterData";
         Ddo_contratoservicostelas_link_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_link_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_link_Filtertype = "Character";
         Ddo_contratoservicostelas_link_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_link_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_link_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_link_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_link_Tooltip = "Op��es";
         Ddo_contratoservicostelas_link_Caption = "";
         Ddo_contratoservicostelas_tela_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicostelas_tela_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicostelas_tela_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicostelas_tela_Loadingdata = "Carregando dados...";
         Ddo_contratoservicostelas_tela_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicostelas_tela_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicostelas_tela_Datalistproc = "GetContratoServicos_TelasWCFilterData";
         Ddo_contratoservicostelas_tela_Datalisttype = "Dynamic";
         Ddo_contratoservicostelas_tela_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicostelas_tela_Filtertype = "Character";
         Ddo_contratoservicostelas_tela_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace = "";
         Ddo_contratoservicostelas_tela_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicostelas_tela_Cls = "ColumnSettings";
         Ddo_contratoservicostelas_tela_Tooltip = "Op��es";
         Ddo_contratoservicostelas_tela_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetContratoServicos_TelasWCFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV22ContratoServicosTelas_TelaTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_TELATITLEFILTERDATA',pic:'',nv:null},{av:'AV26ContratoServicosTelas_LinkTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_LINKTITLEFILTERDATA',pic:'',nv:null},{av:'AV30ContratoServicosTelas_ParmsTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_PARMSTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContratoServicosTelas_StatusTitleFilterData',fld:'vCONTRATOSERVICOSTELAS_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratoServicosTelas_Tela_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_TELA',prop:'Titleformat'},{av:'edtContratoServicosTelas_Tela_Title',ctrl:'CONTRATOSERVICOSTELAS_TELA',prop:'Title'},{av:'edtContratoServicosTelas_Link_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_LINK',prop:'Titleformat'},{av:'edtContratoServicosTelas_Link_Title',ctrl:'CONTRATOSERVICOSTELAS_LINK',prop:'Title'},{av:'edtContratoServicosTelas_Parms_Titleformat',ctrl:'CONTRATOSERVICOSTELAS_PARMS',prop:'Titleformat'},{av:'edtContratoServicosTelas_Parms_Title',ctrl:'CONTRATOSERVICOSTELAS_PARMS',prop:'Title'},{av:'cmbContratoServicosTelas_Status'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E12GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_TELA.ONOPTIONCLICKED","{handler:'E13GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicostelas_tela_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_tela_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_tela_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_LINK.ONOPTIONCLICKED","{handler:'E14GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicostelas_link_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_link_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_link_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_PARMS.ONOPTIONCLICKED","{handler:'E15GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicostelas_parms_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_parms_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'FilteredText_get'},{av:'Ddo_contratoservicostelas_parms_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSTELAS_STATUS.ONOPTIONCLICKED","{handler:'E16GB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV19TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV20TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV23TFContratoServicosTelas_Tela',fld:'vTFCONTRATOSERVICOSTELAS_TELA',pic:'@!',nv:''},{av:'AV24TFContratoServicosTelas_Tela_Sel',fld:'vTFCONTRATOSERVICOSTELAS_TELA_SEL',pic:'@!',nv:''},{av:'AV27TFContratoServicosTelas_Link',fld:'vTFCONTRATOSERVICOSTELAS_LINK',pic:'',nv:''},{av:'AV28TFContratoServicosTelas_Link_Sel',fld:'vTFCONTRATOSERVICOSTELAS_LINK_SEL',pic:'',nv:''},{av:'AV31TFContratoServicosTelas_Parms',fld:'vTFCONTRATOSERVICOSTELAS_PARMS',pic:'',nv:''},{av:'AV32TFContratoServicosTelas_Parms_Sel',fld:'vTFCONTRATOSERVICOSTELAS_PARMS_SEL',pic:'',nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV21ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_TELATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_LINKTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_PARMSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSTELAS_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicostelas_status_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicostelas_status_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicostelas_status_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_STATUS',prop:'SortedStatus'},{av:'AV36TFContratoServicosTelas_Status_Sels',fld:'vTFCONTRATOSERVICOSTELAS_STATUS_SELS',pic:'',nv:null},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_tela_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_TELA',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_link_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_LINK',prop:'SortedStatus'},{av:'Ddo_contratoservicostelas_parms_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSTELAS_PARMS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E20GB2',iparms:[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E17GB2',iparms:[{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratoservicostelas_tela_Activeeventkey = "";
         Ddo_contratoservicostelas_tela_Filteredtext_get = "";
         Ddo_contratoservicostelas_tela_Selectedvalue_get = "";
         Ddo_contratoservicostelas_link_Activeeventkey = "";
         Ddo_contratoservicostelas_link_Filteredtext_get = "";
         Ddo_contratoservicostelas_link_Selectedvalue_get = "";
         Ddo_contratoservicostelas_parms_Activeeventkey = "";
         Ddo_contratoservicostelas_parms_Filteredtext_get = "";
         Ddo_contratoservicostelas_parms_Selectedvalue_get = "";
         Ddo_contratoservicostelas_status_Activeeventkey = "";
         Ddo_contratoservicostelas_status_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV19TFContratada_PessoaNom = "";
         AV20TFContratada_PessoaNom_Sel = "";
         AV23TFContratoServicosTelas_Tela = "";
         AV24TFContratoServicosTelas_Tela_Sel = "";
         AV27TFContratoServicosTelas_Link = "";
         AV28TFContratoServicosTelas_Link_Sel = "";
         AV31TFContratoServicosTelas_Parms = "";
         AV32TFContratoServicosTelas_Parms_Sel = "";
         AV21ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace = "";
         AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace = "";
         AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace = "";
         AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace = "";
         AV46Pgmname = "";
         AV36TFContratoServicosTelas_Status_Sels = new GxSimpleCollection();
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV38DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV18Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22ContratoServicosTelas_TelaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26ContratoServicosTelas_LinkTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV30ContratoServicosTelas_ParmsTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosTelas_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratoservicostelas_tela_Filteredtext_set = "";
         Ddo_contratoservicostelas_tela_Selectedvalue_set = "";
         Ddo_contratoservicostelas_tela_Sortedstatus = "";
         Ddo_contratoservicostelas_link_Filteredtext_set = "";
         Ddo_contratoservicostelas_link_Selectedvalue_set = "";
         Ddo_contratoservicostelas_link_Sortedstatus = "";
         Ddo_contratoservicostelas_parms_Filteredtext_set = "";
         Ddo_contratoservicostelas_parms_Selectedvalue_set = "";
         Ddo_contratoservicostelas_parms_Sortedstatus = "";
         Ddo_contratoservicostelas_status_Selectedvalue_set = "";
         Ddo_contratoservicostelas_status_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV44Update_GXI = "";
         AV16Delete = "";
         AV45Delete_GXI = "";
         A41Contratada_PessoaNom = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         A932ContratoServicosTelas_Status = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19TFContratada_PessoaNom = "";
         lV23TFContratoServicosTelas_Tela = "";
         lV27TFContratoServicosTelas_Link = "";
         lV31TFContratoServicosTelas_Parms = "";
         H00GB2_A74Contrato_Codigo = new int[1] ;
         H00GB2_n74Contrato_Codigo = new bool[] {false} ;
         H00GB2_A39Contratada_Codigo = new int[1] ;
         H00GB2_A40Contratada_PessoaCod = new int[1] ;
         H00GB2_A938ContratoServicosTelas_Sequencial = new short[1] ;
         H00GB2_A932ContratoServicosTelas_Status = new String[] {""} ;
         H00GB2_n932ContratoServicosTelas_Status = new bool[] {false} ;
         H00GB2_A929ContratoServicosTelas_Parms = new String[] {""} ;
         H00GB2_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         H00GB2_A928ContratoServicosTelas_Link = new String[] {""} ;
         H00GB2_A931ContratoServicosTelas_Tela = new String[] {""} ;
         H00GB2_A41Contratada_PessoaNom = new String[] {""} ;
         H00GB2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00GB2_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         H00GB3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35TFContratoServicosTelas_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicosTelas_ContratoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicos_telaswc__default(),
            new Object[][] {
                new Object[] {
               H00GB2_A74Contrato_Codigo, H00GB2_n74Contrato_Codigo, H00GB2_A39Contratada_Codigo, H00GB2_A40Contratada_PessoaCod, H00GB2_A938ContratoServicosTelas_Sequencial, H00GB2_A932ContratoServicosTelas_Status, H00GB2_n932ContratoServicosTelas_Status, H00GB2_A929ContratoServicosTelas_Parms, H00GB2_n929ContratoServicosTelas_Parms, H00GB2_A928ContratoServicosTelas_Link,
               H00GB2_A931ContratoServicosTelas_Tela, H00GB2_A41Contratada_PessoaNom, H00GB2_n41Contratada_PessoaNom, H00GB2_A926ContratoServicosTelas_ContratoCod
               }
               , new Object[] {
               H00GB3_AGRID_nRecordCount
               }
            }
         );
         AV46Pgmname = "ContratoServicos_TelasWC";
         /* GeneXus formulas. */
         AV46Pgmname = "ContratoServicos_TelasWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short A938ContratoServicosTelas_Sequencial ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratoServicosTelas_Tela_Titleformat ;
      private short edtContratoServicosTelas_Link_Titleformat ;
      private short edtContratoServicosTelas_Parms_Titleformat ;
      private short cmbContratoServicosTelas_Status_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicosTelas_ContratoCod ;
      private int wcpOAV7ContratoServicosTelas_ContratoCod ;
      private int subGrid_Rows ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_tela_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_link_Datalistupdateminimumcharacters ;
      private int Ddo_contratoservicostelas_parms_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratoservicostelas_tela_Visible ;
      private int edtavTfcontratoservicostelas_tela_sel_Visible ;
      private int edtavTfcontratoservicostelas_link_Visible ;
      private int edtavTfcontratoservicostelas_link_sel_Visible ;
      private int edtavTfcontratoservicostelas_parms_Visible ;
      private int edtavTfcontratoservicostelas_parms_sel_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV36TFContratoServicosTelas_Status_Sels_Count ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgInsert_Visible ;
      private int AV39PageToGo ;
      private int AV47GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_tela_Activeeventkey ;
      private String Ddo_contratoservicostelas_tela_Filteredtext_get ;
      private String Ddo_contratoservicostelas_tela_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_link_Activeeventkey ;
      private String Ddo_contratoservicostelas_link_Filteredtext_get ;
      private String Ddo_contratoservicostelas_link_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_parms_Activeeventkey ;
      private String Ddo_contratoservicostelas_parms_Filteredtext_get ;
      private String Ddo_contratoservicostelas_parms_Selectedvalue_get ;
      private String Ddo_contratoservicostelas_status_Activeeventkey ;
      private String Ddo_contratoservicostelas_status_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV19TFContratada_PessoaNom ;
      private String AV20TFContratada_PessoaNom_Sel ;
      private String AV23TFContratoServicosTelas_Tela ;
      private String AV24TFContratoServicosTelas_Tela_Sel ;
      private String AV46Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratoservicostelas_tela_Caption ;
      private String Ddo_contratoservicostelas_tela_Tooltip ;
      private String Ddo_contratoservicostelas_tela_Cls ;
      private String Ddo_contratoservicostelas_tela_Filteredtext_set ;
      private String Ddo_contratoservicostelas_tela_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_tela_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_tela_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_tela_Sortedstatus ;
      private String Ddo_contratoservicostelas_tela_Filtertype ;
      private String Ddo_contratoservicostelas_tela_Datalisttype ;
      private String Ddo_contratoservicostelas_tela_Datalistproc ;
      private String Ddo_contratoservicostelas_tela_Sortasc ;
      private String Ddo_contratoservicostelas_tela_Sortdsc ;
      private String Ddo_contratoservicostelas_tela_Loadingdata ;
      private String Ddo_contratoservicostelas_tela_Cleanfilter ;
      private String Ddo_contratoservicostelas_tela_Noresultsfound ;
      private String Ddo_contratoservicostelas_tela_Searchbuttontext ;
      private String Ddo_contratoservicostelas_link_Caption ;
      private String Ddo_contratoservicostelas_link_Tooltip ;
      private String Ddo_contratoservicostelas_link_Cls ;
      private String Ddo_contratoservicostelas_link_Filteredtext_set ;
      private String Ddo_contratoservicostelas_link_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_link_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_link_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_link_Sortedstatus ;
      private String Ddo_contratoservicostelas_link_Filtertype ;
      private String Ddo_contratoservicostelas_link_Datalisttype ;
      private String Ddo_contratoservicostelas_link_Datalistproc ;
      private String Ddo_contratoservicostelas_link_Sortasc ;
      private String Ddo_contratoservicostelas_link_Sortdsc ;
      private String Ddo_contratoservicostelas_link_Loadingdata ;
      private String Ddo_contratoservicostelas_link_Cleanfilter ;
      private String Ddo_contratoservicostelas_link_Noresultsfound ;
      private String Ddo_contratoservicostelas_link_Searchbuttontext ;
      private String Ddo_contratoservicostelas_parms_Caption ;
      private String Ddo_contratoservicostelas_parms_Tooltip ;
      private String Ddo_contratoservicostelas_parms_Cls ;
      private String Ddo_contratoservicostelas_parms_Filteredtext_set ;
      private String Ddo_contratoservicostelas_parms_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_parms_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_parms_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_parms_Sortedstatus ;
      private String Ddo_contratoservicostelas_parms_Filtertype ;
      private String Ddo_contratoservicostelas_parms_Datalisttype ;
      private String Ddo_contratoservicostelas_parms_Datalistproc ;
      private String Ddo_contratoservicostelas_parms_Sortasc ;
      private String Ddo_contratoservicostelas_parms_Sortdsc ;
      private String Ddo_contratoservicostelas_parms_Loadingdata ;
      private String Ddo_contratoservicostelas_parms_Cleanfilter ;
      private String Ddo_contratoservicostelas_parms_Noresultsfound ;
      private String Ddo_contratoservicostelas_parms_Searchbuttontext ;
      private String Ddo_contratoservicostelas_status_Caption ;
      private String Ddo_contratoservicostelas_status_Tooltip ;
      private String Ddo_contratoservicostelas_status_Cls ;
      private String Ddo_contratoservicostelas_status_Selectedvalue_set ;
      private String Ddo_contratoservicostelas_status_Dropdownoptionstype ;
      private String Ddo_contratoservicostelas_status_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicostelas_status_Sortedstatus ;
      private String Ddo_contratoservicostelas_status_Datalisttype ;
      private String Ddo_contratoservicostelas_status_Datalistfixedvalues ;
      private String Ddo_contratoservicostelas_status_Sortasc ;
      private String Ddo_contratoservicostelas_status_Sortdsc ;
      private String Ddo_contratoservicostelas_status_Cleanfilter ;
      private String Ddo_contratoservicostelas_status_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_tela_Internalname ;
      private String edtavTfcontratoservicostelas_tela_Jsonclick ;
      private String edtavTfcontratoservicostelas_tela_sel_Internalname ;
      private String edtavTfcontratoservicostelas_tela_sel_Jsonclick ;
      private String edtavTfcontratoservicostelas_link_Internalname ;
      private String edtavTfcontratoservicostelas_link_Jsonclick ;
      private String edtavTfcontratoservicostelas_link_sel_Internalname ;
      private String edtavTfcontratoservicostelas_link_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfcontratoservicostelas_parms_Internalname ;
      private String edtavTfcontratoservicostelas_parms_sel_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_telatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_linktitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_parmstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicostelas_statustitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosTelas_ContratoCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A931ContratoServicosTelas_Tela ;
      private String edtContratoServicosTelas_Tela_Internalname ;
      private String edtContratoServicosTelas_Link_Internalname ;
      private String edtContratoServicosTelas_Parms_Internalname ;
      private String cmbContratoServicosTelas_Status_Internalname ;
      private String A932ContratoServicosTelas_Status ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV19TFContratada_PessoaNom ;
      private String lV23TFContratoServicosTelas_Tela ;
      private String subGrid_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratoservicostelas_tela_Internalname ;
      private String Ddo_contratoservicostelas_link_Internalname ;
      private String Ddo_contratoservicostelas_parms_Internalname ;
      private String Ddo_contratoservicostelas_status_Internalname ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratoServicosTelas_Tela_Title ;
      private String edtContratoServicosTelas_Link_Title ;
      private String edtContratoServicosTelas_Parms_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContratoServicosTelas_ContratoCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosTelas_ContratoCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratoServicosTelas_Tela_Jsonclick ;
      private String edtContratoServicosTelas_Link_Jsonclick ;
      private String edtContratoServicosTelas_Parms_Jsonclick ;
      private String cmbContratoServicosTelas_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratoservicostelas_tela_Includesortasc ;
      private bool Ddo_contratoservicostelas_tela_Includesortdsc ;
      private bool Ddo_contratoservicostelas_tela_Includefilter ;
      private bool Ddo_contratoservicostelas_tela_Filterisrange ;
      private bool Ddo_contratoservicostelas_tela_Includedatalist ;
      private bool Ddo_contratoservicostelas_link_Includesortasc ;
      private bool Ddo_contratoservicostelas_link_Includesortdsc ;
      private bool Ddo_contratoservicostelas_link_Includefilter ;
      private bool Ddo_contratoservicostelas_link_Filterisrange ;
      private bool Ddo_contratoservicostelas_link_Includedatalist ;
      private bool Ddo_contratoservicostelas_parms_Includesortasc ;
      private bool Ddo_contratoservicostelas_parms_Includesortdsc ;
      private bool Ddo_contratoservicostelas_parms_Includefilter ;
      private bool Ddo_contratoservicostelas_parms_Filterisrange ;
      private bool Ddo_contratoservicostelas_parms_Includedatalist ;
      private bool Ddo_contratoservicostelas_status_Includesortasc ;
      private bool Ddo_contratoservicostelas_status_Includesortdsc ;
      private bool Ddo_contratoservicostelas_status_Includefilter ;
      private bool Ddo_contratoservicostelas_status_Includedatalist ;
      private bool Ddo_contratoservicostelas_status_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n74Contrato_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private String A929ContratoServicosTelas_Parms ;
      private String AV35TFContratoServicosTelas_Status_SelsJson ;
      private String AV27TFContratoServicosTelas_Link ;
      private String AV28TFContratoServicosTelas_Link_Sel ;
      private String AV31TFContratoServicosTelas_Parms ;
      private String AV32TFContratoServicosTelas_Parms_Sel ;
      private String AV21ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV25ddo_ContratoServicosTelas_TelaTitleControlIdToReplace ;
      private String AV29ddo_ContratoServicosTelas_LinkTitleControlIdToReplace ;
      private String AV33ddo_ContratoServicosTelas_ParmsTitleControlIdToReplace ;
      private String AV37ddo_ContratoServicosTelas_StatusTitleControlIdToReplace ;
      private String AV44Update_GXI ;
      private String AV45Delete_GXI ;
      private String A928ContratoServicosTelas_Link ;
      private String lV27TFContratoServicosTelas_Link ;
      private String lV31TFContratoServicosTelas_Parms ;
      private String AV15Update ;
      private String AV16Delete ;
      private IGxSession AV17Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosTelas_Status ;
      private IDataStoreProvider pr_default ;
      private int[] H00GB2_A74Contrato_Codigo ;
      private bool[] H00GB2_n74Contrato_Codigo ;
      private int[] H00GB2_A39Contratada_Codigo ;
      private int[] H00GB2_A40Contratada_PessoaCod ;
      private short[] H00GB2_A938ContratoServicosTelas_Sequencial ;
      private String[] H00GB2_A932ContratoServicosTelas_Status ;
      private bool[] H00GB2_n932ContratoServicosTelas_Status ;
      private String[] H00GB2_A929ContratoServicosTelas_Parms ;
      private bool[] H00GB2_n929ContratoServicosTelas_Parms ;
      private String[] H00GB2_A928ContratoServicosTelas_Link ;
      private String[] H00GB2_A931ContratoServicosTelas_Tela ;
      private String[] H00GB2_A41Contratada_PessoaNom ;
      private bool[] H00GB2_n41Contratada_PessoaNom ;
      private int[] H00GB2_A926ContratoServicosTelas_ContratoCod ;
      private long[] H00GB3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36TFContratoServicosTelas_Status_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV18Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22ContratoServicosTelas_TelaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26ContratoServicosTelas_LinkTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoServicosTelas_ParmsTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContratoServicosTelas_StatusTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV38DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicos_telaswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GB2( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV36TFContratoServicosTelas_Status_Sels ,
                                             String AV20TFContratada_PessoaNom_Sel ,
                                             String AV19TFContratada_PessoaNom ,
                                             String AV24TFContratoServicosTelas_Tela_Sel ,
                                             String AV23TFContratoServicosTelas_Tela ,
                                             String AV28TFContratoServicosTelas_Link_Sel ,
                                             String AV27TFContratoServicosTelas_Link ,
                                             String AV32TFContratoServicosTelas_Parms_Sel ,
                                             String AV31TFContratoServicosTelas_Parms ,
                                             int AV36TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A926ContratoServicosTelas_ContratoCod ,
                                             int AV7ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Sequencial], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod";
         sFromString = " FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV7ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV19TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV20TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV23TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV24TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV27TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV28TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV31TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV32TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV36TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV36TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod] DESC, T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Tela]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod] DESC, T1.[ContratoServicosTelas_Tela] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Link]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod] DESC, T1.[ContratoServicosTelas_Link] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Parms]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod] DESC, T1.[ContratoServicosTelas_Parms] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Status]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod] DESC, T1.[ContratoServicosTelas_Status] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GB3( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV36TFContratoServicosTelas_Status_Sels ,
                                             String AV20TFContratada_PessoaNom_Sel ,
                                             String AV19TFContratada_PessoaNom ,
                                             String AV24TFContratoServicosTelas_Tela_Sel ,
                                             String AV23TFContratoServicosTelas_Tela ,
                                             String AV28TFContratoServicosTelas_Link_Sel ,
                                             String AV27TFContratoServicosTelas_Link ,
                                             String AV32TFContratoServicosTelas_Parms_Sel ,
                                             String AV31TFContratoServicosTelas_Parms ,
                                             int AV36TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A926ContratoServicosTelas_ContratoCod ,
                                             int AV7ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV7ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV19TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV20TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV23TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV24TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV27TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV28TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV31TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV32TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV36TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV36TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GB2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
               case 1 :
                     return conditional_H00GB3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GB2 ;
          prmH00GB2 = new Object[] {
          new Object[] {"@AV7ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV20TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV24TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV28TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV31TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV32TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GB3 ;
          prmH00GB3 = new Object[] {
          new Object[] {"@AV7ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV19TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV20TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV24TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV28TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV31TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV32TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GB2,11,0,true,false )
             ,new CursorDef("H00GB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GB3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

}
