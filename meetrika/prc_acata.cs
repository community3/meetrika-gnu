/*
               File: PRC_Acata
        Description: Acata
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:55.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_acata : GXProcedure
   {
      public prc_acata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_acata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_OsVinculada ,
                           int aP2_UserId ,
                           decimal aP3_PFB ,
                           decimal aP4_PFL ,
                           String aP5_Observacao )
      {
         this.AV9Codigo = aP0_Codigo;
         this.AV14OsVinculada = aP1_OsVinculada;
         this.AV12UserId = aP2_UserId;
         this.AV16PFB = aP3_PFB;
         this.AV17PFL = aP4_PFL;
         this.AV22Observacao = aP5_Observacao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_OsVinculada ,
                                 int aP2_UserId ,
                                 decimal aP3_PFB ,
                                 decimal aP4_PFL ,
                                 String aP5_Observacao )
      {
         prc_acata objprc_acata;
         objprc_acata = new prc_acata();
         objprc_acata.AV9Codigo = aP0_Codigo;
         objprc_acata.AV14OsVinculada = aP1_OsVinculada;
         objprc_acata.AV12UserId = aP2_UserId;
         objprc_acata.AV16PFB = aP3_PFB;
         objprc_acata.AV17PFL = aP4_PFL;
         objprc_acata.AV22Observacao = aP5_Observacao;
         objprc_acata.context.SetSubmitInitialConfig(context);
         objprc_acata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_acata);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_acata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Hora = context.localUtil.ServerTime( context, "DEFAULT");
         AV30OSCodigo = AV9Codigo;
         /* Execute user subroutine: 'STATUS' */
         S121 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Using cursor P008J2 */
         pr_default.execute(0, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A517ContagemResultado_Ultima = P008J2_A517ContagemResultado_Ultima[0];
            A456ContagemResultado_Codigo = P008J2_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P008J2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008J2_n484ContagemResultado_StatusDmn[0];
            A52Contratada_AreaTrabalhoCod = P008J2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008J2_n52Contratada_AreaTrabalhoCod[0];
            A490ContagemResultado_ContratadaCod = P008J2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008J2_n490ContagemResultado_ContratadaCod[0];
            A470ContagemResultado_ContadorFMCod = P008J2_A470ContagemResultado_ContadorFMCod[0];
            A800ContagemResultado_Deflator = P008J2_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P008J2_n800ContagemResultado_Deflator[0];
            A1553ContagemResultado_CntSrvCod = P008J2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008J2_n1553ContagemResultado_CntSrvCod[0];
            A472ContagemResultado_DataEntrega = P008J2_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P008J2_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P008J2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008J2_n912ContagemResultado_HoraEntrega[0];
            A473ContagemResultado_DataCnt = P008J2_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P008J2_A511ContagemResultado_HoraCnt[0];
            A484ContagemResultado_StatusDmn = P008J2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008J2_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P008J2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008J2_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P008J2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008J2_n1553ContagemResultado_CntSrvCod[0];
            A472ContagemResultado_DataEntrega = P008J2_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P008J2_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P008J2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P008J2_n912ContagemResultado_HoraEntrega[0];
            A52Contratada_AreaTrabalhoCod = P008J2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P008J2_n52Contratada_AreaTrabalhoCod[0];
            A517ContagemResultado_Ultima = false;
            AV23StatusAnterior = A484ContagemResultado_StatusDmn;
            AV19ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            AV25AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            AV24Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            AV18ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            AV20ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            AV27ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            AV13PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            new prc_setfimcrr(context ).execute( ref  AV9Codigo) ;
            GXt_dtime1 = DateTimeUtil.ServerNow( context, "DEFAULT");
            new prc_setdataentregaexecucao(context ).execute( ref  A456ContagemResultado_Codigo, ref  GXt_dtime1) ;
            new prc_inslogresponsavel(context ).execute( ref  AV9Codigo,  0,  "T",  "D",  AV12UserId,  0,  AV23StatusAnterior,  AV29Status,  AV22Observacao,  AV13PrazoEntrega,  true) ;
            new prc_disparoservicovinculado(context ).execute(  AV19ContagemResultado_Codigo,  AV12UserId) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008J3 */
            pr_default.execute(1, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P008J4 */
            pr_default.execute(2, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Execute user subroutine: 'NEWCONTAGEM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( ! (0==AV14OsVinculada) )
         {
            AV30OSCodigo = AV14OsVinculada;
            /* Execute user subroutine: 'STATUS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P008J5 */
            pr_default.execute(3, new Object[] {AV14OsVinculada});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A517ContagemResultado_Ultima = P008J5_A517ContagemResultado_Ultima[0];
               A456ContagemResultado_Codigo = P008J5_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = P008J5_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008J5_n484ContagemResultado_StatusDmn[0];
               A52Contratada_AreaTrabalhoCod = P008J5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P008J5_n52Contratada_AreaTrabalhoCod[0];
               A490ContagemResultado_ContratadaCod = P008J5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008J5_n490ContagemResultado_ContratadaCod[0];
               A470ContagemResultado_ContadorFMCod = P008J5_A470ContagemResultado_ContadorFMCod[0];
               A800ContagemResultado_Deflator = P008J5_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P008J5_n800ContagemResultado_Deflator[0];
               A1553ContagemResultado_CntSrvCod = P008J5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008J5_n1553ContagemResultado_CntSrvCod[0];
               A472ContagemResultado_DataEntrega = P008J5_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008J5_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P008J5_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008J5_n912ContagemResultado_HoraEntrega[0];
               A473ContagemResultado_DataCnt = P008J5_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P008J5_A511ContagemResultado_HoraCnt[0];
               A484ContagemResultado_StatusDmn = P008J5_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P008J5_n484ContagemResultado_StatusDmn[0];
               A490ContagemResultado_ContratadaCod = P008J5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P008J5_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P008J5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008J5_n1553ContagemResultado_CntSrvCod[0];
               A472ContagemResultado_DataEntrega = P008J5_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P008J5_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P008J5_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P008J5_n912ContagemResultado_HoraEntrega[0];
               A52Contratada_AreaTrabalhoCod = P008J5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P008J5_n52Contratada_AreaTrabalhoCod[0];
               A517ContagemResultado_Ultima = false;
               AV23StatusAnterior = A484ContagemResultado_StatusDmn;
               AV19ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV25AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
               AV24Contratada_Codigo = A490ContagemResultado_ContratadaCod;
               AV18ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
               AV20ContagemResultado_Deflator = A800ContagemResultado_Deflator;
               AV27ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
               AV13PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008J6 */
               pr_default.execute(4, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P008J7 */
               pr_default.execute(5, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Execute user subroutine: 'NEWCONTAGEM' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            new prc_inslogresponsavel(context ).execute( ref  AV19ContagemResultado_Codigo,  0,  "T",  "D",  AV12UserId,  0,  AV23StatusAnterior,  AV29Status,  AV22Observacao,  AV13PrazoEntrega,  true) ;
            new prc_disparoservicovinculado(context ).execute(  AV19ContagemResultado_Codigo,  AV12UserId) ;
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWCONTAGEM' Routine */
         GXt_char2 = AV28CalculoPFinal;
         new prc_cstuntprdnrm(context ).execute( ref  AV27ContratoServicos_Codigo, ref  AV18ContagemResultado_ContadorFMCod, ref  AV26CstUntNrm, out  GXt_char2) ;
         AV28CalculoPFinal = GXt_char2;
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV19ContagemResultado_Codigo;
         A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
         A511ContagemResultado_HoraCnt = AV8Hora;
         A460ContagemResultado_PFBFM = AV16PFB;
         n460ContagemResultado_PFBFM = false;
         A458ContagemResultado_PFBFS = AV16PFB;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = AV17PFL;
         n459ContagemResultado_PFLFS = false;
         A461ContagemResultado_PFLFM = AV17PFL;
         n461ContagemResultado_PFLFM = false;
         A800ContagemResultado_Deflator = AV20ContagemResultado_Deflator;
         n800ContagemResultado_Deflator = false;
         A833ContagemResultado_CstUntPrd = AV26CstUntNrm;
         n833ContagemResultado_CstUntPrd = false;
         A482ContagemResultadoContagens_Esforco = 0;
         A470ContagemResultado_ContadorFMCod = AV18ContagemResultado_ContadorFMCod;
         A483ContagemResultado_StatusCnt = 5;
         A462ContagemResultado_Divergencia = 0;
         A852ContagemResultado_Planilha = "";
         n852ContagemResultado_Planilha = false;
         n852ContagemResultado_Planilha = true;
         A853ContagemResultado_NomePla = "";
         n853ContagemResultado_NomePla = false;
         n853ContagemResultado_NomePla = true;
         A854ContagemResultado_TipoPla = "";
         n854ContagemResultado_TipoPla = false;
         n854ContagemResultado_TipoPla = true;
         A469ContagemResultado_NaoCnfCntCod = 0;
         n469ContagemResultado_NaoCnfCntCod = false;
         n469ContagemResultado_NaoCnfCntCod = true;
         A463ContagemResultado_ParecerTcn = "";
         n463ContagemResultado_ParecerTcn = false;
         n463ContagemResultado_ParecerTcn = true;
         A517ContagemResultado_Ultima = true;
         /* Using cursor P008J8 */
         A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
         n853ContagemResultado_NomePla = false;
         A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
         n854ContagemResultado_TipoPla = false;
         pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla});
         pr_default.close(6);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(6) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            GXt_char2 = AV8Hora;
            new prc_horasnominutoseguinte(context ).execute(  AV8Hora, out  GXt_char2) ;
            AV8Hora = GXt_char2;
            /*
               INSERT RECORD ON TABLE ContagemResultadoContagens

            */
            W456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            W473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
            W511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
            W460ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            W458ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            W459ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            W461ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            W800ContagemResultado_Deflator = A800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            W833ContagemResultado_CstUntPrd = A833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            W482ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
            W470ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
            W483ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
            W462ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
            W852ContagemResultado_Planilha = A852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            W853ContagemResultado_NomePla = A853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            W854ContagemResultado_TipoPla = A854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            W469ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            W463ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            W517ContagemResultado_Ultima = A517ContagemResultado_Ultima;
            A456ContagemResultado_Codigo = AV19ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
            A511ContagemResultado_HoraCnt = AV8Hora;
            A460ContagemResultado_PFBFM = AV16PFB;
            n460ContagemResultado_PFBFM = false;
            A458ContagemResultado_PFBFS = AV16PFB;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = AV17PFL;
            n459ContagemResultado_PFLFS = false;
            A461ContagemResultado_PFLFM = AV17PFL;
            n461ContagemResultado_PFLFM = false;
            A800ContagemResultado_Deflator = AV20ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            A833ContagemResultado_CstUntPrd = AV26CstUntNrm;
            n833ContagemResultado_CstUntPrd = false;
            A482ContagemResultadoContagens_Esforco = 0;
            A470ContagemResultado_ContadorFMCod = AV18ContagemResultado_ContadorFMCod;
            A483ContagemResultado_StatusCnt = 5;
            A462ContagemResultado_Divergencia = 0;
            A852ContagemResultado_Planilha = "";
            n852ContagemResultado_Planilha = false;
            n852ContagemResultado_Planilha = true;
            A853ContagemResultado_NomePla = "";
            n853ContagemResultado_NomePla = false;
            n853ContagemResultado_NomePla = true;
            A854ContagemResultado_TipoPla = "";
            n854ContagemResultado_TipoPla = false;
            n854ContagemResultado_TipoPla = true;
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
            A463ContagemResultado_ParecerTcn = "";
            n463ContagemResultado_ParecerTcn = false;
            n463ContagemResultado_ParecerTcn = true;
            A517ContagemResultado_Ultima = true;
            /* Using cursor P008J9 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if ( (pr_default.getStatus(7) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A456ContagemResultado_Codigo = W456ContagemResultado_Codigo;
            A473ContagemResultado_DataCnt = W473ContagemResultado_DataCnt;
            A511ContagemResultado_HoraCnt = W511ContagemResultado_HoraCnt;
            A460ContagemResultado_PFBFM = W460ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A458ContagemResultado_PFBFS = W458ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = W459ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            A461ContagemResultado_PFLFM = W461ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A800ContagemResultado_Deflator = W800ContagemResultado_Deflator;
            n800ContagemResultado_Deflator = false;
            A833ContagemResultado_CstUntPrd = W833ContagemResultado_CstUntPrd;
            n833ContagemResultado_CstUntPrd = false;
            A482ContagemResultadoContagens_Esforco = W482ContagemResultadoContagens_Esforco;
            A470ContagemResultado_ContadorFMCod = W470ContagemResultado_ContadorFMCod;
            A483ContagemResultado_StatusCnt = W483ContagemResultado_StatusCnt;
            A462ContagemResultado_Divergencia = W462ContagemResultado_Divergencia;
            A852ContagemResultado_Planilha = W852ContagemResultado_Planilha;
            n852ContagemResultado_Planilha = false;
            A853ContagemResultado_NomePla = W853ContagemResultado_NomePla;
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = W854ContagemResultado_TipoPla;
            n854ContagemResultado_TipoPla = false;
            A469ContagemResultado_NaoCnfCntCod = W469ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            A463ContagemResultado_ParecerTcn = W463ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            A517ContagemResultado_Ultima = W517ContagemResultado_Ultima;
            /* End Insert */
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S121( )
      {
         /* 'STATUS' Routine */
         AV29Status = "R";
         if ( new prc_estevecancelada(context).executeUdp( ref  AV30OSCodigo) )
         {
            AV29Status = "X";
         }
         new prc_alterastatusdmn(context ).execute( ref  AV30OSCodigo,  AV29Status,  0) ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_Acata");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Hora = "";
         scmdbuf = "";
         P008J2_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008J2_A456ContagemResultado_Codigo = new int[1] ;
         P008J2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008J2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008J2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008J2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P008J2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008J2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008J2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P008J2_A800ContagemResultado_Deflator = new decimal[1] ;
         P008J2_n800ContagemResultado_Deflator = new bool[] {false} ;
         P008J2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008J2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008J2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008J2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008J2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008J2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008J2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008J2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV23StatusAnterior = "";
         AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         AV29Status = "";
         P008J5_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008J5_A456ContagemResultado_Codigo = new int[1] ;
         P008J5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008J5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008J5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P008J5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P008J5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008J5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008J5_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P008J5_A800ContagemResultado_Deflator = new decimal[1] ;
         P008J5_n800ContagemResultado_Deflator = new bool[] {false} ;
         P008J5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008J5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008J5_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P008J5_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P008J5_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P008J5_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P008J5_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008J5_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV28CalculoPFinal = "";
         A852ContagemResultado_Planilha = "";
         A853ContagemResultado_NomePla = "";
         A854ContagemResultado_TipoPla = "";
         A463ContagemResultado_ParecerTcn = "";
         Gx_emsg = "";
         GXt_char2 = "";
         W473ContagemResultado_DataCnt = DateTime.MinValue;
         W511ContagemResultado_HoraCnt = "";
         W852ContagemResultado_Planilha = "";
         W853ContagemResultado_NomePla = "";
         W854ContagemResultado_TipoPla = "";
         W463ContagemResultado_ParecerTcn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_acata__default(),
            new Object[][] {
                new Object[] {
               P008J2_A517ContagemResultado_Ultima, P008J2_A456ContagemResultado_Codigo, P008J2_A484ContagemResultado_StatusDmn, P008J2_n484ContagemResultado_StatusDmn, P008J2_A52Contratada_AreaTrabalhoCod, P008J2_n52Contratada_AreaTrabalhoCod, P008J2_A490ContagemResultado_ContratadaCod, P008J2_n490ContagemResultado_ContratadaCod, P008J2_A470ContagemResultado_ContadorFMCod, P008J2_A800ContagemResultado_Deflator,
               P008J2_n800ContagemResultado_Deflator, P008J2_A1553ContagemResultado_CntSrvCod, P008J2_n1553ContagemResultado_CntSrvCod, P008J2_A472ContagemResultado_DataEntrega, P008J2_n472ContagemResultado_DataEntrega, P008J2_A912ContagemResultado_HoraEntrega, P008J2_n912ContagemResultado_HoraEntrega, P008J2_A473ContagemResultado_DataCnt, P008J2_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008J5_A517ContagemResultado_Ultima, P008J5_A456ContagemResultado_Codigo, P008J5_A484ContagemResultado_StatusDmn, P008J5_n484ContagemResultado_StatusDmn, P008J5_A52Contratada_AreaTrabalhoCod, P008J5_n52Contratada_AreaTrabalhoCod, P008J5_A490ContagemResultado_ContratadaCod, P008J5_n490ContagemResultado_ContratadaCod, P008J5_A470ContagemResultado_ContadorFMCod, P008J5_A800ContagemResultado_Deflator,
               P008J5_n800ContagemResultado_Deflator, P008J5_A1553ContagemResultado_CntSrvCod, P008J5_n1553ContagemResultado_CntSrvCod, P008J5_A472ContagemResultado_DataEntrega, P008J5_n472ContagemResultado_DataEntrega, P008J5_A912ContagemResultado_HoraEntrega, P008J5_n912ContagemResultado_HoraEntrega, P008J5_A473ContagemResultado_DataCnt, P008J5_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short W482ContagemResultadoContagens_Esforco ;
      private short W483ContagemResultado_StatusCnt ;
      private int AV9Codigo ;
      private int AV14OsVinculada ;
      private int AV12UserId ;
      private int AV30OSCodigo ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV19ContagemResultado_Codigo ;
      private int AV25AreaTrabalho_Codigo ;
      private int AV24Contratada_Codigo ;
      private int AV18ContagemResultado_ContadorFMCod ;
      private int AV27ContratoServicos_Codigo ;
      private int GX_INS72 ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int W456ContagemResultado_Codigo ;
      private int W470ContagemResultado_ContadorFMCod ;
      private int W469ContagemResultado_NaoCnfCntCod ;
      private decimal AV16PFB ;
      private decimal AV17PFL ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal AV20ContagemResultado_Deflator ;
      private decimal AV26CstUntNrm ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal W460ContagemResultado_PFBFM ;
      private decimal W458ContagemResultado_PFBFS ;
      private decimal W459ContagemResultado_PFLFS ;
      private decimal W461ContagemResultado_PFLFM ;
      private decimal W800ContagemResultado_Deflator ;
      private decimal W833ContagemResultado_CstUntPrd ;
      private decimal W462ContagemResultado_Divergencia ;
      private String AV8Hora ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV23StatusAnterior ;
      private String AV29Status ;
      private String AV28CalculoPFinal ;
      private String A853ContagemResultado_NomePla ;
      private String A854ContagemResultado_TipoPla ;
      private String Gx_emsg ;
      private String GXt_char2 ;
      private String W511ContagemResultado_HoraCnt ;
      private String W853ContagemResultado_NomePla ;
      private String W854ContagemResultado_TipoPla ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV13PrazoEntrega ;
      private DateTime GXt_dtime1 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime W473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool A517ContagemResultado_Ultima ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n800ContagemResultado_Deflator ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n852ContagemResultado_Planilha ;
      private bool n853ContagemResultado_NomePla ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool W517ContagemResultado_Ultima ;
      private String AV22Observacao ;
      private String A463ContagemResultado_ParecerTcn ;
      private String W463ContagemResultado_ParecerTcn ;
      private String A852ContagemResultado_Planilha ;
      private String W852ContagemResultado_Planilha ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P008J2_A517ContagemResultado_Ultima ;
      private int[] P008J2_A456ContagemResultado_Codigo ;
      private String[] P008J2_A484ContagemResultado_StatusDmn ;
      private bool[] P008J2_n484ContagemResultado_StatusDmn ;
      private int[] P008J2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P008J2_n52Contratada_AreaTrabalhoCod ;
      private int[] P008J2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008J2_n490ContagemResultado_ContratadaCod ;
      private int[] P008J2_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P008J2_A800ContagemResultado_Deflator ;
      private bool[] P008J2_n800ContagemResultado_Deflator ;
      private int[] P008J2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008J2_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P008J2_A472ContagemResultado_DataEntrega ;
      private bool[] P008J2_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008J2_A912ContagemResultado_HoraEntrega ;
      private bool[] P008J2_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P008J2_A473ContagemResultado_DataCnt ;
      private String[] P008J2_A511ContagemResultado_HoraCnt ;
      private bool[] P008J5_A517ContagemResultado_Ultima ;
      private int[] P008J5_A456ContagemResultado_Codigo ;
      private String[] P008J5_A484ContagemResultado_StatusDmn ;
      private bool[] P008J5_n484ContagemResultado_StatusDmn ;
      private int[] P008J5_A52Contratada_AreaTrabalhoCod ;
      private bool[] P008J5_n52Contratada_AreaTrabalhoCod ;
      private int[] P008J5_A490ContagemResultado_ContratadaCod ;
      private bool[] P008J5_n490ContagemResultado_ContratadaCod ;
      private int[] P008J5_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P008J5_A800ContagemResultado_Deflator ;
      private bool[] P008J5_n800ContagemResultado_Deflator ;
      private int[] P008J5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008J5_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P008J5_A472ContagemResultado_DataEntrega ;
      private bool[] P008J5_n472ContagemResultado_DataEntrega ;
      private DateTime[] P008J5_A912ContagemResultado_HoraEntrega ;
      private bool[] P008J5_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P008J5_A473ContagemResultado_DataCnt ;
      private String[] P008J5_A511ContagemResultado_HoraCnt ;
   }

   public class prc_acata__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008J2 ;
          prmP008J2 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008J3 ;
          prmP008J3 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008J4 ;
          prmP008J4 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008J5 ;
          prmP008J5 = new Object[] {
          new Object[] {"@AV14OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008J6 ;
          prmP008J6 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008J7 ;
          prmP008J7 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008J8 ;
          prmP008J8 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0}
          } ;
          Object[] prmP008J9 ;
          prmP008J9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008J2", "SELECT TOP 1 T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T3.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_ContadorFMCod], T1.[ContagemResultado_Deflator], T2.[ContagemResultado_CntSrvCod], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV9Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008J2,1,0,true,true )
             ,new CursorDef("P008J3", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008J3)
             ,new CursorDef("P008J4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008J4)
             ,new CursorDef("P008J5", "SELECT TOP 1 T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_StatusDmn], T3.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_ContadorFMCod], T1.[ContagemResultado_Deflator], T2.[ContagemResultado_CntSrvCod], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV14OsVinculada) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008J5,1,0,true,true )
             ,new CursorDef("P008J6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008J6)
             ,new CursorDef("P008J7", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008J7)
             ,new CursorDef("P008J8", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP008J8)
             ,new CursorDef("P008J9", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP008J9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 5) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 2 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                return;
       }
    }

 }

}
