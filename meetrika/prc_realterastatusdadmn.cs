/*
               File: PRC_REAlteraStatusDaDmn
        Description: RE Altera Status Da Dmn
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:0.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_realterastatusdadmn : GXProcedure
   {
      public prc_realterastatusdadmn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_realterastatusdadmn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_StatusDmn ,
                           int aP2_UserId )
      {
         this.AV10Codigo = aP0_Codigo;
         this.AV8StatusDmn = aP1_StatusDmn;
         this.AV11UserId = aP2_UserId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 String aP1_StatusDmn ,
                                 int aP2_UserId )
      {
         prc_realterastatusdadmn objprc_realterastatusdadmn;
         objprc_realterastatusdadmn = new prc_realterastatusdadmn();
         objprc_realterastatusdadmn.AV10Codigo = aP0_Codigo;
         objprc_realterastatusdadmn.AV8StatusDmn = aP1_StatusDmn;
         objprc_realterastatusdadmn.AV11UserId = aP2_UserId;
         objprc_realterastatusdadmn.context.SetSubmitInitialConfig(context);
         objprc_realterastatusdadmn.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_realterastatusdadmn);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_realterastatusdadmn)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new prc_alterastatusdmn(context ).execute( ref  AV10Codigo,  AV8StatusDmn,  AV11UserId) ;
         /* Using cursor P008S2 */
         pr_default.execute(0, new Object[] {AV10Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P008S2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008S2_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P008S2_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P008S2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P008S2_n602ContagemResultado_OSVinculada[0];
            A490ContagemResultado_ContratadaCod = P008S2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P008S2_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P008S2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008S2_n601ContagemResultado_Servico[0];
            A1348ContagemResultado_DataHomologacao = P008S2_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P008S2_n1348ContagemResultado_DataHomologacao[0];
            A601ContagemResultado_Servico = P008S2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P008S2_n601ContagemResultado_Servico[0];
            AV9OSVinculada = A602ContagemResultado_OSVinculada;
            AV13Prestadora = A490ContagemResultado_ContratadaCod;
            AV14Servico = A601ContagemResultado_Servico;
            if ( StringUtil.StrCmp(AV8StatusDmn, "H") == 0 )
            {
               A1348ContagemResultado_DataHomologacao = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1348ContagemResultado_DataHomologacao = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008S3 */
            pr_default.execute(1, new Object[] {n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P008S4 */
            pr_default.execute(2, new Object[] {n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         context.CommitDataStores( "PRC_REAlteraStatusDaDmn");
         if ( (0==AV9OSVinculada) )
         {
            new prc_disparoservicovinculado(context ).execute(  AV10Codigo,  AV11UserId) ;
         }
         else
         {
            /* Using cursor P008S5 */
            pr_default.execute(3, new Object[] {AV9OSVinculada});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A517ContagemResultado_Ultima = P008S5_A517ContagemResultado_Ultima[0];
               A456ContagemResultado_Codigo = P008S5_A456ContagemResultado_Codigo[0];
               A483ContagemResultado_StatusCnt = P008S5_A483ContagemResultado_StatusCnt[0];
               A1348ContagemResultado_DataHomologacao = P008S5_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P008S5_n1348ContagemResultado_DataHomologacao[0];
               A473ContagemResultado_DataCnt = P008S5_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P008S5_A511ContagemResultado_HoraCnt[0];
               A1348ContagemResultado_DataHomologacao = P008S5_A1348ContagemResultado_DataHomologacao[0];
               n1348ContagemResultado_DataHomologacao = P008S5_n1348ContagemResultado_DataHomologacao[0];
               if ( ! ( A483ContagemResultado_StatusCnt == 5 ) )
               {
                  A483ContagemResultado_StatusCnt = 5;
                  new prc_alterastatusdmn(context ).execute( ref  AV9OSVinculada,  AV8StatusDmn,  0) ;
               }
               if ( StringUtil.StrCmp(AV8StatusDmn, "H") == 0 )
               {
                  A1348ContagemResultado_DataHomologacao = DateTimeUtil.ServerNow( context, "DEFAULT");
                  n1348ContagemResultado_DataHomologacao = false;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P008S6 */
               pr_default.execute(4, new Object[] {n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Using cursor P008S7 */
               pr_default.execute(5, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P008S8 */
               pr_default.execute(6, new Object[] {n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, A456ContagemResultado_Codigo});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Using cursor P008S9 */
               pr_default.execute(7, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            new prc_disparoservicovinculado(context ).execute(  AV10Codigo,  AV11UserId) ;
            new prc_disparoservicovinculado(context ).execute(  AV9OSVinculada,  AV11UserId) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_REAlteraStatusDaDmn");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008S2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008S2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008S2_A456ContagemResultado_Codigo = new int[1] ;
         P008S2_A602ContagemResultado_OSVinculada = new int[1] ;
         P008S2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P008S2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P008S2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P008S2_A601ContagemResultado_Servico = new int[1] ;
         P008S2_n601ContagemResultado_Servico = new bool[] {false} ;
         P008S2_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P008S2_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         P008S5_A517ContagemResultado_Ultima = new bool[] {false} ;
         P008S5_A456ContagemResultado_Codigo = new int[1] ;
         P008S5_A483ContagemResultado_StatusCnt = new short[1] ;
         P008S5_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P008S5_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P008S5_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008S5_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_realterastatusdadmn__default(),
            new Object[][] {
                new Object[] {
               P008S2_A1553ContagemResultado_CntSrvCod, P008S2_n1553ContagemResultado_CntSrvCod, P008S2_A456ContagemResultado_Codigo, P008S2_A602ContagemResultado_OSVinculada, P008S2_n602ContagemResultado_OSVinculada, P008S2_A490ContagemResultado_ContratadaCod, P008S2_n490ContagemResultado_ContratadaCod, P008S2_A601ContagemResultado_Servico, P008S2_n601ContagemResultado_Servico, P008S2_A1348ContagemResultado_DataHomologacao,
               P008S2_n1348ContagemResultado_DataHomologacao
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P008S5_A517ContagemResultado_Ultima, P008S5_A456ContagemResultado_Codigo, P008S5_A483ContagemResultado_StatusCnt, P008S5_A1348ContagemResultado_DataHomologacao, P008S5_n1348ContagemResultado_DataHomologacao, P008S5_A473ContagemResultado_DataCnt, P008S5_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A483ContagemResultado_StatusCnt ;
      private int AV10Codigo ;
      private int AV11UserId ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV9OSVinculada ;
      private int AV13Prestadora ;
      private int AV14Servico ;
      private String AV8StatusDmn ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool A517ContagemResultado_Ultima ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008S2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008S2_n1553ContagemResultado_CntSrvCod ;
      private int[] P008S2_A456ContagemResultado_Codigo ;
      private int[] P008S2_A602ContagemResultado_OSVinculada ;
      private bool[] P008S2_n602ContagemResultado_OSVinculada ;
      private int[] P008S2_A490ContagemResultado_ContratadaCod ;
      private bool[] P008S2_n490ContagemResultado_ContratadaCod ;
      private int[] P008S2_A601ContagemResultado_Servico ;
      private bool[] P008S2_n601ContagemResultado_Servico ;
      private DateTime[] P008S2_A1348ContagemResultado_DataHomologacao ;
      private bool[] P008S2_n1348ContagemResultado_DataHomologacao ;
      private bool[] P008S5_A517ContagemResultado_Ultima ;
      private int[] P008S5_A456ContagemResultado_Codigo ;
      private short[] P008S5_A483ContagemResultado_StatusCnt ;
      private DateTime[] P008S5_A1348ContagemResultado_DataHomologacao ;
      private bool[] P008S5_n1348ContagemResultado_DataHomologacao ;
      private DateTime[] P008S5_A473ContagemResultado_DataCnt ;
      private String[] P008S5_A511ContagemResultado_HoraCnt ;
   }

   public class prc_realterastatusdadmn__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008S2 ;
          prmP008S2 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S3 ;
          prmP008S3 = new Object[] {
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S4 ;
          prmP008S4 = new Object[] {
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S5 ;
          prmP008S5 = new Object[] {
          new Object[] {"@AV9OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S6 ;
          prmP008S6 = new Object[] {
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S7 ;
          prmP008S7 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP008S8 ;
          prmP008S8 = new Object[] {
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008S9 ;
          prmP008S9 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008S2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_ContratadaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DataHomologacao] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV10Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008S2,1,0,true,true )
             ,new CursorDef("P008S3", "UPDATE [ContagemResultado] SET [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S3)
             ,new CursorDef("P008S4", "UPDATE [ContagemResultado] SET [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S4)
             ,new CursorDef("P008S5", "SELECT TOP 1 T1.[ContagemResultado_Ultima], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV9OSVinculada) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008S5,1,0,true,true )
             ,new CursorDef("P008S6", "UPDATE [ContagemResultado] SET [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S6)
             ,new CursorDef("P008S7", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S7)
             ,new CursorDef("P008S8", "UPDATE [ContagemResultado] SET [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S8)
             ,new CursorDef("P008S9", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008S9)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
