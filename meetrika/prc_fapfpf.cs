/*
               File: PRC_FAPFPF
        Description: Funcao APF Pontos de Fun��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fapfpf : GXProcedure
   {
      public prc_fapfpf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fapfpf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref decimal aP1_FuncaoAPF_PF )
      {
         this.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV9FuncaoAPF_PF = aP1_FuncaoAPF_PF;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_PF=this.AV9FuncaoAPF_PF;
      }

      public decimal executeUdp( ref int aP0_FuncaoAPF_Codigo )
      {
         this.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV9FuncaoAPF_PF = aP1_FuncaoAPF_PF;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_PF=this.AV9FuncaoAPF_PF;
         return AV9FuncaoAPF_PF ;
      }

      public void executeSubmit( ref int aP0_FuncaoAPF_Codigo ,
                                 ref decimal aP1_FuncaoAPF_PF )
      {
         prc_fapfpf objprc_fapfpf;
         objprc_fapfpf = new prc_fapfpf();
         objprc_fapfpf.AV8FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objprc_fapfpf.AV9FuncaoAPF_PF = aP1_FuncaoAPF_PF;
         objprc_fapfpf.context.SetSubmitInitialConfig(context);
         objprc_fapfpf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fapfpf);
         aP0_FuncaoAPF_Codigo=this.AV8FuncaoAPF_Codigo;
         aP1_FuncaoAPF_PF=this.AV9FuncaoAPF_PF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fapfpf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9FuncaoAPF_PF = 0;
         /* Using cursor P001Y2 */
         pr_default.execute(0, new Object[] {AV8FuncaoAPF_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1146FuncaoAPF_Tecnica = P001Y2_A1146FuncaoAPF_Tecnica[0];
            n1146FuncaoAPF_Tecnica = P001Y2_n1146FuncaoAPF_Tecnica[0];
            A184FuncaoAPF_Tipo = P001Y2_A184FuncaoAPF_Tipo[0];
            A165FuncaoAPF_Codigo = P001Y2_A165FuncaoAPF_Codigo[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( A1146FuncaoAPF_Tecnica == 1 )
            {
            }
            else if ( A1146FuncaoAPF_Tecnica == 2 )
            {
               if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "EE") == 0 )
               {
                  AV9FuncaoAPF_PF = (decimal)(4);
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "CE") == 0 )
               {
                  AV9FuncaoAPF_PF = (decimal)(4);
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "SE") == 0 )
               {
                  AV9FuncaoAPF_PF = (decimal)(5);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "E") == 0 )
               {
                  AV9FuncaoAPF_PF = 0;
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "NM") == 0 )
               {
                  AV9FuncaoAPF_PF = 0;
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "EE") == 0 )
               {
                  if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "B") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(3);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "M") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(4);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "A") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(6);
                  }
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "SE") == 0 )
               {
                  if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "B") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(4);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "M") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(5);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "A") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(7);
                  }
               }
               else if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "CE") == 0 )
               {
                  if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "B") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(3);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "M") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(4);
                  }
                  else if ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, "A") == 0 )
                  {
                     AV9FuncaoAPF_PF = (decimal)(6);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001Y2_A1146FuncaoAPF_Tecnica = new short[1] ;
         P001Y2_n1146FuncaoAPF_Tecnica = new bool[] {false} ;
         P001Y2_A184FuncaoAPF_Tipo = new String[] {""} ;
         P001Y2_A165FuncaoAPF_Codigo = new int[1] ;
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fapfpf__default(),
            new Object[][] {
                new Object[] {
               P001Y2_A1146FuncaoAPF_Tecnica, P001Y2_n1146FuncaoAPF_Tecnica, P001Y2_A184FuncaoAPF_Tipo, P001Y2_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1146FuncaoAPF_Tecnica ;
      private int AV8FuncaoAPF_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private decimal AV9FuncaoAPF_PF ;
      private String scmdbuf ;
      private String A184FuncaoAPF_Tipo ;
      private String A185FuncaoAPF_Complexidade ;
      private String GXt_char1 ;
      private bool n1146FuncaoAPF_Tecnica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private decimal aP1_FuncaoAPF_PF ;
      private IDataStoreProvider pr_default ;
      private short[] P001Y2_A1146FuncaoAPF_Tecnica ;
      private bool[] P001Y2_n1146FuncaoAPF_Tecnica ;
      private String[] P001Y2_A184FuncaoAPF_Tipo ;
      private int[] P001Y2_A165FuncaoAPF_Codigo ;
   }

   public class prc_fapfpf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001Y2 ;
          prmP001Y2 = new Object[] {
          new Object[] {"@AV8FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001Y2", "SELECT [FuncaoAPF_Tecnica], [FuncaoAPF_Tipo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV8FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001Y2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
