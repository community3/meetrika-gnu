/*
               File: GetPromptContratoServicosFilterData
        Description: Get Prompt Contrato Servicos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:16:54.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoservicosfilterdata : GXProcedure
   {
      public getpromptcontratoservicosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoservicosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoservicosfilterdata objgetpromptcontratoservicosfilterdata;
         objgetpromptcontratoservicosfilterdata = new getpromptcontratoservicosfilterdata();
         objgetpromptcontratoservicosfilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptcontratoservicosfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoservicosfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoservicosfilterdata.AV26OptionsJson = "" ;
         objgetpromptcontratoservicosfilterdata.AV29OptionsDescJson = "" ;
         objgetpromptcontratoservicosfilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptcontratoservicosfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoservicosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoservicosfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoservicosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptContratoServicosGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoServicosGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptContratoServicosGridState"), "");
         }
         AV60GXV1 = 1;
         while ( AV60GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV60GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV10TFContratoServicos_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoServicos_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV14TFContrato_Numero = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV15TFContrato_Numero_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICO_CODIGO") == 0 )
            {
               AV16TFServico_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV17TFServico_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV18TFServico_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV19TFServico_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            AV60GXV1 = (int)(AV60GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV39ContratoServicos_Codigo1 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 )
            {
               AV40Contrato_Codigo1 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV41Contrato_Numero1 = AV37GridStateDynamicFilter.gxTpr_Value;
               AV42Contrato_Numero_To1 = AV37GridStateDynamicFilter.gxTpr_Valueto;
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV43Servico_Nome1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 )
               {
                  AV46ContratoServicos_Codigo2 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 )
               {
                  AV47Contrato_Codigo2 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV48Contrato_Numero2 = AV37GridStateDynamicFilter.gxTpr_Value;
                  AV49Contrato_Numero_To2 = AV37GridStateDynamicFilter.gxTpr_Valueto;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV50Servico_Nome2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV51DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV52DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 )
                  {
                     AV53ContratoServicos_Codigo3 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 )
                  {
                     AV54Contrato_Codigo3 = (int)(NumberUtil.Val( AV37GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV55Contrato_Numero3 = AV37GridStateDynamicFilter.gxTpr_Value;
                     AV56Contrato_Numero_To3 = AV37GridStateDynamicFilter.gxTpr_Valueto;
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV57Servico_Nome3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV14TFContrato_Numero = AV20SearchTxt;
         AV15TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ContratoServicos_Codigo1 ,
                                              AV40Contrato_Codigo1 ,
                                              AV41Contrato_Numero1 ,
                                              AV42Contrato_Numero_To1 ,
                                              AV43Servico_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46ContratoServicos_Codigo2 ,
                                              AV47Contrato_Codigo2 ,
                                              AV48Contrato_Numero2 ,
                                              AV49Contrato_Numero_To2 ,
                                              AV50Servico_Nome2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53ContratoServicos_Codigo3 ,
                                              AV54Contrato_Codigo3 ,
                                              AV55Contrato_Numero3 ,
                                              AV56Contrato_Numero_To3 ,
                                              AV57Servico_Nome3 ,
                                              AV10TFContratoServicos_Codigo ,
                                              AV11TFContratoServicos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFServico_Codigo ,
                                              AV17TFServico_Codigo_To ,
                                              AV19TFServico_Nome_Sel ,
                                              AV18TFServico_Nome ,
                                              A160ContratoServicos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A608Servico_Nome ,
                                              A155Servico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV43Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43Servico_Nome1), 50, "%");
         lV50Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Servico_Nome2), 50, "%");
         lV57Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV57Servico_Nome3), 50, "%");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV18TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV18TFServico_Nome), 50, "%");
         /* Using cursor P00JV2 */
         pr_default.execute(0, new Object[] {AV39ContratoServicos_Codigo1, AV40Contrato_Codigo1, AV41Contrato_Numero1, AV42Contrato_Numero_To1, lV43Servico_Nome1, AV46ContratoServicos_Codigo2, AV47Contrato_Codigo2, AV48Contrato_Numero2, AV49Contrato_Numero_To2, lV50Servico_Nome2, AV53ContratoServicos_Codigo3, AV54Contrato_Codigo3, AV55Contrato_Numero3, AV56Contrato_Numero_To3, lV57Servico_Nome3, AV10TFContratoServicos_Codigo, AV11TFContratoServicos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFServico_Codigo, AV17TFServico_Codigo_To, lV18TFServico_Nome, AV19TFServico_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJV2 = false;
            A77Contrato_Numero = P00JV2_A77Contrato_Numero[0];
            A155Servico_Codigo = P00JV2_A155Servico_Codigo[0];
            A608Servico_Nome = P00JV2_A608Servico_Nome[0];
            A74Contrato_Codigo = P00JV2_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = P00JV2_A160ContratoServicos_Codigo[0];
            A608Servico_Nome = P00JV2_A608Servico_Nome[0];
            A77Contrato_Numero = P00JV2_A77Contrato_Numero[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JV2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKJV2 = false;
               A74Contrato_Codigo = P00JV2_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P00JV2_A160ContratoServicos_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKJV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV24Option = A77Contrato_Numero;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJV2 )
            {
               BRKJV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV18TFServico_Nome = AV20SearchTxt;
         AV19TFServico_Nome_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ContratoServicos_Codigo1 ,
                                              AV40Contrato_Codigo1 ,
                                              AV41Contrato_Numero1 ,
                                              AV42Contrato_Numero_To1 ,
                                              AV43Servico_Nome1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46ContratoServicos_Codigo2 ,
                                              AV47Contrato_Codigo2 ,
                                              AV48Contrato_Numero2 ,
                                              AV49Contrato_Numero_To2 ,
                                              AV50Servico_Nome2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53ContratoServicos_Codigo3 ,
                                              AV54Contrato_Codigo3 ,
                                              AV55Contrato_Numero3 ,
                                              AV56Contrato_Numero_To3 ,
                                              AV57Servico_Nome3 ,
                                              AV10TFContratoServicos_Codigo ,
                                              AV11TFContratoServicos_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFServico_Codigo ,
                                              AV17TFServico_Codigo_To ,
                                              AV19TFServico_Nome_Sel ,
                                              AV18TFServico_Nome ,
                                              A160ContratoServicos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A608Servico_Nome ,
                                              A155Servico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV43Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV43Servico_Nome1), 50, "%");
         lV50Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV50Servico_Nome2), 50, "%");
         lV57Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV57Servico_Nome3), 50, "%");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV18TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV18TFServico_Nome), 50, "%");
         /* Using cursor P00JV3 */
         pr_default.execute(1, new Object[] {AV39ContratoServicos_Codigo1, AV40Contrato_Codigo1, AV41Contrato_Numero1, AV42Contrato_Numero_To1, lV43Servico_Nome1, AV46ContratoServicos_Codigo2, AV47Contrato_Codigo2, AV48Contrato_Numero2, AV49Contrato_Numero_To2, lV50Servico_Nome2, AV53ContratoServicos_Codigo3, AV54Contrato_Codigo3, AV55Contrato_Numero3, AV56Contrato_Numero_To3, lV57Servico_Nome3, AV10TFContratoServicos_Codigo, AV11TFContratoServicos_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFServico_Codigo, AV17TFServico_Codigo_To, lV18TFServico_Nome, AV19TFServico_Nome_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJV4 = false;
            A155Servico_Codigo = P00JV3_A155Servico_Codigo[0];
            A608Servico_Nome = P00JV3_A608Servico_Nome[0];
            A77Contrato_Numero = P00JV3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00JV3_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = P00JV3_A160ContratoServicos_Codigo[0];
            A608Servico_Nome = P00JV3_A608Servico_Nome[0];
            A77Contrato_Numero = P00JV3_A77Contrato_Numero[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00JV3_A155Servico_Codigo[0] == A155Servico_Codigo ) )
            {
               BRKJV4 = false;
               A160ContratoServicos_Codigo = P00JV3_A160ContratoServicos_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKJV4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV24Option = A608Servico_Nome;
               AV23InsertIndex = 1;
               while ( ( AV23InsertIndex <= AV25Options.Count ) && ( StringUtil.StrCmp(((String)AV25Options.Item(AV23InsertIndex)), AV24Option) < 0 ) )
               {
                  AV23InsertIndex = (int)(AV23InsertIndex+1);
               }
               AV25Options.Add(AV24Option, AV23InsertIndex);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), AV23InsertIndex);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJV4 )
            {
               BRKJV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContrato_Numero = "";
         AV15TFContrato_Numero_Sel = "";
         AV18TFServico_Nome = "";
         AV19TFServico_Nome_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV41Contrato_Numero1 = "";
         AV42Contrato_Numero_To1 = "";
         AV43Servico_Nome1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV48Contrato_Numero2 = "";
         AV49Contrato_Numero_To2 = "";
         AV50Servico_Nome2 = "";
         AV52DynamicFiltersSelector3 = "";
         AV55Contrato_Numero3 = "";
         AV56Contrato_Numero_To3 = "";
         AV57Servico_Nome3 = "";
         scmdbuf = "";
         lV43Servico_Nome1 = "";
         lV50Servico_Nome2 = "";
         lV57Servico_Nome3 = "";
         lV14TFContrato_Numero = "";
         lV18TFServico_Nome = "";
         A77Contrato_Numero = "";
         A608Servico_Nome = "";
         P00JV2_A77Contrato_Numero = new String[] {""} ;
         P00JV2_A155Servico_Codigo = new int[1] ;
         P00JV2_A608Servico_Nome = new String[] {""} ;
         P00JV2_A74Contrato_Codigo = new int[1] ;
         P00JV2_A160ContratoServicos_Codigo = new int[1] ;
         AV24Option = "";
         P00JV3_A155Servico_Codigo = new int[1] ;
         P00JV3_A608Servico_Nome = new String[] {""} ;
         P00JV3_A77Contrato_Numero = new String[] {""} ;
         P00JV3_A74Contrato_Codigo = new int[1] ;
         P00JV3_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoservicosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JV2_A77Contrato_Numero, P00JV2_A155Servico_Codigo, P00JV2_A608Servico_Nome, P00JV2_A74Contrato_Codigo, P00JV2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00JV3_A155Servico_Codigo, P00JV3_A608Servico_Nome, P00JV3_A77Contrato_Numero, P00JV3_A74Contrato_Codigo, P00JV3_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV60GXV1 ;
      private int AV10TFContratoServicos_Codigo ;
      private int AV11TFContratoServicos_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int AV16TFServico_Codigo ;
      private int AV17TFServico_Codigo_To ;
      private int AV39ContratoServicos_Codigo1 ;
      private int AV40Contrato_Codigo1 ;
      private int AV46ContratoServicos_Codigo2 ;
      private int AV47Contrato_Codigo2 ;
      private int AV53ContratoServicos_Codigo3 ;
      private int AV54Contrato_Codigo3 ;
      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int AV23InsertIndex ;
      private long AV32count ;
      private String AV14TFContrato_Numero ;
      private String AV15TFContrato_Numero_Sel ;
      private String AV18TFServico_Nome ;
      private String AV19TFServico_Nome_Sel ;
      private String AV41Contrato_Numero1 ;
      private String AV42Contrato_Numero_To1 ;
      private String AV43Servico_Nome1 ;
      private String AV48Contrato_Numero2 ;
      private String AV49Contrato_Numero_To2 ;
      private String AV50Servico_Nome2 ;
      private String AV55Contrato_Numero3 ;
      private String AV56Contrato_Numero_To3 ;
      private String AV57Servico_Nome3 ;
      private String scmdbuf ;
      private String lV43Servico_Nome1 ;
      private String lV50Servico_Nome2 ;
      private String lV57Servico_Nome3 ;
      private String lV14TFContrato_Numero ;
      private String lV18TFServico_Nome ;
      private String A77Contrato_Numero ;
      private String A608Servico_Nome ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV51DynamicFiltersEnabled3 ;
      private bool BRKJV2 ;
      private bool BRKJV4 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV52DynamicFiltersSelector3 ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00JV2_A77Contrato_Numero ;
      private int[] P00JV2_A155Servico_Codigo ;
      private String[] P00JV2_A608Servico_Nome ;
      private int[] P00JV2_A74Contrato_Codigo ;
      private int[] P00JV2_A160ContratoServicos_Codigo ;
      private int[] P00JV3_A155Servico_Codigo ;
      private String[] P00JV3_A608Servico_Nome ;
      private String[] P00JV3_A77Contrato_Numero ;
      private int[] P00JV3_A74Contrato_Codigo ;
      private int[] P00JV3_A160ContratoServicos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getpromptcontratoservicosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JV2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             int AV39ContratoServicos_Codigo1 ,
                                             int AV40Contrato_Codigo1 ,
                                             String AV41Contrato_Numero1 ,
                                             String AV42Contrato_Numero_To1 ,
                                             String AV43Servico_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             int AV46ContratoServicos_Codigo2 ,
                                             int AV47Contrato_Codigo2 ,
                                             String AV48Contrato_Numero2 ,
                                             String AV49Contrato_Numero_To2 ,
                                             String AV50Servico_Nome2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             int AV53ContratoServicos_Codigo3 ,
                                             int AV54Contrato_Codigo3 ,
                                             String AV55Contrato_Numero3 ,
                                             String AV56Contrato_Numero_To3 ,
                                             String AV57Servico_Nome3 ,
                                             int AV10TFContratoServicos_Codigo ,
                                             int AV11TFContratoServicos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFServico_Codigo ,
                                             int AV17TFServico_Codigo_To ,
                                             String AV19TFServico_Nome_Sel ,
                                             String AV18TFServico_Nome ,
                                             int A160ContratoServicos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             String A608Servico_Nome ,
                                             int A155Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contrato_Numero], T1.[Servico_Codigo], T2.[Servico_Nome], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV39ContratoServicos_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV39ContratoServicos_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV39ContratoServicos_Codigo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV40Contrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV41Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV41Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero_To1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV43Servico_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV43Servico_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV46ContratoServicos_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV46ContratoServicos_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV46ContratoServicos_Codigo2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV47Contrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV47Contrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV47Contrato_Codigo2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV48Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV48Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49Contrato_Numero_To2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV49Contrato_Numero_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV49Contrato_Numero_To2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV50Servico_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV50Servico_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV53ContratoServicos_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV53ContratoServicos_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV53ContratoServicos_Codigo3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV54Contrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV54Contrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV54Contrato_Codigo3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV55Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV55Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Contrato_Numero_To3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV56Contrato_Numero_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV56Contrato_Numero_To3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV57Servico_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV57Servico_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV10TFContratoServicos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] >= @AV10TFContratoServicos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] >= @AV10TFContratoServicos_Codigo)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV11TFContratoServicos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] <= @AV11TFContratoServicos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] <= @AV11TFContratoServicos_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV16TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV17TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV18TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV18TFServico_Nome)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV19TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV19TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JV3( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             int AV39ContratoServicos_Codigo1 ,
                                             int AV40Contrato_Codigo1 ,
                                             String AV41Contrato_Numero1 ,
                                             String AV42Contrato_Numero_To1 ,
                                             String AV43Servico_Nome1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             int AV46ContratoServicos_Codigo2 ,
                                             int AV47Contrato_Codigo2 ,
                                             String AV48Contrato_Numero2 ,
                                             String AV49Contrato_Numero_To2 ,
                                             String AV50Servico_Nome2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             int AV53ContratoServicos_Codigo3 ,
                                             int AV54Contrato_Codigo3 ,
                                             String AV55Contrato_Numero3 ,
                                             String AV56Contrato_Numero_To3 ,
                                             String AV57Servico_Nome3 ,
                                             int AV10TFContratoServicos_Codigo ,
                                             int AV11TFContratoServicos_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFServico_Codigo ,
                                             int AV17TFServico_Codigo_To ,
                                             String AV19TFServico_Nome_Sel ,
                                             String AV18TFServico_Nome ,
                                             int A160ContratoServicos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             String A608Servico_Nome ,
                                             int A155Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T2.[Servico_Nome], T3.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV39ContratoServicos_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV39ContratoServicos_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV39ContratoServicos_Codigo1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV40Contrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV40Contrato_Codigo1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV41Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV41Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero_To1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV42Contrato_Numero_To1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV43Servico_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV43Servico_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV46ContratoServicos_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV46ContratoServicos_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV46ContratoServicos_Codigo2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV47Contrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV47Contrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV47Contrato_Codigo2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV48Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV48Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49Contrato_Numero_To2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV49Contrato_Numero_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV49Contrato_Numero_To2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV50Servico_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV50Servico_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV53ContratoServicos_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV53ContratoServicos_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV53ContratoServicos_Codigo3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_CODIGO") == 0 ) && ( ! (0==AV54Contrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV54Contrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV54Contrato_Codigo3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] >= @AV55Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] >= @AV55Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Contrato_Numero_To3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] <= @AV56Contrato_Numero_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] <= @AV56Contrato_Numero_To3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV57Servico_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV57Servico_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV10TFContratoServicos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] >= @AV10TFContratoServicos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] >= @AV10TFContratoServicos_Codigo)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV11TFContratoServicos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] <= @AV11TFContratoServicos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] <= @AV11TFContratoServicos_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV16TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV17TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV18TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV18TFServico_Nome)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV19TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV19TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JV2(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] );
               case 1 :
                     return conditional_P00JV3(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JV2 ;
          prmP00JV2 = new Object[] {
          new Object[] {"@AV39ContratoServicos_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40Contrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV42Contrato_Numero_To1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV43Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContratoServicos_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47Contrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49Contrato_Numero_To2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV50Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContratoServicos_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54Contrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV56Contrato_Numero_To3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV57Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFServico_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00JV3 ;
          prmP00JV3 = new Object[] {
          new Object[] {"@AV39ContratoServicos_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40Contrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@AV42Contrato_Numero_To1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV43Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContratoServicos_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47Contrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@AV49Contrato_Numero_To2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV50Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContratoServicos_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV54Contrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV56Contrato_Numero_To3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV57Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoServicos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19TFServico_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JV2,100,0,true,false )
             ,new CursorDef("P00JV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JV3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoservicosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoservicosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoservicosfilterdata") )
          {
             return  ;
          }
          getpromptcontratoservicosfilterdata worker = new getpromptcontratoservicosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
