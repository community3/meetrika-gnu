/*
               File: PRC_ImportarPFFS
        Description: Stub for PRC_ImportarPFFS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/20/2020 0:21:53.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_importarpffs : GXProcedure
   {
      public prc_importarpffs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_importarpffs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Aba ,
                           short aP2_ColDmnn ,
                           short aP3_PraLinha ,
                           short aP4_ColPFBFSn ,
                           short aP5_ColPFLFSn ,
                           short aP6_ColPFBFMn ,
                           short aP7_ColPFLFMn ,
                           bool aP8_Final ,
                           int aP9_ContratadaFS_Codigo ,
                           int aP10_ContadorFS_Codigo ,
                           DateTime aP11_DataCnt ,
                           short aP12_ColDataCntn ,
                           String aP13_RegraDivergencia ,
                           String aP14_FileName ,
                           DateTime aP15_DataDmn ,
                           DateTime aP16_DataEntrega ,
                           String aP17_DemandaFM ,
                           ref int aP18_ContratoservicosFS_Codigo )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Aba = aP1_Aba;
         this.AV4ColDmnn = aP2_ColDmnn;
         this.AV5PraLinha = aP3_PraLinha;
         this.AV6ColPFBFSn = aP4_ColPFBFSn;
         this.AV7ColPFLFSn = aP5_ColPFLFSn;
         this.AV8ColPFBFMn = aP6_ColPFBFMn;
         this.AV9ColPFLFMn = aP7_ColPFLFMn;
         this.AV10Final = aP8_Final;
         this.AV11ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         this.AV12ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         this.AV13DataCnt = aP11_DataCnt;
         this.AV14ColDataCntn = aP12_ColDataCntn;
         this.AV15RegraDivergencia = aP13_RegraDivergencia;
         this.AV16FileName = aP14_FileName;
         this.AV17DataDmn = aP15_DataDmn;
         this.AV18DataEntrega = aP16_DataEntrega;
         this.AV19DemandaFM = aP17_DemandaFM;
         this.AV20ContratoservicosFS_Codigo = aP18_ContratoservicosFS_Codigo;
         initialize();
         executePrivate();
         aP18_ContratoservicosFS_Codigo=this.AV20ContratoservicosFS_Codigo;
      }

      public int executeUdp( String aP0_Arquivo ,
                             String aP1_Aba ,
                             short aP2_ColDmnn ,
                             short aP3_PraLinha ,
                             short aP4_ColPFBFSn ,
                             short aP5_ColPFLFSn ,
                             short aP6_ColPFBFMn ,
                             short aP7_ColPFLFMn ,
                             bool aP8_Final ,
                             int aP9_ContratadaFS_Codigo ,
                             int aP10_ContadorFS_Codigo ,
                             DateTime aP11_DataCnt ,
                             short aP12_ColDataCntn ,
                             String aP13_RegraDivergencia ,
                             String aP14_FileName ,
                             DateTime aP15_DataDmn ,
                             DateTime aP16_DataEntrega ,
                             String aP17_DemandaFM )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Aba = aP1_Aba;
         this.AV4ColDmnn = aP2_ColDmnn;
         this.AV5PraLinha = aP3_PraLinha;
         this.AV6ColPFBFSn = aP4_ColPFBFSn;
         this.AV7ColPFLFSn = aP5_ColPFLFSn;
         this.AV8ColPFBFMn = aP6_ColPFBFMn;
         this.AV9ColPFLFMn = aP7_ColPFLFMn;
         this.AV10Final = aP8_Final;
         this.AV11ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         this.AV12ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         this.AV13DataCnt = aP11_DataCnt;
         this.AV14ColDataCntn = aP12_ColDataCntn;
         this.AV15RegraDivergencia = aP13_RegraDivergencia;
         this.AV16FileName = aP14_FileName;
         this.AV17DataDmn = aP15_DataDmn;
         this.AV18DataEntrega = aP16_DataEntrega;
         this.AV19DemandaFM = aP17_DemandaFM;
         this.AV20ContratoservicosFS_Codigo = aP18_ContratoservicosFS_Codigo;
         initialize();
         executePrivate();
         aP18_ContratoservicosFS_Codigo=this.AV20ContratoservicosFS_Codigo;
         return AV20ContratoservicosFS_Codigo ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Aba ,
                                 short aP2_ColDmnn ,
                                 short aP3_PraLinha ,
                                 short aP4_ColPFBFSn ,
                                 short aP5_ColPFLFSn ,
                                 short aP6_ColPFBFMn ,
                                 short aP7_ColPFLFMn ,
                                 bool aP8_Final ,
                                 int aP9_ContratadaFS_Codigo ,
                                 int aP10_ContadorFS_Codigo ,
                                 DateTime aP11_DataCnt ,
                                 short aP12_ColDataCntn ,
                                 String aP13_RegraDivergencia ,
                                 String aP14_FileName ,
                                 DateTime aP15_DataDmn ,
                                 DateTime aP16_DataEntrega ,
                                 String aP17_DemandaFM ,
                                 ref int aP18_ContratoservicosFS_Codigo )
      {
         prc_importarpffs objprc_importarpffs;
         objprc_importarpffs = new prc_importarpffs();
         objprc_importarpffs.AV2Arquivo = aP0_Arquivo;
         objprc_importarpffs.AV3Aba = aP1_Aba;
         objprc_importarpffs.AV4ColDmnn = aP2_ColDmnn;
         objprc_importarpffs.AV5PraLinha = aP3_PraLinha;
         objprc_importarpffs.AV6ColPFBFSn = aP4_ColPFBFSn;
         objprc_importarpffs.AV7ColPFLFSn = aP5_ColPFLFSn;
         objprc_importarpffs.AV8ColPFBFMn = aP6_ColPFBFMn;
         objprc_importarpffs.AV9ColPFLFMn = aP7_ColPFLFMn;
         objprc_importarpffs.AV10Final = aP8_Final;
         objprc_importarpffs.AV11ContratadaFS_Codigo = aP9_ContratadaFS_Codigo;
         objprc_importarpffs.AV12ContadorFS_Codigo = aP10_ContadorFS_Codigo;
         objprc_importarpffs.AV13DataCnt = aP11_DataCnt;
         objprc_importarpffs.AV14ColDataCntn = aP12_ColDataCntn;
         objprc_importarpffs.AV15RegraDivergencia = aP13_RegraDivergencia;
         objprc_importarpffs.AV16FileName = aP14_FileName;
         objprc_importarpffs.AV17DataDmn = aP15_DataDmn;
         objprc_importarpffs.AV18DataEntrega = aP16_DataEntrega;
         objprc_importarpffs.AV19DemandaFM = aP17_DemandaFM;
         objprc_importarpffs.AV20ContratoservicosFS_Codigo = aP18_ContratoservicosFS_Codigo;
         objprc_importarpffs.context.SetSubmitInitialConfig(context);
         objprc_importarpffs.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_importarpffs);
         aP18_ContratoservicosFS_Codigo=this.AV20ContratoservicosFS_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_importarpffs)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Arquivo,(String)AV3Aba,(short)AV4ColDmnn,(short)AV5PraLinha,(short)AV6ColPFBFSn,(short)AV7ColPFLFSn,(short)AV8ColPFBFMn,(short)AV9ColPFLFMn,(bool)AV10Final,(int)AV11ContratadaFS_Codigo,(int)AV12ContadorFS_Codigo,(DateTime)AV13DataCnt,(short)AV14ColDataCntn,(String)AV15RegraDivergencia,(String)AV16FileName,(DateTime)AV17DataDmn,(DateTime)AV18DataEntrega,(String)AV19DemandaFM,(int)AV20ContratoservicosFS_Codigo} ;
         ClassLoader.Execute("aprc_importarpffs","GeneXus.Programs.aprc_importarpffs", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 19 ) )
         {
            AV20ContratoservicosFS_Codigo = (int)(args[18]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV4ColDmnn ;
      private short AV5PraLinha ;
      private short AV6ColPFBFSn ;
      private short AV7ColPFLFSn ;
      private short AV8ColPFBFMn ;
      private short AV9ColPFLFMn ;
      private short AV14ColDataCntn ;
      private int AV11ContratadaFS_Codigo ;
      private int AV12ContadorFS_Codigo ;
      private int AV20ContratoservicosFS_Codigo ;
      private String AV2Arquivo ;
      private String AV3Aba ;
      private String AV15RegraDivergencia ;
      private String AV16FileName ;
      private DateTime AV13DataCnt ;
      private DateTime AV17DataDmn ;
      private DateTime AV18DataEntrega ;
      private bool AV10Final ;
      private String AV19DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP18_ContratoservicosFS_Codigo ;
      private Object[] args ;
   }

}
