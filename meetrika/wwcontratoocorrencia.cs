/*
               File: WWContratoOcorrencia
        Description:  Contrato Ocorr�ncia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:36:28.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoocorrencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoocorrencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoocorrencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         dynContratoOcorrencia_NaoCnfCod = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLACONTRATOOCORRENCIA_NAOCNFCOD772( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_87 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_87_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_87_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
               AV16ContratoOcorrencia_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
               AV17ContratoOcorrencia_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
               AV39ContratoOcorrencia_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoOcorrencia_Descricao1", AV39ContratoOcorrencia_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
               AV20ContratoOcorrencia_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
               AV21ContratoOcorrencia_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
               AV41ContratoOcorrencia_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoOcorrencia_Descricao2", AV41ContratoOcorrencia_Descricao2);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV45TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Numero", AV45TFContrato_Numero);
               AV46TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero_Sel", AV46TFContrato_Numero_Sel);
               AV49TFContratoOcorrencia_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
               AV50TFContratoOcorrencia_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
               AV55TFContratoOcorrencia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoOcorrencia_Descricao", AV55TFContratoOcorrencia_Descricao);
               AV56TFContratoOcorrencia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrencia_Descricao_Sel", AV56TFContratoOcorrencia_Descricao_Sel);
               AV63TFContratoOcorrencia_NaoCnfCod = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrencia_NaoCnfCod", AV63TFContratoOcorrencia_NaoCnfCod);
               AV66TFContratoOcorrencia_NaoCnfCod_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
               AV47ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_NumeroTitleControlIdToReplace", AV47ddo_Contrato_NumeroTitleControlIdToReplace);
               AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
               AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
               AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace", AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace);
               AV91Pgmname = GetNextPar( );
               AV67TFContratoOcorrencia_NaoCnfCod_SelDsc = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrencia_NaoCnfCod_SelDsc", AV67TFContratoOcorrencia_NaoCnfCod_SelDsc);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A294ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA772( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START772( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117362933");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoocorrencia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA_TO1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DESCRICAO1", AV39ContratoOcorrencia_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DATA_TO2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIA_DESCRICAO2", AV41ContratoOcorrencia_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV45TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV46TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DATA", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DATA_TO", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO", AV55TFContratoOcorrencia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL", AV56TFContratoOcorrencia_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD", StringUtil.RTrim( AV63TFContratoOcorrencia_NaoCnfCod));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_87", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_87), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV58DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV58DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV44Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV44Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_DATATITLEFILTERDATA", AV48ContratoOcorrencia_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_DATATITLEFILTERDATA", AV48ContratoOcorrencia_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA", AV54ContratoOcorrencia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA", AV54ContratoOcorrencia_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_NAOCNFCODTITLEFILTERDATA", AV62ContratoOcorrencia_NaoCnfCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_NAOCNFCODTITLEFILTERDATA", AV62ContratoOcorrencia_NaoCnfCodTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV91Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencia_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencia_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Selectedtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_naocnfcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_naocnfcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_naocnfcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_naocnfcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_naocnfcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencia_naocnfcod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencia_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencia_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_naocnfcod_Selectedtext_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE772( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT772( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoocorrencia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoOcorrencia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Ocorr�ncia" ;
      }

      protected void WB770( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_772( true) ;
         }
         else
         {
            wb_table1_2_772( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_772e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV45TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV45TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV46TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV46TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencia_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_data_Internalname, context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"), context.localUtil.Format( AV49TFContratoOcorrencia_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencia_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencia_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencia_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_data_to_Internalname, context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"), context.localUtil.Format( AV50TFContratoOcorrencia_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencia_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencia_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencia_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencia_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencia_dataauxdate_Internalname, context.localUtil.Format(AV51DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"), context.localUtil.Format( AV51DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencia_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencia_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencia_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencia_dataauxdateto_Internalname, context.localUtil.Format(AV52DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV52DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencia_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_descricao_Internalname, AV55TFContratoOcorrencia_Descricao, StringUtil.RTrim( context.localUtil.Format( AV55TFContratoOcorrencia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_descricao_sel_Internalname, AV56TFContratoOcorrencia_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV56TFContratoOcorrencia_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_naocnfcod_Internalname, StringUtil.RTrim( AV63TFContratoOcorrencia_NaoCnfCod), StringUtil.RTrim( context.localUtil.Format( AV63TFContratoOcorrencia_NaoCnfCod, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_naocnfcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_naocnfcod_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_naocnfcod_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_naocnfcod_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_naocnfcod_sel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_naocnfcod_seldsc_Internalname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, StringUtil.RTrim( context.localUtil.Format( AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_naocnfcod_seldsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_naocnfcod_seldsc_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV47ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_NAOCNFCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Internalname, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrencia.htm");
         }
         wbLoad = true;
      }

      protected void START772( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Ocorr�ncia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP770( ) ;
      }

      protected void WS772( )
      {
         START772( ) ;
         EVT772( ) ;
      }

      protected void EVT772( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11772 */
                              E11772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12772 */
                              E12772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13772 */
                              E13772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14772 */
                              E14772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_NAOCNFCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15772 */
                              E15772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16772 */
                              E16772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17772 */
                              E17772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18772 */
                              E18772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19772 */
                              E19772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20772 */
                              E20772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21772 */
                              E21772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22772 */
                              E22772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23772 */
                              E23772 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_87_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
                              SubsflControlProps_872( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV89Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV90Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A295ContratoOcorrencia_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrencia_Data_Internalname), 0));
                              A296ContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrencia_Descricao_Internalname));
                              dynContratoOcorrencia_NaoCnfCod.Name = dynContratoOcorrencia_NaoCnfCod_Internalname;
                              dynContratoOcorrencia_NaoCnfCod.CurrentValue = cgiGet( dynContratoOcorrencia_NaoCnfCod_Internalname);
                              A2027ContratoOcorrencia_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynContratoOcorrencia_NaoCnfCod_Internalname), "."));
                              n2027ContratoOcorrencia_NaoCnfCod = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24772 */
                                    E24772 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25772 */
                                    E25772 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26772 */
                                    E26772 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV38DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA1"), 0) != AV16ContratoOcorrencia_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO1"), 0) != AV17ContratoOcorrencia_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO1"), AV39ContratoOcorrencia_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV40DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA2"), 0) != AV20ContratoOcorrencia_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO2"), 0) != AV21ContratoOcorrencia_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencia_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO2"), AV41ContratoOcorrencia_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV45TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV46TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA"), 0) != AV49TFContratoOcorrencia_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA_TO"), 0) != AV50TFContratoOcorrencia_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO"), AV55TFContratoOcorrencia_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL"), AV56TFContratoOcorrencia_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_naocnfcod Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD"), AV63TFContratoOcorrencia_NaoCnfCod) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencia_naocnfcod_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL"), ",", ".") != Convert.ToDecimal( AV66TFContratoOcorrencia_NaoCnfCod_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE772( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA772( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIA_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIA_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIA_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
            }
            GXCCtl = "CONTRATOOCORRENCIA_NAOCNFCOD_" + sGXsfl_87_idx;
            dynContratoOcorrencia_NaoCnfCod.Name = GXCCtl;
            dynContratoOcorrencia_NaoCnfCod.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOOCORRENCIA_NAOCNFCOD772( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data772( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOOCORRENCIA_NAOCNFCOD_html772( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data772( ) ;
         gxdynajaxindex = 1;
         dynContratoOcorrencia_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoOcorrencia_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOOCORRENCIA_NAOCNFCOD_data772( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00772 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00772_A2027ContratoOcorrencia_NaoCnfCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00772_A427NaoConformidade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_872( ) ;
         while ( nGXsfl_87_idx <= nRC_GXsfl_87 )
         {
            sendrow_872( ) ;
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV38DynamicFiltersOperator1 ,
                                       DateTime AV16ContratoOcorrencia_Data1 ,
                                       DateTime AV17ContratoOcorrencia_Data_To1 ,
                                       String AV39ContratoOcorrencia_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV40DynamicFiltersOperator2 ,
                                       DateTime AV20ContratoOcorrencia_Data2 ,
                                       DateTime AV21ContratoOcorrencia_Data_To2 ,
                                       String AV41ContratoOcorrencia_Descricao2 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       String AV45TFContrato_Numero ,
                                       String AV46TFContrato_Numero_Sel ,
                                       DateTime AV49TFContratoOcorrencia_Data ,
                                       DateTime AV50TFContratoOcorrencia_Data_To ,
                                       String AV55TFContratoOcorrencia_Descricao ,
                                       String AV56TFContratoOcorrencia_Descricao_Sel ,
                                       String AV63TFContratoOcorrencia_NaoCnfCod ,
                                       int AV66TFContratoOcorrencia_NaoCnfCod_Sel ,
                                       String AV47ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace ,
                                       String AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace ,
                                       String AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace ,
                                       String AV91Pgmname ,
                                       String AV67TFContratoOcorrencia_NaoCnfCod_SelDsc ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A294ContratoOcorrencia_Codigo ,
                                       int A74Contrato_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF772( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DATA", GetSecureSignedToken( "", A295ContratoOcorrencia_Data));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_DATA", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_DESCRICAO", A296ContratoOcorrencia_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_NAOCNFCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_NAOCNFCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF772( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV91Pgmname = "WWContratoOcorrencia";
         context.Gx_err = 0;
      }

      protected void RF772( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 87;
         /* Execute user event: E25772 */
         E25772 ();
         nGXsfl_87_idx = 1;
         sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
         SubsflControlProps_872( ) ;
         nGXsfl_87_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_872( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                                 AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                                 AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                                 AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                                 AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                                 AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                                 AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                                 AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                                 AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                                 AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                                 AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                                 AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                                 AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                                 AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                                 AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                                 AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                                 AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                                 AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                                 AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                                 A295ContratoOcorrencia_Data ,
                                                 A296ContratoOcorrencia_Descricao ,
                                                 A77Contrato_Numero ,
                                                 A427NaoConformidade_Nome ,
                                                 A2027ContratoOcorrencia_NaoCnfCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
            lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
            lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
            lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
            lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero), 20, "%");
            lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao), "%", "");
            lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod), 50, "%");
            /* Using cursor H00773 */
            pr_default.execute(1, new Object[] {AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1, AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1, lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2, AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2, lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero, AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel, AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data, AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to, lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao, AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel, lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod, AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_87_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A427NaoConformidade_Nome = H00773_A427NaoConformidade_Nome[0];
               n427NaoConformidade_Nome = H00773_n427NaoConformidade_Nome[0];
               A74Contrato_Codigo = H00773_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = H00773_A294ContratoOcorrencia_Codigo[0];
               A2027ContratoOcorrencia_NaoCnfCod = H00773_A2027ContratoOcorrencia_NaoCnfCod[0];
               n2027ContratoOcorrencia_NaoCnfCod = H00773_n2027ContratoOcorrencia_NaoCnfCod[0];
               A296ContratoOcorrencia_Descricao = H00773_A296ContratoOcorrencia_Descricao[0];
               A295ContratoOcorrencia_Data = H00773_A295ContratoOcorrencia_Data[0];
               A77Contrato_Numero = H00773_A77Contrato_Numero[0];
               A77Contrato_Numero = H00773_A77Contrato_Numero[0];
               A427NaoConformidade_Nome = H00773_A427NaoConformidade_Nome[0];
               n427NaoConformidade_Nome = H00773_n427NaoConformidade_Nome[0];
               /* Execute user event: E26772 */
               E26772 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 87;
            WB770( ) ;
         }
         nGXsfl_87_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                              AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                              AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                              AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                              AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                              AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                              AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                              AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                              AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                              AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                              AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                              AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                              AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                              AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                              AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                              AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                              A295ContratoOcorrencia_Data ,
                                              A296ContratoOcorrencia_Descricao ,
                                              A77Contrato_Numero ,
                                              A427NaoConformidade_Nome ,
                                              A2027ContratoOcorrencia_NaoCnfCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1), "%", "");
         lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2), "%", "");
         lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero), 20, "%");
         lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = StringUtil.Concat( StringUtil.RTrim( AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao), "%", "");
         lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod), 50, "%");
         /* Using cursor H00774 */
         pr_default.execute(2, new Object[] {AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1, AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1, lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1, AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2, AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2, lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2, lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero, AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel, AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data, AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to, lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao, AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel, lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod, AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel});
         GRID_nRecordCount = H00774_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP770( )
      {
         /* Before Start, stand alone formulas. */
         AV91Pgmname = "WWContratoOcorrencia";
         context.Gx_err = 0;
         GXACONTRATOOCORRENCIA_NAOCNFCOD_html772( ) ;
         /* Using cursor H00775 */
         pr_default.execute(3, new Object[] {n2027ContratoOcorrencia_NaoCnfCod, A2027ContratoOcorrencia_NaoCnfCod});
         A427NaoConformidade_Nome = H00775_A427NaoConformidade_Nome[0];
         n427NaoConformidade_Nome = H00775_n427NaoConformidade_Nome[0];
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24772 */
         E24772 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV58DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV44Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_DATATITLEFILTERDATA"), AV48ContratoOcorrencia_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA"), AV54ContratoOcorrencia_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_NAOCNFCODTITLEFILTERDATA"), AV62ContratoOcorrencia_NaoCnfCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data1"}), 1, "vCONTRATOOCORRENCIA_DATA1");
               GX_FocusControl = edtavContratoocorrencia_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoOcorrencia_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
            }
            else
            {
               AV16ContratoOcorrencia_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data_To1"}), 1, "vCONTRATOOCORRENCIA_DATA_TO1");
               GX_FocusControl = edtavContratoocorrencia_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoOcorrencia_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            else
            {
               AV17ContratoOcorrencia_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            AV39ContratoOcorrencia_Descricao1 = StringUtil.Upper( cgiGet( edtavContratoocorrencia_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoOcorrencia_Descricao1", AV39ContratoOcorrencia_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data2"}), 1, "vCONTRATOOCORRENCIA_DATA2");
               GX_FocusControl = edtavContratoocorrencia_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoOcorrencia_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
            }
            else
            {
               AV20ContratoOcorrencia_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencia_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia_Data_To2"}), 1, "vCONTRATOOCORRENCIA_DATA_TO2");
               GX_FocusControl = edtavContratoocorrencia_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoOcorrencia_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
            }
            else
            {
               AV21ContratoOcorrencia_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencia_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
            }
            AV41ContratoOcorrencia_Descricao2 = StringUtil.Upper( cgiGet( edtavContratoocorrencia_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoOcorrencia_Descricao2", AV41ContratoOcorrencia_Descricao2);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV45TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Numero", AV45TFContrato_Numero);
            AV46TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero_Sel", AV46TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencia_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia_Data"}), 1, "vTFCONTRATOOCORRENCIA_DATA");
               GX_FocusControl = edtavTfcontratoocorrencia_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFContratoOcorrencia_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
            }
            else
            {
               AV49TFContratoOcorrencia_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencia_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencia_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia_Data_To"}), 1, "vTFCONTRATOOCORRENCIA_DATA_TO");
               GX_FocusControl = edtavTfcontratoocorrencia_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContratoOcorrencia_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
            }
            else
            {
               AV50TFContratoOcorrencia_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencia_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencia_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia_Data Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIA_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencia_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_ContratoOcorrencia_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_ContratoOcorrencia_DataAuxDate", context.localUtil.Format(AV51DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV51DDO_ContratoOcorrencia_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencia_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_ContratoOcorrencia_DataAuxDate", context.localUtil.Format(AV51DDO_ContratoOcorrencia_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencia_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia_Data Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIA_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencia_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52DDO_ContratoOcorrencia_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContratoOcorrencia_DataAuxDateTo", context.localUtil.Format(AV52DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV52DDO_ContratoOcorrencia_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencia_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContratoOcorrencia_DataAuxDateTo", context.localUtil.Format(AV52DDO_ContratoOcorrencia_DataAuxDateTo, "99/99/99"));
            }
            AV55TFContratoOcorrencia_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencia_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoOcorrencia_Descricao", AV55TFContratoOcorrencia_Descricao);
            AV56TFContratoOcorrencia_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencia_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrencia_Descricao_Sel", AV56TFContratoOcorrencia_Descricao_Sel);
            AV63TFContratoOcorrencia_NaoCnfCod = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencia_naocnfcod_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrencia_NaoCnfCod", AV63TFContratoOcorrencia_NaoCnfCod);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_naocnfcod_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_naocnfcod_sel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL");
               GX_FocusControl = edtavTfcontratoocorrencia_naocnfcod_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66TFContratoOcorrencia_NaoCnfCod_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
            }
            else
            {
               AV66TFContratoOcorrencia_NaoCnfCod_Sel = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_naocnfcod_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
            }
            AV67TFContratoOcorrencia_NaoCnfCod_SelDsc = cgiGet( edtavTfcontratoocorrencia_naocnfcod_seldsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrencia_NaoCnfCod_SelDsc", AV67TFContratoOcorrencia_NaoCnfCod_SelDsc);
            AV47ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_NumeroTitleControlIdToReplace", AV47ddo_Contrato_NumeroTitleControlIdToReplace);
            AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
            AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
            AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace", AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_87 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_87"), ",", "."));
            AV60GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV61GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratoocorrencia_data_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Caption");
            Ddo_contratoocorrencia_data_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Tooltip");
            Ddo_contratoocorrencia_data_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Cls");
            Ddo_contratoocorrencia_data_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_set");
            Ddo_contratoocorrencia_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_set");
            Ddo_contratoocorrencia_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Dropdownoptionstype");
            Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includesortasc"));
            Ddo_contratoocorrencia_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includesortdsc"));
            Ddo_contratoocorrencia_data_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortedstatus");
            Ddo_contratoocorrencia_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includefilter"));
            Ddo_contratoocorrencia_data_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filtertype");
            Ddo_contratoocorrencia_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filterisrange"));
            Ddo_contratoocorrencia_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Includedatalist"));
            Ddo_contratoocorrencia_data_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortasc");
            Ddo_contratoocorrencia_data_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Sortdsc");
            Ddo_contratoocorrencia_data_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Cleanfilter");
            Ddo_contratoocorrencia_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterfrom");
            Ddo_contratoocorrencia_data_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Rangefilterto");
            Ddo_contratoocorrencia_data_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Searchbuttontext");
            Ddo_contratoocorrencia_descricao_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Caption");
            Ddo_contratoocorrencia_descricao_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Tooltip");
            Ddo_contratoocorrencia_descricao_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cls");
            Ddo_contratoocorrencia_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_set");
            Ddo_contratoocorrencia_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_set");
            Ddo_contratoocorrencia_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortasc"));
            Ddo_contratoocorrencia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includesortdsc"));
            Ddo_contratoocorrencia_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortedstatus");
            Ddo_contratoocorrencia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includefilter"));
            Ddo_contratoocorrencia_descricao_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filtertype");
            Ddo_contratoocorrencia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filterisrange"));
            Ddo_contratoocorrencia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Includedatalist"));
            Ddo_contratoocorrencia_descricao_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalisttype");
            Ddo_contratoocorrencia_descricao_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistproc");
            Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencia_descricao_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortasc");
            Ddo_contratoocorrencia_descricao_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Sortdsc");
            Ddo_contratoocorrencia_descricao_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Loadingdata");
            Ddo_contratoocorrencia_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Cleanfilter");
            Ddo_contratoocorrencia_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Noresultsfound");
            Ddo_contratoocorrencia_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Searchbuttontext");
            Ddo_contratoocorrencia_naocnfcod_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Caption");
            Ddo_contratoocorrencia_naocnfcod_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Tooltip");
            Ddo_contratoocorrencia_naocnfcod_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Cls");
            Ddo_contratoocorrencia_naocnfcod_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filteredtext_set");
            Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedvalue_set");
            Ddo_contratoocorrencia_naocnfcod_Selectedtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedtext_set");
            Ddo_contratoocorrencia_naocnfcod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Dropdownoptionstype");
            Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_naocnfcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includesortasc"));
            Ddo_contratoocorrencia_naocnfcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includesortdsc"));
            Ddo_contratoocorrencia_naocnfcod_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortedstatus");
            Ddo_contratoocorrencia_naocnfcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includefilter"));
            Ddo_contratoocorrencia_naocnfcod_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filtertype");
            Ddo_contratoocorrencia_naocnfcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filterisrange"));
            Ddo_contratoocorrencia_naocnfcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Includedatalist"));
            Ddo_contratoocorrencia_naocnfcod_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalisttype");
            Ddo_contratoocorrencia_naocnfcod_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalistproc");
            Ddo_contratoocorrencia_naocnfcod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencia_naocnfcod_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortasc");
            Ddo_contratoocorrencia_naocnfcod_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Sortdsc");
            Ddo_contratoocorrencia_naocnfcod_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Loadingdata");
            Ddo_contratoocorrencia_naocnfcod_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Cleanfilter");
            Ddo_contratoocorrencia_naocnfcod_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Noresultsfound");
            Ddo_contratoocorrencia_naocnfcod_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratoocorrencia_data_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Activeeventkey");
            Ddo_contratoocorrencia_data_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtext_get");
            Ddo_contratoocorrencia_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DATA_Filteredtextto_get");
            Ddo_contratoocorrencia_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Activeeventkey");
            Ddo_contratoocorrencia_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Filteredtext_get");
            Ddo_contratoocorrencia_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIA_DESCRICAO_Selectedvalue_get");
            Ddo_contratoocorrencia_naocnfcod_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Activeeventkey");
            Ddo_contratoocorrencia_naocnfcod_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Filteredtext_get");
            Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedvalue_get");
            Ddo_contratoocorrencia_naocnfcod_Selectedtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_NAOCNFCOD_Selectedtext_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV38DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA1"), 0) != AV16ContratoOcorrencia_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO1"), 0) != AV17ContratoOcorrencia_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO1"), AV39ContratoOcorrencia_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV40DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA2"), 0) != AV20ContratoOcorrencia_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIA_DATA_TO2"), 0) != AV21ContratoOcorrencia_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIA_DESCRICAO2"), AV41ContratoOcorrencia_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV45TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV46TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA"), 0) != AV49TFContratoOcorrencia_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DATA_TO"), 0) != AV50TFContratoOcorrencia_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO"), AV55TFContratoOcorrencia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_DESCRICAO_SEL"), AV56TFContratoOcorrencia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD"), AV63TFContratoOcorrencia_NaoCnfCod) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL"), ",", ".") != Convert.ToDecimal( AV66TFContratoOcorrencia_NaoCnfCod_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24772 */
         E24772 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24772( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencia_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_data_Visible), 5, 0)));
         edtavTfcontratoocorrencia_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_data_to_Visible), 5, 0)));
         edtavTfcontratoocorrencia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_descricao_Visible), 5, 0)));
         edtavTfcontratoocorrencia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencia_naocnfcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_naocnfcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_naocnfcod_Visible), 5, 0)));
         edtavTfcontratoocorrencia_naocnfcod_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_naocnfcod_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_naocnfcod_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencia_naocnfcod_seldsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_naocnfcod_seldsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_naocnfcod_seldsc_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV47ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_NumeroTitleControlIdToReplace", AV47ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_data_Titlecontrolidtoreplace);
         AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace = Ddo_contratoocorrencia_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace", AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace);
         AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace", AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_NaoCnfCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace);
         AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace = Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace", AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Ocorr�ncia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "N�o conformidade", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV58DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV58DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25772( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV44Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoOcorrencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ContratoOcorrencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrencia_NaoCnfCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV47ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoOcorrencia_Data_Titleformat = 2;
         edtContratoOcorrencia_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Data_Internalname, "Title", edtContratoOcorrencia_Data_Title);
         edtContratoOcorrencia_Descricao_Titleformat = 2;
         edtContratoOcorrencia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Descricao_Internalname, "Title", edtContratoOcorrencia_Descricao_Title);
         dynContratoOcorrencia_NaoCnfCod_Titleformat = 2;
         dynContratoOcorrencia_NaoCnfCod.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�o conformidade", AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Title", dynContratoOcorrencia_NaoCnfCod.Title.Text);
         AV60GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60GridCurrentPage), 10, 0)));
         AV61GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GridPageCount), 10, 0)));
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = AV16ContratoOcorrencia_Data1;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = AV17ContratoOcorrencia_Data_To1;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = AV39ContratoOcorrencia_Descricao1;
         AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = AV20ContratoOcorrencia_Data2;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = AV21ContratoOcorrencia_Data_To2;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = AV41ContratoOcorrencia_Descricao2;
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = AV45TFContrato_Numero;
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = AV46TFContrato_Numero_Sel;
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = AV49TFContratoOcorrencia_Data;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = AV50TFContratoOcorrencia_Data_To;
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = AV55TFContratoOcorrencia_Descricao;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = AV56TFContratoOcorrencia_Descricao_Sel;
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = AV63TFContratoOcorrencia_NaoCnfCod;
         AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel = AV66TFContratoOcorrencia_NaoCnfCod_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Contrato_NumeroTitleFilterData", AV44Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48ContratoOcorrencia_DataTitleFilterData", AV48ContratoOcorrencia_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54ContratoOcorrencia_DescricaoTitleFilterData", AV54ContratoOcorrencia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62ContratoOcorrencia_NaoCnfCodTitleFilterData", AV62ContratoOcorrencia_NaoCnfCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11772( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV59PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV59PageToGo) ;
         }
      }

      protected void E12772( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Numero", AV45TFContrato_Numero);
            AV46TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero_Sel", AV46TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13772( )
      {
         /* Ddo_contratoocorrencia_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContratoOcorrencia_Data = context.localUtil.CToD( Ddo_contratoocorrencia_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
            AV50TFContratoOcorrencia_Data_To = context.localUtil.CToD( Ddo_contratoocorrencia_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14772( )
      {
         /* Ddo_contratoocorrencia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFContratoOcorrencia_Descricao = Ddo_contratoocorrencia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoOcorrencia_Descricao", AV55TFContratoOcorrencia_Descricao);
            AV56TFContratoOcorrencia_Descricao_Sel = Ddo_contratoocorrencia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrencia_Descricao_Sel", AV56TFContratoOcorrencia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15772( )
      {
         /* Ddo_contratoocorrencia_naocnfcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_naocnfcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_naocnfcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SortedStatus", Ddo_contratoocorrencia_naocnfcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_naocnfcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_naocnfcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SortedStatus", Ddo_contratoocorrencia_naocnfcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_naocnfcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFContratoOcorrencia_NaoCnfCod = Ddo_contratoocorrencia_naocnfcod_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrencia_NaoCnfCod", AV63TFContratoOcorrencia_NaoCnfCod);
            AV66TFContratoOcorrencia_NaoCnfCod_Sel = (int)(NumberUtil.Val( Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
            AV67TFContratoOcorrencia_NaoCnfCod_SelDsc = Ddo_contratoocorrencia_naocnfcod_Selectedtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrencia_NaoCnfCod_SelDsc", AV67TFContratoOcorrencia_NaoCnfCod_SelDsc);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26772( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV89Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoocorrencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV90Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoocorrencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         edtContratoOcorrencia_Data_Link = formatLink("viewcontratoocorrencia.aspx") + "?" + UrlEncode("" +A294ContratoOcorrencia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 87;
         }
         sendrow_872( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_87_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(87, GridRow);
         }
      }

      protected void E16772( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21772( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17772( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E22772( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18772( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV16ContratoOcorrencia_Data1, AV17ContratoOcorrencia_Data_To1, AV39ContratoOcorrencia_Descricao1, AV19DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV20ContratoOcorrencia_Data2, AV21ContratoOcorrencia_Data_To2, AV41ContratoOcorrencia_Descricao2, AV18DynamicFiltersEnabled2, AV45TFContrato_Numero, AV46TFContrato_Numero_Sel, AV49TFContratoOcorrencia_Data, AV50TFContratoOcorrencia_Data_To, AV55TFContratoOcorrencia_Descricao, AV56TFContratoOcorrencia_Descricao_Sel, AV63TFContratoOcorrencia_NaoCnfCod, AV66TFContratoOcorrencia_NaoCnfCod_Sel, AV47ddo_Contrato_NumeroTitleControlIdToReplace, AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace, AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace, AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace, AV91Pgmname, AV67TFContratoOcorrencia_NaoCnfCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A294ContratoOcorrencia_Codigo, A74Contrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E23772( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19772( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E20772( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoocorrencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratoocorrencia_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
         Ddo_contratoocorrencia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
         Ddo_contratoocorrencia_naocnfcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SortedStatus", Ddo_contratoocorrencia_naocnfcod_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoocorrencia_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "SortedStatus", Ddo_contratoocorrencia_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoocorrencia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencia_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoocorrencia_naocnfcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SortedStatus", Ddo_contratoocorrencia_naocnfcod_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible), 5, 0)));
         edtavContratoocorrencia_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            edtavContratoocorrencia_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible), 5, 0)));
         edtavContratoocorrencia_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
         {
            edtavContratoocorrencia_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencia_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencia_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContratoOcorrencia_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
         AV21ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV45TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Numero", AV45TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV46TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero_Sel", AV46TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV49TFContratoOcorrencia_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
         Ddo_contratoocorrencia_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredText_set", Ddo_contratoocorrencia_data_Filteredtext_set);
         AV50TFContratoOcorrencia_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
         Ddo_contratoocorrencia_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencia_data_Filteredtextto_set);
         AV55TFContratoOcorrencia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoOcorrencia_Descricao", AV55TFContratoOcorrencia_Descricao);
         Ddo_contratoocorrencia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencia_descricao_Filteredtext_set);
         AV56TFContratoOcorrencia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrencia_Descricao_Sel", AV56TFContratoOcorrencia_Descricao_Sel);
         Ddo_contratoocorrencia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencia_descricao_Selectedvalue_set);
         AV63TFContratoOcorrencia_NaoCnfCod = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrencia_NaoCnfCod", AV63TFContratoOcorrencia_NaoCnfCod);
         Ddo_contratoocorrencia_naocnfcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "FilteredText_set", Ddo_contratoocorrencia_naocnfcod_Filteredtext_set);
         AV66TFContratoOcorrencia_NaoCnfCod_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
         Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SelectedValue_set", Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIA_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoOcorrencia_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
         AV17ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV91Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV91Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV91Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV92GXV1 = 1;
         while ( AV92GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV92GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV45TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Numero", AV45TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV45TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV46TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero_Sel", AV46TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV46TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV49TFContratoOcorrencia_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoOcorrencia_Data", context.localUtil.Format(AV49TFContratoOcorrencia_Data, "99/99/99"));
               AV50TFContratoOcorrencia_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrencia_Data_To", context.localUtil.Format(AV50TFContratoOcorrencia_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV49TFContratoOcorrencia_Data) )
               {
                  Ddo_contratoocorrencia_data_Filteredtext_set = context.localUtil.DToC( AV49TFContratoOcorrencia_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredText_set", Ddo_contratoocorrencia_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV50TFContratoOcorrencia_Data_To) )
               {
                  Ddo_contratoocorrencia_data_Filteredtextto_set = context.localUtil.DToC( AV50TFContratoOcorrencia_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencia_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV55TFContratoOcorrencia_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoOcorrencia_Descricao", AV55TFContratoOcorrencia_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratoOcorrencia_Descricao)) )
               {
                  Ddo_contratoocorrencia_descricao_Filteredtext_set = AV55TFContratoOcorrencia_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencia_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_DESCRICAO_SEL") == 0 )
            {
               AV56TFContratoOcorrencia_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrencia_Descricao_Sel", AV56TFContratoOcorrencia_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoOcorrencia_Descricao_Sel)) )
               {
                  Ddo_contratoocorrencia_descricao_Selectedvalue_set = AV56TFContratoOcorrencia_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencia_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD") == 0 )
            {
               AV63TFContratoOcorrencia_NaoCnfCod = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrencia_NaoCnfCod", AV63TFContratoOcorrencia_NaoCnfCod);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoOcorrencia_NaoCnfCod)) )
               {
                  Ddo_contratoocorrencia_naocnfcod_Filteredtext_set = AV63TFContratoOcorrencia_NaoCnfCod;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "FilteredText_set", Ddo_contratoocorrencia_naocnfcod_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_NAOCNFCOD_SEL") == 0 )
            {
               AV66TFContratoOcorrencia_NaoCnfCod_Sel = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratoOcorrencia_NaoCnfCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0)));
               if ( ! (0==AV66TFContratoOcorrencia_NaoCnfCod_Sel) )
               {
                  Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set = StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SelectedValue_set", Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set);
                  AV67TFContratoOcorrencia_NaoCnfCod_SelDsc = AV11GridStateFilterValue.gxTpr_Valueto;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrencia_NaoCnfCod_SelDsc", AV67TFContratoOcorrencia_NaoCnfCod_SelDsc);
                  Ddo_contratoocorrencia_naocnfcod_Selectedtext_set = AV67TFContratoOcorrencia_NaoCnfCod_SelDsc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_naocnfcod_Internalname, "SelectedText_set", Ddo_contratoocorrencia_naocnfcod_Selectedtext_set);
               }
            }
            AV92GXV1 = (int)(AV92GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 )
            {
               AV16ContratoOcorrencia_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoOcorrencia_Data1", context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"));
               AV17ContratoOcorrencia_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrencia_Data_To1", context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
            {
               AV38DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
               AV39ContratoOcorrencia_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoOcorrencia_Descricao1", AV39ContratoOcorrencia_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 )
               {
                  AV20ContratoOcorrencia_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoOcorrencia_Data2", context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"));
                  AV21ContratoOcorrencia_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoOcorrencia_Data_To2", context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
                  AV41ContratoOcorrencia_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoOcorrencia_Descricao2", AV41ContratoOcorrencia_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV91Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV49TFContratoOcorrencia_Data) && (DateTime.MinValue==AV50TFContratoOcorrencia_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV49TFContratoOcorrencia_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV50TFContratoOcorrencia_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratoOcorrencia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFContratoOcorrencia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoOcorrencia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratoOcorrencia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoOcorrencia_NaoCnfCod)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_NAOCNFCOD";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFContratoOcorrencia_NaoCnfCod;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV66TFContratoOcorrencia_NaoCnfCod_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_NAOCNFCOD_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV66TFContratoOcorrencia_NaoCnfCod_Sel), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = AV67TFContratoOcorrencia_NaoCnfCod_SelDsc;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV91Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16ContratoOcorrencia_Data1) && (DateTime.MinValue==AV17ContratoOcorrencia_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16ContratoOcorrencia_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17ContratoOcorrencia_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContratoOcorrencia_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39ContratoOcorrencia_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV38DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20ContratoOcorrencia_Data2) && (DateTime.MinValue==AV21ContratoOcorrencia_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20ContratoOcorrencia_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21ContratoOcorrencia_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ContratoOcorrencia_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41ContratoOcorrencia_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV40DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV91Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoOcorrencia";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_772( true) ;
         }
         else
         {
            wb_table2_8_772( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_772( true) ;
         }
         else
         {
            wb_table3_81_772( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_772e( true) ;
         }
         else
         {
            wb_table1_2_772e( false) ;
         }
      }

      protected void wb_table3_81_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_84_772( true) ;
         }
         else
         {
            wb_table4_84_772( false) ;
         }
         return  ;
      }

      protected void wb_table4_84_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_772e( true) ;
         }
         else
         {
            wb_table3_81_772e( false) ;
         }
      }

      protected void wb_table4_84_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"87\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrencia_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrencia_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrencia_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrencia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrencia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrencia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynContratoOcorrencia_NaoCnfCod_Titleformat == 0 )
               {
                  context.SendWebValue( dynContratoOcorrencia_NaoCnfCod.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynContratoOcorrencia_NaoCnfCod.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrencia_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrencia_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoOcorrencia_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A296ContratoOcorrencia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrencia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrencia_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynContratoOcorrencia_NaoCnfCod.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynContratoOcorrencia_NaoCnfCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 87 )
         {
            wbEnd = 0;
            nRC_GXsfl_87 = (short)(nGXsfl_87_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_84_772e( true) ;
         }
         else
         {
            wb_table4_84_772e( false) ;
         }
      }

      protected void wb_table2_8_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoocorrenciatitle_Internalname, "Ocorr�ncias", "", "", lblContratoocorrenciatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_772( true) ;
         }
         else
         {
            wb_table5_13_772( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoOcorrencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_772( true) ;
         }
         else
         {
            wb_table6_23_772( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_772e( true) ;
         }
         else
         {
            wb_table2_8_772e( false) ;
         }
      }

      protected void wb_table6_23_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_772( true) ;
         }
         else
         {
            wb_table7_28_772( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_772e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_772e( true) ;
         }
         else
         {
            wb_table6_23_772e( false) ;
         }
      }

      protected void wb_table7_28_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoOcorrencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_772( true) ;
         }
         else
         {
            wb_table8_37_772( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContratoOcorrencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_772( true) ;
         }
         else
         {
            wb_table9_62_772( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_772e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_772e( true) ;
         }
         else
         {
            wb_table7_28_772e( false) ;
         }
      }

      protected void wb_table9_62_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWContratoOcorrencia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_67_772( true) ;
         }
         else
         {
            wb_table10_67_772( false) ;
         }
         return  ;
      }

      protected void wb_table10_67_772e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_descricao2_Internalname, AV41ContratoOcorrencia_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV41ContratoOcorrencia_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencia_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_772e( true) ;
         }
         else
         {
            wb_table9_62_772e( false) ;
         }
      }

      protected void wb_table10_67_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data2_Internalname, context.localUtil.Format(AV20ContratoOcorrencia_Data2, "99/99/99"), context.localUtil.Format( AV20ContratoOcorrencia_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data_to2_Internalname, context.localUtil.Format(AV21ContratoOcorrencia_Data_To2, "99/99/99"), context.localUtil.Format( AV21ContratoOcorrencia_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_67_772e( true) ;
         }
         else
         {
            wb_table10_67_772e( false) ;
         }
      }

      protected void wb_table8_37_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoOcorrencia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_42_772( true) ;
         }
         else
         {
            wb_table11_42_772( false) ;
         }
         return  ;
      }

      protected void wb_table11_42_772e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_descricao1_Internalname, AV39ContratoOcorrencia_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV39ContratoOcorrencia_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencia_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_772e( true) ;
         }
         else
         {
            wb_table8_37_772e( false) ;
         }
      }

      protected void wb_table11_42_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data1_Internalname, context.localUtil.Format(AV16ContratoOcorrencia_Data1, "99/99/99"), context.localUtil.Format( AV16ContratoOcorrencia_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencia_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencia_data_to1_Internalname, context.localUtil.Format(AV17ContratoOcorrencia_Data_To1, "99/99/99"), context.localUtil.Format( AV17ContratoOcorrencia_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencia_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrencia.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencia_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_42_772e( true) ;
         }
         else
         {
            wb_table11_42_772e( false) ;
         }
      }

      protected void wb_table5_13_772( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_772e( true) ;
         }
         else
         {
            wb_table5_13_772e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA772( ) ;
         WS772( ) ;
         WE772( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117363581");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoocorrencia.js", "?20203117363582");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_idx;
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA_"+sGXsfl_87_idx;
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO_"+sGXsfl_87_idx;
         dynContratoOcorrencia_NaoCnfCod_Internalname = "CONTRATOOCORRENCIA_NAOCNFCOD_"+sGXsfl_87_idx;
      }

      protected void SubsflControlProps_fel_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA_"+sGXsfl_87_fel_idx;
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO_"+sGXsfl_87_fel_idx;
         dynContratoOcorrencia_NaoCnfCod_Internalname = "CONTRATOOCORRENCIA_NAOCNFCOD_"+sGXsfl_87_fel_idx;
      }

      protected void sendrow_872( )
      {
         SubsflControlProps_872( ) ;
         WB770( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_87_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_87_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_87_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV89Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV89Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV90Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV90Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Data_Internalname,context.localUtil.Format(A295ContratoOcorrencia_Data, "99/99/99"),context.localUtil.Format( A295ContratoOcorrencia_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoOcorrencia_Data_Link,(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Descricao_Internalname,(String)A296ContratoOcorrencia_Descricao,StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GXACONTRATOOCORRENCIA_NAOCNFCOD_html772( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_87_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOOCORRENCIA_NAOCNFCOD_" + sGXsfl_87_idx;
               dynContratoOcorrencia_NaoCnfCod.Name = GXCCtl;
               dynContratoOcorrencia_NaoCnfCod.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynContratoOcorrencia_NaoCnfCod,(String)dynContratoOcorrencia_NaoCnfCod_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0)),(short)1,(String)dynContratoOcorrencia_NaoCnfCod_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynContratoOcorrencia_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoOcorrencia_NaoCnfCod_Internalname, "Values", (String)(dynContratoOcorrencia_NaoCnfCod.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DATA"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, A295ContratoOcorrencia_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_DESCRICAO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A296ContratoOcorrencia_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_NAOCNFCOD"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, context.localUtil.Format( (decimal)(A2027ContratoOcorrencia_NaoCnfCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         /* End function sendrow_872 */
      }

      protected void init_default_properties( )
      {
         lblContratoocorrenciatitle_Internalname = "CONTRATOOCORRENCIATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoocorrencia_data1_Internalname = "vCONTRATOOCORRENCIA_DATA1";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIA_DATA_RANGEMIDDLETEXT1";
         edtavContratoocorrencia_data_to1_Internalname = "vCONTRATOOCORRENCIA_DATA_TO1";
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1";
         edtavContratoocorrencia_descricao1_Internalname = "vCONTRATOOCORRENCIA_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoocorrencia_data2_Internalname = "vCONTRATOOCORRENCIA_DATA2";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIA_DATA_RANGEMIDDLETEXT2";
         edtavContratoocorrencia_data_to2_Internalname = "vCONTRATOOCORRENCIA_DATA_TO2";
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2";
         edtavContratoocorrencia_descricao2_Internalname = "vCONTRATOOCORRENCIA_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoOcorrencia_Data_Internalname = "CONTRATOOCORRENCIA_DATA";
         edtContratoOcorrencia_Descricao_Internalname = "CONTRATOOCORRENCIA_DESCRICAO";
         dynContratoOcorrencia_NaoCnfCod_Internalname = "CONTRATOOCORRENCIA_NAOCNFCOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratoocorrencia_data_Internalname = "vTFCONTRATOOCORRENCIA_DATA";
         edtavTfcontratoocorrencia_data_to_Internalname = "vTFCONTRATOOCORRENCIA_DATA_TO";
         edtavDdo_contratoocorrencia_dataauxdate_Internalname = "vDDO_CONTRATOOCORRENCIA_DATAAUXDATE";
         edtavDdo_contratoocorrencia_dataauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIA_DATAAUXDATETO";
         divDdo_contratoocorrencia_dataauxdates_Internalname = "DDO_CONTRATOOCORRENCIA_DATAAUXDATES";
         edtavTfcontratoocorrencia_descricao_Internalname = "vTFCONTRATOOCORRENCIA_DESCRICAO";
         edtavTfcontratoocorrencia_descricao_sel_Internalname = "vTFCONTRATOOCORRENCIA_DESCRICAO_SEL";
         edtavTfcontratoocorrencia_naocnfcod_Internalname = "vTFCONTRATOOCORRENCIA_NAOCNFCOD";
         edtavTfcontratoocorrencia_naocnfcod_sel_Internalname = "vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL";
         edtavTfcontratoocorrencia_naocnfcod_seldsc_Internalname = "vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_data_Internalname = "DDO_CONTRATOOCORRENCIA_DATA";
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_descricao_Internalname = "DDO_CONTRATOOCORRENCIA_DESCRICAO";
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_naocnfcod_Internalname = "DDO_CONTRATOOCORRENCIA_NAOCNFCOD";
         edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynContratoOcorrencia_NaoCnfCod_Jsonclick = "";
         edtContratoOcorrencia_Descricao_Jsonclick = "";
         edtContratoOcorrencia_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavContratoocorrencia_data_to1_Jsonclick = "";
         edtavContratoocorrencia_data1_Jsonclick = "";
         edtavContratoocorrencia_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoocorrencia_data_to2_Jsonclick = "";
         edtavContratoocorrencia_data2_Jsonclick = "";
         edtavContratoocorrencia_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoOcorrencia_Data_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         dynContratoOcorrencia_NaoCnfCod_Titleformat = 0;
         edtContratoOcorrencia_Descricao_Titleformat = 0;
         edtContratoOcorrencia_Data_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoocorrencia_descricao2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoocorrencia_descricao1_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible = 1;
         dynContratoOcorrencia_NaoCnfCod.Title.Text = "N�o conformidade";
         edtContratoOcorrencia_Descricao_Title = "Descri��o";
         edtContratoOcorrencia_Data_Title = "Data";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoocorrencia_naocnfcod_seldsc_Jsonclick = "";
         edtavTfcontratoocorrencia_naocnfcod_seldsc_Visible = 1;
         edtavTfcontratoocorrencia_naocnfcod_sel_Jsonclick = "";
         edtavTfcontratoocorrencia_naocnfcod_sel_Visible = 1;
         edtavTfcontratoocorrencia_naocnfcod_Jsonclick = "";
         edtavTfcontratoocorrencia_naocnfcod_Visible = 1;
         edtavTfcontratoocorrencia_descricao_sel_Jsonclick = "";
         edtavTfcontratoocorrencia_descricao_sel_Visible = 1;
         edtavTfcontratoocorrencia_descricao_Jsonclick = "";
         edtavTfcontratoocorrencia_descricao_Visible = 1;
         edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencia_dataauxdate_Jsonclick = "";
         edtavTfcontratoocorrencia_data_to_Jsonclick = "";
         edtavTfcontratoocorrencia_data_to_Visible = 1;
         edtavTfcontratoocorrencia_data_Jsonclick = "";
         edtavTfcontratoocorrencia_data_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoocorrencia_naocnfcod_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_naocnfcod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencia_naocnfcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_naocnfcod_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencia_naocnfcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_naocnfcod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_naocnfcod_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencia_naocnfcod_Datalistproc = "GetWWContratoOcorrenciaFilterData";
         Ddo_contratoocorrencia_naocnfcod_Datalisttype = "Dynamic";
         Ddo_contratoocorrencia_naocnfcod_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_naocnfcod_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_naocnfcod_Filtertype = "Character";
         Ddo_contratoocorrencia_naocnfcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_naocnfcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_naocnfcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_naocnfcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_naocnfcod_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_naocnfcod_Tooltip = "Op��es";
         Ddo_contratoocorrencia_naocnfcod_Caption = "";
         Ddo_contratoocorrencia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencia_descricao_Datalistproc = "GetWWContratoOcorrenciaFilterData";
         Ddo_contratoocorrencia_descricao_Datalisttype = "Dynamic";
         Ddo_contratoocorrencia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_descricao_Filtertype = "Character";
         Ddo_contratoocorrencia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_descricao_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_descricao_Tooltip = "Op��es";
         Ddo_contratoocorrencia_descricao_Caption = "";
         Ddo_contratoocorrencia_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_data_Rangefilterto = "At�";
         Ddo_contratoocorrencia_data_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencia_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Filtertype = "Date";
         Ddo_contratoocorrencia_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_data_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_data_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_data_Tooltip = "Op��es";
         Ddo_contratoocorrencia_data_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoOcorrenciaFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Ocorr�ncia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV44Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV48ContratoOcorrencia_DataTitleFilterData',fld:'vCONTRATOOCORRENCIA_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV54ContratoOcorrencia_DescricaoTitleFilterData',fld:'vCONTRATOOCORRENCIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62ContratoOcorrencia_NaoCnfCodTitleFilterData',fld:'vCONTRATOOCORRENCIA_NAOCNFCODTITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoOcorrencia_Data_Titleformat',ctrl:'CONTRATOOCORRENCIA_DATA',prop:'Titleformat'},{av:'edtContratoOcorrencia_Data_Title',ctrl:'CONTRATOOCORRENCIA_DATA',prop:'Title'},{av:'edtContratoOcorrencia_Descricao_Titleformat',ctrl:'CONTRATOOCORRENCIA_DESCRICAO',prop:'Titleformat'},{av:'edtContratoOcorrencia_Descricao_Title',ctrl:'CONTRATOOCORRENCIA_DESCRICAO',prop:'Title'},{av:'dynContratoOcorrencia_NaoCnfCod'},{av:'AV60GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV61GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E12772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_naocnfcod_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_DATA.ONOPTIONCLICKED","{handler:'E13772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_data_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_data_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_data_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_naocnfcod_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E14772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_descricao_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_naocnfcod_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_NAOCNFCOD.ONOPTIONCLICKED","{handler:'E15772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_naocnfcod_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_naocnfcod_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SelectedValue_get'},{av:'Ddo_contratoocorrencia_naocnfcod_Selectedtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SelectedText_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_naocnfcod_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SortedStatus'},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26772',iparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoOcorrencia_Data_Link',ctrl:'CONTRATOOCORRENCIA_DATA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21772',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22772',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23772',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19772',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV47ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_NAOCNFCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV91Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV67TFContratoOcorrencia_NaoCnfCod_SelDsc',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV45TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV46TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV49TFContratoOcorrencia_Data',fld:'vTFCONTRATOOCORRENCIA_DATA',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredText_set'},{av:'AV50TFContratoOcorrencia_Data_To',fld:'vTFCONTRATOOCORRENCIA_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencia_data_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIA_DATA',prop:'FilteredTextTo_set'},{av:'AV55TFContratoOcorrencia_Descricao',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_contratoocorrencia_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV56TFContratoOcorrencia_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencia_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV63TFContratoOcorrencia_NaoCnfCod',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD',pic:'@!',nv:''},{av:'Ddo_contratoocorrencia_naocnfcod_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'FilteredText_set'},{av:'AV66TFContratoOcorrencia_NaoCnfCod_Sel',fld:'vTFCONTRATOOCORRENCIA_NAOCNFCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIA_NAOCNFCOD',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoOcorrencia_Data1',fld:'vCONTRATOOCORRENCIA_DATA1',pic:'',nv:''},{av:'AV17ContratoOcorrencia_Data_To1',fld:'vCONTRATOOCORRENCIA_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA1',prop:'Visible'},{av:'edtavContratoocorrencia_descricao1_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoOcorrencia_Data2',fld:'vCONTRATOOCORRENCIA_DATA2',pic:'',nv:''},{av:'AV21ContratoOcorrencia_Data_To2',fld:'vCONTRATOOCORRENCIA_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39ContratoOcorrencia_Descricao1',fld:'vCONTRATOOCORRENCIA_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41ContratoOcorrencia_Descricao2',fld:'vCONTRATOOCORRENCIA_DESCRICAO2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIA_DATA2',prop:'Visible'},{av:'edtavContratoocorrencia_descricao2_Visible',ctrl:'vCONTRATOOCORRENCIA_DESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20772',iparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratoocorrencia_data_Activeeventkey = "";
         Ddo_contratoocorrencia_data_Filteredtext_get = "";
         Ddo_contratoocorrencia_data_Filteredtextto_get = "";
         Ddo_contratoocorrencia_descricao_Activeeventkey = "";
         Ddo_contratoocorrencia_descricao_Filteredtext_get = "";
         Ddo_contratoocorrencia_descricao_Selectedvalue_get = "";
         Ddo_contratoocorrencia_naocnfcod_Activeeventkey = "";
         Ddo_contratoocorrencia_naocnfcod_Filteredtext_get = "";
         Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get = "";
         Ddo_contratoocorrencia_naocnfcod_Selectedtext_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoOcorrencia_Data1 = DateTime.MinValue;
         AV17ContratoOcorrencia_Data_To1 = DateTime.MinValue;
         AV39ContratoOcorrencia_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV20ContratoOcorrencia_Data2 = DateTime.MinValue;
         AV21ContratoOcorrencia_Data_To2 = DateTime.MinValue;
         AV41ContratoOcorrencia_Descricao2 = "";
         AV45TFContrato_Numero = "";
         AV46TFContrato_Numero_Sel = "";
         AV49TFContratoOcorrencia_Data = DateTime.MinValue;
         AV50TFContratoOcorrencia_Data_To = DateTime.MinValue;
         AV55TFContratoOcorrencia_Descricao = "";
         AV56TFContratoOcorrencia_Descricao_Sel = "";
         AV63TFContratoOcorrencia_NaoCnfCod = "";
         AV47ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace = "";
         AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace = "";
         AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace = "";
         AV91Pgmname = "";
         AV67TFContratoOcorrencia_NaoCnfCod_SelDsc = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV58DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV44Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoOcorrencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ContratoOcorrencia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrencia_NaoCnfCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratoocorrencia_data_Filteredtext_set = "";
         Ddo_contratoocorrencia_data_Filteredtextto_set = "";
         Ddo_contratoocorrencia_data_Sortedstatus = "";
         Ddo_contratoocorrencia_descricao_Filteredtext_set = "";
         Ddo_contratoocorrencia_descricao_Selectedvalue_set = "";
         Ddo_contratoocorrencia_descricao_Sortedstatus = "";
         Ddo_contratoocorrencia_naocnfcod_Filteredtext_set = "";
         Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set = "";
         Ddo_contratoocorrencia_naocnfcod_Selectedtext_set = "";
         Ddo_contratoocorrencia_naocnfcod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV51DDO_ContratoOcorrencia_DataAuxDate = DateTime.MinValue;
         AV52DDO_ContratoOcorrencia_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV89Update_GXI = "";
         AV29Delete = "";
         AV90Delete_GXI = "";
         A77Contrato_Numero = "";
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         A296ContratoOcorrencia_Descricao = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00772_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         H00772_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         H00772_A427NaoConformidade_Nome = new String[] {""} ;
         H00772_n427NaoConformidade_Nome = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = "";
         lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = "";
         lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = "";
         lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = "";
         lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = "";
         AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 = "";
         AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 = DateTime.MinValue;
         AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 = DateTime.MinValue;
         AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 = "";
         AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 = "";
         AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 = DateTime.MinValue;
         AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 = DateTime.MinValue;
         AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 = "";
         AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel = "";
         AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero = "";
         AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data = DateTime.MinValue;
         AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to = DateTime.MinValue;
         AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel = "";
         AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao = "";
         AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod = "";
         A427NaoConformidade_Nome = "";
         H00773_A427NaoConformidade_Nome = new String[] {""} ;
         H00773_n427NaoConformidade_Nome = new bool[] {false} ;
         H00773_A74Contrato_Codigo = new int[1] ;
         H00773_A294ContratoOcorrencia_Codigo = new int[1] ;
         H00773_A2027ContratoOcorrencia_NaoCnfCod = new int[1] ;
         H00773_n2027ContratoOcorrencia_NaoCnfCod = new bool[] {false} ;
         H00773_A296ContratoOcorrencia_Descricao = new String[] {""} ;
         H00773_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00773_A77Contrato_Numero = new String[] {""} ;
         H00774_AGRID_nRecordCount = new long[1] ;
         H00775_A427NaoConformidade_Nome = new String[] {""} ;
         H00775_n427NaoConformidade_Nome = new bool[] {false} ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoocorrenciatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoocorrencia__default(),
            new Object[][] {
                new Object[] {
               H00772_A2027ContratoOcorrencia_NaoCnfCod, H00772_A427NaoConformidade_Nome, H00772_n427NaoConformidade_Nome
               }
               , new Object[] {
               H00773_A427NaoConformidade_Nome, H00773_n427NaoConformidade_Nome, H00773_A74Contrato_Codigo, H00773_A294ContratoOcorrencia_Codigo, H00773_A2027ContratoOcorrencia_NaoCnfCod, H00773_n2027ContratoOcorrencia_NaoCnfCod, H00773_A296ContratoOcorrencia_Descricao, H00773_A295ContratoOcorrencia_Data, H00773_A77Contrato_Numero
               }
               , new Object[] {
               H00774_AGRID_nRecordCount
               }
               , new Object[] {
               H00775_A427NaoConformidade_Nome, H00775_n427NaoConformidade_Nome
               }
            }
         );
         AV91Pgmname = "WWContratoOcorrencia";
         /* GeneXus formulas. */
         AV91Pgmname = "WWContratoOcorrencia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_87 ;
      private short nGXsfl_87_idx=1 ;
      private short AV13OrderedBy ;
      private short AV38DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_87_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoOcorrencia_Data_Titleformat ;
      private short edtContratoOcorrencia_Descricao_Titleformat ;
      private short dynContratoOcorrencia_NaoCnfCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV66TFContratoOcorrencia_NaoCnfCod_Sel ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencia_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencia_naocnfcod_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratoocorrencia_data_Visible ;
      private int edtavTfcontratoocorrencia_data_to_Visible ;
      private int edtavTfcontratoocorrencia_descricao_Visible ;
      private int edtavTfcontratoocorrencia_descricao_sel_Visible ;
      private int edtavTfcontratoocorrencia_naocnfcod_Visible ;
      private int edtavTfcontratoocorrencia_naocnfcod_sel_Visible ;
      private int edtavTfcontratoocorrencia_naocnfcod_seldsc_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Visible ;
      private int A2027ContratoOcorrencia_NaoCnfCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ;
      private int edtavOrdereddsc_Visible ;
      private int AV59PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencia_data1_Visible ;
      private int edtavContratoocorrencia_descricao1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencia_data2_Visible ;
      private int edtavContratoocorrencia_descricao2_Visible ;
      private int AV92GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV60GridCurrentPage ;
      private long AV61GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratoocorrencia_data_Activeeventkey ;
      private String Ddo_contratoocorrencia_data_Filteredtext_get ;
      private String Ddo_contratoocorrencia_data_Filteredtextto_get ;
      private String Ddo_contratoocorrencia_descricao_Activeeventkey ;
      private String Ddo_contratoocorrencia_descricao_Filteredtext_get ;
      private String Ddo_contratoocorrencia_descricao_Selectedvalue_get ;
      private String Ddo_contratoocorrencia_naocnfcod_Activeeventkey ;
      private String Ddo_contratoocorrencia_naocnfcod_Filteredtext_get ;
      private String Ddo_contratoocorrencia_naocnfcod_Selectedvalue_get ;
      private String Ddo_contratoocorrencia_naocnfcod_Selectedtext_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_87_idx="0001" ;
      private String AV45TFContrato_Numero ;
      private String AV46TFContrato_Numero_Sel ;
      private String AV63TFContratoOcorrencia_NaoCnfCod ;
      private String AV91Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratoocorrencia_data_Caption ;
      private String Ddo_contratoocorrencia_data_Tooltip ;
      private String Ddo_contratoocorrencia_data_Cls ;
      private String Ddo_contratoocorrencia_data_Filteredtext_set ;
      private String Ddo_contratoocorrencia_data_Filteredtextto_set ;
      private String Ddo_contratoocorrencia_data_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_data_Sortedstatus ;
      private String Ddo_contratoocorrencia_data_Filtertype ;
      private String Ddo_contratoocorrencia_data_Sortasc ;
      private String Ddo_contratoocorrencia_data_Sortdsc ;
      private String Ddo_contratoocorrencia_data_Cleanfilter ;
      private String Ddo_contratoocorrencia_data_Rangefilterfrom ;
      private String Ddo_contratoocorrencia_data_Rangefilterto ;
      private String Ddo_contratoocorrencia_data_Searchbuttontext ;
      private String Ddo_contratoocorrencia_descricao_Caption ;
      private String Ddo_contratoocorrencia_descricao_Tooltip ;
      private String Ddo_contratoocorrencia_descricao_Cls ;
      private String Ddo_contratoocorrencia_descricao_Filteredtext_set ;
      private String Ddo_contratoocorrencia_descricao_Selectedvalue_set ;
      private String Ddo_contratoocorrencia_descricao_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_descricao_Sortedstatus ;
      private String Ddo_contratoocorrencia_descricao_Filtertype ;
      private String Ddo_contratoocorrencia_descricao_Datalisttype ;
      private String Ddo_contratoocorrencia_descricao_Datalistproc ;
      private String Ddo_contratoocorrencia_descricao_Sortasc ;
      private String Ddo_contratoocorrencia_descricao_Sortdsc ;
      private String Ddo_contratoocorrencia_descricao_Loadingdata ;
      private String Ddo_contratoocorrencia_descricao_Cleanfilter ;
      private String Ddo_contratoocorrencia_descricao_Noresultsfound ;
      private String Ddo_contratoocorrencia_descricao_Searchbuttontext ;
      private String Ddo_contratoocorrencia_naocnfcod_Caption ;
      private String Ddo_contratoocorrencia_naocnfcod_Tooltip ;
      private String Ddo_contratoocorrencia_naocnfcod_Cls ;
      private String Ddo_contratoocorrencia_naocnfcod_Filteredtext_set ;
      private String Ddo_contratoocorrencia_naocnfcod_Selectedvalue_set ;
      private String Ddo_contratoocorrencia_naocnfcod_Selectedtext_set ;
      private String Ddo_contratoocorrencia_naocnfcod_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_naocnfcod_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_naocnfcod_Sortedstatus ;
      private String Ddo_contratoocorrencia_naocnfcod_Filtertype ;
      private String Ddo_contratoocorrencia_naocnfcod_Datalisttype ;
      private String Ddo_contratoocorrencia_naocnfcod_Datalistproc ;
      private String Ddo_contratoocorrencia_naocnfcod_Sortasc ;
      private String Ddo_contratoocorrencia_naocnfcod_Sortdsc ;
      private String Ddo_contratoocorrencia_naocnfcod_Loadingdata ;
      private String Ddo_contratoocorrencia_naocnfcod_Cleanfilter ;
      private String Ddo_contratoocorrencia_naocnfcod_Noresultsfound ;
      private String Ddo_contratoocorrencia_naocnfcod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratoocorrencia_data_Internalname ;
      private String edtavTfcontratoocorrencia_data_Jsonclick ;
      private String edtavTfcontratoocorrencia_data_to_Internalname ;
      private String edtavTfcontratoocorrencia_data_to_Jsonclick ;
      private String divDdo_contratoocorrencia_dataauxdates_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdate_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencia_dataauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencia_dataauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencia_descricao_Internalname ;
      private String edtavTfcontratoocorrencia_descricao_Jsonclick ;
      private String edtavTfcontratoocorrencia_descricao_sel_Internalname ;
      private String edtavTfcontratoocorrencia_descricao_sel_Jsonclick ;
      private String edtavTfcontratoocorrencia_naocnfcod_Internalname ;
      private String edtavTfcontratoocorrencia_naocnfcod_Jsonclick ;
      private String edtavTfcontratoocorrencia_naocnfcod_sel_Internalname ;
      private String edtavTfcontratoocorrencia_naocnfcod_sel_Jsonclick ;
      private String edtavTfcontratoocorrencia_naocnfcod_seldsc_Internalname ;
      private String edtavTfcontratoocorrencia_naocnfcod_seldsc_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_naocnfcodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoOcorrencia_Data_Internalname ;
      private String edtContratoOcorrencia_Descricao_Internalname ;
      private String dynContratoOcorrencia_NaoCnfCod_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ;
      private String lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ;
      private String AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ;
      private String AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ;
      private String AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ;
      private String A427NaoConformidade_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoocorrencia_data1_Internalname ;
      private String edtavContratoocorrencia_data_to1_Internalname ;
      private String edtavContratoocorrencia_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoocorrencia_data2_Internalname ;
      private String edtavContratoocorrencia_data_to2_Internalname ;
      private String edtavContratoocorrencia_descricao2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratoocorrencia_data_Internalname ;
      private String Ddo_contratoocorrencia_descricao_Internalname ;
      private String Ddo_contratoocorrencia_naocnfcod_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoOcorrencia_Data_Title ;
      private String edtContratoOcorrencia_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoOcorrencia_Data_Link ;
      private String tblTablemergeddynamicfilterscontratoocorrencia_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencia_data2_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoocorrenciatitle_Internalname ;
      private String lblContratoocorrenciatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoocorrencia_descricao2_Jsonclick ;
      private String edtavContratoocorrencia_data2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencia_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoocorrencia_descricao1_Jsonclick ;
      private String edtavContratoocorrencia_data1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencia_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencia_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_87_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoOcorrencia_Data_Jsonclick ;
      private String edtContratoOcorrencia_Descricao_Jsonclick ;
      private String dynContratoOcorrencia_NaoCnfCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16ContratoOcorrencia_Data1 ;
      private DateTime AV17ContratoOcorrencia_Data_To1 ;
      private DateTime AV20ContratoOcorrencia_Data2 ;
      private DateTime AV21ContratoOcorrencia_Data_To2 ;
      private DateTime AV49TFContratoOcorrencia_Data ;
      private DateTime AV50TFContratoOcorrencia_Data_To ;
      private DateTime AV51DDO_ContratoOcorrencia_DataAuxDate ;
      private DateTime AV52DDO_ContratoOcorrencia_DataAuxDateTo ;
      private DateTime A295ContratoOcorrencia_Data ;
      private DateTime AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ;
      private DateTime AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ;
      private DateTime AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ;
      private DateTime AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ;
      private DateTime AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ;
      private DateTime AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratoocorrencia_data_Includesortasc ;
      private bool Ddo_contratoocorrencia_data_Includesortdsc ;
      private bool Ddo_contratoocorrencia_data_Includefilter ;
      private bool Ddo_contratoocorrencia_data_Filterisrange ;
      private bool Ddo_contratoocorrencia_data_Includedatalist ;
      private bool Ddo_contratoocorrencia_descricao_Includesortasc ;
      private bool Ddo_contratoocorrencia_descricao_Includesortdsc ;
      private bool Ddo_contratoocorrencia_descricao_Includefilter ;
      private bool Ddo_contratoocorrencia_descricao_Filterisrange ;
      private bool Ddo_contratoocorrencia_descricao_Includedatalist ;
      private bool Ddo_contratoocorrencia_naocnfcod_Includesortasc ;
      private bool Ddo_contratoocorrencia_naocnfcod_Includesortdsc ;
      private bool Ddo_contratoocorrencia_naocnfcod_Includefilter ;
      private bool Ddo_contratoocorrencia_naocnfcod_Filterisrange ;
      private bool Ddo_contratoocorrencia_naocnfcod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2027ContratoOcorrencia_NaoCnfCod ;
      private bool AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ;
      private bool n427NaoConformidade_Nome ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV39ContratoOcorrencia_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV41ContratoOcorrencia_Descricao2 ;
      private String AV55TFContratoOcorrencia_Descricao ;
      private String AV56TFContratoOcorrencia_Descricao_Sel ;
      private String AV47ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV53ddo_ContratoOcorrencia_DataTitleControlIdToReplace ;
      private String AV57ddo_ContratoOcorrencia_DescricaoTitleControlIdToReplace ;
      private String AV65ddo_ContratoOcorrencia_NaoCnfCodTitleControlIdToReplace ;
      private String AV67TFContratoOcorrencia_NaoCnfCod_SelDsc ;
      private String AV89Update_GXI ;
      private String AV90Delete_GXI ;
      private String A296ContratoOcorrencia_Descricao ;
      private String lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ;
      private String lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ;
      private String lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ;
      private String AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ;
      private String AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ;
      private String AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ;
      private String AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ;
      private String AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ;
      private String AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox dynContratoOcorrencia_NaoCnfCod ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00772_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] H00772_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] H00772_A427NaoConformidade_Nome ;
      private bool[] H00772_n427NaoConformidade_Nome ;
      private String[] H00773_A427NaoConformidade_Nome ;
      private bool[] H00773_n427NaoConformidade_Nome ;
      private int[] H00773_A74Contrato_Codigo ;
      private int[] H00773_A294ContratoOcorrencia_Codigo ;
      private int[] H00773_A2027ContratoOcorrencia_NaoCnfCod ;
      private bool[] H00773_n2027ContratoOcorrencia_NaoCnfCod ;
      private String[] H00773_A296ContratoOcorrencia_Descricao ;
      private DateTime[] H00773_A295ContratoOcorrencia_Data ;
      private String[] H00773_A77Contrato_Numero ;
      private long[] H00774_AGRID_nRecordCount ;
      private String[] H00775_A427NaoConformidade_Nome ;
      private bool[] H00775_n427NaoConformidade_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ContratoOcorrencia_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54ContratoOcorrencia_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ContratoOcorrencia_NaoCnfCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV58DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoocorrencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00773( IGxContext context ,
                                             String AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                             DateTime AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                             short AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                             bool AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                             DateTime AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                             short AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                             String AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                             String AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                             DateTime AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                             DateTime AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                             String AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                             String AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                             int AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                             String AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[NaoConformidade_Nome], T1.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrencia_NaoCnfCod] AS ContratoOcorrencia_NaoCnfCod, T1.[ContratoOcorrencia_Descricao], T1.[ContratoOcorrencia_Data], T2.[Contrato_Numero]";
         sFromString = " FROM (([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( (0==AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[NaoConformidade_Nome] like @lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[NaoConformidade_Nome] like @lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_NaoCnfCod] = @AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_NaoCnfCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_NaoCnfCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00774( IGxContext context ,
                                             String AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1 ,
                                             DateTime AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1 ,
                                             short AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1 ,
                                             bool AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2 ,
                                             DateTime AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2 ,
                                             short AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2 ,
                                             String AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel ,
                                             String AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero ,
                                             DateTime AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data ,
                                             DateTime AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to ,
                                             String AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel ,
                                             String AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao ,
                                             int AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel ,
                                             String AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod ,
                                             DateTime A295ContratoOcorrencia_Data ,
                                             String A296ContratoOcorrencia_Descricao ,
                                             String A77Contrato_Numero ,
                                             String A427NaoConformidade_Nome ,
                                             int A2027ContratoOcorrencia_NaoCnfCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContratoOcorrencia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN [NaoConformidade] T3 WITH (NOLOCK) ON T3.[NaoConformidade_Codigo] = T1.[ContratoOcorrencia_NaoCnfCod])";
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV70WWContratoOcorrenciaDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV71WWContratoOcorrenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV75WWContratoOcorrenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV76WWContratoOcorrenciaDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIA_DESCRICAO") == 0 ) && ( AV77WWContratoOcorrenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like '%' + @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like '%' + @lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] >= @AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] >= @AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Data] <= @AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Data] <= @AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] like @lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] like @lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Descricao] = @AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Descricao] = @AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( (0==AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[NaoConformidade_Nome] like @lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[NaoConformidade_Nome] like @lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_NaoCnfCod] = @AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_NaoCnfCod] = @AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00773(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
               case 2 :
                     return conditional_H00774(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00772 ;
          prmH00772 = new Object[] {
          } ;
          Object[] prmH00775 ;
          prmH00775 = new Object[] {
          new Object[] {"@ContratoOcorrencia_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00773 ;
          prmH00773 = new Object[] {
          new Object[] {"@AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00774 ;
          prmH00774 = new Object[] {
          new Object[] {"@AV72WWContratoOcorrenciaDS_3_Contratoocorrencia_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWContratoOcorrenciaDS_4_Contratoocorrencia_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV74WWContratoOcorrenciaDS_5_Contratoocorrencia_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV78WWContratoOcorrenciaDS_9_Contratoocorrencia_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV79WWContratoOcorrenciaDS_10_Contratoocorrencia_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWContratoOcorrenciaDS_11_Contratoocorrencia_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWContratoOcorrenciaDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV82WWContratoOcorrenciaDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV83WWContratoOcorrenciaDS_14_Tfcontratoocorrencia_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWContratoOcorrenciaDS_15_Tfcontratoocorrencia_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV85WWContratoOcorrenciaDS_16_Tfcontratoocorrencia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV86WWContratoOcorrenciaDS_17_Tfcontratoocorrencia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaDS_18_Tfcontratoocorrencia_naocnfcod",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWContratoOcorrenciaDS_19_Tfcontratoocorrencia_naocnfcod_sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00772", "SELECT [NaoConformidade_Codigo] AS ContratoOcorrencia_NaoCnfCod, [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00772,0,0,true,false )
             ,new CursorDef("H00773", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00773,11,0,true,false )
             ,new CursorDef("H00774", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00774,1,0,true,false )
             ,new CursorDef("H00775", "SELECT [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContratoOcorrencia_NaoCnfCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00775,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
