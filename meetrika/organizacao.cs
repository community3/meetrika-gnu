/*
               File: Organizacao
        Description: Organização
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:39.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class organizacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Organizacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Organizacao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORGANIZACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Organizacao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkOrganizacao_Ativo.Name = "ORGANIZACAO_ATIVO";
         chkOrganizacao_Ativo.WebTags = "";
         chkOrganizacao_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkOrganizacao_Ativo_Internalname, "TitleCaption", chkOrganizacao_Ativo.Caption);
         chkOrganizacao_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Organização", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtOrganizacao_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public organizacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public organizacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Organizacao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Organizacao_Codigo = aP1_Organizacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkOrganizacao_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3D144( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3D144e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtOrganizacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1213Organizacao_Codigo), 6, 0, ",", "")), ((edtOrganizacao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtOrganizacao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtOrganizacao_Codigo_Visible, edtOrganizacao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Organizacao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3D144( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3D144( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3D144e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_3D144( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_3D144e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3D144e( true) ;
         }
         else
         {
            wb_table1_2_3D144e( false) ;
         }
      }

      protected void wb_table3_26_3D144( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_3D144e( true) ;
         }
         else
         {
            wb_table3_26_3D144e( false) ;
         }
      }

      protected void wb_table2_5_3D144( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3D144( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3D144e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3D144e( true) ;
         }
         else
         {
            wb_table2_5_3D144e( false) ;
         }
      }

      protected void wb_table4_13_3D144( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockorganizacao_nome_Internalname, "Nome", "", "", lblTextblockorganizacao_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtOrganizacao_Nome_Internalname, StringUtil.RTrim( A1214Organizacao_Nome), StringUtil.RTrim( context.localUtil.Format( A1214Organizacao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtOrganizacao_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtOrganizacao_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockorganizacao_ativo_Internalname, "Ativo?", "", "", lblTextblockorganizacao_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockorganizacao_ativo_Visible, 1, 0, "HLP_Organizacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkOrganizacao_Ativo_Internalname, StringUtil.BoolToStr( A1215Organizacao_Ativo), "", "", chkOrganizacao_Ativo.Visible, chkOrganizacao_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3D144e( true) ;
         }
         else
         {
            wb_table4_13_3D144e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113D2 */
         E113D2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1214Organizacao_Nome = StringUtil.Upper( cgiGet( edtOrganizacao_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
               A1215Organizacao_Ativo = StringUtil.StrToBool( cgiGet( chkOrganizacao_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
               A1213Organizacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtOrganizacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
               /* Read saved values. */
               Z1213Organizacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1213Organizacao_Codigo"), ",", "."));
               Z1214Organizacao_Nome = cgiGet( "Z1214Organizacao_Nome");
               Z1215Organizacao_Ativo = StringUtil.StrToBool( cgiGet( "Z1215Organizacao_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Organizacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vORGANIZACAO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Organizacao";
               A1213Organizacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtOrganizacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1213Organizacao_Codigo != Z1213Organizacao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("organizacao:[SecurityCheckFailed value for]"+"Organizacao_Codigo:"+context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("organizacao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1213Organizacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode144 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode144;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound144 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3D0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ORGANIZACAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtOrganizacao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113D2 */
                           E113D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123D2 */
                           E123D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123D2 */
            E123D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3D144( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3D144( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3D0( )
      {
         BeforeValidate3D144( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3D144( ) ;
            }
            else
            {
               CheckExtendedTable3D144( ) ;
               CloseExtendedTableCursors3D144( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3D0( )
      {
      }

      protected void E113D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtOrganizacao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Codigo_Visible), 5, 0)));
      }

      protected void E123D2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wworganizacao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3D144( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1214Organizacao_Nome = T003D3_A1214Organizacao_Nome[0];
               Z1215Organizacao_Ativo = T003D3_A1215Organizacao_Ativo[0];
            }
            else
            {
               Z1214Organizacao_Nome = A1214Organizacao_Nome;
               Z1215Organizacao_Ativo = A1215Organizacao_Ativo;
            }
         }
         if ( GX_JID == -6 )
         {
            Z1213Organizacao_Codigo = A1213Organizacao_Codigo;
            Z1214Organizacao_Nome = A1214Organizacao_Nome;
            Z1215Organizacao_Ativo = A1215Organizacao_Ativo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtOrganizacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtOrganizacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Organizacao_Codigo) )
         {
            A1213Organizacao_Codigo = AV7Organizacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         lblTextblockorganizacao_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockorganizacao_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockorganizacao_ativo_Visible), 5, 0)));
         chkOrganizacao_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkOrganizacao_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkOrganizacao_Ativo.Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1215Organizacao_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1215Organizacao_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
         }
      }

      protected void Load3D144( )
      {
         /* Using cursor T003D4 */
         pr_default.execute(2, new Object[] {A1213Organizacao_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound144 = 1;
            A1214Organizacao_Nome = T003D4_A1214Organizacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            A1215Organizacao_Ativo = T003D4_A1215Organizacao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
            ZM3D144( -6) ;
         }
         pr_default.close(2);
         OnLoadActions3D144( ) ;
      }

      protected void OnLoadActions3D144( )
      {
      }

      protected void CheckExtendedTable3D144( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors3D144( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3D144( )
      {
         /* Using cursor T003D5 */
         pr_default.execute(3, new Object[] {A1213Organizacao_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound144 = 1;
         }
         else
         {
            RcdFound144 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003D3 */
         pr_default.execute(1, new Object[] {A1213Organizacao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3D144( 6) ;
            RcdFound144 = 1;
            A1213Organizacao_Codigo = T003D3_A1213Organizacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
            A1214Organizacao_Nome = T003D3_A1214Organizacao_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
            A1215Organizacao_Ativo = T003D3_A1215Organizacao_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
            Z1213Organizacao_Codigo = A1213Organizacao_Codigo;
            sMode144 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3D144( ) ;
            if ( AnyError == 1 )
            {
               RcdFound144 = 0;
               InitializeNonKey3D144( ) ;
            }
            Gx_mode = sMode144;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound144 = 0;
            InitializeNonKey3D144( ) ;
            sMode144 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode144;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3D144( ) ;
         if ( RcdFound144 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound144 = 0;
         /* Using cursor T003D6 */
         pr_default.execute(4, new Object[] {A1213Organizacao_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T003D6_A1213Organizacao_Codigo[0] < A1213Organizacao_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T003D6_A1213Organizacao_Codigo[0] > A1213Organizacao_Codigo ) ) )
            {
               A1213Organizacao_Codigo = T003D6_A1213Organizacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
               RcdFound144 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound144 = 0;
         /* Using cursor T003D7 */
         pr_default.execute(5, new Object[] {A1213Organizacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T003D7_A1213Organizacao_Codigo[0] > A1213Organizacao_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T003D7_A1213Organizacao_Codigo[0] < A1213Organizacao_Codigo ) ) )
            {
               A1213Organizacao_Codigo = T003D7_A1213Organizacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
               RcdFound144 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3D144( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtOrganizacao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3D144( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound144 == 1 )
            {
               if ( A1213Organizacao_Codigo != Z1213Organizacao_Codigo )
               {
                  A1213Organizacao_Codigo = Z1213Organizacao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ORGANIZACAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtOrganizacao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtOrganizacao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3D144( ) ;
                  GX_FocusControl = edtOrganizacao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1213Organizacao_Codigo != Z1213Organizacao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtOrganizacao_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3D144( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ORGANIZACAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtOrganizacao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtOrganizacao_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3D144( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1213Organizacao_Codigo != Z1213Organizacao_Codigo )
         {
            A1213Organizacao_Codigo = Z1213Organizacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ORGANIZACAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtOrganizacao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtOrganizacao_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3D144( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003D2 */
            pr_default.execute(0, new Object[] {A1213Organizacao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Organizacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1214Organizacao_Nome, T003D2_A1214Organizacao_Nome[0]) != 0 ) || ( Z1215Organizacao_Ativo != T003D2_A1215Organizacao_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z1214Organizacao_Nome, T003D2_A1214Organizacao_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("organizacao:[seudo value changed for attri]"+"Organizacao_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1214Organizacao_Nome);
                  GXUtil.WriteLogRaw("Current: ",T003D2_A1214Organizacao_Nome[0]);
               }
               if ( Z1215Organizacao_Ativo != T003D2_A1215Organizacao_Ativo[0] )
               {
                  GXUtil.WriteLog("organizacao:[seudo value changed for attri]"+"Organizacao_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1215Organizacao_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T003D2_A1215Organizacao_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Organizacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3D144( )
      {
         BeforeValidate3D144( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3D144( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3D144( 0) ;
            CheckOptimisticConcurrency3D144( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3D144( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3D144( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003D8 */
                     pr_default.execute(6, new Object[] {A1214Organizacao_Nome, A1215Organizacao_Ativo});
                     A1213Organizacao_Codigo = T003D8_A1213Organizacao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Organizacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3D0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3D144( ) ;
            }
            EndLevel3D144( ) ;
         }
         CloseExtendedTableCursors3D144( ) ;
      }

      protected void Update3D144( )
      {
         BeforeValidate3D144( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3D144( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3D144( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3D144( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3D144( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003D9 */
                     pr_default.execute(7, new Object[] {A1214Organizacao_Nome, A1215Organizacao_Ativo, A1213Organizacao_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Organizacao") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Organizacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3D144( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3D144( ) ;
         }
         CloseExtendedTableCursors3D144( ) ;
      }

      protected void DeferredUpdate3D144( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3D144( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3D144( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3D144( ) ;
            AfterConfirm3D144( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3D144( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003D10 */
                  pr_default.execute(8, new Object[] {A1213Organizacao_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Organizacao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode144 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3D144( ) ;
         Gx_mode = sMode144;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3D144( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T003D11 */
            pr_default.execute(9, new Object[] {A1213Organizacao_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel3D144( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3D144( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Organizacao");
            if ( AnyError == 0 )
            {
               ConfirmValues3D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Organizacao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3D144( )
      {
         /* Scan By routine */
         /* Using cursor T003D12 */
         pr_default.execute(10);
         RcdFound144 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound144 = 1;
            A1213Organizacao_Codigo = T003D12_A1213Organizacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3D144( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound144 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound144 = 1;
            A1213Organizacao_Codigo = T003D12_A1213Organizacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3D144( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm3D144( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3D144( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3D144( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3D144( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3D144( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3D144( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3D144( )
      {
         edtOrganizacao_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Nome_Enabled), 5, 0)));
         chkOrganizacao_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkOrganizacao_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkOrganizacao_Ativo.Enabled), 5, 0)));
         edtOrganizacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtOrganizacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtOrganizacao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3D0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117274013");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("organizacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Organizacao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1213Organizacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1214Organizacao_Nome", StringUtil.RTrim( Z1214Organizacao_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z1215Organizacao_Ativo", Z1215Organizacao_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vORGANIZACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Organizacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vORGANIZACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Organizacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Organizacao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("organizacao:[SendSecurityCheck value for]"+"Organizacao_Codigo:"+context.localUtil.Format( (decimal)(A1213Organizacao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("organizacao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("organizacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Organizacao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Organizacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Organização" ;
      }

      protected void InitializeNonKey3D144( )
      {
         A1214Organizacao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1214Organizacao_Nome", A1214Organizacao_Nome);
         A1215Organizacao_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
         Z1214Organizacao_Nome = "";
         Z1215Organizacao_Ativo = false;
      }

      protected void InitAll3D144( )
      {
         A1213Organizacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1213Organizacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1213Organizacao_Codigo), 6, 0)));
         InitializeNonKey3D144( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1215Organizacao_Ativo = i1215Organizacao_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1215Organizacao_Ativo", A1215Organizacao_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117274026");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("organizacao.js", "?20203117274026");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockorganizacao_nome_Internalname = "TEXTBLOCKORGANIZACAO_NOME";
         edtOrganizacao_Nome_Internalname = "ORGANIZACAO_NOME";
         lblTextblockorganizacao_ativo_Internalname = "TEXTBLOCKORGANIZACAO_ATIVO";
         chkOrganizacao_Ativo_Internalname = "ORGANIZACAO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtOrganizacao_Codigo_Internalname = "ORGANIZACAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Organização";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Organização";
         chkOrganizacao_Ativo.Enabled = 1;
         chkOrganizacao_Ativo.Visible = 1;
         lblTextblockorganizacao_ativo_Visible = 1;
         edtOrganizacao_Nome_Jsonclick = "";
         edtOrganizacao_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtOrganizacao_Codigo_Jsonclick = "";
         edtOrganizacao_Codigo_Enabled = 0;
         edtOrganizacao_Codigo_Visible = 1;
         chkOrganizacao_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Organizacao_Codigo',fld:'vORGANIZACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123D2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1214Organizacao_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockorganizacao_nome_Jsonclick = "";
         A1214Organizacao_Nome = "";
         lblTextblockorganizacao_ativo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode144 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T003D4_A1213Organizacao_Codigo = new int[1] ;
         T003D4_A1214Organizacao_Nome = new String[] {""} ;
         T003D4_A1215Organizacao_Ativo = new bool[] {false} ;
         T003D5_A1213Organizacao_Codigo = new int[1] ;
         T003D3_A1213Organizacao_Codigo = new int[1] ;
         T003D3_A1214Organizacao_Nome = new String[] {""} ;
         T003D3_A1215Organizacao_Ativo = new bool[] {false} ;
         T003D6_A1213Organizacao_Codigo = new int[1] ;
         T003D7_A1213Organizacao_Codigo = new int[1] ;
         T003D2_A1213Organizacao_Codigo = new int[1] ;
         T003D2_A1214Organizacao_Nome = new String[] {""} ;
         T003D2_A1215Organizacao_Ativo = new bool[] {false} ;
         T003D8_A1213Organizacao_Codigo = new int[1] ;
         T003D11_A5AreaTrabalho_Codigo = new int[1] ;
         T003D12_A1213Organizacao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.organizacao__default(),
            new Object[][] {
                new Object[] {
               T003D2_A1213Organizacao_Codigo, T003D2_A1214Organizacao_Nome, T003D2_A1215Organizacao_Ativo
               }
               , new Object[] {
               T003D3_A1213Organizacao_Codigo, T003D3_A1214Organizacao_Nome, T003D3_A1215Organizacao_Ativo
               }
               , new Object[] {
               T003D4_A1213Organizacao_Codigo, T003D4_A1214Organizacao_Nome, T003D4_A1215Organizacao_Ativo
               }
               , new Object[] {
               T003D5_A1213Organizacao_Codigo
               }
               , new Object[] {
               T003D6_A1213Organizacao_Codigo
               }
               , new Object[] {
               T003D7_A1213Organizacao_Codigo
               }
               , new Object[] {
               T003D8_A1213Organizacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003D11_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T003D12_A1213Organizacao_Codigo
               }
            }
         );
         Z1215Organizacao_Ativo = true;
         A1215Organizacao_Ativo = true;
         i1215Organizacao_Ativo = true;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound144 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Organizacao_Codigo ;
      private int Z1213Organizacao_Codigo ;
      private int AV7Organizacao_Codigo ;
      private int trnEnded ;
      private int A1213Organizacao_Codigo ;
      private int edtOrganizacao_Codigo_Enabled ;
      private int edtOrganizacao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtOrganizacao_Nome_Enabled ;
      private int lblTextblockorganizacao_ativo_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1214Organizacao_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkOrganizacao_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtOrganizacao_Nome_Internalname ;
      private String edtOrganizacao_Codigo_Internalname ;
      private String edtOrganizacao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockorganizacao_nome_Internalname ;
      private String lblTextblockorganizacao_nome_Jsonclick ;
      private String A1214Organizacao_Nome ;
      private String edtOrganizacao_Nome_Jsonclick ;
      private String lblTextblockorganizacao_ativo_Internalname ;
      private String lblTextblockorganizacao_ativo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode144 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z1215Organizacao_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1215Organizacao_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i1215Organizacao_Ativo ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkOrganizacao_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T003D4_A1213Organizacao_Codigo ;
      private String[] T003D4_A1214Organizacao_Nome ;
      private bool[] T003D4_A1215Organizacao_Ativo ;
      private int[] T003D5_A1213Organizacao_Codigo ;
      private int[] T003D3_A1213Organizacao_Codigo ;
      private String[] T003D3_A1214Organizacao_Nome ;
      private bool[] T003D3_A1215Organizacao_Ativo ;
      private int[] T003D6_A1213Organizacao_Codigo ;
      private int[] T003D7_A1213Organizacao_Codigo ;
      private int[] T003D2_A1213Organizacao_Codigo ;
      private String[] T003D2_A1214Organizacao_Nome ;
      private bool[] T003D2_A1215Organizacao_Ativo ;
      private int[] T003D8_A1213Organizacao_Codigo ;
      private int[] T003D11_A5AreaTrabalho_Codigo ;
      private int[] T003D12_A1213Organizacao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class organizacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003D4 ;
          prmT003D4 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D5 ;
          prmT003D5 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D3 ;
          prmT003D3 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D6 ;
          prmT003D6 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D7 ;
          prmT003D7 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D2 ;
          prmT003D2 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D8 ;
          prmT003D8 = new Object[] {
          new Object[] {"@Organizacao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Organizacao_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT003D9 ;
          prmT003D9 = new Object[] {
          new Object[] {"@Organizacao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Organizacao_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D10 ;
          prmT003D10 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D11 ;
          prmT003D11 = new Object[] {
          new Object[] {"@Organizacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003D12 ;
          prmT003D12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T003D2", "SELECT [Organizacao_Codigo], [Organizacao_Nome], [Organizacao_Ativo] FROM [Organizacao] WITH (UPDLOCK) WHERE [Organizacao_Codigo] = @Organizacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003D2,1,0,true,false )
             ,new CursorDef("T003D3", "SELECT [Organizacao_Codigo], [Organizacao_Nome], [Organizacao_Ativo] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @Organizacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003D3,1,0,true,false )
             ,new CursorDef("T003D4", "SELECT TM1.[Organizacao_Codigo], TM1.[Organizacao_Nome], TM1.[Organizacao_Ativo] FROM [Organizacao] TM1 WITH (NOLOCK) WHERE TM1.[Organizacao_Codigo] = @Organizacao_Codigo ORDER BY TM1.[Organizacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003D4,100,0,true,false )
             ,new CursorDef("T003D5", "SELECT [Organizacao_Codigo] FROM [Organizacao] WITH (NOLOCK) WHERE [Organizacao_Codigo] = @Organizacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003D5,1,0,true,false )
             ,new CursorDef("T003D6", "SELECT TOP 1 [Organizacao_Codigo] FROM [Organizacao] WITH (NOLOCK) WHERE ( [Organizacao_Codigo] > @Organizacao_Codigo) ORDER BY [Organizacao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003D6,1,0,true,true )
             ,new CursorDef("T003D7", "SELECT TOP 1 [Organizacao_Codigo] FROM [Organizacao] WITH (NOLOCK) WHERE ( [Organizacao_Codigo] < @Organizacao_Codigo) ORDER BY [Organizacao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003D7,1,0,true,true )
             ,new CursorDef("T003D8", "INSERT INTO [Organizacao]([Organizacao_Nome], [Organizacao_Ativo]) VALUES(@Organizacao_Nome, @Organizacao_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003D8)
             ,new CursorDef("T003D9", "UPDATE [Organizacao] SET [Organizacao_Nome]=@Organizacao_Nome, [Organizacao_Ativo]=@Organizacao_Ativo  WHERE [Organizacao_Codigo] = @Organizacao_Codigo", GxErrorMask.GX_NOMASK,prmT003D9)
             ,new CursorDef("T003D10", "DELETE FROM [Organizacao]  WHERE [Organizacao_Codigo] = @Organizacao_Codigo", GxErrorMask.GX_NOMASK,prmT003D10)
             ,new CursorDef("T003D11", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_OrganizacaoCod] = @Organizacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003D11,1,0,true,true )
             ,new CursorDef("T003D12", "SELECT [Organizacao_Codigo] FROM [Organizacao] WITH (NOLOCK) ORDER BY [Organizacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003D12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
