/*
               File: PRC_PausarContinuarOS
        Description: Pausar Continuar OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:47.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pausarcontinuaros : GXProcedure
   {
      public prc_pausarcontinuaros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pausarcontinuaros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_StatusDmn ,
                           String aP2_Observacao ,
                           int aP3_UserId )
      {
         this.AV9Codigo = aP0_Codigo;
         this.AV15StatusDmn = aP1_StatusDmn;
         this.AV11Observacao = aP2_Observacao;
         this.AV17UserId = aP3_UserId;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 String aP1_StatusDmn ,
                                 String aP2_Observacao ,
                                 int aP3_UserId )
      {
         prc_pausarcontinuaros objprc_pausarcontinuaros;
         objprc_pausarcontinuaros = new prc_pausarcontinuaros();
         objprc_pausarcontinuaros.AV9Codigo = aP0_Codigo;
         objprc_pausarcontinuaros.AV15StatusDmn = aP1_StatusDmn;
         objprc_pausarcontinuaros.AV11Observacao = aP2_Observacao;
         objprc_pausarcontinuaros.AV17UserId = aP3_UserId;
         objprc_pausarcontinuaros.context.SetSubmitInitialConfig(context);
         objprc_pausarcontinuaros.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pausarcontinuaros);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pausarcontinuaros)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV15StatusDmn, "B") == 0 )
         {
            /* Using cursor P00BY2 */
            pr_default.execute(0, new Object[] {AV9Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00BY2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00BY2_n1553ContagemResultado_CntSrvCod[0];
               A894LogResponsavel_Acao = P00BY2_A894LogResponsavel_Acao[0];
               A892LogResponsavel_DemandaCod = P00BY2_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P00BY2_n892LogResponsavel_DemandaCod[0];
               A1650ContagemResultado_PrzInc = P00BY2_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P00BY2_n1650ContagemResultado_PrzInc[0];
               A1237ContagemResultado_PrazoMaisDias = P00BY2_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00BY2_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00BY2_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00BY2_n1227ContagemResultado_PrazoInicialDias[0];
               A1611ContagemResultado_PrzTpDias = P00BY2_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00BY2_n1611ContagemResultado_PrzTpDias[0];
               A912ContagemResultado_HoraEntrega = P00BY2_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00BY2_n912ContagemResultado_HoraEntrega[0];
               A602ContagemResultado_OSVinculada = P00BY2_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00BY2_n602ContagemResultado_OSVinculada[0];
               A1130LogResponsavel_Status = P00BY2_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = P00BY2_n1130LogResponsavel_Status[0];
               A484ContagemResultado_StatusDmn = P00BY2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00BY2_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P00BY2_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00BY2_n472ContagemResultado_DataEntrega[0];
               A890ContagemResultado_Responsavel = P00BY2_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00BY2_n890ContagemResultado_Responsavel[0];
               A1797LogResponsavel_Codigo = P00BY2_A1797LogResponsavel_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P00BY2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00BY2_n1553ContagemResultado_CntSrvCod[0];
               A1237ContagemResultado_PrazoMaisDias = P00BY2_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00BY2_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00BY2_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00BY2_n1227ContagemResultado_PrazoInicialDias[0];
               A912ContagemResultado_HoraEntrega = P00BY2_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00BY2_n912ContagemResultado_HoraEntrega[0];
               A602ContagemResultado_OSVinculada = P00BY2_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00BY2_n602ContagemResultado_OSVinculada[0];
               A484ContagemResultado_StatusDmn = P00BY2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00BY2_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P00BY2_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00BY2_n472ContagemResultado_DataEntrega[0];
               A890ContagemResultado_Responsavel = P00BY2_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00BY2_n890ContagemResultado_Responsavel[0];
               A1650ContagemResultado_PrzInc = P00BY2_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P00BY2_n1650ContagemResultado_PrzInc[0];
               A1611ContagemResultado_PrzTpDias = P00BY2_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00BY2_n1611ContagemResultado_PrzTpDias[0];
               AV13PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               if ( A1650ContagemResultado_PrzInc == 2 )
               {
                  new prc_datadeinicioutil(context ).execute( ref  AV13PrazoEntrega) ;
               }
               AV10Dias = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               GXt_dtime1 = AV13PrazoEntrega;
               new prc_adddiasuteis(context ).execute(  AV13PrazoEntrega,  AV10Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime1) ;
               AV13PrazoEntrega = GXt_dtime1;
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               AV12OSVinculada = A602ContagemResultado_OSVinculada;
               AV14StatusAtual = A1130LogResponsavel_Status;
               A484ContagemResultado_StatusDmn = A1130LogResponsavel_Status;
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV13PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               new prc_inslogresponsavel(context ).execute( ref  A892LogResponsavel_DemandaCod,  A890ContagemResultado_Responsavel,  "RE",  "D",  AV17UserId,  0,  "B",  AV14StatusAtual,  AV11Observacao,  AV13PrazoEntrega,  true) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00BY3 */
               pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P00BY4 */
               pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Execute user subroutine: 'CONTINUARCONTRAPARTE' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else
         {
            /* Using cursor P00BY6 */
            pr_default.execute(3, new Object[] {AV9Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A456ContagemResultado_Codigo = P00BY6_A456ContagemResultado_Codigo[0];
               A602ContagemResultado_OSVinculada = P00BY6_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00BY6_n602ContagemResultado_OSVinculada[0];
               A484ContagemResultado_StatusDmn = P00BY6_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00BY6_n484ContagemResultado_StatusDmn[0];
               A531ContagemResultado_StatusUltCnt = P00BY6_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00BY6_n531ContagemResultado_StatusUltCnt[0];
               A531ContagemResultado_StatusUltCnt = P00BY6_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00BY6_n531ContagemResultado_StatusUltCnt[0];
               AV12OSVinculada = A602ContagemResultado_OSVinculada;
               AV16StatusUltCnt = A531ContagemResultado_StatusUltCnt;
               AV14StatusAtual = A484ContagemResultado_StatusDmn;
               AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
               A484ContagemResultado_StatusDmn = "B";
               n484ContagemResultado_StatusDmn = false;
               new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "B",  "D",  AV17UserId,  0,  AV14StatusAtual,  "B",  AV11Observacao,  AV13PrazoEntrega,  true) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00BY7 */
               pr_default.execute(4, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P00BY8 */
               pr_default.execute(5, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( AV16StatusUltCnt == 7 )
            {
               /* Execute user subroutine: 'PAUSARCONTRAPARTE' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PAUSARCONTRAPARTE' Routine */
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV12OSVinculada ,
                                              A602ContagemResultado_OSVinculada ,
                                              AV9Codigo ,
                                              A456ContagemResultado_Codigo ,
                                              A483ContagemResultado_StatusCnt },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         /* Using cursor P00BY9 */
         pr_default.execute(6, new Object[] {AV9Codigo, AV12OSVinculada});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A483ContagemResultado_StatusCnt = P00BY9_A483ContagemResultado_StatusCnt[0];
            A456ContagemResultado_Codigo = P00BY9_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00BY9_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00BY9_n602ContagemResultado_OSVinculada[0];
            A484ContagemResultado_StatusDmn = P00BY9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00BY9_n484ContagemResultado_StatusDmn[0];
            A473ContagemResultado_DataCnt = P00BY9_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00BY9_A511ContagemResultado_HoraCnt[0];
            A602ContagemResultado_OSVinculada = P00BY9_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00BY9_n602ContagemResultado_OSVinculada[0];
            A484ContagemResultado_StatusDmn = P00BY9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00BY9_n484ContagemResultado_StatusDmn[0];
            AV14StatusAtual = A484ContagemResultado_StatusDmn;
            AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
            A484ContagemResultado_StatusDmn = "B";
            n484ContagemResultado_StatusDmn = false;
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "B",  "D",  AV17UserId,  0,  AV14StatusAtual,  "B",  AV11Observacao,  AV13PrazoEntrega,  true) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00BY10 */
            pr_default.execute(7, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00BY11 */
            pr_default.execute(8, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void S121( )
      {
         /* 'CONTINUARCONTRAPARTE' Routine */
         if ( (0==AV12OSVinculada) )
         {
            /* Using cursor P00BY13 */
            pr_default.execute(9, new Object[] {AV9Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A602ContagemResultado_OSVinculada = P00BY13_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00BY13_n602ContagemResultado_OSVinculada[0];
               A456ContagemResultado_Codigo = P00BY13_A456ContagemResultado_Codigo[0];
               A531ContagemResultado_StatusUltCnt = P00BY13_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00BY13_n531ContagemResultado_StatusUltCnt[0];
               A531ContagemResultado_StatusUltCnt = P00BY13_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00BY13_n531ContagemResultado_StatusUltCnt[0];
               AV12OSVinculada = A456ContagemResultado_Codigo;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(9);
            }
            pr_default.close(9);
         }
         if ( AV12OSVinculada > 0 )
         {
            /* Using cursor P00BY14 */
            pr_default.execute(10, new Object[] {AV12OSVinculada});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00BY14_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00BY14_n1553ContagemResultado_CntSrvCod[0];
               A894LogResponsavel_Acao = P00BY14_A894LogResponsavel_Acao[0];
               A892LogResponsavel_DemandaCod = P00BY14_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P00BY14_n892LogResponsavel_DemandaCod[0];
               A1130LogResponsavel_Status = P00BY14_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = P00BY14_n1130LogResponsavel_Status[0];
               A1650ContagemResultado_PrzInc = P00BY14_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P00BY14_n1650ContagemResultado_PrzInc[0];
               A1237ContagemResultado_PrazoMaisDias = P00BY14_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00BY14_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00BY14_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00BY14_n1227ContagemResultado_PrazoInicialDias[0];
               A1611ContagemResultado_PrzTpDias = P00BY14_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00BY14_n1611ContagemResultado_PrzTpDias[0];
               A912ContagemResultado_HoraEntrega = P00BY14_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00BY14_n912ContagemResultado_HoraEntrega[0];
               A484ContagemResultado_StatusDmn = P00BY14_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00BY14_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P00BY14_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00BY14_n472ContagemResultado_DataEntrega[0];
               A890ContagemResultado_Responsavel = P00BY14_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00BY14_n890ContagemResultado_Responsavel[0];
               A1797LogResponsavel_Codigo = P00BY14_A1797LogResponsavel_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P00BY14_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00BY14_n1553ContagemResultado_CntSrvCod[0];
               A1237ContagemResultado_PrazoMaisDias = P00BY14_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00BY14_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00BY14_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00BY14_n1227ContagemResultado_PrazoInicialDias[0];
               A912ContagemResultado_HoraEntrega = P00BY14_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00BY14_n912ContagemResultado_HoraEntrega[0];
               A484ContagemResultado_StatusDmn = P00BY14_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00BY14_n484ContagemResultado_StatusDmn[0];
               A472ContagemResultado_DataEntrega = P00BY14_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00BY14_n472ContagemResultado_DataEntrega[0];
               A890ContagemResultado_Responsavel = P00BY14_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00BY14_n890ContagemResultado_Responsavel[0];
               A1650ContagemResultado_PrzInc = P00BY14_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P00BY14_n1650ContagemResultado_PrzInc[0];
               A1611ContagemResultado_PrzTpDias = P00BY14_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00BY14_n1611ContagemResultado_PrzTpDias[0];
               AV14StatusAtual = A1130LogResponsavel_Status;
               AV13PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               if ( A1650ContagemResultado_PrzInc == 2 )
               {
                  new prc_datadeinicioutil(context ).execute( ref  AV13PrazoEntrega) ;
               }
               AV10Dias = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               GXt_dtime1 = AV13PrazoEntrega;
               new prc_adddiasuteis(context ).execute(  AV13PrazoEntrega,  AV10Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime1) ;
               AV13PrazoEntrega = GXt_dtime1;
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
               AV13PrazoEntrega = DateTimeUtil.TAdd( AV13PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
               A484ContagemResultado_StatusDmn = AV14StatusAtual;
               n484ContagemResultado_StatusDmn = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV13PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               new prc_inslogresponsavel(context ).execute( ref  A892LogResponsavel_DemandaCod,  A890ContagemResultado_Responsavel,  "RE",  "D",  AV17UserId,  0,  "B",  AV14StatusAtual,  AV11Observacao,  AV13PrazoEntrega,  true) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00BY15 */
               pr_default.execute(11, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P00BY16 */
               pr_default.execute(12, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
               pr_default.close(12);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(10);
            }
            pr_default.close(10);
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_PausarContinuarOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BY2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00BY2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00BY2_A894LogResponsavel_Acao = new String[] {""} ;
         P00BY2_A892LogResponsavel_DemandaCod = new int[1] ;
         P00BY2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00BY2_A1650ContagemResultado_PrzInc = new short[1] ;
         P00BY2_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P00BY2_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00BY2_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00BY2_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00BY2_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00BY2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00BY2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00BY2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00BY2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00BY2_A602ContagemResultado_OSVinculada = new int[1] ;
         P00BY2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00BY2_A1130LogResponsavel_Status = new String[] {""} ;
         P00BY2_n1130LogResponsavel_Status = new bool[] {false} ;
         P00BY2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00BY2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00BY2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00BY2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00BY2_A890ContagemResultado_Responsavel = new int[1] ;
         P00BY2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00BY2_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         A1611ContagemResultado_PrzTpDias = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1130LogResponsavel_Status = "";
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         AV13PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV14StatusAtual = "";
         P00BY6_A456ContagemResultado_Codigo = new int[1] ;
         P00BY6_A602ContagemResultado_OSVinculada = new int[1] ;
         P00BY6_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00BY6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00BY6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00BY6_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00BY6_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         P00BY9_A483ContagemResultado_StatusCnt = new short[1] ;
         P00BY9_A456ContagemResultado_Codigo = new int[1] ;
         P00BY9_A602ContagemResultado_OSVinculada = new int[1] ;
         P00BY9_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00BY9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00BY9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00BY9_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00BY9_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P00BY13_A602ContagemResultado_OSVinculada = new int[1] ;
         P00BY13_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00BY13_A456ContagemResultado_Codigo = new int[1] ;
         P00BY13_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00BY13_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         P00BY14_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00BY14_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00BY14_A894LogResponsavel_Acao = new String[] {""} ;
         P00BY14_A892LogResponsavel_DemandaCod = new int[1] ;
         P00BY14_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00BY14_A1130LogResponsavel_Status = new String[] {""} ;
         P00BY14_n1130LogResponsavel_Status = new bool[] {false} ;
         P00BY14_A1650ContagemResultado_PrzInc = new short[1] ;
         P00BY14_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P00BY14_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00BY14_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00BY14_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00BY14_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00BY14_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00BY14_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00BY14_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00BY14_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00BY14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00BY14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00BY14_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00BY14_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00BY14_A890ContagemResultado_Responsavel = new int[1] ;
         P00BY14_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00BY14_A1797LogResponsavel_Codigo = new long[1] ;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_pausarcontinuaros__default(),
            new Object[][] {
                new Object[] {
               P00BY2_A1553ContagemResultado_CntSrvCod, P00BY2_n1553ContagemResultado_CntSrvCod, P00BY2_A894LogResponsavel_Acao, P00BY2_A892LogResponsavel_DemandaCod, P00BY2_n892LogResponsavel_DemandaCod, P00BY2_A1650ContagemResultado_PrzInc, P00BY2_n1650ContagemResultado_PrzInc, P00BY2_A1237ContagemResultado_PrazoMaisDias, P00BY2_n1237ContagemResultado_PrazoMaisDias, P00BY2_A1227ContagemResultado_PrazoInicialDias,
               P00BY2_n1227ContagemResultado_PrazoInicialDias, P00BY2_A1611ContagemResultado_PrzTpDias, P00BY2_n1611ContagemResultado_PrzTpDias, P00BY2_A912ContagemResultado_HoraEntrega, P00BY2_n912ContagemResultado_HoraEntrega, P00BY2_A602ContagemResultado_OSVinculada, P00BY2_n602ContagemResultado_OSVinculada, P00BY2_A1130LogResponsavel_Status, P00BY2_n1130LogResponsavel_Status, P00BY2_A484ContagemResultado_StatusDmn,
               P00BY2_n484ContagemResultado_StatusDmn, P00BY2_A472ContagemResultado_DataEntrega, P00BY2_n472ContagemResultado_DataEntrega, P00BY2_A890ContagemResultado_Responsavel, P00BY2_n890ContagemResultado_Responsavel, P00BY2_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00BY6_A456ContagemResultado_Codigo, P00BY6_A602ContagemResultado_OSVinculada, P00BY6_n602ContagemResultado_OSVinculada, P00BY6_A484ContagemResultado_StatusDmn, P00BY6_n484ContagemResultado_StatusDmn, P00BY6_A531ContagemResultado_StatusUltCnt, P00BY6_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00BY9_A483ContagemResultado_StatusCnt, P00BY9_A456ContagemResultado_Codigo, P00BY9_A602ContagemResultado_OSVinculada, P00BY9_n602ContagemResultado_OSVinculada, P00BY9_A484ContagemResultado_StatusDmn, P00BY9_n484ContagemResultado_StatusDmn, P00BY9_A473ContagemResultado_DataCnt, P00BY9_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00BY13_A602ContagemResultado_OSVinculada, P00BY13_n602ContagemResultado_OSVinculada, P00BY13_A456ContagemResultado_Codigo, P00BY13_A531ContagemResultado_StatusUltCnt, P00BY13_n531ContagemResultado_StatusUltCnt
               }
               , new Object[] {
               P00BY14_A1553ContagemResultado_CntSrvCod, P00BY14_n1553ContagemResultado_CntSrvCod, P00BY14_A894LogResponsavel_Acao, P00BY14_A892LogResponsavel_DemandaCod, P00BY14_n892LogResponsavel_DemandaCod, P00BY14_A1130LogResponsavel_Status, P00BY14_n1130LogResponsavel_Status, P00BY14_A1650ContagemResultado_PrzInc, P00BY14_n1650ContagemResultado_PrzInc, P00BY14_A1237ContagemResultado_PrazoMaisDias,
               P00BY14_n1237ContagemResultado_PrazoMaisDias, P00BY14_A1227ContagemResultado_PrazoInicialDias, P00BY14_n1227ContagemResultado_PrazoInicialDias, P00BY14_A1611ContagemResultado_PrzTpDias, P00BY14_n1611ContagemResultado_PrzTpDias, P00BY14_A912ContagemResultado_HoraEntrega, P00BY14_n912ContagemResultado_HoraEntrega, P00BY14_A484ContagemResultado_StatusDmn, P00BY14_n484ContagemResultado_StatusDmn, P00BY14_A472ContagemResultado_DataEntrega,
               P00BY14_n472ContagemResultado_DataEntrega, P00BY14_A890ContagemResultado_Responsavel, P00BY14_n890ContagemResultado_Responsavel, P00BY14_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1650ContagemResultado_PrzInc ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV10Dias ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short AV16StatusUltCnt ;
      private short A483ContagemResultado_StatusCnt ;
      private int AV9Codigo ;
      private int AV17UserId ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A892LogResponsavel_DemandaCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A890ContagemResultado_Responsavel ;
      private int AV12OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private String AV15StatusDmn ;
      private String scmdbuf ;
      private String A894LogResponsavel_Acao ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A1130LogResponsavel_Status ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV14StatusAtual ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV13PrazoEntrega ;
      private DateTime GXt_dtime1 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1650ContagemResultado_PrzInc ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1130LogResponsavel_Status ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n890ContagemResultado_Responsavel ;
      private bool returnInSub ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private String AV11Observacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BY2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00BY2_n1553ContagemResultado_CntSrvCod ;
      private String[] P00BY2_A894LogResponsavel_Acao ;
      private int[] P00BY2_A892LogResponsavel_DemandaCod ;
      private bool[] P00BY2_n892LogResponsavel_DemandaCod ;
      private short[] P00BY2_A1650ContagemResultado_PrzInc ;
      private bool[] P00BY2_n1650ContagemResultado_PrzInc ;
      private short[] P00BY2_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00BY2_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P00BY2_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00BY2_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00BY2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00BY2_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00BY2_A912ContagemResultado_HoraEntrega ;
      private bool[] P00BY2_n912ContagemResultado_HoraEntrega ;
      private int[] P00BY2_A602ContagemResultado_OSVinculada ;
      private bool[] P00BY2_n602ContagemResultado_OSVinculada ;
      private String[] P00BY2_A1130LogResponsavel_Status ;
      private bool[] P00BY2_n1130LogResponsavel_Status ;
      private String[] P00BY2_A484ContagemResultado_StatusDmn ;
      private bool[] P00BY2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00BY2_A472ContagemResultado_DataEntrega ;
      private bool[] P00BY2_n472ContagemResultado_DataEntrega ;
      private int[] P00BY2_A890ContagemResultado_Responsavel ;
      private bool[] P00BY2_n890ContagemResultado_Responsavel ;
      private long[] P00BY2_A1797LogResponsavel_Codigo ;
      private int[] P00BY6_A456ContagemResultado_Codigo ;
      private int[] P00BY6_A602ContagemResultado_OSVinculada ;
      private bool[] P00BY6_n602ContagemResultado_OSVinculada ;
      private String[] P00BY6_A484ContagemResultado_StatusDmn ;
      private bool[] P00BY6_n484ContagemResultado_StatusDmn ;
      private short[] P00BY6_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00BY6_n531ContagemResultado_StatusUltCnt ;
      private short[] P00BY9_A483ContagemResultado_StatusCnt ;
      private int[] P00BY9_A456ContagemResultado_Codigo ;
      private int[] P00BY9_A602ContagemResultado_OSVinculada ;
      private bool[] P00BY9_n602ContagemResultado_OSVinculada ;
      private String[] P00BY9_A484ContagemResultado_StatusDmn ;
      private bool[] P00BY9_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00BY9_A473ContagemResultado_DataCnt ;
      private String[] P00BY9_A511ContagemResultado_HoraCnt ;
      private int[] P00BY13_A602ContagemResultado_OSVinculada ;
      private bool[] P00BY13_n602ContagemResultado_OSVinculada ;
      private int[] P00BY13_A456ContagemResultado_Codigo ;
      private short[] P00BY13_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00BY13_n531ContagemResultado_StatusUltCnt ;
      private int[] P00BY14_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00BY14_n1553ContagemResultado_CntSrvCod ;
      private String[] P00BY14_A894LogResponsavel_Acao ;
      private int[] P00BY14_A892LogResponsavel_DemandaCod ;
      private bool[] P00BY14_n892LogResponsavel_DemandaCod ;
      private String[] P00BY14_A1130LogResponsavel_Status ;
      private bool[] P00BY14_n1130LogResponsavel_Status ;
      private short[] P00BY14_A1650ContagemResultado_PrzInc ;
      private bool[] P00BY14_n1650ContagemResultado_PrzInc ;
      private short[] P00BY14_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00BY14_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P00BY14_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00BY14_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00BY14_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00BY14_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00BY14_A912ContagemResultado_HoraEntrega ;
      private bool[] P00BY14_n912ContagemResultado_HoraEntrega ;
      private String[] P00BY14_A484ContagemResultado_StatusDmn ;
      private bool[] P00BY14_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00BY14_A472ContagemResultado_DataEntrega ;
      private bool[] P00BY14_n472ContagemResultado_DataEntrega ;
      private int[] P00BY14_A890ContagemResultado_Responsavel ;
      private bool[] P00BY14_n890ContagemResultado_Responsavel ;
      private long[] P00BY14_A1797LogResponsavel_Codigo ;
   }

   public class prc_pausarcontinuaros__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00BY9( IGxContext context ,
                                             int AV12OSVinculada ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int AV9Codigo ,
                                             int A456ContagemResultado_Codigo ,
                                             short A483ContagemResultado_StatusCnt )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_OSVinculada], T2.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusCnt] = 7)";
         if ( (0==AV12OSVinculada) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_OSVinculada] = @AV9Codigo)";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV12OSVinculada > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV12OSVinculada)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 6 :
                     return conditional_P00BY9(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (short)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BY2 ;
          prmP00BY2 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY3 ;
          prmP00BY3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY4 ;
          prmP00BY4 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY6 ;
          prmP00BY6 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY7 ;
          prmP00BY7 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY8 ;
          prmP00BY8 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY10 ;
          prmP00BY10 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY11 ;
          prmP00BY11 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY13 ;
          prmP00BY13 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY14 ;
          prmP00BY14 = new Object[] {
          new Object[] {"@AV12OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY15 ;
          prmP00BY15 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY16 ;
          prmP00BY16 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BY9 ;
          prmP00BY9 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12OSVinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BY2", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_Acao], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T3.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T2.[ContagemResultado_PrazoMaisDias], T2.[ContagemResultado_PrazoInicialDias], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T2.[ContagemResultado_HoraEntrega], T2.[ContagemResultado_OSVinculada], T1.[LogResponsavel_Status], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_Responsavel], T1.[LogResponsavel_Codigo] FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV9Codigo) AND (T1.[LogResponsavel_Acao] = 'B') ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BY2,1,0,true,true )
             ,new CursorDef("P00BY3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY3)
             ,new CursorDef("P00BY4", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY4)
             ,new CursorDef("P00BY6", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_StatusDmn], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV9Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BY6,1,0,true,true )
             ,new CursorDef("P00BY7", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY7)
             ,new CursorDef("P00BY8", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY8)
             ,new CursorDef("P00BY9", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BY9,1,0,true,true )
             ,new CursorDef("P00BY10", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY10)
             ,new CursorDef("P00BY11", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY11)
             ,new CursorDef("P00BY13", "SELECT TOP 1 T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_OSVinculada] = @AV9Codigo) AND (COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BY13,1,0,false,true )
             ,new CursorDef("P00BY14", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_Acao], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Status], T3.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T2.[ContagemResultado_PrazoMaisDias], T2.[ContagemResultado_PrazoInicialDias], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T2.[ContagemResultado_HoraEntrega], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_Responsavel], T1.[LogResponsavel_Codigo] FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV12OSVinculada) AND (T1.[LogResponsavel_Acao] = 'B') ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BY14,1,0,true,true )
             ,new CursorDef("P00BY15", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY15)
             ,new CursorDef("P00BY16", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BY16)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((long[]) buf[25])[0] = rslt.getLong(14) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((long[]) buf[23])[0] = rslt.getLong(13) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
       }
    }

 }

}
