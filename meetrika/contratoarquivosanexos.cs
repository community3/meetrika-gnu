/*
               File: ContratoArquivosAnexos
        Description: Contrato Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:17.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoarquivosanexos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoArquivosAnexos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOARQUIVOSANEXOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoArquivosAnexos_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoarquivosanexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoarquivosanexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoArquivosAnexos_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoArquivosAnexos_Codigo = aP1_ContratoArquivosAnexos_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_83_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table3_83_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0L22e( true) ;
         }
         else
         {
            wb_table1_2_0L22e( false) ;
         }
      }

      protected void wb_table3_83_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_83_0L22e( true) ;
         }
         else
         {
            wb_table3_83_0L22e( false) ;
         }
      }

      protected void wb_table2_5_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0L22e( true) ;
         }
         else
         {
            wb_table2_5_0L22e( false) ;
         }
      }

      protected void wb_table4_13_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "ao Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoArquivosAnexos.htm");
            wb_table5_17_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_48_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table6_48_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0L22e( true) ;
         }
         else
         {
            wb_table4_13_0L22e( false) ;
         }
      }

      protected void wb_table6_48_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoarquivosanexos_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoarquivosanexos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoArquivosAnexos_Descricao_Internalname, A110ContratoArquivosAnexos_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, 1, edtContratoArquivosAnexos_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoarquivosanexos_arquivo_Internalname, "Arquivo", "", "", lblTextblockcontratoarquivosanexos_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  class='DataContentCell'>") ;
            wb_table7_58_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table7_58_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoarquivosanexos_nomearq_Internalname, "Nome", "", "", lblTextblockcontratoarquivosanexos_nomearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoArquivosAnexos_NomeArq_Internalname, StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq), StringUtil.RTrim( context.localUtil.Format( A112ContratoArquivosAnexos_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoArquivosAnexos_NomeArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoArquivosAnexos_NomeArq_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoarquivosanexos_tipoarq_Internalname, "Tipo", "", "", lblTextblockcontratoarquivosanexos_tipoarq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoArquivosAnexos_TipoArq_Internalname, StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq), StringUtil.RTrim( context.localUtil.Format( A109ContratoArquivosAnexos_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoArquivosAnexos_TipoArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoArquivosAnexos_TipoArq_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoarquivosanexos_data_Internalname, "Data/Hora", "", "", lblTextblockcontratoarquivosanexos_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoArquivosAnexos_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoArquivosAnexos_Data_Internalname, context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoArquivosAnexos_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContratoArquivosAnexos_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtContratoArquivosAnexos_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoArquivosAnexos_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_48_0L22e( true) ;
         }
         else
         {
            wb_table6_48_0L22e( false) ;
         }
      }

      protected void wb_table7_58_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoarquivosanexos_arquivo_Internalname, tblTablemergedcontratoarquivosanexos_arquivo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            edtContratoArquivosAnexos_Arquivo_Filename = A112ContratoArquivosAnexos_NomeArq;
            edtContratoArquivosAnexos_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            edtContratoArquivosAnexos_Arquivo_Filetype = A109ContratoArquivosAnexos_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
            {
               gxblobfileaux.Source = A111ContratoArquivosAnexos_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContratoArquivosAnexos_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContratoArquivosAnexos_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A111ContratoArquivosAnexos_Arquivo = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
                  edtContratoArquivosAnexos_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtContratoArquivosAnexos_Arquivo_Internalname, StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo), context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filetype)) ? A111ContratoArquivosAnexos_Arquivo : edtContratoArquivosAnexos_Arquivo_Filetype)) : edtContratoArquivosAnexos_Arquivo_Contenttype), true, "", edtContratoArquivosAnexos_Arquivo_Parameters, 0, edtContratoArquivosAnexos_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtContratoArquivosAnexos_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", "", "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleararquivoanexo_Internalname, context.GetImagePath( "b2e8640a-9c28-412e-aa86-7f21d878f895", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgCleararquivoanexo_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleararquivoanexo_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEARARQUIVOANEXO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_58_0L22e( true) ;
         }
         else
         {
            wb_table7_58_0L22e( false) ;
         }
      }

      protected void wb_table5_17_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_22_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table8_22_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table9_38_0L22( true) ;
         }
         return  ;
      }

      protected void wb_table9_38_0L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_0L22e( true) ;
         }
         else
         {
            wb_table5_17_0L22e( false) ;
         }
      }

      protected void wb_table9_38_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_38_0L22e( true) ;
         }
         else
         {
            wb_table9_38_0L22e( false) ;
         }
      }

      protected void wb_table8_22_0L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_22_0L22e( true) ;
         }
         else
         {
            wb_table8_22_0L22e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110L2 */
         E110L2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               A110ContratoArquivosAnexos_Descricao = cgiGet( edtContratoArquivosAnexos_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110ContratoArquivosAnexos_Descricao", A110ContratoArquivosAnexos_Descricao);
               A111ContratoArquivosAnexos_Arquivo = cgiGet( edtContratoArquivosAnexos_Arquivo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
               A113ContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( edtContratoArquivosAnexos_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
               /* Read saved values. */
               Z108ContratoArquivosAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z108ContratoArquivosAnexos_Codigo"), ",", "."));
               Z113ContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( "Z113ContratoArquivosAnexos_Data"), 0);
               O109ContratoArquivosAnexos_TipoArq = cgiGet( "O109ContratoArquivosAnexos_TipoArq");
               O112ContratoArquivosAnexos_NomeArq = cgiGet( "O112ContratoArquivosAnexos_NomeArq");
               O111ContratoArquivosAnexos_Arquivo = cgiGet( "O111ContratoArquivosAnexos_Arquivo");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               AV7ContratoArquivosAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOARQUIVOSANEXOS_CODIGO"), ",", "."));
               A108ContratoArquivosAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOARQUIVOSANEXOS_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtContratoArquivosAnexos_Arquivo_Filetype = cgiGet( "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filetype");
               edtContratoArquivosAnexos_Arquivo_Filename = cgiGet( "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filename");
               edtContratoArquivosAnexos_Arquivo_Filename = cgiGet( "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filename");
               edtContratoArquivosAnexos_Arquivo_Filetype = cgiGet( "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
               {
                  edtContratoArquivosAnexos_Arquivo_Filename = (String)(CGIGetFileName(edtContratoArquivosAnexos_Arquivo_Internalname));
                  edtContratoArquivosAnexos_Arquivo_Filetype = (String)(CGIGetFileType(edtContratoArquivosAnexos_Arquivo_Internalname));
               }
               A109ContratoArquivosAnexos_TipoArq = edtContratoArquivosAnexos_Arquivo_Filetype;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
               A112ContratoArquivosAnexos_NomeArq = edtContratoArquivosAnexos_Arquivo_Filename;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
               {
                  GXCCtlgxBlob = "CONTRATOARQUIVOSANEXOS_ARQUIVO" + "_gxBlob";
                  A111ContratoArquivosAnexos_Arquivo = cgiGet( GXCCtlgxBlob);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoArquivosAnexos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoarquivosanexos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoarquivosanexos:[SecurityCheckFailed value for]"+"ContratoArquivosAnexos_Codigo:"+context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A108ContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode22 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode22;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound22 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0L0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110L2 */
                           E110L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120L2 */
                           E120L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEARARQUIVOANEXO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130L2 */
                           E130L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120L2 */
            E120L2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0L22( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0L22( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0L0( )
      {
         BeforeValidate0L22( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0L22( ) ;
            }
            else
            {
               CheckExtendedTable0L22( ) ;
               CloseExtendedTableCursors0L22( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0L0( )
      {
      }

      protected void E110L2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E120L2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoarquivosanexos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E130L2( )
      {
         /* 'DoClearArquivoAnexo' Routine */
         new prc_cleararquivoanexo(context ).execute(  A108ContratoArquivosAnexos_Codigo,  "Contrato") ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0L22( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z113ContratoArquivosAnexos_Data = T000L3_A113ContratoArquivosAnexos_Data[0];
            }
            else
            {
               Z113ContratoArquivosAnexos_Data = A113ContratoArquivosAnexos_Data;
            }
         }
         if ( GX_JID == -12 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z108ContratoArquivosAnexos_Codigo = A108ContratoArquivosAnexos_Codigo;
            Z113ContratoArquivosAnexos_Data = A113ContratoArquivosAnexos_Data;
            Z112ContratoArquivosAnexos_NomeArq = A112ContratoArquivosAnexos_NomeArq;
            Z109ContratoArquivosAnexos_TipoArq = A109ContratoArquivosAnexos_TipoArq;
            Z110ContratoArquivosAnexos_Descricao = A110ContratoArquivosAnexos_Descricao;
            Z111ContratoArquivosAnexos_Arquivo = A111ContratoArquivosAnexos_Arquivo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoArquivosAnexos_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_Data_Enabled), 5, 0)));
         AV14Pgmname = "ContratoArquivosAnexos";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContratoArquivosAnexos_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_Data_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoArquivosAnexos_Codigo) )
         {
            A108ContratoArquivosAnexos_Codigo = AV7ContratoArquivosAnexos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
         }
         /* Using cursor T000L4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A77Contrato_Numero = T000L4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T000L4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T000L4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T000L4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A39Contratada_Codigo = T000L4_A39Contratada_Codigo[0];
         pr_default.close(2);
         /* Using cursor T000L5 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T000L5_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T000L6 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000L6_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000L6_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000L6_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000L6_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A113ContratoArquivosAnexos_Data) && ( Gx_BScreen == 0 ) )
         {
            A113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
            {
               A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
      }

      protected void Load0L22( )
      {
         /* Using cursor T000L7 */
         pr_default.execute(5, new Object[] {A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound22 = 1;
            A113ContratoArquivosAnexos_Data = T000L7_A113ContratoArquivosAnexos_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            A112ContratoArquivosAnexos_NomeArq = T000L7_A112ContratoArquivosAnexos_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
            edtContratoArquivosAnexos_Arquivo_Filename = A112ContratoArquivosAnexos_NomeArq;
            A109ContratoArquivosAnexos_TipoArq = T000L7_A109ContratoArquivosAnexos_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
            edtContratoArquivosAnexos_Arquivo_Filetype = A109ContratoArquivosAnexos_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            A77Contrato_Numero = T000L7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000L7_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000L7_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000L7_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T000L7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000L7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000L7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000L7_n42Contratada_PessoaCNPJ[0];
            A110ContratoArquivosAnexos_Descricao = T000L7_A110ContratoArquivosAnexos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110ContratoArquivosAnexos_Descricao", A110ContratoArquivosAnexos_Descricao);
            A39Contratada_Codigo = T000L7_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = T000L7_A40Contratada_PessoaCod[0];
            A111ContratoArquivosAnexos_Arquivo = T000L7_A111ContratoArquivosAnexos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
            ZM0L22( -12) ;
         }
         pr_default.close(5);
         OnLoadActions0L22( ) ;
      }

      protected void OnLoadActions0L22( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
      }

      protected void CheckExtendedTable0L22( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
      }

      protected void CloseExtendedTableCursors0L22( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0L22( )
      {
         /* Using cursor T000L8 */
         pr_default.execute(6, new Object[] {A108ContratoArquivosAnexos_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound22 = 1;
         }
         else
         {
            RcdFound22 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000L3 */
         pr_default.execute(1, new Object[] {A108ContratoArquivosAnexos_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T000L3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
         {
            ZM0L22( 12) ;
            RcdFound22 = 1;
            A108ContratoArquivosAnexos_Codigo = T000L3_A108ContratoArquivosAnexos_Codigo[0];
            A113ContratoArquivosAnexos_Data = T000L3_A113ContratoArquivosAnexos_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            A112ContratoArquivosAnexos_NomeArq = T000L3_A112ContratoArquivosAnexos_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
            edtContratoArquivosAnexos_Arquivo_Filename = A112ContratoArquivosAnexos_NomeArq;
            A109ContratoArquivosAnexos_TipoArq = T000L3_A109ContratoArquivosAnexos_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
            edtContratoArquivosAnexos_Arquivo_Filetype = A109ContratoArquivosAnexos_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            A110ContratoArquivosAnexos_Descricao = T000L3_A110ContratoArquivosAnexos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110ContratoArquivosAnexos_Descricao", A110ContratoArquivosAnexos_Descricao);
            A111ContratoArquivosAnexos_Arquivo = T000L3_A111ContratoArquivosAnexos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
            O109ContratoArquivosAnexos_TipoArq = A109ContratoArquivosAnexos_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
            O112ContratoArquivosAnexos_NomeArq = A112ContratoArquivosAnexos_NomeArq;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
            O111ContratoArquivosAnexos_Arquivo = A111ContratoArquivosAnexos_Arquivo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
            Z108ContratoArquivosAnexos_Codigo = A108ContratoArquivosAnexos_Codigo;
            sMode22 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0L22( ) ;
            if ( AnyError == 1 )
            {
               RcdFound22 = 0;
               InitializeNonKey0L22( ) ;
            }
            Gx_mode = sMode22;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound22 = 0;
            InitializeNonKey0L22( ) ;
            sMode22 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode22;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0L22( ) ;
         if ( RcdFound22 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound22 = 0;
         /* Using cursor T000L9 */
         pr_default.execute(7, new Object[] {A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000L9_A108ContratoArquivosAnexos_Codigo[0] < A108ContratoArquivosAnexos_Codigo ) ) && ( T000L9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000L9_A108ContratoArquivosAnexos_Codigo[0] > A108ContratoArquivosAnexos_Codigo ) ) && ( T000L9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A108ContratoArquivosAnexos_Codigo = T000L9_A108ContratoArquivosAnexos_Codigo[0];
               RcdFound22 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound22 = 0;
         /* Using cursor T000L10 */
         pr_default.execute(8, new Object[] {A108ContratoArquivosAnexos_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000L10_A108ContratoArquivosAnexos_Codigo[0] > A108ContratoArquivosAnexos_Codigo ) ) && ( T000L10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000L10_A108ContratoArquivosAnexos_Codigo[0] < A108ContratoArquivosAnexos_Codigo ) ) && ( T000L10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A108ContratoArquivosAnexos_Codigo = T000L10_A108ContratoArquivosAnexos_Codigo[0];
               RcdFound22 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0L22( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0L22( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound22 == 1 )
            {
               if ( A108ContratoArquivosAnexos_Codigo != Z108ContratoArquivosAnexos_Codigo )
               {
                  A108ContratoArquivosAnexos_Codigo = Z108ContratoArquivosAnexos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0L22( ) ;
                  GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A108ContratoArquivosAnexos_Codigo != Z108ContratoArquivosAnexos_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0L22( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0L22( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A108ContratoArquivosAnexos_Codigo != Z108ContratoArquivosAnexos_Codigo )
         {
            A108ContratoArquivosAnexos_Codigo = Z108ContratoArquivosAnexos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoArquivosAnexos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0L22( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000L2 */
            pr_default.execute(0, new Object[] {A108ContratoArquivosAnexos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoArquivosAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z113ContratoArquivosAnexos_Data != T000L2_A113ContratoArquivosAnexos_Data[0] ) )
            {
               if ( Z113ContratoArquivosAnexos_Data != T000L2_A113ContratoArquivosAnexos_Data[0] )
               {
                  GXUtil.WriteLog("contratoarquivosanexos:[seudo value changed for attri]"+"ContratoArquivosAnexos_Data");
                  GXUtil.WriteLogRaw("Old: ",Z113ContratoArquivosAnexos_Data);
                  GXUtil.WriteLogRaw("Current: ",T000L2_A113ContratoArquivosAnexos_Data[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoArquivosAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0L22( )
      {
         BeforeValidate0L22( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0L22( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0L22( 0) ;
            CheckOptimisticConcurrency0L22( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0L22( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0L22( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000L11 */
                     A112ContratoArquivosAnexos_NomeArq = edtContratoArquivosAnexos_Arquivo_Filename;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
                     A109ContratoArquivosAnexos_TipoArq = edtContratoArquivosAnexos_Arquivo_Filetype;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
                     pr_default.execute(9, new Object[] {A74Contrato_Codigo, A113ContratoArquivosAnexos_Data, A112ContratoArquivosAnexos_NomeArq, A109ContratoArquivosAnexos_TipoArq, A110ContratoArquivosAnexos_Descricao, A111ContratoArquivosAnexos_Arquivo});
                     A108ContratoArquivosAnexos_Codigo = T000L11_A108ContratoArquivosAnexos_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoArquivosAnexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0L0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0L22( ) ;
            }
            EndLevel0L22( ) ;
         }
         CloseExtendedTableCursors0L22( ) ;
      }

      protected void Update0L22( )
      {
         BeforeValidate0L22( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0L22( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0L22( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0L22( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0L22( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000L12 */
                     A112ContratoArquivosAnexos_NomeArq = edtContratoArquivosAnexos_Arquivo_Filename;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
                     A109ContratoArquivosAnexos_TipoArq = edtContratoArquivosAnexos_Arquivo_Filetype;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
                     pr_default.execute(10, new Object[] {A74Contrato_Codigo, A113ContratoArquivosAnexos_Data, A112ContratoArquivosAnexos_NomeArq, A109ContratoArquivosAnexos_TipoArq, A110ContratoArquivosAnexos_Descricao, A108ContratoArquivosAnexos_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoArquivosAnexos") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoArquivosAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0L22( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0L22( ) ;
         }
         CloseExtendedTableCursors0L22( ) ;
      }

      protected void DeferredUpdate0L22( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000L13 */
            pr_default.execute(11, new Object[] {A111ContratoArquivosAnexos_Arquivo, A108ContratoArquivosAnexos_Codigo});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoArquivosAnexos") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate0L22( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0L22( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0L22( ) ;
            AfterConfirm0L22( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0L22( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000L14 */
                  pr_default.execute(12, new Object[] {A108ContratoArquivosAnexos_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoArquivosAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode22 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0L22( ) ;
         Gx_mode = sMode22;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0L22( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            imgCleararquivoanexo_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleararquivoanexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCleararquivoanexo_Visible), 5, 0)));
         }
      }

      protected void EndLevel0L22( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0L22( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoArquivosAnexos");
            if ( AnyError == 0 )
            {
               ConfirmValues0L0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoArquivosAnexos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0L22( )
      {
         /* Scan By routine */
         /* Using cursor T000L15 */
         pr_default.execute(13, new Object[] {A74Contrato_Codigo});
         RcdFound22 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound22 = 1;
            A108ContratoArquivosAnexos_Codigo = T000L15_A108ContratoArquivosAnexos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0L22( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound22 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound22 = 1;
            A108ContratoArquivosAnexos_Codigo = T000L15_A108ContratoArquivosAnexos_Codigo[0];
         }
      }

      protected void ScanEnd0L22( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm0L22( )
      {
         /* After Confirm Rules */
         if ( StringUtil.StrCmp(A111ContratoArquivosAnexos_Arquivo, O111ContratoArquivosAnexos_Arquivo) != 0 )
         {
            A113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(A109ContratoArquivosAnexos_TipoArq, O109ContratoArquivosAnexos_TipoArq) == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
         {
            A109ContratoArquivosAnexos_TipoArq = edtContratoArquivosAnexos_Arquivo_Filetype;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
         }
         if ( ( StringUtil.StrCmp(A112ContratoArquivosAnexos_NomeArq, O112ContratoArquivosAnexos_NomeArq) == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
         {
            A112ContratoArquivosAnexos_NomeArq = edtContratoArquivosAnexos_Arquivo_Filename;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
         }
      }

      protected void BeforeInsert0L22( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0L22( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0L22( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0L22( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0L22( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0L22( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoArquivosAnexos_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_Descricao_Enabled), 5, 0)));
         edtContratoArquivosAnexos_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_Arquivo_Enabled), 5, 0)));
         edtContratoArquivosAnexos_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_NomeArq_Enabled), 5, 0)));
         edtContratoArquivosAnexos_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_TipoArq_Enabled), 5, 0)));
         edtContratoArquivosAnexos_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoArquivosAnexos_Data_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0L0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117171914");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoarquivosanexos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoArquivosAnexos_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z108ContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z113ContratoArquivosAnexos_Data", context.localUtil.TToC( Z113ContratoArquivosAnexos_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "O109ContratoArquivosAnexos_TipoArq", StringUtil.RTrim( O109ContratoArquivosAnexos_TipoArq));
         GxWebStd.gx_hidden_field( context, "O112ContratoArquivosAnexos_NomeArq", StringUtil.RTrim( O112ContratoArquivosAnexos_NomeArq));
         GxWebStd.gx_hidden_field( context, "O111ContratoArquivosAnexos_Arquivo", O111ContratoArquivosAnexos_Arquivo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOARQUIVOSANEXOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoArquivosAnexos_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "CONTRATOARQUIVOSANEXOS_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A111ContratoArquivosAnexos_Arquivo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filetype", StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filename", StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filename", StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_ARQUIVO_Filetype", StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoArquivosAnexos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoarquivosanexos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoarquivosanexos:[SendSecurityCheck value for]"+"ContratoArquivosAnexos_Codigo:"+context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoarquivosanexos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoArquivosAnexos_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoArquivosAnexos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Arquivos Anexos" ;
      }

      protected void InitializeNonKey0L22( )
      {
         A112ContratoArquivosAnexos_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
         A109ContratoArquivosAnexos_TipoArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
         A110ContratoArquivosAnexos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110ContratoArquivosAnexos_Descricao", A110ContratoArquivosAnexos_Descricao);
         A111ContratoArquivosAnexos_Arquivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
         A113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         O109ContratoArquivosAnexos_TipoArq = A109ContratoArquivosAnexos_TipoArq;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109ContratoArquivosAnexos_TipoArq", A109ContratoArquivosAnexos_TipoArq);
         O112ContratoArquivosAnexos_NomeArq = A112ContratoArquivosAnexos_NomeArq;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112ContratoArquivosAnexos_NomeArq", A112ContratoArquivosAnexos_NomeArq);
         O111ContratoArquivosAnexos_Arquivo = A111ContratoArquivosAnexos_Arquivo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111ContratoArquivosAnexos_Arquivo", A111ContratoArquivosAnexos_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
         Z113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
      }

      protected void InitAll0L22( )
      {
         A108ContratoArquivosAnexos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108ContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0)));
         InitializeNonKey0L22( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A113ContratoArquivosAnexos_Data = i113ContratoArquivosAnexos_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113ContratoArquivosAnexos_Data", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117171939");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoarquivosanexos.js", "?20203117171939");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratoarquivosanexos_descricao_Internalname = "TEXTBLOCKCONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         lblTextblockcontratoarquivosanexos_arquivo_Internalname = "TEXTBLOCKCONTRATOARQUIVOSANEXOS_ARQUIVO";
         edtContratoArquivosAnexos_Arquivo_Internalname = "CONTRATOARQUIVOSANEXOS_ARQUIVO";
         imgCleararquivoanexo_Internalname = "CLEARARQUIVOANEXO";
         tblTablemergedcontratoarquivosanexos_arquivo_Internalname = "TABLEMERGEDCONTRATOARQUIVOSANEXOS_ARQUIVO";
         lblTextblockcontratoarquivosanexos_nomearq_Internalname = "TEXTBLOCKCONTRATOARQUIVOSANEXOS_NOMEARQ";
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ";
         lblTextblockcontratoarquivosanexos_tipoarq_Internalname = "TEXTBLOCKCONTRATOARQUIVOSANEXOS_TIPOARQ";
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ";
         lblTextblockcontratoarquivosanexos_data_Internalname = "TEXTBLOCKCONTRATOARQUIVOSANEXOS_DATA";
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Arquivo Anexo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Arquivos Anexos";
         edtContratoArquivosAnexos_Arquivo_Filename = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         imgCleararquivoanexo_Visible = 1;
         edtContratoArquivosAnexos_Arquivo_Jsonclick = "";
         edtContratoArquivosAnexos_Arquivo_Parameters = "";
         edtContratoArquivosAnexos_Arquivo_Contenttype = "";
         edtContratoArquivosAnexos_Arquivo_Filetype = "";
         edtContratoArquivosAnexos_Arquivo_Enabled = 1;
         edtContratoArquivosAnexos_Data_Jsonclick = "";
         edtContratoArquivosAnexos_Data_Enabled = 0;
         edtContratoArquivosAnexos_TipoArq_Jsonclick = "";
         edtContratoArquivosAnexos_TipoArq_Enabled = 1;
         edtContratoArquivosAnexos_NomeArq_Jsonclick = "";
         edtContratoArquivosAnexos_NomeArq_Enabled = 1;
         edtContratoArquivosAnexos_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoArquivosAnexos_Codigo',fld:'vCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120L2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOCLEARARQUIVOANEXO'","{handler:'E130L2',iparms:[{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         O109ContratoArquivosAnexos_TipoArq = "";
         O112ContratoArquivosAnexos_NomeArq = "";
         O111ContratoArquivosAnexos_Arquivo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratoarquivosanexos_descricao_Jsonclick = "";
         A110ContratoArquivosAnexos_Descricao = "";
         lblTextblockcontratoarquivosanexos_arquivo_Jsonclick = "";
         lblTextblockcontratoarquivosanexos_nomearq_Jsonclick = "";
         A112ContratoArquivosAnexos_NomeArq = "";
         lblTextblockcontratoarquivosanexos_tipoarq_Jsonclick = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         lblTextblockcontratoarquivosanexos_data_Jsonclick = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A111ContratoArquivosAnexos_Arquivo = "";
         imgCleararquivoanexo_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode22 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z112ContratoArquivosAnexos_NomeArq = "";
         Z109ContratoArquivosAnexos_TipoArq = "";
         Z110ContratoArquivosAnexos_Descricao = "";
         Z111ContratoArquivosAnexos_Arquivo = "";
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T000L4_A77Contrato_Numero = new String[] {""} ;
         T000L4_A78Contrato_NumeroAta = new String[] {""} ;
         T000L4_n78Contrato_NumeroAta = new bool[] {false} ;
         T000L4_A79Contrato_Ano = new short[1] ;
         T000L4_A39Contratada_Codigo = new int[1] ;
         T000L5_A40Contratada_PessoaCod = new int[1] ;
         T000L6_A41Contratada_PessoaNom = new String[] {""} ;
         T000L6_n41Contratada_PessoaNom = new bool[] {false} ;
         T000L6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000L6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000L7_A74Contrato_Codigo = new int[1] ;
         T000L7_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L7_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T000L7_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         T000L7_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         T000L7_A77Contrato_Numero = new String[] {""} ;
         T000L7_A78Contrato_NumeroAta = new String[] {""} ;
         T000L7_n78Contrato_NumeroAta = new bool[] {false} ;
         T000L7_A79Contrato_Ano = new short[1] ;
         T000L7_A41Contratada_PessoaNom = new String[] {""} ;
         T000L7_n41Contratada_PessoaNom = new bool[] {false} ;
         T000L7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000L7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000L7_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         T000L7_A39Contratada_Codigo = new int[1] ;
         T000L7_A40Contratada_PessoaCod = new int[1] ;
         T000L7_A111ContratoArquivosAnexos_Arquivo = new String[] {""} ;
         T000L8_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L3_A74Contrato_Codigo = new int[1] ;
         T000L3_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L3_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T000L3_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         T000L3_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         T000L3_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         T000L3_A111ContratoArquivosAnexos_Arquivo = new String[] {""} ;
         T000L9_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L9_A74Contrato_Codigo = new int[1] ;
         T000L10_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L10_A74Contrato_Codigo = new int[1] ;
         T000L2_A74Contrato_Codigo = new int[1] ;
         T000L2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T000L2_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         T000L2_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         T000L2_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         T000L2_A111ContratoArquivosAnexos_Arquivo = new String[] {""} ;
         T000L11_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         T000L15_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoarquivosanexos__default(),
            new Object[][] {
                new Object[] {
               T000L2_A74Contrato_Codigo, T000L2_A108ContratoArquivosAnexos_Codigo, T000L2_A113ContratoArquivosAnexos_Data, T000L2_A112ContratoArquivosAnexos_NomeArq, T000L2_A109ContratoArquivosAnexos_TipoArq, T000L2_A110ContratoArquivosAnexos_Descricao, T000L2_A111ContratoArquivosAnexos_Arquivo
               }
               , new Object[] {
               T000L3_A74Contrato_Codigo, T000L3_A108ContratoArquivosAnexos_Codigo, T000L3_A113ContratoArquivosAnexos_Data, T000L3_A112ContratoArquivosAnexos_NomeArq, T000L3_A109ContratoArquivosAnexos_TipoArq, T000L3_A110ContratoArquivosAnexos_Descricao, T000L3_A111ContratoArquivosAnexos_Arquivo
               }
               , new Object[] {
               T000L4_A77Contrato_Numero, T000L4_A78Contrato_NumeroAta, T000L4_n78Contrato_NumeroAta, T000L4_A79Contrato_Ano, T000L4_A39Contratada_Codigo
               }
               , new Object[] {
               T000L5_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000L6_A41Contratada_PessoaNom, T000L6_n41Contratada_PessoaNom, T000L6_A42Contratada_PessoaCNPJ, T000L6_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000L7_A74Contrato_Codigo, T000L7_A108ContratoArquivosAnexos_Codigo, T000L7_A113ContratoArquivosAnexos_Data, T000L7_A112ContratoArquivosAnexos_NomeArq, T000L7_A109ContratoArquivosAnexos_TipoArq, T000L7_A77Contrato_Numero, T000L7_A78Contrato_NumeroAta, T000L7_n78Contrato_NumeroAta, T000L7_A79Contrato_Ano, T000L7_A41Contratada_PessoaNom,
               T000L7_n41Contratada_PessoaNom, T000L7_A42Contratada_PessoaCNPJ, T000L7_n42Contratada_PessoaCNPJ, T000L7_A110ContratoArquivosAnexos_Descricao, T000L7_A39Contratada_Codigo, T000L7_A40Contratada_PessoaCod, T000L7_A111ContratoArquivosAnexos_Arquivo
               }
               , new Object[] {
               T000L8_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               T000L9_A108ContratoArquivosAnexos_Codigo, T000L9_A74Contrato_Codigo
               }
               , new Object[] {
               T000L10_A108ContratoArquivosAnexos_Codigo, T000L10_A74Contrato_Codigo
               }
               , new Object[] {
               T000L11_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000L15_A108ContratoArquivosAnexos_Codigo
               }
            }
         );
         N74Contrato_Codigo = 0;
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV14Pgmname = "ContratoArquivosAnexos";
         Z113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i113ContratoArquivosAnexos_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short Gx_BScreen ;
      private short RcdFound22 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short gxajaxcallmode ;
      private int wcpOAV7ContratoArquivosAnexos_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z108ContratoArquivosAnexos_Codigo ;
      private int N74Contrato_Codigo ;
      private int AV7ContratoArquivosAnexos_Codigo ;
      private int A74Contrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoArquivosAnexos_Descricao_Enabled ;
      private int edtContratoArquivosAnexos_NomeArq_Enabled ;
      private int edtContratoArquivosAnexos_TipoArq_Enabled ;
      private int edtContratoArquivosAnexos_Data_Enabled ;
      private int edtContratoArquivosAnexos_Arquivo_Enabled ;
      private int imgCleararquivoanexo_Visible ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV15GXV1 ;
      private int Z74Contrato_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String O109ContratoArquivosAnexos_TipoArq ;
      private String O112ContratoArquivosAnexos_NomeArq ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoArquivosAnexos_Descricao_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockcontratoarquivosanexos_descricao_Internalname ;
      private String lblTextblockcontratoarquivosanexos_descricao_Jsonclick ;
      private String lblTextblockcontratoarquivosanexos_arquivo_Internalname ;
      private String lblTextblockcontratoarquivosanexos_arquivo_Jsonclick ;
      private String lblTextblockcontratoarquivosanexos_nomearq_Internalname ;
      private String lblTextblockcontratoarquivosanexos_nomearq_Jsonclick ;
      private String edtContratoArquivosAnexos_NomeArq_Internalname ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String edtContratoArquivosAnexos_NomeArq_Jsonclick ;
      private String lblTextblockcontratoarquivosanexos_tipoarq_Internalname ;
      private String lblTextblockcontratoarquivosanexos_tipoarq_Jsonclick ;
      private String edtContratoArquivosAnexos_TipoArq_Internalname ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private String edtContratoArquivosAnexos_TipoArq_Jsonclick ;
      private String lblTextblockcontratoarquivosanexos_data_Internalname ;
      private String lblTextblockcontratoarquivosanexos_data_Jsonclick ;
      private String edtContratoArquivosAnexos_Data_Internalname ;
      private String edtContratoArquivosAnexos_Data_Jsonclick ;
      private String tblTablemergedcontratoarquivosanexos_arquivo_Internalname ;
      private String edtContratoArquivosAnexos_Arquivo_Filename ;
      private String edtContratoArquivosAnexos_Arquivo_Filetype ;
      private String edtContratoArquivosAnexos_Arquivo_Internalname ;
      private String edtContratoArquivosAnexos_Arquivo_Contenttype ;
      private String edtContratoArquivosAnexos_Arquivo_Parameters ;
      private String edtContratoArquivosAnexos_Arquivo_Jsonclick ;
      private String imgCleararquivoanexo_Internalname ;
      private String imgCleararquivoanexo_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode22 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z112ContratoArquivosAnexos_NomeArq ;
      private String Z109ContratoArquivosAnexos_TipoArq ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z113ContratoArquivosAnexos_Data ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private DateTime i113ContratoArquivosAnexos_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A110ContratoArquivosAnexos_Descricao ;
      private String Z110ContratoArquivosAnexos_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private String O111ContratoArquivosAnexos_Arquivo ;
      private String A111ContratoArquivosAnexos_Arquivo ;
      private String Z111ContratoArquivosAnexos_Arquivo ;
      private IGxSession AV10WebSession ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T000L4_A77Contrato_Numero ;
      private String[] T000L4_A78Contrato_NumeroAta ;
      private bool[] T000L4_n78Contrato_NumeroAta ;
      private short[] T000L4_A79Contrato_Ano ;
      private int[] T000L4_A39Contratada_Codigo ;
      private int[] T000L5_A40Contratada_PessoaCod ;
      private String[] T000L6_A41Contratada_PessoaNom ;
      private bool[] T000L6_n41Contratada_PessoaNom ;
      private String[] T000L6_A42Contratada_PessoaCNPJ ;
      private bool[] T000L6_n42Contratada_PessoaCNPJ ;
      private int[] T000L7_A74Contrato_Codigo ;
      private int[] T000L7_A108ContratoArquivosAnexos_Codigo ;
      private DateTime[] T000L7_A113ContratoArquivosAnexos_Data ;
      private String[] T000L7_A112ContratoArquivosAnexos_NomeArq ;
      private String[] T000L7_A109ContratoArquivosAnexos_TipoArq ;
      private String[] T000L7_A77Contrato_Numero ;
      private String[] T000L7_A78Contrato_NumeroAta ;
      private bool[] T000L7_n78Contrato_NumeroAta ;
      private short[] T000L7_A79Contrato_Ano ;
      private String[] T000L7_A41Contratada_PessoaNom ;
      private bool[] T000L7_n41Contratada_PessoaNom ;
      private String[] T000L7_A42Contratada_PessoaCNPJ ;
      private bool[] T000L7_n42Contratada_PessoaCNPJ ;
      private String[] T000L7_A110ContratoArquivosAnexos_Descricao ;
      private int[] T000L7_A39Contratada_Codigo ;
      private int[] T000L7_A40Contratada_PessoaCod ;
      private String[] T000L7_A111ContratoArquivosAnexos_Arquivo ;
      private int[] T000L8_A108ContratoArquivosAnexos_Codigo ;
      private int[] T000L3_A74Contrato_Codigo ;
      private int[] T000L3_A108ContratoArquivosAnexos_Codigo ;
      private DateTime[] T000L3_A113ContratoArquivosAnexos_Data ;
      private String[] T000L3_A112ContratoArquivosAnexos_NomeArq ;
      private String[] T000L3_A109ContratoArquivosAnexos_TipoArq ;
      private String[] T000L3_A110ContratoArquivosAnexos_Descricao ;
      private String[] T000L3_A111ContratoArquivosAnexos_Arquivo ;
      private int[] T000L9_A108ContratoArquivosAnexos_Codigo ;
      private int[] T000L9_A74Contrato_Codigo ;
      private int[] T000L10_A108ContratoArquivosAnexos_Codigo ;
      private int[] T000L10_A74Contrato_Codigo ;
      private int[] T000L2_A74Contrato_Codigo ;
      private int[] T000L2_A108ContratoArquivosAnexos_Codigo ;
      private DateTime[] T000L2_A113ContratoArquivosAnexos_Data ;
      private String[] T000L2_A112ContratoArquivosAnexos_NomeArq ;
      private String[] T000L2_A109ContratoArquivosAnexos_TipoArq ;
      private String[] T000L2_A110ContratoArquivosAnexos_Descricao ;
      private String[] T000L2_A111ContratoArquivosAnexos_Arquivo ;
      private int[] T000L11_A108ContratoArquivosAnexos_Codigo ;
      private int[] T000L15_A108ContratoArquivosAnexos_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratoarquivosanexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000L4 ;
          prmT000L4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L5 ;
          prmT000L5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L6 ;
          prmT000L6 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L7 ;
          prmT000L7 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L8 ;
          prmT000L8 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L3 ;
          prmT000L3 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L9 ;
          prmT000L9 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L10 ;
          prmT000L10 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L2 ;
          prmT000L2 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L11 ;
          prmT000L11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContratoArquivosAnexos_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoArquivosAnexos_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContratoArquivosAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoArquivosAnexos_Arquivo",SqlDbType.VarBinary,1024,0}
          } ;
          Object[] prmT000L12 ;
          prmT000L12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContratoArquivosAnexos_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoArquivosAnexos_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContratoArquivosAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L13 ;
          prmT000L13 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L14 ;
          prmT000L14 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000L15 ;
          prmT000L15 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000L2", "SELECT [Contrato_Codigo], [ContratoArquivosAnexos_Codigo], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Descricao], [ContratoArquivosAnexos_Arquivo] FROM [ContratoArquivosAnexos] WITH (UPDLOCK) WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000L2,1,0,true,false )
             ,new CursorDef("T000L3", "SELECT [Contrato_Codigo], [ContratoArquivosAnexos_Codigo], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Descricao], [ContratoArquivosAnexos_Arquivo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000L3,1,0,true,false )
             ,new CursorDef("T000L4", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000L4,1,0,true,false )
             ,new CursorDef("T000L5", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000L5,1,0,true,false )
             ,new CursorDef("T000L6", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000L6,1,0,true,false )
             ,new CursorDef("T000L7", "SELECT TM1.[Contrato_Codigo], TM1.[ContratoArquivosAnexos_Codigo], TM1.[ContratoArquivosAnexos_Data], TM1.[ContratoArquivosAnexos_NomeArq], TM1.[ContratoArquivosAnexos_TipoArq], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoArquivosAnexos_Descricao], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, TM1.[ContratoArquivosAnexos_Arquivo] FROM ((([ContratoArquivosAnexos] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo and TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[ContratoArquivosAnexos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000L7,100,0,true,false )
             ,new CursorDef("T000L8", "SELECT [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000L8,1,0,true,false )
             ,new CursorDef("T000L9", "SELECT TOP 1 [ContratoArquivosAnexos_Codigo], [Contrato_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE ( [ContratoArquivosAnexos_Codigo] > @ContratoArquivosAnexos_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoArquivosAnexos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000L9,1,0,true,true )
             ,new CursorDef("T000L10", "SELECT TOP 1 [ContratoArquivosAnexos_Codigo], [Contrato_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE ( [ContratoArquivosAnexos_Codigo] < @ContratoArquivosAnexos_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoArquivosAnexos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000L10,1,0,true,true )
             ,new CursorDef("T000L11", "INSERT INTO [ContratoArquivosAnexos]([Contrato_Codigo], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Descricao], [ContratoArquivosAnexos_Arquivo]) VALUES(@Contrato_Codigo, @ContratoArquivosAnexos_Data, @ContratoArquivosAnexos_NomeArq, @ContratoArquivosAnexos_TipoArq, @ContratoArquivosAnexos_Descricao, @ContratoArquivosAnexos_Arquivo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000L11)
             ,new CursorDef("T000L12", "UPDATE [ContratoArquivosAnexos] SET [Contrato_Codigo]=@Contrato_Codigo, [ContratoArquivosAnexos_Data]=@ContratoArquivosAnexos_Data, [ContratoArquivosAnexos_NomeArq]=@ContratoArquivosAnexos_NomeArq, [ContratoArquivosAnexos_TipoArq]=@ContratoArquivosAnexos_TipoArq, [ContratoArquivosAnexos_Descricao]=@ContratoArquivosAnexos_Descricao  WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT000L12)
             ,new CursorDef("T000L13", "UPDATE [ContratoArquivosAnexos] SET [ContratoArquivosAnexos_Arquivo]=@ContratoArquivosAnexos_Arquivo  WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT000L13)
             ,new CursorDef("T000L14", "DELETE FROM [ContratoArquivosAnexos]  WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT000L14)
             ,new CursorDef("T000L15", "SELECT [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoArquivosAnexos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000L15,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getBLOBFile(7, rslt.getString(5, 10), rslt.getString(4, 50)) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getBLOBFile(7, rslt.getString(5, 10), rslt.getString(4, 50)) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((String[]) buf[9])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(9);
                ((String[]) buf[11])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(11) ;
                ((int[]) buf[14])[0] = rslt.getInt(12) ;
                ((int[]) buf[15])[0] = rslt.getInt(13) ;
                ((String[]) buf[16])[0] = rslt.getBLOBFile(14, rslt.getString(5, 10), rslt.getString(4, 50)) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
