/*
               File: TipoProjeto
        Description: Tipos de Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:1.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tipoprojeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7TipoProjeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoProjeto_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTIPOPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoProjeto_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tipos de Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTipoProjeto_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tipoprojeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tipoprojeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_TipoProjeto_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7TipoProjeto_Codigo = aP1_TipoProjeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2Y118( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2Y118e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoProjeto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A979TipoProjeto_Codigo), 6, 0, ",", "")), ((edtTipoProjeto_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoProjeto_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTipoProjeto_Codigo_Visible, edtTipoProjeto_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TipoProjeto.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2Y118( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2Y118( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2Y118e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_2Y118( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_2Y118e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2Y118e( true) ;
         }
         else
         {
            wb_table1_2_2Y118e( false) ;
         }
      }

      protected void wb_table3_31_2Y118( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_2Y118e( true) ;
         }
         else
         {
            wb_table3_31_2Y118e( false) ;
         }
      }

      protected void wb_table2_5_2Y118( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2Y118( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2Y118e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2Y118e( true) ;
         }
         else
         {
            wb_table2_5_2Y118e( false) ;
         }
      }

      protected void wb_table4_13_2Y118( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipoprojeto_nome_Internalname, "Nome", "", "", lblTextblocktipoprojeto_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoProjeto_Nome_Internalname, StringUtil.RTrim( A980TipoProjeto_Nome), StringUtil.RTrim( context.localUtil.Format( A980TipoProjeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoProjeto_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipoProjeto_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipoprojeto_prazo_Internalname, "Prazo �timo", "", "", lblTextblocktipoprojeto_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoProjeto_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( A981TipoProjeto_Prazo, 5, 2, ",", "")), ((edtTipoProjeto_Prazo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A981TipoProjeto_Prazo, "Z9.99")) : context.localUtil.Format( A981TipoProjeto_Prazo, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoProjeto_Prazo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipoProjeto_Prazo_Enabled, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipoprojeto_cnstexp_Internalname, "Constante exponencial", "", "", lblTextblocktipoprojeto_cnstexp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoProjeto_CnstExp_Internalname, StringUtil.LTrim( StringUtil.NToC( A982TipoProjeto_CnstExp, 5, 2, ",", "")), ((edtTipoProjeto_CnstExp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A982TipoProjeto_CnstExp, "Z9.99")) : context.localUtil.Format( A982TipoProjeto_CnstExp, "Z9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoProjeto_CnstExp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTipoProjeto_CnstExp_Enabled, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_TipoProjeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2Y118e( true) ;
         }
         else
         {
            wb_table4_13_2Y118e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112Y2 */
         E112Y2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A980TipoProjeto_Nome = StringUtil.Upper( cgiGet( edtTipoProjeto_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A980TipoProjeto_Nome", A980TipoProjeto_Nome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTipoProjeto_Prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTipoProjeto_Prazo_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TIPOPROJETO_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoProjeto_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A981TipoProjeto_Prazo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( A981TipoProjeto_Prazo, 5, 2)));
               }
               else
               {
                  A981TipoProjeto_Prazo = context.localUtil.CToN( cgiGet( edtTipoProjeto_Prazo_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( A981TipoProjeto_Prazo, 5, 2)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtTipoProjeto_CnstExp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTipoProjeto_CnstExp_Internalname), ",", ".") > 99.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TIPOPROJETO_CNSTEXP");
                  AnyError = 1;
                  GX_FocusControl = edtTipoProjeto_CnstExp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A982TipoProjeto_CnstExp = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( A982TipoProjeto_CnstExp, 5, 2)));
               }
               else
               {
                  A982TipoProjeto_CnstExp = context.localUtil.CToN( cgiGet( edtTipoProjeto_CnstExp_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( A982TipoProjeto_CnstExp, 5, 2)));
               }
               A979TipoProjeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
               /* Read saved values. */
               Z979TipoProjeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z979TipoProjeto_Codigo"), ",", "."));
               Z980TipoProjeto_Nome = cgiGet( "Z980TipoProjeto_Nome");
               Z981TipoProjeto_Prazo = context.localUtil.CToN( cgiGet( "Z981TipoProjeto_Prazo"), ",", ".");
               Z982TipoProjeto_CnstExp = context.localUtil.CToN( cgiGet( "Z982TipoProjeto_CnstExp"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7TipoProjeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "vTIPOPROJETO_CODIGO"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "TipoProjeto";
               A979TipoProjeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A979TipoProjeto_Codigo != Z979TipoProjeto_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tipoprojeto:[SecurityCheckFailed value for]"+"TipoProjeto_Codigo:"+context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tipoprojeto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A979TipoProjeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode118 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode118;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound118 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2Y0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TIPOPROJETO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTipoProjeto_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112Y2 */
                           E112Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122Y2 */
                           E122Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122Y2 */
            E122Y2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2Y118( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2Y118( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2Y0( )
      {
         BeforeValidate2Y118( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2Y118( ) ;
            }
            else
            {
               CheckExtendedTable2Y118( ) ;
               CloseExtendedTableCursors2Y118( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2Y0( )
      {
      }

      protected void E112Y2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtTipoProjeto_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Codigo_Visible), 5, 0)));
      }

      protected void E122Y2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtipoprojeto.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2Y118( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z980TipoProjeto_Nome = T002Y3_A980TipoProjeto_Nome[0];
               Z981TipoProjeto_Prazo = T002Y3_A981TipoProjeto_Prazo[0];
               Z982TipoProjeto_CnstExp = T002Y3_A982TipoProjeto_CnstExp[0];
            }
            else
            {
               Z980TipoProjeto_Nome = A980TipoProjeto_Nome;
               Z981TipoProjeto_Prazo = A981TipoProjeto_Prazo;
               Z982TipoProjeto_CnstExp = A982TipoProjeto_CnstExp;
            }
         }
         if ( GX_JID == -3 )
         {
            Z979TipoProjeto_Codigo = A979TipoProjeto_Codigo;
            Z980TipoProjeto_Nome = A980TipoProjeto_Nome;
            Z981TipoProjeto_Prazo = A981TipoProjeto_Prazo;
            Z982TipoProjeto_CnstExp = A982TipoProjeto_CnstExp;
         }
      }

      protected void standaloneNotModal( )
      {
         edtTipoProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Codigo_Enabled), 5, 0)));
         edtTipoProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7TipoProjeto_Codigo) )
         {
            A979TipoProjeto_Codigo = AV7TipoProjeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load2Y118( )
      {
         /* Using cursor T002Y4 */
         pr_default.execute(2, new Object[] {A979TipoProjeto_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound118 = 1;
            A980TipoProjeto_Nome = T002Y4_A980TipoProjeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A980TipoProjeto_Nome", A980TipoProjeto_Nome);
            A981TipoProjeto_Prazo = T002Y4_A981TipoProjeto_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( A981TipoProjeto_Prazo, 5, 2)));
            A982TipoProjeto_CnstExp = T002Y4_A982TipoProjeto_CnstExp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( A982TipoProjeto_CnstExp, 5, 2)));
            ZM2Y118( -3) ;
         }
         pr_default.close(2);
         OnLoadActions2Y118( ) ;
      }

      protected void OnLoadActions2Y118( )
      {
      }

      protected void CheckExtendedTable2Y118( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors2Y118( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2Y118( )
      {
         /* Using cursor T002Y5 */
         pr_default.execute(3, new Object[] {A979TipoProjeto_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound118 = 1;
         }
         else
         {
            RcdFound118 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002Y3 */
         pr_default.execute(1, new Object[] {A979TipoProjeto_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2Y118( 3) ;
            RcdFound118 = 1;
            A979TipoProjeto_Codigo = T002Y3_A979TipoProjeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
            A980TipoProjeto_Nome = T002Y3_A980TipoProjeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A980TipoProjeto_Nome", A980TipoProjeto_Nome);
            A981TipoProjeto_Prazo = T002Y3_A981TipoProjeto_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( A981TipoProjeto_Prazo, 5, 2)));
            A982TipoProjeto_CnstExp = T002Y3_A982TipoProjeto_CnstExp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( A982TipoProjeto_CnstExp, 5, 2)));
            Z979TipoProjeto_Codigo = A979TipoProjeto_Codigo;
            sMode118 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2Y118( ) ;
            if ( AnyError == 1 )
            {
               RcdFound118 = 0;
               InitializeNonKey2Y118( ) ;
            }
            Gx_mode = sMode118;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound118 = 0;
            InitializeNonKey2Y118( ) ;
            sMode118 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode118;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2Y118( ) ;
         if ( RcdFound118 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound118 = 0;
         /* Using cursor T002Y6 */
         pr_default.execute(4, new Object[] {A979TipoProjeto_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T002Y6_A979TipoProjeto_Codigo[0] < A979TipoProjeto_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T002Y6_A979TipoProjeto_Codigo[0] > A979TipoProjeto_Codigo ) ) )
            {
               A979TipoProjeto_Codigo = T002Y6_A979TipoProjeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
               RcdFound118 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound118 = 0;
         /* Using cursor T002Y7 */
         pr_default.execute(5, new Object[] {A979TipoProjeto_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T002Y7_A979TipoProjeto_Codigo[0] > A979TipoProjeto_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T002Y7_A979TipoProjeto_Codigo[0] < A979TipoProjeto_Codigo ) ) )
            {
               A979TipoProjeto_Codigo = T002Y7_A979TipoProjeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
               RcdFound118 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2Y118( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTipoProjeto_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2Y118( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound118 == 1 )
            {
               if ( A979TipoProjeto_Codigo != Z979TipoProjeto_Codigo )
               {
                  A979TipoProjeto_Codigo = Z979TipoProjeto_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TIPOPROJETO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoProjeto_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTipoProjeto_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2Y118( ) ;
                  GX_FocusControl = edtTipoProjeto_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A979TipoProjeto_Codigo != Z979TipoProjeto_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtTipoProjeto_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2Y118( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TIPOPROJETO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTipoProjeto_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTipoProjeto_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2Y118( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A979TipoProjeto_Codigo != Z979TipoProjeto_Codigo )
         {
            A979TipoProjeto_Codigo = Z979TipoProjeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TIPOPROJETO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipoProjeto_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTipoProjeto_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2Y118( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002Y2 */
            pr_default.execute(0, new Object[] {A979TipoProjeto_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoProjeto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z980TipoProjeto_Nome, T002Y2_A980TipoProjeto_Nome[0]) != 0 ) || ( Z981TipoProjeto_Prazo != T002Y2_A981TipoProjeto_Prazo[0] ) || ( Z982TipoProjeto_CnstExp != T002Y2_A982TipoProjeto_CnstExp[0] ) )
            {
               if ( StringUtil.StrCmp(Z980TipoProjeto_Nome, T002Y2_A980TipoProjeto_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tipoprojeto:[seudo value changed for attri]"+"TipoProjeto_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z980TipoProjeto_Nome);
                  GXUtil.WriteLogRaw("Current: ",T002Y2_A980TipoProjeto_Nome[0]);
               }
               if ( Z981TipoProjeto_Prazo != T002Y2_A981TipoProjeto_Prazo[0] )
               {
                  GXUtil.WriteLog("tipoprojeto:[seudo value changed for attri]"+"TipoProjeto_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z981TipoProjeto_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T002Y2_A981TipoProjeto_Prazo[0]);
               }
               if ( Z982TipoProjeto_CnstExp != T002Y2_A982TipoProjeto_CnstExp[0] )
               {
                  GXUtil.WriteLog("tipoprojeto:[seudo value changed for attri]"+"TipoProjeto_CnstExp");
                  GXUtil.WriteLogRaw("Old: ",Z982TipoProjeto_CnstExp);
                  GXUtil.WriteLogRaw("Current: ",T002Y2_A982TipoProjeto_CnstExp[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TipoProjeto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2Y118( )
      {
         BeforeValidate2Y118( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Y118( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2Y118( 0) ;
            CheckOptimisticConcurrency2Y118( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Y118( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2Y118( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Y8 */
                     pr_default.execute(6, new Object[] {A980TipoProjeto_Nome, A981TipoProjeto_Prazo, A982TipoProjeto_CnstExp});
                     A979TipoProjeto_Codigo = T002Y8_A979TipoProjeto_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoProjeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2Y0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2Y118( ) ;
            }
            EndLevel2Y118( ) ;
         }
         CloseExtendedTableCursors2Y118( ) ;
      }

      protected void Update2Y118( )
      {
         BeforeValidate2Y118( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2Y118( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Y118( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2Y118( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2Y118( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002Y9 */
                     pr_default.execute(7, new Object[] {A980TipoProjeto_Nome, A981TipoProjeto_Prazo, A982TipoProjeto_CnstExp, A979TipoProjeto_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoProjeto") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoProjeto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2Y118( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2Y118( ) ;
         }
         CloseExtendedTableCursors2Y118( ) ;
      }

      protected void DeferredUpdate2Y118( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2Y118( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2Y118( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2Y118( ) ;
            AfterConfirm2Y118( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2Y118( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002Y10 */
                  pr_default.execute(8, new Object[] {A979TipoProjeto_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("TipoProjeto") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode118 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2Y118( ) ;
         Gx_mode = sMode118;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2Y118( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T002Y11 */
            pr_default.execute(9, new Object[] {A979TipoProjeto_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel2Y118( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2Y118( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "TipoProjeto");
            if ( AnyError == 0 )
            {
               ConfirmValues2Y0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "TipoProjeto");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2Y118( )
      {
         /* Scan By routine */
         /* Using cursor T002Y12 */
         pr_default.execute(10);
         RcdFound118 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound118 = 1;
            A979TipoProjeto_Codigo = T002Y12_A979TipoProjeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2Y118( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound118 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound118 = 1;
            A979TipoProjeto_Codigo = T002Y12_A979TipoProjeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2Y118( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm2Y118( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2Y118( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2Y118( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2Y118( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2Y118( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2Y118( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2Y118( )
      {
         edtTipoProjeto_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Nome_Enabled), 5, 0)));
         edtTipoProjeto_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Prazo_Enabled), 5, 0)));
         edtTipoProjeto_CnstExp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_CnstExp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_CnstExp_Enabled), 5, 0)));
         edtTipoProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoProjeto_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2Y0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311726225");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tipoprojeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoProjeto_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z979TipoProjeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z980TipoProjeto_Nome", StringUtil.RTrim( Z980TipoProjeto_Nome));
         GxWebStd.gx_hidden_field( context, "Z981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.NToC( Z981TipoProjeto_Prazo, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.NToC( Z982TipoProjeto_CnstExp, 5, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTIPOPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7TipoProjeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTIPOPROJETO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7TipoProjeto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "TipoProjeto";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tipoprojeto:[SendSecurityCheck value for]"+"TipoProjeto_Codigo:"+context.localUtil.Format( (decimal)(A979TipoProjeto_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tipoprojeto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tipoprojeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7TipoProjeto_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "TipoProjeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipos de Projeto" ;
      }

      protected void InitializeNonKey2Y118( )
      {
         A980TipoProjeto_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A980TipoProjeto_Nome", A980TipoProjeto_Nome);
         A981TipoProjeto_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A981TipoProjeto_Prazo", StringUtil.LTrim( StringUtil.Str( A981TipoProjeto_Prazo, 5, 2)));
         A982TipoProjeto_CnstExp = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A982TipoProjeto_CnstExp", StringUtil.LTrim( StringUtil.Str( A982TipoProjeto_CnstExp, 5, 2)));
         Z980TipoProjeto_Nome = "";
         Z981TipoProjeto_Prazo = 0;
         Z982TipoProjeto_CnstExp = 0;
      }

      protected void InitAll2Y118( )
      {
         A979TipoProjeto_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A979TipoProjeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A979TipoProjeto_Codigo), 6, 0)));
         InitializeNonKey2Y118( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311726237");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tipoprojeto.js", "?2020311726237");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktipoprojeto_nome_Internalname = "TEXTBLOCKTIPOPROJETO_NOME";
         edtTipoProjeto_Nome_Internalname = "TIPOPROJETO_NOME";
         lblTextblocktipoprojeto_prazo_Internalname = "TEXTBLOCKTIPOPROJETO_PRAZO";
         edtTipoProjeto_Prazo_Internalname = "TIPOPROJETO_PRAZO";
         lblTextblocktipoprojeto_cnstexp_Internalname = "TEXTBLOCKTIPOPROJETO_CNSTEXP";
         edtTipoProjeto_CnstExp_Internalname = "TIPOPROJETO_CNSTEXP";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtTipoProjeto_Codigo_Internalname = "TIPOPROJETO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Tipo de Projeto";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tipos de Projeto";
         edtTipoProjeto_CnstExp_Jsonclick = "";
         edtTipoProjeto_CnstExp_Enabled = 1;
         edtTipoProjeto_Prazo_Jsonclick = "";
         edtTipoProjeto_Prazo_Enabled = 1;
         edtTipoProjeto_Nome_Jsonclick = "";
         edtTipoProjeto_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtTipoProjeto_Codigo_Jsonclick = "";
         edtTipoProjeto_Codigo_Enabled = 0;
         edtTipoProjeto_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7TipoProjeto_Codigo',fld:'vTIPOPROJETO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122Y2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z980TipoProjeto_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktipoprojeto_nome_Jsonclick = "";
         A980TipoProjeto_Nome = "";
         lblTextblocktipoprojeto_prazo_Jsonclick = "";
         lblTextblocktipoprojeto_cnstexp_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode118 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T002Y4_A979TipoProjeto_Codigo = new int[1] ;
         T002Y4_A980TipoProjeto_Nome = new String[] {""} ;
         T002Y4_A981TipoProjeto_Prazo = new decimal[1] ;
         T002Y4_A982TipoProjeto_CnstExp = new decimal[1] ;
         T002Y5_A979TipoProjeto_Codigo = new int[1] ;
         T002Y3_A979TipoProjeto_Codigo = new int[1] ;
         T002Y3_A980TipoProjeto_Nome = new String[] {""} ;
         T002Y3_A981TipoProjeto_Prazo = new decimal[1] ;
         T002Y3_A982TipoProjeto_CnstExp = new decimal[1] ;
         T002Y6_A979TipoProjeto_Codigo = new int[1] ;
         T002Y7_A979TipoProjeto_Codigo = new int[1] ;
         T002Y2_A979TipoProjeto_Codigo = new int[1] ;
         T002Y2_A980TipoProjeto_Nome = new String[] {""} ;
         T002Y2_A981TipoProjeto_Prazo = new decimal[1] ;
         T002Y2_A982TipoProjeto_CnstExp = new decimal[1] ;
         T002Y8_A979TipoProjeto_Codigo = new int[1] ;
         T002Y11_A648Projeto_Codigo = new int[1] ;
         T002Y12_A979TipoProjeto_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tipoprojeto__default(),
            new Object[][] {
                new Object[] {
               T002Y2_A979TipoProjeto_Codigo, T002Y2_A980TipoProjeto_Nome, T002Y2_A981TipoProjeto_Prazo, T002Y2_A982TipoProjeto_CnstExp
               }
               , new Object[] {
               T002Y3_A979TipoProjeto_Codigo, T002Y3_A980TipoProjeto_Nome, T002Y3_A981TipoProjeto_Prazo, T002Y3_A982TipoProjeto_CnstExp
               }
               , new Object[] {
               T002Y4_A979TipoProjeto_Codigo, T002Y4_A980TipoProjeto_Nome, T002Y4_A981TipoProjeto_Prazo, T002Y4_A982TipoProjeto_CnstExp
               }
               , new Object[] {
               T002Y5_A979TipoProjeto_Codigo
               }
               , new Object[] {
               T002Y6_A979TipoProjeto_Codigo
               }
               , new Object[] {
               T002Y7_A979TipoProjeto_Codigo
               }
               , new Object[] {
               T002Y8_A979TipoProjeto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002Y11_A648Projeto_Codigo
               }
               , new Object[] {
               T002Y12_A979TipoProjeto_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound118 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7TipoProjeto_Codigo ;
      private int Z979TipoProjeto_Codigo ;
      private int AV7TipoProjeto_Codigo ;
      private int trnEnded ;
      private int A979TipoProjeto_Codigo ;
      private int edtTipoProjeto_Codigo_Enabled ;
      private int edtTipoProjeto_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtTipoProjeto_Nome_Enabled ;
      private int edtTipoProjeto_Prazo_Enabled ;
      private int edtTipoProjeto_CnstExp_Enabled ;
      private int idxLst ;
      private decimal Z981TipoProjeto_Prazo ;
      private decimal Z982TipoProjeto_CnstExp ;
      private decimal A981TipoProjeto_Prazo ;
      private decimal A982TipoProjeto_CnstExp ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z980TipoProjeto_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTipoProjeto_Nome_Internalname ;
      private String edtTipoProjeto_Codigo_Internalname ;
      private String edtTipoProjeto_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktipoprojeto_nome_Internalname ;
      private String lblTextblocktipoprojeto_nome_Jsonclick ;
      private String A980TipoProjeto_Nome ;
      private String edtTipoProjeto_Nome_Jsonclick ;
      private String lblTextblocktipoprojeto_prazo_Internalname ;
      private String lblTextblocktipoprojeto_prazo_Jsonclick ;
      private String edtTipoProjeto_Prazo_Internalname ;
      private String edtTipoProjeto_Prazo_Jsonclick ;
      private String lblTextblocktipoprojeto_cnstexp_Internalname ;
      private String lblTextblocktipoprojeto_cnstexp_Jsonclick ;
      private String edtTipoProjeto_CnstExp_Internalname ;
      private String edtTipoProjeto_CnstExp_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode118 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T002Y4_A979TipoProjeto_Codigo ;
      private String[] T002Y4_A980TipoProjeto_Nome ;
      private decimal[] T002Y4_A981TipoProjeto_Prazo ;
      private decimal[] T002Y4_A982TipoProjeto_CnstExp ;
      private int[] T002Y5_A979TipoProjeto_Codigo ;
      private int[] T002Y3_A979TipoProjeto_Codigo ;
      private String[] T002Y3_A980TipoProjeto_Nome ;
      private decimal[] T002Y3_A981TipoProjeto_Prazo ;
      private decimal[] T002Y3_A982TipoProjeto_CnstExp ;
      private int[] T002Y6_A979TipoProjeto_Codigo ;
      private int[] T002Y7_A979TipoProjeto_Codigo ;
      private int[] T002Y2_A979TipoProjeto_Codigo ;
      private String[] T002Y2_A980TipoProjeto_Nome ;
      private decimal[] T002Y2_A981TipoProjeto_Prazo ;
      private decimal[] T002Y2_A982TipoProjeto_CnstExp ;
      private int[] T002Y8_A979TipoProjeto_Codigo ;
      private int[] T002Y11_A648Projeto_Codigo ;
      private int[] T002Y12_A979TipoProjeto_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class tipoprojeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002Y4 ;
          prmT002Y4 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y5 ;
          prmT002Y5 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y3 ;
          prmT002Y3 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y6 ;
          prmT002Y6 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y7 ;
          prmT002Y7 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y2 ;
          prmT002Y2 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y8 ;
          prmT002Y8 = new Object[] {
          new Object[] {"@TipoProjeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoProjeto_Prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@TipoProjeto_CnstExp",SqlDbType.Decimal,5,2}
          } ;
          Object[] prmT002Y9 ;
          prmT002Y9 = new Object[] {
          new Object[] {"@TipoProjeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoProjeto_Prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@TipoProjeto_CnstExp",SqlDbType.Decimal,5,2} ,
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y10 ;
          prmT002Y10 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y11 ;
          prmT002Y11 = new Object[] {
          new Object[] {"@TipoProjeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002Y12 ;
          prmT002Y12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T002Y2", "SELECT [TipoProjeto_Codigo], [TipoProjeto_Nome], [TipoProjeto_Prazo], [TipoProjeto_CnstExp] FROM [TipoProjeto] WITH (UPDLOCK) WHERE [TipoProjeto_Codigo] = @TipoProjeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y2,1,0,true,false )
             ,new CursorDef("T002Y3", "SELECT [TipoProjeto_Codigo], [TipoProjeto_Nome], [TipoProjeto_Prazo], [TipoProjeto_CnstExp] FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @TipoProjeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y3,1,0,true,false )
             ,new CursorDef("T002Y4", "SELECT TM1.[TipoProjeto_Codigo], TM1.[TipoProjeto_Nome], TM1.[TipoProjeto_Prazo], TM1.[TipoProjeto_CnstExp] FROM [TipoProjeto] TM1 WITH (NOLOCK) WHERE TM1.[TipoProjeto_Codigo] = @TipoProjeto_Codigo ORDER BY TM1.[TipoProjeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y4,100,0,true,false )
             ,new CursorDef("T002Y5", "SELECT [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @TipoProjeto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y5,1,0,true,false )
             ,new CursorDef("T002Y6", "SELECT TOP 1 [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK) WHERE ( [TipoProjeto_Codigo] > @TipoProjeto_Codigo) ORDER BY [TipoProjeto_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y6,1,0,true,true )
             ,new CursorDef("T002Y7", "SELECT TOP 1 [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK) WHERE ( [TipoProjeto_Codigo] < @TipoProjeto_Codigo) ORDER BY [TipoProjeto_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y7,1,0,true,true )
             ,new CursorDef("T002Y8", "INSERT INTO [TipoProjeto]([TipoProjeto_Nome], [TipoProjeto_Prazo], [TipoProjeto_CnstExp]) VALUES(@TipoProjeto_Nome, @TipoProjeto_Prazo, @TipoProjeto_CnstExp); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002Y8)
             ,new CursorDef("T002Y9", "UPDATE [TipoProjeto] SET [TipoProjeto_Nome]=@TipoProjeto_Nome, [TipoProjeto_Prazo]=@TipoProjeto_Prazo, [TipoProjeto_CnstExp]=@TipoProjeto_CnstExp  WHERE [TipoProjeto_Codigo] = @TipoProjeto_Codigo", GxErrorMask.GX_NOMASK,prmT002Y9)
             ,new CursorDef("T002Y10", "DELETE FROM [TipoProjeto]  WHERE [TipoProjeto_Codigo] = @TipoProjeto_Codigo", GxErrorMask.GX_NOMASK,prmT002Y10)
             ,new CursorDef("T002Y11", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_TipoProjetoCod] = @TipoProjeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y11,1,0,true,true )
             ,new CursorDef("T002Y12", "SELECT [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK) ORDER BY [TipoProjeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002Y12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
