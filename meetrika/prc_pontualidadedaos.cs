/*
               File: PRC_PontualidadeDaOS
        Description: Pontualidade Da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:52.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pontualidadedaos : GXProcedure
   {
      public prc_pontualidadedaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pontualidadedaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           decimal aP1_AFaturar ,
                           out decimal aP2_Reduz ,
                           out decimal aP3_GlsValor ,
                           out decimal aP4_GlsPF ,
                           out String aP5_Descricao )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV27AFaturar = aP1_AFaturar;
         this.AV18Reduz = 0 ;
         this.AV20GlsValor = 0 ;
         this.AV21GlsPF = 0 ;
         this.AV11Descricao = "" ;
         initialize();
         executePrivate();
         aP2_Reduz=this.AV18Reduz;
         aP3_GlsValor=this.AV20GlsValor;
         aP4_GlsPF=this.AV21GlsPF;
         aP5_Descricao=this.AV11Descricao;
      }

      public String executeUdp( int aP0_Codigo ,
                                decimal aP1_AFaturar ,
                                out decimal aP2_Reduz ,
                                out decimal aP3_GlsValor ,
                                out decimal aP4_GlsPF )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV27AFaturar = aP1_AFaturar;
         this.AV18Reduz = 0 ;
         this.AV20GlsValor = 0 ;
         this.AV21GlsPF = 0 ;
         this.AV11Descricao = "" ;
         initialize();
         executePrivate();
         aP2_Reduz=this.AV18Reduz;
         aP3_GlsValor=this.AV20GlsValor;
         aP4_GlsPF=this.AV21GlsPF;
         aP5_Descricao=this.AV11Descricao;
         return AV11Descricao ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 decimal aP1_AFaturar ,
                                 out decimal aP2_Reduz ,
                                 out decimal aP3_GlsValor ,
                                 out decimal aP4_GlsPF ,
                                 out String aP5_Descricao )
      {
         prc_pontualidadedaos objprc_pontualidadedaos;
         objprc_pontualidadedaos = new prc_pontualidadedaos();
         objprc_pontualidadedaos.AV8Codigo = aP0_Codigo;
         objprc_pontualidadedaos.AV27AFaturar = aP1_AFaturar;
         objprc_pontualidadedaos.AV18Reduz = 0 ;
         objprc_pontualidadedaos.AV20GlsValor = 0 ;
         objprc_pontualidadedaos.AV21GlsPF = 0 ;
         objprc_pontualidadedaos.AV11Descricao = "" ;
         objprc_pontualidadedaos.context.SetSubmitInitialConfig(context);
         objprc_pontualidadedaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pontualidadedaos);
         aP2_Reduz=this.AV18Reduz;
         aP3_GlsValor=this.AV20GlsValor;
         aP4_GlsPF=this.AV21GlsPF;
         aP5_Descricao=this.AV11Descricao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pontualidadedaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_char1 = "";
         new geralog(context ).execute( ref  GXt_char1) ;
         GXt_char2 = "";
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char3 = "";
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char4 = AV36Pgmname + "<<<INICIO>>>";
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char5 = AV36Pgmname + "<<<INICIO>>>";
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char6 = AV36Pgmname + "<<<INICIO>>>";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char7 = AV36Pgmname + "<< 1 >> PARM >> " + " Codigo = " + context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char8 = AV36Pgmname + "<< 2 >> PARM >> " + "&AFaturar = " + context.localUtil.Format( AV27AFaturar, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         new geralog(context ).execute( ref  GXt_char8) ;
         /* Using cursor P00WI2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1405ContagemResultadoExecucao_Codigo = P00WI2_A1405ContagemResultadoExecucao_Codigo[0];
            A471ContagemResultado_DataDmn = P00WI2_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00WI2_n471ContagemResultado_DataDmn[0];
            A512ContagemResultado_ValorPF = P00WI2_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00WI2_n512ContagemResultado_ValorPF[0];
            A1406ContagemResultadoExecucao_Inicio = P00WI2_A1406ContagemResultadoExecucao_Inicio[0];
            A1408ContagemResultadoExecucao_Prevista = P00WI2_A1408ContagemResultadoExecucao_Prevista[0];
            n1408ContagemResultadoExecucao_Prevista = P00WI2_n1408ContagemResultadoExecucao_Prevista[0];
            A1407ContagemResultadoExecucao_Fim = P00WI2_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00WI2_n1407ContagemResultadoExecucao_Fim[0];
            A1410ContagemResultadoExecucao_Dias = P00WI2_A1410ContagemResultadoExecucao_Dias[0];
            n1410ContagemResultadoExecucao_Dias = P00WI2_n1410ContagemResultadoExecucao_Dias[0];
            A1411ContagemResultadoExecucao_PrazoDias = P00WI2_A1411ContagemResultadoExecucao_PrazoDias[0];
            n1411ContagemResultadoExecucao_PrazoDias = P00WI2_n1411ContagemResultadoExecucao_PrazoDias[0];
            A1404ContagemResultadoExecucao_OSCod = P00WI2_A1404ContagemResultadoExecucao_OSCod[0];
            A2028ContagemResultadoExecucao_Glosavel = P00WI2_A2028ContagemResultadoExecucao_Glosavel[0];
            n2028ContagemResultadoExecucao_Glosavel = P00WI2_n2028ContagemResultadoExecucao_Glosavel[0];
            A1553ContagemResultado_CntSrvCod = P00WI2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00WI2_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P00WI2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00WI2_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00WI2_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00WI2_n471ContagemResultado_DataDmn[0];
            A512ContagemResultado_ValorPF = P00WI2_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00WI2_n512ContagemResultado_ValorPF[0];
            A1553ContagemResultado_CntSrvCod = P00WI2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00WI2_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P00WI2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00WI2_n484ContagemResultado_StatusDmn[0];
            OV30Faixa = AV30Faixa;
            GXt_char8 = AV36Pgmname + "<< 3 >> ContagemResultadoExecucao_Codigo = " + context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char8) ;
            AV24Solicitada = A471ContagemResultado_DataDmn;
            AV22ValorPF = A512ContagemResultado_ValorPF;
            AV18Reduz = 0;
            AV19ReduzRS = 0;
            AV21GlsPF = 0;
            AV30Faixa = 0;
            AV29Inicio = A1406ContagemResultadoExecucao_Inicio;
            AV14Previsto = DateTimeUtil.ResetTime(A1408ContagemResultadoExecucao_Prevista);
            AV15Entregue = DateTimeUtil.ResetTime(A1407ContagemResultadoExecucao_Fim);
            AV13DiasExec = A1410ContagemResultadoExecucao_Dias;
            AV16Prazo = A1411ContagemResultadoExecucao_PrazoDias;
            GXt_char8 = AV36Pgmname + "<< 4 >> &Solicitada = " + context.localUtil.Format( AV24Solicitada, "99/99/99");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_char7 = AV36Pgmname + "<< 5 >> &ValorPF = " + context.localUtil.Format( AV22ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999");
            new geralog(context ).execute( ref  GXt_char7) ;
            GXt_char6 = AV36Pgmname + "<< 6 >> &Inicio = " + context.localUtil.Format( AV29Inicio, "99/99/99 99:99");
            new geralog(context ).execute( ref  GXt_char6) ;
            GXt_char5 = AV36Pgmname + "<< 7 >> &Previsto = " + context.localUtil.Format( AV14Previsto, "99/99/99");
            new geralog(context ).execute( ref  GXt_char5) ;
            GXt_char4 = AV36Pgmname + "<< 8 >> &Entregue = " + context.localUtil.Format( AV15Entregue, "99/99/99");
            new geralog(context ).execute( ref  GXt_char4) ;
            GXt_char3 = AV36Pgmname + "<< 9 >> &DiasExec = " + context.localUtil.Format( (decimal)(AV13DiasExec), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char3) ;
            GXt_char2 = AV36Pgmname + "<< 10 >> &Prazo = " + context.localUtil.Format( (decimal)(AV16Prazo), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char2) ;
            new geralog(context ).execute( ref  AV36Pgmname) ;
            AV28OSCodigo = A1404ContagemResultadoExecucao_OSCod;
            GXt_char8 = AV36Pgmname + "<< 17.1 >> &DataExtensao = " + context.localUtil.Format( AV9DataExtensao, "99/99/99 99:99");
            new geralog(context ).execute( ref  GXt_char8) ;
            if ( (DateTime.MinValue==AV9DataExtensao) )
            {
               AV12Atraso = (short)(AV13DiasExec-AV16Prazo);
            }
            else
            {
               if ( A2028ContagemResultadoExecucao_Glosavel )
               {
                  GXt_char8 = "ContagemResultadoExecucao_Glosavel  = TRUE ";
                  new geralog(context ).execute( ref  GXt_char8) ;
                  AV12Atraso = (short)(AV13DiasExec+AV10DiasAnt);
               }
               else
               {
                  GXt_char8 = "ContagemResultadoExecucao_Glosavel  = FALSE ";
                  new geralog(context ).execute( ref  GXt_char8) ;
                  AV12Atraso = (short)(AV13DiasExec-AV16Prazo+AV10DiasAnt);
               }
            }
            GXt_char8 = AV36Pgmname + "<< 18 >> &Atraso = " + context.localUtil.Format( (decimal)(AV12Atraso), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char8) ;
            /* Execute user subroutine: 'CALCULAINDP' */
            S131 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            GXt_char8 = AV36Pgmname + "<< 19 CalculaIndP >> &IndP = " + context.localUtil.Format( AV17IndP, "ZZ9.99");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_decimal9 = AV18Reduz;
            new prc_pontualidadedemandareduz(context ).execute( ref  A1553ContagemResultado_CntSrvCod,  AV12Atraso,  AV17IndP, ref  AV23Indicador, ref  AV31Indicador_Sigla, ref  AV30Faixa, out  GXt_decimal9) ;
            AV18Reduz = GXt_decimal9;
            GXt_char8 = AV36Pgmname + "<< 20 >> &Faixa = " + context.localUtil.Format( (decimal)(AV30Faixa), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_char7 = AV36Pgmname + "<< 21 >> &Reduz = " + context.localUtil.Format( AV18Reduz, "ZZ9.99 %");
            new geralog(context ).execute( ref  GXt_char7) ;
            GXt_char6 = AV36Pgmname + "<< 21.1 >> &DataExtensao = " + context.localUtil.Format( AV9DataExtensao, "99/99/99 99:99");
            new geralog(context ).execute( ref  GXt_char6) ;
            if ( (DateTime.MinValue==AV9DataExtensao) )
            {
               AV11Descricao = AV31Indicador_Sigla + ": Em " + context.localUtil.TToC( AV29Inicio, 8, 5, 0, 3, "/", ":", " ") + " Previsto " + context.localUtil.DToC( AV14Previsto, 2, "/") + " (" + StringUtil.Trim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)) + "d) Entregue " + context.localUtil.DToC( AV15Entregue, 2, "/") + " (" + StringUtil.Trim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)) + "d) Atraso " + StringUtil.Trim( StringUtil.Str( (decimal)(AV12Atraso), 4, 0)) + "d";
            }
            else
            {
               AV11Descricao = AV31Indicador_Sigla + ": Em " + context.localUtil.TToC( AV29Inicio, 8, 5, 0, 3, "/", ":", " ") + " Previsto " + context.localUtil.DToC( AV14Previsto, 2, "/") + " (" + StringUtil.Trim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)) + "d) Entregue " + ((0==AV12Atraso) ? "" : "extendida ") + context.localUtil.DToC( AV15Entregue, 2, "/") + " (" + StringUtil.Trim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)) + "d) Atraso " + StringUtil.Trim( StringUtil.Str( (decimal)(AV12Atraso), 4, 0)) + "d";
            }
            if ( AV12Atraso > 0 )
            {
               AV11Descricao = AV11Descricao + " (Fx." + StringUtil.Trim( StringUtil.Str( (decimal)(AV30Faixa), 4, 0)) + ")";
            }
            GXt_char8 = AV36Pgmname + "<< 22 >> &Descricao = " + StringUtil.RTrim( context.localUtil.Format( AV11Descricao, ""));
            new geralog(context ).execute( ref  GXt_char8) ;
            AV19ReduzRS = 0;
            if ( (Convert.ToDecimal(0)==AV18Reduz) )
            {
               AV11Descricao = AV11Descricao + ".";
            }
            else
            {
               AV19ReduzRS = (decimal)(AV27AFaturar*(AV18Reduz/ (decimal)(100)));
               AV11Descricao = AV11Descricao + " reduz " + StringUtil.Trim( StringUtil.Str( AV18Reduz, 6, 2)) + "% R$ -" + StringUtil.Trim( StringUtil.Str( AV19ReduzRS, 18, 3));
               GXt_char8 = AV36Pgmname + "<< 23 >> &ReduzRS = " + context.localUtil.Format( AV19ReduzRS, "ZZ,ZZZ,ZZZ,ZZ9.999");
               new geralog(context ).execute( ref  GXt_char8) ;
               GXt_char7 = AV36Pgmname + "<< 24 >> &Descricao = " + AV11Descricao;
               new geralog(context ).execute( ref  GXt_char7) ;
            }
            GXt_char6 = AV36Pgmname + "<< 25 StatusDemanda >>  = " + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            new geralog(context ).execute( ref  GXt_char6) ;
            if ( ! ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) ) )
            {
               GXt_char8 = AV36Pgmname + "<< 26 StatusDemanda >>  = " + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
               new geralog(context ).execute( ref  GXt_char8) ;
               /* Execute user subroutine: 'SALVARGLOSA' */
               S121 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            GXt_char8 = AV36Pgmname + "<< 40 >> &ReduzRS = " + context.localUtil.Format( AV19ReduzRS, "ZZ,ZZZ,ZZZ,ZZ9.999");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_char7 = AV36Pgmname + "<< 41 >> &GlsValor = " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
            new geralog(context ).execute( ref  GXt_char7) ;
            GXt_char6 = AV36Pgmname + "<< 42 >> &ValorPF = " + context.localUtil.Format( AV22ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999");
            new geralog(context ).execute( ref  GXt_char6) ;
            AV20GlsValor = AV19ReduzRS;
            GXt_char8 = AV36Pgmname + "<< 43 >> &GlsValor = &ReduzRS = " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
            new geralog(context ).execute( ref  GXt_char8) ;
            if ( ( AV22ValorPF > Convert.ToDecimal( 0 )) )
            {
               AV21GlsPF = (decimal)(AV20GlsValor/ (decimal)(AV22ValorPF));
               GXt_char8 = AV36Pgmname + "<< 44 >> &GlsPF = &GlsValor / &ValorPF = " + context.localUtil.Format( AV21GlsPF, "ZZ,ZZZ,ZZ9.999");
               new geralog(context ).execute( ref  GXt_char8) ;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         GXt_char7 = AV36Pgmname + "<< 45 >> &Reduz		= " + context.localUtil.Format( AV18Reduz, "ZZ9.99 %");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = AV36Pgmname + "<< 46 >> &GlsValor		= " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = AV36Pgmname + "<< 47 >> &GlsPF 		= " + context.localUtil.Format( AV21GlsPF, "ZZ,ZZZ,ZZ9.999");
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = AV36Pgmname + "<< 48 >> &Descricao	= " + AV11Descricao;
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = AV36Pgmname + "<<<FIM>>>";
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = AV36Pgmname + "<<<FIM>>>";
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char1 = AV36Pgmname + "<<<FIM>>>";
         new geralog(context ).execute( ref  GXt_char1) ;
         GXt_char10 = "";
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char11 = "";
         new geralog(context ).execute( ref  GXt_char11) ;
         GXt_char12 = "";
         new geralog(context ).execute( ref  GXt_char12) ;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'EXTENSAO' Routine */
         AV9DataExtensao = (DateTime)(DateTime.MinValue);
         GXt_char12 = AV36Pgmname + "<< 11 >> Extensao = ";
         new geralog(context ).execute( ref  GXt_char12) ;
         GXt_char11 = AV36Pgmname + "<< 11 >> Extensao = ";
         new geralog(context ).execute( ref  GXt_char11) ;
         GXt_char10 = AV36Pgmname + "<< 12 >> &OSCodigo = " + context.localUtil.Format( (decimal)(AV28OSCodigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char8 = AV36Pgmname + "<< 13 >> &&Inicio = " + context.localUtil.Format( AV29Inicio, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char8) ;
         /* Using cursor P00WI3 */
         pr_default.execute(1, new Object[] {AV28OSCodigo, AV29Inicio});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A893LogResponsavel_DataHora = P00WI3_A893LogResponsavel_DataHora[0];
            A894LogResponsavel_Acao = P00WI3_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = P00WI3_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = P00WI3_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P00WI3_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00WI3_n892LogResponsavel_DemandaCod[0];
            GXt_boolean13 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean13) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean13;
            if ( A1149LogResponsavel_OwnerEhContratante )
            {
               AV9DataExtensao = A893LogResponsavel_DataHora;
               GXt_char12 = AV36Pgmname + "<< 14 >> &DataExtensao = " + context.localUtil.Format( AV9DataExtensao, "99/99/99 99:99");
               new geralog(context ).execute( ref  GXt_char12) ;
               GXt_char11 = AV36Pgmname + "<< 14.1 >> LogResponsavel_OwnerEhContratante = " + context.localUtil.Format( (decimal)(A896LogResponsavel_Owner), "ZZZZZ9");
               new geralog(context ).execute( ref  GXt_char11) ;
               GXt_char10 = AV36Pgmname + "<< 14.2 >> LogResponsavel_Codigo = " + context.localUtil.Format( (decimal)(A1797LogResponsavel_Codigo), "ZZZZZZZZZ9");
               new geralog(context ).execute( ref  GXt_char10) ;
               GXt_boolean13 = AV33LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean13) ;
               AV33LogResponsavel_OwnerEhContratante = GXt_boolean13;
               GXt_char12 = AV36Pgmname + "<< 14.3 >> &LogResponsavel_OwnerEhContratante = TRUE = " + StringUtil.BoolToStr( AV33LogResponsavel_OwnerEhContratante);
               new geralog(context ).execute( ref  GXt_char12) ;
               /* Using cursor P00WI4 */
               pr_default.execute(2, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod, A893LogResponsavel_DataHora, AV29Inicio});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1404ContagemResultadoExecucao_OSCod = P00WI4_A1404ContagemResultadoExecucao_OSCod[0];
                  A2028ContagemResultadoExecucao_Glosavel = P00WI4_A2028ContagemResultadoExecucao_Glosavel[0];
                  n2028ContagemResultadoExecucao_Glosavel = P00WI4_n2028ContagemResultadoExecucao_Glosavel[0];
                  A1406ContagemResultadoExecucao_Inicio = P00WI4_A1406ContagemResultadoExecucao_Inicio[0];
                  A1410ContagemResultadoExecucao_Dias = P00WI4_A1410ContagemResultadoExecucao_Dias[0];
                  n1410ContagemResultadoExecucao_Dias = P00WI4_n1410ContagemResultadoExecucao_Dias[0];
                  A1405ContagemResultadoExecucao_Codigo = P00WI4_A1405ContagemResultadoExecucao_Codigo[0];
                  AV10DiasAnt = (short)(AV10DiasAnt+A1410ContagemResultadoExecucao_Dias);
                  GXt_char12 = AV36Pgmname + "<< 15 >> &DiasAnt = " + context.localUtil.Format( (decimal)(AV10DiasAnt), "ZZZ9");
                  new geralog(context ).execute( ref  GXt_char12) ;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         GXt_char11 = AV36Pgmname + "<< 14.1 >> &DataExtensao = " + context.localUtil.Format( AV9DataExtensao, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char11) ;
         GXt_char10 = AV36Pgmname + "<< 15.1 >> &DiasAnt = " + context.localUtil.Format( (decimal)(AV10DiasAnt), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char10) ;
         GXt_char8 = AV36Pgmname + "<< 16 >> &DiasAnt = " + context.localUtil.Format( (decimal)(AV10DiasAnt), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char8) ;
         GXt_char7 = AV36Pgmname + "<< 17 >> Extensao";
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = AV36Pgmname + "<< 17 >> Extensao";
         new geralog(context ).execute( ref  GXt_char6) ;
      }

      protected void S121( )
      {
         /* 'SALVARGLOSA' Routine */
         GXt_char5 = AV36Pgmname + "<< 27 >> SalvarGlosa ";
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = AV36Pgmname + "<< 27 >> SalvarGlosa ";
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = AV36Pgmname + "<< 28 >> &OSCodigo =  " + context.localUtil.Format( (decimal)(AV28OSCodigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = AV36Pgmname + "<< 29 >> &Indicador =  " + context.localUtil.Format( (decimal)(AV23Indicador), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char1 = AV36Pgmname + "<< 30 >> &GlsValor =  " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
         new geralog(context ).execute( ref  GXt_char1) ;
         if ( (Convert.ToDecimal(0)==AV20GlsValor) )
         {
            GXt_char12 = AV36Pgmname + "<< 31 >> &GlsValor.IsEmpty() =  " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
            new geralog(context ).execute( ref  GXt_char12) ;
            GXt_char11 = AV36Pgmname + "<< 32 >> VAI REMOVER A GLOSA EXISTENTE";
            new geralog(context ).execute( ref  GXt_char11) ;
            GXt_char10 = AV36Pgmname + "<< 33 >> VAI REMOVER A GLOSA EXISTENTE";
            new geralog(context ).execute( ref  GXt_char10) ;
            /* Using cursor P00WI5 */
            pr_default.execute(3, new Object[] {AV28OSCodigo, AV23Indicador});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1315ContagemResultadoIndicadores_IndicadorCod = P00WI5_A1315ContagemResultadoIndicadores_IndicadorCod[0];
               A1314ContagemResultadoIndicadores_DemandaCod = P00WI5_A1314ContagemResultadoIndicadores_DemandaCod[0];
               GXt_char12 = AV36Pgmname + "<< 34 >> REMOVENDO A GLOSA EXISTENTE >>>>>>>>>>";
               new geralog(context ).execute( ref  GXt_char12) ;
               /* Using cursor P00WI6 */
               pr_default.execute(4, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            GXt_char11 = AV36Pgmname + "<< 35 >> CONCLUIL A REMO��O DA GLOSA EXISTENTE >>>>>>>>>>";
            new geralog(context ).execute( ref  GXt_char11) ;
         }
         else
         {
            GXt_char10 = AV36Pgmname + "<< 36 >> Verifica se encontrou a glosa - para atualizar = &GlsValor = " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
            new geralog(context ).execute( ref  GXt_char10) ;
            GXt_char8 = AV36Pgmname + "<< 37 >> &OSCodigo  =  " + context.localUtil.Format( (decimal)(AV28OSCodigo), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_char7 = AV36Pgmname + "<< 38 >> &Indicador =  " + context.localUtil.Format( (decimal)(AV23Indicador), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char7) ;
            GXt_char6 = AV36Pgmname + "<< 39.1 >> &Solicitada =  " + context.localUtil.Format( AV24Solicitada, "99/99/99");
            new geralog(context ).execute( ref  GXt_char6) ;
            GXt_char5 = AV36Pgmname + "<< 39.2 >> &Prazo 	  =  " + context.localUtil.Format( (decimal)(AV16Prazo), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char5) ;
            GXt_char4 = AV36Pgmname + "<< 39.3 >> &Entregue   =  " + context.localUtil.Format( AV15Entregue, "99/99/99");
            new geralog(context ).execute( ref  GXt_char4) ;
            GXt_char3 = AV36Pgmname + "<< 39.4 >> &Atraso 	  =  " + context.localUtil.Format( (decimal)(AV12Atraso), "ZZZ9");
            new geralog(context ).execute( ref  GXt_char3) ;
            GXt_char2 = AV36Pgmname + "<< 39.5 >> &Reduz 	  =  " + context.localUtil.Format( AV18Reduz, "ZZ9.99 %");
            new geralog(context ).execute( ref  GXt_char2) ;
            GXt_char1 = AV36Pgmname + "<< 39.6 >> &GlsValor   =  " + context.localUtil.Format( AV20GlsValor, "ZZZZZZZZZZZ9.99999");
            new geralog(context ).execute( ref  GXt_char1) ;
            AV41GXLvl238 = 0;
            /* Using cursor P00WI7 */
            pr_default.execute(5, new Object[] {AV28OSCodigo, AV23Indicador});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A1315ContagemResultadoIndicadores_IndicadorCod = P00WI7_A1315ContagemResultadoIndicadores_IndicadorCod[0];
               A1314ContagemResultadoIndicadores_DemandaCod = P00WI7_A1314ContagemResultadoIndicadores_DemandaCod[0];
               A484ContagemResultado_StatusDmn = P00WI7_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00WI7_n484ContagemResultado_StatusDmn[0];
               A1317ContagemResultadoIndicadores_Data = P00WI7_A1317ContagemResultadoIndicadores_Data[0];
               A1318ContagemResultadoIndicadores_Solicitada = P00WI7_A1318ContagemResultadoIndicadores_Solicitada[0];
               A1319ContagemResultadoIndicadores_Prazo = P00WI7_A1319ContagemResultadoIndicadores_Prazo[0];
               A1324ContagemResultadoIndicadores_Entregue = P00WI7_A1324ContagemResultadoIndicadores_Entregue[0];
               A1321ContagemResultadoIndicadores_Atraso = P00WI7_A1321ContagemResultadoIndicadores_Atraso[0];
               A1322ContagemResultadoIndicadores_Reduz = P00WI7_A1322ContagemResultadoIndicadores_Reduz[0];
               A1323ContagemResultadoIndicadores_Valor = P00WI7_A1323ContagemResultadoIndicadores_Valor[0];
               A484ContagemResultado_StatusDmn = P00WI7_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00WI7_n484ContagemResultado_StatusDmn[0];
               AV41GXLvl238 = 1;
               GXt_char12 = AV36Pgmname + "<< 39.7 >> Acho a Glosa - ContagemResultado_StatusDmn  =  " + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
               new geralog(context ).execute( ref  GXt_char12) ;
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 )
               {
                  A1317ContagemResultadoIndicadores_Data = DateTimeUtil.ServerDate( context, "DEFAULT");
                  A1318ContagemResultadoIndicadores_Solicitada = AV24Solicitada;
                  A1319ContagemResultadoIndicadores_Prazo = AV16Prazo;
                  A1324ContagemResultadoIndicadores_Entregue = AV15Entregue;
                  A1321ContagemResultadoIndicadores_Atraso = AV12Atraso;
                  A1322ContagemResultadoIndicadores_Reduz = AV18Reduz;
                  A1323ContagemResultadoIndicadores_Valor = AV20GlsValor;
                  GXt_char12 = AV36Pgmname + "<< 39.8 >> ATUALIZOU GLOSA";
                  new geralog(context ).execute( ref  GXt_char12) ;
               }
               AV26Achado = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00WI8 */
               pr_default.execute(6, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
               pr_default.close(6);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
               if (true) break;
               /* Using cursor P00WI9 */
               pr_default.execute(7, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
            if ( AV41GXLvl238 == 0 )
            {
               AV26Achado = false;
            }
            GXt_char12 = AV36Pgmname + "<< 39.9 >> VERIFICA SE DEVE INSERIR A GLOSA >> &Achado = " + StringUtil.BoolToStr( AV26Achado) + " << &Indicador = " + context.localUtil.Format( (decimal)(AV23Indicador), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char12) ;
            if ( ! AV26Achado && ( AV23Indicador > 0 ) )
            {
               GXt_char12 = AV36Pgmname + "<< 39.10 >> INSERIR A GLOSA >> &Achado = " + StringUtil.BoolToStr( AV26Achado) + " << &Indicador = " + context.localUtil.Format( (decimal)(AV23Indicador), "ZZZZZ9");
               new geralog(context ).execute( ref  GXt_char12) ;
               /*
                  INSERT RECORD ON TABLE ContagemResultadoIndicadores

               */
               A1314ContagemResultadoIndicadores_DemandaCod = AV28OSCodigo;
               A1315ContagemResultadoIndicadores_IndicadorCod = AV23Indicador;
               A1317ContagemResultadoIndicadores_Data = DateTimeUtil.ServerDate( context, "DEFAULT");
               A1318ContagemResultadoIndicadores_Solicitada = AV24Solicitada;
               A1319ContagemResultadoIndicadores_Prazo = AV16Prazo;
               A1324ContagemResultadoIndicadores_Entregue = AV15Entregue;
               A1321ContagemResultadoIndicadores_Atraso = AV12Atraso;
               A1322ContagemResultadoIndicadores_Reduz = AV18Reduz;
               A1323ContagemResultadoIndicadores_Valor = AV20GlsValor;
               A1316ContagemResultadoIndicadores_LoteCod = 0;
               n1316ContagemResultadoIndicadores_LoteCod = false;
               n1316ContagemResultadoIndicadores_LoteCod = true;
               /* Using cursor P00WI10 */
               pr_default.execute(8, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod, A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod, A1324ContagemResultadoIndicadores_Entregue});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
               if ( (pr_default.getStatus(8) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
      }

      protected void S131( )
      {
         /* 'CALCULAINDP' Routine */
         AV17IndP = 0;
         if ( AV12Atraso < 0 )
         {
            AV12Atraso = 0;
         }
         else
         {
            if ( ( AV16Prazo > 0 ) && ( AV12Atraso > 0 ) )
            {
               AV17IndP = (decimal)((AV12Atraso/ (decimal)(AV16Prazo)));
            }
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_PontualidadeDaOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV36Pgmname = "";
         scmdbuf = "";
         P00WI2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P00WI2_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00WI2_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00WI2_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00WI2_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00WI2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00WI2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         P00WI2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         P00WI2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00WI2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P00WI2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00WI2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P00WI2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         P00WI2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         P00WI2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00WI2_A2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         P00WI2_n2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         P00WI2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00WI2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00WI2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00WI2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         AV30Faixa = 0;
         AV24Solicitada = DateTime.MinValue;
         AV29Inicio = (DateTime)(DateTime.MinValue);
         AV14Previsto = DateTime.MinValue;
         AV15Entregue = DateTime.MinValue;
         AV9DataExtensao = (DateTime)(DateTime.MinValue);
         AV31Indicador_Sigla = "";
         P00WI3_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00WI3_A894LogResponsavel_Acao = new String[] {""} ;
         P00WI3_A1797LogResponsavel_Codigo = new long[1] ;
         P00WI3_A896LogResponsavel_Owner = new int[1] ;
         P00WI3_A892LogResponsavel_DemandaCod = new int[1] ;
         P00WI3_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A894LogResponsavel_Acao = "";
         P00WI4_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00WI4_A2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         P00WI4_n2028ContagemResultadoExecucao_Glosavel = new bool[] {false} ;
         P00WI4_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00WI4_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00WI4_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P00WI4_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P00WI5_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         P00WI5_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         GXt_char11 = "";
         GXt_char10 = "";
         GXt_char8 = "";
         GXt_char7 = "";
         GXt_char6 = "";
         GXt_char5 = "";
         GXt_char4 = "";
         GXt_char3 = "";
         GXt_char2 = "";
         GXt_char1 = "";
         P00WI7_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         P00WI7_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         P00WI7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00WI7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00WI7_A1317ContagemResultadoIndicadores_Data = new DateTime[] {DateTime.MinValue} ;
         P00WI7_A1318ContagemResultadoIndicadores_Solicitada = new DateTime[] {DateTime.MinValue} ;
         P00WI7_A1319ContagemResultadoIndicadores_Prazo = new short[1] ;
         P00WI7_A1324ContagemResultadoIndicadores_Entregue = new DateTime[] {DateTime.MinValue} ;
         P00WI7_A1321ContagemResultadoIndicadores_Atraso = new short[1] ;
         P00WI7_A1322ContagemResultadoIndicadores_Reduz = new decimal[1] ;
         P00WI7_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         A1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         A1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         A1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         GXt_char12 = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_pontualidadedaos__default(),
            new Object[][] {
                new Object[] {
               P00WI2_A1405ContagemResultadoExecucao_Codigo, P00WI2_A471ContagemResultado_DataDmn, P00WI2_n471ContagemResultado_DataDmn, P00WI2_A512ContagemResultado_ValorPF, P00WI2_n512ContagemResultado_ValorPF, P00WI2_A1406ContagemResultadoExecucao_Inicio, P00WI2_A1408ContagemResultadoExecucao_Prevista, P00WI2_n1408ContagemResultadoExecucao_Prevista, P00WI2_A1407ContagemResultadoExecucao_Fim, P00WI2_n1407ContagemResultadoExecucao_Fim,
               P00WI2_A1410ContagemResultadoExecucao_Dias, P00WI2_n1410ContagemResultadoExecucao_Dias, P00WI2_A1411ContagemResultadoExecucao_PrazoDias, P00WI2_n1411ContagemResultadoExecucao_PrazoDias, P00WI2_A1404ContagemResultadoExecucao_OSCod, P00WI2_A2028ContagemResultadoExecucao_Glosavel, P00WI2_n2028ContagemResultadoExecucao_Glosavel, P00WI2_A1553ContagemResultado_CntSrvCod, P00WI2_n1553ContagemResultado_CntSrvCod, P00WI2_A484ContagemResultado_StatusDmn,
               P00WI2_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               P00WI3_A893LogResponsavel_DataHora, P00WI3_A894LogResponsavel_Acao, P00WI3_A1797LogResponsavel_Codigo, P00WI3_A896LogResponsavel_Owner, P00WI3_A892LogResponsavel_DemandaCod, P00WI3_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00WI4_A1404ContagemResultadoExecucao_OSCod, P00WI4_A2028ContagemResultadoExecucao_Glosavel, P00WI4_n2028ContagemResultadoExecucao_Glosavel, P00WI4_A1406ContagemResultadoExecucao_Inicio, P00WI4_A1410ContagemResultadoExecucao_Dias, P00WI4_n1410ContagemResultadoExecucao_Dias, P00WI4_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               P00WI5_A1315ContagemResultadoIndicadores_IndicadorCod, P00WI5_A1314ContagemResultadoIndicadores_DemandaCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00WI7_A1315ContagemResultadoIndicadores_IndicadorCod, P00WI7_A1314ContagemResultadoIndicadores_DemandaCod, P00WI7_A484ContagemResultado_StatusDmn, P00WI7_n484ContagemResultado_StatusDmn, P00WI7_A1317ContagemResultadoIndicadores_Data, P00WI7_A1318ContagemResultadoIndicadores_Solicitada, P00WI7_A1319ContagemResultadoIndicadores_Prazo, P00WI7_A1324ContagemResultadoIndicadores_Entregue, P00WI7_A1321ContagemResultadoIndicadores_Atraso, P00WI7_A1322ContagemResultadoIndicadores_Reduz,
               P00WI7_A1323ContagemResultadoIndicadores_Valor
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         AV36Pgmname = "PRC_PontualidadeDaOS";
         /* GeneXus formulas. */
         AV36Pgmname = "PRC_PontualidadeDaOS";
         context.Gx_err = 0;
      }

      private short A1410ContagemResultadoExecucao_Dias ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short OV30Faixa ;
      private short AV30Faixa ;
      private short AV13DiasExec ;
      private short AV16Prazo ;
      private short AV12Atraso ;
      private short AV10DiasAnt ;
      private short AV41GXLvl238 ;
      private short A1319ContagemResultadoIndicadores_Prazo ;
      private short A1321ContagemResultadoIndicadores_Atraso ;
      private int AV8Codigo ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int AV28OSCodigo ;
      private int AV23Indicador ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int A1314ContagemResultadoIndicadores_DemandaCod ;
      private int GX_INS160 ;
      private int A1316ContagemResultadoIndicadores_LoteCod ;
      private long A1797LogResponsavel_Codigo ;
      private decimal AV27AFaturar ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV22ValorPF ;
      private decimal AV18Reduz ;
      private decimal AV19ReduzRS ;
      private decimal AV21GlsPF ;
      private decimal AV17IndP ;
      private decimal GXt_decimal9 ;
      private decimal AV20GlsValor ;
      private decimal A1322ContagemResultadoIndicadores_Reduz ;
      private decimal A1323ContagemResultadoIndicadores_Valor ;
      private String AV36Pgmname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV31Indicador_Sigla ;
      private String A894LogResponsavel_Acao ;
      private String GXt_char11 ;
      private String GXt_char10 ;
      private String GXt_char8 ;
      private String GXt_char7 ;
      private String GXt_char6 ;
      private String GXt_char5 ;
      private String GXt_char4 ;
      private String GXt_char3 ;
      private String GXt_char2 ;
      private String GXt_char1 ;
      private String GXt_char12 ;
      private String Gx_emsg ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime AV29Inicio ;
      private DateTime AV9DataExtensao ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV24Solicitada ;
      private DateTime AV14Previsto ;
      private DateTime AV15Entregue ;
      private DateTime A1317ContagemResultadoIndicadores_Data ;
      private DateTime A1318ContagemResultadoIndicadores_Solicitada ;
      private DateTime A1324ContagemResultadoIndicadores_Entregue ;
      private bool n471ContagemResultado_DataDmn ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool A2028ContagemResultadoExecucao_Glosavel ;
      private bool n2028ContagemResultadoExecucao_Glosavel ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool returnInSub ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool AV33LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean13 ;
      private bool AV26Achado ;
      private bool n1316ContagemResultadoIndicadores_LoteCod ;
      private String AV11Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00WI2_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] P00WI2_A471ContagemResultado_DataDmn ;
      private bool[] P00WI2_n471ContagemResultado_DataDmn ;
      private decimal[] P00WI2_A512ContagemResultado_ValorPF ;
      private bool[] P00WI2_n512ContagemResultado_ValorPF ;
      private DateTime[] P00WI2_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] P00WI2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] P00WI2_n1408ContagemResultadoExecucao_Prevista ;
      private DateTime[] P00WI2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00WI2_n1407ContagemResultadoExecucao_Fim ;
      private short[] P00WI2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00WI2_n1410ContagemResultadoExecucao_Dias ;
      private short[] P00WI2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] P00WI2_n1411ContagemResultadoExecucao_PrazoDias ;
      private int[] P00WI2_A1404ContagemResultadoExecucao_OSCod ;
      private bool[] P00WI2_A2028ContagemResultadoExecucao_Glosavel ;
      private bool[] P00WI2_n2028ContagemResultadoExecucao_Glosavel ;
      private int[] P00WI2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00WI2_n1553ContagemResultado_CntSrvCod ;
      private String[] P00WI2_A484ContagemResultado_StatusDmn ;
      private bool[] P00WI2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00WI3_A893LogResponsavel_DataHora ;
      private String[] P00WI3_A894LogResponsavel_Acao ;
      private long[] P00WI3_A1797LogResponsavel_Codigo ;
      private int[] P00WI3_A896LogResponsavel_Owner ;
      private int[] P00WI3_A892LogResponsavel_DemandaCod ;
      private bool[] P00WI3_n892LogResponsavel_DemandaCod ;
      private int[] P00WI4_A1404ContagemResultadoExecucao_OSCod ;
      private bool[] P00WI4_A2028ContagemResultadoExecucao_Glosavel ;
      private bool[] P00WI4_n2028ContagemResultadoExecucao_Glosavel ;
      private DateTime[] P00WI4_A1406ContagemResultadoExecucao_Inicio ;
      private short[] P00WI4_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00WI4_n1410ContagemResultadoExecucao_Dias ;
      private int[] P00WI4_A1405ContagemResultadoExecucao_Codigo ;
      private int[] P00WI5_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] P00WI5_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] P00WI7_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] P00WI7_A1314ContagemResultadoIndicadores_DemandaCod ;
      private String[] P00WI7_A484ContagemResultado_StatusDmn ;
      private bool[] P00WI7_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00WI7_A1317ContagemResultadoIndicadores_Data ;
      private DateTime[] P00WI7_A1318ContagemResultadoIndicadores_Solicitada ;
      private short[] P00WI7_A1319ContagemResultadoIndicadores_Prazo ;
      private DateTime[] P00WI7_A1324ContagemResultadoIndicadores_Entregue ;
      private short[] P00WI7_A1321ContagemResultadoIndicadores_Atraso ;
      private decimal[] P00WI7_A1322ContagemResultadoIndicadores_Reduz ;
      private decimal[] P00WI7_A1323ContagemResultadoIndicadores_Valor ;
      private decimal aP2_Reduz ;
      private decimal aP3_GlsValor ;
      private decimal aP4_GlsPF ;
      private String aP5_Descricao ;
   }

   public class prc_pontualidadedaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WI2 ;
          prmP00WI2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI3 ;
          prmP00WI3 = new Object[] {
          new Object[] {"@AV28OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29Inicio",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00WI4 ;
          prmP00WI4 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV29Inicio",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00WI5 ;
          prmP00WI5 = new Object[] {
          new Object[] {"@AV28OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI6 ;
          prmP00WI6 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI7 ;
          prmP00WI7 = new Object[] {
          new Object[] {"@AV28OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI8 ;
          prmP00WI8 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI9 ;
          prmP00WI9 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WI10 ;
          prmP00WI10 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WI2", "SELECT TOP 1 T1.[ContagemResultadoExecucao_Codigo], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_ValorPF], T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_Prevista], T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_PrazoDias], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T1.[ContagemResultadoExecucao_Glosavel], T2.[ContagemResultado_CntSrvCod], T2.[ContagemResultado_StatusDmn] FROM ([ContagemResultadoExecucao] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) WHERE T1.[ContagemResultadoExecucao_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultadoExecucao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WI2,1,0,true,true )
             ,new CursorDef("P00WI3", "SELECT [LogResponsavel_DataHora], [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV28OSCodigo) AND ([LogResponsavel_DataHora] < @AV29Inicio) AND ([LogResponsavel_Acao] = 'R') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WI3,100,0,true,false )
             ,new CursorDef("P00WI4", "SELECT [ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, [ContagemResultadoExecucao_Glosavel], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE ([ContagemResultadoExecucao_OSCod] = @LogResponsavel_DemandaCod and [ContagemResultadoExecucao_Inicio] >= @LogResponsavel_DataHora) AND ([ContagemResultadoExecucao_Glosavel] = 1) AND ([ContagemResultadoExecucao_Inicio] < @AV29Inicio) ORDER BY [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WI4,100,0,true,false )
             ,new CursorDef("P00WI5", "SELECT TOP 1 [ContagemResultadoIndicadores_IndicadorCod], [ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_D FROM [ContagemResultadoIndicadores] WITH (UPDLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @AV28OSCodigo and [ContagemResultadoIndicadores_IndicadorCod] = @AV23Indicador ORDER BY [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WI5,1,0,true,true )
             ,new CursorDef("P00WI6", "DELETE FROM [ContagemResultadoIndicadores]  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WI6)
             ,new CursorDef("P00WI7", "SELECT TOP 1 T1.[ContagemResultadoIndicadores_IndicadorCod], T1.[ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_D, T2.[ContagemResultado_StatusDmn], T1.[ContagemResultadoIndicadores_Data], T1.[ContagemResultadoIndicadores_Solicitada], T1.[ContagemResultadoIndicadores_Prazo], T1.[ContagemResultadoIndicadores_Entregue], T1.[ContagemResultadoIndicadores_Atraso], T1.[ContagemResultadoIndicadores_Reduz], T1.[ContagemResultadoIndicadores_Valor] FROM ([ContagemResultadoIndicadores] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoIndicadores_DemandaCod]) WHERE T1.[ContagemResultadoIndicadores_DemandaCod] = @AV28OSCodigo and T1.[ContagemResultadoIndicadores_IndicadorCod] = @AV23Indicador ORDER BY T1.[ContagemResultadoIndicadores_DemandaCod], T1.[ContagemResultadoIndicadores_IndicadorCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WI7,1,0,true,true )
             ,new CursorDef("P00WI8", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_Data]=@ContagemResultadoIndicadores_Data, [ContagemResultadoIndicadores_Solicitada]=@ContagemResultadoIndicadores_Solicitada, [ContagemResultadoIndicadores_Prazo]=@ContagemResultadoIndicadores_Prazo, [ContagemResultadoIndicadores_Entregue]=@ContagemResultadoIndicadores_Entregue, [ContagemResultadoIndicadores_Atraso]=@ContagemResultadoIndicadores_Atraso, [ContagemResultadoIndicadores_Reduz]=@ContagemResultadoIndicadores_Reduz, [ContagemResultadoIndicadores_Valor]=@ContagemResultadoIndicadores_Valor  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WI8)
             ,new CursorDef("P00WI9", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_Data]=@ContagemResultadoIndicadores_Data, [ContagemResultadoIndicadores_Solicitada]=@ContagemResultadoIndicadores_Solicitada, [ContagemResultadoIndicadores_Prazo]=@ContagemResultadoIndicadores_Prazo, [ContagemResultadoIndicadores_Entregue]=@ContagemResultadoIndicadores_Entregue, [ContagemResultadoIndicadores_Atraso]=@ContagemResultadoIndicadores_Atraso, [ContagemResultadoIndicadores_Reduz]=@ContagemResultadoIndicadores_Reduz, [ContagemResultadoIndicadores_Valor]=@ContagemResultadoIndicadores_Valor  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00WI9)
             ,new CursorDef("P00WI10", "INSERT INTO [ContagemResultadoIndicadores]([ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod], [ContagemResultadoIndicadores_Data], [ContagemResultadoIndicadores_Solicitada], [ContagemResultadoIndicadores_Prazo], [ContagemResultadoIndicadores_Atraso], [ContagemResultadoIndicadores_Reduz], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_LoteCod], [ContagemResultadoIndicadores_Entregue]) VALUES(@ContagemResultadoIndicadores_DemandaCod, @ContagemResultadoIndicadores_IndicadorCod, @ContagemResultadoIndicadores_Data, @ContagemResultadoIndicadores_Solicitada, @ContagemResultadoIndicadores_Prazo, @ContagemResultadoIndicadores_Atraso, @ContagemResultadoIndicadores_Reduz, @ContagemResultadoIndicadores_Valor, @ContagemResultadoIndicadores_LoteCod, @ContagemResultadoIndicadores_Entregue)", GxErrorMask.GX_NOMASK,prmP00WI10)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 7 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (decimal)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[9]);
                }
                stmt.SetParameter(10, (DateTime)parms[10]);
                return;
       }
    }

 }

}
