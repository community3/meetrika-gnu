/*
               File: type_SdtSDT_RedmineIssues
        Description: SDT_RedmineIssues
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:4.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "issues" )]
   [XmlType(TypeName =  "issues" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue ))]
   [Serializable]
   public class SdtSDT_RedmineIssues : GxUserType
   {
      public SdtSDT_RedmineIssues( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineIssues_Type = "";
      }

      public SdtSDT_RedmineIssues( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineIssues deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineIssues)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineIssues obj ;
         obj = this;
         obj.gxTpr_Total_count = deserialized.gxTpr_Total_count;
         obj.gxTpr_Offset = deserialized.gxTpr_Offset;
         obj.gxTpr_Limit = deserialized.gxTpr_Limit;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Issues = deserialized.gxTpr_Issues;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("total_count") == 1 )
         {
            gxTv_SdtSDT_RedmineIssues_Total_count = (short)(NumberUtil.Val( oReader.GetAttributeByName("total_count"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("offset") == 1 )
         {
            gxTv_SdtSDT_RedmineIssues_Offset = (short)(NumberUtil.Val( oReader.GetAttributeByName("offset"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("limit") == 1 )
         {
            gxTv_SdtSDT_RedmineIssues_Limit = (short)(NumberUtil.Val( oReader.GetAttributeByName("limit"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_RedmineIssues_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_RedmineIssues_Issues != null )
            {
               gxTv_SdtSDT_RedmineIssues_Issues.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "issue") == 0 )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_Issues == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_Issues = new GxObjectCollection( context, "SDT_RedmineIssues.issue", "", "SdtSDT_RedmineIssues_issue", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_RedmineIssues_Issues.readxmlcollection(oReader, "", "issue");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "issues";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("total_count", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssues_Total_count), 4, 0)));
         oWriter.WriteAttribute("offset", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssues_Offset), 2, 0)));
         oWriter.WriteAttribute("limit", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssues_Limit), 2, 0)));
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_Type));
         if ( gxTv_SdtSDT_RedmineIssues_Issues != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_Issues.writexmlcollection(oWriter, "", sNameSpace1, "issue", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("total_count", gxTv_SdtSDT_RedmineIssues_Total_count, false);
         AddObjectProperty("offset", gxTv_SdtSDT_RedmineIssues_Offset, false);
         AddObjectProperty("limit", gxTv_SdtSDT_RedmineIssues_Limit, false);
         AddObjectProperty("type", gxTv_SdtSDT_RedmineIssues_Type, false);
         if ( gxTv_SdtSDT_RedmineIssues_Issues != null )
         {
            AddObjectProperty("issues", gxTv_SdtSDT_RedmineIssues_Issues, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "total_count" )]
      [XmlAttribute( AttributeName = "total_count" )]
      public short gxTpr_Total_count
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_Total_count ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_Total_count = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "offset" )]
      [XmlAttribute( AttributeName = "offset" )]
      public short gxTpr_Offset
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_Offset ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_Offset = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "limit" )]
      [XmlAttribute( AttributeName = "limit" )]
      public short gxTpr_Limit
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_Limit ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_Limit = (short)(value);
         }

      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_Type ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_RedmineIssues_Issues_SdtSDT_RedmineIssues_issue_80compatibility:SdtSDT_RedmineIssues_issue {}
      [  SoapElement( ElementName = "issue" )]
      [  XmlElement( ElementName = "issue" , Namespace = "" , Type= typeof( SdtSDT_RedmineIssues_issue ))]
      public GxObjectCollection gxTpr_Issues_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_Issues == null )
            {
               gxTv_SdtSDT_RedmineIssues_Issues = new GxObjectCollection( context, "SDT_RedmineIssues.issue", "", "SdtSDT_RedmineIssues_issue", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RedmineIssues_Issues ;
         }

         set {
            if ( gxTv_SdtSDT_RedmineIssues_Issues == null )
            {
               gxTv_SdtSDT_RedmineIssues_Issues = new GxObjectCollection( context, "SDT_RedmineIssues.issue", "", "SdtSDT_RedmineIssues_issue", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RedmineIssues_Issues = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Issues
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_Issues == null )
            {
               gxTv_SdtSDT_RedmineIssues_Issues = new GxObjectCollection( context, "SDT_RedmineIssues.issue", "", "SdtSDT_RedmineIssues_issue", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RedmineIssues_Issues ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_Issues = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_Issues_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_Issues = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_Issues_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_Issues == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineIssues_Type = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineIssues_Total_count ;
      protected short gxTv_SdtSDT_RedmineIssues_Offset ;
      protected short gxTv_SdtSDT_RedmineIssues_Limit ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineIssues_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineIssues_issue ))]
      protected IGxCollection gxTv_SdtSDT_RedmineIssues_Issues=null ;
   }

   [DataContract(Name = @"SDT_RedmineIssues", Namespace = "")]
   public class SdtSDT_RedmineIssues_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineIssues>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineIssues_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineIssues_RESTInterface( SdtSDT_RedmineIssues psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "total_count" , Order = 0 )]
      public Nullable<short> gxTpr_Total_count
      {
         get {
            return sdt.gxTpr_Total_count ;
         }

         set {
            sdt.gxTpr_Total_count = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "offset" , Order = 1 )]
      public Nullable<short> gxTpr_Offset
      {
         get {
            return sdt.gxTpr_Offset ;
         }

         set {
            sdt.gxTpr_Offset = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "limit" , Order = 2 )]
      public Nullable<short> gxTpr_Limit
      {
         get {
            return sdt.gxTpr_Limit ;
         }

         set {
            sdt.gxTpr_Limit = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "type" , Order = 3 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "issues" , Order = 4 )]
      public GxGenericCollection<SdtSDT_RedmineIssues_issue_RESTInterface> gxTpr_Issues
      {
         get {
            return new GxGenericCollection<SdtSDT_RedmineIssues_issue_RESTInterface>(sdt.gxTpr_Issues) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Issues);
         }

      }

      public SdtSDT_RedmineIssues sdt
      {
         get {
            return (SdtSDT_RedmineIssues)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineIssues() ;
         }
      }

   }

}
