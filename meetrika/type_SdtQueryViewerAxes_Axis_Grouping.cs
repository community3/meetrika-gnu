/*
               File: type_SdtQueryViewerAxes_Axis_Grouping
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:57.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis.Grouping" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis.Grouping" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtQueryViewerAxes_Axis_Grouping : GxUserType
   {
      public SdtQueryViewerAxes_Axis_Grouping( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle = "";
      }

      public SdtQueryViewerAxes_Axis_Grouping( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis_Grouping deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis_Grouping)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis_Grouping obj ;
         obj = this;
         obj.gxTpr_Groupbyyear = deserialized.gxTpr_Groupbyyear;
         obj.gxTpr_Yeartitle = deserialized.gxTpr_Yeartitle;
         obj.gxTpr_Groupbysemester = deserialized.gxTpr_Groupbysemester;
         obj.gxTpr_Semestertitle = deserialized.gxTpr_Semestertitle;
         obj.gxTpr_Groupbyquarter = deserialized.gxTpr_Groupbyquarter;
         obj.gxTpr_Quartertitle = deserialized.gxTpr_Quartertitle;
         obj.gxTpr_Groupbymonth = deserialized.gxTpr_Groupbymonth;
         obj.gxTpr_Monthtitle = deserialized.gxTpr_Monthtitle;
         obj.gxTpr_Groupbydayofweek = deserialized.gxTpr_Groupbydayofweek;
         obj.gxTpr_Dayofweektitle = deserialized.gxTpr_Dayofweektitle;
         obj.gxTpr_Hidevalue = deserialized.gxTpr_Hidevalue;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "GroupByYear") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "YearTitle") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GroupBySemester") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SemesterTitle") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GroupByQuarter") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "QuarterTitle") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GroupByMonth") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MonthTitle") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GroupByDayOfWeek") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DayOfWeekTitle") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HideValue") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis.Grouping";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("GroupByYear", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("YearTitle", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GroupBySemester", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SemesterTitle", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GroupByQuarter", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("QuarterTitle", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GroupByMonth", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("MonthTitle", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GroupByDayOfWeek", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("DayOfWeekTitle", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("HideValue", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("GroupByYear", gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear, false);
         AddObjectProperty("YearTitle", gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle, false);
         AddObjectProperty("GroupBySemester", gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester, false);
         AddObjectProperty("SemesterTitle", gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle, false);
         AddObjectProperty("GroupByQuarter", gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter, false);
         AddObjectProperty("QuarterTitle", gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle, false);
         AddObjectProperty("GroupByMonth", gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth, false);
         AddObjectProperty("MonthTitle", gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle, false);
         AddObjectProperty("GroupByDayOfWeek", gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek, false);
         AddObjectProperty("DayOfWeekTitle", gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle, false);
         AddObjectProperty("HideValue", gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue, false);
         return  ;
      }

      [  SoapElement( ElementName = "GroupByYear" )]
      [  XmlElement( ElementName = "GroupByYear"   )]
      public bool gxTpr_Groupbyyear
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear = value;
         }

      }

      [  SoapElement( ElementName = "YearTitle" )]
      [  XmlElement( ElementName = "YearTitle"   )]
      public String gxTpr_Yeartitle
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle = (String)(value);
         }

      }

      [  SoapElement( ElementName = "GroupBySemester" )]
      [  XmlElement( ElementName = "GroupBySemester"   )]
      public bool gxTpr_Groupbysemester
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester = value;
         }

      }

      [  SoapElement( ElementName = "SemesterTitle" )]
      [  XmlElement( ElementName = "SemesterTitle"   )]
      public String gxTpr_Semestertitle
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle = (String)(value);
         }

      }

      [  SoapElement( ElementName = "GroupByQuarter" )]
      [  XmlElement( ElementName = "GroupByQuarter"   )]
      public bool gxTpr_Groupbyquarter
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter = value;
         }

      }

      [  SoapElement( ElementName = "QuarterTitle" )]
      [  XmlElement( ElementName = "QuarterTitle"   )]
      public String gxTpr_Quartertitle
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle = (String)(value);
         }

      }

      [  SoapElement( ElementName = "GroupByMonth" )]
      [  XmlElement( ElementName = "GroupByMonth"   )]
      public bool gxTpr_Groupbymonth
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth = value;
         }

      }

      [  SoapElement( ElementName = "MonthTitle" )]
      [  XmlElement( ElementName = "MonthTitle"   )]
      public String gxTpr_Monthtitle
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle = (String)(value);
         }

      }

      [  SoapElement( ElementName = "GroupByDayOfWeek" )]
      [  XmlElement( ElementName = "GroupByDayOfWeek"   )]
      public bool gxTpr_Groupbydayofweek
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek = value;
         }

      }

      [  SoapElement( ElementName = "DayOfWeekTitle" )]
      [  XmlElement( ElementName = "DayOfWeekTitle"   )]
      public String gxTpr_Dayofweektitle
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle = (String)(value);
         }

      }

      [  SoapElement( ElementName = "HideValue" )]
      [  XmlElement( ElementName = "HideValue"   )]
      public bool gxTpr_Hidevalue
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle = "";
         gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Grouping_Yeartitle ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Grouping_Semestertitle ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Grouping_Quartertitle ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Grouping_Monthtitle ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Grouping_Dayofweektitle ;
      protected String sTagName ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyyear ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbysemester ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbyquarter ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbymonth ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Groupbydayofweek ;
      protected bool gxTv_SdtQueryViewerAxes_Axis_Grouping_Hidevalue ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis.Grouping", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtQueryViewerAxes_Axis_Grouping_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis_Grouping>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_Grouping_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_Grouping_RESTInterface( SdtQueryViewerAxes_Axis_Grouping psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "GroupByYear" , Order = 0 )]
      public bool gxTpr_Groupbyyear
      {
         get {
            return sdt.gxTpr_Groupbyyear ;
         }

         set {
            sdt.gxTpr_Groupbyyear = value;
         }

      }

      [DataMember( Name = "YearTitle" , Order = 1 )]
      public String gxTpr_Yeartitle
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Yeartitle) ;
         }

         set {
            sdt.gxTpr_Yeartitle = (String)(value);
         }

      }

      [DataMember( Name = "GroupBySemester" , Order = 2 )]
      public bool gxTpr_Groupbysemester
      {
         get {
            return sdt.gxTpr_Groupbysemester ;
         }

         set {
            sdt.gxTpr_Groupbysemester = value;
         }

      }

      [DataMember( Name = "SemesterTitle" , Order = 3 )]
      public String gxTpr_Semestertitle
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Semestertitle) ;
         }

         set {
            sdt.gxTpr_Semestertitle = (String)(value);
         }

      }

      [DataMember( Name = "GroupByQuarter" , Order = 4 )]
      public bool gxTpr_Groupbyquarter
      {
         get {
            return sdt.gxTpr_Groupbyquarter ;
         }

         set {
            sdt.gxTpr_Groupbyquarter = value;
         }

      }

      [DataMember( Name = "QuarterTitle" , Order = 5 )]
      public String gxTpr_Quartertitle
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Quartertitle) ;
         }

         set {
            sdt.gxTpr_Quartertitle = (String)(value);
         }

      }

      [DataMember( Name = "GroupByMonth" , Order = 6 )]
      public bool gxTpr_Groupbymonth
      {
         get {
            return sdt.gxTpr_Groupbymonth ;
         }

         set {
            sdt.gxTpr_Groupbymonth = value;
         }

      }

      [DataMember( Name = "MonthTitle" , Order = 7 )]
      public String gxTpr_Monthtitle
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Monthtitle) ;
         }

         set {
            sdt.gxTpr_Monthtitle = (String)(value);
         }

      }

      [DataMember( Name = "GroupByDayOfWeek" , Order = 8 )]
      public bool gxTpr_Groupbydayofweek
      {
         get {
            return sdt.gxTpr_Groupbydayofweek ;
         }

         set {
            sdt.gxTpr_Groupbydayofweek = value;
         }

      }

      [DataMember( Name = "DayOfWeekTitle" , Order = 9 )]
      public String gxTpr_Dayofweektitle
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Dayofweektitle) ;
         }

         set {
            sdt.gxTpr_Dayofweektitle = (String)(value);
         }

      }

      [DataMember( Name = "HideValue" , Order = 10 )]
      public bool gxTpr_Hidevalue
      {
         get {
            return sdt.gxTpr_Hidevalue ;
         }

         set {
            sdt.gxTpr_Hidevalue = value;
         }

      }

      public SdtQueryViewerAxes_Axis_Grouping sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis_Grouping)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis_Grouping() ;
         }
      }

   }

}
