/*
               File: ContagemResultadoRequisito
        Description: Contagem Resultado Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:16.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadorequisito : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A2003ContagemResultadoRequisito_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A2003ContagemResultadoRequisito_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A2004ContagemResultadoRequisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A2004ContagemResultadoRequisito_ReqCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkContagemResultadoRequisito_Owner.Name = "CONTAGEMRESULTADOREQUISITO_OWNER";
         chkContagemResultadoRequisito_Owner.WebTags = "";
         chkContagemResultadoRequisito_Owner.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoRequisito_Owner_Internalname, "TitleCaption", chkContagemResultadoRequisito_Owner.Caption);
         chkContagemResultadoRequisito_Owner.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Requisito", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadorequisito( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadorequisito( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkContagemResultadoRequisito_Owner = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4Z221( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4Z221e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4Z221( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4Z221( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4Z221e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Requisito", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoRequisito.htm");
            wb_table3_28_4Z221( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4Z221e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4Z221e( true) ;
         }
         else
         {
            wb_table1_2_4Z221e( false) ;
         }
      }

      protected void wb_table3_28_4Z221( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4Z221( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4Z221e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoRequisito.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoRequisito.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4Z221e( true) ;
         }
         else
         {
            wb_table3_28_4Z221e( false) ;
         }
      }

      protected void wb_table4_34_4Z221( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadorequisito_codigo_Internalname, "Resultado Requisito_Codigo", "", "", lblTextblockcontagemresultadorequisito_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoRequisito_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0, ",", "")), ((edtContagemResultadoRequisito_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2005ContagemResultadoRequisito_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2005ContagemResultadoRequisito_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoRequisito_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoRequisito_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadorequisito_oscod_Internalname, "Resultado Requisito_OSCod", "", "", lblTextblockcontagemresultadorequisito_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoRequisito_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")), ((edtContagemResultadoRequisito_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2003ContagemResultadoRequisito_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2003ContagemResultadoRequisito_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoRequisito_OSCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoRequisito_OSCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadorequisito_reqcod_Internalname, "Requisito_Req Cod", "", "", lblTextblockcontagemresultadorequisito_reqcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoRequisito_ReqCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")), ((edtContagemResultadoRequisito_ReqCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2004ContagemResultadoRequisito_ReqCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2004ContagemResultadoRequisito_ReqCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoRequisito_ReqCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoRequisito_ReqCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadorequisito_owner_Internalname, "Resultado Requisito_Owner", "", "", lblTextblockcontagemresultadorequisito_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoRequisito.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContagemResultadoRequisito_Owner_Internalname, StringUtil.BoolToStr( A2006ContagemResultadoRequisito_Owner), "", "", 1, chkContagemResultadoRequisito_Owner.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(54, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4Z221e( true) ;
         }
         else
         {
            wb_table4_34_4Z221e( false) ;
         }
      }

      protected void wb_table2_5_4Z221( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoRequisito.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4Z221e( true) ;
         }
         else
         {
            wb_table2_5_4Z221e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOREQUISITO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2005ContagemResultadoRequisito_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
               }
               else
               {
                  A2005ContagemResultadoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOREQUISITO_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2003ContagemResultadoRequisito_OSCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
               }
               else
               {
                  A2003ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_OSCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_ReqCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_ReqCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOREQUISITO_REQCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoRequisito_ReqCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2004ContagemResultadoRequisito_ReqCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
               }
               else
               {
                  A2004ContagemResultadoRequisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoRequisito_ReqCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
               }
               A2006ContagemResultadoRequisito_Owner = StringUtil.StrToBool( cgiGet( chkContagemResultadoRequisito_Owner_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
               /* Read saved values. */
               Z2005ContagemResultadoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2005ContagemResultadoRequisito_Codigo"), ",", "."));
               Z2006ContagemResultadoRequisito_Owner = StringUtil.StrToBool( cgiGet( "Z2006ContagemResultadoRequisito_Owner"));
               Z2003ContagemResultadoRequisito_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z2003ContagemResultadoRequisito_OSCod"), ",", "."));
               Z2004ContagemResultadoRequisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( "Z2004ContagemResultadoRequisito_ReqCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A2005ContagemResultadoRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4Z221( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes4Z221( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4Z0( )
      {
      }

      protected void ZM4Z221( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2006ContagemResultadoRequisito_Owner = T004Z3_A2006ContagemResultadoRequisito_Owner[0];
               Z2003ContagemResultadoRequisito_OSCod = T004Z3_A2003ContagemResultadoRequisito_OSCod[0];
               Z2004ContagemResultadoRequisito_ReqCod = T004Z3_A2004ContagemResultadoRequisito_ReqCod[0];
            }
            else
            {
               Z2006ContagemResultadoRequisito_Owner = A2006ContagemResultadoRequisito_Owner;
               Z2003ContagemResultadoRequisito_OSCod = A2003ContagemResultadoRequisito_OSCod;
               Z2004ContagemResultadoRequisito_ReqCod = A2004ContagemResultadoRequisito_ReqCod;
            }
         }
         if ( GX_JID == -2 )
         {
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
            Z2006ContagemResultadoRequisito_Owner = A2006ContagemResultadoRequisito_Owner;
            Z2003ContagemResultadoRequisito_OSCod = A2003ContagemResultadoRequisito_OSCod;
            Z2004ContagemResultadoRequisito_ReqCod = A2004ContagemResultadoRequisito_ReqCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2006ContagemResultadoRequisito_Owner) && ( Gx_BScreen == 0 ) )
         {
            A2006ContagemResultadoRequisito_Owner = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load4Z221( )
      {
         /* Using cursor T004Z6 */
         pr_default.execute(4, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound221 = 1;
            A2006ContagemResultadoRequisito_Owner = T004Z6_A2006ContagemResultadoRequisito_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
            A2003ContagemResultadoRequisito_OSCod = T004Z6_A2003ContagemResultadoRequisito_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
            A2004ContagemResultadoRequisito_ReqCod = T004Z6_A2004ContagemResultadoRequisito_ReqCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
            ZM4Z221( -2) ;
         }
         pr_default.close(4);
         OnLoadActions4Z221( ) ;
      }

      protected void OnLoadActions4Z221( )
      {
      }

      protected void CheckExtendedTable4Z221( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T004Z4 */
         pr_default.execute(2, new Object[] {A2003ContagemResultadoRequisito_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T004Z5 */
         pr_default.execute(3, new Object[] {A2004ContagemResultadoRequisito_ReqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_Req Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_REQCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_ReqCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4Z221( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A2003ContagemResultadoRequisito_OSCod )
      {
         /* Using cursor T004Z7 */
         pr_default.execute(5, new Object[] {A2003ContagemResultadoRequisito_OSCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_4( int A2004ContagemResultadoRequisito_ReqCod )
      {
         /* Using cursor T004Z8 */
         pr_default.execute(6, new Object[] {A2004ContagemResultadoRequisito_ReqCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_Req Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_REQCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_ReqCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey4Z221( )
      {
         /* Using cursor T004Z9 */
         pr_default.execute(7, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound221 = 1;
         }
         else
         {
            RcdFound221 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004Z3 */
         pr_default.execute(1, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4Z221( 2) ;
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = T004Z3_A2005ContagemResultadoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
            A2006ContagemResultadoRequisito_Owner = T004Z3_A2006ContagemResultadoRequisito_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
            A2003ContagemResultadoRequisito_OSCod = T004Z3_A2003ContagemResultadoRequisito_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
            A2004ContagemResultadoRequisito_ReqCod = T004Z3_A2004ContagemResultadoRequisito_ReqCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
            sMode221 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4Z221( ) ;
            if ( AnyError == 1 )
            {
               RcdFound221 = 0;
               InitializeNonKey4Z221( ) ;
            }
            Gx_mode = sMode221;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound221 = 0;
            InitializeNonKey4Z221( ) ;
            sMode221 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode221;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound221 = 0;
         /* Using cursor T004Z10 */
         pr_default.execute(8, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004Z10_A2005ContagemResultadoRequisito_Codigo[0] < A2005ContagemResultadoRequisito_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004Z10_A2005ContagemResultadoRequisito_Codigo[0] > A2005ContagemResultadoRequisito_Codigo ) ) )
            {
               A2005ContagemResultadoRequisito_Codigo = T004Z10_A2005ContagemResultadoRequisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
               RcdFound221 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound221 = 0;
         /* Using cursor T004Z11 */
         pr_default.execute(9, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004Z11_A2005ContagemResultadoRequisito_Codigo[0] > A2005ContagemResultadoRequisito_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004Z11_A2005ContagemResultadoRequisito_Codigo[0] < A2005ContagemResultadoRequisito_Codigo ) ) )
            {
               A2005ContagemResultadoRequisito_Codigo = T004Z11_A2005ContagemResultadoRequisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
               RcdFound221 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4Z221( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4Z221( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound221 == 1 )
            {
               if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
               {
                  A2005ContagemResultadoRequisito_Codigo = Z2005ContagemResultadoRequisito_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4Z221( ) ;
                  GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4Z221( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOREQUISITO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4Z221( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
         {
            A2005ContagemResultadoRequisito_Codigo = Z2005ContagemResultadoRequisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOREQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4Z221( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound221 != 0 )
            {
               ScanNext4Z221( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4Z221( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4Z221( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004Z2 */
            pr_default.execute(0, new Object[] {A2005ContagemResultadoRequisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoRequisito"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2006ContagemResultadoRequisito_Owner != T004Z2_A2006ContagemResultadoRequisito_Owner[0] ) || ( Z2003ContagemResultadoRequisito_OSCod != T004Z2_A2003ContagemResultadoRequisito_OSCod[0] ) || ( Z2004ContagemResultadoRequisito_ReqCod != T004Z2_A2004ContagemResultadoRequisito_ReqCod[0] ) )
            {
               if ( Z2006ContagemResultadoRequisito_Owner != T004Z2_A2006ContagemResultadoRequisito_Owner[0] )
               {
                  GXUtil.WriteLog("contagemresultadorequisito:[seudo value changed for attri]"+"ContagemResultadoRequisito_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z2006ContagemResultadoRequisito_Owner);
                  GXUtil.WriteLogRaw("Current: ",T004Z2_A2006ContagemResultadoRequisito_Owner[0]);
               }
               if ( Z2003ContagemResultadoRequisito_OSCod != T004Z2_A2003ContagemResultadoRequisito_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadorequisito:[seudo value changed for attri]"+"ContagemResultadoRequisito_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z2003ContagemResultadoRequisito_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T004Z2_A2003ContagemResultadoRequisito_OSCod[0]);
               }
               if ( Z2004ContagemResultadoRequisito_ReqCod != T004Z2_A2004ContagemResultadoRequisito_ReqCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadorequisito:[seudo value changed for attri]"+"ContagemResultadoRequisito_ReqCod");
                  GXUtil.WriteLogRaw("Old: ",Z2004ContagemResultadoRequisito_ReqCod);
                  GXUtil.WriteLogRaw("Current: ",T004Z2_A2004ContagemResultadoRequisito_ReqCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoRequisito"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4Z221( )
      {
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4Z221( 0) ;
            CheckOptimisticConcurrency4Z221( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Z221( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4Z221( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Z12 */
                     pr_default.execute(10, new Object[] {A2006ContagemResultadoRequisito_Owner, A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod});
                     A2005ContagemResultadoRequisito_Codigo = T004Z12_A2005ContagemResultadoRequisito_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4Z0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4Z221( ) ;
            }
            EndLevel4Z221( ) ;
         }
         CloseExtendedTableCursors4Z221( ) ;
      }

      protected void Update4Z221( )
      {
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Z221( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Z221( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4Z221( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Z13 */
                     pr_default.execute(11, new Object[] {A2006ContagemResultadoRequisito_Owner, A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod, A2005ContagemResultadoRequisito_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoRequisito"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4Z221( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4Z0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4Z221( ) ;
         }
         CloseExtendedTableCursors4Z221( ) ;
      }

      protected void DeferredUpdate4Z221( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4Z221( ) ;
            AfterConfirm4Z221( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4Z221( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004Z14 */
                  pr_default.execute(12, new Object[] {A2005ContagemResultadoRequisito_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound221 == 0 )
                        {
                           InitAll4Z221( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4Z0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode221 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4Z221( ) ;
         Gx_mode = sMode221;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4Z221( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4Z221( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContagemResultadoRequisito");
            if ( AnyError == 0 )
            {
               ConfirmValues4Z0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContagemResultadoRequisito");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4Z221( )
      {
         /* Using cursor T004Z15 */
         pr_default.execute(13);
         RcdFound221 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = T004Z15_A2005ContagemResultadoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4Z221( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound221 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = T004Z15_A2005ContagemResultadoRequisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4Z221( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm4Z221( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4Z221( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4Z221( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4Z221( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4Z221( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4Z221( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4Z221( )
      {
         edtContagemResultadoRequisito_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoRequisito_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_Codigo_Enabled), 5, 0)));
         edtContagemResultadoRequisito_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoRequisito_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_OSCod_Enabled), 5, 0)));
         edtContagemResultadoRequisito_ReqCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoRequisito_ReqCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoRequisito_ReqCod_Enabled), 5, 0)));
         chkContagemResultadoRequisito_Owner.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoRequisito_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContagemResultadoRequisito_Owner.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4Z0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221121725");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadorequisito.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2005ContagemResultadoRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2006ContagemResultadoRequisito_Owner", Z2006ContagemResultadoRequisito_Owner);
         GxWebStd.gx_hidden_field( context, "Z2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadorequisito.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoRequisito" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Requisito" ;
      }

      protected void InitializeNonKey4Z221( )
      {
         A2003ContagemResultadoRequisito_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
         A2004ContagemResultadoRequisito_ReqCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
         A2006ContagemResultadoRequisito_Owner = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
         Z2006ContagemResultadoRequisito_Owner = false;
         Z2003ContagemResultadoRequisito_OSCod = 0;
         Z2004ContagemResultadoRequisito_ReqCod = 0;
      }

      protected void InitAll4Z221( )
      {
         A2005ContagemResultadoRequisito_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2005ContagemResultadoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2005ContagemResultadoRequisito_Codigo), 6, 0)));
         InitializeNonKey4Z221( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2006ContagemResultadoRequisito_Owner = i2006ContagemResultadoRequisito_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2006ContagemResultadoRequisito_Owner", A2006ContagemResultadoRequisito_Owner);
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221121730");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadorequisito.js", "?202031221121730");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadorequisito_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOREQUISITO_CODIGO";
         edtContagemResultadoRequisito_Codigo_Internalname = "CONTAGEMRESULTADOREQUISITO_CODIGO";
         lblTextblockcontagemresultadorequisito_oscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOREQUISITO_OSCOD";
         edtContagemResultadoRequisito_OSCod_Internalname = "CONTAGEMRESULTADOREQUISITO_OSCOD";
         lblTextblockcontagemresultadorequisito_reqcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOREQUISITO_REQCOD";
         edtContagemResultadoRequisito_ReqCod_Internalname = "CONTAGEMRESULTADOREQUISITO_REQCOD";
         lblTextblockcontagemresultadorequisito_owner_Internalname = "TEXTBLOCKCONTAGEMRESULTADOREQUISITO_OWNER";
         chkContagemResultadoRequisito_Owner_Internalname = "CONTAGEMRESULTADOREQUISITO_OWNER";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Requisito";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         chkContagemResultadoRequisito_Owner.Enabled = 1;
         edtContagemResultadoRequisito_ReqCod_Jsonclick = "";
         edtContagemResultadoRequisito_ReqCod_Enabled = 1;
         edtContagemResultadoRequisito_OSCod_Jsonclick = "";
         edtContagemResultadoRequisito_OSCod_Enabled = 1;
         edtContagemResultadoRequisito_Codigo_Jsonclick = "";
         edtContagemResultadoRequisito_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkContagemResultadoRequisito_Owner.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadorequisito_codigo( int GX_Parm1 ,
                                                           bool GX_Parm2 ,
                                                           int GX_Parm3 ,
                                                           int GX_Parm4 )
      {
         A2005ContagemResultadoRequisito_Codigo = GX_Parm1;
         A2006ContagemResultadoRequisito_Owner = GX_Parm2;
         A2003ContagemResultadoRequisito_OSCod = GX_Parm3;
         A2004ContagemResultadoRequisito_ReqCod = GX_Parm4;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ".", "")));
         isValidOutput.Add(A2006ContagemResultadoRequisito_Owner);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2005ContagemResultadoRequisito_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")));
         isValidOutput.Add(Z2006ContagemResultadoRequisito_Owner);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadorequisito_oscod( int GX_Parm1 )
      {
         A2003ContagemResultadoRequisito_OSCod = GX_Parm1;
         /* Using cursor T004Z16 */
         pr_default.execute(14, new Object[] {A2003ContagemResultadoRequisito_OSCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_OSCod_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadorequisito_reqcod( int GX_Parm1 )
      {
         A2004ContagemResultadoRequisito_ReqCod = GX_Parm1;
         /* Using cursor T004Z17 */
         pr_default.execute(15, new Object[] {A2004ContagemResultadoRequisito_ReqCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_Req Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_REQCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoRequisito_ReqCod_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadorequisito_codigo_Jsonclick = "";
         lblTextblockcontagemresultadorequisito_oscod_Jsonclick = "";
         lblTextblockcontagemresultadorequisito_reqcod_Jsonclick = "";
         lblTextblockcontagemresultadorequisito_owner_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T004Z6_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z6_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         T004Z6_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z6_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         T004Z4_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z5_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         T004Z7_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z8_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         T004Z9_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z3_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z3_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         T004Z3_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z3_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         sMode221 = "";
         T004Z10_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z11_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z2_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z2_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         T004Z2_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z2_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         T004Z12_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004Z15_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T004Z16_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         T004Z17_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadorequisito__default(),
            new Object[][] {
                new Object[] {
               T004Z2_A2005ContagemResultadoRequisito_Codigo, T004Z2_A2006ContagemResultadoRequisito_Owner, T004Z2_A2003ContagemResultadoRequisito_OSCod, T004Z2_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               T004Z3_A2005ContagemResultadoRequisito_Codigo, T004Z3_A2006ContagemResultadoRequisito_Owner, T004Z3_A2003ContagemResultadoRequisito_OSCod, T004Z3_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               T004Z4_A2003ContagemResultadoRequisito_OSCod
               }
               , new Object[] {
               T004Z5_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               T004Z6_A2005ContagemResultadoRequisito_Codigo, T004Z6_A2006ContagemResultadoRequisito_Owner, T004Z6_A2003ContagemResultadoRequisito_OSCod, T004Z6_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               T004Z7_A2003ContagemResultadoRequisito_OSCod
               }
               , new Object[] {
               T004Z8_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               T004Z9_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004Z10_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004Z11_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004Z12_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004Z15_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004Z16_A2003ContagemResultadoRequisito_OSCod
               }
               , new Object[] {
               T004Z17_A2004ContagemResultadoRequisito_ReqCod
               }
            }
         );
         Z2006ContagemResultadoRequisito_Owner = false;
         A2006ContagemResultadoRequisito_Owner = false;
         i2006ContagemResultadoRequisito_Owner = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound221 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z2005ContagemResultadoRequisito_Codigo ;
      private int Z2003ContagemResultadoRequisito_OSCod ;
      private int Z2004ContagemResultadoRequisito_ReqCod ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private int edtContagemResultadoRequisito_Codigo_Enabled ;
      private int edtContagemResultadoRequisito_OSCod_Enabled ;
      private int edtContagemResultadoRequisito_ReqCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkContagemResultadoRequisito_Owner_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoRequisito_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadorequisito_codigo_Internalname ;
      private String lblTextblockcontagemresultadorequisito_codigo_Jsonclick ;
      private String edtContagemResultadoRequisito_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadorequisito_oscod_Internalname ;
      private String lblTextblockcontagemresultadorequisito_oscod_Jsonclick ;
      private String edtContagemResultadoRequisito_OSCod_Internalname ;
      private String edtContagemResultadoRequisito_OSCod_Jsonclick ;
      private String lblTextblockcontagemresultadorequisito_reqcod_Internalname ;
      private String lblTextblockcontagemresultadorequisito_reqcod_Jsonclick ;
      private String edtContagemResultadoRequisito_ReqCod_Internalname ;
      private String edtContagemResultadoRequisito_ReqCod_Jsonclick ;
      private String lblTextblockcontagemresultadorequisito_owner_Internalname ;
      private String lblTextblockcontagemresultadorequisito_owner_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode221 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool Z2006ContagemResultadoRequisito_Owner ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private bool i2006ContagemResultadoRequisito_Owner ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkContagemResultadoRequisito_Owner ;
      private IDataStoreProvider pr_default ;
      private int[] T004Z6_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] T004Z6_A2006ContagemResultadoRequisito_Owner ;
      private int[] T004Z6_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z6_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] T004Z4_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z5_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] T004Z7_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z8_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] T004Z9_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004Z3_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] T004Z3_A2006ContagemResultadoRequisito_Owner ;
      private int[] T004Z3_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z3_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] T004Z10_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004Z11_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004Z2_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] T004Z2_A2006ContagemResultadoRequisito_Owner ;
      private int[] T004Z2_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z2_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] T004Z12_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004Z15_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004Z16_A2003ContagemResultadoRequisito_OSCod ;
      private int[] T004Z17_A2004ContagemResultadoRequisito_ReqCod ;
      private GXWebForm Form ;
   }

   public class contagemresultadorequisito__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004Z6 ;
          prmT004Z6 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z4 ;
          prmT004Z4 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z5 ;
          prmT004Z5 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z7 ;
          prmT004Z7 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z8 ;
          prmT004Z8 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z9 ;
          prmT004Z9 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z3 ;
          prmT004Z3 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z10 ;
          prmT004Z10 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z11 ;
          prmT004Z11 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z2 ;
          prmT004Z2 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z12 ;
          prmT004Z12 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z13 ;
          prmT004Z13 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z14 ;
          prmT004Z14 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z15 ;
          prmT004Z15 = new Object[] {
          } ;
          Object[] prmT004Z16 ;
          prmT004Z16 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Z17 ;
          prmT004Z17 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004Z2", "SELECT [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] WITH (UPDLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z2,1,0,true,false )
             ,new CursorDef("T004Z3", "SELECT [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z3,1,0,true,false )
             ,new CursorDef("T004Z4", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoRequisito_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoRequisito_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z4,1,0,true,false )
             ,new CursorDef("T004Z5", "SELECT [Requisito_Codigo] AS ContagemResultadoRequisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z5,1,0,true,false )
             ,new CursorDef("T004Z6", "SELECT TM1.[ContagemResultadoRequisito_Codigo], TM1.[ContagemResultadoRequisito_Owner], TM1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, TM1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ORDER BY TM1.[ContagemResultadoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z6,100,0,true,false )
             ,new CursorDef("T004Z7", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoRequisito_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoRequisito_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z7,1,0,true,false )
             ,new CursorDef("T004Z8", "SELECT [Requisito_Codigo] AS ContagemResultadoRequisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z8,1,0,true,false )
             ,new CursorDef("T004Z9", "SELECT [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z9,1,0,true,false )
             ,new CursorDef("T004Z10", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE ( [ContagemResultadoRequisito_Codigo] > @ContagemResultadoRequisito_Codigo) ORDER BY [ContagemResultadoRequisito_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z10,1,0,true,true )
             ,new CursorDef("T004Z11", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE ( [ContagemResultadoRequisito_Codigo] < @ContagemResultadoRequisito_Codigo) ORDER BY [ContagemResultadoRequisito_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z11,1,0,true,true )
             ,new CursorDef("T004Z12", "INSERT INTO [ContagemResultadoRequisito]([ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod]) VALUES(@ContagemResultadoRequisito_Owner, @ContagemResultadoRequisito_OSCod, @ContagemResultadoRequisito_ReqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004Z12)
             ,new CursorDef("T004Z13", "UPDATE [ContagemResultadoRequisito] SET [ContagemResultadoRequisito_Owner]=@ContagemResultadoRequisito_Owner, [ContagemResultadoRequisito_OSCod]=@ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod]=@ContagemResultadoRequisito_ReqCod  WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmT004Z13)
             ,new CursorDef("T004Z14", "DELETE FROM [ContagemResultadoRequisito]  WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmT004Z14)
             ,new CursorDef("T004Z15", "SELECT [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) ORDER BY [ContagemResultadoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z15,100,0,true,false )
             ,new CursorDef("T004Z16", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoRequisito_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoRequisito_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z16,1,0,true,false )
             ,new CursorDef("T004Z17", "SELECT [Requisito_Codigo] AS ContagemResultadoRequisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Z17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
