/*
               File: ModuloModuloFuncoesWC
        Description: Modulo Modulo Funcoes WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:24.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulomodulofuncoeswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public modulomodulofuncoeswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public modulomodulofuncoeswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Modulo_Codigo )
      {
         this.AV7Modulo_Codigo = aP0_Modulo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavFuncaousuario_nomeoperator = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Modulo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_33 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_33_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_33_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV52FuncaoUsuario_NomeOperator = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
                  AV53FuncaoUsuario_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV57TFFuncaoUsuario_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
                  AV58TFFuncaoUsuario_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoUsuario_Nome_Sel", AV58TFFuncaoUsuario_Nome_Sel);
                  AV7Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
                  AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
                  AV67Pgmname = GetNextPar( );
                  A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA3P2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV67Pgmname = "ModuloModuloFuncoesWC";
               context.Gx_err = 0;
               WS3P2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Modulo Modulo Funcoes WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311772457");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("modulomodulofuncoeswc.aspx") + "?" + UrlEncode("" +AV7Modulo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOUSUARIO_NOMEOPERATOR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOUSUARIO_NOME", AV53FuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME", AV57TFFuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME_SEL", AV58TFFuncaoUsuario_Nome_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_33", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_33), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOUSUARIO_NOMETITLEFILTERDATA", AV56FuncaoUsuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOUSUARIO_NOMETITLEFILTERDATA", AV56FuncaoUsuario_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Modulo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV67Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Caption", StringUtil.RTrim( Ddo_funcaousuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaousuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Cls", StringUtil.RTrim( Ddo_funcaousuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaousuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaousuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaousuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaousuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaousuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaousuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaousuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaousuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaousuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaousuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaousuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaousuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaousuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaousuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOUSUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaousuario_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm3P2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("modulomodulofuncoeswc.js", "?2020311772519");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ModuloModuloFuncoesWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Modulo Modulo Funcoes WC" ;
      }

      protected void WB3P0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "modulomodulofuncoeswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_3P2( true) ;
         }
         else
         {
            wb_table1_2_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtModulo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A146Modulo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtModulo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtModulo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ModuloModuloFuncoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ModuloModuloFuncoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_nome_Internalname, AV57TFFuncaoUsuario_Nome, StringUtil.RTrim( context.localUtil.Format( AV57TFFuncaoUsuario_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloModuloFuncoesWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_nome_sel_Internalname, AV58TFFuncaoUsuario_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV58TFFuncaoUsuario_Nome_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloModuloFuncoesWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOUSUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", 0, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ModuloModuloFuncoesWC.htm");
         }
         wbLoad = true;
      }

      protected void START3P2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Modulo Modulo Funcoes WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP3P0( ) ;
            }
         }
      }

      protected void WS3P2( )
      {
         START3P2( ) ;
         EVT3P2( ) ;
      }

      protected void EVT3P2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E113P2 */
                                    E113P2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOUSUARIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E123P2 */
                                    E123P2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E133P2 */
                                    E133P2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3P0( ) ;
                              }
                              nGXsfl_33_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
                              SubsflControlProps_332( ) ;
                              A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
                              A162FuncaoUsuario_Nome = cgiGet( edtFuncaoUsuario_Nome_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E143P2 */
                                          E143P2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E153P2 */
                                          E153P2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E163P2 */
                                          E163P2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Funcaousuario_nomeoperator Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOUSUARIO_NOMEOPERATOR"), ",", ".") != Convert.ToDecimal( AV52FuncaoUsuario_NomeOperator )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaousuario_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOUSUARIO_NOME"), AV53FuncaoUsuario_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaousuario_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME"), AV57TFFuncaoUsuario_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaousuario_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME_SEL"), AV58TFFuncaoUsuario_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP3P0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3P2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3P2( ) ;
            }
         }
      }

      protected void PA3P2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavFuncaousuario_nomeoperator.Name = "vFUNCAOUSUARIO_NOMEOPERATOR";
            cmbavFuncaousuario_nomeoperator.WebTags = "";
            cmbavFuncaousuario_nomeoperator.addItem("0", "Come�a com", 0);
            cmbavFuncaousuario_nomeoperator.addItem("1", "Cont�m", 0);
            if ( cmbavFuncaousuario_nomeoperator.ItemCount > 0 )
            {
               AV52FuncaoUsuario_NomeOperator = (short)(NumberUtil.Val( cmbavFuncaousuario_nomeoperator.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavFuncaousuario_nomeoperator_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_332( ) ;
         while ( nGXsfl_33_idx <= nRC_GXsfl_33 )
         {
            sendrow_332( ) ;
            nGXsfl_33_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_33_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV52FuncaoUsuario_NomeOperator ,
                                       String AV53FuncaoUsuario_Nome ,
                                       bool AV15OrderedDsc ,
                                       String AV57TFFuncaoUsuario_Nome ,
                                       String AV58TFFuncaoUsuario_Nome_Sel ,
                                       int AV7Modulo_Codigo ,
                                       String AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace ,
                                       String AV67Pgmname ,
                                       int A161FuncaoUsuario_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3P2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOUSUARIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavFuncaousuario_nomeoperator.ItemCount > 0 )
         {
            AV52FuncaoUsuario_NomeOperator = (short)(NumberUtil.Val( cmbavFuncaousuario_nomeoperator.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3P2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV67Pgmname = "ModuloModuloFuncoesWC";
         context.Gx_err = 0;
      }

      protected void RF3P2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 33;
         /* Execute user event: E153P2 */
         E153P2 ();
         nGXsfl_33_idx = 1;
         sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
         SubsflControlProps_332( ) ;
         nGXsfl_33_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_332( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV52FuncaoUsuario_NomeOperator ,
                                                 AV53FuncaoUsuario_Nome ,
                                                 AV58TFFuncaoUsuario_Nome_Sel ,
                                                 AV57TFFuncaoUsuario_Nome ,
                                                 A162FuncaoUsuario_Nome ,
                                                 AV15OrderedDsc ,
                                                 A146Modulo_Codigo ,
                                                 AV7Modulo_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV53FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV53FuncaoUsuario_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
            lV53FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV53FuncaoUsuario_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
            lV57TFFuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV57TFFuncaoUsuario_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
            /* Using cursor H003P2 */
            pr_default.execute(0, new Object[] {AV7Modulo_Codigo, lV53FuncaoUsuario_Nome, lV53FuncaoUsuario_Nome, lV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_33_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A146Modulo_Codigo = H003P2_A146Modulo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               A162FuncaoUsuario_Nome = H003P2_A162FuncaoUsuario_Nome[0];
               A161FuncaoUsuario_Codigo = H003P2_A161FuncaoUsuario_Codigo[0];
               A162FuncaoUsuario_Nome = H003P2_A162FuncaoUsuario_Nome[0];
               /* Execute user event: E163P2 */
               E163P2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 33;
            WB3P0( ) ;
         }
         nGXsfl_33_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52FuncaoUsuario_NomeOperator ,
                                              AV53FuncaoUsuario_Nome ,
                                              AV58TFFuncaoUsuario_Nome_Sel ,
                                              AV57TFFuncaoUsuario_Nome ,
                                              A162FuncaoUsuario_Nome ,
                                              AV15OrderedDsc ,
                                              A146Modulo_Codigo ,
                                              AV7Modulo_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV53FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV53FuncaoUsuario_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
         lV53FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV53FuncaoUsuario_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
         lV57TFFuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV57TFFuncaoUsuario_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
         /* Using cursor H003P3 */
         pr_default.execute(1, new Object[] {AV7Modulo_Codigo, lV53FuncaoUsuario_Nome, lV53FuncaoUsuario_Nome, lV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel});
         GRID_nRecordCount = H003P3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV52FuncaoUsuario_NomeOperator, AV53FuncaoUsuario_Nome, AV15OrderedDsc, AV57TFFuncaoUsuario_Nome, AV58TFFuncaoUsuario_Nome_Sel, AV7Modulo_Codigo, AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV67Pgmname, A161FuncaoUsuario_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3P0( )
      {
         /* Before Start, stand alone formulas. */
         AV67Pgmname = "ModuloModuloFuncoesWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E143P2 */
         E143P2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV60DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOUSUARIO_NOMETITLEFILTERDATA"), AV56FuncaoUsuario_NomeTitleFilterData);
            /* Read variables values. */
            cmbavFuncaousuario_nomeoperator.Name = cmbavFuncaousuario_nomeoperator_Internalname;
            cmbavFuncaousuario_nomeoperator.CurrentValue = cgiGet( cmbavFuncaousuario_nomeoperator_Internalname);
            AV52FuncaoUsuario_NomeOperator = (short)(NumberUtil.Val( cgiGet( cmbavFuncaousuario_nomeoperator_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
            AV53FuncaoUsuario_Nome = cgiGet( edtavFuncaousuario_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
            A146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtModulo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV57TFFuncaoUsuario_Nome = cgiGet( edtavTffuncaousuario_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
            AV58TFFuncaoUsuario_Nome_Sel = cgiGet( edtavTffuncaousuario_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoUsuario_Nome_Sel", AV58TFFuncaoUsuario_Nome_Sel);
            AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_33 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_33"), ",", "."));
            AV62GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV63GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Modulo_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaousuario_nome_Caption = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Caption");
            Ddo_funcaousuario_nome_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Tooltip");
            Ddo_funcaousuario_nome_Cls = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Cls");
            Ddo_funcaousuario_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filteredtext_set");
            Ddo_funcaousuario_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Selectedvalue_set");
            Ddo_funcaousuario_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Dropdownoptionstype");
            Ddo_funcaousuario_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_funcaousuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includesortasc"));
            Ddo_funcaousuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includesortdsc"));
            Ddo_funcaousuario_nome_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortedstatus");
            Ddo_funcaousuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includefilter"));
            Ddo_funcaousuario_nome_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filtertype");
            Ddo_funcaousuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filterisrange"));
            Ddo_funcaousuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Includedatalist"));
            Ddo_funcaousuario_nome_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalisttype");
            Ddo_funcaousuario_nome_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalistproc");
            Ddo_funcaousuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaousuario_nome_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortasc");
            Ddo_funcaousuario_nome_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Sortdsc");
            Ddo_funcaousuario_nome_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Loadingdata");
            Ddo_funcaousuario_nome_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Cleanfilter");
            Ddo_funcaousuario_nome_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Noresultsfound");
            Ddo_funcaousuario_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaousuario_nome_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Activeeventkey");
            Ddo_funcaousuario_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Filteredtext_get");
            Ddo_funcaousuario_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOUSUARIO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOUSUARIO_NOMEOPERATOR"), ",", ".") != Convert.ToDecimal( AV52FuncaoUsuario_NomeOperator )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOUSUARIO_NOME"), AV53FuncaoUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME"), AV57TFFuncaoUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOUSUARIO_NOME_SEL"), AV58TFFuncaoUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E143P2 */
         E143P2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E143P2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTffuncaousuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaousuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_nome_Visible), 5, 0)));
         edtavTffuncaousuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaousuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_nome_sel_Visible), 5, 0)));
         Ddo_funcaousuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoUsuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaousuario_nome_Titlecontrolidtoreplace);
         AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace = Ddo_funcaousuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         edtModulo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtModulo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV60DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV60DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E153P2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV56FuncaoUsuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoUsuario_Nome_Titleformat = 2;
         edtFuncaoUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fun��o de Usu�rio", AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoUsuario_Nome_Internalname, "Title", edtFuncaoUsuario_Nome_Title);
         AV62GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV62GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridCurrentPage), 10, 0)));
         AV63GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV56FuncaoUsuario_NomeTitleFilterData", AV56FuncaoUsuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E113P2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV61PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV61PageToGo) ;
         }
      }

      protected void E123P2( )
      {
         /* Ddo_funcaousuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaousuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaousuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFFuncaoUsuario_Nome = Ddo_funcaousuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
            AV58TFFuncaoUsuario_Nome_Sel = Ddo_funcaousuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoUsuario_Nome_Sel", AV58TFFuncaoUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E163P2( )
      {
         /* Grid_Load Routine */
         edtFuncaoUsuario_Nome_Link = formatLink("viewfuncaousuario.aspx") + "?" + UrlEncode("" +A161FuncaoUsuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 33;
         }
         sendrow_332( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_33_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(33, GridRow);
         }
      }

      protected void E133P2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavFuncaousuario_nomeoperator.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaousuario_nomeoperator_Internalname, "Values", cmbavFuncaousuario_nomeoperator.ToJavascriptSource());
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_funcaousuario_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'CLEANFILTERS' Routine */
         AV52FuncaoUsuario_NomeOperator = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
         AV53FuncaoUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
         AV57TFFuncaoUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
         Ddo_funcaousuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "FilteredText_set", Ddo_funcaousuario_nome_Filteredtext_set);
         AV58TFFuncaoUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoUsuario_Nome_Sel", AV58TFFuncaoUsuario_Nome_Sel);
         Ddo_funcaousuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "SelectedValue_set", Ddo_funcaousuario_nome_Selectedvalue_set);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV67Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV67Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV31Session.Get(AV67Pgmname+"GridState"), "");
         }
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV68GXV1 = 1;
         while ( AV68GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV68GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "FUNCAOUSUARIO_NOME") == 0 )
            {
               AV53FuncaoUsuario_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53FuncaoUsuario_Nome", AV53FuncaoUsuario_Nome);
               AV52FuncaoUsuario_NomeOperator = AV12GridStateFilterValue.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52FuncaoUsuario_NomeOperator", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)));
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME") == 0 )
            {
               AV57TFFuncaoUsuario_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57TFFuncaoUsuario_Nome", AV57TFFuncaoUsuario_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFFuncaoUsuario_Nome)) )
               {
                  Ddo_funcaousuario_nome_Filteredtext_set = AV57TFFuncaoUsuario_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "FilteredText_set", Ddo_funcaousuario_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME_SEL") == 0 )
            {
               AV58TFFuncaoUsuario_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV58TFFuncaoUsuario_Nome_Sel", AV58TFFuncaoUsuario_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) )
               {
                  Ddo_funcaousuario_nome_Selectedvalue_set = AV58TFFuncaoUsuario_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaousuario_nome_Internalname, "SelectedValue_set", Ddo_funcaousuario_nome_Selectedvalue_set);
               }
            }
            AV68GXV1 = (int)(AV68GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV31Session.Get(AV67Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53FuncaoUsuario_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "FUNCAOUSUARIO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV53FuncaoUsuario_Nome;
            AV12GridStateFilterValue.gxTpr_Operator = AV52FuncaoUsuario_NomeOperator;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFFuncaoUsuario_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV57TFFuncaoUsuario_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV58TFFuncaoUsuario_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Modulo_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&MODULO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV67Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ModuloFuncoes";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Modulo_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV31Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_3P2( true) ;
         }
         else
         {
            wb_table2_8_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_30_3P2( true) ;
         }
         else
         {
            wb_table3_30_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table3_30_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3P2e( true) ;
         }
         else
         {
            wb_table1_2_3P2e( false) ;
         }
      }

      protected void wb_table3_30_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"33\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A162FuncaoUsuario_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoUsuario_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 33 )
         {
            wbEnd = 0;
            nRC_GXsfl_33 = (short)(nGXsfl_33_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_30_3P2e( true) ;
         }
         else
         {
            wb_table3_30_3P2e( false) ;
         }
      }

      protected void wb_table2_8_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            wb_table4_11_3P2( true) ;
         }
         else
         {
            wb_table4_11_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_15_3P2( true) ;
         }
         else
         {
            wb_table5_15_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table5_15_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3P2e( true) ;
         }
         else
         {
            wb_table2_8_3P2e( false) ;
         }
      }

      protected void wb_table5_15_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloModuloFuncoesWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextfuncaousuario_nome_Internalname, "Fun��o de Usu�rio", "", "", lblFiltertextfuncaousuario_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ModuloModuloFuncoesWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_22_3P2( true) ;
         }
         else
         {
            wb_table6_22_3P2( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_3P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_15_3P2e( true) ;
         }
         else
         {
            wb_table5_15_3P2e( false) ;
         }
      }

      protected void wb_table6_22_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedfuncaousuario_nome_Internalname, tblTablemergedfuncaousuario_nome_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaousuario_nomeoperator, cmbavFuncaousuario_nomeoperator_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0)), 1, cmbavFuncaousuario_nomeoperator_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_ModuloModuloFuncoesWC.htm");
            cmbavFuncaousuario_nomeoperator.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV52FuncaoUsuario_NomeOperator), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaousuario_nomeoperator_Internalname, "Values", (String)(cmbavFuncaousuario_nomeoperator.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_33_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaousuario_nome_Internalname, AV53FuncaoUsuario_Nome, StringUtil.RTrim( context.localUtil.Format( AV53FuncaoUsuario_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaousuario_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloModuloFuncoesWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_3P2e( true) ;
         }
         else
         {
            wb_table6_22_3P2e( false) ;
         }
      }

      protected void wb_table4_11_3P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_3P2e( true) ;
         }
         else
         {
            wb_table4_11_3P2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Modulo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3P2( ) ;
         WS3P2( ) ;
         WE3P2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Modulo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA3P2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "modulomodulofuncoeswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA3P2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Modulo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
         }
         wcpOAV7Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Modulo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Modulo_Codigo != wcpOAV7Modulo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Modulo_Codigo = AV7Modulo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Modulo_Codigo = cgiGet( sPrefix+"AV7Modulo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Modulo_Codigo) > 0 )
         {
            AV7Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Modulo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Modulo_Codigo), 6, 0)));
         }
         else
         {
            AV7Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Modulo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA3P2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS3P2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS3P2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Modulo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Modulo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Modulo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Modulo_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Modulo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE3P2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031177274");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("modulomodulofuncoeswc.js", "?202031177274");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_332( )
      {
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO_"+sGXsfl_33_idx;
         edtFuncaoUsuario_Nome_Internalname = sPrefix+"FUNCAOUSUARIO_NOME_"+sGXsfl_33_idx;
      }

      protected void SubsflControlProps_fel_332( )
      {
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO_"+sGXsfl_33_fel_idx;
         edtFuncaoUsuario_Nome_Internalname = sPrefix+"FUNCAOUSUARIO_NOME_"+sGXsfl_33_fel_idx;
      }

      protected void sendrow_332( )
      {
         SubsflControlProps_332( ) ;
         WB3P0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_33_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_33_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_33_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_Nome_Internalname,(String)A162FuncaoUsuario_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtFuncaoUsuario_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)33,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOUSUARIO_CODIGO"+"_"+sGXsfl_33_idx, GetSecureSignedToken( sPrefix+sGXsfl_33_idx, context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_33_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_33_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_33_idx+1));
            sGXsfl_33_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_33_idx), 4, 0)), 4, "0");
            SubsflControlProps_332( ) ;
         }
         /* End function sendrow_332 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextfuncaousuario_nome_Internalname = sPrefix+"FILTERTEXTFUNCAOUSUARIO_NOME";
         cmbavFuncaousuario_nomeoperator_Internalname = sPrefix+"vFUNCAOUSUARIO_NOMEOPERATOR";
         edtavFuncaousuario_nome_Internalname = sPrefix+"vFUNCAOUSUARIO_NOME";
         tblTablemergedfuncaousuario_nome_Internalname = sPrefix+"TABLEMERGEDFUNCAOUSUARIO_NOME";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtFuncaoUsuario_Codigo_Internalname = sPrefix+"FUNCAOUSUARIO_CODIGO";
         edtFuncaoUsuario_Nome_Internalname = sPrefix+"FUNCAOUSUARIO_NOME";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtModulo_Codigo_Internalname = sPrefix+"MODULO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTffuncaousuario_nome_Internalname = sPrefix+"vTFFUNCAOUSUARIO_NOME";
         edtavTffuncaousuario_nome_sel_Internalname = sPrefix+"vTFFUNCAOUSUARIO_NOME_SEL";
         Ddo_funcaousuario_nome_Internalname = sPrefix+"DDO_FUNCAOUSUARIO_NOME";
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoUsuario_Nome_Jsonclick = "";
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtavFuncaousuario_nome_Jsonclick = "";
         cmbavFuncaousuario_nomeoperator_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoUsuario_Nome_Link = "";
         edtFuncaoUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtFuncaoUsuario_Nome_Title = "Fun��o de Usu�rio";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaousuario_nome_sel_Jsonclick = "";
         edtavTffuncaousuario_nome_sel_Visible = 1;
         edtavTffuncaousuario_nome_Jsonclick = "";
         edtavTffuncaousuario_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtModulo_Codigo_Jsonclick = "";
         edtModulo_Codigo_Visible = 1;
         Ddo_funcaousuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaousuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaousuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaousuario_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaousuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaousuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaousuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaousuario_nome_Datalistproc = "GetModuloModuloFuncoesWCFilterData";
         Ddo_funcaousuario_nome_Datalisttype = "Dynamic";
         Ddo_funcaousuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaousuario_nome_Filtertype = "Character";
         Ddo_funcaousuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaousuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaousuario_nome_Cls = "ColumnSettings";
         Ddo_funcaousuario_nome_Tooltip = "Op��es";
         Ddo_funcaousuario_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV53FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV52FuncaoUsuario_NomeOperator',fld:'vFUNCAOUSUARIO_NOMEOPERATOR',pic:'ZZZ9',nv:0},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV56FuncaoUsuario_NomeTitleFilterData',fld:'vFUNCAOUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoUsuario_Nome_Titleformat',ctrl:'FUNCAOUSUARIO_NOME',prop:'Titleformat'},{av:'edtFuncaoUsuario_Nome_Title',ctrl:'FUNCAOUSUARIO_NOME',prop:'Title'},{av:'AV62GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV63GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV52FuncaoUsuario_NomeOperator',fld:'vFUNCAOUSUARIO_NOMEOPERATOR',pic:'ZZZ9',nv:0},{av:'AV53FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOUSUARIO_NOME.ONOPTIONCLICKED","{handler:'E123P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV52FuncaoUsuario_NomeOperator',fld:'vFUNCAOUSUARIO_NOMEOPERATOR',pic:'ZZZ9',nv:0},{av:'AV53FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_funcaousuario_nome_Activeeventkey',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaousuario_nome_Filteredtext_get',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaousuario_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaousuario_nome_Sortedstatus',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SortedStatus'},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E163P2',iparms:[{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtFuncaoUsuario_Nome_Link',ctrl:'FUNCAOUSUARIO_NOME',prop:'Link'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E133P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV52FuncaoUsuario_NomeOperator',fld:'vFUNCAOUSUARIO_NOMEOPERATOR',pic:'ZZZ9',nv:0},{av:'AV53FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV7Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV52FuncaoUsuario_NomeOperator',fld:'vFUNCAOUSUARIO_NOMEOPERATOR',pic:'ZZZ9',nv:0},{av:'AV53FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV57TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'Ddo_funcaousuario_nome_Filteredtext_set',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'FilteredText_set'},{av:'AV58TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaousuario_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SelectedValue_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaousuario_nome_Activeeventkey = "";
         Ddo_funcaousuario_nome_Filteredtext_get = "";
         Ddo_funcaousuario_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV53FuncaoUsuario_Nome = "";
         AV57TFFuncaoUsuario_Nome = "";
         AV58TFFuncaoUsuario_Nome_Sel = "";
         AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace = "";
         AV67Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV60DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV56FuncaoUsuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaousuario_nome_Filteredtext_set = "";
         Ddo_funcaousuario_nome_Selectedvalue_set = "";
         Ddo_funcaousuario_nome_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A162FuncaoUsuario_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV53FuncaoUsuario_Nome = "";
         lV57TFFuncaoUsuario_Nome = "";
         H003P2_A146Modulo_Codigo = new int[1] ;
         H003P2_A162FuncaoUsuario_Nome = new String[] {""} ;
         H003P2_A161FuncaoUsuario_Codigo = new int[1] ;
         H003P3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GridRow = new GXWebRow();
         AV31Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         lblFiltertextfuncaousuario_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Modulo_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulomodulofuncoeswc__default(),
            new Object[][] {
                new Object[] {
               H003P2_A146Modulo_Codigo, H003P2_A162FuncaoUsuario_Nome, H003P2_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               H003P3_AGRID_nRecordCount
               }
            }
         );
         AV67Pgmname = "ModuloModuloFuncoesWC";
         /* GeneXus formulas. */
         AV67Pgmname = "ModuloModuloFuncoesWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_33 ;
      private short nGXsfl_33_idx=1 ;
      private short AV52FuncaoUsuario_NomeOperator ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_33_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoUsuario_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Modulo_Codigo ;
      private int wcpOAV7Modulo_Codigo ;
      private int subGrid_Rows ;
      private int A161FuncaoUsuario_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaousuario_nome_Datalistupdateminimumcharacters ;
      private int A146Modulo_Codigo ;
      private int edtModulo_Codigo_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTffuncaousuario_nome_Visible ;
      private int edtavTffuncaousuario_nome_sel_Visible ;
      private int edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV61PageToGo ;
      private int AV68GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV62GridCurrentPage ;
      private long AV63GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaousuario_nome_Activeeventkey ;
      private String Ddo_funcaousuario_nome_Filteredtext_get ;
      private String Ddo_funcaousuario_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_33_idx="0001" ;
      private String AV67Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaousuario_nome_Caption ;
      private String Ddo_funcaousuario_nome_Tooltip ;
      private String Ddo_funcaousuario_nome_Cls ;
      private String Ddo_funcaousuario_nome_Filteredtext_set ;
      private String Ddo_funcaousuario_nome_Selectedvalue_set ;
      private String Ddo_funcaousuario_nome_Dropdownoptionstype ;
      private String Ddo_funcaousuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaousuario_nome_Sortedstatus ;
      private String Ddo_funcaousuario_nome_Filtertype ;
      private String Ddo_funcaousuario_nome_Datalisttype ;
      private String Ddo_funcaousuario_nome_Datalistproc ;
      private String Ddo_funcaousuario_nome_Sortasc ;
      private String Ddo_funcaousuario_nome_Sortdsc ;
      private String Ddo_funcaousuario_nome_Loadingdata ;
      private String Ddo_funcaousuario_nome_Cleanfilter ;
      private String Ddo_funcaousuario_nome_Noresultsfound ;
      private String Ddo_funcaousuario_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtModulo_Codigo_Internalname ;
      private String edtModulo_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTffuncaousuario_nome_Internalname ;
      private String edtavTffuncaousuario_nome_Jsonclick ;
      private String edtavTffuncaousuario_nome_sel_Internalname ;
      private String edtavTffuncaousuario_nome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavFuncaousuario_nomeoperator_Internalname ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String edtFuncaoUsuario_Nome_Internalname ;
      private String scmdbuf ;
      private String edtavFuncaousuario_nome_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaousuario_nome_Internalname ;
      private String edtFuncaoUsuario_Nome_Title ;
      private String edtFuncaoUsuario_Nome_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextfuncaousuario_nome_Internalname ;
      private String lblFiltertextfuncaousuario_nome_Jsonclick ;
      private String tblTablemergedfuncaousuario_nome_Internalname ;
      private String cmbavFuncaousuario_nomeoperator_Jsonclick ;
      private String edtavFuncaousuario_nome_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV7Modulo_Codigo ;
      private String sGXsfl_33_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String edtFuncaoUsuario_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaousuario_nome_Includesortasc ;
      private bool Ddo_funcaousuario_nome_Includesortdsc ;
      private bool Ddo_funcaousuario_nome_Includefilter ;
      private bool Ddo_funcaousuario_nome_Filterisrange ;
      private bool Ddo_funcaousuario_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV53FuncaoUsuario_Nome ;
      private String AV57TFFuncaoUsuario_Nome ;
      private String AV58TFFuncaoUsuario_Nome_Sel ;
      private String AV59ddo_FuncaoUsuario_NomeTitleControlIdToReplace ;
      private String A162FuncaoUsuario_Nome ;
      private String lV53FuncaoUsuario_Nome ;
      private String lV57TFFuncaoUsuario_Nome ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavFuncaousuario_nomeoperator ;
      private IDataStoreProvider pr_default ;
      private int[] H003P2_A146Modulo_Codigo ;
      private String[] H003P2_A162FuncaoUsuario_Nome ;
      private int[] H003P2_A161FuncaoUsuario_Codigo ;
      private long[] H003P3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56FuncaoUsuario_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV60DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class modulomodulofuncoeswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003P2( IGxContext context ,
                                             short AV52FuncaoUsuario_NomeOperator ,
                                             String AV53FuncaoUsuario_Nome ,
                                             String AV58TFFuncaoUsuario_Nome_Sel ,
                                             String AV57TFFuncaoUsuario_Nome ,
                                             String A162FuncaoUsuario_Nome ,
                                             bool AV15OrderedDsc ,
                                             int A146Modulo_Codigo ,
                                             int AV7Modulo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [10] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Modulo_Codigo], T2.[FuncaoUsuario_Nome], T1.[FuncaoUsuario_Codigo]";
         sFromString = " FROM ([ModuloFuncoes1] T1 WITH (NOLOCK) INNER JOIN [ModuloFuncoes] T2 WITH (NOLOCK) ON T2.[FuncaoUsuario_Codigo] = T1.[FuncaoUsuario_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Modulo_Codigo] = @AV7Modulo_Codigo)";
         if ( ( AV52FuncaoUsuario_NomeOperator == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV53FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( AV52FuncaoUsuario_NomeOperator == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like '%' + @lV53FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFFuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV57TFFuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] = @AV58TFFuncaoUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Codigo], T2.[FuncaoUsuario_Nome]";
         }
         else if ( AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Codigo] DESC, T2.[FuncaoUsuario_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Modulo_Codigo], T1.[FuncaoUsuario_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H003P3( IGxContext context ,
                                             short AV52FuncaoUsuario_NomeOperator ,
                                             String AV53FuncaoUsuario_Nome ,
                                             String AV58TFFuncaoUsuario_Nome_Sel ,
                                             String AV57TFFuncaoUsuario_Nome ,
                                             String A162FuncaoUsuario_Nome ,
                                             bool AV15OrderedDsc ,
                                             int A146Modulo_Codigo ,
                                             int AV7Modulo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ModuloFuncoes1] T1 WITH (NOLOCK) INNER JOIN [ModuloFuncoes] T2 WITH (NOLOCK) ON T2.[FuncaoUsuario_Codigo] = T1.[FuncaoUsuario_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Modulo_Codigo] = @AV7Modulo_Codigo)";
         if ( ( AV52FuncaoUsuario_NomeOperator == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV53FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( AV52FuncaoUsuario_NomeOperator == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like '%' + @lV53FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFFuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV57TFFuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFFuncaoUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] = @AV58TFFuncaoUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003P2(context, (short)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 1 :
                     return conditional_H003P3(context, (short)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003P2 ;
          prmH003P2 = new Object[] {
          new Object[] {"@AV7Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV53FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV53FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57TFFuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58TFFuncaoUsuario_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH003P3 ;
          prmH003P3 = new Object[] {
          new Object[] {"@AV7Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV53FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV53FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57TFFuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV58TFFuncaoUsuario_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003P2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003P2,11,0,true,false )
             ,new CursorDef("H003P3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003P3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
