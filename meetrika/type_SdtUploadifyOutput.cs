/*
               File: type_SdtUploadifyOutput
        Description: UploadifyOutput
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:7.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "UploadifyOutput" )]
   [XmlType(TypeName =  "UploadifyOutput" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtUploadifyOutput : GxUserType
   {
      public SdtUploadifyOutput( )
      {
         /* Constructor for serialization */
         gxTv_SdtUploadifyOutput_Originalfilename = "";
         gxTv_SdtUploadifyOutput_Temporalfilename = "";
         gxTv_SdtUploadifyOutput_Filetype = "";
         gxTv_SdtUploadifyOutput_Creationdate = (DateTime)(DateTime.MinValue);
         gxTv_SdtUploadifyOutput_Modificationdate = (DateTime)(DateTime.MinValue);
      }

      public SdtUploadifyOutput( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtUploadifyOutput deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtUploadifyOutput)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtUploadifyOutput obj ;
         obj = this;
         obj.gxTpr_Originalfilename = deserialized.gxTpr_Originalfilename;
         obj.gxTpr_Temporalfilename = deserialized.gxTpr_Temporalfilename;
         obj.gxTpr_Filesize = deserialized.gxTpr_Filesize;
         obj.gxTpr_Filetype = deserialized.gxTpr_Filetype;
         obj.gxTpr_Creationdate = deserialized.gxTpr_Creationdate;
         obj.gxTpr_Modificationdate = deserialized.gxTpr_Modificationdate;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "OriginalFileName") )
               {
                  gxTv_SdtUploadifyOutput_Originalfilename = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TemporalFileName") )
               {
                  gxTv_SdtUploadifyOutput_Temporalfilename = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FileSize") )
               {
                  gxTv_SdtUploadifyOutput_Filesize = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FileType") )
               {
                  gxTv_SdtUploadifyOutput_Filetype = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CreationDate") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtUploadifyOutput_Creationdate = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtUploadifyOutput_Creationdate = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ModificationDate") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtUploadifyOutput_Modificationdate = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtUploadifyOutput_Modificationdate = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "UploadifyOutput";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("OriginalFileName", StringUtil.RTrim( gxTv_SdtUploadifyOutput_Originalfilename));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TemporalFileName", StringUtil.RTrim( gxTv_SdtUploadifyOutput_Temporalfilename));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FileSize", StringUtil.Trim( StringUtil.Str( gxTv_SdtUploadifyOutput_Filesize, 10, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FileType", StringUtil.RTrim( gxTv_SdtUploadifyOutput_Filetype));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtUploadifyOutput_Creationdate) )
         {
            oWriter.WriteStartElement("CreationDate");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtUploadifyOutput_Creationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("CreationDate", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtUploadifyOutput_Modificationdate) )
         {
            oWriter.WriteStartElement("ModificationDate");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtUploadifyOutput_Modificationdate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ModificationDate", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("OriginalFileName", gxTv_SdtUploadifyOutput_Originalfilename, false);
         AddObjectProperty("TemporalFileName", gxTv_SdtUploadifyOutput_Temporalfilename, false);
         AddObjectProperty("FileSize", gxTv_SdtUploadifyOutput_Filesize, false);
         AddObjectProperty("FileType", gxTv_SdtUploadifyOutput_Filetype, false);
         datetime_STZ = gxTv_SdtUploadifyOutput_Creationdate;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("CreationDate", sDateCnv, false);
         datetime_STZ = gxTv_SdtUploadifyOutput_Modificationdate;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ModificationDate", sDateCnv, false);
         return  ;
      }

      [  SoapElement( ElementName = "OriginalFileName" )]
      [  XmlElement( ElementName = "OriginalFileName"   )]
      public String gxTpr_Originalfilename
      {
         get {
            return gxTv_SdtUploadifyOutput_Originalfilename ;
         }

         set {
            gxTv_SdtUploadifyOutput_Originalfilename = (String)(value);
         }

      }

      [  SoapElement( ElementName = "TemporalFileName" )]
      [  XmlElement( ElementName = "TemporalFileName"   )]
      public String gxTpr_Temporalfilename
      {
         get {
            return gxTv_SdtUploadifyOutput_Temporalfilename ;
         }

         set {
            gxTv_SdtUploadifyOutput_Temporalfilename = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FileSize" )]
      [  XmlElement( ElementName = "FileSize"   )]
      public double gxTpr_Filesize_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtUploadifyOutput_Filesize) ;
         }

         set {
            gxTv_SdtUploadifyOutput_Filesize = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Filesize
      {
         get {
            return gxTv_SdtUploadifyOutput_Filesize ;
         }

         set {
            gxTv_SdtUploadifyOutput_Filesize = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "FileType" )]
      [  XmlElement( ElementName = "FileType"   )]
      public String gxTpr_Filetype
      {
         get {
            return gxTv_SdtUploadifyOutput_Filetype ;
         }

         set {
            gxTv_SdtUploadifyOutput_Filetype = (String)(value);
         }

      }

      [  SoapElement( ElementName = "CreationDate" )]
      [  XmlElement( ElementName = "CreationDate"  , IsNullable=true )]
      public string gxTpr_Creationdate_Nullable
      {
         get {
            if ( gxTv_SdtUploadifyOutput_Creationdate == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtUploadifyOutput_Creationdate).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtUploadifyOutput_Creationdate = DateTime.MinValue;
            else
               gxTv_SdtUploadifyOutput_Creationdate = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Creationdate
      {
         get {
            return gxTv_SdtUploadifyOutput_Creationdate ;
         }

         set {
            gxTv_SdtUploadifyOutput_Creationdate = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ModificationDate" )]
      [  XmlElement( ElementName = "ModificationDate"  , IsNullable=true )]
      public string gxTpr_Modificationdate_Nullable
      {
         get {
            if ( gxTv_SdtUploadifyOutput_Modificationdate == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtUploadifyOutput_Modificationdate).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtUploadifyOutput_Modificationdate = DateTime.MinValue;
            else
               gxTv_SdtUploadifyOutput_Modificationdate = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Modificationdate
      {
         get {
            return gxTv_SdtUploadifyOutput_Modificationdate ;
         }

         set {
            gxTv_SdtUploadifyOutput_Modificationdate = (DateTime)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtUploadifyOutput_Originalfilename = "";
         gxTv_SdtUploadifyOutput_Temporalfilename = "";
         gxTv_SdtUploadifyOutput_Filetype = "";
         gxTv_SdtUploadifyOutput_Creationdate = (DateTime)(DateTime.MinValue);
         gxTv_SdtUploadifyOutput_Modificationdate = (DateTime)(DateTime.MinValue);
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected decimal gxTv_SdtUploadifyOutput_Filesize ;
      protected String gxTv_SdtUploadifyOutput_Originalfilename ;
      protected String gxTv_SdtUploadifyOutput_Temporalfilename ;
      protected String gxTv_SdtUploadifyOutput_Filetype ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtUploadifyOutput_Creationdate ;
      protected DateTime gxTv_SdtUploadifyOutput_Modificationdate ;
      protected DateTime datetime_STZ ;
   }

   [DataContract(Name = @"UploadifyOutput", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtUploadifyOutput_RESTInterface : GxGenericCollectionItem<SdtUploadifyOutput>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtUploadifyOutput_RESTInterface( ) : base()
      {
      }

      public SdtUploadifyOutput_RESTInterface( SdtUploadifyOutput psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "OriginalFileName" , Order = 0 )]
      public String gxTpr_Originalfilename
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Originalfilename) ;
         }

         set {
            sdt.gxTpr_Originalfilename = (String)(value);
         }

      }

      [DataMember( Name = "TemporalFileName" , Order = 1 )]
      public String gxTpr_Temporalfilename
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Temporalfilename) ;
         }

         set {
            sdt.gxTpr_Temporalfilename = (String)(value);
         }

      }

      [DataMember( Name = "FileSize" , Order = 2 )]
      public String gxTpr_Filesize
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Filesize, 10, 2)) ;
         }

         set {
            sdt.gxTpr_Filesize = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "FileType" , Order = 3 )]
      public String gxTpr_Filetype
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Filetype) ;
         }

         set {
            sdt.gxTpr_Filetype = (String)(value);
         }

      }

      [DataMember( Name = "CreationDate" , Order = 4 )]
      public String gxTpr_Creationdate
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Creationdate) ;
         }

         set {
            sdt.gxTpr_Creationdate = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ModificationDate" , Order = 5 )]
      public String gxTpr_Modificationdate
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Modificationdate) ;
         }

         set {
            sdt.gxTpr_Modificationdate = DateTimeUtil.CToT2( (String)(value));
         }

      }

      public SdtUploadifyOutput sdt
      {
         get {
            return (SdtUploadifyOutput)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtUploadifyOutput() ;
         }
      }

   }

}
