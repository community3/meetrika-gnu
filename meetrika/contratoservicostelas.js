/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:21:28.50
*/
gx.evt.autoSkip = false;
gx.define('contratoservicostelas', false, function () {
   this.ServerClass =  "contratoservicostelas" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7ContratoServicosTelas_ContratoCod=gx.fn.getIntegerValue("vCONTRATOSERVICOSTELAS_CONTRATOCOD",'.') ;
      this.AV12ContratoServicosTelas_Sequencial=gx.fn.getIntegerValue("vCONTRATOSERVICOSTELAS_SEQUENCIAL",'.') ;
      this.A938ContratoServicosTelas_Sequencial=gx.fn.getIntegerValue("CONTRATOSERVICOSTELAS_SEQUENCIAL",'.') ;
      this.A74Contrato_Codigo=gx.fn.getIntegerValue("CONTRATO_CODIGO",'.') ;
      this.A925ContratoServicosTelas_ServicoCod=gx.fn.getIntegerValue("CONTRATOSERVICOSTELAS_SERVICOCOD",'.') ;
      this.A39Contratada_Codigo=gx.fn.getIntegerValue("CONTRATADA_CODIGO",'.') ;
      this.A40Contratada_PessoaCod=gx.fn.getIntegerValue("CONTRATADA_PESSOACOD",'.') ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Contratoservicostelas_contratocod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contratoservicostelas_contratocod",["gx.O.A926ContratoServicosTelas_ContratoCod", "gx.O.A74Contrato_Codigo", "gx.O.A39Contratada_Codigo", "gx.O.A40Contratada_PessoaCod", "gx.O.A925ContratoServicosTelas_ServicoCod", "gx.O.A41Contratada_PessoaNom", "gx.O.A937ContratoServicosTelas_ServicoSigla"],["A74Contrato_Codigo", "A925ContratoServicosTelas_ServicoCod", "A39Contratada_Codigo", "A40Contratada_PessoaCod", "A41Contratada_PessoaNom", "A937ContratoServicosTelas_ServicoSigla"]);
      return true;
   }
   this.Valid_Contratoservicostelas_status=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATOSERVICOSTELAS_STATUS");
         this.AnyError  = 0;
         if ( ! ( ( this.A932ContratoServicosTelas_Status == "B" ) || ( this.A932ContratoServicosTelas_Status == "S" ) || ( this.A932ContratoServicosTelas_Status == "E" ) || ( this.A932ContratoServicosTelas_Status == "A" ) || ( this.A932ContratoServicosTelas_Status == "R" ) || ( this.A932ContratoServicosTelas_Status == "C" ) || ( this.A932ContratoServicosTelas_Status == "D" ) || ( this.A932ContratoServicosTelas_Status == "H" ) || ( this.A932ContratoServicosTelas_Status == "O" ) || ( this.A932ContratoServicosTelas_Status == "P" ) || ( this.A932ContratoServicosTelas_Status == "L" ) || ( this.A932ContratoServicosTelas_Status == "X" ) || ( this.A932ContratoServicosTelas_Status == "N" ) || ( this.A932ContratoServicosTelas_Status == "J" ) || ( this.A932ContratoServicosTelas_Status == "I" ) || ( this.A932ContratoServicosTelas_Status == "T" ) || ( this.A932ContratoServicosTelas_Status == "Q" ) || ( this.A932ContratoServicosTelas_Status == "G" ) || ( this.A932ContratoServicosTelas_Status == "M" ) || ( this.A932ContratoServicosTelas_Status == "U" ) || ((''==this.A932ContratoServicosTelas_Status)) ) )
         {
            try {
               gxballoon.setError("Campo Status fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e122w2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e132w116_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e142w116_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,21,23,26,28,31,33,36,38,41,43,46,48,51,59];
   this.GXLastCtrlId =59;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Tela associada", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKCONTRATADA_PESSOANOM", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOANOM",gxz:"Z41Contratada_PessoaNom",gxold:"O41Contratada_PessoaNom",gxvar:"A41Contratada_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A41Contratada_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z41Contratada_PessoaNom=Value},v2c:function(){gx.fn.setControlValue("CONTRATADA_PESSOANOM",gx.O.A41Contratada_PessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A41Contratada_PessoaNom=this.val()},val:function(){return gx.fn.getControlValue("CONTRATADA_PESSOANOM")},nac:gx.falseFn};
   this.declareDomainHdlr( 18 , function() {
   });
   GXValidFnc[21]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_SERVICOSIGLA", format:0,grid:0};
   GXValidFnc[23]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_SERVICOSIGLA",gxz:"Z937ContratoServicosTelas_ServicoSigla",gxold:"O937ContratoServicosTelas_ServicoSigla",gxvar:"A937ContratoServicosTelas_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A937ContratoServicosTelas_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z937ContratoServicosTelas_ServicoSigla=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_SERVICOSIGLA",gx.O.A937ContratoServicosTelas_ServicoSigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A937ContratoServicosTelas_ServicoSigla=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_SERVICOSIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 23 , function() {
   });
   GXValidFnc[26]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_TELA", format:0,grid:0};
   GXValidFnc[28]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_TELA",gxz:"Z931ContratoServicosTelas_Tela",gxold:"O931ContratoServicosTelas_Tela",gxvar:"A931ContratoServicosTelas_Tela",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A931ContratoServicosTelas_Tela=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z931ContratoServicosTelas_Tela=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_TELA",gx.O.A931ContratoServicosTelas_Tela,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A931ContratoServicosTelas_Tela=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_TELA")},nac:gx.falseFn};
   this.declareDomainHdlr( 28 , function() {
   });
   GXValidFnc[31]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_LINK", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:80,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_LINK",gxz:"Z928ContratoServicosTelas_Link",gxold:"O928ContratoServicosTelas_Link",gxvar:"A928ContratoServicosTelas_Link",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A928ContratoServicosTelas_Link=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z928ContratoServicosTelas_Link=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_LINK",gx.O.A928ContratoServicosTelas_Link,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A928ContratoServicosTelas_Link=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_LINK")},nac:gx.falseFn};
   this.declareDomainHdlr( 33 , function() {
   });
   GXValidFnc[36]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_PARMS", format:0,grid:0};
   GXValidFnc[38]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_PARMS",gxz:"Z929ContratoServicosTelas_Parms",gxold:"O929ContratoServicosTelas_Parms",gxvar:"A929ContratoServicosTelas_Parms",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A929ContratoServicosTelas_Parms=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z929ContratoServicosTelas_Parms=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_PARMS",gx.O.A929ContratoServicosTelas_Parms,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A929ContratoServicosTelas_Parms=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_PARMS")},nac:gx.falseFn};
   this.declareDomainHdlr( 38 , function() {
   });
   GXValidFnc[41]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_STATUS", format:0,grid:0};
   GXValidFnc[43]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratoservicostelas_status,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_STATUS",gxz:"Z932ContratoServicosTelas_Status",gxold:"O932ContratoServicosTelas_Status",gxvar:"A932ContratoServicosTelas_Status",ucs:[],op:[43],ip:[43],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A932ContratoServicosTelas_Status=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z932ContratoServicosTelas_Status=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSERVICOSTELAS_STATUS",gx.O.A932ContratoServicosTelas_Status);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A932ContratoServicosTelas_Status=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_STATUS")},nac:gx.falseFn};
   this.declareDomainHdlr( 43 , function() {
   });
   GXValidFnc[46]={fld:"TEXTBLOCKCONTRATOSERVICOSTELAS_TEXTMOUSE", format:0,grid:0};
   GXValidFnc[48]={lvl:0,type:"char",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_TEXTMOUSE",gxz:"Z934ContratoServicosTelas_TextMouse",gxold:"O934ContratoServicosTelas_TextMouse",gxvar:"A934ContratoServicosTelas_TextMouse",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A934ContratoServicosTelas_TextMouse=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z934ContratoServicosTelas_TextMouse=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_TEXTMOUSE",gx.O.A934ContratoServicosTelas_TextMouse,0)},c2v:function(){if(this.val()!==undefined)gx.O.A934ContratoServicosTelas_TextMouse=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSTELAS_TEXTMOUSE")},nac:gx.falseFn};
   GXValidFnc[51]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[59]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratoservicostelas_contratocod,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSTELAS_CONTRATOCOD",gxz:"Z926ContratoServicosTelas_ContratoCod",gxold:"O926ContratoServicosTelas_ContratoCod",gxvar:"A926ContratoServicosTelas_ContratoCod",ucs:[],op:[23,18],ip:[23,18,59],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A926ContratoServicosTelas_ContratoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z926ContratoServicosTelas_ContratoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSTELAS_CONTRATOCOD",gx.O.A926ContratoServicosTelas_ContratoCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A926ContratoServicosTelas_ContratoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSERVICOSTELAS_CONTRATOCOD",'.')},nac:function(){return !((0==this.AV7ContratoServicosTelas_ContratoCod))}};
   this.declareDomainHdlr( 59 , function() {
   });
   this.A41Contratada_PessoaNom = "" ;
   this.Z41Contratada_PessoaNom = "" ;
   this.O41Contratada_PessoaNom = "" ;
   this.A937ContratoServicosTelas_ServicoSigla = "" ;
   this.Z937ContratoServicosTelas_ServicoSigla = "" ;
   this.O937ContratoServicosTelas_ServicoSigla = "" ;
   this.A931ContratoServicosTelas_Tela = "" ;
   this.Z931ContratoServicosTelas_Tela = "" ;
   this.O931ContratoServicosTelas_Tela = "" ;
   this.A928ContratoServicosTelas_Link = "" ;
   this.Z928ContratoServicosTelas_Link = "" ;
   this.O928ContratoServicosTelas_Link = "" ;
   this.A929ContratoServicosTelas_Parms = "" ;
   this.Z929ContratoServicosTelas_Parms = "" ;
   this.O929ContratoServicosTelas_Parms = "" ;
   this.A932ContratoServicosTelas_Status = "" ;
   this.Z932ContratoServicosTelas_Status = "" ;
   this.O932ContratoServicosTelas_Status = "" ;
   this.A934ContratoServicosTelas_TextMouse = "" ;
   this.Z934ContratoServicosTelas_TextMouse = "" ;
   this.O934ContratoServicosTelas_TextMouse = "" ;
   this.A926ContratoServicosTelas_ContratoCod = 0 ;
   this.Z926ContratoServicosTelas_ContratoCod = 0 ;
   this.O926ContratoServicosTelas_ContratoCod = 0 ;
   this.A40Contratada_PessoaCod = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV7ContratoServicosTelas_ContratoCod = 0 ;
   this.AV12ContratoServicosTelas_Sequencial = 0 ;
   this.AV10WebSession = {} ;
   this.A926ContratoServicosTelas_ContratoCod = 0 ;
   this.A938ContratoServicosTelas_Sequencial = 0 ;
   this.A41Contratada_PessoaNom = "" ;
   this.A925ContratoServicosTelas_ServicoCod = 0 ;
   this.A937ContratoServicosTelas_ServicoSigla = "" ;
   this.A931ContratoServicosTelas_Tela = "" ;
   this.A928ContratoServicosTelas_Link = "" ;
   this.A929ContratoServicosTelas_Parms = "" ;
   this.A932ContratoServicosTelas_Status = "" ;
   this.A934ContratoServicosTelas_TextMouse = "" ;
   this.Gx_mode = "" ;
   this.Events = {"e122w2_client": ["AFTER TRN", true] ,"e132w116_client": ["ENTER", true] ,"e142w116_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosTelas_ContratoCod',fld:'vCONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV12ContratoServicosTelas_Sequencial',fld:'vCONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV7ContratoServicosTelas_ContratoCod", "vCONTRATOSERVICOSTELAS_CONTRATOCOD", 0, "int");
   this.setVCMap("AV12ContratoServicosTelas_Sequencial", "vCONTRATOSERVICOSTELAS_SEQUENCIAL", 0, "int");
   this.setVCMap("A938ContratoServicosTelas_Sequencial", "CONTRATOSERVICOSTELAS_SEQUENCIAL", 0, "int");
   this.setVCMap("A74Contrato_Codigo", "CONTRATO_CODIGO", 0, "int");
   this.setVCMap("A925ContratoServicosTelas_ServicoCod", "CONTRATOSERVICOSTELAS_SERVICOCOD", 0, "int");
   this.setVCMap("A39Contratada_Codigo", "CONTRATADA_CODIGO", 0, "int");
   this.setVCMap("A40Contratada_PessoaCod", "CONTRATADA_PESSOACOD", 0, "int");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(contratoservicostelas);
