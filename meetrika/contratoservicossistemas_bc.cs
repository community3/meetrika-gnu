/*
               File: ContratoServicosSistemas_BC
        Description: Sistemas do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:36.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicossistemas_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoservicossistemas_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicossistemas_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow34128( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey34128( ) ;
         standaloneModal( ) ;
         AddRow34128( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
               Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_340( )
      {
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls34128( ) ;
            }
            else
            {
               CheckExtendedTable34128( ) ;
               if ( AnyError == 0 )
               {
                  ZM34128( 2) ;
                  ZM34128( 3) ;
                  ZM34128( 4) ;
               }
               CloseExtendedTableCursors34128( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM34128( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1068ContratoServicosSistemas_ServicoSigla = A1068ContratoServicosSistemas_ServicoSigla;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1064ContratoServicosSistemas_SistemaSigla = A1064ContratoServicosSistemas_SistemaSigla;
         }
         if ( GX_JID == -1 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
            Z1068ContratoServicosSistemas_ServicoSigla = A1068ContratoServicosSistemas_ServicoSigla;
            Z1064ContratoServicosSistemas_SistemaSigla = A1064ContratoServicosSistemas_SistemaSigla;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load34128( )
      {
         /* Using cursor BC00347 */
         pr_default.execute(5, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound128 = 1;
            A1068ContratoServicosSistemas_ServicoSigla = BC00347_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC00347_n1068ContratoServicosSistemas_ServicoSigla[0];
            A1064ContratoServicosSistemas_SistemaSigla = BC00347_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC00347_n1064ContratoServicosSistemas_SistemaSigla[0];
            ZM34128( -1) ;
         }
         pr_default.close(5);
         OnLoadActions34128( ) ;
      }

      protected void OnLoadActions34128( )
      {
      }

      protected void CheckExtendedTable34128( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00344 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC00345 */
         pr_default.execute(3, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
            AnyError = 1;
         }
         A1068ContratoServicosSistemas_ServicoSigla = BC00345_A1068ContratoServicosSistemas_ServicoSigla[0];
         n1068ContratoServicosSistemas_ServicoSigla = BC00345_n1068ContratoServicosSistemas_ServicoSigla[0];
         pr_default.close(3);
         /* Using cursor BC00346 */
         pr_default.execute(4, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
            AnyError = 1;
         }
         A1064ContratoServicosSistemas_SistemaSigla = BC00346_A1064ContratoServicosSistemas_SistemaSigla[0];
         n1064ContratoServicosSistemas_SistemaSigla = BC00346_n1064ContratoServicosSistemas_SistemaSigla[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors34128( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey34128( )
      {
         /* Using cursor BC00348 */
         pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound128 = 1;
         }
         else
         {
            RcdFound128 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00343 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM34128( 1) ;
            RcdFound128 = 1;
            A160ContratoServicos_Codigo = BC00343_A160ContratoServicos_Codigo[0];
            A1067ContratoServicosSistemas_ServicoCod = BC00343_A1067ContratoServicosSistemas_ServicoCod[0];
            A1063ContratoServicosSistemas_SistemaCod = BC00343_A1063ContratoServicosSistemas_SistemaCod[0];
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
            sMode128 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load34128( ) ;
            if ( AnyError == 1 )
            {
               RcdFound128 = 0;
               InitializeNonKey34128( ) ;
            }
            Gx_mode = sMode128;
         }
         else
         {
            RcdFound128 = 0;
            InitializeNonKey34128( ) ;
            sMode128 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode128;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey34128( ) ;
         if ( RcdFound128 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_340( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency34128( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00342 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosSistemas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosSistemas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert34128( )
      {
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable34128( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM34128( 0) ;
            CheckOptimisticConcurrency34128( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm34128( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert34128( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00349 */
                     pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosSistemas") ;
                     if ( (pr_default.getStatus(7) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load34128( ) ;
            }
            EndLevel34128( ) ;
         }
         CloseExtendedTableCursors34128( ) ;
      }

      protected void Update34128( )
      {
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable34128( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency34128( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm34128( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate34128( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratoServicosSistemas] */
                     DeferredUpdate34128( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel34128( ) ;
         }
         CloseExtendedTableCursors34128( ) ;
      }

      protected void DeferredUpdate34128( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate34128( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency34128( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls34128( ) ;
            AfterConfirm34128( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete34128( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003410 */
                  pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosSistemas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode128 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel34128( ) ;
         Gx_mode = sMode128;
      }

      protected void OnDeleteControls34128( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003411 */
            pr_default.execute(9, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
            A1068ContratoServicosSistemas_ServicoSigla = BC003411_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC003411_n1068ContratoServicosSistemas_ServicoSigla[0];
            pr_default.close(9);
            /* Using cursor BC003412 */
            pr_default.execute(10, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
            A1064ContratoServicosSistemas_SistemaSigla = BC003412_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC003412_n1064ContratoServicosSistemas_SistemaSigla[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel34128( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete34128( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart34128( )
      {
         /* Using cursor BC003413 */
         pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo, A1067ContratoServicosSistemas_ServicoCod, A1063ContratoServicosSistemas_SistemaCod});
         RcdFound128 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound128 = 1;
            A1068ContratoServicosSistemas_ServicoSigla = BC003413_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC003413_n1068ContratoServicosSistemas_ServicoSigla[0];
            A1064ContratoServicosSistemas_SistemaSigla = BC003413_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC003413_n1064ContratoServicosSistemas_SistemaSigla[0];
            A160ContratoServicos_Codigo = BC003413_A160ContratoServicos_Codigo[0];
            A1067ContratoServicosSistemas_ServicoCod = BC003413_A1067ContratoServicosSistemas_ServicoCod[0];
            A1063ContratoServicosSistemas_SistemaCod = BC003413_A1063ContratoServicosSistemas_SistemaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext34128( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound128 = 0;
         ScanKeyLoad34128( ) ;
      }

      protected void ScanKeyLoad34128( )
      {
         sMode128 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound128 = 1;
            A1068ContratoServicosSistemas_ServicoSigla = BC003413_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC003413_n1068ContratoServicosSistemas_ServicoSigla[0];
            A1064ContratoServicosSistemas_SistemaSigla = BC003413_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC003413_n1064ContratoServicosSistemas_SistemaSigla[0];
            A160ContratoServicos_Codigo = BC003413_A160ContratoServicos_Codigo[0];
            A1067ContratoServicosSistemas_ServicoCod = BC003413_A1067ContratoServicosSistemas_ServicoCod[0];
            A1063ContratoServicosSistemas_SistemaCod = BC003413_A1063ContratoServicosSistemas_SistemaCod[0];
         }
         Gx_mode = sMode128;
      }

      protected void ScanKeyEnd34128( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm34128( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert34128( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate34128( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete34128( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete34128( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate34128( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes34128( )
      {
      }

      protected void AddRow34128( )
      {
         VarsToRow128( bcContratoServicosSistemas) ;
      }

      protected void ReadRow34128( )
      {
         RowToVars128( bcContratoServicosSistemas, 1) ;
      }

      protected void InitializeNonKey34128( )
      {
         A1068ContratoServicosSistemas_ServicoSigla = "";
         n1068ContratoServicosSistemas_ServicoSigla = false;
         A1064ContratoServicosSistemas_SistemaSigla = "";
         n1064ContratoServicosSistemas_SistemaSigla = false;
      }

      protected void InitAll34128( )
      {
         A160ContratoServicos_Codigo = 0;
         A1067ContratoServicosSistemas_ServicoCod = 0;
         A1063ContratoServicosSistemas_SistemaCod = 0;
         InitializeNonKey34128( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow128( SdtContratoServicosSistemas obj128 )
      {
         obj128.gxTpr_Mode = Gx_mode;
         obj128.gxTpr_Contratoservicossistemas_servicosigla = A1068ContratoServicosSistemas_ServicoSigla;
         obj128.gxTpr_Contratoservicossistemas_sistemasigla = A1064ContratoServicosSistemas_SistemaSigla;
         obj128.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj128.gxTpr_Contratoservicossistemas_servicocod = A1067ContratoServicosSistemas_ServicoCod;
         obj128.gxTpr_Contratoservicossistemas_sistemacod = A1063ContratoServicosSistemas_SistemaCod;
         obj128.gxTpr_Contratoservicos_codigo_Z = Z160ContratoServicos_Codigo;
         obj128.gxTpr_Contratoservicossistemas_servicocod_Z = Z1067ContratoServicosSistemas_ServicoCod;
         obj128.gxTpr_Contratoservicossistemas_servicosigla_Z = Z1068ContratoServicosSistemas_ServicoSigla;
         obj128.gxTpr_Contratoservicossistemas_sistemacod_Z = Z1063ContratoServicosSistemas_SistemaCod;
         obj128.gxTpr_Contratoservicossistemas_sistemasigla_Z = Z1064ContratoServicosSistemas_SistemaSigla;
         obj128.gxTpr_Contratoservicossistemas_servicosigla_N = (short)(Convert.ToInt16(n1068ContratoServicosSistemas_ServicoSigla));
         obj128.gxTpr_Contratoservicossistemas_sistemasigla_N = (short)(Convert.ToInt16(n1064ContratoServicosSistemas_SistemaSigla));
         obj128.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow128( SdtContratoServicosSistemas obj128 )
      {
         obj128.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj128.gxTpr_Contratoservicossistemas_servicocod = A1067ContratoServicosSistemas_ServicoCod;
         obj128.gxTpr_Contratoservicossistemas_sistemacod = A1063ContratoServicosSistemas_SistemaCod;
         return  ;
      }

      public void RowToVars128( SdtContratoServicosSistemas obj128 ,
                                int forceLoad )
      {
         Gx_mode = obj128.gxTpr_Mode;
         A1068ContratoServicosSistemas_ServicoSigla = obj128.gxTpr_Contratoservicossistemas_servicosigla;
         n1068ContratoServicosSistemas_ServicoSigla = false;
         A1064ContratoServicosSistemas_SistemaSigla = obj128.gxTpr_Contratoservicossistemas_sistemasigla;
         n1064ContratoServicosSistemas_SistemaSigla = false;
         A160ContratoServicos_Codigo = obj128.gxTpr_Contratoservicos_codigo;
         A1067ContratoServicosSistemas_ServicoCod = obj128.gxTpr_Contratoservicossistemas_servicocod;
         A1063ContratoServicosSistemas_SistemaCod = obj128.gxTpr_Contratoservicossistemas_sistemacod;
         Z160ContratoServicos_Codigo = obj128.gxTpr_Contratoservicos_codigo_Z;
         Z1067ContratoServicosSistemas_ServicoCod = obj128.gxTpr_Contratoservicossistemas_servicocod_Z;
         Z1068ContratoServicosSistemas_ServicoSigla = obj128.gxTpr_Contratoservicossistemas_servicosigla_Z;
         Z1063ContratoServicosSistemas_SistemaCod = obj128.gxTpr_Contratoservicossistemas_sistemacod_Z;
         Z1064ContratoServicosSistemas_SistemaSigla = obj128.gxTpr_Contratoservicossistemas_sistemasigla_Z;
         n1068ContratoServicosSistemas_ServicoSigla = (bool)(Convert.ToBoolean(obj128.gxTpr_Contratoservicossistemas_servicosigla_N));
         n1064ContratoServicosSistemas_SistemaSigla = (bool)(Convert.ToBoolean(obj128.gxTpr_Contratoservicossistemas_sistemasigla_N));
         Gx_mode = obj128.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A160ContratoServicos_Codigo = (int)getParm(obj,0);
         A1067ContratoServicosSistemas_ServicoCod = (int)getParm(obj,1);
         A1063ContratoServicosSistemas_SistemaCod = (int)getParm(obj,2);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey34128( ) ;
         ScanKeyStart34128( ) ;
         if ( RcdFound128 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003414 */
            pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC003411 */
            pr_default.execute(9, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
               AnyError = 1;
            }
            A1068ContratoServicosSistemas_ServicoSigla = BC003411_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC003411_n1068ContratoServicosSistemas_ServicoSigla[0];
            pr_default.close(9);
            /* Using cursor BC003412 */
            pr_default.execute(10, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
               AnyError = 1;
            }
            A1064ContratoServicosSistemas_SistemaSigla = BC003412_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC003412_n1064ContratoServicosSistemas_SistemaSigla[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
         }
         ZM34128( -1) ;
         OnLoadActions34128( ) ;
         AddRow34128( ) ;
         ScanKeyEnd34128( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars128( bcContratoServicosSistemas, 0) ;
         ScanKeyStart34128( ) ;
         if ( RcdFound128 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003414 */
            pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC003411 */
            pr_default.execute(9, new Object[] {A1067ContratoServicosSistemas_ServicoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Servico'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD");
               AnyError = 1;
            }
            A1068ContratoServicosSistemas_ServicoSigla = BC003411_A1068ContratoServicosSistemas_ServicoSigla[0];
            n1068ContratoServicosSistemas_ServicoSigla = BC003411_n1068ContratoServicosSistemas_ServicoSigla[0];
            pr_default.close(9);
            /* Using cursor BC003412 */
            pr_default.execute(10, new Object[] {A1063ContratoServicosSistemas_SistemaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD");
               AnyError = 1;
            }
            A1064ContratoServicosSistemas_SistemaSigla = BC003412_A1064ContratoServicosSistemas_SistemaSigla[0];
            n1064ContratoServicosSistemas_SistemaSigla = BC003412_n1064ContratoServicosSistemas_SistemaSigla[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1067ContratoServicosSistemas_ServicoCod = A1067ContratoServicosSistemas_ServicoCod;
            Z1063ContratoServicosSistemas_SistemaCod = A1063ContratoServicosSistemas_SistemaCod;
         }
         ZM34128( -1) ;
         OnLoadActions34128( ) ;
         AddRow34128( ) ;
         ScanKeyEnd34128( ) ;
         if ( RcdFound128 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars128( bcContratoServicosSistemas, 0) ;
         nKeyPressed = 1;
         GetKey34128( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert34128( ) ;
         }
         else
         {
            if ( RcdFound128 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  A1067ContratoServicosSistemas_ServicoCod = Z1067ContratoServicosSistemas_ServicoCod;
                  A1063ContratoServicosSistemas_SistemaCod = Z1063ContratoServicosSistemas_SistemaCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update34128( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert34128( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert34128( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow128( bcContratoServicosSistemas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars128( bcContratoServicosSistemas, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey34128( ) ;
         if ( RcdFound128 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
            {
               A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
               A1067ContratoServicosSistemas_ServicoCod = Z1067ContratoServicosSistemas_ServicoCod;
               A1063ContratoServicosSistemas_SistemaCod = Z1063ContratoServicosSistemas_SistemaCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1067ContratoServicosSistemas_ServicoCod != Z1067ContratoServicosSistemas_ServicoCod ) || ( A1063ContratoServicosSistemas_SistemaCod != Z1063ContratoServicosSistemas_SistemaCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContratoServicosSistemas_BC");
         VarsToRow128( bcContratoServicosSistemas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoServicosSistemas.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoServicosSistemas.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoServicosSistemas )
         {
            bcContratoServicosSistemas = (SdtContratoServicosSistemas)(sdt);
            if ( StringUtil.StrCmp(bcContratoServicosSistemas.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosSistemas.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow128( bcContratoServicosSistemas) ;
            }
            else
            {
               RowToVars128( bcContratoServicosSistemas, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoServicosSistemas.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosSistemas.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars128( bcContratoServicosSistemas, 1) ;
         return  ;
      }

      public SdtContratoServicosSistemas ContratoServicosSistemas_BC
      {
         get {
            return bcContratoServicosSistemas ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1068ContratoServicosSistemas_ServicoSigla = "";
         A1068ContratoServicosSistemas_ServicoSigla = "";
         Z1064ContratoServicosSistemas_SistemaSigla = "";
         A1064ContratoServicosSistemas_SistemaSigla = "";
         BC00347_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         BC00347_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         BC00347_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         BC00347_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         BC00347_A160ContratoServicos_Codigo = new int[1] ;
         BC00347_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC00347_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC00344_A160ContratoServicos_Codigo = new int[1] ;
         BC00345_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         BC00345_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         BC00346_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         BC00346_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         BC00348_A160ContratoServicos_Codigo = new int[1] ;
         BC00348_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC00348_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC00343_A160ContratoServicos_Codigo = new int[1] ;
         BC00343_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC00343_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         sMode128 = "";
         BC00342_A160ContratoServicos_Codigo = new int[1] ;
         BC00342_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC00342_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC003411_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         BC003411_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         BC003412_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         BC003412_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         BC003413_A1068ContratoServicosSistemas_ServicoSigla = new String[] {""} ;
         BC003413_n1068ContratoServicosSistemas_ServicoSigla = new bool[] {false} ;
         BC003413_A1064ContratoServicosSistemas_SistemaSigla = new String[] {""} ;
         BC003413_n1064ContratoServicosSistemas_SistemaSigla = new bool[] {false} ;
         BC003413_A160ContratoServicos_Codigo = new int[1] ;
         BC003413_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC003413_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC003414_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicossistemas_bc__default(),
            new Object[][] {
                new Object[] {
               BC00342_A160ContratoServicos_Codigo, BC00342_A1067ContratoServicosSistemas_ServicoCod, BC00342_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC00343_A160ContratoServicos_Codigo, BC00343_A1067ContratoServicosSistemas_ServicoCod, BC00343_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC00344_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC00345_A1068ContratoServicosSistemas_ServicoSigla, BC00345_n1068ContratoServicosSistemas_ServicoSigla
               }
               , new Object[] {
               BC00346_A1064ContratoServicosSistemas_SistemaSigla, BC00346_n1064ContratoServicosSistemas_SistemaSigla
               }
               , new Object[] {
               BC00347_A1068ContratoServicosSistemas_ServicoSigla, BC00347_n1068ContratoServicosSistemas_ServicoSigla, BC00347_A1064ContratoServicosSistemas_SistemaSigla, BC00347_n1064ContratoServicosSistemas_SistemaSigla, BC00347_A160ContratoServicos_Codigo, BC00347_A1067ContratoServicosSistemas_ServicoCod, BC00347_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC00348_A160ContratoServicos_Codigo, BC00348_A1067ContratoServicosSistemas_ServicoCod, BC00348_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003411_A1068ContratoServicosSistemas_ServicoSigla, BC003411_n1068ContratoServicosSistemas_ServicoSigla
               }
               , new Object[] {
               BC003412_A1064ContratoServicosSistemas_SistemaSigla, BC003412_n1064ContratoServicosSistemas_SistemaSigla
               }
               , new Object[] {
               BC003413_A1068ContratoServicosSistemas_ServicoSigla, BC003413_n1068ContratoServicosSistemas_ServicoSigla, BC003413_A1064ContratoServicosSistemas_SistemaSigla, BC003413_n1064ContratoServicosSistemas_SistemaSigla, BC003413_A160ContratoServicos_Codigo, BC003413_A1067ContratoServicosSistemas_ServicoCod, BC003413_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC003414_A160ContratoServicos_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound128 ;
      private int trnEnded ;
      private int Z160ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int Z1067ContratoServicosSistemas_ServicoCod ;
      private int A1067ContratoServicosSistemas_ServicoCod ;
      private int Z1063ContratoServicosSistemas_SistemaCod ;
      private int A1063ContratoServicosSistemas_SistemaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1068ContratoServicosSistemas_ServicoSigla ;
      private String A1068ContratoServicosSistemas_ServicoSigla ;
      private String Z1064ContratoServicosSistemas_SistemaSigla ;
      private String A1064ContratoServicosSistemas_SistemaSigla ;
      private String sMode128 ;
      private bool n1068ContratoServicosSistemas_ServicoSigla ;
      private bool n1064ContratoServicosSistemas_SistemaSigla ;
      private SdtContratoServicosSistemas bcContratoServicosSistemas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00347_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] BC00347_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] BC00347_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] BC00347_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] BC00347_A160ContratoServicos_Codigo ;
      private int[] BC00347_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC00347_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC00344_A160ContratoServicos_Codigo ;
      private String[] BC00345_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] BC00345_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] BC00346_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] BC00346_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] BC00348_A160ContratoServicos_Codigo ;
      private int[] BC00348_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC00348_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC00343_A160ContratoServicos_Codigo ;
      private int[] BC00343_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC00343_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC00342_A160ContratoServicos_Codigo ;
      private int[] BC00342_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC00342_A1063ContratoServicosSistemas_SistemaCod ;
      private String[] BC003411_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] BC003411_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] BC003412_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] BC003412_n1064ContratoServicosSistemas_SistemaSigla ;
      private String[] BC003413_A1068ContratoServicosSistemas_ServicoSigla ;
      private bool[] BC003413_n1068ContratoServicosSistemas_ServicoSigla ;
      private String[] BC003413_A1064ContratoServicosSistemas_SistemaSigla ;
      private bool[] BC003413_n1064ContratoServicosSistemas_SistemaSigla ;
      private int[] BC003413_A160ContratoServicos_Codigo ;
      private int[] BC003413_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC003413_A1063ContratoServicosSistemas_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC003414_A160ContratoServicos_Codigo ;
   }

   public class contratoservicossistemas_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00347 ;
          prmBC00347 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00344 ;
          prmBC00344 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00345 ;
          prmBC00345 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00346 ;
          prmBC00346 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00348 ;
          prmBC00348 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00343 ;
          prmBC00343 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00342 ;
          prmBC00342 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00349 ;
          prmBC00349 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003410 ;
          prmBC003410 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003413 ;
          prmBC003413 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003414 ;
          prmBC003414 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003411 ;
          prmBC003411 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003412 ;
          prmBC003412 = new Object[] {
          new Object[] {"@ContratoServicosSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00342", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00342,1,0,true,false )
             ,new CursorDef("BC00343", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00343,1,0,true,false )
             ,new CursorDef("BC00344", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00344,1,0,true,false )
             ,new CursorDef("BC00345", "SELECT [Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosSistemas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00345,1,0,true,false )
             ,new CursorDef("BC00346", "SELECT [Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00346,1,0,true,false )
             ,new CursorDef("BC00347", "SELECT T2.[Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla, T3.[Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla, TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, TM1.[ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM (([ContratoServicosSistemas] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ContratoServicosSistemas_ServicoCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = TM1.[ContratoServicosSistemas_SistemaCod]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod and TM1.[ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod], TM1.[ContratoServicosSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00347,100,0,true,false )
             ,new CursorDef("BC00348", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, [ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00348,1,0,true,false )
             ,new CursorDef("BC00349", "INSERT INTO [ContratoServicosSistemas]([ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod]) VALUES(@ContratoServicos_Codigo, @ContratoServicosSistemas_ServicoCod, @ContratoServicosSistemas_SistemaCod)", GxErrorMask.GX_NOMASK,prmBC00349)
             ,new CursorDef("BC003410", "DELETE FROM [ContratoServicosSistemas]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod AND [ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod", GxErrorMask.GX_NOMASK,prmBC003410)
             ,new CursorDef("BC003411", "SELECT [Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosSistemas_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003411,1,0,true,false )
             ,new CursorDef("BC003412", "SELECT [Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoServicosSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003412,1,0,true,false )
             ,new CursorDef("BC003413", "SELECT T2.[Servico_Sigla] AS ContratoServicosSistemas_ServicoSigla, T3.[Sistema_Sigla] AS ContratoServicosSistemas_SistemaSigla, TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod] AS ContratoServicosSistemas_ServicoCod, TM1.[ContratoServicosSistemas_SistemaCod] AS ContratoServicosSistemas_SistemaCod FROM (([ContratoServicosSistemas] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ContratoServicosSistemas_ServicoCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = TM1.[ContratoServicosSistemas_SistemaCod]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosSistemas_ServicoCod] = @ContratoServicosSistemas_ServicoCod and TM1.[ContratoServicosSistemas_SistemaCod] = @ContratoServicosSistemas_SistemaCod ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosSistemas_ServicoCod], TM1.[ContratoServicosSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003413,100,0,true,false )
             ,new CursorDef("BC003414", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003414,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
