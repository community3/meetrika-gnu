/*
               File: Prc_AceiteEnviarEmailLote
        Description: Prc_Aceite Enviar Email Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:19:39.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_aceiteenviaremaillote : GXProcedure
   {
      public prc_aceiteenviaremaillote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_aceiteenviaremaillote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_LoteAceiteCod ,
                           decimal aP1_ContagemResultado_VlrAceite ,
                           short aP2_QtdeSelecionadas )
      {
         this.AV27ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         this.AV28ContagemResultado_VlrAceite = aP1_ContagemResultado_VlrAceite;
         this.AV33QtdeSelecionadas = aP2_QtdeSelecionadas;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_LoteAceiteCod ,
                                 decimal aP1_ContagemResultado_VlrAceite ,
                                 short aP2_QtdeSelecionadas )
      {
         prc_aceiteenviaremaillote objprc_aceiteenviaremaillote;
         objprc_aceiteenviaremaillote = new prc_aceiteenviaremaillote();
         objprc_aceiteenviaremaillote.AV27ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         objprc_aceiteenviaremaillote.AV28ContagemResultado_VlrAceite = aP1_ContagemResultado_VlrAceite;
         objprc_aceiteenviaremaillote.AV33QtdeSelecionadas = aP2_QtdeSelecionadas;
         objprc_aceiteenviaremaillote.context.SetSubmitInitialConfig(context);
         objprc_aceiteenviaremaillote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_aceiteenviaremaillote);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_aceiteenviaremaillote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV22WWPContext) ;
         AV39ParametrosSistema_URLApp = "";
         AV40ParametrosSistema_URLOtherVer = "";
         /* Using cursor P00BR2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1679ParametrosSistema_URLApp = P00BR2_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = P00BR2_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = P00BR2_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = P00BR2_n1952ParametrosSistema_URLOtherVer[0];
            A330ParametrosSistema_Codigo = P00BR2_A330ParametrosSistema_Codigo[0];
            AV39ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            AV40ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P00BR6 */
         pr_default.execute(1, new Object[] {AV27ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A596Lote_Codigo = P00BR6_A596Lote_Codigo[0];
            A562Lote_Numero = P00BR6_A562Lote_Numero[0];
            A564Lote_Data = P00BR6_A564Lote_Data[0];
            A1057Lote_ValorGlosas = P00BR6_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00BR6_n1057Lote_ValorGlosas[0];
            A1057Lote_ValorGlosas = P00BR6_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00BR6_n1057Lote_ValorGlosas[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            AV18Lote_Numero = A562Lote_Numero;
            AV17Lote_Data = A564Lote_Data;
            AV37Lote_Valor = A572Lote_Valor;
            AV38AuxLote_Valor = AV37Lote_Valor;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         AV14ContratoCollection.Clear();
         /* Using cursor P00BR7 */
         pr_default.execute(2, new Object[] {AV27ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00BR7_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00BR7_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P00BR7_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00BR7_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00BR7_A456ContagemResultado_Codigo[0];
            AV25AuxContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            /* Execute user subroutine: 'ADDCONTRATO' */
            S131 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV10Contrato_CodigoCollection.Count )
         {
            AV8AuxContrato_Codigo = (int)(AV10Contrato_CodigoCollection.GetNumeric(AV46GXV1));
            /* Using cursor P00BR8 */
            pr_default.execute(3, new Object[] {AV8AuxContrato_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A39Contratada_Codigo = P00BR8_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00BR8_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = P00BR8_A74Contrato_Codigo[0];
               A41Contratada_PessoaNom = P00BR8_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00BR8_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = P00BR8_A40Contratada_PessoaCod[0];
               A41Contratada_PessoaNom = P00BR8_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P00BR8_n41Contratada_PessoaNom[0];
               AV9Contrato_Codigo = A74Contrato_Codigo;
               new prc_contratovigencia(context ).execute(  AV9Contrato_Codigo, out  AV11Contrato_DataVigenciaInicio, out  AV12Contrato_DataVigenciaTermino) ;
               AV29Contratada_PessoaNom = A41Contratada_PessoaNom;
               /* Using cursor P00BR9 */
               pr_default.execute(4, new Object[] {AV8AuxContrato_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00BR9_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00BR9_A1079ContratoGestor_UsuarioCod[0];
                  AV19Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
                  /* Execute user subroutine: 'USUARIO' */
                  S121 ();
                  if ( returnInSub )
                  {
                     pr_default.close(4);
                     this.cleanup();
                     if (true) return;
                  }
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               AV19Usuario_Codigo = AV22WWPContext.gxTpr_Userid;
               /* Execute user subroutine: 'EVIAEMAIL' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  this.cleanup();
                  if (true) return;
               }
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'EVIAEMAIL' Routine */
         /* Using cursor P00BR11 */
         pr_default.execute(5, new Object[] {AV22WWPContext.gxTpr_Userid});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A40000Usuario_Nome = P00BR11_A40000Usuario_Nome[0];
         }
         else
         {
            A40000Usuario_Nome = "";
         }
         pr_default.close(5);
         AV30DataRegistro = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV20USuario_Nome = A40000Usuario_Nome;
         AV32EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + "Um lote de faturamento foi criado, segue abaixo os detalhes." + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Contrato: %1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Vig�ncia: %1 at� %2", context.localUtil.DToC( AV11Contrato_DataVigenciaInicio, 0, "-"), context.localUtil.DToC( AV12Contrato_DataVigenciaTermino, 0, "-"), "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Contratada: %1", AV29Contratada_PessoaNom, "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Contratante: %1", AV22WWPContext.gxTpr_Contratante_nomefantasia, "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "N�mero de Lote: %1", AV18Lote_Numero, "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Data de cria��o: %1", context.localUtil.Format( AV17Lote_Data, "99/99/99 99:99"), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Quantidade de demandas: %1", StringUtil.Str( (decimal)(AV33QtdeSelecionadas), 4, 0), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.Format( "Valor do Lote: %1", StringUtil.Trim( StringUtil.Str( AV38AuxLote_Valor, 18, 2)), "", "", "", "", "", "", "", "") + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + "Para outras informa��es, acesse o sistema MEETRIKA - Sistema de Gest�o da Execu��o Contratual." + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + "link: " + (!String.IsNullOrEmpty(StringUtil.RTrim( AV39ParametrosSistema_URLApp)) ? AV39ParametrosSistema_URLApp : (!String.IsNullOrEmpty(StringUtil.RTrim( AV40ParametrosSistema_URLOtherVer)) ? AV40ParametrosSistema_URLOtherVer : "N�o Informado")) + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.NewLine( );
         AV23AreaTrabalho_Codigo = AV22WWPContext.gxTpr_Areatrabalho_codigo;
         AV32EmailText = AV32EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV22WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV22WWPContext.gxTpr_Username) + " - " + AV20USuario_Nome + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
         AV35Subject = StringUtil.Format( "Lote Faturamento %1 - %2 - Criado (No reply)", StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)), StringUtil.Trim( AV13Contrato_Numero), "", "", "", "", "", "", "");
         AV21WebSession.Set("SendTo", AV36Usuarios.ToXml(false, true, "Collection", ""));
         new prc_enviaremailsemnotificacao(context ).execute(  AV23AreaTrabalho_Codigo,  AV35Subject,  AV32EmailText,  AV24Attachments, ref  AV34Resultado) ;
         AV21WebSession.Remove("SendTo");
      }

      protected void S121( )
      {
         /* 'USUARIO' Routine */
         /* Using cursor P00BR12 */
         pr_default.execute(6, new Object[] {AV19Usuario_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1Usuario_Codigo = P00BR12_A1Usuario_Codigo[0];
            A1647Usuario_Email = P00BR12_A1647Usuario_Email[0];
            n1647Usuario_Email = P00BR12_n1647Usuario_Email[0];
            A341Usuario_UserGamGuid = P00BR12_A341Usuario_UserGamGuid[0];
            if ( P00BR12_n1647Usuario_Email[0] || String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) )
            {
               AV15GamUser.load( A341Usuario_UserGamGuid);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15GamUser.gxTpr_Email)) )
               {
                  AV36Usuarios.Add(A1Usuario_Codigo, 0);
               }
            }
            else
            {
               AV36Usuarios.Add(A1Usuario_Codigo, 0);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
      }

      protected void S131( )
      {
         /* 'ADDCONTRATO' Routine */
         /* Using cursor P00BR13 */
         pr_default.execute(7, new Object[] {AV25AuxContratoServicos_Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A160ContratoServicos_Codigo = P00BR13_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = P00BR13_A74Contrato_Codigo[0];
            AV8AuxContrato_Codigo = A74Contrato_Codigo;
            /* Execute user subroutine: 'VRFDUP' */
            S148 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            if ( ! AV16isDuplicado )
            {
               AV10Contrato_CodigoCollection.Add(AV8AuxContrato_Codigo, 0);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
      }

      protected void S148( )
      {
         /* 'VRFDUP' Routine */
         AV16isDuplicado = false;
         AV51GXV2 = 1;
         while ( AV51GXV2 <= AV10Contrato_CodigoCollection.Count )
         {
            AV9Contrato_Codigo = (int)(AV10Contrato_CodigoCollection.GetNumeric(AV51GXV2));
            if ( AV9Contrato_Codigo == AV8AuxContrato_Codigo )
            {
               AV16isDuplicado = true;
            }
            else
            {
               AV16isDuplicado = false;
            }
            AV51GXV2 = (int)(AV51GXV2+1);
         }
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor P00BR14 */
         pr_default.execute(8, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(8) != 101) && ( P00BR14_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P00BR14_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P00BR14_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV39ParametrosSistema_URLApp = "";
         AV40ParametrosSistema_URLOtherVer = "";
         scmdbuf = "";
         P00BR2_A1679ParametrosSistema_URLApp = new String[] {""} ;
         P00BR2_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         P00BR2_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         P00BR2_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         P00BR2_A330ParametrosSistema_Codigo = new int[1] ;
         A1679ParametrosSistema_URLApp = "";
         A1952ParametrosSistema_URLOtherVer = "";
         P00BR6_A596Lote_Codigo = new int[1] ;
         P00BR6_A562Lote_Numero = new String[] {""} ;
         P00BR6_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00BR6_A1057Lote_ValorGlosas = new decimal[1] ;
         P00BR6_n1057Lote_ValorGlosas = new bool[] {false} ;
         A562Lote_Numero = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         AV18Lote_Numero = "";
         AV17Lote_Data = (DateTime)(DateTime.MinValue);
         AV14ContratoCollection = new GxObjectCollection( context, "Contrato", "GxEv3Up14_Meetrika", "SdtContrato", "GeneXus.Programs");
         P00BR7_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00BR7_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00BR7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00BR7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00BR7_A456ContagemResultado_Codigo = new int[1] ;
         AV10Contrato_CodigoCollection = new GxSimpleCollection();
         P00BR8_A39Contratada_Codigo = new int[1] ;
         P00BR8_A40Contratada_PessoaCod = new int[1] ;
         P00BR8_A74Contrato_Codigo = new int[1] ;
         P00BR8_A41Contratada_PessoaNom = new String[] {""} ;
         P00BR8_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         AV11Contrato_DataVigenciaInicio = DateTime.MinValue;
         AV12Contrato_DataVigenciaTermino = DateTime.MinValue;
         AV29Contratada_PessoaNom = "";
         P00BR9_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00BR9_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00BR11_A40000Usuario_Nome = new String[] {""} ;
         A40000Usuario_Nome = "";
         AV30DataRegistro = (DateTime)(DateTime.MinValue);
         AV20USuario_Nome = "";
         AV32EmailText = "";
         AV35Subject = "";
         AV13Contrato_Numero = "";
         AV21WebSession = context.GetSession();
         AV36Usuarios = new GxSimpleCollection();
         AV24Attachments = new GxSimpleCollection();
         AV34Resultado = "";
         P00BR12_A1Usuario_Codigo = new int[1] ;
         P00BR12_A1647Usuario_Email = new String[] {""} ;
         P00BR12_n1647Usuario_Email = new bool[] {false} ;
         P00BR12_A341Usuario_UserGamGuid = new String[] {""} ;
         A1647Usuario_Email = "";
         A341Usuario_UserGamGuid = "";
         AV15GamUser = new SdtGAMUser(context);
         P00BR13_A160ContratoServicos_Codigo = new int[1] ;
         P00BR13_A74Contrato_Codigo = new int[1] ;
         P00BR14_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00BR14_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00BR14_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00BR14_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00BR14_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_aceiteenviaremaillote__default(),
            new Object[][] {
                new Object[] {
               P00BR2_A1679ParametrosSistema_URLApp, P00BR2_n1679ParametrosSistema_URLApp, P00BR2_A1952ParametrosSistema_URLOtherVer, P00BR2_n1952ParametrosSistema_URLOtherVer, P00BR2_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               P00BR6_A596Lote_Codigo, P00BR6_A562Lote_Numero, P00BR6_A564Lote_Data, P00BR6_A1057Lote_ValorGlosas, P00BR6_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P00BR7_A597ContagemResultado_LoteAceiteCod, P00BR7_n597ContagemResultado_LoteAceiteCod, P00BR7_A1553ContagemResultado_CntSrvCod, P00BR7_n1553ContagemResultado_CntSrvCod, P00BR7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00BR8_A39Contratada_Codigo, P00BR8_A40Contratada_PessoaCod, P00BR8_A74Contrato_Codigo, P00BR8_A41Contratada_PessoaNom, P00BR8_n41Contratada_PessoaNom
               }
               , new Object[] {
               P00BR9_A1078ContratoGestor_ContratoCod, P00BR9_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P00BR11_A40000Usuario_Nome
               }
               , new Object[] {
               P00BR12_A1Usuario_Codigo, P00BR12_A1647Usuario_Email, P00BR12_n1647Usuario_Email, P00BR12_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00BR13_A160ContratoServicos_Codigo, P00BR13_A74Contrato_Codigo
               }
               , new Object[] {
               P00BR14_A597ContagemResultado_LoteAceiteCod, P00BR14_n597ContagemResultado_LoteAceiteCod, P00BR14_A512ContagemResultado_ValorPF, P00BR14_n512ContagemResultado_ValorPF, P00BR14_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33QtdeSelecionadas ;
      private int AV27ContagemResultado_LoteAceiteCod ;
      private int A330ParametrosSistema_Codigo ;
      private int A596Lote_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV25AuxContratoServicos_Codigo ;
      private int AV46GXV1 ;
      private int AV8AuxContrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A74Contrato_Codigo ;
      private int AV9Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV19Usuario_Codigo ;
      private int AV23AreaTrabalho_Codigo ;
      private int A1Usuario_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV51GXV2 ;
      private decimal AV28ContagemResultado_VlrAceite ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A572Lote_Valor ;
      private decimal A1058Lote_ValorOSs ;
      private decimal AV37Lote_Valor ;
      private decimal AV38AuxLote_Valor ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String scmdbuf ;
      private String A562Lote_Numero ;
      private String AV18Lote_Numero ;
      private String A41Contratada_PessoaNom ;
      private String AV29Contratada_PessoaNom ;
      private String A40000Usuario_Nome ;
      private String AV20USuario_Nome ;
      private String AV32EmailText ;
      private String AV35Subject ;
      private String AV13Contrato_Numero ;
      private String AV34Resultado ;
      private String A341Usuario_UserGamGuid ;
      private DateTime A564Lote_Data ;
      private DateTime AV17Lote_Data ;
      private DateTime AV30DataRegistro ;
      private DateTime AV11Contrato_DataVigenciaInicio ;
      private DateTime AV12Contrato_DataVigenciaTermino ;
      private bool n1679ParametrosSistema_URLApp ;
      private bool n1952ParametrosSistema_URLOtherVer ;
      private bool n1057Lote_ValorGlosas ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool n1647Usuario_Email ;
      private bool AV16isDuplicado ;
      private String AV39ParametrosSistema_URLApp ;
      private String AV40ParametrosSistema_URLOtherVer ;
      private String A1679ParametrosSistema_URLApp ;
      private String A1952ParametrosSistema_URLOtherVer ;
      private String A1647Usuario_Email ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV36Usuarios ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00BR2_A1679ParametrosSistema_URLApp ;
      private bool[] P00BR2_n1679ParametrosSistema_URLApp ;
      private String[] P00BR2_A1952ParametrosSistema_URLOtherVer ;
      private bool[] P00BR2_n1952ParametrosSistema_URLOtherVer ;
      private int[] P00BR2_A330ParametrosSistema_Codigo ;
      private int[] P00BR6_A596Lote_Codigo ;
      private String[] P00BR6_A562Lote_Numero ;
      private DateTime[] P00BR6_A564Lote_Data ;
      private decimal[] P00BR6_A1057Lote_ValorGlosas ;
      private bool[] P00BR6_n1057Lote_ValorGlosas ;
      private int[] P00BR7_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00BR7_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00BR7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00BR7_n1553ContagemResultado_CntSrvCod ;
      private int[] P00BR7_A456ContagemResultado_Codigo ;
      private int[] P00BR8_A39Contratada_Codigo ;
      private int[] P00BR8_A40Contratada_PessoaCod ;
      private int[] P00BR8_A74Contrato_Codigo ;
      private String[] P00BR8_A41Contratada_PessoaNom ;
      private bool[] P00BR8_n41Contratada_PessoaNom ;
      private int[] P00BR9_A1078ContratoGestor_ContratoCod ;
      private int[] P00BR9_A1079ContratoGestor_UsuarioCod ;
      private String[] P00BR11_A40000Usuario_Nome ;
      private int[] P00BR12_A1Usuario_Codigo ;
      private String[] P00BR12_A1647Usuario_Email ;
      private bool[] P00BR12_n1647Usuario_Email ;
      private String[] P00BR12_A341Usuario_UserGamGuid ;
      private int[] P00BR13_A160ContratoServicos_Codigo ;
      private int[] P00BR13_A74Contrato_Codigo ;
      private int[] P00BR14_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00BR14_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00BR14_A512ContagemResultado_ValorPF ;
      private bool[] P00BR14_n512ContagemResultado_ValorPF ;
      private int[] P00BR14_A456ContagemResultado_Codigo ;
      private IGxSession AV21WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV10Contrato_CodigoCollection ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24Attachments ;
      [ObjectCollection(ItemType=typeof( SdtContrato ))]
      private IGxCollection AV14ContratoCollection ;
      private SdtGAMUser AV15GamUser ;
      private wwpbaseobjects.SdtWWPContext AV22WWPContext ;
   }

   public class prc_aceiteenviaremaillote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BR2 ;
          prmP00BR2 = new Object[] {
          } ;
          Object[] prmP00BR6 ;
          prmP00BR6 = new Object[] {
          new Object[] {"@AV27ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR7 ;
          prmP00BR7 = new Object[] {
          new Object[] {"@AV27ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR8 ;
          prmP00BR8 = new Object[] {
          new Object[] {"@AV8AuxContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR9 ;
          prmP00BR9 = new Object[] {
          new Object[] {"@AV8AuxContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR11 ;
          prmP00BR11 = new Object[] {
          new Object[] {"@AV22WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00BR12 ;
          prmP00BR12 = new Object[] {
          new Object[] {"@AV19Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR13 ;
          prmP00BR13 = new Object[] {
          new Object[] {"@AV25AuxContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BR14 ;
          prmP00BR14 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BR2", "SELECT TOP 1 [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR2,1,0,false,true )
             ,new CursorDef("P00BR6", "SELECT T1.[Lote_Codigo], T1.[Lote_Numero], T1.[Lote_Data], COALESCE( T2.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM ([Lote] T1 WITH (NOLOCK) INNER JOIN (SELECT COALESCE( T5.[GXC6], 0) + COALESCE( T4.[GXC7], 0) AS Lote_ValorGlosas, T3.[Lote_Codigo] FROM (([Lote] T3 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T6.[ContagemResultadoIndicadores_Valor]) AS GXC7, T7.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T6 WITH (NOLOCK),  [Lote] T7 WITH (NOLOCK) WHERE T6.[ContagemResultadoIndicadores_LoteCod] = T7.[Lote_Codigo] GROUP BY T7.[Lote_Codigo] ) T4 ON T4.[Lote_Codigo] = T3.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC6, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T3.[Lote_Codigo]) ) T2 ON T2.[Lote_Codigo] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV27ContagemResultado_LoteAceiteCod ORDER BY T1.[Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR6,1,0,true,true )
             ,new CursorDef("P00BR7", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_CntSrvCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @AV27ContagemResultado_LoteAceiteCod ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR7,100,0,true,false )
             ,new CursorDef("P00BR8", "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T3.[Pessoa_Nome] AS Contratada_PessoaNom FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV8AuxContrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR8,1,0,true,true )
             ,new CursorDef("P00BR9", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV8AuxContrato_Codigo ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR9,100,0,true,false )
             ,new CursorDef("P00BR11", "SELECT COALESCE( T1.[Usuario_Nome], '') AS Usuario_Nome FROM (SELECT [Usuario_Nome] AS Usuario_Nome, [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV22WWPContext__Userid ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR11,1,0,true,true )
             ,new CursorDef("P00BR12", "SELECT [Usuario_Codigo], [Usuario_Email], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV19Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR12,1,0,true,true )
             ,new CursorDef("P00BR13", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV25AuxContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR13,1,0,true,true )
             ,new CursorDef("P00BR14", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BR14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 40) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
