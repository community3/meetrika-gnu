/*
               File: type_SdtGAMUpdateRepositoryConfiguration
        Description: GAMUpdateRepositoryConfiguration
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:41.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMUpdateRepositoryConfiguration : GxUserType, IGxExternalObject
   {
      public SdtGAMUpdateRepositoryConfiguration( )
      {
         initialize();
      }

      public SdtGAMUpdateRepositoryConfiguration( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMUpdateRepositoryConfiguration_externalReference == null )
         {
            GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
         }
         returntostring = "";
         returntostring = (String)(GAMUpdateRepositoryConfiguration_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Packagedirectorypath
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            return GAMUpdateRepositoryConfiguration_externalReference.PackageDirectoryPath ;
         }

         set {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            GAMUpdateRepositoryConfiguration_externalReference.PackageDirectoryPath = value;
         }

      }

      public bool gxTpr_Fullimport
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            return GAMUpdateRepositoryConfiguration_externalReference.FullImport ;
         }

         set {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            GAMUpdateRepositoryConfiguration_externalReference.FullImport = value;
         }

      }

      public bool gxTpr_Importallroles
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            return GAMUpdateRepositoryConfiguration_externalReference.ImportAllRoles ;
         }

         set {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            GAMUpdateRepositoryConfiguration_externalReference.ImportAllRoles = value;
         }

      }

      public bool gxTpr_Importallapplications
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            return GAMUpdateRepositoryConfiguration_externalReference.ImportAllApplications ;
         }

         set {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            GAMUpdateRepositoryConfiguration_externalReference.ImportAllApplications = value;
         }

      }

      public IGxCollection gxTpr_Applicationstoimport
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMUpdateRepositoryConfigurationApplicationsToImport", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport> externalParm0 ;
            externalParm0 = GAMUpdateRepositoryConfiguration_externalReference.ApplicationsToImport;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport>), intValue.ExternalInstance);
            GAMUpdateRepositoryConfiguration_externalReference.ApplicationsToImport = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMUpdateRepositoryConfiguration_externalReference == null )
            {
               GAMUpdateRepositoryConfiguration_externalReference = new Artech.Security.GAMUpdateRepositoryConfiguration(context);
            }
            return GAMUpdateRepositoryConfiguration_externalReference ;
         }

         set {
            GAMUpdateRepositoryConfiguration_externalReference = (Artech.Security.GAMUpdateRepositoryConfiguration)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMUpdateRepositoryConfiguration GAMUpdateRepositoryConfiguration_externalReference=null ;
   }

}
