/*
               File: PRC_UsuarioEmailGET
        Description: Usuario Email GET
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:48.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioemailget : GXProcedure
   {
      public prc_usuarioemailget( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioemailget( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Usuario_UserGamGuid ,
                           out String aP1_Email )
      {
         this.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV9Email = "" ;
         initialize();
         executePrivate();
         aP1_Email=this.AV9Email;
      }

      public String executeUdp( String aP0_Usuario_UserGamGuid )
      {
         this.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         this.AV9Email = "" ;
         initialize();
         executePrivate();
         aP1_Email=this.AV9Email;
         return AV9Email ;
      }

      public void executeSubmit( String aP0_Usuario_UserGamGuid ,
                                 out String aP1_Email )
      {
         prc_usuarioemailget objprc_usuarioemailget;
         objprc_usuarioemailget = new prc_usuarioemailget();
         objprc_usuarioemailget.AV10Usuario_UserGamGuid = aP0_Usuario_UserGamGuid;
         objprc_usuarioemailget.AV9Email = "" ;
         objprc_usuarioemailget.context.SetSubmitInitialConfig(context);
         objprc_usuarioemailget.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioemailget);
         aP1_Email=this.AV9Email;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioemailget)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8GamUser.load( AV10Usuario_UserGamGuid);
         AV9Email = AV8GamUser.gxTpr_Email;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8GamUser = new SdtGAMUser(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV10Usuario_UserGamGuid ;
      private String AV9Email ;
      private String aP1_Email ;
      private SdtGAMUser AV8GamUser ;
   }

}
