/*
               File: ExportReportWWSistema
        Description: Export Report WWSistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:30.52
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportwwsistema : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV60TFSistema_Sigla = GetNextPar( );
                  AV61TFSistema_Sigla_Sel = GetNextPar( );
                  AV64TFSistema_Tecnica_SelsJson = GetNextPar( );
                  AV68TFSistema_Coordenacao = GetNextPar( );
                  AV69TFSistema_Coordenacao_Sel = GetNextPar( );
                  AV70TFAmbienteTecnologico_Descricao = GetNextPar( );
                  AV71TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
                  AV72TFMetodologia_Descricao = GetNextPar( );
                  AV73TFMetodologia_Descricao_Sel = GetNextPar( );
                  AV78TFSistema_ProjetoNome = GetNextPar( );
                  AV79TFSistema_ProjetoNome_Sel = GetNextPar( );
                  AV84TFSistemaVersao_Id = GetNextPar( );
                  AV85TFSistemaVersao_Id_Sel = GetNextPar( );
                  AV80TFSistema_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV22GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public exportreportwwsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportreportwwsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_AreaTrabalhoCod ,
                           String aP1_TFSistema_Sigla ,
                           String aP2_TFSistema_Sigla_Sel ,
                           String aP3_TFSistema_Tecnica_SelsJson ,
                           String aP4_TFSistema_Coordenacao ,
                           String aP5_TFSistema_Coordenacao_Sel ,
                           String aP6_TFAmbienteTecnologico_Descricao ,
                           String aP7_TFAmbienteTecnologico_Descricao_Sel ,
                           String aP8_TFMetodologia_Descricao ,
                           String aP9_TFMetodologia_Descricao_Sel ,
                           String aP10_TFSistema_ProjetoNome ,
                           String aP11_TFSistema_ProjetoNome_Sel ,
                           String aP12_TFSistemaVersao_Id ,
                           String aP13_TFSistemaVersao_Id_Sel ,
                           short aP14_TFSistema_Ativo_Sel ,
                           short aP15_OrderedBy ,
                           bool aP16_OrderedDsc ,
                           String aP17_GridStateXML )
      {
         this.AV12Sistema_AreaTrabalhoCod = aP0_Sistema_AreaTrabalhoCod;
         this.AV60TFSistema_Sigla = aP1_TFSistema_Sigla;
         this.AV61TFSistema_Sigla_Sel = aP2_TFSistema_Sigla_Sel;
         this.AV64TFSistema_Tecnica_SelsJson = aP3_TFSistema_Tecnica_SelsJson;
         this.AV68TFSistema_Coordenacao = aP4_TFSistema_Coordenacao;
         this.AV69TFSistema_Coordenacao_Sel = aP5_TFSistema_Coordenacao_Sel;
         this.AV70TFAmbienteTecnologico_Descricao = aP6_TFAmbienteTecnologico_Descricao;
         this.AV71TFAmbienteTecnologico_Descricao_Sel = aP7_TFAmbienteTecnologico_Descricao_Sel;
         this.AV72TFMetodologia_Descricao = aP8_TFMetodologia_Descricao;
         this.AV73TFMetodologia_Descricao_Sel = aP9_TFMetodologia_Descricao_Sel;
         this.AV78TFSistema_ProjetoNome = aP10_TFSistema_ProjetoNome;
         this.AV79TFSistema_ProjetoNome_Sel = aP11_TFSistema_ProjetoNome_Sel;
         this.AV84TFSistemaVersao_Id = aP12_TFSistemaVersao_Id;
         this.AV85TFSistemaVersao_Id_Sel = aP13_TFSistemaVersao_Id_Sel;
         this.AV80TFSistema_Ativo_Sel = aP14_TFSistema_Ativo_Sel;
         this.AV10OrderedBy = aP15_OrderedBy;
         this.AV11OrderedDsc = aP16_OrderedDsc;
         this.AV22GridStateXML = aP17_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Sistema_AreaTrabalhoCod ,
                                 String aP1_TFSistema_Sigla ,
                                 String aP2_TFSistema_Sigla_Sel ,
                                 String aP3_TFSistema_Tecnica_SelsJson ,
                                 String aP4_TFSistema_Coordenacao ,
                                 String aP5_TFSistema_Coordenacao_Sel ,
                                 String aP6_TFAmbienteTecnologico_Descricao ,
                                 String aP7_TFAmbienteTecnologico_Descricao_Sel ,
                                 String aP8_TFMetodologia_Descricao ,
                                 String aP9_TFMetodologia_Descricao_Sel ,
                                 String aP10_TFSistema_ProjetoNome ,
                                 String aP11_TFSistema_ProjetoNome_Sel ,
                                 String aP12_TFSistemaVersao_Id ,
                                 String aP13_TFSistemaVersao_Id_Sel ,
                                 short aP14_TFSistema_Ativo_Sel ,
                                 short aP15_OrderedBy ,
                                 bool aP16_OrderedDsc ,
                                 String aP17_GridStateXML )
      {
         exportreportwwsistema objexportreportwwsistema;
         objexportreportwwsistema = new exportreportwwsistema();
         objexportreportwwsistema.AV12Sistema_AreaTrabalhoCod = aP0_Sistema_AreaTrabalhoCod;
         objexportreportwwsistema.AV60TFSistema_Sigla = aP1_TFSistema_Sigla;
         objexportreportwwsistema.AV61TFSistema_Sigla_Sel = aP2_TFSistema_Sigla_Sel;
         objexportreportwwsistema.AV64TFSistema_Tecnica_SelsJson = aP3_TFSistema_Tecnica_SelsJson;
         objexportreportwwsistema.AV68TFSistema_Coordenacao = aP4_TFSistema_Coordenacao;
         objexportreportwwsistema.AV69TFSistema_Coordenacao_Sel = aP5_TFSistema_Coordenacao_Sel;
         objexportreportwwsistema.AV70TFAmbienteTecnologico_Descricao = aP6_TFAmbienteTecnologico_Descricao;
         objexportreportwwsistema.AV71TFAmbienteTecnologico_Descricao_Sel = aP7_TFAmbienteTecnologico_Descricao_Sel;
         objexportreportwwsistema.AV72TFMetodologia_Descricao = aP8_TFMetodologia_Descricao;
         objexportreportwwsistema.AV73TFMetodologia_Descricao_Sel = aP9_TFMetodologia_Descricao_Sel;
         objexportreportwwsistema.AV78TFSistema_ProjetoNome = aP10_TFSistema_ProjetoNome;
         objexportreportwwsistema.AV79TFSistema_ProjetoNome_Sel = aP11_TFSistema_ProjetoNome_Sel;
         objexportreportwwsistema.AV84TFSistemaVersao_Id = aP12_TFSistemaVersao_Id;
         objexportreportwwsistema.AV85TFSistemaVersao_Id_Sel = aP13_TFSistemaVersao_Id_Sel;
         objexportreportwwsistema.AV80TFSistema_Ativo_Sel = aP14_TFSistema_Ativo_Sel;
         objexportreportwwsistema.AV10OrderedBy = aP15_OrderedBy;
         objexportreportwwsistema.AV11OrderedDsc = aP16_OrderedDsc;
         objexportreportwwsistema.AV22GridStateXML = aP17_GridStateXML;
         objexportreportwwsistema.context.SetSubmitInitialConfig(context);
         objexportreportwwsistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportwwsistema);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportwwsistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H4X0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         H4X0( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Relat�rio de Sistemas", 5, Gx_line+5, 785, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( ! (0==AV12Sistema_AreaTrabalhoCod) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("C�d. �rea Trabalho", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Sistema_AreaTrabalhoCod), "ZZZZZ9")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV23GridState.gxTpr_Dynamicfilters.FromXml(AV22GridStateXML, "");
         if ( AV23GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV24GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV23GridState.gxTpr_Dynamicfilters.Item(1));
            AV13DynamicFiltersSelector1 = AV24GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
            {
               AV15Sistema_Nome1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15Sistema_Nome1)) )
               {
                  AV26Sistema_Nome = AV15Sistema_Nome1;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Nome", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV26Sistema_Nome, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
            {
               AV14Sistema_Sigla1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14Sistema_Sigla1)) )
               {
                  AV25Sistema_Sigla = AV14Sistema_Sigla1;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Sigla", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25Sistema_Sigla, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_TIPO") == 0 )
            {
               AV33Sistema_Tipo1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Sistema_Tipo1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV33Sistema_Tipo1), "D") == 0 )
                  {
                     AV35FilterSistema_Tipo1ValueDescription = "Desenvolvimento";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV33Sistema_Tipo1), "M") == 0 )
                  {
                     AV35FilterSistema_Tipo1ValueDescription = "Melhoria";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV33Sistema_Tipo1), "A") == 0 )
                  {
                     AV35FilterSistema_Tipo1ValueDescription = "Aplica��o";
                  }
                  AV34FilterSistema_TipoValueDescription = AV35FilterSistema_Tipo1ValueDescription;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Tipo", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34FilterSistema_TipoValueDescription, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_TECNICA") == 0 )
            {
               AV40Sistema_Tecnica1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Sistema_Tecnica1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV40Sistema_Tecnica1), "I") == 0 )
                  {
                     AV42FilterSistema_Tecnica1ValueDescription = "Indicativa";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV40Sistema_Tecnica1), "E") == 0 )
                  {
                     AV42FilterSistema_Tecnica1ValueDescription = "Estimada";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV40Sistema_Tecnica1), "D") == 0 )
                  {
                     AV42FilterSistema_Tecnica1ValueDescription = "Detalhada";
                  }
                  AV41FilterSistema_TecnicaValueDescription = AV42FilterSistema_Tecnica1ValueDescription;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("T�cnica", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV41FilterSistema_TecnicaValueDescription, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
            {
               AV43Sistema_Coordenacao1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Sistema_Coordenacao1)) )
               {
                  AV50Sistema_Coordenacao = AV43Sistema_Coordenacao1;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV50Sistema_Coordenacao, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_PROJETONOME") == 0 )
            {
               AV51Sistema_ProjetoNome1 = AV24GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Sistema_ProjetoNome1)) )
               {
                  AV53Sistema_ProjetoNome = AV51Sistema_ProjetoNome1;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Projeto", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Sistema_ProjetoNome, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV13DynamicFiltersSelector1, "SISTEMA_PF") == 0 )
            {
               AV39DynamicFiltersOperator1 = AV24GridStateDynamicFilter.gxTpr_Operator;
               AV54Sistema_PF1 = NumberUtil.Val( AV24GridStateDynamicFilter.gxTpr_Value, ".");
               if ( ! (Convert.ToDecimal(0)==AV54Sistema_PF1) )
               {
                  if ( AV39DynamicFiltersOperator1 == 0 )
                  {
                     AV56FilterSistema_PFDescription = "PF.B (<)";
                  }
                  else if ( AV39DynamicFiltersOperator1 == 1 )
                  {
                     AV56FilterSistema_PFDescription = "PF.B (=)";
                  }
                  else if ( AV39DynamicFiltersOperator1 == 2 )
                  {
                     AV56FilterSistema_PFDescription = "PF.B (>)";
                  }
                  AV57Sistema_PF = AV54Sistema_PF1;
                  H4X0( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV56FilterSistema_PFDescription, "")), 5, Gx_line+2, 108, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV57Sistema_PF, "ZZ,ZZZ,ZZ9.999")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV23GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV17DynamicFiltersEnabled2 = true;
               AV24GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV23GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV24GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_NOME") == 0 )
               {
                  AV20Sistema_Nome2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Sistema_Nome2)) )
                  {
                     AV26Sistema_Nome = AV20Sistema_Nome2;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Nome", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV26Sistema_Nome, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
               {
                  AV19Sistema_Sigla2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Sistema_Sigla2)) )
                  {
                     AV25Sistema_Sigla = AV19Sistema_Sigla2;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Sigla", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25Sistema_Sigla, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_TIPO") == 0 )
               {
                  AV36Sistema_Tipo2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Sistema_Tipo2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV36Sistema_Tipo2), "D") == 0 )
                     {
                        AV37FilterSistema_Tipo2ValueDescription = "Desenvolvimento";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV36Sistema_Tipo2), "M") == 0 )
                     {
                        AV37FilterSistema_Tipo2ValueDescription = "Melhoria";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV36Sistema_Tipo2), "A") == 0 )
                     {
                        AV37FilterSistema_Tipo2ValueDescription = "Aplica��o";
                     }
                     AV34FilterSistema_TipoValueDescription = AV37FilterSistema_Tipo2ValueDescription;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Tipo", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34FilterSistema_TipoValueDescription, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_TECNICA") == 0 )
               {
                  AV45Sistema_Tecnica2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Sistema_Tecnica2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV45Sistema_Tecnica2), "I") == 0 )
                     {
                        AV46FilterSistema_Tecnica2ValueDescription = "Indicativa";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV45Sistema_Tecnica2), "E") == 0 )
                     {
                        AV46FilterSistema_Tecnica2ValueDescription = "Estimada";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV45Sistema_Tecnica2), "D") == 0 )
                     {
                        AV46FilterSistema_Tecnica2ValueDescription = "Detalhada";
                     }
                     AV41FilterSistema_TecnicaValueDescription = AV46FilterSistema_Tecnica2ValueDescription;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("T�cnica", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV41FilterSistema_TecnicaValueDescription, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
               {
                  AV47Sistema_Coordenacao2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Sistema_Coordenacao2)) )
                  {
                     AV50Sistema_Coordenacao = AV47Sistema_Coordenacao2;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV50Sistema_Coordenacao, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_PROJETONOME") == 0 )
               {
                  AV52Sistema_ProjetoNome2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Sistema_ProjetoNome2)) )
                  {
                     AV53Sistema_ProjetoNome = AV52Sistema_ProjetoNome2;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Projeto", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Sistema_ProjetoNome, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "SISTEMA_PF") == 0 )
               {
                  AV44DynamicFiltersOperator2 = AV24GridStateDynamicFilter.gxTpr_Operator;
                  AV55Sistema_PF2 = NumberUtil.Val( AV24GridStateDynamicFilter.gxTpr_Value, ".");
                  if ( ! (Convert.ToDecimal(0)==AV55Sistema_PF2) )
                  {
                     if ( AV44DynamicFiltersOperator2 == 0 )
                     {
                        AV56FilterSistema_PFDescription = "PF.B (<)";
                     }
                     else if ( AV44DynamicFiltersOperator2 == 1 )
                     {
                        AV56FilterSistema_PFDescription = "PF.B (=)";
                     }
                     else if ( AV44DynamicFiltersOperator2 == 2 )
                     {
                        AV56FilterSistema_PFDescription = "PF.B (>)";
                     }
                     AV57Sistema_PF = AV55Sistema_PF2;
                     H4X0( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV56FilterSistema_PFDescription, "")), 5, Gx_line+2, 108, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV57Sistema_PF, "ZZ,ZZZ,ZZ9.999")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFSistema_Sigla_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sigla", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV61TFSistema_Sigla_Sel, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFSistema_Sigla)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Sigla", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV60TFSistema_Sigla, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         AV66TFSistema_Tecnica_Sels.FromJSonString(AV64TFSistema_Tecnica_SelsJson);
         if ( ! ( AV66TFSistema_Tecnica_Sels.Count == 0 ) )
         {
            AV83i = 1;
            AV88GXV1 = 1;
            while ( AV88GXV1 <= AV66TFSistema_Tecnica_Sels.Count )
            {
               AV67TFSistema_Tecnica_Sel = AV66TFSistema_Tecnica_Sels.GetString(AV88GXV1);
               if ( AV83i == 1 )
               {
                  AV65TFSistema_Tecnica_SelDscs = "";
               }
               else
               {
                  AV65TFSistema_Tecnica_SelDscs = AV65TFSistema_Tecnica_SelDscs + ", ";
               }
               if ( StringUtil.StrCmp(StringUtil.Trim( AV67TFSistema_Tecnica_Sel), "I") == 0 )
               {
                  AV81FilterTFSistema_Tecnica_SelValueDescription = "Indicativa";
               }
               else if ( StringUtil.StrCmp(StringUtil.Trim( AV67TFSistema_Tecnica_Sel), "E") == 0 )
               {
                  AV81FilterTFSistema_Tecnica_SelValueDescription = "Estimada";
               }
               else if ( StringUtil.StrCmp(StringUtil.Trim( AV67TFSistema_Tecnica_Sel), "D") == 0 )
               {
                  AV81FilterTFSistema_Tecnica_SelValueDescription = "Detalhada";
               }
               AV65TFSistema_Tecnica_SelDscs = AV65TFSistema_Tecnica_SelDscs + AV81FilterTFSistema_Tecnica_SelValueDescription;
               AV83i = (long)(AV83i+1);
               AV88GXV1 = (int)(AV88GXV1+1);
            }
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("T�cnica", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV65TFSistema_Tecnica_SelDscs, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFSistema_Coordenacao_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV69TFSistema_Coordenacao_Sel, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFSistema_Coordenacao)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Coordena��o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV68TFSistema_Coordenacao, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFAmbienteTecnologico_Descricao_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Amb.Tec.", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV71TFAmbienteTecnologico_Descricao_Sel, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFAmbienteTecnologico_Descricao)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Amb.Tec.", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV70TFAmbienteTecnologico_Descricao, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFMetodologia_Descricao_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Met.", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73TFMetodologia_Descricao_Sel, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFMetodologia_Descricao)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Met.", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV72TFMetodologia_Descricao, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFSistema_ProjetoNome_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Projeto", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV79TFSistema_ProjetoNome_Sel, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFSistema_ProjetoNome)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Projeto", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78TFSistema_ProjetoNome, "@!")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFSistemaVersao_Id_Sel)) )
         {
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Vers�o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV85TFSistemaVersao_Id_Sel, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFSistemaVersao_Id)) )
            {
               H4X0( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Vers�o", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84TFSistemaVersao_Id, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! (0==AV80TFSistema_Ativo_Sel) )
         {
            if ( AV80TFSistema_Ativo_Sel == 1 )
            {
               AV82FilterTFSistema_Ativo_SelValueDescription = "Marcado";
            }
            else if ( AV80TFSistema_Ativo_Sel == 2 )
            {
               AV82FilterTFSistema_Ativo_SelValueDescription = "Desmarcado";
            }
            H4X0( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Ativo", 5, Gx_line+2, 108, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV82FilterTFSistema_Ativo_SelValueDescription, "")), 110, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H4X0( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 157, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(162, Gx_line+30, 314, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(319, Gx_line+30, 471, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(476, Gx_line+30, 628, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(633, Gx_line+30, 785, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Sigla", 5, Gx_line+15, 157, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("T�cnica", 162, Gx_line+15, 314, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Coordena��o", 319, Gx_line+15, 471, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Projeto", 476, Gx_line+15, 628, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Vers�o", 633, Gx_line+15, 785, Gx_line+29, 0, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV90WWSistemaDS_1_Sistema_areatrabalhocod = AV12Sistema_AreaTrabalhoCod;
         AV91WWSistemaDS_2_Dynamicfiltersselector1 = AV13DynamicFiltersSelector1;
         AV92WWSistemaDS_3_Dynamicfiltersoperator1 = AV39DynamicFiltersOperator1;
         AV93WWSistemaDS_4_Sistema_nome1 = AV15Sistema_Nome1;
         AV94WWSistemaDS_5_Sistema_sigla1 = AV14Sistema_Sigla1;
         AV95WWSistemaDS_6_Sistema_tipo1 = AV33Sistema_Tipo1;
         AV96WWSistemaDS_7_Sistema_tecnica1 = AV40Sistema_Tecnica1;
         AV97WWSistemaDS_8_Sistema_coordenacao1 = AV43Sistema_Coordenacao1;
         AV98WWSistemaDS_9_Sistema_projetonome1 = AV51Sistema_ProjetoNome1;
         AV99WWSistemaDS_10_Sistema_pf1 = AV54Sistema_PF1;
         AV100WWSistemaDS_11_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV101WWSistemaDS_12_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV102WWSistemaDS_13_Dynamicfiltersoperator2 = AV44DynamicFiltersOperator2;
         AV103WWSistemaDS_14_Sistema_nome2 = AV20Sistema_Nome2;
         AV104WWSistemaDS_15_Sistema_sigla2 = AV19Sistema_Sigla2;
         AV105WWSistemaDS_16_Sistema_tipo2 = AV36Sistema_Tipo2;
         AV106WWSistemaDS_17_Sistema_tecnica2 = AV45Sistema_Tecnica2;
         AV107WWSistemaDS_18_Sistema_coordenacao2 = AV47Sistema_Coordenacao2;
         AV108WWSistemaDS_19_Sistema_projetonome2 = AV52Sistema_ProjetoNome2;
         AV109WWSistemaDS_20_Sistema_pf2 = AV55Sistema_PF2;
         AV110WWSistemaDS_21_Tfsistema_sigla = AV60TFSistema_Sigla;
         AV111WWSistemaDS_22_Tfsistema_sigla_sel = AV61TFSistema_Sigla_Sel;
         AV112WWSistemaDS_23_Tfsistema_tecnica_sels = AV66TFSistema_Tecnica_Sels;
         AV113WWSistemaDS_24_Tfsistema_coordenacao = AV68TFSistema_Coordenacao;
         AV114WWSistemaDS_25_Tfsistema_coordenacao_sel = AV69TFSistema_Coordenacao_Sel;
         AV115WWSistemaDS_26_Tfambientetecnologico_descricao = AV70TFAmbienteTecnologico_Descricao;
         AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV71TFAmbienteTecnologico_Descricao_Sel;
         AV117WWSistemaDS_28_Tfmetodologia_descricao = AV72TFMetodologia_Descricao;
         AV118WWSistemaDS_29_Tfmetodologia_descricao_sel = AV73TFMetodologia_Descricao_Sel;
         AV119WWSistemaDS_30_Tfsistema_projetonome = AV78TFSistema_ProjetoNome;
         AV120WWSistemaDS_31_Tfsistema_projetonome_sel = AV79TFSistema_ProjetoNome_Sel;
         AV121WWSistemaDS_32_Tfsistemaversao_id = AV84TFSistemaVersao_Id;
         AV122WWSistemaDS_33_Tfsistemaversao_id_sel = AV85TFSistemaVersao_Id_Sel;
         AV123WWSistemaDS_34_Tfsistema_ativo_sel = AV80TFSistema_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV112WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV91WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV93WWSistemaDS_4_Sistema_nome1 ,
                                              AV94WWSistemaDS_5_Sistema_sigla1 ,
                                              AV95WWSistemaDS_6_Sistema_tipo1 ,
                                              AV96WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV97WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV98WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV100WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV101WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV103WWSistemaDS_14_Sistema_nome2 ,
                                              AV104WWSistemaDS_15_Sistema_sigla2 ,
                                              AV105WWSistemaDS_16_Sistema_tipo2 ,
                                              AV106WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV107WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV108WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV111WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV110WWSistemaDS_21_Tfsistema_sigla ,
                                              AV112WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV114WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV113WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV115WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV118WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV117WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV120WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV119WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV122WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV121WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV123WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV10OrderedBy ,
                                              AV11OrderedDsc ,
                                              AV92WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV99WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV102WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV109WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV93WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV93WWSistemaDS_4_Sistema_nome1), "%", "");
         lV94WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV94WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV97WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV97WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV98WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV98WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV103WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV103WWSistemaDS_14_Sistema_nome2), "%", "");
         lV104WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV107WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV107WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV108WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV108WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV110WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV110WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV113WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV113WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV115WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV115WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV117WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV117WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV119WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV119WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV121WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV121WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P004X2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV93WWSistemaDS_4_Sistema_nome1, lV94WWSistemaDS_5_Sistema_sigla1, AV95WWSistemaDS_6_Sistema_tipo1, AV96WWSistemaDS_7_Sistema_tecnica1, lV97WWSistemaDS_8_Sistema_coordenacao1, lV98WWSistemaDS_9_Sistema_projetonome1, lV103WWSistemaDS_14_Sistema_nome2, lV104WWSistemaDS_15_Sistema_sigla2, AV105WWSistemaDS_16_Sistema_tipo2, AV106WWSistemaDS_17_Sistema_tecnica2, lV107WWSistemaDS_18_Sistema_coordenacao2, lV108WWSistemaDS_19_Sistema_projetonome2, lV110WWSistemaDS_21_Tfsistema_sigla, AV111WWSistemaDS_22_Tfsistema_sigla_sel, lV113WWSistemaDS_24_Tfsistema_coordenacao, AV114WWSistemaDS_25_Tfsistema_coordenacao_sel, lV115WWSistemaDS_26_Tfambientetecnologico_descricao, AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV117WWSistemaDS_28_Tfmetodologia_descricao, AV118WWSistemaDS_29_Tfmetodologia_descricao_sel, lV119WWSistemaDS_30_Tfsistema_projetonome, AV120WWSistemaDS_31_Tfsistema_projetonome_sel, lV121WWSistemaDS_32_Tfsistemaversao_id, AV122WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A137Metodologia_Codigo = P004X2_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P004X2_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P004X2_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P004X2_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = P004X2_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P004X2_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P004X2_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P004X2_n691Sistema_ProjetoCod[0];
            A130Sistema_Ativo = P004X2_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P004X2_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P004X2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P004X2_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P004X2_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P004X2_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P004X2_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P004X2_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P004X2_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P004X2_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P004X2_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P004X2_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P004X2_A129Sistema_Sigla[0];
            A416Sistema_Nome = P004X2_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P004X2_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P004X2_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P004X2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P004X2_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = P004X2_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P004X2_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P004X2_n703Sistema_ProjetoNome[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV92WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV99WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV99WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV92WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV99WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV99WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV92WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV99WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV99WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV102WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV109WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV109WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV102WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV109WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV109WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV102WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV109WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV109WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              if ( StringUtil.StrCmp(StringUtil.Trim( A700Sistema_Tecnica), "I") == 0 )
                              {
                                 AV38Sistema_TecnicaDescription = "Indicativa";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( A700Sistema_Tecnica), "E") == 0 )
                              {
                                 AV38Sistema_TecnicaDescription = "Estimada";
                              }
                              else if ( StringUtil.StrCmp(StringUtil.Trim( A700Sistema_Tecnica), "D") == 0 )
                              {
                                 AV38Sistema_TecnicaDescription = "Detalhada";
                              }
                              /* Execute user subroutine: 'BEFOREPRINTLINE' */
                              S152 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(0);
                                 returnInSub = true;
                                 if (true) return;
                              }
                              H4X0( false, 20) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")), 5, Gx_line+2, 157, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV38Sistema_TecnicaDescription, "")), 162, Gx_line+2, 314, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!")), 319, Gx_line+2, 471, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A703Sistema_ProjetoNome, "@!")), 476, Gx_line+2, 628, Gx_line+17, 0, 0, 0, 0) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, "")), 633, Gx_line+2, 785, Gx_line+17, 0, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+20);
                              /* Execute user subroutine: 'AFTERPRINTLINE' */
                              S162 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(0);
                                 returnInSub = true;
                                 if (true) return;
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H4X0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV23GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV24GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV13DynamicFiltersSelector1 = "";
         AV15Sistema_Nome1 = "";
         AV26Sistema_Nome = "";
         AV14Sistema_Sigla1 = "";
         AV25Sistema_Sigla = "";
         AV33Sistema_Tipo1 = "A";
         AV35FilterSistema_Tipo1ValueDescription = "";
         AV34FilterSistema_TipoValueDescription = "";
         AV40Sistema_Tecnica1 = "";
         AV42FilterSistema_Tecnica1ValueDescription = "";
         AV41FilterSistema_TecnicaValueDescription = "";
         AV43Sistema_Coordenacao1 = "";
         AV50Sistema_Coordenacao = "";
         AV51Sistema_ProjetoNome1 = "";
         AV53Sistema_ProjetoNome = "";
         AV56FilterSistema_PFDescription = "";
         AV18DynamicFiltersSelector2 = "";
         AV20Sistema_Nome2 = "";
         AV19Sistema_Sigla2 = "";
         AV36Sistema_Tipo2 = "A";
         AV37FilterSistema_Tipo2ValueDescription = "";
         AV45Sistema_Tecnica2 = "";
         AV46FilterSistema_Tecnica2ValueDescription = "";
         AV47Sistema_Coordenacao2 = "";
         AV52Sistema_ProjetoNome2 = "";
         AV66TFSistema_Tecnica_Sels = new GxSimpleCollection();
         AV67TFSistema_Tecnica_Sel = "";
         AV65TFSistema_Tecnica_SelDscs = "";
         AV81FilterTFSistema_Tecnica_SelValueDescription = "";
         AV82FilterTFSistema_Ativo_SelValueDescription = "";
         AV91WWSistemaDS_2_Dynamicfiltersselector1 = "";
         AV93WWSistemaDS_4_Sistema_nome1 = "";
         AV94WWSistemaDS_5_Sistema_sigla1 = "";
         AV95WWSistemaDS_6_Sistema_tipo1 = "";
         AV96WWSistemaDS_7_Sistema_tecnica1 = "";
         AV97WWSistemaDS_8_Sistema_coordenacao1 = "";
         AV98WWSistemaDS_9_Sistema_projetonome1 = "";
         AV101WWSistemaDS_12_Dynamicfiltersselector2 = "";
         AV103WWSistemaDS_14_Sistema_nome2 = "";
         AV104WWSistemaDS_15_Sistema_sigla2 = "";
         AV105WWSistemaDS_16_Sistema_tipo2 = "";
         AV106WWSistemaDS_17_Sistema_tecnica2 = "";
         AV107WWSistemaDS_18_Sistema_coordenacao2 = "";
         AV108WWSistemaDS_19_Sistema_projetonome2 = "";
         AV110WWSistemaDS_21_Tfsistema_sigla = "";
         AV111WWSistemaDS_22_Tfsistema_sigla_sel = "";
         AV112WWSistemaDS_23_Tfsistema_tecnica_sels = new GxSimpleCollection();
         AV113WWSistemaDS_24_Tfsistema_coordenacao = "";
         AV114WWSistemaDS_25_Tfsistema_coordenacao_sel = "";
         AV115WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel = "";
         AV117WWSistemaDS_28_Tfmetodologia_descricao = "";
         AV118WWSistemaDS_29_Tfmetodologia_descricao_sel = "";
         AV119WWSistemaDS_30_Tfsistema_projetonome = "";
         AV120WWSistemaDS_31_Tfsistema_projetonome_sel = "";
         AV121WWSistemaDS_32_Tfsistemaversao_id = "";
         AV122WWSistemaDS_33_Tfsistemaversao_id_sel = "";
         scmdbuf = "";
         lV93WWSistemaDS_4_Sistema_nome1 = "";
         lV94WWSistemaDS_5_Sistema_sigla1 = "";
         lV97WWSistemaDS_8_Sistema_coordenacao1 = "";
         lV98WWSistemaDS_9_Sistema_projetonome1 = "";
         lV103WWSistemaDS_14_Sistema_nome2 = "";
         lV104WWSistemaDS_15_Sistema_sigla2 = "";
         lV107WWSistemaDS_18_Sistema_coordenacao2 = "";
         lV108WWSistemaDS_19_Sistema_projetonome2 = "";
         lV110WWSistemaDS_21_Tfsistema_sigla = "";
         lV113WWSistemaDS_24_Tfsistema_coordenacao = "";
         lV115WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         lV117WWSistemaDS_28_Tfmetodologia_descricao = "";
         lV119WWSistemaDS_30_Tfsistema_projetonome = "";
         lV121WWSistemaDS_32_Tfsistemaversao_id = "";
         A700Sistema_Tecnica = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A699Sistema_Tipo = "";
         A513Sistema_Coordenacao = "";
         A703Sistema_ProjetoNome = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         A1860SistemaVersao_Id = "";
         P004X2_A137Metodologia_Codigo = new int[1] ;
         P004X2_n137Metodologia_Codigo = new bool[] {false} ;
         P004X2_A351AmbienteTecnologico_Codigo = new int[1] ;
         P004X2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P004X2_A1859SistemaVersao_Codigo = new int[1] ;
         P004X2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P004X2_A691Sistema_ProjetoCod = new int[1] ;
         P004X2_n691Sistema_ProjetoCod = new bool[] {false} ;
         P004X2_A130Sistema_Ativo = new bool[] {false} ;
         P004X2_A1860SistemaVersao_Id = new String[] {""} ;
         P004X2_A138Metodologia_Descricao = new String[] {""} ;
         P004X2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P004X2_A703Sistema_ProjetoNome = new String[] {""} ;
         P004X2_n703Sistema_ProjetoNome = new bool[] {false} ;
         P004X2_A513Sistema_Coordenacao = new String[] {""} ;
         P004X2_n513Sistema_Coordenacao = new bool[] {false} ;
         P004X2_A700Sistema_Tecnica = new String[] {""} ;
         P004X2_n700Sistema_Tecnica = new bool[] {false} ;
         P004X2_A699Sistema_Tipo = new String[] {""} ;
         P004X2_n699Sistema_Tipo = new bool[] {false} ;
         P004X2_A129Sistema_Sigla = new String[] {""} ;
         P004X2_A416Sistema_Nome = new String[] {""} ;
         P004X2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P004X2_A127Sistema_Codigo = new int[1] ;
         AV38Sistema_TecnicaDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportreportwwsistema__default(),
            new Object[][] {
                new Object[] {
               P004X2_A137Metodologia_Codigo, P004X2_n137Metodologia_Codigo, P004X2_A351AmbienteTecnologico_Codigo, P004X2_n351AmbienteTecnologico_Codigo, P004X2_A1859SistemaVersao_Codigo, P004X2_n1859SistemaVersao_Codigo, P004X2_A691Sistema_ProjetoCod, P004X2_n691Sistema_ProjetoCod, P004X2_A130Sistema_Ativo, P004X2_A1860SistemaVersao_Id,
               P004X2_A138Metodologia_Descricao, P004X2_A352AmbienteTecnologico_Descricao, P004X2_A703Sistema_ProjetoNome, P004X2_n703Sistema_ProjetoNome, P004X2_A513Sistema_Coordenacao, P004X2_n513Sistema_Coordenacao, P004X2_A700Sistema_Tecnica, P004X2_n700Sistema_Tecnica, P004X2_A699Sistema_Tipo, P004X2_n699Sistema_Tipo,
               P004X2_A129Sistema_Sigla, P004X2_A416Sistema_Nome, P004X2_A135Sistema_AreaTrabalhoCod, P004X2_A127Sistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV80TFSistema_Ativo_Sel ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV39DynamicFiltersOperator1 ;
      private short AV44DynamicFiltersOperator2 ;
      private short AV92WWSistemaDS_3_Dynamicfiltersoperator1 ;
      private short AV102WWSistemaDS_13_Dynamicfiltersoperator2 ;
      private short AV123WWSistemaDS_34_Tfsistema_ativo_sel ;
      private int AV12Sistema_AreaTrabalhoCod ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int AV88GXV1 ;
      private int AV90WWSistemaDS_1_Sistema_areatrabalhocod ;
      private int AV112WWSistemaDS_23_Tfsistema_tecnica_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A137Metodologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A1859SistemaVersao_Codigo ;
      private int A691Sistema_ProjetoCod ;
      private int A127Sistema_Codigo ;
      private long AV83i ;
      private decimal AV54Sistema_PF1 ;
      private decimal AV57Sistema_PF ;
      private decimal AV55Sistema_PF2 ;
      private decimal AV99WWSistemaDS_10_Sistema_pf1 ;
      private decimal AV109WWSistemaDS_20_Sistema_pf2 ;
      private decimal A395Sistema_PF ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV60TFSistema_Sigla ;
      private String AV61TFSistema_Sigla_Sel ;
      private String AV78TFSistema_ProjetoNome ;
      private String AV79TFSistema_ProjetoNome_Sel ;
      private String AV84TFSistemaVersao_Id ;
      private String AV85TFSistemaVersao_Id_Sel ;
      private String AV14Sistema_Sigla1 ;
      private String AV25Sistema_Sigla ;
      private String AV33Sistema_Tipo1 ;
      private String AV40Sistema_Tecnica1 ;
      private String AV51Sistema_ProjetoNome1 ;
      private String AV53Sistema_ProjetoNome ;
      private String AV19Sistema_Sigla2 ;
      private String AV36Sistema_Tipo2 ;
      private String AV45Sistema_Tecnica2 ;
      private String AV52Sistema_ProjetoNome2 ;
      private String AV67TFSistema_Tecnica_Sel ;
      private String AV94WWSistemaDS_5_Sistema_sigla1 ;
      private String AV95WWSistemaDS_6_Sistema_tipo1 ;
      private String AV96WWSistemaDS_7_Sistema_tecnica1 ;
      private String AV98WWSistemaDS_9_Sistema_projetonome1 ;
      private String AV104WWSistemaDS_15_Sistema_sigla2 ;
      private String AV105WWSistemaDS_16_Sistema_tipo2 ;
      private String AV106WWSistemaDS_17_Sistema_tecnica2 ;
      private String AV108WWSistemaDS_19_Sistema_projetonome2 ;
      private String AV110WWSistemaDS_21_Tfsistema_sigla ;
      private String AV111WWSistemaDS_22_Tfsistema_sigla_sel ;
      private String AV119WWSistemaDS_30_Tfsistema_projetonome ;
      private String AV120WWSistemaDS_31_Tfsistema_projetonome_sel ;
      private String AV121WWSistemaDS_32_Tfsistemaversao_id ;
      private String AV122WWSistemaDS_33_Tfsistemaversao_id_sel ;
      private String scmdbuf ;
      private String lV94WWSistemaDS_5_Sistema_sigla1 ;
      private String lV98WWSistemaDS_9_Sistema_projetonome1 ;
      private String lV104WWSistemaDS_15_Sistema_sigla2 ;
      private String lV108WWSistemaDS_19_Sistema_projetonome2 ;
      private String lV110WWSistemaDS_21_Tfsistema_sigla ;
      private String lV119WWSistemaDS_30_Tfsistema_projetonome ;
      private String lV121WWSistemaDS_32_Tfsistemaversao_id ;
      private String A700Sistema_Tecnica ;
      private String A129Sistema_Sigla ;
      private String A699Sistema_Tipo ;
      private String A703Sistema_ProjetoNome ;
      private String A1860SistemaVersao_Id ;
      private bool entryPointCalled ;
      private bool AV11OrderedDsc ;
      private bool returnInSub ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV100WWSistemaDS_11_Dynamicfiltersenabled2 ;
      private bool A130Sistema_Ativo ;
      private bool n137Metodologia_Codigo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n691Sistema_ProjetoCod ;
      private bool n703Sistema_ProjetoNome ;
      private bool n513Sistema_Coordenacao ;
      private bool n700Sistema_Tecnica ;
      private bool n699Sistema_Tipo ;
      private String AV64TFSistema_Tecnica_SelsJson ;
      private String AV22GridStateXML ;
      private String AV68TFSistema_Coordenacao ;
      private String AV69TFSistema_Coordenacao_Sel ;
      private String AV70TFAmbienteTecnologico_Descricao ;
      private String AV71TFAmbienteTecnologico_Descricao_Sel ;
      private String AV72TFMetodologia_Descricao ;
      private String AV73TFMetodologia_Descricao_Sel ;
      private String AV13DynamicFiltersSelector1 ;
      private String AV15Sistema_Nome1 ;
      private String AV26Sistema_Nome ;
      private String AV35FilterSistema_Tipo1ValueDescription ;
      private String AV34FilterSistema_TipoValueDescription ;
      private String AV42FilterSistema_Tecnica1ValueDescription ;
      private String AV41FilterSistema_TecnicaValueDescription ;
      private String AV43Sistema_Coordenacao1 ;
      private String AV50Sistema_Coordenacao ;
      private String AV56FilterSistema_PFDescription ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV20Sistema_Nome2 ;
      private String AV37FilterSistema_Tipo2ValueDescription ;
      private String AV46FilterSistema_Tecnica2ValueDescription ;
      private String AV47Sistema_Coordenacao2 ;
      private String AV65TFSistema_Tecnica_SelDscs ;
      private String AV81FilterTFSistema_Tecnica_SelValueDescription ;
      private String AV82FilterTFSistema_Ativo_SelValueDescription ;
      private String AV91WWSistemaDS_2_Dynamicfiltersselector1 ;
      private String AV93WWSistemaDS_4_Sistema_nome1 ;
      private String AV97WWSistemaDS_8_Sistema_coordenacao1 ;
      private String AV101WWSistemaDS_12_Dynamicfiltersselector2 ;
      private String AV103WWSistemaDS_14_Sistema_nome2 ;
      private String AV107WWSistemaDS_18_Sistema_coordenacao2 ;
      private String AV113WWSistemaDS_24_Tfsistema_coordenacao ;
      private String AV114WWSistemaDS_25_Tfsistema_coordenacao_sel ;
      private String AV115WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel ;
      private String AV117WWSistemaDS_28_Tfmetodologia_descricao ;
      private String AV118WWSistemaDS_29_Tfmetodologia_descricao_sel ;
      private String lV93WWSistemaDS_4_Sistema_nome1 ;
      private String lV97WWSistemaDS_8_Sistema_coordenacao1 ;
      private String lV103WWSistemaDS_14_Sistema_nome2 ;
      private String lV107WWSistemaDS_18_Sistema_coordenacao2 ;
      private String lV113WWSistemaDS_24_Tfsistema_coordenacao ;
      private String lV115WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String lV117WWSistemaDS_28_Tfmetodologia_descricao ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String AV38Sistema_TecnicaDescription ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P004X2_A137Metodologia_Codigo ;
      private bool[] P004X2_n137Metodologia_Codigo ;
      private int[] P004X2_A351AmbienteTecnologico_Codigo ;
      private bool[] P004X2_n351AmbienteTecnologico_Codigo ;
      private int[] P004X2_A1859SistemaVersao_Codigo ;
      private bool[] P004X2_n1859SistemaVersao_Codigo ;
      private int[] P004X2_A691Sistema_ProjetoCod ;
      private bool[] P004X2_n691Sistema_ProjetoCod ;
      private bool[] P004X2_A130Sistema_Ativo ;
      private String[] P004X2_A1860SistemaVersao_Id ;
      private String[] P004X2_A138Metodologia_Descricao ;
      private String[] P004X2_A352AmbienteTecnologico_Descricao ;
      private String[] P004X2_A703Sistema_ProjetoNome ;
      private bool[] P004X2_n703Sistema_ProjetoNome ;
      private String[] P004X2_A513Sistema_Coordenacao ;
      private bool[] P004X2_n513Sistema_Coordenacao ;
      private String[] P004X2_A700Sistema_Tecnica ;
      private bool[] P004X2_n700Sistema_Tecnica ;
      private String[] P004X2_A699Sistema_Tipo ;
      private bool[] P004X2_n699Sistema_Tipo ;
      private String[] P004X2_A129Sistema_Sigla ;
      private String[] P004X2_A416Sistema_Nome ;
      private int[] P004X2_A135Sistema_AreaTrabalhoCod ;
      private int[] P004X2_A127Sistema_Codigo ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV66TFSistema_Tecnica_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV112WWSistemaDS_23_Tfsistema_tecnica_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV23GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV24GridStateDynamicFilter ;
   }

   public class exportreportwwsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P004X2( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV112WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV91WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV93WWSistemaDS_4_Sistema_nome1 ,
                                             String AV94WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV95WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV96WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV97WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV98WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV100WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV101WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV103WWSistemaDS_14_Sistema_nome2 ,
                                             String AV104WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV105WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV106WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV107WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV108WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV111WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV110WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV112WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV114WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV113WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV115WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV118WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV117WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV120WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV119WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV122WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV121WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV123WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV10OrderedBy ,
                                             bool AV11OrderedDsc ,
                                             short AV92WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV99WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV102WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV109WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [25] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T5.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T5 WITH (NOLOCK) ON T5.[Projeto_Codigo] = T1.[Sistema_ProjetoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV93WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV94WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV95WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV96WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV97WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV98WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV103WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV104WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV105WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV106WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV107WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV100WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV101WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV108WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV111WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV110WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV111WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV112WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV112WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV114WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV113WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV114WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV115WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV118WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV117WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV118WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV120WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like @lV119WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] = @AV120WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV122WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV121WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV122WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV123WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV123WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV10OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Nome]";
         }
         else if ( ( AV10OrderedBy == 2 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla]";
         }
         else if ( ( AV10OrderedBy == 2 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla] DESC";
         }
         else if ( ( AV10OrderedBy == 3 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Tecnica]";
         }
         else if ( ( AV10OrderedBy == 3 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Tecnica] DESC";
         }
         else if ( ( AV10OrderedBy == 4 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Coordenacao]";
         }
         else if ( ( AV10OrderedBy == 4 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Coordenacao] DESC";
         }
         else if ( ( AV10OrderedBy == 5 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV10OrderedBy == 5 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[AmbienteTecnologico_Descricao] DESC";
         }
         else if ( ( AV10OrderedBy == 6 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Metodologia_Descricao]";
         }
         else if ( ( AV10OrderedBy == 6 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Metodologia_Descricao] DESC";
         }
         else if ( ( AV10OrderedBy == 7 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[Projeto_Nome]";
         }
         else if ( ( AV10OrderedBy == 7 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[Projeto_Nome] DESC";
         }
         else if ( ( AV10OrderedBy == 8 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[SistemaVersao_Id]";
         }
         else if ( ( AV10OrderedBy == 8 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[SistemaVersao_Id] DESC";
         }
         else if ( ( AV10OrderedBy == 9 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Ativo]";
         }
         else if ( ( AV10OrderedBy == 9 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Ativo] DESC";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P004X2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (bool)dynConstraints[41] , (short)dynConstraints[42] , (decimal)dynConstraints[43] , (decimal)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004X2 ;
          prmP004X2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV93WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV94WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV95WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV96WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV97WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV104WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV105WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV106WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV107WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV108WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV110WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV111WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV113WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV114WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV115WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV116WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV118WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV119WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV120WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV121WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV122WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004X2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004X2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

}
