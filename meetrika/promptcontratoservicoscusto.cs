/*
               File: PromptContratoServicosCusto
        Description: Selecione Custo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:0:52.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicoscusto : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicoscusto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicoscusto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoServicosCusto_Codigo ,
                           ref decimal aP1_InOutContratoServicosCusto_CstUntPrdNrm )
      {
         this.AV7InOutContratoServicosCusto_Codigo = aP0_InOutContratoServicosCusto_Codigo;
         this.AV8InOutContratoServicosCusto_CstUntPrdNrm = aP1_InOutContratoServicosCusto_CstUntPrdNrm;
         executePrivate();
         aP0_InOutContratoServicosCusto_Codigo=this.AV7InOutContratoServicosCusto_Codigo;
         aP1_InOutContratoServicosCusto_CstUntPrdNrm=this.AV8InOutContratoServicosCusto_CstUntPrdNrm;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosCusto_CstUntPrdNrm1 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosCusto_CstUntPrdNrm1", StringUtil.LTrim( StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoServicosCusto_CstUntPrdNrm2 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosCusto_CstUntPrdNrm2", StringUtil.LTrim( StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ContratoServicosCusto_CstUntPrdNrm3 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosCusto_CstUntPrdNrm3", StringUtil.LTrim( StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFContratoServicosCusto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0)));
               AV32TFContratoServicosCusto_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosCusto_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0)));
               AV35TFContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0)));
               AV36TFContratoServicosCusto_CntSrvCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosCusto_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0)));
               AV39TFContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0)));
               AV40TFContratoServicosCusto_UsuarioCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosCusto_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0)));
               AV43TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               AV44TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
               AV47TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
               AV48TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
               AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace", AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace);
               AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace", AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace);
               AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace", AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace);
               AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
               AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoServicosCusto_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosCusto_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV8InOutContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAL22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSL22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEL22( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311905318");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicoscusto.aspx") + "?" + UrlEncode("" +AV7InOutContratoServicosCusto_Codigo) + "," + UrlEncode(StringUtil.Str(AV8InOutContratoServicosCusto_CstUntPrdNrm,18,5))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1", StringUtil.LTrim( StringUtil.NToC( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2", StringUtil.LTrim( StringUtil.NToC( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3", StringUtil.LTrim( StringUtil.NToC( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO", StringUtil.LTrim( StringUtil.NToC( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO", StringUtil.LTrim( StringUtil.NToC( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSCUSTO_CODIGOTITLEFILTERDATA", AV30ContratoServicosCusto_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSCUSTO_CODIGOTITLEFILTERDATA", AV30ContratoServicosCusto_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSCUSTO_CNTSRVCODTITLEFILTERDATA", AV34ContratoServicosCusto_CntSrvCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSCUSTO_CNTSRVCODTITLEFILTERDATA", AV34ContratoServicosCusto_CntSrvCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSCUSTO_USUARIOCODTITLEFILTERDATA", AV38ContratoServicosCusto_UsuarioCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSCUSTO_USUARIOCODTITLEFILTERDATA", AV38ContratoServicosCusto_UsuarioCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA", AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA", AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA", AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA", AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSCUSTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoServicosCusto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( AV8InOutContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cntsrvcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cntsrvcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cntsrvcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cntsrvcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cntsrvcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usuariocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usuariocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usuariocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usuariocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usuariocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormL22( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicosCusto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Custo do Servi�o" ;
      }

      protected void WBL20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_L22( true) ;
         }
         else
         {
            wb_table1_2_L22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(92, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContratoServicosCusto_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoServicosCusto_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cntsrvcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cntsrvcod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cntsrvcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cntsrvcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cntsrvcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_usuariocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_usuariocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_usuariocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_usuariocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_usuariocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_usuariocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV43TFContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV44TFContratoServicosCusto_CstUntPrdNrm_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdext_Internalname, StringUtil.LTrim( StringUtil.NToC( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV47TFContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV48TFContratoServicosCusto_CstUntPrdExt_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSCUSTO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Internalname, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosCusto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSCUSTO_CNTSRVCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Internalname, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosCusto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSCUSTO_USUARIOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Internalname, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosCusto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosCusto.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosCusto.htm");
         }
         wbLoad = true;
      }

      protected void STARTL22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Custo do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPL20( ) ;
      }

      protected void WSL22( )
      {
         STARTL22( ) ;
         EVTL22( ) ;
      }

      protected void EVTL22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11L22 */
                           E11L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12L22 */
                           E12L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13L22 */
                           E13L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14L22 */
                           E14L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15L22 */
                           E15L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16L22 */
                           E16L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17L22 */
                           E17L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18L22 */
                           E18L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19L22 */
                           E19L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20L22 */
                           E20L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21L22 */
                           E21L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22L22 */
                           E22L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23L22 */
                           E23L22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV56Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_Codigo_Internalname), ",", "."));
                           A1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", "."));
                           A1472ContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_UsuarioCod_Internalname), ",", "."));
                           A1474ContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".");
                           A1475ContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".");
                           n1475ContratoServicosCusto_CstUntPrdExt = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24L22 */
                                 E24L22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25L22 */
                                 E25L22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26L22 */
                                 E26L22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicoscusto_cstuntprdnrm1 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1"), ",", ".") != AV17ContratoServicosCusto_CstUntPrdNrm1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicoscusto_cstuntprdnrm2 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2"), ",", ".") != AV21ContratoServicosCusto_CstUntPrdNrm2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicoscusto_cstuntprdnrm3 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3"), ",", ".") != AV25ContratoServicosCusto_CstUntPrdNrm3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoServicosCusto_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosCusto_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cntsrvcod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV35TFContratoServicosCusto_CntSrvCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cntsrvcod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFContratoServicosCusto_CntSrvCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_usuariocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV39TFContratoServicosCusto_UsuarioCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_usuariocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosCusto_UsuarioCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cstuntprdnrm Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"), ",", ".") != AV43TFContratoServicosCusto_CstUntPrdNrm )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cstuntprdnrm_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO"), ",", ".") != AV44TFContratoServicosCusto_CstUntPrdNrm_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cstuntprdext Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"), ",", ".") != AV47TFContratoServicosCusto_CstUntPrdExt )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicoscusto_cstuntprdext_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO"), ",", ".") != AV48TFContratoServicosCusto_CstUntPrdExt_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E27L22 */
                                       E27L22 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEL22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormL22( ) ;
            }
         }
      }

      protected void PAL22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", "R$", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", "R$", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", "R$", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       decimal AV17ContratoServicosCusto_CstUntPrdNrm1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       decimal AV21ContratoServicosCusto_CstUntPrdNrm2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       decimal AV25ContratoServicosCusto_CstUntPrdNrm3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFContratoServicosCusto_Codigo ,
                                       int AV32TFContratoServicosCusto_Codigo_To ,
                                       int AV35TFContratoServicosCusto_CntSrvCod ,
                                       int AV36TFContratoServicosCusto_CntSrvCod_To ,
                                       int AV39TFContratoServicosCusto_UsuarioCod ,
                                       int AV40TFContratoServicosCusto_UsuarioCod_To ,
                                       decimal AV43TFContratoServicosCusto_CstUntPrdNrm ,
                                       decimal AV44TFContratoServicosCusto_CstUntPrdNrm_To ,
                                       decimal AV47TFContratoServicosCusto_CstUntPrdExt ,
                                       decimal AV48TFContratoServicosCusto_CstUntPrdExt_To ,
                                       String AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace ,
                                       String AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace ,
                                       String AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace ,
                                       String AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFL22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", GetSecureSignedToken( "", context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", GetSecureSignedToken( "", context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFL22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFL22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E25L22 */
         E25L22 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ContratoServicosCusto_CstUntPrdNrm1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21ContratoServicosCusto_CstUntPrdNrm2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25ContratoServicosCusto_CstUntPrdNrm3 ,
                                                 AV31TFContratoServicosCusto_Codigo ,
                                                 AV32TFContratoServicosCusto_Codigo_To ,
                                                 AV35TFContratoServicosCusto_CntSrvCod ,
                                                 AV36TFContratoServicosCusto_CntSrvCod_To ,
                                                 AV39TFContratoServicosCusto_UsuarioCod ,
                                                 AV40TFContratoServicosCusto_UsuarioCod_To ,
                                                 AV43TFContratoServicosCusto_CstUntPrdNrm ,
                                                 AV44TFContratoServicosCusto_CstUntPrdNrm_To ,
                                                 AV47TFContratoServicosCusto_CstUntPrdExt ,
                                                 AV48TFContratoServicosCusto_CstUntPrdExt_To ,
                                                 A1474ContratoServicosCusto_CstUntPrdNrm ,
                                                 A1473ContratoServicosCusto_Codigo ,
                                                 A1471ContratoServicosCusto_CntSrvCod ,
                                                 A1472ContratoServicosCusto_UsuarioCod ,
                                                 A1475ContratoServicosCusto_CstUntPrdExt ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00L22 */
            pr_default.execute(0, new Object[] {AV17ContratoServicosCusto_CstUntPrdNrm1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV21ContratoServicosCusto_CstUntPrdNrm2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV25ContratoServicosCusto_CstUntPrdNrm3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1475ContratoServicosCusto_CstUntPrdExt = H00L22_A1475ContratoServicosCusto_CstUntPrdExt[0];
               n1475ContratoServicosCusto_CstUntPrdExt = H00L22_n1475ContratoServicosCusto_CstUntPrdExt[0];
               A1474ContratoServicosCusto_CstUntPrdNrm = H00L22_A1474ContratoServicosCusto_CstUntPrdNrm[0];
               A1472ContratoServicosCusto_UsuarioCod = H00L22_A1472ContratoServicosCusto_UsuarioCod[0];
               A1471ContratoServicosCusto_CntSrvCod = H00L22_A1471ContratoServicosCusto_CntSrvCod[0];
               A1473ContratoServicosCusto_Codigo = H00L22_A1473ContratoServicosCusto_Codigo[0];
               /* Execute user event: E26L22 */
               E26L22 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBL20( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ContratoServicosCusto_CstUntPrdNrm1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21ContratoServicosCusto_CstUntPrdNrm2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25ContratoServicosCusto_CstUntPrdNrm3 ,
                                              AV31TFContratoServicosCusto_Codigo ,
                                              AV32TFContratoServicosCusto_Codigo_To ,
                                              AV35TFContratoServicosCusto_CntSrvCod ,
                                              AV36TFContratoServicosCusto_CntSrvCod_To ,
                                              AV39TFContratoServicosCusto_UsuarioCod ,
                                              AV40TFContratoServicosCusto_UsuarioCod_To ,
                                              AV43TFContratoServicosCusto_CstUntPrdNrm ,
                                              AV44TFContratoServicosCusto_CstUntPrdNrm_To ,
                                              AV47TFContratoServicosCusto_CstUntPrdExt ,
                                              AV48TFContratoServicosCusto_CstUntPrdExt_To ,
                                              A1474ContratoServicosCusto_CstUntPrdNrm ,
                                              A1473ContratoServicosCusto_Codigo ,
                                              A1471ContratoServicosCusto_CntSrvCod ,
                                              A1472ContratoServicosCusto_UsuarioCod ,
                                              A1475ContratoServicosCusto_CstUntPrdExt ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00L23 */
         pr_default.execute(1, new Object[] {AV17ContratoServicosCusto_CstUntPrdNrm1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV21ContratoServicosCusto_CstUntPrdNrm2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV25ContratoServicosCusto_CstUntPrdNrm3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To});
         GRID_nRecordCount = H00L23_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPL20( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24L22 */
         E24L22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV50DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSCUSTO_CODIGOTITLEFILTERDATA"), AV30ContratoServicosCusto_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSCUSTO_CNTSRVCODTITLEFILTERDATA"), AV34ContratoServicosCusto_CntSrvCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSCUSTO_USUARIOCODTITLEFILTERDATA"), AV38ContratoServicosCusto_UsuarioCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA"), AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA"), AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm1_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm1_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1");
               GX_FocusControl = edtavContratoservicoscusto_cstuntprdnrm1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoServicosCusto_CstUntPrdNrm1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosCusto_CstUntPrdNrm1", StringUtil.LTrim( StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5)));
            }
            else
            {
               AV17ContratoServicosCusto_CstUntPrdNrm1 = context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm1_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosCusto_CstUntPrdNrm1", StringUtil.LTrim( StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm2_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm2_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2");
               GX_FocusControl = edtavContratoservicoscusto_cstuntprdnrm2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoServicosCusto_CstUntPrdNrm2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosCusto_CstUntPrdNrm2", StringUtil.LTrim( StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5)));
            }
            else
            {
               AV21ContratoServicosCusto_CstUntPrdNrm2 = context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosCusto_CstUntPrdNrm2", StringUtil.LTrim( StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm3_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm3_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3");
               GX_FocusControl = edtavContratoservicoscusto_cstuntprdnrm3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoServicosCusto_CstUntPrdNrm3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosCusto_CstUntPrdNrm3", StringUtil.LTrim( StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5)));
            }
            else
            {
               AV25ContratoServicosCusto_CstUntPrdNrm3 = context.localUtil.CToN( cgiGet( edtavContratoservicoscusto_cstuntprdnrm3_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosCusto_CstUntPrdNrm3", StringUtil.LTrim( StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CODIGO");
               GX_FocusControl = edtavTfcontratoservicoscusto_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContratoServicosCusto_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0)));
            }
            else
            {
               AV31TFContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoServicosCusto_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosCusto_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFContratoServicosCusto_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosCusto_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD");
               GX_FocusControl = edtavTfcontratoservicoscusto_cntsrvcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFContratoServicosCusto_CntSrvCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0)));
            }
            else
            {
               AV35TFContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContratoServicosCusto_CntSrvCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosCusto_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0)));
            }
            else
            {
               AV36TFContratoServicosCusto_CntSrvCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosCusto_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_USUARIOCOD");
               GX_FocusControl = edtavTfcontratoservicoscusto_usuariocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContratoServicosCusto_UsuarioCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0)));
            }
            else
            {
               AV39TFContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_usuariocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoServicosCusto_UsuarioCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosCusto_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0)));
            }
            else
            {
               AV40TFContratoServicosCusto_UsuarioCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_usuariocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosCusto_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFContratoServicosCusto_CstUntPrdNrm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            }
            else
            {
               AV43TFContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContratoServicosCusto_CstUntPrdNrm_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            }
            else
            {
               AV44TFContratoServicosCusto_CstUntPrdNrm_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdext_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContratoServicosCusto_CstUntPrdExt = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            }
            else
            {
               AV47TFContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratoServicosCusto_CstUntPrdExt_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            }
            else
            {
               AV48TFContratoServicosCusto_CstUntPrdExt_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            }
            AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace", AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace);
            AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace", AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace);
            AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace", AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace);
            AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
            AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV52GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV53GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicoscusto_codigo_Caption = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Caption");
            Ddo_contratoservicoscusto_codigo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Tooltip");
            Ddo_contratoservicoscusto_codigo_Cls = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Cls");
            Ddo_contratoservicoscusto_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtext_set");
            Ddo_contratoservicoscusto_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtextto_set");
            Ddo_contratoservicoscusto_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Dropdownoptionstype");
            Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includesortasc"));
            Ddo_contratoservicoscusto_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includesortdsc"));
            Ddo_contratoservicoscusto_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortedstatus");
            Ddo_contratoservicoscusto_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includefilter"));
            Ddo_contratoservicoscusto_codigo_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filtertype");
            Ddo_contratoservicoscusto_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filterisrange"));
            Ddo_contratoservicoscusto_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Includedatalist"));
            Ddo_contratoservicoscusto_codigo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortasc");
            Ddo_contratoservicoscusto_codigo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Sortdsc");
            Ddo_contratoservicoscusto_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Cleanfilter");
            Ddo_contratoservicoscusto_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Rangefilterfrom");
            Ddo_contratoservicoscusto_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Rangefilterto");
            Ddo_contratoservicoscusto_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Searchbuttontext");
            Ddo_contratoservicoscusto_cntsrvcod_Caption = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Caption");
            Ddo_contratoservicoscusto_cntsrvcod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Tooltip");
            Ddo_contratoservicoscusto_cntsrvcod_Cls = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Cls");
            Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtext_set");
            Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtextto_set");
            Ddo_contratoservicoscusto_cntsrvcod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Dropdownoptionstype");
            Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_cntsrvcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includesortasc"));
            Ddo_contratoservicoscusto_cntsrvcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includesortdsc"));
            Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortedstatus");
            Ddo_contratoservicoscusto_cntsrvcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includefilter"));
            Ddo_contratoservicoscusto_cntsrvcod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filtertype");
            Ddo_contratoservicoscusto_cntsrvcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filterisrange"));
            Ddo_contratoservicoscusto_cntsrvcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Includedatalist"));
            Ddo_contratoservicoscusto_cntsrvcod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortasc");
            Ddo_contratoservicoscusto_cntsrvcod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Sortdsc");
            Ddo_contratoservicoscusto_cntsrvcod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Cleanfilter");
            Ddo_contratoservicoscusto_cntsrvcod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Rangefilterfrom");
            Ddo_contratoservicoscusto_cntsrvcod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Rangefilterto");
            Ddo_contratoservicoscusto_cntsrvcod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Searchbuttontext");
            Ddo_contratoservicoscusto_usuariocod_Caption = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Caption");
            Ddo_contratoservicoscusto_usuariocod_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Tooltip");
            Ddo_contratoservicoscusto_usuariocod_Cls = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Cls");
            Ddo_contratoservicoscusto_usuariocod_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtext_set");
            Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtextto_set");
            Ddo_contratoservicoscusto_usuariocod_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Dropdownoptionstype");
            Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_usuariocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includesortasc"));
            Ddo_contratoservicoscusto_usuariocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includesortdsc"));
            Ddo_contratoservicoscusto_usuariocod_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortedstatus");
            Ddo_contratoservicoscusto_usuariocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includefilter"));
            Ddo_contratoservicoscusto_usuariocod_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filtertype");
            Ddo_contratoservicoscusto_usuariocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filterisrange"));
            Ddo_contratoservicoscusto_usuariocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Includedatalist"));
            Ddo_contratoservicoscusto_usuariocod_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortasc");
            Ddo_contratoservicoscusto_usuariocod_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Sortdsc");
            Ddo_contratoservicoscusto_usuariocod_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Cleanfilter");
            Ddo_contratoservicoscusto_usuariocod_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Rangefilterfrom");
            Ddo_contratoservicoscusto_usuariocod_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Rangefilterto");
            Ddo_contratoservicoscusto_usuariocod_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Searchbuttontext");
            Ddo_contratoservicoscusto_cstuntprdnrm_Caption = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Caption");
            Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Tooltip");
            Ddo_contratoservicoscusto_cstuntprdnrm_Cls = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cls");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_set");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_set");
            Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Dropdownoptionstype");
            Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortasc"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortdsc"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortedstatus");
            Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includefilter"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filtertype");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filterisrange"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includedatalist"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortasc");
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortdsc");
            Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cleanfilter");
            Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterfrom");
            Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterto");
            Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Searchbuttontext");
            Ddo_contratoservicoscusto_cstuntprdext_Caption = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Caption");
            Ddo_contratoservicoscusto_cstuntprdext_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Tooltip");
            Ddo_contratoservicoscusto_cstuntprdext_Cls = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cls");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_set");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_set");
            Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Dropdownoptionstype");
            Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_cstuntprdext_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortasc"));
            Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortdsc"));
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortedstatus");
            Ddo_contratoservicoscusto_cstuntprdext_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includefilter"));
            Ddo_contratoservicoscusto_cstuntprdext_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filtertype");
            Ddo_contratoservicoscusto_cstuntprdext_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filterisrange"));
            Ddo_contratoservicoscusto_cstuntprdext_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includedatalist"));
            Ddo_contratoservicoscusto_cstuntprdext_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortasc");
            Ddo_contratoservicoscusto_cstuntprdext_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortdsc");
            Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cleanfilter");
            Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterfrom");
            Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterto");
            Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicoscusto_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Activeeventkey");
            Ddo_contratoservicoscusto_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtext_get");
            Ddo_contratoservicoscusto_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CODIGO_Filteredtextto_get");
            Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Activeeventkey");
            Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtext_get");
            Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD_Filteredtextto_get");
            Ddo_contratoservicoscusto_usuariocod_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Activeeventkey");
            Ddo_contratoservicoscusto_usuariocod_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtext_get");
            Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD_Filteredtextto_get");
            Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Activeeventkey");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_get");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_get");
            Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Activeeventkey");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_get");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1"), ",", ".") != AV17ContratoServicosCusto_CstUntPrdNrm1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2"), ",", ".") != AV21ContratoServicosCusto_CstUntPrdNrm2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3"), ",", ".") != AV25ContratoServicosCusto_CstUntPrdNrm3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoServicosCusto_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosCusto_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD"), ",", ".") != Convert.ToDecimal( AV35TFContratoServicosCusto_CntSrvCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFContratoServicosCusto_CntSrvCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV39TFContratoServicosCusto_UsuarioCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFContratoServicosCusto_UsuarioCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"), ",", ".") != AV43TFContratoServicosCusto_CstUntPrdNrm )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO"), ",", ".") != AV44TFContratoServicosCusto_CstUntPrdNrm_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"), ",", ".") != AV47TFContratoServicosCusto_CstUntPrdExt )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO"), ",", ".") != AV48TFContratoServicosCusto_CstUntPrdExt_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24L22 */
         E24L22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24L22( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicoscusto_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_codigo_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_codigo_to_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cntsrvcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cntsrvcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cntsrvcod_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cntsrvcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cntsrvcod_to_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_usuariocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_usuariocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_usuariocod_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_usuariocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_usuariocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_usuariocod_to_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdnrm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdnrm_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdext_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdext_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdext_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdext_to_Visible), 5, 0)));
         Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace);
         AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace = Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace", AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_CntSrvCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace);
         AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace = Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace", AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_UsuarioCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace);
         AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace = Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace", AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_CstUntPrdNrm";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace);
         AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_CstUntPrdExt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace);
         AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Custo do Servi�o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "R$", 0);
         cmbavOrderedby.addItem("2", "Servicos Custo_Codigo", 0);
         cmbavOrderedby.addItem("3", "Srv Cod", 0);
         cmbavOrderedby.addItem("4", "Custo_Usuario Cod", 0);
         cmbavOrderedby.addItem("5", " R$", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV50DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV50DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25L22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30ContratoServicosCusto_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosCusto_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoServicosCusto_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtContratoServicosCusto_Codigo_Titleformat = 2;
         edtContratoServicosCusto_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servicos Custo_Codigo", AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_Codigo_Internalname, "Title", edtContratoServicosCusto_Codigo_Title);
         edtContratoServicosCusto_CntSrvCod_Titleformat = 2;
         edtContratoServicosCusto_CntSrvCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Srv Cod", AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CntSrvCod_Internalname, "Title", edtContratoServicosCusto_CntSrvCod_Title);
         edtContratoServicosCusto_UsuarioCod_Titleformat = 2;
         edtContratoServicosCusto_UsuarioCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Custo_Usuario Cod", AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_UsuarioCod_Internalname, "Title", edtContratoServicosCusto_UsuarioCod_Title);
         edtContratoServicosCusto_CstUntPrdNrm_Titleformat = 2;
         edtContratoServicosCusto_CstUntPrdNrm_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "R$", AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CstUntPrdNrm_Internalname, "Title", edtContratoServicosCusto_CstUntPrdNrm_Title);
         edtContratoServicosCusto_CstUntPrdExt_Titleformat = 2;
         edtContratoServicosCusto_CstUntPrdExt_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", " R$", AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosCusto_CstUntPrdExt_Internalname, "Title", edtContratoServicosCusto_CstUntPrdExt_Title);
         AV52GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52GridCurrentPage), 10, 0)));
         AV53GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ContratoServicosCusto_CodigoTitleFilterData", AV30ContratoServicosCusto_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34ContratoServicosCusto_CntSrvCodTitleFilterData", AV34ContratoServicosCusto_CntSrvCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ContratoServicosCusto_UsuarioCodTitleFilterData", AV38ContratoServicosCusto_UsuarioCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData", AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData", AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData);
      }

      protected void E11L22( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV51PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV51PageToGo) ;
         }
      }

      protected void E12L22( )
      {
         /* Ddo_contratoservicoscusto_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "SortedStatus", Ddo_contratoservicoscusto_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "SortedStatus", Ddo_contratoservicoscusto_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoServicosCusto_Codigo = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0)));
            AV32TFContratoServicosCusto_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosCusto_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13L22( )
      {
         /* Ddo_contratoservicoscusto_cntsrvcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0)));
            AV36TFContratoServicosCusto_CntSrvCod_To = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosCusto_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14L22( )
      {
         /* Ddo_contratoservicoscusto_usuariocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usuariocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_usuariocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usuariocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_usuariocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usuariocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContratoServicosCusto_UsuarioCod = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_usuariocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0)));
            AV40TFContratoServicosCusto_UsuarioCod_To = (int)(NumberUtil.Val( Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosCusto_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15L22( )
      {
         /* Ddo_contratoservicoscusto_cstuntprdnrm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            AV44TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16L22( )
      {
         /* Ddo_contratoservicoscusto_cstuntprdext_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            AV48TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26L22( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV56Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E27L22 */
         E27L22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27L22( )
      {
         /* Enter Routine */
         AV7InOutContratoServicosCusto_Codigo = A1473ContratoServicosCusto_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosCusto_Codigo), 6, 0)));
         AV8InOutContratoServicosCusto_CstUntPrdNrm = A1474ContratoServicosCusto_CstUntPrdNrm;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV8InOutContratoServicosCusto_CstUntPrdNrm, 18, 5)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoServicosCusto_Codigo,(decimal)AV8InOutContratoServicosCusto_CstUntPrdNrm});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17L22( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22L22( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18L22( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23L22( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19L22( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20L22( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosCusto_CstUntPrdNrm1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosCusto_CstUntPrdNrm2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosCusto_CstUntPrdNrm3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoServicosCusto_Codigo, AV32TFContratoServicosCusto_Codigo_To, AV35TFContratoServicosCusto_CntSrvCod, AV36TFContratoServicosCusto_CntSrvCod_To, AV39TFContratoServicosCusto_UsuarioCod, AV40TFContratoServicosCusto_UsuarioCod_To, AV43TFContratoServicosCusto_CstUntPrdNrm, AV44TFContratoServicosCusto_CstUntPrdNrm_To, AV47TFContratoServicosCusto_CstUntPrdExt, AV48TFContratoServicosCusto_CstUntPrdExt_To, AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace, AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace, AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace, AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21L22( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicoscusto_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "SortedStatus", Ddo_contratoservicoscusto_codigo_Sortedstatus);
         Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus);
         Ddo_contratoservicoscusto_usuariocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usuariocod_Sortedstatus);
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
         Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicoscusto_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "SortedStatus", Ddo_contratoservicoscusto_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicoscusto_usuariocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usuariocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicoscusto_cstuntprdnrm1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
         {
            edtavContratoservicoscusto_cstuntprdnrm1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicoscusto_cstuntprdnrm2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
         {
            edtavContratoservicoscusto_cstuntprdnrm2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicoscusto_cstuntprdnrm3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
         {
            edtavContratoservicoscusto_cstuntprdnrm3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicoscusto_cstuntprdnrm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicoscusto_cstuntprdnrm3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoServicosCusto_CstUntPrdNrm2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosCusto_CstUntPrdNrm2", StringUtil.LTrim( StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ContratoServicosCusto_CstUntPrdNrm3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosCusto_CstUntPrdNrm3", StringUtil.LTrim( StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFContratoServicosCusto_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoServicosCusto_Codigo), 6, 0)));
         Ddo_contratoservicoscusto_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_codigo_Filteredtext_set);
         AV32TFContratoServicosCusto_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosCusto_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosCusto_Codigo_To), 6, 0)));
         Ddo_contratoservicoscusto_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_codigo_Filteredtextto_set);
         AV35TFContratoServicosCusto_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratoServicosCusto_CntSrvCod), 6, 0)));
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set);
         AV36TFContratoServicosCusto_CntSrvCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosCusto_CntSrvCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratoServicosCusto_CntSrvCod_To), 6, 0)));
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cntsrvcod_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set);
         AV39TFContratoServicosCusto_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContratoServicosCusto_UsuarioCod), 6, 0)));
         Ddo_contratoservicoscusto_usuariocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_usuariocod_Filteredtext_set);
         AV40TFContratoServicosCusto_UsuarioCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosCusto_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContratoServicosCusto_UsuarioCod_To), 6, 0)));
         Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_usuariocod_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set);
         AV43TFContratoServicosCusto_CstUntPrdNrm = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set);
         AV44TFContratoServicosCusto_CstUntPrdNrm_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV44TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set);
         AV47TFContratoServicosCusto_CstUntPrdExt = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV47TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set);
         AV48TFContratoServicosCusto_CstUntPrdExt_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV48TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoServicosCusto_CstUntPrdNrm1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosCusto_CstUntPrdNrm1", StringUtil.LTrim( StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosCusto_CstUntPrdNrm1 = NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosCusto_CstUntPrdNrm1", StringUtil.LTrim( StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoServicosCusto_CstUntPrdNrm2 = NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosCusto_CstUntPrdNrm2", StringUtil.LTrim( StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoServicosCusto_CstUntPrdNrm3 = NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, ".");
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosCusto_CstUntPrdNrm3", StringUtil.LTrim( StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_L22( true) ;
         }
         else
         {
            wb_table2_5_L22( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_L22( true) ;
         }
         else
         {
            wb_table3_74_L22( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_L22e( true) ;
         }
         else
         {
            wb_table1_2_L22e( false) ;
         }
      }

      protected void wb_table3_74_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_L22( true) ;
         }
         else
         {
            wb_table4_77_L22( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_L22e( true) ;
         }
         else
         {
            wb_table3_74_L22e( false) ;
         }
      }

      protected void wb_table4_77_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_CntSrvCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_CntSrvCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_CntSrvCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_CstUntPrdNrm_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_CstUntPrdNrm_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_CstUntPrdNrm_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_CstUntPrdExt_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_CstUntPrdExt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_CstUntPrdExt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_CntSrvCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_CntSrvCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_CstUntPrdNrm_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_CstUntPrdNrm_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_CstUntPrdExt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_CstUntPrdExt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_L22e( true) ;
         }
         else
         {
            wb_table4_77_L22e( false) ;
         }
      }

      protected void wb_table2_5_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_L22( true) ;
         }
         else
         {
            wb_table5_14_L22( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_L22e( true) ;
         }
         else
         {
            wb_table2_5_L22e( false) ;
         }
      }

      protected void wb_table5_14_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_L22( true) ;
         }
         else
         {
            wb_table6_19_L22( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_L22e( true) ;
         }
         else
         {
            wb_table5_14_L22e( false) ;
         }
      }

      protected void wb_table6_19_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e28l21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_L22( true) ;
         }
         else
         {
            wb_table7_28_L22( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e29l21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_L22( true) ;
         }
         else
         {
            wb_table8_45_L22( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e30l21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_L22( true) ;
         }
         else
         {
            wb_table9_62_L22( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_L22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_L22e( true) ;
         }
         else
         {
            wb_table6_19_L22e( false) ;
         }
      }

      protected void wb_table9_62_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicoscusto_cstuntprdnrm3_Internalname, StringUtil.LTrim( StringUtil.NToC( AV25ContratoServicosCusto_CstUntPrdNrm3, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV25ContratoServicosCusto_CstUntPrdNrm3, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicoscusto_cstuntprdnrm3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicoscusto_cstuntprdnrm3_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_L22e( true) ;
         }
         else
         {
            wb_table9_62_L22e( false) ;
         }
      }

      protected void wb_table8_45_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicoscusto_cstuntprdnrm2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21ContratoServicosCusto_CstUntPrdNrm2, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV21ContratoServicosCusto_CstUntPrdNrm2, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicoscusto_cstuntprdnrm2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicoscusto_cstuntprdnrm2_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_L22e( true) ;
         }
         else
         {
            wb_table8_45_L22e( false) ;
         }
      }

      protected void wb_table7_28_L22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoServicosCusto.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicoscusto_cstuntprdnrm1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV17ContratoServicosCusto_CstUntPrdNrm1, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV17ContratoServicosCusto_CstUntPrdNrm1, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicoscusto_cstuntprdnrm1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicoscusto_cstuntprdnrm1_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosCusto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_L22e( true) ;
         }
         else
         {
            wb_table7_28_L22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoServicosCusto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoServicosCusto_Codigo), 6, 0)));
         AV8InOutContratoServicosCusto_CstUntPrdNrm = (decimal)(Convert.ToDecimal((decimal)getParm(obj,1)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV8InOutContratoServicosCusto_CstUntPrdNrm, 18, 5)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAL22( ) ;
         WSL22( ) ;
         WEL22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311905940");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicoscusto.js", "?2020311905940");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtContratoServicosCusto_Codigo_Internalname = "CONTRATOSERVICOSCUSTO_CODIGO_"+sGXsfl_80_idx;
         edtContratoServicosCusto_CntSrvCod_Internalname = "CONTRATOSERVICOSCUSTO_CNTSRVCOD_"+sGXsfl_80_idx;
         edtContratoServicosCusto_UsuarioCod_Internalname = "CONTRATOSERVICOSCUSTO_USUARIOCOD_"+sGXsfl_80_idx;
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_"+sGXsfl_80_idx;
         edtContratoServicosCusto_CstUntPrdExt_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtContratoServicosCusto_Codigo_Internalname = "CONTRATOSERVICOSCUSTO_CODIGO_"+sGXsfl_80_fel_idx;
         edtContratoServicosCusto_CntSrvCod_Internalname = "CONTRATOSERVICOSCUSTO_CNTSRVCOD_"+sGXsfl_80_fel_idx;
         edtContratoServicosCusto_UsuarioCod_Internalname = "CONTRATOSERVICOSCUSTO_USUARIOCOD_"+sGXsfl_80_fel_idx;
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_"+sGXsfl_80_fel_idx;
         edtContratoServicosCusto_CstUntPrdExt_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBL20( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV56Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV56Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_CntSrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_CntSrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_CstUntPrdNrm_Internalname,StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")),context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_CstUntPrdNrm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_CstUntPrdExt_Internalname,StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")),context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_CstUntPrdExt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_USUARIOCOD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoservicoscusto_cstuntprdnrm1_Internalname = "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoservicoscusto_cstuntprdnrm2_Internalname = "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoservicoscusto_cstuntprdnrm3_Internalname = "vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoServicosCusto_Codigo_Internalname = "CONTRATOSERVICOSCUSTO_CODIGO";
         edtContratoServicosCusto_CntSrvCod_Internalname = "CONTRATOSERVICOSCUSTO_CNTSRVCOD";
         edtContratoServicosCusto_UsuarioCod_Internalname = "CONTRATOSERVICOSCUSTO_USUARIOCOD";
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtContratoServicosCusto_CstUntPrdExt_Internalname = "CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicoscusto_codigo_Internalname = "vTFCONTRATOSERVICOSCUSTO_CODIGO";
         edtavTfcontratoservicoscusto_codigo_to_Internalname = "vTFCONTRATOSERVICOSCUSTO_CODIGO_TO";
         edtavTfcontratoservicoscusto_cntsrvcod_Internalname = "vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD";
         edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname = "vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO";
         edtavTfcontratoservicoscusto_usuariocod_Internalname = "vTFCONTRATOSERVICOSCUSTO_USUARIOCOD";
         edtavTfcontratoservicoscusto_usuariocod_to_Internalname = "vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO";
         edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname = "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname = "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO";
         edtavTfcontratoservicoscusto_cstuntprdext_Internalname = "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname = "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO";
         Ddo_contratoservicoscusto_codigo_Internalname = "DDO_CONTRATOSERVICOSCUSTO_CODIGO";
         edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_cntsrvcod_Internalname = "DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD";
         edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_usuariocod_Internalname = "DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD";
         edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_cstuntprdnrm_Internalname = "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_cstuntprdext_Internalname = "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosCusto_CstUntPrdExt_Jsonclick = "";
         edtContratoServicosCusto_CstUntPrdNrm_Jsonclick = "";
         edtContratoServicosCusto_UsuarioCod_Jsonclick = "";
         edtContratoServicosCusto_CntSrvCod_Jsonclick = "";
         edtContratoServicosCusto_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratoservicoscusto_cstuntprdnrm1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoservicoscusto_cstuntprdnrm2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratoservicoscusto_cstuntprdnrm3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoServicosCusto_CstUntPrdExt_Titleformat = 0;
         edtContratoServicosCusto_CstUntPrdNrm_Titleformat = 0;
         edtContratoServicosCusto_UsuarioCod_Titleformat = 0;
         edtContratoServicosCusto_CntSrvCod_Titleformat = 0;
         edtContratoServicosCusto_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoservicoscusto_cstuntprdnrm3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoservicoscusto_cstuntprdnrm2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoservicoscusto_cstuntprdnrm1_Visible = 1;
         edtContratoServicosCusto_CstUntPrdExt_Title = " R$";
         edtContratoServicosCusto_CstUntPrdNrm_Title = "R$";
         edtContratoServicosCusto_UsuarioCod_Title = "Custo_Usuario Cod";
         edtContratoServicosCusto_CntSrvCod_Title = "Srv Cod";
         edtContratoServicosCusto_Codigo_Title = "Servicos Custo_Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdext_to_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdext_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdnrm_Visible = 1;
         edtavTfcontratoservicoscusto_usuariocod_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_usuariocod_to_Visible = 1;
         edtavTfcontratoservicoscusto_usuariocod_Jsonclick = "";
         edtavTfcontratoservicoscusto_usuariocod_Visible = 1;
         edtavTfcontratoservicoscusto_cntsrvcod_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_cntsrvcod_to_Visible = 1;
         edtavTfcontratoservicoscusto_cntsrvcod_Jsonclick = "";
         edtavTfcontratoservicoscusto_cntsrvcod_Visible = 1;
         edtavTfcontratoservicoscusto_codigo_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_codigo_to_Visible = 1;
         edtavTfcontratoservicoscusto_codigo_Jsonclick = "";
         edtavTfcontratoservicoscusto_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_cstuntprdext_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_cstuntprdext_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_cstuntprdext_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_cstuntprdext_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_cstuntprdext_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_cstuntprdext_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_cstuntprdext_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_cstuntprdext_Caption = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_cstuntprdnrm_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_cstuntprdnrm_Caption = "";
         Ddo_contratoservicoscusto_usuariocod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_usuariocod_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_usuariocod_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_usuariocod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_usuariocod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_usuariocod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_usuariocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_usuariocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usuariocod_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_usuariocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usuariocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usuariocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_usuariocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_usuariocod_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_usuariocod_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_usuariocod_Caption = "";
         Ddo_contratoservicoscusto_cntsrvcod_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_cntsrvcod_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_cntsrvcod_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_cntsrvcod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_cntsrvcod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_cntsrvcod_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_cntsrvcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_cntsrvcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cntsrvcod_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_cntsrvcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cntsrvcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cntsrvcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_cntsrvcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_cntsrvcod_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_cntsrvcod_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_cntsrvcod_Caption = "";
         Ddo_contratoservicoscusto_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_codigo_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_codigo_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_codigo_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_codigo_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Custo do Servi�o";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV30ContratoServicosCusto_CodigoTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContratoServicosCusto_CntSrvCodTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContratoServicosCusto_UsuarioCodTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USUARIOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_Codigo_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CODIGO',prop:'Titleformat'},{av:'edtContratoServicosCusto_Codigo_Title',ctrl:'CONTRATOSERVICOSCUSTO_CODIGO',prop:'Title'},{av:'edtContratoServicosCusto_CntSrvCod_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'Titleformat'},{av:'edtContratoServicosCusto_CntSrvCod_Title',ctrl:'CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'Title'},{av:'edtContratoServicosCusto_UsuarioCod_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsuarioCod_Title',ctrl:'CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'AV52GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV53GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CODIGO.ONOPTIONCLICKED","{handler:'E12L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicoscusto_codigo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'SortedStatus'},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD.ONOPTIONCLICKED","{handler:'E13L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'SortedStatus'},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD.ONOPTIONCLICKED","{handler:'E14L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicoscusto_usuariocod_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_usuariocod_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'SortedStatus'},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM.ONOPTIONCLICKED","{handler:'E15L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT.ONOPTIONCLICKED","{handler:'E16L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_codigo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_usuariocod_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26L22',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E27L22',iparms:[{av:'A1473ContratoServicosCusto_Codigo',fld:'CONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1474ContratoServicosCusto_CstUntPrdNrm',fld:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV7InOutContratoServicosCusto_Codigo',fld:'vINOUTCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoServicosCusto_CstUntPrdNrm',fld:'vINOUTCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22L22',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicoscusto_cstuntprdnrm2_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicoscusto_cstuntprdnrm3_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicoscusto_cstuntprdnrm1_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E28L21',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicoscusto_cstuntprdnrm1_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23L22',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicoscusto_cstuntprdnrm2_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicoscusto_cstuntprdnrm3_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicoscusto_cstuntprdnrm1_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E29L21',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicoscusto_cstuntprdnrm2_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicoscusto_cstuntprdnrm2_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicoscusto_cstuntprdnrm3_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicoscusto_cstuntprdnrm1_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E30L21',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicoscusto_cstuntprdnrm3_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21L22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CNTSRVCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFContratoServicosCusto_Codigo',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'FilteredText_set'},{av:'AV32TFContratoServicosCusto_Codigo_To',fld:'vTFCONTRATOSERVICOSCUSTO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFContratoServicosCusto_CntSrvCod',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'FilteredText_set'},{av:'AV36TFContratoServicosCusto_CntSrvCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_CNTSRVCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CNTSRVCOD',prop:'FilteredTextTo_set'},{av:'AV39TFContratoServicosCusto_UsuarioCod',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_usuariocod_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'FilteredText_set'},{av:'AV40TFContratoServicosCusto_UsuarioCod_To',fld:'vTFCONTRATOSERVICOSCUSTO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USUARIOCOD',prop:'FilteredTextTo_set'},{av:'AV43TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredText_set'},{av:'AV44TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredTextTo_set'},{av:'AV47TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredText_set'},{av:'AV48TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosCusto_CstUntPrdNrm1',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicoscusto_cstuntprdnrm1_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosCusto_CstUntPrdNrm2',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosCusto_CstUntPrdNrm3',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicoscusto_cstuntprdnrm2_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicoscusto_cstuntprdnrm3_Visible',ctrl:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicoscusto_codigo_Activeeventkey = "";
         Ddo_contratoservicoscusto_codigo_Filteredtext_get = "";
         Ddo_contratoservicoscusto_codigo_Filteredtextto_get = "";
         Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey = "";
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get = "";
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get = "";
         Ddo_contratoservicoscusto_usuariocod_Activeeventkey = "";
         Ddo_contratoservicoscusto_usuariocod_Filteredtext_get = "";
         Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get = "";
         Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace = "";
         AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace = "";
         AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace = "";
         AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = "";
         AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV50DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30ContratoServicosCusto_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContratoServicosCusto_CntSrvCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContratoServicosCusto_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_contratoservicoscusto_codigo_Filteredtext_set = "";
         Ddo_contratoservicoscusto_codigo_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_codigo_Sortedstatus = "";
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set = "";
         Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus = "";
         Ddo_contratoservicoscusto_usuariocod_Filteredtext_set = "";
         Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_usuariocod_Sortedstatus = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV56Select_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00L22_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         H00L22_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         H00L22_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         H00L22_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         H00L22_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         H00L22_A1473ContratoServicosCusto_Codigo = new int[1] ;
         H00L23_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicoscusto__default(),
            new Object[][] {
                new Object[] {
               H00L22_A1475ContratoServicosCusto_CstUntPrdExt, H00L22_n1475ContratoServicosCusto_CstUntPrdExt, H00L22_A1474ContratoServicosCusto_CstUntPrdNrm, H00L22_A1472ContratoServicosCusto_UsuarioCod, H00L22_A1471ContratoServicosCusto_CntSrvCod, H00L22_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               H00L23_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosCusto_Codigo_Titleformat ;
      private short edtContratoServicosCusto_CntSrvCod_Titleformat ;
      private short edtContratoServicosCusto_UsuarioCod_Titleformat ;
      private short edtContratoServicosCusto_CstUntPrdNrm_Titleformat ;
      private short edtContratoServicosCusto_CstUntPrdExt_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoServicosCusto_Codigo ;
      private int wcpOAV7InOutContratoServicosCusto_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFContratoServicosCusto_Codigo ;
      private int AV32TFContratoServicosCusto_Codigo_To ;
      private int AV35TFContratoServicosCusto_CntSrvCod ;
      private int AV36TFContratoServicosCusto_CntSrvCod_To ;
      private int AV39TFContratoServicosCusto_UsuarioCod ;
      private int AV40TFContratoServicosCusto_UsuarioCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfcontratoservicoscusto_codigo_Visible ;
      private int edtavTfcontratoservicoscusto_codigo_to_Visible ;
      private int edtavTfcontratoservicoscusto_cntsrvcod_Visible ;
      private int edtavTfcontratoservicoscusto_cntsrvcod_to_Visible ;
      private int edtavTfcontratoservicoscusto_usuariocod_Visible ;
      private int edtavTfcontratoservicoscusto_usuariocod_to_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdnrm_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdext_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdext_to_Visible ;
      private int edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible ;
      private int A1473ContratoServicosCusto_Codigo ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV51PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicoscusto_cstuntprdnrm1_Visible ;
      private int edtavContratoservicoscusto_cstuntprdnrm2_Visible ;
      private int edtavContratoservicoscusto_cstuntprdnrm3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV52GridCurrentPage ;
      private long AV53GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV8InOutContratoServicosCusto_CstUntPrdNrm ;
      private decimal wcpOAV8InOutContratoServicosCusto_CstUntPrdNrm ;
      private decimal AV17ContratoServicosCusto_CstUntPrdNrm1 ;
      private decimal AV21ContratoServicosCusto_CstUntPrdNrm2 ;
      private decimal AV25ContratoServicosCusto_CstUntPrdNrm3 ;
      private decimal AV43TFContratoServicosCusto_CstUntPrdNrm ;
      private decimal AV44TFContratoServicosCusto_CstUntPrdNrm_To ;
      private decimal AV47TFContratoServicosCusto_CstUntPrdExt ;
      private decimal AV48TFContratoServicosCusto_CstUntPrdExt_To ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A1475ContratoServicosCusto_CstUntPrdExt ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicoscusto_codigo_Activeeventkey ;
      private String Ddo_contratoservicoscusto_codigo_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_codigo_Filteredtextto_get ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Activeeventkey ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_get ;
      private String Ddo_contratoservicoscusto_usuariocod_Activeeventkey ;
      private String Ddo_contratoservicoscusto_usuariocod_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_usuariocod_Filteredtextto_get ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicoscusto_codigo_Caption ;
      private String Ddo_contratoservicoscusto_codigo_Tooltip ;
      private String Ddo_contratoservicoscusto_codigo_Cls ;
      private String Ddo_contratoservicoscusto_codigo_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_codigo_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_codigo_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_codigo_Sortedstatus ;
      private String Ddo_contratoservicoscusto_codigo_Filtertype ;
      private String Ddo_contratoservicoscusto_codigo_Sortasc ;
      private String Ddo_contratoservicoscusto_codigo_Sortdsc ;
      private String Ddo_contratoservicoscusto_codigo_Cleanfilter ;
      private String Ddo_contratoservicoscusto_codigo_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_codigo_Rangefilterto ;
      private String Ddo_contratoservicoscusto_codigo_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Caption ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Tooltip ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Cls ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Sortedstatus ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Filtertype ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Sortasc ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Sortdsc ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Cleanfilter ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Rangefilterto ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_usuariocod_Caption ;
      private String Ddo_contratoservicoscusto_usuariocod_Tooltip ;
      private String Ddo_contratoservicoscusto_usuariocod_Cls ;
      private String Ddo_contratoservicoscusto_usuariocod_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_usuariocod_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_usuariocod_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_usuariocod_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_usuariocod_Sortedstatus ;
      private String Ddo_contratoservicoscusto_usuariocod_Filtertype ;
      private String Ddo_contratoservicoscusto_usuariocod_Sortasc ;
      private String Ddo_contratoservicoscusto_usuariocod_Sortdsc ;
      private String Ddo_contratoservicoscusto_usuariocod_Cleanfilter ;
      private String Ddo_contratoservicoscusto_usuariocod_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_usuariocod_Rangefilterto ;
      private String Ddo_contratoservicoscusto_usuariocod_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Caption ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Cls ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Caption ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Tooltip ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Cls ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filtertype ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortasc ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortdsc ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicoscusto_codigo_Internalname ;
      private String edtavTfcontratoservicoscusto_codigo_Jsonclick ;
      private String edtavTfcontratoservicoscusto_codigo_to_Internalname ;
      private String edtavTfcontratoservicoscusto_codigo_to_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cntsrvcod_Internalname ;
      private String edtavTfcontratoservicoscusto_cntsrvcod_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cntsrvcod_to_Internalname ;
      private String edtavTfcontratoservicoscusto_cntsrvcod_to_Jsonclick ;
      private String edtavTfcontratoservicoscusto_usuariocod_Internalname ;
      private String edtavTfcontratoservicoscusto_usuariocod_Jsonclick ;
      private String edtavTfcontratoservicoscusto_usuariocod_to_Internalname ;
      private String edtavTfcontratoservicoscusto_usuariocod_to_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick ;
      private String edtavDdo_contratoservicoscusto_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_cntsrvcodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_usuariocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoServicosCusto_Codigo_Internalname ;
      private String edtContratoServicosCusto_CntSrvCod_Internalname ;
      private String edtContratoServicosCusto_UsuarioCod_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdExt_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoservicoscusto_cstuntprdnrm1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoservicoscusto_cstuntprdnrm2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoservicoscusto_cstuntprdnrm3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicoscusto_codigo_Internalname ;
      private String Ddo_contratoservicoscusto_cntsrvcod_Internalname ;
      private String Ddo_contratoservicoscusto_usuariocod_Internalname ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Internalname ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosCusto_Codigo_Title ;
      private String edtContratoServicosCusto_CntSrvCod_Title ;
      private String edtContratoServicosCusto_UsuarioCod_Title ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Title ;
      private String edtContratoServicosCusto_CstUntPrdExt_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratoservicoscusto_cstuntprdnrm3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoservicoscusto_cstuntprdnrm2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoservicoscusto_cstuntprdnrm1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicosCusto_Codigo_Jsonclick ;
      private String edtContratoServicosCusto_CntSrvCod_Jsonclick ;
      private String edtContratoServicosCusto_UsuarioCod_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdExt_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicoscusto_codigo_Includesortasc ;
      private bool Ddo_contratoservicoscusto_codigo_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_codigo_Includefilter ;
      private bool Ddo_contratoservicoscusto_codigo_Filterisrange ;
      private bool Ddo_contratoservicoscusto_codigo_Includedatalist ;
      private bool Ddo_contratoservicoscusto_cntsrvcod_Includesortasc ;
      private bool Ddo_contratoservicoscusto_cntsrvcod_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_cntsrvcod_Includefilter ;
      private bool Ddo_contratoservicoscusto_cntsrvcod_Filterisrange ;
      private bool Ddo_contratoservicoscusto_cntsrvcod_Includedatalist ;
      private bool Ddo_contratoservicoscusto_usuariocod_Includesortasc ;
      private bool Ddo_contratoservicoscusto_usuariocod_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_usuariocod_Includefilter ;
      private bool Ddo_contratoservicoscusto_usuariocod_Filterisrange ;
      private bool Ddo_contratoservicoscusto_usuariocod_Includedatalist ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includesortasc ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includefilter ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Filterisrange ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1475ContratoServicosCusto_CstUntPrdExt ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33ddo_ContratoServicosCusto_CodigoTitleControlIdToReplace ;
      private String AV37ddo_ContratoServicosCusto_CntSrvCodTitleControlIdToReplace ;
      private String AV41ddo_ContratoServicosCusto_UsuarioCodTitleControlIdToReplace ;
      private String AV45ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace ;
      private String AV49ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace ;
      private String AV56Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoServicosCusto_Codigo ;
      private decimal aP1_InOutContratoServicosCusto_CstUntPrdNrm ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00L22_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] H00L22_n1475ContratoServicosCusto_CstUntPrdExt ;
      private decimal[] H00L22_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private int[] H00L22_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] H00L22_A1471ContratoServicosCusto_CntSrvCod ;
      private int[] H00L22_A1473ContratoServicosCusto_Codigo ;
      private long[] H00L23_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoServicosCusto_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContratoServicosCusto_CntSrvCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContratoServicosCusto_UsuarioCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42ContratoServicosCusto_CstUntPrdNrmTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46ContratoServicosCusto_CstUntPrdExtTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV50DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicoscusto__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00L22( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             decimal AV17ContratoServicosCusto_CstUntPrdNrm1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             decimal AV21ContratoServicosCusto_CstUntPrdNrm2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             decimal AV25ContratoServicosCusto_CstUntPrdNrm3 ,
                                             int AV31TFContratoServicosCusto_Codigo ,
                                             int AV32TFContratoServicosCusto_Codigo_To ,
                                             int AV35TFContratoServicosCusto_CntSrvCod ,
                                             int AV36TFContratoServicosCusto_CntSrvCod_To ,
                                             int AV39TFContratoServicosCusto_UsuarioCod ,
                                             int AV40TFContratoServicosCusto_UsuarioCod_To ,
                                             decimal AV43TFContratoServicosCusto_CstUntPrdNrm ,
                                             decimal AV44TFContratoServicosCusto_CstUntPrdNrm_To ,
                                             decimal AV47TFContratoServicosCusto_CstUntPrdExt ,
                                             decimal AV48TFContratoServicosCusto_CstUntPrdExt_To ,
                                             decimal A1474ContratoServicosCusto_CstUntPrdNrm ,
                                             int A1473ContratoServicosCusto_Codigo ,
                                             int A1471ContratoServicosCusto_CntSrvCod ,
                                             int A1472ContratoServicosCusto_UsuarioCod ,
                                             decimal A1475ContratoServicosCusto_CstUntPrdExt ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [24] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicosCusto_CstUntPrdExt], [ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_UsuarioCod], [ContratoServicosCusto_CntSrvCod], [ContratoServicosCusto_Codigo]";
         sFromString = " FROM [ContratoServicosCusto] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV31TFContratoServicosCusto_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_Codigo] >= @AV31TFContratoServicosCusto_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_Codigo] >= @AV31TFContratoServicosCusto_Codigo)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosCusto_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_Codigo] <= @AV32TFContratoServicosCusto_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_Codigo] <= @AV32TFContratoServicosCusto_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV35TFContratoServicosCusto_CntSrvCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CntSrvCod] >= @AV35TFContratoServicosCusto_CntSrvCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CntSrvCod] >= @AV35TFContratoServicosCusto_CntSrvCod)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV36TFContratoServicosCusto_CntSrvCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CntSrvCod] <= @AV36TFContratoServicosCusto_CntSrvCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CntSrvCod] <= @AV36TFContratoServicosCusto_CntSrvCod_To)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV39TFContratoServicosCusto_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_UsuarioCod] >= @AV39TFContratoServicosCusto_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_UsuarioCod] >= @AV39TFContratoServicosCusto_UsuarioCod)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV40TFContratoServicosCusto_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_UsuarioCod] <= @AV40TFContratoServicosCusto_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_UsuarioCod] <= @AV40TFContratoServicosCusto_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43TFContratoServicosCusto_CstUntPrdNrm) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] >= @AV43TFContratoServicosCusto_CstUntPrdNrm)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] >= @AV43TFContratoServicosCusto_CstUntPrdNrm)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFContratoServicosCusto_CstUntPrdNrm_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] <= @AV44TFContratoServicosCusto_CstUntPrdNrm_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] <= @AV44TFContratoServicosCusto_CstUntPrdNrm_To)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV47TFContratoServicosCusto_CstUntPrdExt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdExt] >= @AV47TFContratoServicosCusto_CstUntPrdExt)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdExt] >= @AV47TFContratoServicosCusto_CstUntPrdExt)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFContratoServicosCusto_CstUntPrdExt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdExt] <= @AV48TFContratoServicosCusto_CstUntPrdExt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdExt] <= @AV48TFContratoServicosCusto_CstUntPrdExt_To)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CstUntPrdNrm]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CstUntPrdNrm] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CntSrvCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CntSrvCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_UsuarioCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_UsuarioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CstUntPrdExt]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_CstUntPrdExt] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosCusto_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00L23( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             decimal AV17ContratoServicosCusto_CstUntPrdNrm1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             decimal AV21ContratoServicosCusto_CstUntPrdNrm2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             decimal AV25ContratoServicosCusto_CstUntPrdNrm3 ,
                                             int AV31TFContratoServicosCusto_Codigo ,
                                             int AV32TFContratoServicosCusto_Codigo_To ,
                                             int AV35TFContratoServicosCusto_CntSrvCod ,
                                             int AV36TFContratoServicosCusto_CntSrvCod_To ,
                                             int AV39TFContratoServicosCusto_UsuarioCod ,
                                             int AV40TFContratoServicosCusto_UsuarioCod_To ,
                                             decimal AV43TFContratoServicosCusto_CstUntPrdNrm ,
                                             decimal AV44TFContratoServicosCusto_CstUntPrdNrm_To ,
                                             decimal AV47TFContratoServicosCusto_CstUntPrdExt ,
                                             decimal AV48TFContratoServicosCusto_CstUntPrdExt_To ,
                                             decimal A1474ContratoServicosCusto_CstUntPrdNrm ,
                                             int A1473ContratoServicosCusto_Codigo ,
                                             int A1471ContratoServicosCusto_CntSrvCod ,
                                             int A1472ContratoServicosCusto_UsuarioCod ,
                                             decimal A1475ContratoServicosCusto_CstUntPrdExt ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [19] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosCusto] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV17ContratoServicosCusto_CstUntPrdNrm1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV17ContratoServicosCusto_CstUntPrdNrm1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV21ContratoServicosCusto_CstUntPrdNrm2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV21ContratoServicosCusto_CstUntPrdNrm2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] < @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] < @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] = @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] = @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (Convert.ToDecimal(0)==AV25ContratoServicosCusto_CstUntPrdNrm3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] > @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] > @AV25ContratoServicosCusto_CstUntPrdNrm3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV31TFContratoServicosCusto_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_Codigo] >= @AV31TFContratoServicosCusto_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_Codigo] >= @AV31TFContratoServicosCusto_Codigo)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosCusto_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_Codigo] <= @AV32TFContratoServicosCusto_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_Codigo] <= @AV32TFContratoServicosCusto_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV35TFContratoServicosCusto_CntSrvCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CntSrvCod] >= @AV35TFContratoServicosCusto_CntSrvCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CntSrvCod] >= @AV35TFContratoServicosCusto_CntSrvCod)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV36TFContratoServicosCusto_CntSrvCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CntSrvCod] <= @AV36TFContratoServicosCusto_CntSrvCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CntSrvCod] <= @AV36TFContratoServicosCusto_CntSrvCod_To)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV39TFContratoServicosCusto_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_UsuarioCod] >= @AV39TFContratoServicosCusto_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_UsuarioCod] >= @AV39TFContratoServicosCusto_UsuarioCod)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV40TFContratoServicosCusto_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_UsuarioCod] <= @AV40TFContratoServicosCusto_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_UsuarioCod] <= @AV40TFContratoServicosCusto_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV43TFContratoServicosCusto_CstUntPrdNrm) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] >= @AV43TFContratoServicosCusto_CstUntPrdNrm)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] >= @AV43TFContratoServicosCusto_CstUntPrdNrm)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV44TFContratoServicosCusto_CstUntPrdNrm_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdNrm] <= @AV44TFContratoServicosCusto_CstUntPrdNrm_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdNrm] <= @AV44TFContratoServicosCusto_CstUntPrdNrm_To)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV47TFContratoServicosCusto_CstUntPrdExt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdExt] >= @AV47TFContratoServicosCusto_CstUntPrdExt)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdExt] >= @AV47TFContratoServicosCusto_CstUntPrdExt)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFContratoServicosCusto_CstUntPrdExt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosCusto_CstUntPrdExt] <= @AV48TFContratoServicosCusto_CstUntPrdExt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosCusto_CstUntPrdExt] <= @AV48TFContratoServicosCusto_CstUntPrdExt_To)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00L22(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (decimal)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (decimal)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (decimal)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H00L23(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (decimal)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (decimal)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (decimal)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00L22 ;
          prmH00L22 = new Object[] {
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV31TFContratoServicosCusto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoServicosCusto_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratoServicosCusto_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContratoServicosCusto_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV44TFContratoServicosCusto_CstUntPrdNrm_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV47TFContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFContratoServicosCusto_CstUntPrdExt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00L23 ;
          prmH00L23 = new Object[] {
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17ContratoServicosCusto_CstUntPrdNrm1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21ContratoServicosCusto_CstUntPrdNrm2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25ContratoServicosCusto_CstUntPrdNrm3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV31TFContratoServicosCusto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoServicosCusto_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratoServicosCusto_CntSrvCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContratoServicosCusto_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContratoServicosCusto_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV44TFContratoServicosCusto_CstUntPrdNrm_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV47TFContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFContratoServicosCusto_CstUntPrdExt_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00L22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L22,11,0,true,false )
             ,new CursorDef("H00L23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                return;
       }
    }

 }

}
