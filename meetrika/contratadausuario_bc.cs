/*
               File: ContratadaUsuario_BC
        Description: Contratada Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:5.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadausuario_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratadausuario_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratadausuario_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0F16( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0F16( ) ;
         standaloneModal( ) ;
         AddRow0F16( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110F2 */
            E110F2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
               Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0F0( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0F16( ) ;
            }
            else
            {
               CheckExtendedTable0F16( ) ;
               if ( AnyError == 0 )
               {
                  ZM0F16( 2) ;
                  ZM0F16( 3) ;
                  ZM0F16( 4) ;
                  ZM0F16( 5) ;
                  ZM0F16( 6) ;
               }
               CloseExtendedTableCursors0F16( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120F2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
      }

      protected void E110F2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_tirardagestaodecontratos(context ).execute( ref  AV8ContratadaUsuario_UsuarioCod) ;
         }
      }

      protected void ZM0F16( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z576ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
            Z577ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
            Z1518ContratadaUsuario_TmpEstAnl = A1518ContratadaUsuario_TmpEstAnl;
            Z1503ContratadaUsuario_TmpEstExc = A1503ContratadaUsuario_TmpEstExc;
            Z1504ContratadaUsuario_TmpEstCrr = A1504ContratadaUsuario_TmpEstCrr;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z67ContratadaUsuario_ContratadaPessoaCod = A67ContratadaUsuario_ContratadaPessoaCod;
            Z1228ContratadaUsuario_AreaTrabalhoCod = A1228ContratadaUsuario_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1018ContratadaUsuario_UsuarioEhContratada = A1018ContratadaUsuario_UsuarioEhContratada;
            Z1394ContratadaUsuario_UsuarioAtivo = A1394ContratadaUsuario_UsuarioAtivo;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z70ContratadaUsuario_UsuarioPessoaCod = A70ContratadaUsuario_UsuarioPessoaCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z68ContratadaUsuario_ContratadaPessoaNom = A68ContratadaUsuario_ContratadaPessoaNom;
            Z348ContratadaUsuario_ContratadaPessoaCNPJ = A348ContratadaUsuario_ContratadaPessoaCNPJ;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z1297ContratadaUsuario_AreaTrabalhoDes = A1297ContratadaUsuario_AreaTrabalhoDes;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z71ContratadaUsuario_UsuarioPessoaNom = A71ContratadaUsuario_UsuarioPessoaNom;
            Z491ContratadaUsuario_UsuarioPessoaDoc = A491ContratadaUsuario_UsuarioPessoaDoc;
         }
         if ( GX_JID == -1 )
         {
            Z576ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
            Z577ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
            Z1518ContratadaUsuario_TmpEstAnl = A1518ContratadaUsuario_TmpEstAnl;
            Z1503ContratadaUsuario_TmpEstExc = A1503ContratadaUsuario_TmpEstExc;
            Z1504ContratadaUsuario_TmpEstCrr = A1504ContratadaUsuario_TmpEstCrr;
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            Z67ContratadaUsuario_ContratadaPessoaCod = A67ContratadaUsuario_ContratadaPessoaCod;
            Z1228ContratadaUsuario_AreaTrabalhoCod = A1228ContratadaUsuario_AreaTrabalhoCod;
            Z68ContratadaUsuario_ContratadaPessoaNom = A68ContratadaUsuario_ContratadaPessoaNom;
            Z348ContratadaUsuario_ContratadaPessoaCNPJ = A348ContratadaUsuario_ContratadaPessoaCNPJ;
            Z1297ContratadaUsuario_AreaTrabalhoDes = A1297ContratadaUsuario_AreaTrabalhoDes;
            Z1018ContratadaUsuario_UsuarioEhContratada = A1018ContratadaUsuario_UsuarioEhContratada;
            Z1394ContratadaUsuario_UsuarioAtivo = A1394ContratadaUsuario_UsuarioAtivo;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z70ContratadaUsuario_UsuarioPessoaCod = A70ContratadaUsuario_UsuarioPessoaCod;
            Z71ContratadaUsuario_UsuarioPessoaNom = A71ContratadaUsuario_UsuarioPessoaNom;
            Z491ContratadaUsuario_UsuarioPessoaDoc = A491ContratadaUsuario_UsuarioPessoaDoc;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load0F16( )
      {
         /* Using cursor BC000F9 */
         pr_default.execute(7, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound16 = 1;
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F9_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F9_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F9_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F9_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F9_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F9_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F9_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F9_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F9_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F9_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F9_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F9_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F9_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F9_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F9_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F9_n341Usuario_UserGamGuid[0];
            A576ContratadaUsuario_CstUntPrdNrm = BC000F9_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = BC000F9_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = BC000F9_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = BC000F9_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = BC000F9_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = BC000F9_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = BC000F9_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = BC000F9_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = BC000F9_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = BC000F9_n1504ContratadaUsuario_TmpEstCrr[0];
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F9_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F9_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F9_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F9_n70ContratadaUsuario_UsuarioPessoaCod[0];
            ZM0F16( -1) ;
         }
         pr_default.close(7);
         OnLoadActions0F16( ) ;
      }

      protected void OnLoadActions0F16( )
      {
      }

      protected void CheckExtendedTable0F16( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000F4 */
         pr_default.execute(2, new Object[] {A66ContratadaUsuario_ContratadaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_CONTRATADACOD");
            AnyError = 1;
         }
         A67ContratadaUsuario_ContratadaPessoaCod = BC000F4_A67ContratadaUsuario_ContratadaPessoaCod[0];
         n67ContratadaUsuario_ContratadaPessoaCod = BC000F4_n67ContratadaUsuario_ContratadaPessoaCod[0];
         A1228ContratadaUsuario_AreaTrabalhoCod = BC000F4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
         n1228ContratadaUsuario_AreaTrabalhoCod = BC000F4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
         pr_default.close(2);
         /* Using cursor BC000F6 */
         pr_default.execute(4, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A68ContratadaUsuario_ContratadaPessoaNom = BC000F6_A68ContratadaUsuario_ContratadaPessoaNom[0];
         n68ContratadaUsuario_ContratadaPessoaNom = BC000F6_n68ContratadaUsuario_ContratadaPessoaNom[0];
         A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
         n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
         pr_default.close(4);
         /* Using cursor BC000F7 */
         pr_default.execute(5, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1297ContratadaUsuario_AreaTrabalhoDes = BC000F7_A1297ContratadaUsuario_AreaTrabalhoDes[0];
         n1297ContratadaUsuario_AreaTrabalhoDes = BC000F7_n1297ContratadaUsuario_AreaTrabalhoDes[0];
         pr_default.close(5);
         /* Using cursor BC000F5 */
         pr_default.execute(3, new Object[] {A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
            AnyError = 1;
         }
         A1018ContratadaUsuario_UsuarioEhContratada = BC000F5_A1018ContratadaUsuario_UsuarioEhContratada[0];
         n1018ContratadaUsuario_UsuarioEhContratada = BC000F5_n1018ContratadaUsuario_UsuarioEhContratada[0];
         A1394ContratadaUsuario_UsuarioAtivo = BC000F5_A1394ContratadaUsuario_UsuarioAtivo[0];
         n1394ContratadaUsuario_UsuarioAtivo = BC000F5_n1394ContratadaUsuario_UsuarioAtivo[0];
         A2Usuario_Nome = BC000F5_A2Usuario_Nome[0];
         n2Usuario_Nome = BC000F5_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = BC000F5_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = BC000F5_n341Usuario_UserGamGuid[0];
         A70ContratadaUsuario_UsuarioPessoaCod = BC000F5_A70ContratadaUsuario_UsuarioPessoaCod[0];
         n70ContratadaUsuario_UsuarioPessoaCod = BC000F5_n70ContratadaUsuario_UsuarioPessoaCod[0];
         pr_default.close(3);
         /* Using cursor BC000F8 */
         pr_default.execute(6, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A71ContratadaUsuario_UsuarioPessoaNom = BC000F8_A71ContratadaUsuario_UsuarioPessoaNom[0];
         n71ContratadaUsuario_UsuarioPessoaNom = BC000F8_n71ContratadaUsuario_UsuarioPessoaNom[0];
         A491ContratadaUsuario_UsuarioPessoaDoc = BC000F8_A491ContratadaUsuario_UsuarioPessoaDoc[0];
         n491ContratadaUsuario_UsuarioPessoaDoc = BC000F8_n491ContratadaUsuario_UsuarioPessoaDoc[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0F16( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(3);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0F16( )
      {
         /* Using cursor BC000F10 */
         pr_default.execute(8, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound16 = 1;
         }
         else
         {
            RcdFound16 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000F3 */
         pr_default.execute(1, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0F16( 1) ;
            RcdFound16 = 1;
            A576ContratadaUsuario_CstUntPrdNrm = BC000F3_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = BC000F3_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = BC000F3_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = BC000F3_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = BC000F3_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = BC000F3_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = BC000F3_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = BC000F3_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = BC000F3_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = BC000F3_n1504ContratadaUsuario_TmpEstCrr[0];
            A66ContratadaUsuario_ContratadaCod = BC000F3_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = BC000F3_A69ContratadaUsuario_UsuarioCod[0];
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0F16( ) ;
            if ( AnyError == 1 )
            {
               RcdFound16 = 0;
               InitializeNonKey0F16( ) ;
            }
            Gx_mode = sMode16;
         }
         else
         {
            RcdFound16 = 0;
            InitializeNonKey0F16( ) ;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode16;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0F16( ) ;
         if ( RcdFound16 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0F0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0F16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000F2 */
            pr_default.execute(0, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratadaUsuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z576ContratadaUsuario_CstUntPrdNrm != BC000F2_A576ContratadaUsuario_CstUntPrdNrm[0] ) || ( Z577ContratadaUsuario_CstUntPrdExt != BC000F2_A577ContratadaUsuario_CstUntPrdExt[0] ) || ( Z1518ContratadaUsuario_TmpEstAnl != BC000F2_A1518ContratadaUsuario_TmpEstAnl[0] ) || ( Z1503ContratadaUsuario_TmpEstExc != BC000F2_A1503ContratadaUsuario_TmpEstExc[0] ) || ( Z1504ContratadaUsuario_TmpEstCrr != BC000F2_A1504ContratadaUsuario_TmpEstCrr[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratadaUsuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0F16( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0F16( 0) ;
            CheckOptimisticConcurrency0F16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0F16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0F16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000F11 */
                     pr_default.execute(9, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, n1504ContratadaUsuario_TmpEstCrr, A1504ContratadaUsuario_TmpEstCrr, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0F16( ) ;
            }
            EndLevel0F16( ) ;
         }
         CloseExtendedTableCursors0F16( ) ;
      }

      protected void Update0F16( )
      {
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0F16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0F16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0F16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000F12 */
                     pr_default.execute(10, new Object[] {n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt, n1518ContratadaUsuario_TmpEstAnl, A1518ContratadaUsuario_TmpEstAnl, n1503ContratadaUsuario_TmpEstExc, A1503ContratadaUsuario_TmpEstExc, n1504ContratadaUsuario_TmpEstCrr, A1504ContratadaUsuario_TmpEstCrr, A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratadaUsuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0F16( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0F16( ) ;
         }
         CloseExtendedTableCursors0F16( ) ;
      }

      protected void DeferredUpdate0F16( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0F16( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0F16( ) ;
            AfterConfirm0F16( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0F16( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000F13 */
                  pr_default.execute(11, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode16 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0F16( ) ;
         Gx_mode = sMode16;
      }

      protected void OnDeleteControls0F16( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000F14 */
            pr_default.execute(12, new Object[] {A66ContratadaUsuario_ContratadaCod});
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F14_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F14_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            pr_default.close(12);
            /* Using cursor BC000F15 */
            pr_default.execute(13, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F15_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F15_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            pr_default.close(13);
            /* Using cursor BC000F16 */
            pr_default.execute(14, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            pr_default.close(14);
            /* Using cursor BC000F17 */
            pr_default.execute(15, new Object[] {A69ContratadaUsuario_UsuarioCod});
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F17_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F17_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F17_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F17_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F17_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F17_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F17_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F17_n341Usuario_UserGamGuid[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F17_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F17_n70ContratadaUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000F18 */
            pr_default.execute(16, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F18_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F18_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
      }

      protected void EndLevel0F16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0F16( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0F16( )
      {
         /* Scan By routine */
         /* Using cursor BC000F19 */
         pr_default.execute(17, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod});
         RcdFound16 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound16 = 1;
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F19_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F19_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F19_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F19_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F19_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F19_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F19_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F19_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F19_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F19_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F19_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F19_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F19_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F19_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F19_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F19_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F19_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F19_n341Usuario_UserGamGuid[0];
            A576ContratadaUsuario_CstUntPrdNrm = BC000F19_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = BC000F19_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = BC000F19_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = BC000F19_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = BC000F19_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = BC000F19_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = BC000F19_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = BC000F19_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = BC000F19_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = BC000F19_n1504ContratadaUsuario_TmpEstCrr[0];
            A66ContratadaUsuario_ContratadaCod = BC000F19_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = BC000F19_A69ContratadaUsuario_UsuarioCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F19_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F19_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F19_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F19_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F19_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F19_n70ContratadaUsuario_UsuarioPessoaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0F16( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound16 = 0;
         ScanKeyLoad0F16( ) ;
      }

      protected void ScanKeyLoad0F16( )
      {
         sMode16 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound16 = 1;
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F19_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F19_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F19_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F19_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F19_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F19_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F19_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F19_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F19_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F19_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F19_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F19_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F19_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F19_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F19_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F19_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F19_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F19_n341Usuario_UserGamGuid[0];
            A576ContratadaUsuario_CstUntPrdNrm = BC000F19_A576ContratadaUsuario_CstUntPrdNrm[0];
            n576ContratadaUsuario_CstUntPrdNrm = BC000F19_n576ContratadaUsuario_CstUntPrdNrm[0];
            A577ContratadaUsuario_CstUntPrdExt = BC000F19_A577ContratadaUsuario_CstUntPrdExt[0];
            n577ContratadaUsuario_CstUntPrdExt = BC000F19_n577ContratadaUsuario_CstUntPrdExt[0];
            A1518ContratadaUsuario_TmpEstAnl = BC000F19_A1518ContratadaUsuario_TmpEstAnl[0];
            n1518ContratadaUsuario_TmpEstAnl = BC000F19_n1518ContratadaUsuario_TmpEstAnl[0];
            A1503ContratadaUsuario_TmpEstExc = BC000F19_A1503ContratadaUsuario_TmpEstExc[0];
            n1503ContratadaUsuario_TmpEstExc = BC000F19_n1503ContratadaUsuario_TmpEstExc[0];
            A1504ContratadaUsuario_TmpEstCrr = BC000F19_A1504ContratadaUsuario_TmpEstCrr[0];
            n1504ContratadaUsuario_TmpEstCrr = BC000F19_n1504ContratadaUsuario_TmpEstCrr[0];
            A66ContratadaUsuario_ContratadaCod = BC000F19_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = BC000F19_A69ContratadaUsuario_UsuarioCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F19_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F19_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F19_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F19_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F19_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F19_n70ContratadaUsuario_UsuarioPessoaCod[0];
         }
         Gx_mode = sMode16;
      }

      protected void ScanKeyEnd0F16( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm0F16( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0F16( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0F16( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0F16( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0F16( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0F16( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0F16( )
      {
      }

      protected void AddRow0F16( )
      {
         VarsToRow16( bcContratadaUsuario) ;
      }

      protected void ReadRow0F16( )
      {
         RowToVars16( bcContratadaUsuario, 1) ;
      }

      protected void InitializeNonKey0F16( )
      {
         A1228ContratadaUsuario_AreaTrabalhoCod = 0;
         n1228ContratadaUsuario_AreaTrabalhoCod = false;
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         n1297ContratadaUsuario_AreaTrabalhoDes = false;
         A67ContratadaUsuario_ContratadaPessoaCod = 0;
         n67ContratadaUsuario_ContratadaPessoaCod = false;
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         n68ContratadaUsuario_ContratadaPessoaNom = false;
         A348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         n348ContratadaUsuario_ContratadaPessoaCNPJ = false;
         A70ContratadaUsuario_UsuarioPessoaCod = 0;
         n70ContratadaUsuario_UsuarioPessoaCod = false;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         n71ContratadaUsuario_UsuarioPessoaNom = false;
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         n491ContratadaUsuario_UsuarioPessoaDoc = false;
         A1018ContratadaUsuario_UsuarioEhContratada = false;
         n1018ContratadaUsuario_UsuarioEhContratada = false;
         A1394ContratadaUsuario_UsuarioAtivo = false;
         n1394ContratadaUsuario_UsuarioAtivo = false;
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = "";
         n341Usuario_UserGamGuid = false;
         A576ContratadaUsuario_CstUntPrdNrm = 0;
         n576ContratadaUsuario_CstUntPrdNrm = false;
         A577ContratadaUsuario_CstUntPrdExt = 0;
         n577ContratadaUsuario_CstUntPrdExt = false;
         A1518ContratadaUsuario_TmpEstAnl = 0;
         n1518ContratadaUsuario_TmpEstAnl = false;
         A1503ContratadaUsuario_TmpEstExc = 0;
         n1503ContratadaUsuario_TmpEstExc = false;
         A1504ContratadaUsuario_TmpEstCrr = 0;
         n1504ContratadaUsuario_TmpEstCrr = false;
         Z576ContratadaUsuario_CstUntPrdNrm = 0;
         Z577ContratadaUsuario_CstUntPrdExt = 0;
         Z1518ContratadaUsuario_TmpEstAnl = 0;
         Z1503ContratadaUsuario_TmpEstExc = 0;
         Z1504ContratadaUsuario_TmpEstCrr = 0;
      }

      protected void InitAll0F16( )
      {
         A66ContratadaUsuario_ContratadaCod = 0;
         A69ContratadaUsuario_UsuarioCod = 0;
         InitializeNonKey0F16( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow16( SdtContratadaUsuario obj16 )
      {
         obj16.gxTpr_Mode = Gx_mode;
         obj16.gxTpr_Contratadausuario_areatrabalhocod = A1228ContratadaUsuario_AreaTrabalhoCod;
         obj16.gxTpr_Contratadausuario_areatrabalhodes = A1297ContratadaUsuario_AreaTrabalhoDes;
         obj16.gxTpr_Contratadausuario_contratadapessoacod = A67ContratadaUsuario_ContratadaPessoaCod;
         obj16.gxTpr_Contratadausuario_contratadapessoanom = A68ContratadaUsuario_ContratadaPessoaNom;
         obj16.gxTpr_Contratadausuario_contratadapessoacnpj = A348ContratadaUsuario_ContratadaPessoaCNPJ;
         obj16.gxTpr_Contratadausuario_usuariopessoacod = A70ContratadaUsuario_UsuarioPessoaCod;
         obj16.gxTpr_Contratadausuario_usuariopessoanom = A71ContratadaUsuario_UsuarioPessoaNom;
         obj16.gxTpr_Contratadausuario_usuariopessoadoc = A491ContratadaUsuario_UsuarioPessoaDoc;
         obj16.gxTpr_Contratadausuario_usuarioehcontratada = A1018ContratadaUsuario_UsuarioEhContratada;
         obj16.gxTpr_Contratadausuario_usuarioativo = A1394ContratadaUsuario_UsuarioAtivo;
         obj16.gxTpr_Usuario_nome = A2Usuario_Nome;
         obj16.gxTpr_Usuario_usergamguid = A341Usuario_UserGamGuid;
         obj16.gxTpr_Contratadausuario_cstuntprdnrm = A576ContratadaUsuario_CstUntPrdNrm;
         obj16.gxTpr_Contratadausuario_cstuntprdext = A577ContratadaUsuario_CstUntPrdExt;
         obj16.gxTpr_Contratadausuario_tmpestanl = A1518ContratadaUsuario_TmpEstAnl;
         obj16.gxTpr_Contratadausuario_tmpestexc = A1503ContratadaUsuario_TmpEstExc;
         obj16.gxTpr_Contratadausuario_tmpestcrr = A1504ContratadaUsuario_TmpEstCrr;
         obj16.gxTpr_Contratadausuario_contratadacod = A66ContratadaUsuario_ContratadaCod;
         obj16.gxTpr_Contratadausuario_usuariocod = A69ContratadaUsuario_UsuarioCod;
         obj16.gxTpr_Contratadausuario_contratadacod_Z = Z66ContratadaUsuario_ContratadaCod;
         obj16.gxTpr_Contratadausuario_areatrabalhocod_Z = Z1228ContratadaUsuario_AreaTrabalhoCod;
         obj16.gxTpr_Contratadausuario_areatrabalhodes_Z = Z1297ContratadaUsuario_AreaTrabalhoDes;
         obj16.gxTpr_Contratadausuario_contratadapessoacod_Z = Z67ContratadaUsuario_ContratadaPessoaCod;
         obj16.gxTpr_Contratadausuario_contratadapessoanom_Z = Z68ContratadaUsuario_ContratadaPessoaNom;
         obj16.gxTpr_Contratadausuario_contratadapessoacnpj_Z = Z348ContratadaUsuario_ContratadaPessoaCNPJ;
         obj16.gxTpr_Contratadausuario_usuariocod_Z = Z69ContratadaUsuario_UsuarioCod;
         obj16.gxTpr_Contratadausuario_usuariopessoacod_Z = Z70ContratadaUsuario_UsuarioPessoaCod;
         obj16.gxTpr_Contratadausuario_usuariopessoanom_Z = Z71ContratadaUsuario_UsuarioPessoaNom;
         obj16.gxTpr_Contratadausuario_usuariopessoadoc_Z = Z491ContratadaUsuario_UsuarioPessoaDoc;
         obj16.gxTpr_Contratadausuario_usuarioehcontratada_Z = Z1018ContratadaUsuario_UsuarioEhContratada;
         obj16.gxTpr_Contratadausuario_usuarioativo_Z = Z1394ContratadaUsuario_UsuarioAtivo;
         obj16.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj16.gxTpr_Usuario_usergamguid_Z = Z341Usuario_UserGamGuid;
         obj16.gxTpr_Contratadausuario_cstuntprdnrm_Z = Z576ContratadaUsuario_CstUntPrdNrm;
         obj16.gxTpr_Contratadausuario_cstuntprdext_Z = Z577ContratadaUsuario_CstUntPrdExt;
         obj16.gxTpr_Contratadausuario_tmpestanl_Z = Z1518ContratadaUsuario_TmpEstAnl;
         obj16.gxTpr_Contratadausuario_tmpestexc_Z = Z1503ContratadaUsuario_TmpEstExc;
         obj16.gxTpr_Contratadausuario_tmpestcrr_Z = Z1504ContratadaUsuario_TmpEstCrr;
         obj16.gxTpr_Contratadausuario_areatrabalhocod_N = (short)(Convert.ToInt16(n1228ContratadaUsuario_AreaTrabalhoCod));
         obj16.gxTpr_Contratadausuario_areatrabalhodes_N = (short)(Convert.ToInt16(n1297ContratadaUsuario_AreaTrabalhoDes));
         obj16.gxTpr_Contratadausuario_contratadapessoacod_N = (short)(Convert.ToInt16(n67ContratadaUsuario_ContratadaPessoaCod));
         obj16.gxTpr_Contratadausuario_contratadapessoanom_N = (short)(Convert.ToInt16(n68ContratadaUsuario_ContratadaPessoaNom));
         obj16.gxTpr_Contratadausuario_contratadapessoacnpj_N = (short)(Convert.ToInt16(n348ContratadaUsuario_ContratadaPessoaCNPJ));
         obj16.gxTpr_Contratadausuario_usuariopessoacod_N = (short)(Convert.ToInt16(n70ContratadaUsuario_UsuarioPessoaCod));
         obj16.gxTpr_Contratadausuario_usuariopessoanom_N = (short)(Convert.ToInt16(n71ContratadaUsuario_UsuarioPessoaNom));
         obj16.gxTpr_Contratadausuario_usuariopessoadoc_N = (short)(Convert.ToInt16(n491ContratadaUsuario_UsuarioPessoaDoc));
         obj16.gxTpr_Contratadausuario_usuarioehcontratada_N = (short)(Convert.ToInt16(n1018ContratadaUsuario_UsuarioEhContratada));
         obj16.gxTpr_Contratadausuario_usuarioativo_N = (short)(Convert.ToInt16(n1394ContratadaUsuario_UsuarioAtivo));
         obj16.gxTpr_Usuario_nome_N = (short)(Convert.ToInt16(n2Usuario_Nome));
         obj16.gxTpr_Usuario_usergamguid_N = (short)(Convert.ToInt16(n341Usuario_UserGamGuid));
         obj16.gxTpr_Contratadausuario_cstuntprdnrm_N = (short)(Convert.ToInt16(n576ContratadaUsuario_CstUntPrdNrm));
         obj16.gxTpr_Contratadausuario_cstuntprdext_N = (short)(Convert.ToInt16(n577ContratadaUsuario_CstUntPrdExt));
         obj16.gxTpr_Contratadausuario_tmpestanl_N = (short)(Convert.ToInt16(n1518ContratadaUsuario_TmpEstAnl));
         obj16.gxTpr_Contratadausuario_tmpestexc_N = (short)(Convert.ToInt16(n1503ContratadaUsuario_TmpEstExc));
         obj16.gxTpr_Contratadausuario_tmpestcrr_N = (short)(Convert.ToInt16(n1504ContratadaUsuario_TmpEstCrr));
         obj16.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow16( SdtContratadaUsuario obj16 )
      {
         obj16.gxTpr_Contratadausuario_contratadacod = A66ContratadaUsuario_ContratadaCod;
         obj16.gxTpr_Contratadausuario_usuariocod = A69ContratadaUsuario_UsuarioCod;
         return  ;
      }

      public void RowToVars16( SdtContratadaUsuario obj16 ,
                               int forceLoad )
      {
         Gx_mode = obj16.gxTpr_Mode;
         A1228ContratadaUsuario_AreaTrabalhoCod = obj16.gxTpr_Contratadausuario_areatrabalhocod;
         n1228ContratadaUsuario_AreaTrabalhoCod = false;
         A1297ContratadaUsuario_AreaTrabalhoDes = obj16.gxTpr_Contratadausuario_areatrabalhodes;
         n1297ContratadaUsuario_AreaTrabalhoDes = false;
         A67ContratadaUsuario_ContratadaPessoaCod = obj16.gxTpr_Contratadausuario_contratadapessoacod;
         n67ContratadaUsuario_ContratadaPessoaCod = false;
         A68ContratadaUsuario_ContratadaPessoaNom = obj16.gxTpr_Contratadausuario_contratadapessoanom;
         n68ContratadaUsuario_ContratadaPessoaNom = false;
         A348ContratadaUsuario_ContratadaPessoaCNPJ = obj16.gxTpr_Contratadausuario_contratadapessoacnpj;
         n348ContratadaUsuario_ContratadaPessoaCNPJ = false;
         A70ContratadaUsuario_UsuarioPessoaCod = obj16.gxTpr_Contratadausuario_usuariopessoacod;
         n70ContratadaUsuario_UsuarioPessoaCod = false;
         A71ContratadaUsuario_UsuarioPessoaNom = obj16.gxTpr_Contratadausuario_usuariopessoanom;
         n71ContratadaUsuario_UsuarioPessoaNom = false;
         A491ContratadaUsuario_UsuarioPessoaDoc = obj16.gxTpr_Contratadausuario_usuariopessoadoc;
         n491ContratadaUsuario_UsuarioPessoaDoc = false;
         A1018ContratadaUsuario_UsuarioEhContratada = obj16.gxTpr_Contratadausuario_usuarioehcontratada;
         n1018ContratadaUsuario_UsuarioEhContratada = false;
         A1394ContratadaUsuario_UsuarioAtivo = obj16.gxTpr_Contratadausuario_usuarioativo;
         n1394ContratadaUsuario_UsuarioAtivo = false;
         A2Usuario_Nome = obj16.gxTpr_Usuario_nome;
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = obj16.gxTpr_Usuario_usergamguid;
         n341Usuario_UserGamGuid = false;
         A576ContratadaUsuario_CstUntPrdNrm = obj16.gxTpr_Contratadausuario_cstuntprdnrm;
         n576ContratadaUsuario_CstUntPrdNrm = false;
         A577ContratadaUsuario_CstUntPrdExt = obj16.gxTpr_Contratadausuario_cstuntprdext;
         n577ContratadaUsuario_CstUntPrdExt = false;
         A1518ContratadaUsuario_TmpEstAnl = obj16.gxTpr_Contratadausuario_tmpestanl;
         n1518ContratadaUsuario_TmpEstAnl = false;
         A1503ContratadaUsuario_TmpEstExc = obj16.gxTpr_Contratadausuario_tmpestexc;
         n1503ContratadaUsuario_TmpEstExc = false;
         A1504ContratadaUsuario_TmpEstCrr = obj16.gxTpr_Contratadausuario_tmpestcrr;
         n1504ContratadaUsuario_TmpEstCrr = false;
         A66ContratadaUsuario_ContratadaCod = obj16.gxTpr_Contratadausuario_contratadacod;
         A69ContratadaUsuario_UsuarioCod = obj16.gxTpr_Contratadausuario_usuariocod;
         Z66ContratadaUsuario_ContratadaCod = obj16.gxTpr_Contratadausuario_contratadacod_Z;
         Z1228ContratadaUsuario_AreaTrabalhoCod = obj16.gxTpr_Contratadausuario_areatrabalhocod_Z;
         Z1297ContratadaUsuario_AreaTrabalhoDes = obj16.gxTpr_Contratadausuario_areatrabalhodes_Z;
         Z67ContratadaUsuario_ContratadaPessoaCod = obj16.gxTpr_Contratadausuario_contratadapessoacod_Z;
         Z68ContratadaUsuario_ContratadaPessoaNom = obj16.gxTpr_Contratadausuario_contratadapessoanom_Z;
         Z348ContratadaUsuario_ContratadaPessoaCNPJ = obj16.gxTpr_Contratadausuario_contratadapessoacnpj_Z;
         Z69ContratadaUsuario_UsuarioCod = obj16.gxTpr_Contratadausuario_usuariocod_Z;
         Z70ContratadaUsuario_UsuarioPessoaCod = obj16.gxTpr_Contratadausuario_usuariopessoacod_Z;
         Z71ContratadaUsuario_UsuarioPessoaNom = obj16.gxTpr_Contratadausuario_usuariopessoanom_Z;
         Z491ContratadaUsuario_UsuarioPessoaDoc = obj16.gxTpr_Contratadausuario_usuariopessoadoc_Z;
         Z1018ContratadaUsuario_UsuarioEhContratada = obj16.gxTpr_Contratadausuario_usuarioehcontratada_Z;
         Z1394ContratadaUsuario_UsuarioAtivo = obj16.gxTpr_Contratadausuario_usuarioativo_Z;
         Z2Usuario_Nome = obj16.gxTpr_Usuario_nome_Z;
         Z341Usuario_UserGamGuid = obj16.gxTpr_Usuario_usergamguid_Z;
         Z576ContratadaUsuario_CstUntPrdNrm = obj16.gxTpr_Contratadausuario_cstuntprdnrm_Z;
         Z577ContratadaUsuario_CstUntPrdExt = obj16.gxTpr_Contratadausuario_cstuntprdext_Z;
         Z1518ContratadaUsuario_TmpEstAnl = obj16.gxTpr_Contratadausuario_tmpestanl_Z;
         Z1503ContratadaUsuario_TmpEstExc = obj16.gxTpr_Contratadausuario_tmpestexc_Z;
         Z1504ContratadaUsuario_TmpEstCrr = obj16.gxTpr_Contratadausuario_tmpestcrr_Z;
         n1228ContratadaUsuario_AreaTrabalhoCod = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_areatrabalhocod_N));
         n1297ContratadaUsuario_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_areatrabalhodes_N));
         n67ContratadaUsuario_ContratadaPessoaCod = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_contratadapessoacod_N));
         n68ContratadaUsuario_ContratadaPessoaNom = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_contratadapessoanom_N));
         n348ContratadaUsuario_ContratadaPessoaCNPJ = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_contratadapessoacnpj_N));
         n70ContratadaUsuario_UsuarioPessoaCod = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_usuariopessoacod_N));
         n71ContratadaUsuario_UsuarioPessoaNom = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_usuariopessoanom_N));
         n491ContratadaUsuario_UsuarioPessoaDoc = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_usuariopessoadoc_N));
         n1018ContratadaUsuario_UsuarioEhContratada = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_usuarioehcontratada_N));
         n1394ContratadaUsuario_UsuarioAtivo = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_usuarioativo_N));
         n2Usuario_Nome = (bool)(Convert.ToBoolean(obj16.gxTpr_Usuario_nome_N));
         n341Usuario_UserGamGuid = (bool)(Convert.ToBoolean(obj16.gxTpr_Usuario_usergamguid_N));
         n576ContratadaUsuario_CstUntPrdNrm = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_cstuntprdnrm_N));
         n577ContratadaUsuario_CstUntPrdExt = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_cstuntprdext_N));
         n1518ContratadaUsuario_TmpEstAnl = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_tmpestanl_N));
         n1503ContratadaUsuario_TmpEstExc = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_tmpestexc_N));
         n1504ContratadaUsuario_TmpEstCrr = (bool)(Convert.ToBoolean(obj16.gxTpr_Contratadausuario_tmpestcrr_N));
         Gx_mode = obj16.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A66ContratadaUsuario_ContratadaCod = (int)getParm(obj,0);
         A69ContratadaUsuario_UsuarioCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0F16( ) ;
         ScanKeyStart0F16( ) ;
         if ( RcdFound16 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000F14 */
            pr_default.execute(12, new Object[] {A66ContratadaUsuario_ContratadaCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_CONTRATADACOD");
               AnyError = 1;
            }
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F14_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F14_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            pr_default.close(12);
            /* Using cursor BC000F15 */
            pr_default.execute(13, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F15_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F15_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            pr_default.close(13);
            /* Using cursor BC000F16 */
            pr_default.execute(14, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            pr_default.close(14);
            /* Using cursor BC000F17 */
            pr_default.execute(15, new Object[] {A69ContratadaUsuario_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
               AnyError = 1;
            }
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F17_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F17_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F17_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F17_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F17_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F17_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F17_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F17_n341Usuario_UserGamGuid[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F17_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F17_n70ContratadaUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000F18 */
            pr_default.execute(16, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F18_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F18_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
         }
         ZM0F16( -1) ;
         OnLoadActions0F16( ) ;
         AddRow0F16( ) ;
         ScanKeyEnd0F16( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars16( bcContratadaUsuario, 0) ;
         ScanKeyStart0F16( ) ;
         if ( RcdFound16 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000F14 */
            pr_default.execute(12, new Object[] {A66ContratadaUsuario_ContratadaCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_CONTRATADACOD");
               AnyError = 1;
            }
            A67ContratadaUsuario_ContratadaPessoaCod = BC000F14_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = BC000F14_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            pr_default.close(12);
            /* Using cursor BC000F15 */
            pr_default.execute(13, new Object[] {n67ContratadaUsuario_ContratadaPessoaCod, A67ContratadaUsuario_ContratadaPessoaCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A68ContratadaUsuario_ContratadaPessoaNom = BC000F15_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = BC000F15_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            n348ContratadaUsuario_ContratadaPessoaCNPJ = BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
            pr_default.close(13);
            /* Using cursor BC000F16 */
            pr_default.execute(14, new Object[] {n1228ContratadaUsuario_AreaTrabalhoCod, A1228ContratadaUsuario_AreaTrabalhoCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes[0];
            n1297ContratadaUsuario_AreaTrabalhoDes = BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes[0];
            pr_default.close(14);
            /* Using cursor BC000F17 */
            pr_default.execute(15, new Object[] {A69ContratadaUsuario_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contratada Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATADAUSUARIO_USUARIOCOD");
               AnyError = 1;
            }
            A1018ContratadaUsuario_UsuarioEhContratada = BC000F17_A1018ContratadaUsuario_UsuarioEhContratada[0];
            n1018ContratadaUsuario_UsuarioEhContratada = BC000F17_n1018ContratadaUsuario_UsuarioEhContratada[0];
            A1394ContratadaUsuario_UsuarioAtivo = BC000F17_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = BC000F17_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = BC000F17_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000F17_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000F17_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = BC000F17_n341Usuario_UserGamGuid[0];
            A70ContratadaUsuario_UsuarioPessoaCod = BC000F17_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = BC000F17_n70ContratadaUsuario_UsuarioPessoaCod[0];
            pr_default.close(15);
            /* Using cursor BC000F18 */
            pr_default.execute(16, new Object[] {n70ContratadaUsuario_UsuarioPessoaCod, A70ContratadaUsuario_UsuarioPessoaCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A71ContratadaUsuario_UsuarioPessoaNom = BC000F18_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = BC000F18_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z66ContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
            Z69ContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
         }
         ZM0F16( -1) ;
         OnLoadActions0F16( ) ;
         AddRow0F16( ) ;
         ScanKeyEnd0F16( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars16( bcContratadaUsuario, 0) ;
         nKeyPressed = 1;
         GetKey0F16( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0F16( ) ;
         }
         else
         {
            if ( RcdFound16 == 1 )
            {
               if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
               {
                  A66ContratadaUsuario_ContratadaCod = Z66ContratadaUsuario_ContratadaCod;
                  A69ContratadaUsuario_UsuarioCod = Z69ContratadaUsuario_UsuarioCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0F16( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0F16( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0F16( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow16( bcContratadaUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars16( bcContratadaUsuario, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0F16( ) ;
         if ( RcdFound16 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
            {
               A66ContratadaUsuario_ContratadaCod = Z66ContratadaUsuario_ContratadaCod;
               A69ContratadaUsuario_UsuarioCod = Z69ContratadaUsuario_UsuarioCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A66ContratadaUsuario_ContratadaCod != Z66ContratadaUsuario_ContratadaCod ) || ( A69ContratadaUsuario_UsuarioCod != Z69ContratadaUsuario_UsuarioCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
         context.RollbackDataStores( "ContratadaUsuario_BC");
         VarsToRow16( bcContratadaUsuario) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratadaUsuario.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratadaUsuario.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratadaUsuario )
         {
            bcContratadaUsuario = (SdtContratadaUsuario)(sdt);
            if ( StringUtil.StrCmp(bcContratadaUsuario.gxTpr_Mode, "") == 0 )
            {
               bcContratadaUsuario.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow16( bcContratadaUsuario) ;
            }
            else
            {
               RowToVars16( bcContratadaUsuario, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratadaUsuario.gxTpr_Mode, "") == 0 )
            {
               bcContratadaUsuario.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars16( bcContratadaUsuario, 1) ;
         return  ;
      }

      public SdtContratadaUsuario ContratadaUsuario_BC
      {
         get {
            return bcContratadaUsuario ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z2Usuario_Nome = "";
         A2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         A341Usuario_UserGamGuid = "";
         Z68ContratadaUsuario_ContratadaPessoaNom = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         Z348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         A348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         Z1297ContratadaUsuario_AreaTrabalhoDes = "";
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         Z71ContratadaUsuario_UsuarioPessoaNom = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         Z491ContratadaUsuario_UsuarioPessoaDoc = "";
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         BC000F9_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         BC000F9_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         BC000F9_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         BC000F9_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         BC000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         BC000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         BC000F9_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000F9_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000F9_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000F9_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000F9_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F9_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F9_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F9_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F9_A2Usuario_Nome = new String[] {""} ;
         BC000F9_n2Usuario_Nome = new bool[] {false} ;
         BC000F9_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000F9_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000F9_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         BC000F9_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         BC000F9_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         BC000F9_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         BC000F9_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         BC000F9_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         BC000F9_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         BC000F9_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         BC000F9_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         BC000F9_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         BC000F9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000F9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000F9_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         BC000F9_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         BC000F9_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         BC000F9_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000F9_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         BC000F9_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000F4_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         BC000F4_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         BC000F4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         BC000F4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000F6_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         BC000F6_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         BC000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         BC000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         BC000F7_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         BC000F7_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         BC000F5_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F5_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F5_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F5_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F5_A2Usuario_Nome = new String[] {""} ;
         BC000F5_n2Usuario_Nome = new bool[] {false} ;
         BC000F5_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000F5_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000F5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         BC000F5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000F8_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000F8_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000F8_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000F8_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000F10_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000F10_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000F3_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         BC000F3_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         BC000F3_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         BC000F3_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         BC000F3_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         BC000F3_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         BC000F3_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         BC000F3_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         BC000F3_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         BC000F3_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         BC000F3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000F3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         sMode16 = "";
         BC000F2_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         BC000F2_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         BC000F2_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         BC000F2_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         BC000F2_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         BC000F2_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         BC000F2_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         BC000F2_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         BC000F2_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         BC000F2_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         BC000F2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000F2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000F14_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         BC000F14_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000F15_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         BC000F15_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         BC000F17_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F17_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F17_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F17_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F17_A2Usuario_Nome = new String[] {""} ;
         BC000F17_n2Usuario_Nome = new bool[] {false} ;
         BC000F17_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000F17_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000F17_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         BC000F17_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BC000F18_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000F18_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000F19_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         BC000F19_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         BC000F19_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         BC000F19_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         BC000F19_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         BC000F19_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         BC000F19_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         BC000F19_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         BC000F19_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         BC000F19_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         BC000F19_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F19_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         BC000F19_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F19_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         BC000F19_A2Usuario_Nome = new String[] {""} ;
         BC000F19_n2Usuario_Nome = new bool[] {false} ;
         BC000F19_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000F19_n341Usuario_UserGamGuid = new bool[] {false} ;
         BC000F19_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         BC000F19_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         BC000F19_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         BC000F19_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         BC000F19_A1518ContratadaUsuario_TmpEstAnl = new int[1] ;
         BC000F19_n1518ContratadaUsuario_TmpEstAnl = new bool[] {false} ;
         BC000F19_A1503ContratadaUsuario_TmpEstExc = new int[1] ;
         BC000F19_n1503ContratadaUsuario_TmpEstExc = new bool[] {false} ;
         BC000F19_A1504ContratadaUsuario_TmpEstCrr = new int[1] ;
         BC000F19_n1504ContratadaUsuario_TmpEstCrr = new bool[] {false} ;
         BC000F19_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000F19_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000F19_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         BC000F19_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         BC000F19_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         BC000F19_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         BC000F19_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         BC000F19_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadausuario_bc__default(),
            new Object[][] {
                new Object[] {
               BC000F2_A576ContratadaUsuario_CstUntPrdNrm, BC000F2_n576ContratadaUsuario_CstUntPrdNrm, BC000F2_A577ContratadaUsuario_CstUntPrdExt, BC000F2_n577ContratadaUsuario_CstUntPrdExt, BC000F2_A1518ContratadaUsuario_TmpEstAnl, BC000F2_n1518ContratadaUsuario_TmpEstAnl, BC000F2_A1503ContratadaUsuario_TmpEstExc, BC000F2_n1503ContratadaUsuario_TmpEstExc, BC000F2_A1504ContratadaUsuario_TmpEstCrr, BC000F2_n1504ContratadaUsuario_TmpEstCrr,
               BC000F2_A66ContratadaUsuario_ContratadaCod, BC000F2_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               BC000F3_A576ContratadaUsuario_CstUntPrdNrm, BC000F3_n576ContratadaUsuario_CstUntPrdNrm, BC000F3_A577ContratadaUsuario_CstUntPrdExt, BC000F3_n577ContratadaUsuario_CstUntPrdExt, BC000F3_A1518ContratadaUsuario_TmpEstAnl, BC000F3_n1518ContratadaUsuario_TmpEstAnl, BC000F3_A1503ContratadaUsuario_TmpEstExc, BC000F3_n1503ContratadaUsuario_TmpEstExc, BC000F3_A1504ContratadaUsuario_TmpEstCrr, BC000F3_n1504ContratadaUsuario_TmpEstCrr,
               BC000F3_A66ContratadaUsuario_ContratadaCod, BC000F3_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               BC000F4_A67ContratadaUsuario_ContratadaPessoaCod, BC000F4_n67ContratadaUsuario_ContratadaPessoaCod, BC000F4_A1228ContratadaUsuario_AreaTrabalhoCod, BC000F4_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               BC000F5_A1018ContratadaUsuario_UsuarioEhContratada, BC000F5_n1018ContratadaUsuario_UsuarioEhContratada, BC000F5_A1394ContratadaUsuario_UsuarioAtivo, BC000F5_n1394ContratadaUsuario_UsuarioAtivo, BC000F5_A2Usuario_Nome, BC000F5_n2Usuario_Nome, BC000F5_A341Usuario_UserGamGuid, BC000F5_n341Usuario_UserGamGuid, BC000F5_A70ContratadaUsuario_UsuarioPessoaCod, BC000F5_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000F6_A68ContratadaUsuario_ContratadaPessoaNom, BC000F6_n68ContratadaUsuario_ContratadaPessoaNom, BC000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ
               }
               , new Object[] {
               BC000F7_A1297ContratadaUsuario_AreaTrabalhoDes, BC000F7_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               BC000F8_A71ContratadaUsuario_UsuarioPessoaNom, BC000F8_n71ContratadaUsuario_UsuarioPessoaNom, BC000F8_A491ContratadaUsuario_UsuarioPessoaDoc, BC000F8_n491ContratadaUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               BC000F9_A1297ContratadaUsuario_AreaTrabalhoDes, BC000F9_n1297ContratadaUsuario_AreaTrabalhoDes, BC000F9_A68ContratadaUsuario_ContratadaPessoaNom, BC000F9_n68ContratadaUsuario_ContratadaPessoaNom, BC000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F9_A71ContratadaUsuario_UsuarioPessoaNom, BC000F9_n71ContratadaUsuario_UsuarioPessoaNom, BC000F9_A491ContratadaUsuario_UsuarioPessoaDoc, BC000F9_n491ContratadaUsuario_UsuarioPessoaDoc,
               BC000F9_A1018ContratadaUsuario_UsuarioEhContratada, BC000F9_n1018ContratadaUsuario_UsuarioEhContratada, BC000F9_A1394ContratadaUsuario_UsuarioAtivo, BC000F9_n1394ContratadaUsuario_UsuarioAtivo, BC000F9_A2Usuario_Nome, BC000F9_n2Usuario_Nome, BC000F9_A341Usuario_UserGamGuid, BC000F9_n341Usuario_UserGamGuid, BC000F9_A576ContratadaUsuario_CstUntPrdNrm, BC000F9_n576ContratadaUsuario_CstUntPrdNrm,
               BC000F9_A577ContratadaUsuario_CstUntPrdExt, BC000F9_n577ContratadaUsuario_CstUntPrdExt, BC000F9_A1518ContratadaUsuario_TmpEstAnl, BC000F9_n1518ContratadaUsuario_TmpEstAnl, BC000F9_A1503ContratadaUsuario_TmpEstExc, BC000F9_n1503ContratadaUsuario_TmpEstExc, BC000F9_A1504ContratadaUsuario_TmpEstCrr, BC000F9_n1504ContratadaUsuario_TmpEstCrr, BC000F9_A66ContratadaUsuario_ContratadaCod, BC000F9_A69ContratadaUsuario_UsuarioCod,
               BC000F9_A67ContratadaUsuario_ContratadaPessoaCod, BC000F9_n67ContratadaUsuario_ContratadaPessoaCod, BC000F9_A1228ContratadaUsuario_AreaTrabalhoCod, BC000F9_n1228ContratadaUsuario_AreaTrabalhoCod, BC000F9_A70ContratadaUsuario_UsuarioPessoaCod, BC000F9_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000F10_A66ContratadaUsuario_ContratadaCod, BC000F10_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000F14_A67ContratadaUsuario_ContratadaPessoaCod, BC000F14_n67ContratadaUsuario_ContratadaPessoaCod, BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod, BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               BC000F15_A68ContratadaUsuario_ContratadaPessoaNom, BC000F15_n68ContratadaUsuario_ContratadaPessoaNom, BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ
               }
               , new Object[] {
               BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes, BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               BC000F17_A1018ContratadaUsuario_UsuarioEhContratada, BC000F17_n1018ContratadaUsuario_UsuarioEhContratada, BC000F17_A1394ContratadaUsuario_UsuarioAtivo, BC000F17_n1394ContratadaUsuario_UsuarioAtivo, BC000F17_A2Usuario_Nome, BC000F17_n2Usuario_Nome, BC000F17_A341Usuario_UserGamGuid, BC000F17_n341Usuario_UserGamGuid, BC000F17_A70ContratadaUsuario_UsuarioPessoaCod, BC000F17_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               BC000F18_A71ContratadaUsuario_UsuarioPessoaNom, BC000F18_n71ContratadaUsuario_UsuarioPessoaNom, BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc, BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               BC000F19_A1297ContratadaUsuario_AreaTrabalhoDes, BC000F19_n1297ContratadaUsuario_AreaTrabalhoDes, BC000F19_A68ContratadaUsuario_ContratadaPessoaNom, BC000F19_n68ContratadaUsuario_ContratadaPessoaNom, BC000F19_A348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F19_n348ContratadaUsuario_ContratadaPessoaCNPJ, BC000F19_A71ContratadaUsuario_UsuarioPessoaNom, BC000F19_n71ContratadaUsuario_UsuarioPessoaNom, BC000F19_A491ContratadaUsuario_UsuarioPessoaDoc, BC000F19_n491ContratadaUsuario_UsuarioPessoaDoc,
               BC000F19_A1018ContratadaUsuario_UsuarioEhContratada, BC000F19_n1018ContratadaUsuario_UsuarioEhContratada, BC000F19_A1394ContratadaUsuario_UsuarioAtivo, BC000F19_n1394ContratadaUsuario_UsuarioAtivo, BC000F19_A2Usuario_Nome, BC000F19_n2Usuario_Nome, BC000F19_A341Usuario_UserGamGuid, BC000F19_n341Usuario_UserGamGuid, BC000F19_A576ContratadaUsuario_CstUntPrdNrm, BC000F19_n576ContratadaUsuario_CstUntPrdNrm,
               BC000F19_A577ContratadaUsuario_CstUntPrdExt, BC000F19_n577ContratadaUsuario_CstUntPrdExt, BC000F19_A1518ContratadaUsuario_TmpEstAnl, BC000F19_n1518ContratadaUsuario_TmpEstAnl, BC000F19_A1503ContratadaUsuario_TmpEstExc, BC000F19_n1503ContratadaUsuario_TmpEstExc, BC000F19_A1504ContratadaUsuario_TmpEstCrr, BC000F19_n1504ContratadaUsuario_TmpEstCrr, BC000F19_A66ContratadaUsuario_ContratadaCod, BC000F19_A69ContratadaUsuario_UsuarioCod,
               BC000F19_A67ContratadaUsuario_ContratadaPessoaCod, BC000F19_n67ContratadaUsuario_ContratadaPessoaCod, BC000F19_A1228ContratadaUsuario_AreaTrabalhoCod, BC000F19_n1228ContratadaUsuario_AreaTrabalhoCod, BC000F19_A70ContratadaUsuario_UsuarioPessoaCod, BC000F19_n70ContratadaUsuario_UsuarioPessoaCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120F2 */
         E120F2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound16 ;
      private int trnEnded ;
      private int Z66ContratadaUsuario_ContratadaCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int Z69ContratadaUsuario_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV8ContratadaUsuario_UsuarioCod ;
      private int Z1518ContratadaUsuario_TmpEstAnl ;
      private int A1518ContratadaUsuario_TmpEstAnl ;
      private int Z1503ContratadaUsuario_TmpEstExc ;
      private int A1503ContratadaUsuario_TmpEstExc ;
      private int Z1504ContratadaUsuario_TmpEstCrr ;
      private int A1504ContratadaUsuario_TmpEstCrr ;
      private int Z67ContratadaUsuario_ContratadaPessoaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int Z1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int Z70ContratadaUsuario_UsuarioPessoaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private decimal Z576ContratadaUsuario_CstUntPrdNrm ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal Z577ContratadaUsuario_CstUntPrdExt ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z2Usuario_Nome ;
      private String A2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String A341Usuario_UserGamGuid ;
      private String Z68ContratadaUsuario_ContratadaPessoaNom ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String Z71ContratadaUsuario_UsuarioPessoaNom ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String sMode16 ;
      private bool Z1018ContratadaUsuario_UsuarioEhContratada ;
      private bool A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool Z1394ContratadaUsuario_UsuarioAtivo ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n2Usuario_Nome ;
      private bool n341Usuario_UserGamGuid ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool n1518ContratadaUsuario_TmpEstAnl ;
      private bool n1503ContratadaUsuario_TmpEstExc ;
      private bool n1504ContratadaUsuario_TmpEstCrr ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private String Z348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String Z1297ContratadaUsuario_AreaTrabalhoDes ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private String Z491ContratadaUsuario_UsuarioPessoaDoc ;
      private String A491ContratadaUsuario_UsuarioPessoaDoc ;
      private IGxSession AV11WebSession ;
      private SdtContratadaUsuario bcContratadaUsuario ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000F9_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F9_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private String[] BC000F9_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] BC000F9_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] BC000F9_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] BC000F9_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] BC000F9_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] BC000F9_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] BC000F9_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F9_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F9_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F9_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F9_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] BC000F9_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] BC000F9_A2Usuario_Nome ;
      private bool[] BC000F9_n2Usuario_Nome ;
      private String[] BC000F9_A341Usuario_UserGamGuid ;
      private bool[] BC000F9_n341Usuario_UserGamGuid ;
      private decimal[] BC000F9_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] BC000F9_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] BC000F9_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] BC000F9_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] BC000F9_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] BC000F9_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] BC000F9_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] BC000F9_n1503ContratadaUsuario_TmpEstExc ;
      private int[] BC000F9_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] BC000F9_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] BC000F9_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000F9_A69ContratadaUsuario_UsuarioCod ;
      private int[] BC000F9_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] BC000F9_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] BC000F9_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] BC000F9_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] BC000F9_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] BC000F9_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] BC000F4_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] BC000F4_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] BC000F4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] BC000F4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] BC000F6_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] BC000F6_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] BC000F6_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] BC000F6_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] BC000F7_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F7_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F5_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F5_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F5_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] BC000F5_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] BC000F5_A2Usuario_Nome ;
      private bool[] BC000F5_n2Usuario_Nome ;
      private String[] BC000F5_A341Usuario_UserGamGuid ;
      private bool[] BC000F5_n341Usuario_UserGamGuid ;
      private int[] BC000F5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] BC000F5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] BC000F8_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] BC000F8_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] BC000F8_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F8_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private int[] BC000F10_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000F10_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] BC000F3_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] BC000F3_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] BC000F3_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] BC000F3_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] BC000F3_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] BC000F3_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] BC000F3_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] BC000F3_n1503ContratadaUsuario_TmpEstExc ;
      private int[] BC000F3_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] BC000F3_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] BC000F3_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000F3_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] BC000F2_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] BC000F2_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] BC000F2_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] BC000F2_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] BC000F2_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] BC000F2_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] BC000F2_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] BC000F2_n1503ContratadaUsuario_TmpEstExc ;
      private int[] BC000F2_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] BC000F2_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] BC000F2_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000F2_A69ContratadaUsuario_UsuarioCod ;
      private int[] BC000F14_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] BC000F14_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] BC000F14_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] BC000F14_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] BC000F15_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] BC000F15_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] BC000F15_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] BC000F15_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] BC000F16_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F16_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F17_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F17_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F17_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] BC000F17_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] BC000F17_A2Usuario_Nome ;
      private bool[] BC000F17_n2Usuario_Nome ;
      private String[] BC000F17_A341Usuario_UserGamGuid ;
      private bool[] BC000F17_n341Usuario_UserGamGuid ;
      private int[] BC000F17_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] BC000F17_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] BC000F18_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] BC000F18_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] BC000F18_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F18_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private String[] BC000F19_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] BC000F19_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private String[] BC000F19_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] BC000F19_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] BC000F19_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] BC000F19_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String[] BC000F19_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] BC000F19_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] BC000F19_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F19_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] BC000F19_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F19_n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] BC000F19_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] BC000F19_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] BC000F19_A2Usuario_Nome ;
      private bool[] BC000F19_n2Usuario_Nome ;
      private String[] BC000F19_A341Usuario_UserGamGuid ;
      private bool[] BC000F19_n341Usuario_UserGamGuid ;
      private decimal[] BC000F19_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] BC000F19_n576ContratadaUsuario_CstUntPrdNrm ;
      private decimal[] BC000F19_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] BC000F19_n577ContratadaUsuario_CstUntPrdExt ;
      private int[] BC000F19_A1518ContratadaUsuario_TmpEstAnl ;
      private bool[] BC000F19_n1518ContratadaUsuario_TmpEstAnl ;
      private int[] BC000F19_A1503ContratadaUsuario_TmpEstExc ;
      private bool[] BC000F19_n1503ContratadaUsuario_TmpEstExc ;
      private int[] BC000F19_A1504ContratadaUsuario_TmpEstCrr ;
      private bool[] BC000F19_n1504ContratadaUsuario_TmpEstCrr ;
      private int[] BC000F19_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000F19_A69ContratadaUsuario_UsuarioCod ;
      private int[] BC000F19_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] BC000F19_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] BC000F19_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] BC000F19_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] BC000F19_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] BC000F19_n70ContratadaUsuario_UsuarioPessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratadausuario_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000F9 ;
          prmBC000F9 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F4 ;
          prmBC000F4 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F6 ;
          prmBC000F6 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F7 ;
          prmBC000F7 = new Object[] {
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F5 ;
          prmBC000F5 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F8 ;
          prmBC000F8 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F10 ;
          prmBC000F10 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F3 ;
          prmBC000F3 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F2 ;
          prmBC000F2 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F11 ;
          prmBC000F11 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F12 ;
          prmBC000F12 = new Object[] {
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F13 ;
          prmBC000F13 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F19 ;
          prmBC000F19 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F14 ;
          prmBC000F14 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F15 ;
          prmBC000F15 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F16 ;
          prmBC000F16 = new Object[] {
          new Object[] {"@ContratadaUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F17 ;
          prmBC000F17 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000F18 ;
          prmBC000F18 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000F2", "SELECT [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (UPDLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F2,1,0,true,false )
             ,new CursorDef("BC000F3", "SELECT [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F3,1,0,true,false )
             ,new CursorDef("BC000F4", "SELECT [Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, [Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratadaUsuario_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F4,1,0,true,false )
             ,new CursorDef("BC000F5", "SELECT [Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, [Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F5,1,0,true,false )
             ,new CursorDef("BC000F6", "SELECT [Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, [Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_ContratadaPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F6,1,0,true,false )
             ,new CursorDef("BC000F7", "SELECT [AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratadaUsuario_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F7,1,0,true,false )
             ,new CursorDef("BC000F8", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, [Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F8,1,0,true,false )
             ,new CursorDef("BC000F9", "SELECT T4.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T3.[Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ, T6.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T6.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso, T5.[Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, T5.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T5.[Usuario_Nome], T5.[Usuario_UserGamGuid], TM1.[ContratadaUsuario_CstUntPrdNrm], TM1.[ContratadaUsuario_CstUntPrdExt], TM1.[ContratadaUsuario_TmpEstAnl], TM1.[ContratadaUsuario_TmpEstExc], TM1.[ContratadaUsuario_TmpEstCrr], TM1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, TM1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T5.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM ((((([ContratadaUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = TM1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and TM1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY TM1.[ContratadaUsuario_ContratadaCod], TM1.[ContratadaUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F9,100,0,true,false )
             ,new CursorDef("BC000F10", "SELECT [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F10,1,0,true,false )
             ,new CursorDef("BC000F11", "INSERT INTO [ContratadaUsuario]([ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstAnl], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod]) VALUES(@ContratadaUsuario_CstUntPrdNrm, @ContratadaUsuario_CstUntPrdExt, @ContratadaUsuario_TmpEstAnl, @ContratadaUsuario_TmpEstExc, @ContratadaUsuario_TmpEstCrr, @ContratadaUsuario_ContratadaCod, @ContratadaUsuario_UsuarioCod)", GxErrorMask.GX_NOMASK,prmBC000F11)
             ,new CursorDef("BC000F12", "UPDATE [ContratadaUsuario] SET [ContratadaUsuario_CstUntPrdNrm]=@ContratadaUsuario_CstUntPrdNrm, [ContratadaUsuario_CstUntPrdExt]=@ContratadaUsuario_CstUntPrdExt, [ContratadaUsuario_TmpEstAnl]=@ContratadaUsuario_TmpEstAnl, [ContratadaUsuario_TmpEstExc]=@ContratadaUsuario_TmpEstExc, [ContratadaUsuario_TmpEstCrr]=@ContratadaUsuario_TmpEstCrr  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC000F12)
             ,new CursorDef("BC000F13", "DELETE FROM [ContratadaUsuario]  WHERE [ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod AND [ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC000F13)
             ,new CursorDef("BC000F14", "SELECT [Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, [Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratadaUsuario_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F14,1,0,true,false )
             ,new CursorDef("BC000F15", "SELECT [Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, [Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_ContratadaPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F15,1,0,true,false )
             ,new CursorDef("BC000F16", "SELECT [AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratadaUsuario_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F16,1,0,true,false )
             ,new CursorDef("BC000F17", "SELECT [Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, [Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratadaUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F17,1,0,true,false )
             ,new CursorDef("BC000F18", "SELECT [Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, [Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratadaUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F18,1,0,true,false )
             ,new CursorDef("BC000F19", "SELECT T4.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T3.[Pessoa_Docto] AS ContratadaUsuario_ContratadaPessoaCNPJ, T6.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T6.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPesso, T5.[Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, T5.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T5.[Usuario_Nome], T5.[Usuario_UserGamGuid], TM1.[ContratadaUsuario_CstUntPrdNrm], TM1.[ContratadaUsuario_CstUntPrdExt], TM1.[ContratadaUsuario_TmpEstAnl], TM1.[ContratadaUsuario_TmpEstExc], TM1.[ContratadaUsuario_TmpEstCrr], TM1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, TM1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T5.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM ((((([ContratadaUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = TM1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratadaUsuario_ContratadaCod] = @ContratadaUsuario_ContratadaCod and TM1.[ContratadaUsuario_UsuarioCod] = @ContratadaUsuario_UsuarioCod ORDER BY TM1.[ContratadaUsuario_ContratadaCod], TM1.[ContratadaUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000F19,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 40) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 40) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 40) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 40) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
