/*
               File: DP_Carrega_MenuAcessoRapidoAdm
        Description: Carrega_Menu Acesso Rapido Administrator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:36.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_carrega_menuacessorapidoadm : GXProcedure
   {
      public dp_carrega_menuacessorapidoadm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_carrega_menuacessorapidoadm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         dp_carrega_menuacessorapidoadm objdp_carrega_menuacessorapidoadm;
         objdp_carrega_menuacessorapidoadm = new dp_carrega_menuacessorapidoadm();
         objdp_carrega_menuacessorapidoadm.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         objdp_carrega_menuacessorapidoadm.context.SetSubmitInitialConfig(context);
         objdp_carrega_menuacessorapidoadm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_carrega_menuacessorapidoadm);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_carrega_menuacessorapidoadm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00052 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A280Menu_Tipo = P00052_A280Menu_Tipo[0];
            A284Menu_Ativo = P00052_A284Menu_Ativo[0];
            A40000Menu_Imagem_GXI = P00052_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = P00052_n40000Menu_Imagem_GXI[0];
            A283Menu_Ordem = P00052_A283Menu_Ordem[0];
            A279Menu_Descricao = P00052_A279Menu_Descricao[0];
            n279Menu_Descricao = P00052_n279Menu_Descricao[0];
            A281Menu_Link = P00052_A281Menu_Link[0];
            n281Menu_Link = P00052_n281Menu_Link[0];
            A278Menu_Nome = P00052_A278Menu_Nome[0];
            A277Menu_Codigo = P00052_A277Menu_Codigo[0];
            A282Menu_Imagem = P00052_A282Menu_Imagem[0];
            n282Menu_Imagem = P00052_n282Menu_Imagem[0];
            Gxm1sdt_menuacessorapido = new SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem(context);
            Gxm2rootcol.Add(Gxm1sdt_menuacessorapido, 0);
            Gxm1sdt_menuacessorapido.gxTpr_Menu_codigo = A277Menu_Codigo;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_nome = A278Menu_Nome;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_link = A281Menu_Link;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_imagem = A282Menu_Imagem;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_imagem_gxi = A40000Menu_Imagem_GXI;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_descricao = A279Menu_Descricao;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_ordem = A283Menu_Ordem;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00052_A280Menu_Tipo = new short[1] ;
         P00052_A284Menu_Ativo = new bool[] {false} ;
         P00052_A40000Menu_Imagem_GXI = new String[] {""} ;
         P00052_n40000Menu_Imagem_GXI = new bool[] {false} ;
         P00052_A283Menu_Ordem = new short[1] ;
         P00052_A279Menu_Descricao = new String[] {""} ;
         P00052_n279Menu_Descricao = new bool[] {false} ;
         P00052_A281Menu_Link = new String[] {""} ;
         P00052_n281Menu_Link = new bool[] {false} ;
         P00052_A278Menu_Nome = new String[] {""} ;
         P00052_A277Menu_Codigo = new int[1] ;
         P00052_A282Menu_Imagem = new String[] {""} ;
         P00052_n282Menu_Imagem = new bool[] {false} ;
         A40000Menu_Imagem_GXI = "";
         A279Menu_Descricao = "";
         A281Menu_Link = "";
         A278Menu_Nome = "";
         A282Menu_Imagem = "";
         Gxm1sdt_menuacessorapido = new SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_carrega_menuacessorapidoadm__default(),
            new Object[][] {
                new Object[] {
               P00052_A280Menu_Tipo, P00052_A284Menu_Ativo, P00052_A40000Menu_Imagem_GXI, P00052_n40000Menu_Imagem_GXI, P00052_A283Menu_Ordem, P00052_A279Menu_Descricao, P00052_n279Menu_Descricao, P00052_A281Menu_Link, P00052_n281Menu_Link, P00052_A278Menu_Nome,
               P00052_A277Menu_Codigo, P00052_A282Menu_Imagem, P00052_n282Menu_Imagem
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private int A277Menu_Codigo ;
      private String scmdbuf ;
      private String A278Menu_Nome ;
      private bool A284Menu_Ativo ;
      private bool n40000Menu_Imagem_GXI ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private bool n282Menu_Imagem ;
      private String A40000Menu_Imagem_GXI ;
      private String A279Menu_Descricao ;
      private String A281Menu_Link ;
      private String A282Menu_Imagem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P00052_A280Menu_Tipo ;
      private bool[] P00052_A284Menu_Ativo ;
      private String[] P00052_A40000Menu_Imagem_GXI ;
      private bool[] P00052_n40000Menu_Imagem_GXI ;
      private short[] P00052_A283Menu_Ordem ;
      private String[] P00052_A279Menu_Descricao ;
      private bool[] P00052_n279Menu_Descricao ;
      private String[] P00052_A281Menu_Link ;
      private bool[] P00052_n281Menu_Link ;
      private String[] P00052_A278Menu_Nome ;
      private int[] P00052_A277Menu_Codigo ;
      private String[] P00052_A282Menu_Imagem ;
      private bool[] P00052_n282Menu_Imagem ;
      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem Gxm1sdt_menuacessorapido ;
   }

   public class dp_carrega_menuacessorapidoadm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00052 ;
          prmP00052 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00052", "SELECT DISTINCT NULL AS [Menu_Tipo], NULL AS [Menu_Ativo], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo], [Menu_Imagem] FROM ( SELECT TOP(100) PERCENT [Menu_Tipo], [Menu_Ativo], [Menu_Imagem_GXI], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE ([Menu_Tipo] = 2) AND ([Menu_Ativo] = 1) ORDER BY [Menu_Codigo]) DistinctT ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00052,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaUri(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((String[]) buf[11])[0] = rslt.getMultimediaFile(9, rslt.getVarchar(3)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
